<?php
class Admin_DealerzyController extends Ogolny_Controller_Admin
{
    public function init()
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
    }

    public function dodajAction()
	{
        if ($this->_request->isPost())
		{
            $firma = $this->_request->getPost('firma');
            $mail = $this->_request->getPost('mail');
            $woj = $this->_request->getPost('woj');
			$kod = $this->_request->getPost('kod');
			$ulica = $this->_request->getPost('ulica');
			$miasto = $this->_request->getPost('miasto');
			$kier = $this->_request->getPost('kier', '');
			$tel = $this->_request->getPost('tel');
			$fax = $this->_request->getPost('fax');

            if (empty($firma)) 
			{
                $error['err'] = '<div class="k_blad">Pole nazwa jest wymagane</div>';
            }

            if (empty($error))
			{
                $addArray = array(
                    'firma' => $firma,
                    'mail' => $mail,
                    'woj' => $woj,
                    'kod' => $kod,
					'ulica' => $ulica,
					'miasto' => $miasto,
					'kier' => $kier,
					'tel' => $tel,
					'fax' => $fax
                );
                $nowa = new Dealerzy;

                $id = $nowa->dodaj($addArray);

                $this->_redirect("admin/dealerzy/edytuj/id/" . $id);
                $this->view->error = '<div class="k_ok">Edycja przebiegla pomyslnie</div>';
            }
			else
			{
                $this->view->error = $error['err'];
            }
        }
    }

	public function indexAction()
	{
		
	}
	
    public function listaAction()
	{
        $wszystkie = new Dealerzy;
        if ($this->_request->getParam('delid') > 0)
		{
            $filter = new Zend_Filter_StripTags();
            $id = $filter->filter($this->_request->getParam('delid'));

            $db = $wszystkie->getAdapter();
            $where = $db->quoteInto('id = ?', $id);
            $rows_affected = $wszystkie->delete($where);
        }

        $all = $wszystkie->fetchAll();
        $this->view->sklepy = $all;
    }

    public function edytujAction()
	{
        $wybrana = new Dealerzy;

        $filter = new Zend_Filter_StripTags;
        $id = $filter->filter($this->_request->getParam('id'));
		
		//echo $id;

        if ($this->_request->isPost())
		{
            $firma = $this->_request->getPost('firma');
            $mail = $this->_request->getPost('mail');
            $woj = $this->_request->getPost('woj');
			$kod = $this->_request->getPost('kod');
			$ulica = $this->_request->getPost('ulica');
			$miasto = $this->_request->getPost('miasto');
			$kier = $this->_request->getPost('kier', '');
			$tel = $this->_request->getPost('tel');
			$fax = $this->_request->getPost('fax');
			
            if (empty($firma))
			{
                $error['err'] = '<div class="k_blad">Pole nazwa jest wymagane</div>';
            }

            if (empty($error))
			{
                $editArray = array(
                    'firma' => $firma,
                    'mail' => $mail,
                    'woj' => $woj,
                    'kod' => $kod,
					'ulica' => $ulica,
					'miasto' => $miasto,
					'kier' => $kier,
					'tel' => $tel,
					'fax' => $fax
                );
                $edycja = new Dealerzy;

                $db = $edycja->edytuj($editArray, $id);

                $this->view->error = '<div class="k_ok">Edycja przebiegła pomyślnie </div>';
            }
			else
			{
                $this->view->error = $error['err'];
            }
        }		
        $print = $wybrana->pojedyncza($id);
        $this->view->pojedynczy = $print;
    }
}
?>