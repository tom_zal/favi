<?php
class Admin_PaczkaController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		
		$this->tryb = $this->_getParam('tryb', null);
		if(strpos($this->URI, '/tryb/') !== false) $this->tryb = '';
		if(strpos($this->URI, '/tryb/allegro') !== false) $this->tryb = 'allegro';
		//var_dump($this->tryb);die();
		$this->zadania = $this->_getParam('zadania','brak');
		$this->view->headScript()->appendFile($this->_request->getBaseUrl().'/public/scripts/functionadmin_zamow_lista.js', 'text/javascript');
		$this->view->headScript()->appendFile($this->_request->getBaseUrl(). '/public/scripts/SiodemkaServiceSoapHttpPort.js', 'text/javascript');
		$this->view->headScript()->appendFile($this->_request->getBaseUrl().'/public/scripts/jquery.xml.js', 'text/javascript');
		$this->view->errorSiodemka = '';
		
		if(!empty($this->zadania) && $this->zadania != 'brak' && strpos($this->zadania, 'paczki') !== false)
		{
			$this->view->allegro = $this->allegro = $this->zadania == 'paczki_allegro';
			$_SESSION['paczkaTryb'] = $this->allegro ? "allegro" : "";
		}
		else
		{
			if(!isset($_SESSION['paczkaTryb'])) $_SESSION['paczkaTryb'] = '';
			if($this->tryb !== null) $_SESSION['paczkaTryb'] = $this->tryb;
			$this->view->allegro = $this->allegro = $_SESSION['paczkaTryb'] == 'allegro';
		}
		$this->tryb = $_SESSION['paczkaTryb'];
		//var_dump($this->tryb);die();
		$this->view->table = $this->table = $this->allegro ? "Paczkaallegro" : "Paczka";
	}
	function __call($method, $args)
	{
		$this->_redirect($this->view->baseUrl.'/admin/index/panel/');
	}
	function utworzAction()
	{
		$paczka = new Paczka($this->table);
		$zamowienia = $this->allegro ? new AllegroZamowienia() : new Zamowienia();
		//var_dump($_POST);die();
		if($this->_request->isPost())
		{
			$checkbox = $this->_getParam('checkbox', '');
			switch($this->zadania)
			{
				case 'paczki':
				case 'paczki_allegro':
					if(!empty($checkbox))
					{
						$error = '';						
						foreach($checkbox as $id => $dostawca)
						{
							$paczka->id = $id;
							if(!$paczka->sprawdzZamowienie() && $dostawca != '')
							{
								$paczka->dodaj(array
								(
									'id_zam'=> $id,
									'data'=> date('Y-m-d H:i:s'),
									'dostawca'=> $dostawca
								));
							}
							else if($dostawca == '')
							{
								$error .= 'Zamówienie o nr '.$id.' nie posiada dostawy do API kurierów. Nie można dodać paczki.<br />';
							}
						}
						if(!empty($error)) $this->view->error = '<div class="k_blad">'.$error.'</div>'; 
					}
				break;    
				case 'usunlist':
				case 'usunlist_allegro':
					if(!empty($checkbox))
					{
						foreach($checkbox as $id => $dostawca)
						{
							$paczka->id = $id;
							$paczka->usun();
						}
					}
				break;    
			}
		}
		$this->view->wiersze = $wiersze = $paczka->wypisz();
		$this->view->zamowienia = Query::sortById($zamowienia->selectIds(Query::setIds($wiersze,'id_zam'), true));		
	}
	
	function dodajAction()
	{
		$paczka = new Paczka($this->table);
		$paczka->dane = $this->_getAllParams();
		$zamowienia = $this->allegro ? new AllegroZamowienia() : new Zamowienia();
		
		//$kurier = new Kurier();
		//$dane = $kurier->showData();
		//$config = $dane[0]->toArray();
		
		$dhl = new Dhl('admin', $this->table);
		$siodemka = new Siodemka('admin', $this->table);
		$pocztaPolska = new Pocztapolska('admin', $this->table);
		$paczkomaty = new Paczkomaty('admin', $this->table);
		
		$dhlDane = new Dhldane();
		$configs['dhl'] = $dhlDane->wypisz();
		$oPoczta = new Pocztapolskakonto();
		$pocztaPolskaDane = new Pocztapolskadane('admin');
		$configs['pocztapolska'] = $pocztaPolska->wypiszOne(1);
		foreach($pocztaPolska->typy as $typ => $typNazwa)
		$configs['pocztapolska'][$typ] = @unserialize($configs['pocztapolska'][$typ]);
		//var_dump($configs);die();
		
		//var_dump($_POST);die();
		$wiersze = $paczka->wypisz(1);
		//if(!$this->_request->isPost()) {var_dump($wiersze);die();}
		$kontr = Query::sortById($zamowienia->selectIds(Query::setIds($wiersze,'id_zam'), true));
		$this->view->wiersze = $paczka->wypiszPoPrzesylka($wiersze);
		$paczkomaty->getDefaultData($kontr);
		//var_dump($wiersze);die();
		
		$przesylkiDodane = array();
		$paczkaBlad = array();
		//var_dump($kontr);die();
		$defaultData = $paczka->setDefaultDane($kontr, @$config, $configs); //przygotowanie danych
		//var_dump($defaultData);die();
		$odbiorca = $defaultData['odbiorca'];
		$config = $defaultData['config'];
		$przesylki = $defaultData['przesylki'];
		$urzadWydaniaEPrzesylki = $defaultData['urzadWydaniaEPrzesylki'];
		
		$error = $errorDomyslne = '';
		if($this->_request->isPost())
		{
			$checkbox = $this->_getParam('checkbox', '');
			switch($this->zadania)
			{
				case 'wysylkowe':
				case 'wysylkowe_allegro':
					if(!empty($checkbox))
					{
						$symbol = md5(date('h-i-s, j-m-y').'abcdefg1248');
						$symbolListy = $paczka->getMaxSymbol();
						$fit = 0;
						$listaNr = $symbolListy['listaNr'];
						$paczka->edytujNumerPrzesylka(array('czyList' => 0), "");
						
						$listaDos = '';
						foreach($checkbox as $id => $dostawca)
						{
							if($fit == 0)
							{
								$listaDos[$dostawca] = $listaNr;
							}
							if(!isset($listaDos[$dostawca]))
							{
								$listaDos[$dostawca] = $listaNr++;
							}
							$paczka->id = $id;
							$paczka->edytuj(array
							(
								'aktualizacja'=> date('Y-m-d H:i:s'),
								'czyList'=> 1,
								'symbol'=> $symbol.''.$dostawca,
								'listaSymbol'=> $symbolListy['listaSymbol'],
								'listaNr'=> $listaDos[$dostawca]
							));
							$fit++;
						}
						$this->_redirect($this->baseUrl.'/admin/paczka/dodaj/tryb/'.$this->tryb);
					}
				break; 
				case 'paczki': //Dodawanie listow przewozowych do API kurierow
				case 'paczki_allegro':
					$config = $cfg = $this->_getParam('config');
					$odbiorca = $odb = $this->_getParam('odbiorca');  
					$ik = 0; //var_dump($cfg);die();
					foreach($odbiorca as $dostawca => $danePrzesylki)
					{
						if(!empty($danePrzesylki))
						{
							$paczka->dostawca = $dostawca;
							switch($dostawca)
							{
								case 'dhl':
									$dhl->paczka = $paczka;
									$dhl->setDefaultData($configs['dhl']);
									$dhl->waliduj($cfg, $danePrzesylki);									
									if($dhl->counterBlad == 0)
									{
										unset($cfg['domyslne']);
										foreach($cfg as $key=>$dane)
										{
											$response = $dhl->addToApi($key, array_merge($dhl->config, $dane), $danePrzesylki[$key]);
											if($paczka->checkErrors($response, $key)) { //Sprawdzenie bledow
												$paczka->editPrzesylka($response, array('odbiorca'=>$danePrzesylki[$key],'config'=>$cfg[$key]), $ik); // Zapisany numerow przesylek
											}
										}
									}
								break;
								
								case 'siodemka':
									$siodemka->paczka = $paczka;
									$siodemka->setDefaultData($cfg);
									$siodemka->waliduj($cfg, $danePrzesylki);
									if($siodemka->counterBlad == 0)
									{
										unset($cfg['domyslne']);
										foreach($cfg as $key=>$dane)
										{
											$response = $siodemka->addToApi($key, array_merge($siodemka->config, $dane), $danePrzesylki[$key]);
											if($paczka->checkErrors($response, $key)) { //Sprawdzenie bledow
												$paczka->editPrzesylka($response, array('odbiorca'=>$danePrzesylki[$key],'config'=>$cfg[$key]), $ik); // Zapisany numerow przesylek
											}
										}
									}
								break;
								
								case 'pocztapolska':
									$params = $this->_getAllParams();									
									$pocztaPolska->paczka = $paczka;									
									//var_dump($danePrzesylki);die();
									$buffor = $pocztaPolska->createBuffor(); //Stworzenie buffora dla przesylek
									//var_dump($buffor);die();
									if(isset($buffor['buffor']))
									{
										foreach($danePrzesylki as $idP => $odbiorcaP)
										{
											$config[$idP]['typ'] = $typ = @$params['typ'][$idP];
											$pocztaPolska->getDane($idP, $odbiorcaP, $params); //Przygotowanie danych dla przesylki
											$response = $pocztaPolska->addShipment($buffor['buffor']); //Dodanie do buffora przesylek
											//var_dump($response);die();
											$paczka->id = $idP;
											if($paczka->checkErrors($response, $idP))  //Sprawdzenie bledow
											{
												$response = array_merge($response, $buffor);
												if($typ == "nierejestrowana") $response['numerPrzesylka'] = $response['guid'];
												if($typ == "firmowanierejestrowana") $response['numerPrzesylka'] = $response['guid'];
												$paczka->editPrzesylka($response, array('odbiorca'=>$odbiorcaP,'config'=>NULL), $ik); // Zapisany numerow przesylek
											}
										}
										if(isset($paczka->przesylkiDodane[$dostawca]) && count($paczka->przesylkiDodane[$dostawca])>0)
										{
											$pocztaPolska->sendBuffor($buffor); //Wyslanie buffora dla przesylek
										}
									}
									else
									{
										foreach($danePrzesylki as $idP => $odbiorcaP)
										$config[$idP]['typ'] = $typ = @$params['typ'][$idP];
										$paczka->errors[$dostawca] = "Błąd tworzenia bufora Poczty Polskiej";
										//var_dump($paczka->errors);die();
									}
								break;
								
								case 'paczkomaty':
									$params = $this->_getAllParams();
									$paczkomaty->paczka = $paczka;
									$paczkomaty->nadawca = $params['nadawca'];
									if(isset($params['inpostodbiorca']['paczkomaty']))
									foreach($params['inpostodbiorca']['paczkomaty'] as $idP=>$dane)
									{
										$paczkomaty->idP = $idP;										
										$paczkomaty->zamowienie = @$kontr[$idP];
										$paczkomaty->setData($params);
										$response = $paczkomaty->addPackage();
										$paczka->id = $idP;
										if($paczka->checkErrors($response, $idP)) { //Sprawdzenie bledow
											$paczka->editPrzesylka($response, array('odbiorca'=>$dane,'config'=>NULL), $ik); // Zapisany numerow przesylek
										}
									}
								break;    
							}
						}
						$ik++;
					}
					if(count($paczka->przesylkiDodane) == $ik && count($paczka->error) == 0 && count($paczka->errors) == 0) 
						$this->_redirect($this->view->baseUrl.'/admin/paczka/lista/tryb/'.$this->tryb);
				break;    
			}
		}
		
		$this->view->przesylkiDodane = $paczka->przesylkiDodane;
		$this->view->zamowienia = $odbiorca;
		$this->view->config = $config;
		$this->view->errors = $paczka->errors;
		$this->view->error = $paczka->errorsToView();
		$this->view->komunikatok = $paczka->infoMsg();
		$this->view->allDane = $paczka->dane;
		$this->view->kontr = $kontr;
		$this->view->przesylki = $przesylki;
		if(count($przesylki) > 0)
		foreach($przesylki as $typ => $paczki)
		{
			$this->view->{$typ} = $this->_request->getPost($typ, $paczki);
			if($typ == "przesylkakurierska48" && $this->_request->getPost($typ))
			if(count($this->view->{$typ}) > 0)
			foreach($this->view->{$typ} as $krId => $paczka)
			if(@!empty($paczka['urzadWydaniaEPrzesylkiInput']))
			{
				$urzadWydaniaEPrzesylki[$krId] = $pocztaPolskaDane->urzadWydaniaEPrzesylkiSzukaj($paczka['urzadWydaniaEPrzesylkiInput']);
			}
			else
			{
				$this->view->{$typ}[$krId]['urzadWydaniaEPrzesylki'] = $paczki[$krId]['urzadWydaniaEPrzesylki'];
				$this->view->{$typ}[$krId]['urzadWydaniaEPrzesylkiInput'] = $paczki[$krId]['urzadWydaniaEPrzesylkiInput'];
			}
		}
		$this->view->urzadWydaniaEPrzesylki = $urzadWydaniaEPrzesylki;
		//var_dump($this->view->przesylkakurierska48);die();
	}
	
	function listaAction()
	{
		$paczka = new Paczka($this->table);
		$dhl = new Dhl('admin', $this->table);		
		$siodemka = new Siodemka('admin', $this->table);
		$pocztaPolska = new Pocztapolska('admin', $this->table);
		$paczkomaty = new Paczkomaty('admin', $this->table);
		
		$this->view->paczki = $paczki = $paczka->getPaczkiSymbol();
		$this->view->wiersze = $paczka->getSymbol();

		$redirect = $this->view->baseUrl.'/admin/paczka/lista/tryb/'.$this->tryb;
		$error = $dane = $danezbiorczo = '';
		//var_dump($redirect);die();
		
		if($this->_request->isPost() || !empty($this->zadania))
		{
			$checkbox = $this->_getParam('checkbox', '');
			$symbol = trim($this->_getParam('symbol', ''));
			switch($this->zadania)
			{
				case 'usunwysylka':
					if(!empty($checkbox))
					{
						foreach($checkbox as $symbol => $data)
						{
							$paczka->id = trim($symbol);
							$paczka->usun(1);
						}
						$this->_redirect($redirect);
					}
				break;
				
				case 'dokumenty':
					$dostawa = $this->_getParam('dostawa','siodemka');
					if(isset($paczki[$symbol]))
					{
						$globalList = $globalEtykieta = array();
						$paczki = $paczki[$symbol];
						
						switch($dostawa)
						{
							case 'dhl':
								$nrPrzesylek = $paczka->getNumeryPrzesylek($paczki);
								$dhl->ar2 = $paczka->ar2; //tablica z id zamowien
								//var_dump($nrPrzesylek);die();
								
								//list($errorObsluga, $globalList) = $dhl->getDokument($nrPrzesylek, 'getLabels', 'LP', 'list');
								//if(@$_GET['test']) { var_dump($errorObsluga); var_dump($globalList); }
								//$danezbiorczo['wydruk_list_global'] = $dhl->joinFilePdf($globalList , 'wydruk_list_'.$symbol.'.pdf');

								list($errorObsluga, $globalList) = $dhl->getDokument($nrPrzesylek, 'getLabels', 'BLP', 'etykieta');
								//if(@$_GET['test']) { var_dump($errorObsluga2); var_dump($globalList); }
								$danezbiorczo['wydruk_etykieta_global'] = $dhl->joinFilePdf($globalList , 'wydruk_etykieta_'.$symbol.'.pdf');

								//$errorObsluga.= $errorObsluga2;
								if(strlen($errorObsluga) > 0)
								{
									$this->view->error = '<div class="k_blad">Bład pobierania danych z API DHL.</div>';
									$this->view->error .= '<div class="lista_bledow">'.$errorObsluga.'</div>';
								}
								if(!empty($danezbiorczo['wydruk_etykieta_global'])) //$danezbiorczo['wydruk_list_global']
								{
									$this->view->error .= '<div class="k_ok">Pobierz pliki do wydruku.</div>';
									$this->view->error .= '<div class="lista_bledow">									
									<a target="_blank" href="/public/admin/pdf/'.$danezbiorczo['wydruk_etykieta_global'].'">Pobierz Etykiety adresowe</a><br/>   
									</div>';
									//<a target="_blank" href="/public/admin/pdf/'.$danezbiorczo['wydruk_list_global'].'">Pobierz Listy przewozowe</a><br />
								}
								$paczka->symbol = $symbol;
								$paczka->edytujPoSymbol($danezbiorczo);
							break;
								
							case 'siodemka':
								$nrPrzesylek = $paczka->getNumeryPrzesylek($paczki);
								$siodemka->ar2 = $paczka->ar2; //tablica z id zamowien
								list($errorObsluga, $globalList) = $siodemka->getDokument($nrPrzesylek, 'wydrukListPdfElement', 'numer', 'list');
								$danezbiorczo['wydruk_list_global'] = $siodemka->joinFilePdf($globalList , 'wydruk_list_'.$symbol.'.pdf');

								list($errorObsluga2, $globalList) = $siodemka->getDokument($nrPrzesylek, 'wydrukEtykietaPdfElement','numery', 'etykieta');
								$danezbiorczo['wydruk_etykieta_global'] = $siodemka->joinFilePdf($globalList , 'wydruk_etykieta_'.$symbol.'.pdf');

								$errorObsluga.=$errorObsluga2;
								if(strlen($errorObsluga) > 0)
								{
									$this->view->error = '<div class="k_blad">Błąd pobierania danych z API Siódemka.</div>';
									$this->view->error .= '<div class="lista_bledow">'.$errorObsluga.'</div>';
								}
								if(!empty($danezbiorczo['wydruk_list_global']) || $danezbiorczo['wydruk_etykieta_global'])
								{
									$this->view->error .= '<div class="k_ok">Pobierz pliki do wydruku.</div>';
									$this->view->error .= '<div class="lista_bledow">
										<a target="_blank" href="/public/admin/pdf/'.$danezbiorczo['wydruk_list_global'].'">Pobierz Listy przewozowe</a><br />
										<a target="_blank" href="/public/admin/pdf/'.$danezbiorczo['wydruk_etykieta_global'].'">Pobierz Etykiety adresowe</a><br />   
									</div>';
								}
								$paczka->symbol = $symbol;
								$paczka->edytujPoSymbol($danezbiorczo);
							break;
							
							case 'pocztapolska':								
								$nrGuid = $paczka->getGuid($paczki);
								$pocztaPolska->arBuffor = $paczka->arBuffor;
								$pocztaPolska->arEnvelope = $paczka->arEnvelope;
								$pocztaPolska->ar2 = $paczka->ar2;
								$pocztaPolska->getDokument($nrGuid);
								
								$danezbiorczo['wydruk_etykieta_global'] = $siodemka->joinFilePdf($pocztaPolska->globalList , 'wydruk_etykieta_'.substr($symbol, 0, 10).'.pdf');
								
								if(!empty($danezbiorczo['wydruk_list_global']) || $danezbiorczo['wydruk_etykieta_global'])
								{
									$this->view->error .= '<div class="k_ok">Pobierz pliki do wydruku.</div>';
									$this->view->error .= '<div class="lista_bledow"><a target="_blank" href="/public/admin/pdf/'.$danezbiorczo['wydruk_etykieta_global'].'">Pobierz Etykiety adresowe</a><br /></div>';
								}
								$paczka->symbol = $symbol;
								$paczka->edytujPoSymbol($danezbiorczo);
							break;
							
							case 'paczkomaty':
								$packcode = $paczkomaty->sortCode($paczki);
								$paczkomaty->symbol = $symbol;
								$response = $paczkomaty->getStickers($packcode);
								$response2 = $paczkomaty->getPrintout($packcode);
								if(!empty($response2['wydruk_list_global']) || !empty($response['wydruk_etykieta_global']))
								{
									$this->view->error .= '<div class="k_ok">Pobierz pliki do wydruku';//.</div><div class="lista_bledow">';
									if(!empty($response2['wydruk_etykieta_global'])) $this->view->error .= '<a target="_blank" href="/public/admin/pdf/'.$response['wydruk_etykieta_global'].'">Pobierz Etykiety adresowe</a><br />';
									if(!empty($response2['wydruk_list_global'])) $this->view->error .= '<a target="_blank" href="/public/admin/pdf/'.$response2['wydruk_list_global'].'">Pobierz Potwierdzenie nadania</a><br />';								
									$this->view->error .= '</div>';
								}
								if(!empty($response2['error']) || !empty($response['error']))
								{
									$this->view->error .= '<div class="k_blad">';
									if(!empty($response['error']))
									$this->view->error .= 'Nie udało się wygenerować etykiety adresowej.<br />'.$response['error'].'<br />';
									if(!empty($response2['error']))
									$this->view->error .= 'Nie udało się wydrukować potwierdzenia nadania.<br />'.$response2['error'].'<br />';
									$this->view->error .= '</div>';
								}
								$danezbiorczo = array_merge($response, $response2);
								if(isset($danezbiorczo['error'])) unset($danezbiorczo['error']);
								$paczka->symbol = $symbol;
								//var_dump($packcode);var_dump($response);var_dump($response2);die();
								if(count($danezbiorczo) > 0)
								$paczka->edytujPoSymbol($danezbiorczo);
							break;    
							
							//var_dump($redirect);die();
							$this->getResponse()->setHeader('Refresh', '5; URL='.$redirect);
						}
					}
					else $this->_redirect($redirect);
				break;
				
				case 'kurier':
					$dostawa = $this->_getParam('dostawa','siodemka');
					if(isset($paczki[$symbol]))
					{
						$globalList = array();
						$paczki = $paczki[$symbol];
						
						switch($dostawa)
						{
							case 'dhl':
								$kurier = $this->_getParam('kurier');
								$nrPrzesylek = $paczka->getNumeryPrzesylek($paczki);
								$dhl->ar2 = $paczka->ar2; //tablica z id zamowien
								//var_dump($kurier);die();
								list($errorObsluga, $globalList) = $dhl->getKurier($nrPrzesylek, $kurier);
								if(!empty($globalList)) $danezbiorczo['kurier'] = $globalList;
								//var_dump(($danezbiorczo));die();
								if(strlen($errorObsluga) > 0)
								{
									$this->view->error = '<div class="k_blad">Błąd zamawiania kuriera z API DHL.</div>';
									$this->view->error .= '<div class="lista_bledow">'.$errorObsluga.'</div>';
								}
								if(!empty($danezbiorczo['kurier']))
								{
									$this->view->error .= '<div class="k_ok">Zamówiono kuriera.</div>';
									$this->view->error .= '<div class="lista_bledow">Numer zlecenia: '.$danezbiorczo['kurier'].'</div>';
								}
								if(isset($danezbiorczo['error'])) unset($danezbiorczo['error']);
								if(!empty($danezbiorczo))//930515WWW
								{
									$paczka->symbol = $symbol;
									$paczka->edytujPoSymbol($danezbiorczo);
								}
								$this->view->kurierDHL = $kurier;
							break;
								
							case 'siodemka':
							break;
							
							case 'pocztapolska':
							break;
							
							case 'paczkomaty':
							break;    
							
							//var_dump($redirect);die();
							$this->getResponse()->setHeader('Refresh', '5; URL='.$redirect);
						}
					}
					else $this->_redirect($redirect);
				break;
				
				case 'kurier_odwolaj':
					$dostawa = $this->_getParam('dostawa','siodemka');
					if(isset($paczki[$symbol]))
					{
						$globalList = array();
						$paczki = $paczki[$symbol];
						
						switch($dostawa)
						{
							case 'dhl':
								$kurier = $this->_getParam('kurier');
								$nrPrzesylek = $paczka->getNumeryPrzesylek($paczki);
								$dhl->ar2 = $paczka->ar2; //tablica z id zamowien
								//var_dump($kurier);die();
								list($errorObsluga, $globalList) = $dhl->delKurier($nrPrzesylek, array('orderId' => $kurier));
								if(empty($errorObsluga)) $danezbiorczo['kurier'] = $globalList;
								//var_dump(($danezbiorczo));die();
								if(strlen($errorObsluga) > 0)
								{
									$this->view->error = '<div class="k_blad">Błąd odwołania kuriera '.$kurier.' z API DHL.</div>';
									$this->view->error .= '<div class="lista_bledow">'.$errorObsluga.'</div>';
								}
								if(empty($errorObsluga))
								{
									$this->view->error .= '<div class="k_ok">Odwołano kuriera.</div>';
									$this->view->error .= '<div class="lista_bledow">Numer zlecenia: '.$danezbiorczo['kurier'].'</div>';
								}
								if(isset($danezbiorczo['error'])) unset($danezbiorczo['error']);
								if(!empty($danezbiorczo))
								{
									$danezbiorczo['kurier'] = "";
									$paczka->symbol = $symbol;
									$paczka->edytujPoSymbol($danezbiorczo);
								}
							break;
								
							case 'siodemka':
							break;
							
							case 'pocztapolska':
							break;
							
							case 'paczkomaty':
							break;    
							
							//var_dump($redirect);die();
							$this->getResponse()->setHeader('Refresh', '5; URL='.$redirect);
						}
					}
					else $this->_redirect($redirect);
				break;
			}
			
			$this->view->paczki = $paczki = $paczka->getPaczkiSymbol();
			$this->view->wiersze = $paczka->getSymbol();
		}
	}        
}
?>