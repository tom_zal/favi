<?php
class Admin_LojalnoscController extends Ogolny_Controller_Admin
{
    public function init()
    {
		parent::init();
		$this->view->baseUrl = $this->baseUrl = $this->_request->getBaseUrl();
		$this->view->action = $this->_getParam('action');
		$this->view->www = $_SERVER['HTTP_HOST'];
		$this->root = $_SERVER['DOCUMENT_ROOT'];
		$this->view->headLink()->appendStylesheet($this->view->baseUrl.'/public/styles/facebox.css');
		$this->view->headScript()->appendFile($this->_request->getBaseUrl() . '/public/scripts/facebox/facebox.js', 'text/javascript');
		
		$this->rowscount = 30;
		$this->view->cfg = $this->cfg = new Zend_Config_Ini('../application/config.ini', 'lojalnosc');
		$this->view->infosms = 'Bez znaków specjalnych<br/>1 sms = 160zn.<br/>2 sms = 306zn.<br/> 3 sms = 459zn.<br/> <br/>Ze znakami specjalnymi(w tym PL)<br />1 sms = 70zn.<br />2 sms = 134zn.<br /> 3 sms = 201zn.<br />';
		$this->timedop = '23:59:59';
		
		$this->table = 'Kontrahenci';
		$this->subtable = 'Zamowieniakontrahenci';
		$this->rowscount = 30;
    }
    function __call($method, $args) 
    {
        $this->_redirect('/admin');
    }
	
	public function indexAction()
	{
		$this->view->headLink()->appendStylesheet($this->view->baseUrl.'/public/styles/facebox.css');
		$this->view->headScript()->appendFile($this->_request->getBaseUrl() . '/public/scripts/facebox/facebox.js', 'text/javascript');
		$this->view->headScript()->appendFile($this->_request->getBaseUrl().'/public/scripts/jquery.uploadify/jquery.uploadify.v2.1.4.js', 'text/javascript');
		$this->view->headLink()->appendStylesheet($this->_request->getBaseUrl().'/public/styles/calendar.css');
		
		$ustawienia = $this->_request->getPost('ustawienia');
		$przedluz = $this->_request->getPost('przedluz');

		$settings = new Lojalnoscustawienia();
		$ust = $settings->showData()->toArray();
		$this->view->dane = $ust[0];
		$this->view->blokuj =  isset($przedluz)? 0 : $this->view->dane['zapisano']; 
		$this->view->przedluz = isset($przedluz)? 1 : 0;
		
		$this->data = $ust[0]['koniec'];
		
		if($this->_request->isPost() && $ustawienia)
		{
			$dane = $this->_request->getPost('post');
			if(!$this->walidacjaProgram($dane))
			{
				$dane['zapisano'] = 1;
				$dane['zapisanoczas'] = date('Y-m-d H:i:s');
				$dane['status'] = 0;
				unset($dane['przedluz']);
				$lojalnosc = new Lojalnoscustawienia();
				$lojalnosc->updateData($dane);
				$this->view->error = '<div class="k_ok">Ustawienia zostały zapisane poprawnie.</div>';
				$this->getResponse()->setHeader('Refresh', '2; URL='.$this->view->baseUrl.'/admin/lojalnosc/index');
			}
			else
			{
				$this->view->blokuj = 0;
				$this->view->przedluz = $dane['przedluz'];
				$this->view->dane = $dane;
				if($dane['przedluz'])
				{
					$this->view->dane['start'] = $ust[0]['start'];
				}
				$this->view->dane['zapisano'] = $ust[0]['zapisano'];
				
				$this->view->error = '<div class="k_blad">Wypełnij poprawnie formularz.</div>';
				$this->view->error .= '<div>'.$this->_er.'</div>'; 
			}
		}

		$ob = new Query('Upominki');
		$ob->value = 1;
		$ob->order = 'nazwa';
		$ob->column = 'polecany';
		$this->view->rowsp = $ob->getRowsValue();
		$ob->column = 'polecajacy';
		$this->view->rowspa = $ob->getRowsValue();
	}
        
	public function walidacjaProgram($dane)
	{
		$counter = 0;
		$this->_er ='';
		if($dane['przedluz'] == 0) 
		{     
			if(empty($dane['koniec']) || empty($dane['start'])) 
			{
				$this->_er .= ++$counter.'. Start i koniec programu jest wymagany.<br />';
			}
			if(!empty($dane['koniec']) && strtotime($dane['koniec']) < strtotime($dane['start']))
			{
				$this->_er .= ++$counter.'. Data końca programu powinna mieć późniejszy termin niż data staru.<br />';
			}
		}
		elseif($dane['przedluz'] == 1)
		{
			if(empty($dane['koniec'])) 
			{
				$this->_er .= ++$counter.'. Koniec programu jest wymagany.<br />';
			}
			if(!empty($dane['koniec']) && strtotime($dane['koniec']) <= strtotime($this->data))
			{
				$this->_er .= ++$counter.'. Data końca programu po przedłużeniu powinna mieć późniejszy termin niż data wcześniejsza.<br />';
			}
		}
		return $this->_er;
	}
        
	public function drzewoAction()
	{
		ini_set("memory_limit", "512M");
		
		$szukanie = new Zend_Session_Namespace('szukaniePolecajacyAdmin');
		if($this->_request->getParam('reset'))
		{
			$szukanie->klient = 0;
			$szukanie->ile = 10;
			$szukanie->od = 0;
        }
		if($this->_request->getParam('klient') != '')
		{
			$szukanie->klient = @intval($this->_request->getParam('klient'));
		}
		if($this->_request->getParam('ile'))
		{
			$szukanie->ile = @intval($this->_request->getParam('ile'));
		}
		if($this->_request->getParam('od') != '')
		{
			$szukanie->od = @intval($this->_request->getParam('od'));
		}
		if(!isset($szukanie->klient)) $szukanie->klient = 0;
		if(!isset($szukanie->ile)) $szukanie->ile = 10;
		if(!isset($szukanie->od)) $szukanie->od = 0;
		
		$polecenia = new Lojalnoscpolecenia();
		$polecenia->klient = $szukanie->klient;
		$polecenia->getAllWithDetails($szukanie->klient);

		//var_dump($polecenia->polecenia);echo '<br/><br/>';
		//var_dump($polecenia->dzieci[0]);echo '<br/><br/>';
		//var_dump($polecenia->klienci);echo '<br/><br/>';
		//var_dump($polecenia->archiwum);echo '<br/><br/>';
		$polecenia->getWartosciZamowien(0, null);
                $polecenia->display_children_admin(0, 0);
                $this->view->drzewo = $polecenia->drzewo;

		//$this->view->klienci = $polecenia->klienci;
		$kontrahent = new Kontrahenci();
		$this->view->klienci = $kontrahent->getKontrahenci('id');
		$kontrahent->id = $szukanie->klient;
		$this->view->klient = $kontrahent->wypiszPojedynczy();
		
		$ilosc = $polecenia->wypiszIluPolecajacych();
		$this->view->polecajacy = $polecenia->wypiszPolecajacych($szukanie);
		
		$this->view->ilosc = ceil($ilosc / $szukanie->ile);
		$this->view->strony = ceil($ilosc / $szukanie->ile);
		$this->view->strona = $szukanie->od;
		$this->view->link = 'admin/lojalnosc/drzewo';
		$this->view->szukanie = $szukanie;
		$this->view->od = $szukanie->od;
		$this->view->ile = $szukanie->ile;
		//$this->view->sort = $sortowanie->sort;
		//$this->view->order = $sortowanie->order == 'asc' ? 'desc' : 'asc';		
	}
	
	function listaAction()
	{
        $kontrahent = new Kontrahenci();

		$delid = (int)$this->_request->getParam('delid', 0);
        if($delid > 0)
		{
            $kontrahent->id = $delid;
            $kontrahent->usun();
        }
		
		$szukanie = new Zend_Session_Namespace('szukanieKontrLojalAdmin');
		if($this->_request->getParam('reset')) 
		{
			$szukanie->nazwa = '';
			$szukanie->grupa = 'all';
			$szukanie->plec = 'all';
			$szukanie->miasto = 'all';
			$szukanie->wojew = 'all';
			$szukanie->dataOd = '1900-01-01';
			$szukanie->dataDo = date('Y-m-d');
			$szukanie->data_ur = '';
			$szukanie->zalogowano = '';
			$szukanie->data_rej = '';
			$szukanie->od = 0;
			$szukanie->ile = 20;
			$szukanie->karta = '';
        }
        $sortuj = '';
        if($this->_request->isPost())
		{
			$szukanie->nazwa = $this->_request->getPost('nazwa');
			if($szukanie->nazwa == 'imię, nazwisko, email') $szukanie->nazwa = '';
			$szukanie->grupa = $this->_request->getPost('grupa');
			$szukanie->plec = $this->_request->getPost('plec');
			$szukanie->miasto = $this->_request->getPost('miasto');
			$szukanie->wojew = $this->_request->getPost('wojew');
			$od = $this->_request->getPost('dataOd');
			$szukanie->dataOd = $od['rok'].'-'.$od['mc'].'-'.$od['day'];
			$do = $this->_request->getPost('dataDo');
			$szukanie->dataDo = $do['rok'].'-'.$do['mc'].'-'.$do['day'];
			$szukanie->data_ur = $this->_request->getPost('data_ur', '');
			$szukanie->zalogowano = $this->_request->getPost('zalogowano', '');
			$szukanie->data_rej = $this->_request->getPost('data_rej', '');
			$szukanie->od = 0;
			$szukanie->ile = 20;
			$szukanie->karta = $this->_request->getPost('karta');
        }
		if ($this->_request->getParam('ile'))
		{
			$szukanie->ile = $this->_request->getParam('ile');
            $szukanie->ile = $szukanie->ile == 'all' ? 99999 : $szukanie->ile;
		}
        if ($this->_request->getParam('sortuj'))
		{
			$szukanie->sortuj = $this->_request->getParam('sortuj');
                        $sortuj = $szukanie->sortuj;
		}
		if ($this->_request->getParam('od') != '')
		{
			$szukanie->od = $this->_request->getParam('od');
		}
		if (!isset($szukanie->nazwa)) $szukanie->nazwa = '';
		if (!isset($szukanie->grupa)) $szukanie->grupa = 'all';
		if (!isset($szukanie->plec)) $szukanie->plec = 'all';
		if (!isset($szukanie->miasto)) $szukanie->miasto = 'all';
		if (!isset($szukanie->wojew)) $szukanie->wojew = 'all';
		if (!isset($szukanie->dataOd)) $szukanie->dataOd = '1900-01-01';
		if (!isset($szukanie->dataDo)) $szukanie->dataDo = date('Y-m-d');
		if (!isset($szukanie->data_ur)) $szukanie->data_ur = '';
		if (!isset($szukanie->zalogowano)) $szukanie->zalogowano = '';
		if (!isset($szukanie->data_rej)) $szukanie->data_rej = '';
		if (!isset($szukanie->ile)) $szukanie->ile = 40;
		if (!isset($szukanie->od)) $szukanie->od = 0;
		if (!isset($szukanie->karta)) $szukanie->karta = '';
		//$szukanie->ile = 1;

		$ile = $szukanie->ile;
		$od = floor($szukanie->od / $ile);
		$od = $szukanie->od;

		$sortowanie = new Zend_Session_Namespace('sortowanieKontrLojalAdmin');
		if($this->_request->getParam('reset')) 
		{
			$sortowanie->sort = 'data_rej';
			$sortowanie->order = 'desc';
			$this->_redirect('admin/lojalnosc/lista');
		}
		if($this->_request->getParam('sort'))
		{
			$sortowanie->sort = $this->_request->getParam('sort', 'data_rej');
			$sortowanie->order = $this->_request->getParam('order', 'desc');
		}
		if(!isset($sortowanie->sort))
		{
			$sortowanie->sort = 'data_rej';
			$sortowanie->order = 'desc';
		}

		if($this->_request->isPost()) $this->_redirect('admin/lojalnosc/lista');

		$ilosc = $kontrahent->wypiszKontr(true, $od, $ile, $sortowanie, $szukanie, true);
		$all = $kontrahent->wypiszKontr(false, $od, $ile, $sortowanie, $szukanie, true);
		//var_dump($all);die();

		$zamowienia = new Kontrahenci();
		$aktualne = $zamowienia->getSumClient('email', false);
		$archiwum = new Archiwum();
		$archiwum = $archiwum->getSumyKlientow('email', true);
		//var_dump($archiwum);die();
		if(count($all) > 0)
		foreach($all as $i => $klient)
		{
			$all[$i]['aktualne'] = @floatval($aktualne[$klient['email']]);
			$all[$i]['archiwum'] = @floatval($archiwum[$klient['email']]);
		}

		$this->view->miasta = $kontrahent->wypiszMiasta();
		$kontrahenciGrupy = new Kontrahencigrupy();
		$this->view->grupy = $kontrahenciGrupy->wypisz();
		$this->view->ilosc = ceil($ilosc / $ile);
		$this->view->kontrahenci = $all;
		$this->view->szukanie = $szukanie;
		$this->view->sortowanie = $sortowanie;
        $this->view->sortuj = isset($szukanie->sortuj) ? $szukanie->sortuj : 'za';
		$this->view->od = $od;
		$this->view->strona = $szukanie->od;
		$this->view->ile = $ile;
		$this->view->strony = ceil($ilosc / $ile);
		$this->view->link = 'admin/lojalnosc/lista';
		$this->view->order = $sortowanie->order == 'asc' ? 'desc' : 'asc';
    }
	
	function dodajAction()
	{
		$id = $this->_request->getParam('id', 0);
		$rabaty = new Lojalnoscrabaty();
		$rabaty->id = $id;
		$this->view->pojedynczy = $rabaty->wypiszID();
		
		$del = $this->_request->getParam('delid', 0);
		if($del!=0)
		{
			$rabaty->id = $del;
			$rabaty->usun();
			//$this->view->blad_edycji = '<div class="k_ok">Wybrany rozmiar zosta� usuni�ty.</div>';
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/lojalnosc/dodaj');
		}
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa', '');
			$od = $this->_request->getPost('od', 0);
			$do = $this->_request->getPost('do', 0);
			$wartosc = $this->_request->getPost('wartosc', 0);
			$dane = array('nazwa' => $nazwa, 'od' => $od, 'do' => $do, 'wartosc' => $wartosc);
			$rabaty->dodaj($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebieg�a pomy�lnie.</div>';
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/lojalnosc/dodaj');
		}
		
		$this->view->rabaty = $rabaty->wypisz();
	}
        
    function edytujAction() {
            $id = $this->_request->getParam('id', 0);
            $rabaty = new Lojalnoscrabaty();
            $rabaty->id = $id;
            $this->view->pojedynczy = $rabaty->wypiszID();

            if($this->_request->isPost())
            {
                    $id = $this->_request->getPost('id', 0);
                    $nazwa = $this->_request->getPost('nazwa', '');
                    $od = $this->_request->getPost('od', 0);
                    $do = $this->_request->getPost('do', 0);
                    $wartosc = $this->_request->getPost('wartosc', 0);
                    $dane = array('nazwa' => $nazwa, 'od' => $od, 'do' => $do, 'wartosc' => $wartosc);
                    $rabaty->id = $id;
                    $rabaty->edytuj($dane);
                    //$this->view->blad_edycji = '<div class="k_ok">Edycja przebieg�a pomy�lnie.</div>';
                    $this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/lojalnosc/dodaj');
            }

            $this->view->rabaty = $rabaty->wypisz();
    }
        
    function dodajzakupAction()
	{
        
    }
    
    function dodajklientAction()
	{
        $this->view->headLink()->appendStylesheet($this->_request->getBaseUrl().'/public/styles/calendar.css');

        if($this->_request->isPost()) 
        {
            $post = $this->_getParam('post');
            $walid = $this->walidacjaKlient($post);

            if(empty($walid)) 
            {
                unset($post['save']);
                $ob = new Query('Kontrahenci');
                $post['kartaaktywna'] = 1;
                $post['weryfikacja'] = 1;
                $post['kod_weryfikacja'] = null;

                if(isset($this->polecajacy)) $post['polecajacy'] = $this->polecajacy;

                if(isset($this->newkarta) && !empty($this->newkarta)) 
                {
                    $ob->id = intval($this->newkarta);
                    $ob->_update($post);
                    $this->view->error = '<div class="k_ok">Dodano nowego klienta.</div>';
                } 
                else 
                {
                    $this->view->error = '<div class="k_ok">Błędny numer karty.</div>';
                }    
                $this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/lojalnosc/dodajklient/');

            } 
            else  
            {
                $this->view->dane = $post;
                $this->view->error = '<div class="k_blad">Wypełnij poprawnie formularz.</div>';
                $this->view->error .= '<div>'.$this->_er.'</div>';
            }
        }
    }
    
    protected function walidacjaKlient($dane)
	{
        $counter = 0;
        $this->_er ='';
        
        if(empty($dane['karta'])) 
        {
            $this->_er .= ++$counter.'. Pole karta jest wymagane.<br />';
        }
		elseif(!empty($dane['karta'])) 
        {
            $ob = new Query('Kontrahenci');
            $ob->order = 'id';
            $ob->value = $dane['karta'];
            $ob->column = 'karta';
            $row = $ob->getRowsValue();
            if(empty($row) || !empty($row) && isset($row[0]['kartaaktywna']) && $row[0]['kartaaktywna'] == 1)
			{
                $this->_er .= ++$counter.'. Podaj prawidłowy numer karty.<br />';
            }
            $this->newkarta = isset($row[0]['id']) ? $row[0]['id'] : ''; 
        }
        
        if(!empty($dane['polecajacy'])) 
        {
            $ob = new Query('Kontrahenci');
            $ob->order = 'id';
            $ob->value = $dane['polecajacy'];
            $ob->column = 'karta';
            $row = $ob->getRowsValue();
            if(empty($row) || !empty($row) && isset($row[0]['kartaaktywna']) && $row[0]['kartaaktywna'] == 0)
			{
                $this->_er .= ++$counter.'. Podaj prawidłowy numer karty osoby polecającej.<br />';
            }
            $this->polecajacy = isset($row[0]['id']) ? $row[0]['id'] : ''; 
        }
        
        if(!empty($dane['email']))
		{
            if(!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $dane['email']))
			{
                $this->_er .= ++$counter.'. Podałeś nieprawidłowy e-mail.<br />';
            }
        }
        return $this->_er;
    }
    
    function dokumentyAction()
	{        
        $ob->karta = null;
        
        $obi = new Query('Kontrahenci');
        $obi->column = 'kartaaktywna';
        $obi->order = 'id';  
        $obi->value = 1;
        $row1 = $obi->getRowsValue();
        $this->view->iloscaktywna = count($row1);
        $obi->value = 0;
        $row1 = $obi->getRowsValue();
        $this->view->iloscnieaktywna = count($row1);
        $row1 = $obi->getRows();
        $this->view->iloscwszystkich = count($row1);
        
        if($this->_request->isPost())
        {
            $ob = new Kontrahenci();
            $post = $this->_getParam('post');
            if(!empty($post['ilosc']))
            {
                try
                {
                    $ilosckart = intval($post['ilosc']);
                    if($ilosckart > 0) 
                    {
                        $page = ceil($ilosckart / $this->cfg->rowpagekarta);
                        $pagehaslo = ceil($ilosckart / $this->cfg->rowpagekartahaslo);
                        $pagereje = ceil($ilosckart / $this->cfg->rowpagereje);
                        $pagepol = ceil($ilosckart / $this->cfg->rowpagepol);
                        
                        $k = $k2 = $k3 = $k4 = 1;
                        $arg = $arg2 = $arg3 = $arg4 = 1;
                        $kartystrona = $kartystronapol = $kartystronareje = array();

                        for($i=0; $i<$ilosckart; $i++)
						{
                            $ob->numeruj = $post['numeruj'];
                            $newkarta = $ob->getNowyNumerKartyMulti();
                            
                            $kartystronapol[$arg3][$newkarta] = $kartystronareje[$arg2][$newkarta] = $kartystrona[$arg][$newkarta] = $newkarta;
                            $haslo = Query::getRandNumber(2000,5000,4);
                            $kartyar[$newkarta] = $haslo;
                            $kartystronahaslo[$arg4][$newkarta] = $haslo;

                            if($k == $this->cfg->rowpagekarta) {$arg++; $k = 1;} else $k++;
                            if($k2 == $this->cfg->rowpagereje) {$arg2++; $k2 = 1;} else $k2++;
                            if($k3 == $this->cfg->rowpagepol) {$arg3++; $k3 = 1;} else $k3++;
                            if($k4 == $this->cfg->rowpagekartahaslo) {$arg4++; $k4 = 1;} else $k4++;
                        }
                        if(!empty($kartystrona))
						{
                            $timeq = date('Y-m-d H:i:s');
                            $time = str_replace(' ','_',str_replace(':', '_', $timeq));
                            
                            $namefile = array
							(
                                'karta_'.$time.'.pdf',
                                'formularz_'.$time.'.pdf',
                                'polecam_'.$time.'.pdf',
                                'hasla_'.$time.'.pdf'
                            );
                            $genkarta = Query::doPdf($kartystrona, $this->cfg->pozx, $this->cfg->pozy, $this->cfg->pozxjump, $this->cfg->pozyjump, $page, $this->cfg->rowelementkarta, $this->root.''.$this->cfg->pathpdf, $namefile[0], $this->root.''.$this->cfg->pathimg.''.$this->cfg->kartaplik, $this->cfg->widthpdf, $this->cfg->heightpdf);
                            $genkartahaslo = Query::doPdf($kartystronahaslo, $this->cfg->pozx, $this->cfg->pozy, $this->cfg->pozxjump, $this->cfg->pozyjump, $pagehaslo, $this->cfg->rowelementkartahaslo, $this->root.''.$this->cfg->pathpdf, $namefile[3], $this->root.''.$this->cfg->pathimg.''.$this->cfg->hasloplik, $this->cfg->widthpdf, $this->cfg->heightpdf, $this->cfg->twolinejump);
                            $genform = Query::doPdf($kartystronareje, $this->cfg->pozxre, $this->cfg->pozyre, $this->cfg->pozxjumpre, $this->cfg->pozyjumpre, $pagereje, $this->cfg->rowelementreje, $this->root.''.$this->cfg->pathpdf, $namefile[1], $this->root.''.$this->cfg->pathimg.''.$this->cfg->formularzplik, $this->cfg->widthpdf, $this->cfg->heightpdf);
                            $genpolec = Query::doPdf($kartystronapol, $this->cfg->pozxpol, $this->cfg->pozypol, $this->cfg->pozxjumppol, $this->cfg->pozyjumppol, $pagepol, $this->cfg->rowelementpol, $this->root.''.$this->cfg->pathpdf, $namefile[2], $this->root.''.$this->cfg->pathimg.''.$this->cfg->polecamplik, $this->cfg->widthpdf, $this->cfg->heightpdf);
                            
                            if($genkarta == null && $genform == null && $genpolec == null && $genkartahaslo == null)
                            {
                                $ob2 = new Query('Kontrahenci');
                                foreach($kartyar as $id => $value)
                                {
                                    try
                                    {
                                        $ob2->_save(array('karta'=> $id, 'kartaaktywna'=> 0, 'haslo'=>md5($value)));
                                    }
                                    catch(Exception $e)
                                    {
                                        echo 'Wystąpił problem! Wygenerowano identyczny numer karty.';
                                        continue;
                                    }
                                    
                                }
                                
                                $ob3 = new Query('Lojalnoscpliki');
                                foreach($namefile as $file) $ob3->_save (array('nazwa'=>$file, 'data'=>$timeq)); 
                                
                                $this->view->error = '<div class="k_ok">Wygenerowano '.$ilosckart.' nowych kart. Pobierz pliki do wydruku w zakładce utworzone pliki.</div>';
                                $this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/lojalnosc/dokumenty/');
                            }                            
                        }
                    }                    
                }
                catch(Exception $e)
                {
                    $this->view->error =  '<div class="k_blad">Wystąpił problem! Nie udało się wygenerować nowych kart. Spróbuj ponownie.';
                }
            }
        }
    }
    
    function kartasortAction() 
    {
        $this->_helper->viewRenderer->setNoRender();
        die;
        $ob = new Query('Kontrahenci');
        $rows = $ob->getRows();
        $karta = '1';
        foreach($rows as $row)
        {
            $kartt = '';
            if(strlen($karta) < 5)
            for($i=0;$i<(5 - strlen($karta)); $i++) 
            {
                $kartt .= '0';
            }
            $dane['karta'] = $kartt.''.$karta;
            $ob->id = $row['id'];
            $ob->_update($dane);
            $karta++;
        }    
    }
    function marketingAction()
    {
        $id = intval($this->_getParam('id',0));
        $strona = intval($this->_getParam('od',0));
        $this->view->sortuj = $this->session->marketing->sortuj = $this->_getParam('sortuj') ? $this->_getParam('sortuj')  : 'zd';
        $this->view->ile = $this->session->marketing->ile = $this->_getParam('ile') ? $this->_getParam('ile') : 50;

        $this->rowcount = $this->view->ile == 'all' ? 99999 : $this->view->ile;

        $ob = new Query('Marketing');
        $ob->where = '1';
        $ob->order = $this->view->sortuj == 'zd' ? 'data' : 'nazwa';
        $ob->page = $strona + 1;
        $ob->rowcount = 
        $this->pager = $ob->getRowsFieldCount();
        $this->view->strona = $strona;
        $this->view->strony = ceil($this->pager / $this->rowcount);
        $this->view->link = $this->_getParam('module').'/'.$this->_getParam('controller').'/'.$this->_getParam('action');
        $this->view->rows = $ob->getRowsField();
    }
    function marketingdodajAction()
    {
        if($this->_request->isPost())
        {
            $post = $this->_getParam('post');
            $wyslij = $this->_getParam('wyslij');
            if(!$this->walidacjaMarketing($post))
            {
                $post['klient'] = serialize($post['klient']);
                $post['tekst'] = $post['typ'] == 0 ? strip_tags($post['tekst']) : $post['tekst2'];
                //$post['data'] = date('Y-m-d');
                unset($post['tekst2']);
                $ob = new Query('Marketing');
                $id = $ob->_save($post);
                
                if(isset($wyslij))
                {
                    $this->view->error = '<div class="k_ok">Dodano pomyślnie akcję marketingową. Zostaniesz przeniesiony do wysyłki</div>';
                    $this->getResponse()->setHeader('Refresh', '1; URL='.$this->baseUrl.'/admin/lojalnosc/marketingrozpocznij/id/'.$id);
                } 
                else
                {
                    $this->view->error = '<div class="k_ok">Dodano pomyślnie akcję marketingową.</div>';
                    $this->getResponse()->setHeader('Refresh', '1; URL='.$this->baseUrl.'/admin/lojalnosc/marketingedytuj/id/'.$id);
                }    
            }
            else
            {
                $this->view->dane = $post;
                $this->view->error = '<div class="k_blad">Wypełnij poprawnie formularz.</div>';
                $this->view->error .= '<div>'.$this->_er.'</div>';
            }
        }
        $this->view->editor = $this->fcKeditor('post[tekst2]', isset($post['tekst2']) ? stripslashes($post['tekst2']) : '', 300, 'posredni', 400);
    }
    function marketingedytujAction()
    {
        $id = intval($this->_getParam('id'));
        $ob = new Query('Marketing');
        $ob->id = $id;
        $this->view->dane = $ob->getRow()->toArray();
        $this->view->dane['klient'] = is_string($this->view->dane['klient'])? unserialize($this->view->dane['klient']) : '';
        $this->view->dane['tekst2'] = $this->view->dane['typ'] == 0 ? $this->view->dane['tekst'] : '';

        if($this->_request->isPost())
        {
            $post = $this->_getParam('post');
            $wyslij = $this->_getParam('wyslij');
            if(!$this->walidacjaMarketing($post))
            {
                $post['klient'] = serialize($post['klient']);
                $post['tekst'] = $post['typ'] == 0 ?  $post['tekst2'] : strip_tags($post['tekst']);
                $post['datadeaktywacja'] = $post['aktywna'] == 0 ? date('Y-m-d') : '';

                unset($post['tekst2']);
                $ob->_update($post);
                
                if(isset($wyslij))
                {
                    $this->view->error = '<div class="k_ok">Edycja przebiegła pomyślnie. Zostaniesz przeniesiony do wysyłki</div>';
                    $this->getResponse()->setHeader('Refresh', '1; URL='.$this->baseUrl.'/admin/lojalnosc/marketingrozpocznij/id/'.$id);
                } 
                else
                {
                    $this->view->error = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
                    $this->getResponse()->setHeader('Refresh', '1; URL='.$this->baseUrl.'/admin/lojalnosc/marketingedytuj/id/'.$id);
                } 
            }
            else
            {
                $this->view->dane = $post;
                $this->view->error = '<div class="k_blad">Wypełnij poprawnie formularz.</div>';
                $this->view->error .= '<div>'.$this->_er.'</div>';
            }
        }
        $this->view->editor = $this->fcKeditor('post[tekst2]', isset($this->view->dane['tekst2']) ? stripslashes($this->view->dane['tekst2']) : '', 300, 'posredni', 400);
        if($this->view->dane['typ'] == 0) $this->view->dane['tekst'] = '';
    }
    function marketingskutecznoscAction()
    {
        $this->view->headScript()->appendFile('https://www.google.com/jsapi', 'text/javascript');
        $id = $this->_getParam('id');
        $ob = new Query('Marketing');
        $ob->id = $id;
        $this->view->row = $row = $ob->getRow();
        
        if(!empty($row))
        {
            if($row['suma'] > 0)
            {
                $ar = array();
                $klienci = is_string($row['klient'])? unserialize($row['klient']) : null;
				//var_dump($klienci);

                if(!empty($klienci))
                {
                    foreach($klienci as $klient)
                    {
                        $ex = explode(',', $klient);
                        if(isset($ex[1])) $ar[] = $ex[1];//4
                    }
                }

                /***WYKRES 2***/
                $ob = new Query('Zamowieniakontrahenci');
                $ob->value = $row['data'];
                //$ob->value = '2012-03-01';
                $ob->valuemax = $row['datadeaktywacja'] ? $row['datadeaktywacja'].''.$this->timedop : '';
                $ob->column = 'data';
                $ob->columnin = 'email';
                $ob->where = 1;
                $ob->in = $ar;
                $ob->order = 'data';
                $ob->columnsum = 'kwotazaplata';
                $rows = $ob->getRowsRangeIn();
                $sum = $ob->getRowsRangeInSum();

                $ob->group = 'email';
                $rows2 = $ob->getRowsRangeIn();

                $this->view->ilosczam = count($rows);
                $this->view->ilosckl = count($rows2);
                $this->view->sumzam = $sum['sum'] ? $sum['sum'] : 0.00;
                $this->view->skutilosckl = $row['suma'] != 0 ? ceil($this->view->ilosckl * 100 / $row['suma']) : ( $this->view->ilosckl > 0 ? 100: 0);
                $this->view->skutilosc = $row['suma'] != 0 ? ceil($this->view->ilosczam * 100 / $row['suma']) : ( $this->view->ilosczam > 0 ? 100: 0);
                $this->view->skutwar = $row['wartosc'] != 0 ? ceil($this->view->sumzam * 100 / $row['wartosc']) : ( $this->view->sumzam > 0 ? 100: 0);

                $array = array();
                $arraysort = array();
                if(!empty($rows2))
                {
                    if(count($rows2) == 1)
					{
                        $rows2[1]['polecajacy'] = 0;
                        $rows2[1]['data'] = $this->view->row['data'];
                    }
                    foreach($rows2 as $row)
                    {						
                        $ex = explode(' ', $row['data']);
                        if(isset($ex[0]))
                        {
                            if(!isset($array[$ex[0]]))
                            {
                                $array[$ex[0]][2] = empty($row['polecajacy'])? 1: 0;
                                $array[$ex[0]][3] = !empty($row['polecajacy']) ? 1 : 0;
                                $arraysort[] = $ex[0];
                            }
                            else
                            {
                                $array[$ex[0]][2] += empty($row['polecajacy']) ? 1 : 0;
                                $array[$ex[0]][3] += !empty($row['polecajacy']) ? 1 : 0;
                            }

                        }
                    }
                    array_multisort($arraysort, SORT_ASC,$array);
                    $array2 = array();
                    $temp1 = null;
                    $temp2 = null;
                    if(!empty($array))
                    foreach($array as $key=>$r)
                    {
                        $temp1 = $temp1 + $r[2];
                        $temp2 = $temp2 + $r[3];
                        $array2[$key][2] = $temp1;
                        $array2[$key][3] = $temp2;
                    }

                    $this->view->wykresklienci = $array;
                }

                $array = array();
                $arraysort = array();
                if(!empty($rows))
                {
                    if(count($rows) == 1)
					{
                        $rows[1]['polecajacy'] = 0;
                        $rows[1]['kwotazaplata'] = 0;
                        $rows[1]['data'] = $this->view->row['data'];
                    }
                    foreach($rows as $row)
                    {
                        $ex = explode(' ', $row['data']);
                        if(isset($ex[0]))
                        {
                            if(!isset($array[$ex[0]]))
                            {
                                $array[$ex[0]][2] = empty($row['polecajacy'])? $row['kwotazaplata']: 0;
                                $array[$ex[0]][3] = !empty($row['polecajacy']) ? $row['kwotazaplata'] : 0;
                                $arraysort[] = $ex[0];
                            }
                            else
                            {
                                $array[$ex[0]][2] += empty($row['polecajacy']) ? $row['kwotazaplata'] : 0;
                                $array[$ex[0]][3] += !empty($row['polecajacy']) ? $row['kwotazaplata'] : 0;
                            }
                        }
                    }
                    array_multisort($arraysort, SORT_ASC,$array);
                    $array2 = array();
                    $temp1 = null;
                    $temp2 = null;
                    if(!empty($array))
                    foreach($array as $key=>$r)
                    {
                        $temp1 = $temp1 + $r[2];
                        $temp2 = $temp2 + $r[3];
                        $array2[$key][2] = $temp1;
                        $array2[$key][3] = $temp2;
                    }
                    $this->view->wykreszamowienia = $array;
                }
                /***END WYKRES 2***/
                $array = array();
                $arraysort = array();
                if(!empty($rows))
                {
                    if(count($rows) == 1)
					{
                        $rows[1]['polecajacy'] = 0;
                        $rows[1]['kwotazaplata'] = 0;
                        $rows[1]['data'] = $this->view->row['data'];
                    }
                    foreach($rows as $row)
                    {
                        $ex = explode(' ', $row['data']);
                        if(isset($ex[0]))
                        {
                            if(!isset($array[$ex[0]]))
                            {
                                $array[$ex[0]][2] = empty($row['polecajacy'])? 1 : 0;
                                $array[$ex[0]][3] = !empty($row['polecajacy']) ? 1 : 0;
                                $arraysort[] = $ex[0];
                            }
                            else
                            {
                                $array[$ex[0]][2] += empty($row['polecajacy']) ? 1 : 0;
                                $array[$ex[0]][3] += !empty($row['polecajacy']) ? 1 : 0;
                            }
                        }
                    }
                    array_multisort($arraysort, SORT_ASC,$array);
                    $array2 = array();
                    $temp1 = null;
                    $temp2 = null;
                    if(!empty($array))
                    foreach($array as $key=>$r)
                    {
                        $temp1 = $temp1 + $r[2];
                        $temp2 = $temp2 + $r[3];
                        $array2[$key][2] = $temp1;
                        $array2[$key][3] = $temp2;
                    }
                    $this->view->wykreszamowienia2 = $array;
                }
                /***END WYKRES 2***/
            }
            else
            {
                $this->view->message = '<div class="k_blad">Nie przeprowadziłeś jeszcze wysyłki dla wybranej akcji marketingowej.</div>';
            }
        }
        else
        {
            $this->view->message = '<div class="k_blad">Nie ma takiej akcji marketingowej.</div>';
        }
    }

    function marketingrozpocznijAction()
    {
        $id = intval($this->_getParam('id'));
        $ob = new Query('Marketing');
        $ob->id = $id;
        $this->view->dane = $ob->getRow()->toArray();
        $this->view->dane['klient'] = !empty($this->view->dane['klient']) ? unserialize($this->view->dane['klient']) : '';
        
        $soap = new Wiadomosci();
        
        if($this->view->dane['typ'] == 1){
            $conf = new Confsms();
            $data = $conf->wyswietl();
            $daneSMS['nadawca'] = $data['pole_nadawca'];
            $daneSMS['typ'] = $data['typ_wiadomosci'];
            $soap = new SmsClientSoap();
            $smskonto = $soap->wartoscKonta();
			//var_dump($smskonto);
            $this->view->bladkonta = (0 && is_string($smskonto)) ? '<div class="k_blad">Błędne dane konta sms w zakładce <a href="'.$this->baseUrl.'/admin/confsms/edytuj/">Ustawienia &rarr; Ustawienia sms</a></div>' : false;
        }
        if($this->_request->isPost())
        {
            $post = $this->_getParam('post');
            if(!$this->walidacjaRozpocznij($post))
            {
                $klienci = $post['klient'];
                $err = '';
                /*SMS*/
                if($this->view->dane['typ'] == 1)
                {
                    $err = '';
                    $wyslanych = $this->view->dane['suma'];
                    $stankonta = $this->view->dane['wartosc'];
                    $powtorzen = $this->view->dane['powtorzen'];
                    if(@count($klienci) > 0)
                    {
                        $k = 0;
                        foreach($klienci as $key => $value)
                        {
                            $blad = $soap->send(strip_tags($this->view->dane['tekst']), 
										$value, $daneSMS['typ'], $daneSMS['nadawca']);
                            if(!empty($blad))//isset($blad['error']) && !empty($blad['error']))
							{
                                $err .= ++$k.'. Telefon: <strong>'.$value.'</strong> - '.$blad.'<br />';
                            }
							else
							{
                                $wyslanych++;
                                $stankonta += $blad['points'];
                            }
                        }
                        if(!empty($err))
						{
                            $this->view->error = '<div>Wystąpiły problemy z wysłaniem następujących sms:</div>';
                            $this->view->error .= '<div>'.$err.'</div>';
							//echo $this->view->error.'<br>';							
                        }
						//var_dump($blad);//die();
                        $ob->_update(array('datarozpoczecia'=> date('Y-m-d'), 'powtorzen'=>$powtorzen+1, 'suma'=>$wyslanych, 'wartosc'=>$stankonta));
                    }
                }
                /*END SMS*/
                /*E-MAIL*/
                if($this->view->dane['typ'] == 0)
                {
                    $err = '';
                    $wyslanych = $this->view->dane['suma'];
                    $powtorzen = $this->view->dane['powtorzen'];
                    if(@count($klienci) > 0)
                    {
                        $k = 0;

                        $conf = new Confmail();
                        $data = $conf->showData();
                        $daneMail['temat'] = $this->view->dane['nazwa'];
                        $daneMail['email'] = $data[0]['From'];
                        $daneMail['nazwa'] = $this->view->tytulosoby;
                        $daneMail['opis'] = stripslashes($this->view->dane['tekst']);
                        
                        foreach($klienci as $key=>$value)
                        {
                            $daneMail['mail'] = $value;
                            $blad = $soap->mailKlient($daneMail, false);
                            if(isset($blad['error']) && !empty($blad['error']))
							{
                                $err .= ++$k.'. E-mail: <strong>'.$value.'</strong> - '.$blad.'<br />';
                            }
							else
							{
                                $wyslanych++;
                            }
                        }
                        if(!empty($err))
						{
                            $this->view->error = '<div>Wystąpiły problemy z wysłaniem następujących wiadomości:</div>';
                            $this->view->error .= '<div>'.$err.'</div>';
                        }
                        $ob->_update(array('datarozpoczecia'=> date('Y-m-d'), 'powtorzen'=>$powtorzen+1, 'suma'=>$wyslanych));
                    }
                }
                /*END SMS*/

                $this->view->error = '<div class="k_ok">Akcja została przeprowadzona poprawnie.</div>'.$this->view->error;
                $this->getResponse()->setHeader('Refresh', '3; URL='.$this->baseUrl.'/admin/lojalnosc/marketingrozpocznij/id/'.$id);
            }
            else
            {
                $this->view->dane = $post;
                $this->view->error = '<div class="k_blad">Wypełnij poprawnie formularz.</div>';
                $this->view->error .= '<div>'.$this->_er.'</div>';
            }
        }
        $this->view->editor = $this->fcKeditor('post[tekst]', isset($this->view->dane['tekst']) ? stripslashes($this->view->dane['tekst']) : '', 300, 'posredni', 400, 'office2003', true);
    }

    function marketingdeleteAction()
    {
        $ob = new Query('Marketing');
        $ob->id = intval($this->_getParam('id'));
        $ob->_delete();
        $this->_redirect('/admin/lojalnosc/marketing/');
    }

    protected function walidacjaMarketing($dane)
	{
        $counter = 0;
        $this->_er ='';
        if(empty($dane['nazwa']))
        {
            $this->_er .= ++$counter.'. Pole nazwa jest wymagane.<br />';
        }
        if(!isset($dane['typ']))
        {
            $this->_er .= ++$counter.'. Pole typ jest wymagane.<br />';
        }
        if(false && !isset($dane['czas']))
        {
            $this->_er .= ++$counter.'. Pole wysyłka jest wymagane.<br />';
        }
        if(!isset($dane['klient']) || empty($dane['klient']))
        {
            $this->_er .= ++$counter.'. Pole klient jest wymagane.<br />';
        }
        return $this->_er;
    }
    public function zakupyallAction()
    {
        if($this->_getParam('reset'))
        {
            $reset = $this->_getParam('reset');
            if($reset == 'all')
            {
                if(isset($this->session->zakupyall)) unset($this->session->zakupyall);
                $this->_redirect('/admin/lojalnosc/zakupyall/');
            }
        }        
        
        if($this->_request->isPost())
        {
            $post = $this->_getParam('post');
            if(isset($post['od']) && !empty($post['od']))
            {
                $this->session->zakupyall->od = ' AND data >="'.$post['od'].'"';
                $this->session->zakupyall->pole = $post['od'];
            } 
            else   
            {
                $this->session->zakupyall->pole = $this->session->zakupyall->od = '';
            } 
            if(isset($post['do']) && !empty($post['do']))
            {
                $this->session->zakupyall->do = ' AND data <="'.$post['do'].'"';
                $this->session->zakupyall->pole2 = $post['do'];
            } 
            else   
            {
                $this->session->zakupyall->pole2 = $this->session->zakupyall->do = '';
            }
        }
        
        $this->view->od = isset($this->session->zakupyall->pole)? $this->session->zakupyall->pole : '';
        $this->view->do = isset($this->session->zakupyall->pole2)? $this->session->zakupyall->pole2 : '';
        
        $where = isset($this->session->zakupyall->od)? $this->session->zakupyall->od : '';
        $where .= isset($this->session->zakupyall->do)? $this->session->zakupyall->do : '';
        
        $strona = intval($this->_getParam('od',0));
        $this->view->headLink()->appendStylesheet($this->_request->getBaseUrl().'/public/styles/calendar.css');
        $ob = new Query('Zamowieniakontrahenci');
        $ob->where = '1'.$where;
        $ob->order = 'data';
        $ob->page = $strona+1;
        $ob->rowcount = $this->rowscount;
        $this->pager = $ob->getRowsFieldCount();
        $this->view->strona = $strona;
        $this->view->strony = ceil($this->pager / $this->rowscount);
        $this->view->link = $this->_getParam('module').'/'.$this->_getParam('controller').'/'.$this->_getParam('action');
        $this->view->rows = $ob->getRowsField();
    }
    protected function walidacjaRozpocznij($dane)
    {
        $counter = 0;
        $this->_er ='';
        if(!isset($dane['klient']) || empty($dane['klient']))
        {
            $this->_er .= ++$counter.'. Dodaj do akcji klientów do których można wysyłać wiadomości.<br />';
        }
        return $this->_er;
    }
	
	public function pokazAction() 
	{
        $this->view->headScript()->appendFile('https://www.google.com/jsapi', 'text/javascript');
        $this->view->headScript()->appendFile($this->_request->getBaseUrl() . '/public/scripts/datepicker.js', 'text/javascript');
        $this->view->headScript()->appendFile($this->_request->getBaseUrl() . '/public/scripts/ui.core.js', 'text/javascript');
        $this->view->headScript()->appendFile($this->_request->getBaseUrl() . '/public/scripts/functionadmin.js', 'text/javascript');
        $this->view->headLink()->appendStylesheet($this->_request->getBaseUrl().'/public/styles/calendar.css');

        $kontrahenci = new Kontrahenci();
        $this->view->kontrahenci = $kontrahenci->wypisz()->toArray();

        $post = $this->_getParam('post');
        $wybrany = $this->_getParam('id', 0);

        if(!$wybrany)
        {
            $ob = new Query('Lojalnoscustawienia');
            $ob->id = 1;
            $this->view->cfg = $cfg = $ob->getRow();
            $this->view->start = $this->start = !empty($cfg->start) ? $cfg->start : null;
            $this->view->koniec = $this->koniec = !empty($cfg->koniec) ? $cfg->koniec : null;

            /***WYKRES 1***/
            $ob = new Query('Kontrahenci');
            $ob->value = (@$post['rokklienci'] ? $post['rokklienci'] : date('Y')).'-01-01';
            $ob->valuemax = (@$post['rokklienci'] ? $post['rokklienci'] : date('Y')).'-12-31 '.$this->timedop;
            $ob->column = 'data_rej';
            //$ob->column3 = 'kartaaktywna';
            //$ob->value3 = 1;
            $this->view->rokklienci = @$post['rokklienci'] ? $post['rokklienci'] : date('Y');
            $rows = $ob->getRowsRange();

            $ob = new  Query('Kontrahenci');
            $ob->where = '1';//kartaaktywna <> 0';
            $this->view->allcl = $ob->getRowsFieldCount();
            $ob->where = 'polecajacy = 0';//AND kartaaktywna <> 0
            $this->view->stcl = $ob->getRowsFieldCount();
            $ob->where = 'polecajacy <> 0';//AND kartaaktywna <> 0
            $this->view->zncl = $ob->getRowsFieldCount();
            $obz = new  Query('Zamowieniakontrahenci');
            $obz->where = 1;
            $this->view->allzam = $obz->getRowsFieldCount();
            $obz->where = 'polecajacy = 0 OR polecajacy = ""';
            $this->view->stzam = $obz->getRowsFieldCount();
            $obz->where = 'polecajacy <> 0';
            $this->view->znzam = $obz->getRowsFieldCount();
            
            $array = array();
            $arraysort = array();
            if(!empty($rows))
            {
                $temp1 = null;
                $temp2 = null;
                foreach($rows as $row)
                {
                    $ex = explode(' ', $row['data_rej']);
                    if(isset($ex[0]))
                    {
                        if(!isset($array[$ex[0]]))
                        {
                            $array[$ex[0]][2] = $row['polecajacy'] == 0 ? 1 : 0;
                            $array[$ex[0]][3] = $row['polecajacy'] != 0 ? 1 : 0;
                            $arraysort[] = $ex[0];
                        }   
                        else
                        {
                            $array[$ex[0]][2] += $row['polecajacy'] == 0 ? 1 : 0;
                            $array[$ex[0]][3] += $row['polecajacy'] != 0 ? 1 : 0;
                        }
                    } 
                }
                array_multisort($arraysort, SORT_ASC,$array);
                $array2 = array();
                if(!empty($array))
                foreach($array as $key=>$r)
                {
                    $temp1 = $temp1 + $r[2];
                    $temp2 = $temp2 + $r[3];
                    $array2[$key][2] = $temp1;
                    $array2[$key][3] = $temp2;
                }
                $this->view->wykresuzytkownicy = Query::charDrawWeek($array, $arraysort);
                $this->view->wykresuzytkownicy2 = Query::charDrawWeek($array2, $arraysort, true);
            }
            /***END WYKRES 1***/

            /***WYKRES 2***/
            $ob = new Query('Zamowieniakontrahenci');
            $ob->value = (@$post['rokzam'] ? $post['rokzam'] : date('Y')).'-01-01';
            $ob->valuemax = (@$post['rokzam'] ? $post['rokzam'] : date('Y')).'-12-31 '.$this->timedop;
            $ob->column = 'data';

            $this->view->rokzam = @$post['rokzam'] ? $post['rokzam'] : date('Y');
            $rows = $ob->getRowsRange();
            $array = array();
            $arraysort = array();
            if(!empty($rows))
            {
                if(count($rows) == 1)
				{
                    $rows[1]['polecajacy'] = 0;
                    $rows[1]['kwotazaplata'] = 0;
                    $rows[1]['data'] = $this->view->start;
                }
                foreach($rows as $row)
                {
                    $ex = explode(' ', $row['data']);
                    if(isset($ex[0]))
                    {
                        if(!isset($array[$ex[0]]))
                        {
                            $array[$ex[0]][2] = empty($row['polecajacy'])? $row['kwotazaplata']: 0;
                            $array[$ex[0]][3] = !empty($row['polecajacy']) ? $row['kwotazaplata'] : 0;
                            $arraysort[] = $ex[0];
                        }
                        else
                        {
                            $array[$ex[0]][2] += empty($row['polecajacy']) ? $row['kwotazaplata'] : 0;
                            $array[$ex[0]][3] += !empty($row['polecajacy']) ? $row['kwotazaplata'] : 0;
                        }

                    }
                }
                array_multisort($arraysort, SORT_ASC,$array);
                $array2 = array();
                $temp1 = null;
                $temp2 = null;
                if(!empty($array))
                foreach($array as $key=>$r)
                {
                    $temp1 = $temp1 + $r[2];
                    $temp2 = $temp2 + $r[3];
                    $array2[$key][2] = $temp1;
                    $array2[$key][3] = $temp2;
                }

                $this->view->wykreszamowienia = Query::charDrawWeek($array, $arraysort);
                $this->view->wykreszamowienia2 = Query::charDrawWeek($array2, $arraysort, true);
				//var_dump($this->view->wykreszamowienia2);
            }
            /***END WYKRES 2***/
        }

        $this->view->opis = '';
        $od = $this->_request->getPost('od', '2010-01-01');
        $do = $this->_request->getPost('do', date('Y-m-d'));
        $this->view->id = $this->_request->getParam('id') ? false : true;
        $this->view->dane = array('od' => $od, 'do' => $do, 'id' => $this->_request->getParam('id'));
        
        if($this->_request->getParam('szukaj') || $this->_request->getParam('id'))
        {
            /***WYKRES 3***/
			
			$obj = new Query('Kontrahenci');
            $obj->id = intval($this->_getParam('id'));
            $klient = $obj->getRow();
			
            $ob = new Query('Zamowieniakontrahenci');
            $ob2 = new Query('Zamowieniakontrahenci');
            //$ob->id = intval($this->_request->getParam('id'));
			$ob->id = $klient['email'];
            $ob2->value = $ob->value = $this->view->dane['od'];
            $ob2->valuemax = $ob->valuemax = $this->view->dane['do'].' '.$this->timedop;
            $ob2->column = $ob->column = 'data';
            $ob->column2 = 'email';
            $ob2->order = $ob->order = 'data ASC';
            $rows = $ob->getRowsRange();
            $this->view->all = $rows;
            
            $array = array();
            $arraysort = array();
            if(!empty($rows))
            {
                $ob2->column2 = 'polecajacy';
                $ob2->id = intval($this->_getParam('id'));
                $rowspol = $ob2->getRowsRange();

                $rows = array_merge($rows, $rowspol);
                if(count($rows) == 1)
				{
                    $rows[1]['polecajacy'] = 0;
                    $rows[1]['kwotazaplata'] = 0;
                    $rows[1]['data'] = $klient['data_rej'];
                }

                foreach($rows as $row)
                {
                    $ex = explode(' ', $row['data']);
                    if(isset($ex[0]))
                    {
                        if(!isset($array[$ex[0]]))
                        {
                            $array[$ex[0]][2] = $row['polecajacy'] != intval($this->_getParam('id')) ? $row['kwotazaplata']: 0;
                            $array[$ex[0]][3] = $row['polecajacy'] == intval($this->_getParam('id')) ? $row['kwotazaplata'] : 0;
                            $arraysort[] = $ex[0];
                        }
                        else
                        {
                            $array[$ex[0]][2] += $row['polecajacy'] != intval($this->_getParam('id')) ? $row['kwotazaplata'] : 0;
                            $array[$ex[0]][3] += $row['polecajacy'] == intval($this->_getParam('id')) ? $row['kwotazaplata'] : 0;
                        }
                    }
                }
                $this->view->wykresyzamklient = Query::charDrawWeek($array, $arraysort);
            }
            /***END WYKRES 3***/

            /***WYKRES 4***/
            $ob = new Query('Kontrahenci');
            $ob->value = ($post['rokkliencikl'] ? $post['rokkliencikl'] : date('Y')).'-01-01';
            $ob->valuemax = ($post['rokkliencikl'] ? $post['rokkliencikl'] : date('Y')).'-12-31 '.$this->timedop;
            $ob->column = 'data_rej';
            //$ob->column3 = 'kartaaktywna';
            //$ob->value3 = 1;
            $ob->column2 = 'polecajacy';
            $ob->id = intval($this->_getParam('id'));
            $this->view->rokkliencikl = $post['rokkliencikl'] ? $post['rokkliencikl'] : date('Y');
            $rows = $ob->getRowsRange();

            $ob->value = intval($this->_getParam('id'));
            $ob->column = 'polecajacy';
            $ob->order = 'id';
            $rowc = count($ob->getRowsValue());
            $this->view->iloscznajomi = $rowc;

            $array = array();
            $arraysort = array();
            if(!empty($rows))
            {
                foreach($rows as $row)
                {
                    $ex = explode(' ', $row['data_rej']);
                    if(isset($ex[0]))
                    {
                        if(!isset($array[$ex[0]]))
                        {
                            $array[$ex[0]][2] = 1;
                            $arraysort[] = $ex[0];
                        }
                        else
                        {
                            $array[$ex[0]][2] += 1;
                        }

                    }
                }
                $this->view->wykresyznajomi = Query::charDrawWeek($array, $arraysort);
            }
            /***END WYKRES 4***/
            
            if(count($this->view->all) == 0 && count($this->view->zam) == 0)
            {
                $this->view->opis = 'Brak dokonanych zakupów w okresie od dnia '.$od.' do '.$do.'.';
            }
        }     
        $this->_request->getParam('id') && $wybrany ? $this->_helper->viewRenderer->render('pokazwybrany') : '';        
    }
	
	function ustawieniaAction()
	{
		$ustawienia = $this->_request->getPost('ustawienia');
		if($ustawienia)
		{
			$dane = $this->_request->getPost('ustaw');
			if(false)
			{
				$ceny = '';
				foreach($this->ceny as $nazwa => $label)
				$ceny .= $this->_request->getPost($nazwa, '') == 'on' ? $nazwa.';' : '';
				$dane['ceny'] = $ceny;
			}
			$settings = new Ustawienia();
			$settings->updateData($dane);
			$this->view->error = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/index/ustawienia/');
		}
		$settings = new Ustawienia();
		$ustawienia = $settings->showData()->toArray();
		$this->view->dane = $ustawienia[0];
	}
	
	function regulaminAction()
	{
		$ob = new Query('Podstrony');
		//$ob->id = intval(1);
		$ob->column = 'link';
		$ob->value = $this->_getParam('link');//'Program-lojalnosciowy'
		$this->view->dane = $ob->getRowValue();
		$common = new Common();
		$this->view->dane['tekst'] = $common->fixTekst($this->view->dane['tekst'], true);
		$this->view->tekst = $this->fcKeditor('ustaw[tekst]', stripslashes($this->view->dane['tekst']));
		if($this->_request->isPost())
		{
			$post = $this->_getParam('ustaw');
			if(!empty($post['temat']))
			{
				$ob->_updateValue($post);
				$this->view->error = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->getResponse()->setHeader('Refresh','1; URL='.$this->view->baseUrl.'/admin/lojalnosc/regulamin/link/'.$this->_getParam('link'));
			}
			else
			{
				$this->view->error = '<div class="k_blad">Tytuł jest wymagany.</div>';
			}
		}
	}
	
	public function confmailAction()
	{
		$mail = new Confmail;
		$err=0;
		if($this->_request->isPost())
		{
			$valid = new Zend_Validate();
			$mainValid = $valid->addValidator( new Zend_Validate_NotEmpty() );
			if($mainValid->isValid($this->_request->getPost('From'))){
				$from = $this->_request->getPost('From'); 				
			}else{ $err=1; }
			if($mainValid->isValid($this->_request->getPost('FromName'))){
				$FromName= $this->_request->getPost('FromName'); 				
			}else{ $err=1; }
			//if($mainValid->isValid($this->_request->getPost('Host', ''))){
				$Host = $this->_request->getPost('Host', ''); 				
			//}else{ $err=0; }
			//if($mainValid->isValid($this->_request->getPost('POP3', ''))){
				$POP3 = $this->_request->getPost('POP3', ''); 				
			//}else{ $err=0; }
			//if($mainValid->isValid($this->_request->getPost('Username', ''))){
				$Username = $this->_request->getPost('Username', ''); 				
			//}else{ $err=0; }
			//if($mainValid->isValid($this->_request->getPost('Password', ''))){
				$Password = $this->_request->getPost('Password', ''); 				
			//}else{ $err=0; }
			//if($mainValid->isValid($this->_request->getPost('Mailer', ''))){
				$Mailer = $this->_request->getPost('Mailer', ''); 				
			//}else{ $err=0; }
			//if($mainValid->isValid($this->_request->getPost('ToCV', ''))){
				$ToCV = $this->_request->getPost('ToCV', ''); 				
			//}else{ $err=0; }
			
			if(!$err)
			{
				$array = array('From'=>$from, 'FromName'=>$FromName, 'Host'=>$Host, 'POP3'=>$POP3, 'Username'=>$Username, 'Password'=>$Password, 'Mailer'=>$Mailer, 'ToCV'=>$ToCV);
				if($this->_getParam('tytulpol'))
				$array['tytulpol'] = $this->_getParam('tytulpol');
				if($this->_getParam('trescpol'))
                $array['trescpol'] = $this->_getParam('trescpol');
                                
                $mail->updateData($array, 1);
				//$this->view->error = '<div class="k_ok"> Edycja przebiegła pomyślnie </div>';
			}
			else
			{
				$this->view->error = '<div class="k_blad"> Wszystkie pola są wymagane </div>';
			}
		}
		$this->view->drukuj = $mail->showData();
	}
	
	function upominkiAction()
    {
        $id = intval($this->_getParam('id',0));
        $strona = intval($this->_getParam('od',0));

        $ob = new Query($this->table);
        $ob->id = $id;
        $this->view->row = $ob->getRow();
        if(!empty($this->view->row))
        {
            $ob = new Query('Lojalnoscrabatypole');
            $ob->where = 'idpol="'.$id.'" AND upominek = "1"';
            $ob->order = 'data';
            $ob->page = $strona+1;
            $ob->rowcount = $this->rowscount;
            $this->pager = $ob->getRowsFieldCount();
            $this->view->strona = $strona;
            $this->view->strony = ceil($this->pager / $this->rowscount);
			$this->view->link = 
				$this->_getParam('module').'/'.
				$this->_getParam('controller').'/'.
				$this->_getParam('action').'/id/'.$id;
            $this->view->rows = $ob->getRowsField();
        }
        else
        {
            $this->_redirect('/admin/');
        }
    }

    function upominekwybierzAction()
    {
        $id = intval($this->_getParam('id',0));
        $idzam = intval($this->_getParam('idzam',0));
        $strona = intval($this->_getParam('od',0));

        $ob = new Query($this->table);
        $ob->id = $id;
        $this->view->row = $ob->getRow();
		
        if(!empty($this->view->row))
        {
            $ob = new Query('Lojalnoscrabatypole');
            $ob->id  = $idzam;
            $this->view->rows =$ob->getRow();

            $post = $this->_getParam('post');
            if($this->_request->isPost($post))
            {
                if(!empty($post['upominekwybrany']))
                {
                    $ob->_update(array
					(
						'dataodebrania'=>date('Y-m-d H:i:s'),
						'upominekwybrany'=>intval($post['upominekwybrany'])
					));
                    $this->view->error = '<div class="k_ok">Upominek został wybrany.</div>';
                    $this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/lojalnosc/upominekwybierz/id/'.$id.'/idzam/'.$idzam);
                }
                else
                {
                    $this->view->error = '<div class="k_blad">Wybierz upominek.</div>';
                }
            }
        }
        else
        {
            $this->_redirect('/admin/');
        }
    }
	
	function zakupyAction()
    {
        $id = intval($this->_getParam('id',0));
        $strona = intval($this->_getParam('od',0));

        $ob = new Query($this->table);
        $ob->id = $id;
        $this->view->row = $ob->getRow();
		
        if(!empty($this->view->row))
        {
            $ob = new Query($this->subtable);
            $ob->where = 'email="'.$this->view->row['email'].'"';
            $ob->order = 'data';
            $ob->page = $strona+1;
            $ob->rowcount = $this->rowscount;
            $this->pager = $ob->getRowsFieldCount();
            $this->view->strona = $strona;
            $this->view->strony = ceil($this->pager / $this->rowscount);
			$this->view->link = 
				$this->_getParam('module').'/'.
				$this->_getParam('controller').'/'.
				$this->_getParam('action').'/id/'.$id;
            $this->view->rows = $ob->getRowsField();
        }
        else
        {
            $this->_redirect('/admin/');
        }
    }
}
?>