<?php
class Admin_ConfsmsController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
	}	
	function __call($method, $args)
	{
		$this->_redirect('/admin/index/panel/');
	}
	
	public function edytujAction()
	{
		$this->view->r_zapisz = 'zapisz';
		$sms = new Confsms();
		//echo $sms->wyslij('513701688', '123456789');//die();
		$dane = $this->_request->getParam('sms');

		if(isset($dane['zapisz']))
		{		
			$walid = $this->walidacja($dane);
			
			if(empty($walid))
			{
				unset($dane['zapisz']);
				$sms->id = 1;
				if(@!empty($dane['password']))
				$dane['password'] = md5($dane['password']);
				else unset($dane['password']);
				$sms->edytuj($dane);
				$this->view->error = '<div class="k_ok"> Ustawienia zostały zapisane. </div>';
				$this->_redirect('/admin/confsms/edytuj/');
				$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/confsms/edytuj/');
			}
			else
			{
				$this->view->error = '<div class="k_blad"> Błąd edycji danych. </div>';
				$this->view->error .= '<div class="lista_bledow">'.$walid.'</div>';
			}
		
		}
		
		$this->view->html = $sms->wyswietl();
		$smsapi = new SmsClientSoap();
		$this->view->ilosc_kredytow = $smsapi->wartoscKonta();
	}
	
	private function walidacja($dane)
	{
		$counter = 0;
		$error = '';
		
		//echo $dane['typ_wiadomosci'];
		
		if(empty($dane['login']))
		{
			$counter++;
			$error .= $counter.'. Pole login jest wymagane.<br />';		
		}
		
		if(false && empty($dane['password']))
		{
			$counter++;
			$error .= $counter.'. Pole hasło jest wymagane.<br />';		
		}
		
		if(!isset($dane['typ_wiadomosci']))
		{
			$counter++;
			$error .= $counter.'. Pole typ wiadomości jest wymagane.<br />';		
		}
		
		if(false && empty($dane['pole_nadawca']))
		{
			$counter++;
			$error .= $counter.'. Pole nadawca jest wymagane.<br />';		
		}
		
		if(false && empty($dane['admin_sms']))
		{
			$counter++;
			$error .= $counter.'. Pole telefon admina jest wymagane.<br />';		
		}
		if(true && !empty($dane['admin_sms']))
		if(!is_numeric($dane['admin_sms']))
		{
			$counter++;
			$error .= $counter.'. Niepoprawny format telefonu admina, wymagany format to numeryczny.<br />';	
		}
		
		return $error;
	}
}	
?>