<?php
class Admin_GaleriaController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		//echo $this->view->baseUrl;
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}
	
	function dodajAction()
	{
		$this->view->r_zapisz = 'zapisz';
		$this->view->r_lista = 'admin/galeria/lista/';
		
        $this->view->editor = $this->fcKeditor('opis', "", 400);
        $this->view->rodzaj = $this->_request->getParam('typ');
        if($this->_request->isPost()) 
		{
            $filter = new Zend_Filter_StripTags();
            $nazwa = $filter->filter($this->_request->getPost('nazwa'));
            $opis = $this->_request->getPost('opis');

            if(empty($nazwa)) 
			{
                $error['err'] = '<div class="k_blad">Pole nazwa jest wymagane</div>';
            }
			
			if(isset($_FILES['img']) && !empty($_FILES['img']['tmp_name']))
			{
				$handle = @new upload($_FILES['img']);
				
				if($handle->uploaded) 
				{
					$handle->file_new_name_body = 'galeria';
					$handle->image_resize = false;
					$handle->image_x = $this->ImageDir->GaleriaX;
					$handle->image_y = $this->ImageDir->GaleriaY;
					$handle->image_ratio_x = true;
					$handle->process($this->ImageDir->GaleriaX);
					
					if($handle->processed) 
					{
						$handle->clean();
						$img = $handle->file_dst_name;
					}
					else 
					{
						$error['err'] = '<div class="k_blad">Upload nieudany. Nie można utworzyć miniaturki.</div>';
					}
				}
				else 
				{
					$error['err'] = '<div class="k_blad">Upload nieudany. Nie można przenieść zdjęcia na serwer.</div>';
				}
			}

            if(empty($error)) 
			{
                $date = date("Y-m-d H:i:s");
                $addArray = array
				(
					'nazwa' => $nazwa,
                    'opis' => $opis,
                    'data' => $date
                );
				if(isset($img)) $addArray['img'] = $img;
                $sliderTypy = new Slidertypy();
                $id = $sliderTypy->dodaj($addArray, $this->lang);
                $this->_redirect("admin/galeria/edytuj/id/" . $id);
                //$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegla pomyslnie</div>';
            } 
			else 
			{
                $this->view->blad_edycji = $error['err'];
            }
        }
	}
	
	public function listaAction() 
	{
		$this->view->r_dodaj = 'admin/galeria/dodaj';		
        $wszystkie = new Slidertypy();
        //$rodzaj = $this->_request->getParam('typ');
        if($this->_request->getParam('delid') > 0) 
		{
            $filter = new Zend_Filter_StripTags();
            $id = $filter->filter($this->_request->getParam('delid'));
            $db = $wszystkie->getAdapter();
            $where = $db->quoteInto('id = ?', $id);
            $rows_affected = $wszystkie->delete($where);
        }
		if(($pokazid = $this->_request->getParam('pokazid')) > 0)
		{
			$widoczny = @intval($this->_request->getParam('widoczny')?1:0);
            $wszystkie->edytuj(array('wyswietl' => $widoczny), $pokazid);
			$this->_redirect('admin/galeria/lista');
        }
		if(($glownaid = $this->_request->getParam('glownaid')) > 0)
		{
			$glowna = @intval($this->_request->getParam('glowna')?1:0);
            $wszystkie->edytuj(array('glowna' => $glowna), $glownaid);
			$this->_redirect('admin/galeria/lista');
        }
        $this->view->realizacje = $wszystkie->wypisz($this->lang);
    }

    public function edytujAction() 
	{
        $wybrana = new Slidertypy();

        $filter = new Zend_Filter_StripTags;
        $id = $filter->filter($this->_request->getParam('id', 0));
        $this->view->rodzaj = $filter->filter($this->_request->getParam('typ'));

        if($this->_request->isPost()) 
		{
            $filter = new Zend_Filter_StripTags();
            $nazwa = $filter->filter($this->_request->getPost('nazwa'));
            $opis = $this->_request->getPost('opis');
            
			if(empty($nazwa)) 
			{
                $error['err'] = '<div class="k_blad">Pole nazwa jest wymagane</div>';
            }
			
			if($this->obConfig->galeriaZdjecia)
			if(isset($_FILES['img']) && !empty($_FILES['img']['tmp_name']))
			{
				$handle = @new upload($_FILES['img']);				
				if($handle->uploaded) 
				{
					$handle->file_new_name_body = 'galeria';
					$handle->image_resize = false;
					$handle->image_x = $this->ImageDir->GaleriaX;
					$handle->image_y = $this->ImageDir->GaleriaY;
					$handle->image_ratio_x = true;
					$handle->process($this->ImageDir->Galeria);					
					if($handle->processed) 
					{
						$handle->clean();
						$img = $handle->file_dst_name;
					}
					else 
					{
						$error['err'] = '<div class="k_blad">Upload nieudany. Nie można utworzyć miniaturki.</div>';
					}
				}
				else 
				{
					$error['err'] = '<div class="k_blad">Upload nieudany. Nie można przenieść zdjęcia na serwer.</div>';
				}
			}

            if(empty($error)) 
			{
                $editArray = array
				(
                    'nazwa' => $nazwa,
                    'opis' => $opis
                );
				if(isset($img)) $editArray['img'] = $img;
                $sliderTypy = new Slidertypy();
                $db = $sliderTypy->edytuj($editArray, $id);
                //$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie </div>';
            } 
			else 
			{
                $this->view->blad_edycji = $error['err'];
            }
        }
        $print = $wybrana->pojedyncza($id);
        $this->view->dane = $print;

        $this->view->editor = $this->fcKeditor('opis', @stripslashes($print->opis), 400);
		
		$this->view->lista_bledow = '';
		$this->view->typ = $typ = 'galeria';
		if($this->obConfig->podstronyGaleria)
		{
			$galeria = new Galeria();
			$this->view->lista_bledow .= $this->common->galeriaSupport($id, $typ, $this->_request);
			$imgEdit = intval($this->_request->getParam('imgEdit', 0));
			if($imgEdit > 0) $this->view->img = $galeria->pobierzNazwe($imgEdit);
			$this->view->galeria = $galeria->wyswietlGalerie($id, 'desc', 0, $typ);
		}
		if($this->obConfig->podstronyVideo)
		{
			$video = new Video();
			$this->view->lista_bledow .= $this->common->videoSupport($id, $typ, $this->_request);
			$vidEdit = $video->id = intval($this->_request->getParam('vidEdit', 0));
			if($vidEdit > 0) $this->view->video = $video->wypiszPojedynczego();
			$this->view->videos = $video->wypisz(false, $id, $typ);
		}
		//var_dump($this->view->blad_edycji);var_dump($this->view->lista_bledow);
		$this->view->link = 'admin/galeria/edytuj/id/'.$id;
		if($this->_request->isPost())
		if(@empty($this->view->blad_edycji))
		if(@empty($this->view->lista_bledow))
		$this->_redirect($this->view->link);
		
		$this->view->r_zapisz = 'zapisz';
		$this->view->r_dodaj = 'admin/galeria/dodaj';
		$this->view->r_usun = 'admin/galeria/lista/delid/'.$id;
		$this->view->r_lista = 'admin/galeria/lista';
		//$this->view->r_pokaz = $this->common->makeLink($this->common->lang('galeria')).'/nr/'.$id;
    }
	
	function galeriaAction()
	{
		$slider = new Slider();
		$this->view->gal = $gal = $this->_request->getParam('id', 0);
		
		$wybrana = new Slidertypy();
		$print = $wybrana->pojedyncza($gal);
        $this->view->dane = $print;
		
		if($this->obConfig->galeriaRodzaje)
		{
			$this->view->r_dodaj = 'admin/galeria/dodaj';
			$this->view->r_lista = 'admin/galeria/lista';
		}
		
		$del = $this->_request->getParam('delid', 0);
		if($del > 0)
		{
			$slider->id = $del;
			$slider->usun();
			//$this->view->blad_edycji = '<div class="k_ok">Wybrana slider została usunięta.</div>';
			$this->_redirect('/admin/galeria/galeria/id/'.$gal);
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/galeria/galeria/');
		}
		$glowny = intval($this->_request->getParam('glowny', 0));
		if($glowny > 0)
		{
			$slider->id = $glowny;
			$slider->ustawGlowne($glowny, 'galeria', $gal);
			//$this->view->blad_edycji = '<div class="k_ok">Wybrana slider została usunięta.</div>';
			$this->_redirect('/admin/galeria/galeria/id/'.$gal);
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/galeria/galeria/');
		}
		$nieaktywny = $this->_request->getParam('inactive', 0);
		if($nieaktywny)
		{
			$slider->id = $nieaktywny;
			$dane = array('aktywny' => 0);
			$slider->edytuj($dane);
			$this->_redirect('/admin/galeria/galeria/id/'.$gal);
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/galeria/galeria/');
		}
		$aktywny = $this->_request->getParam('active', 0);
		if($aktywny)
		{
			$slider->id = $aktywny;
			$dane = array('aktywny' => 1);
			$slider->edytuj($dane);
			$this->_redirect('/admin/galeria/galeria/id/'.$gal);
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/galeria/galeria/');
		}
		$up = $this->_request->getParam('up');
		if($up)
		{
			$prev = $this->_request->getParam('prev', 0);
			$slider->id = $up;
			$upSlajd = $slider->wypiszJeden();
			$slider->id = $prev;
			$prevSlajd = $slider->wypiszJeden();
			$dane = array('kolejnosc' => $upSlajd['kolejnosc']);
			$slider->edytuj($dane);
			$slider->id = $up;
			$dane = array('kolejnosc' => $prevSlajd['kolejnosc']);
			$slider->edytuj($dane);
			$this->_redirect('/admin/galeria/galeria/id/'.$gal);
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/galeria/galeria/');
		}
		$down = $this->_request->getParam('down');
		if($down)
		{
			$next = $this->_request->getParam('next', 0);
			$slider->id = $down;
			$downSlajd = $slider->wypiszJeden();
			$slider->id = $next;
			$nextSlajd = $slider->wypiszJeden();
			$dane = array('kolejnosc' => $downSlajd['kolejnosc']);
			$slider->edytuj($dane);
			$slider->id = $down;
			$dane = array('kolejnosc' => $nextSlajd['kolejnosc']);
			$slider->edytuj($dane);
			$this->_redirect('/admin/galeria/galeria/id/'.$gal);
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/galeria/galeria/');
		}
		if($this->_request->getPost('slideLinkiZapisz'))
		{
			$linki = $this->_request->getPost('linki');
			$opisy = $this->_request->getPost('opisy');
			$czasy = $this->_request->getPost('czasy');
			$aktywny = $this->_request->getPost('aktywny');
			foreach($opisy as $id => $link)
			{
				$slider = new Slider();
				$slider->id = $id;
				$active = isset($aktywny[$id]) ? 1 : 0;
				$dane['link'] = isset($linki[$id]) ? $linki[$id] : "";
				$dane['opis'] = isset($opisy[$id]) ? $opisy[$id] : "";
				$dane['czas'] = isset($czasy[$id]) ? $czasy[$id] : 5;
				$dane['aktywny'] = $active;
				$slider->edytuj($dane);
			}
		}
		if($this->_request->getPost('slideZapisz'))
		{
			//var_dump($_FILES);
			$link = $this->_request->getPost('link', '');
			$opis = $this->_request->getPost('opis', '');
			for($i = 0; $i < count($_FILES['img']['tmp_name']); $i++)
			{
				//set_time_limit(60);
				
				$array = array
				(
					'name' => $_FILES['img']['name'][$i], 
					'type' => $_FILES['img']['type'][$i],
					'tmp_name' => $_FILES['img']['tmp_name'][$i],
					'error' => $_FILES['img']['error'][$i],
					'size' => $_FILES['img']['size'][$i]
				);
				
				$handle = @new upload($array);
							
				if ($handle->uploaded) 
				{				
					$handle->file_new_name_body = 'galeria';
					$handle->image_resize = true;
					$handle->image_x = $this->ImageDir->GaleriaX;
					$handle->image_y = $this->ImageDir->GaleriaY;
					//$this->ImageDir->szerokosc;
					$handle->image_ratio_y = true;
					$handle->process($this->ImageDir->Galeria);

					if ($handle->processed) 
					{
						$handle->clean();
						
						$dane['img'] = $handle->file_dst_name;
						$dane['aktywny'] = 1;
						$dane['link'] = $link;
						$dane['opis'] = $opis;
						$dane['czas'] = 5;
						$dane['kolejnosc'] = $slider->maxKolejnosc('galeria') + 1;
						$dane['typ'] = 'galeria';
						$dane['rodzaj'] = $gal;
						$slider->dodaj($dane);

						//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
					}
					else 
					{
						$this->view->blad_edycji = '<div class="k_blad">Upload nieudany. Nie można przenieść zdjęćia na serwer.</div>';
					}
				}
			}
						
			//$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/galeria/galeria/');
		}
		$this->view->slajdy = $slider->wypisz('galeria', $gal)->toArray();

	}
}
?>