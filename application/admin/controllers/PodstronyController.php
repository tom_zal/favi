<?php
class Admin_PodstronyController extends Ogolny_Controller_Admin 
{
    public function init() 
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
		$this->view->position = $this->common->position;
		$this->view->polozenie = $this->common->polozenie;
    }
    function __call($method, $args) 
	{
        $this->_redirect('/admin');
    }
	
    function edytujAction() 
	{
        $this->view->blad_edycji ='';
        $this->view->brak_tematu ='';
		
		$this->view->minLevel = $this->minLevel = 1;
		$this->view->maxLevel = $this->maxLevel = 1;

        $akcja = new Podstrony();
        if($this->_request->isPost()) 
		{
            $filter = new Zend_Filter_StripTags();
            $temat = trim($filter->filter($this->_request->getPost('temat')));
			$temat_color = $this->_request->getPost('temat_color', '000000');
            $tekst = $this->_request->getPost('tekst', NULL);
            $link = $filter->filter($this->_request->getPost('link'));
            $id = (int)$filter->filter($this->_request->getPost('id'));
            $short = $this->_request->getPost('short', NULL);
			$position = trim($this->_request->getPost('typ', 'menu'));
			$data = $this->_request->getPost('data', date('Y-m-d H:i:s'));
			$rodzic = $filter->filter($this->_request->getPost('rodzic', 0));
			$parent = $filter->filter($this->_request->getPost('parent', ''));
			$kategs = $this->_request->getPost('kategs', NULL);
			$polozenie = trim($this->_request->getPost('polozenie', NULL));//'topbottom'
			
			$error = array();
			if(empty($temat)) $error[] = 'Pole temat jest wymagane';
			if($parent == 'kategorie')
			{
				$minLevel = @intval($kategs[$this->minLevel]);
				$rodzic = count($kategs) > 0 ? @intval($kategs[count($kategs)-1]) : 0;
				if($minLevel <= 0 || $rodzic <= 0)
				$error[] = 'Wybierz kategorię na poziomie '.$this->minLevel;
			}
			//var_dump($error);die();
            if(empty($error))
			{
                $dane = array
				(
                    'temat' => $temat,
					'temat_color' => $temat_color,
                    'tekst' => $tekst,
                    'short' => $short,
                    'link' => $link,
					'rodzic' => $rodzic,
					'parent' => $parent,
					'data' => $data
                );
                $akcja->id = $id;
				//var_dump($dane);//die();
				//echo $id;return;
				if($id > 0)
				{
					//if(strpos($dane['link'], "-blok") === false)
					$dane['polozenie'] = $position == 'menu' ? $polozenie : null;
					$nowa = $akcja->setPodstrona($dane);
				}
				if($id == 0)
				{
					$dane['lp'] = null;
					$dane['tytul'] = $dane['temat'];
					$dane['link'] = $dane['temat'];
					if(false && $this->obConfig->jezyki)
					$dane['link'] .= '-'.$this->lang;
					$dane['position'] = $position;
					$dane['polozenie'] = $position == 'menu' ? $polozenie : null;
					$dane['lang'] = $this->lang;
					//var_dump($dane);die();
					if($position == 'blok') $nowa = $akcja->insertBlok($dane);
					else if($position == 'allegro') $nowa = $akcja->insertBlok($dane);
					else if($position == 'lojalnosc') $nowa = $akcja->insertBlok($dane);
					else if($position == 'menu') $nowa = $akcja->insertPodstrona($dane);
				}
				if($nowa['id'] == 0)
				{
					$this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>';
					$this->view->lista_bledow = '<div id="lista_bledow">Istnieje już taka podstrona</div>';
				}
				else if($id == 0) $this->_redirect('/admin/podstrony/edytuj/strona/'.$nowa['link']);
				//$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.			'/admin/podstrony/edytuj/strona/'.$nowa['link']);				
                //$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
            }
            else 
			{
                $this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>';
				$this->view->lista_bledow = '<div id="lista_bledow">';
				foreach($error as $blad) $this->view->lista_bledow .= $blad.'<br/>';
				$this->view->lista_bledow.= '</div>';
            }
        }

        $strona = trim($this->_request->getParam('strona', ''));
		if(@!empty($nowa['link']) && $strona != $nowa['link'])
		$this->_redirect('/admin/podstrony/edytuj/strona/'.$nowa['link']);
		if(@!empty($nowa['link'])) $strona = $nowa['link'];
		$position = trim($this->_request->getParam('typ', 'menu'));
		if($strona == '')
		{
			$zawartosc = array();
			$zawartosc['position'] = $position;
			$zawartosc['polozenie'] = $position == 'menu' ? 'topbottom' : '';
			$zawartosc['temat'] = $position == 'blok' ? 'Nowy blok' : 'Nowa podstrona';
			$zawartosc['temat_color'] = '000000';
			$zawartosc['id'] = 0;			
			$zawartosc['link'] = '';
			$zawartosc['rodzic'] = 0;
			$zawartosc['parent'] = '';
			$zawartosc['data'] = date('Y-m-d H:i:s');
			$this->view->zawartosc = $zawartosc;
			$this->view->tytul = '';
			$this->view->editor = $this->fcKeditor('tekst', '', 500);
			$this->view->seditor = null;//$this->fcKeditor('short', '', 300);
		}
		else
		{
			$akcja->link = $strona;
			$this->view->zawartosc = $akcja->getPodstrona($this->lang);
			//var_dump($this->view->zawartosc);die();
			$id = $this->view->zawartosc['id'];
			$position = $this->view->zawartosc['position'];
			$this->view->tytul = $this->view->zawartosc['temat'];
			if(isset($this->view->zawartosc['tekst']) && $this->view->zawartosc['tekst'] !== NULL)
			$this->view->editor = $this->fcKeditor('tekst', stripcslashes($this->view->zawartosc['tekst']), 500);
			if(isset($this->view->zawartosc['short']) && $this->view->zawartosc['short'] !== NULL)
			$this->view->seditor = $this->fcKeditor('short', stripcslashes($this->view->zawartosc['short']), 300);

			if($position == 'menu')
			{
				$this->view->typ = $typ = 'podstrony';
				if($this->obConfig->podstronyGaleria)
				{
					$galeria = new Galeria();
					$this->view->blad_edycji = $this->common->galeriaSupport($id, $typ, $this->_request);
					$imgEdit = intval($this->_request->getParam('imgEdit', 0));
					if($imgEdit > 0) $this->view->img = $galeria->pobierzNazwe($imgEdit);
					$this->view->galeria = $galeria->wyswietlGalerie($id, 'desc', 0, $typ);
				}
				if($this->obConfig->podstronyVideo)
				{
					$video = new Video();
					$this->view->blad_edycji = $this->common->videoSupport($id, $typ, $this->_request);
					$vidEdit = $video->id = intval($this->_request->getParam('vidEdit', 0));
					if($vidEdit > 0) $this->view->video = $video->wypiszPojedynczego();
					$this->view->videos = $video->wypisz(false, $id, $typ);
				}
				$this->view->link = 'admin/podstrony/edytuj/strona/'.$strona;
				if($this->_request->isPost())
				if(@empty($this->view->error))
				if(@empty($this->view->blad_edycji))
				$this->_redirect($this->view->link);
			}
		}
		
		$lista = '';
		if($position == 'menu') $lista = 'podstrony';
		if($position == 'blok') $lista = 'bloki';
		if($position == 'allegro') $lista = 'allegro';
		//if($position == 'lojalnosc') $lista = 'lojalnosc';
		
		if(true)
		{			
			$ajax = new Kategorie('admin', 'kategorie');
			$this->view->glowne = $ajax->start();
			if($this->view->zawartosc['rodzic'] > 0 && $this->view->zawartosc['parent'] == 'kategorie')
			{
				$path = $ajax->get_path($this->view->zawartosc['rodzic']);
				foreach($path as $k => $kat)
				{
					$this->view->{'kateg'.$k} = $kat['id'];
					$this->view->{'kategs'.$k} = $ajax->showWybranyRodzic($kat['rodzic']);
				}
				$this->view->kateg = @$this->view->kateg0;
				//var_dump($this->view);die();
			}
		}
		
		$this->view->r_zapisz = 'zapisz';
		if($this->adminNazwa == 'bigcom')
		$this->view->r_dodaj = 'admin/podstrony/edytuj/typ/'.$position;
		if($this->adminNazwa == 'bigcom')
		if(!empty($strona) && !empty($lista))
		$this->view->r_usun = 'admin/podstrony/lista'.$lista.'/delid/'.$strona;
		if(!empty($lista))
		$this->view->r_lista = 'admin/podstrony/lista'.$lista;
		if(!empty($strona) && $position == 'menu')
		$this->view->r_pokaz = $this->view->zawartosc['link'];
    }
	
	function listablokiAction() 
	{
		$link = $this->_request->getParam('delid');
		if($link)
		{
			$podstrony = new Podstrony();
			$podstrony->link = $link;
			$podstrony->deleteBlok();
			$this->_redirect('/admin/podstrony/listabloki');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/podstrony/listabloki/');
		}
	}
	function listaallegroAction() 
	{
		$link = $this->_request->getParam('delid');
		if($link)
		{
			$podstrony = new Podstrony();
			$podstrony->link = $link;
			$podstrony->deleteBlok();
			$this->_redirect('/admin/podstrony/listaallegro');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/podstrony/listaallegro/');
		}
	}
	function listalojalnoscAction() 
	{
		$link = $this->_request->getParam('delid');
		if($link)
		{
			$podstrony = new Podstrony();
			$podstrony->link = $link;
			$podstrony->deleteBlok();
			$this->_redirect('/admin/podstrony/listaallegro');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/podstrony/listaallegro/');
		}
	}

	function listapodstronyAction() 
	{
		$link = $this->_request->getParam('delid');
		if($link)
		{
			$podstrony = new Podstrony();
			$podstrony->link = $link;
			$podstrony->deletePodstrona();
			$this->_redirect('/admin/podstrony/listapodstrony');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/podstrony/listapodstrony/');
		}
		$powiel = $this->_request->getParam('powiel');
		if($powiel)
		{
			$podstrony = new Podstrony();
			$podstrony->link = $powiel;
			$daneOrg = $podstrony->getPodstrona()->toArray();
			unset($daneOrg['id']);
			if(@count($this->langs) > 0)
			foreach($this->langs as $lg => $lang)
			{
				if($lg == $this->lang) continue;
				$dane = $daneOrg;
				$dane['lang'] = $lg;
				$dane['link'] = str_replace('-'.$this->lang, '-'.$lg, $dane['link']);
				//$dane['tytul'] = $this->common->lang($dane['link'], $lg);
				//$dane['temat'] = $this->common->lang($dane['link'], $lg);
				//$dane['link'] = $this->common->makeLink($dane['tytul']);
				//var_dump($dane);//die();
				if($daneOrg['position'] == 'menu')
				$id = $podstrony->insertPodstrona($dane);
				else $id = $podstrony->insertBlok($dane);
				//if($id == 0) continue;
			}
			//die();
			$this->_redirect('/admin/podstrony/listapodstrony');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/podstrony/listapodstrony/');
		}
	}

	function dodajpodstronyAction() 
	{
		
	}
}
?>