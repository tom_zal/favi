<?php
class Admin_AtrybutyController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();

		$this->status = array(array('nazwa'=>'aktywny', 'id'=>1), array('nazwa'=>'nieaktywny', 'id'=>0));
		$this->view->status = $this->status;
		$Object = new Atrybuty();
		$this->view->typatr = $this->typatr = $Object->typatr;
		$this->view->typpola = $this->typpola = $Object->typpola;
		$this->view->opcjeWybor = $this->opcjeWybor = $Object->opcjeWybor;
	}
	function __call($method, $args)
	{
        $this->_redirect('/admin/index/panel/');
	}	
	function dodajAction()
	{
		$Object = new Atrybutygrupy();
		$Object->cache = $this->obConfig->cache;
		$Object->cachename = 'atrybuty_grupy';
		$Object->lang = $this->lang;
		$ObjectA = new Atrybuty();
		
		$del = intval($this->_request->getParam('del', 0));
		if($del > 0)
		{
			$ObjectR = new Atrybutyrodzaje();
			$ObjectR->id = $del;
			$ObjectR->_delete();
			$Object->_updateRodzaj($del, 0);
			$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/'.$this->_action);
		}
		
		if($this->_request->getPost('nazwa'))
		{
			$post['nazwa'] = $this->_request->getPost('nazwa');
			if(!empty($post['nazwa']))
			{
				$ObjectR = new Atrybutyrodzaje();
				if(false) $ObjectR->_update($post);
				else $ObjectR->_save($post);
				$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/'.$this->_action);
				//$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/'.$this->_action);
			}
			else
			{
				$this->view->blad_edycji = '<div class="k_blad">1. Pole nazwa jest wymagane.<br></div>';
				$this->view->nazwa = $post['nazwa'];
			}
		}

		if($this->_request->getPost('post'))
		{
			$post = $this->_request->getPost('post');
			$post['lang'] = $this->lang;
			$kategorieall = $this->_request->getPost('kategorieall');
			$kategorie = $this->_request->getPost('kategorie');
			if(!empty($post['nazwa']))
			{				
				if(strlen($kategorieall) >= 0) $post['id_kat'] = "";
				if(!empty($kategorie) && empty($kategorieall))
				$post['id_kat'] = implode(';', $kategorie);
				
				$grupa = $Object->_save($post);
				$noweOpcje = $this->_request->getPost('opcje');
				if($noweOpcje)
				{
					//var_dump($noweOpcje);//die();
					if(count($noweOpcje) > 0)
					foreach($noweOpcje as $nowaOpcja)
					if(!empty($nowaOpcja))
					{
						$nowaOpcjaData['id_gr'] = $grupa;
						$nowaOpcjaData['id_prod'] = 0;
						$nowaOpcjaData['nazwa'] = $nowaOpcja;
						//var_dump($nowaOpcjaData);
						$nowaOpcjaID = $ObjectA->_save($nowaOpcjaData);
					}
					//die();
				}
				$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/');
				$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/');
			}
			else
			{
				$this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>
				<div id="lista_bledow">1. Pole nazwa jest wymagane.<br></div>';
				$this->view->temp = $post;
			}
		}
        
		if($this->obConfig->atrybutyKategorie)
		{
			$kategorie = new Kategorie();
			$kategorie->typ = 'kategorie';
			$kategorie->getAllWithDetails(-1);
			$kat = $kategorie->display_children_admin_select(0,0);
			$this->view->kategorie = $kategorie->input;
			//print_r($kat);
		}
		if($this->obConfig->atrybutyRodzaje)
		{
			$rows = $Object->getRows();
			$this->view->rows = $this->common->sortByPole($rows, 'id_rodzaj', 'id');
			//var_dump($this->view->rows);die();
			$Object = new Atrybutyrodzaje();
			$rodzaje = $Object->getRows();
			$this->view->rodzaje = $this->common->sortByPole($rodzaje, 'id');
		}
		else $this->view->rows = $Object->getRows();
	}
	
	function edytujAction()
	{
		$id = $this->_request->getParam('id');
		$Object = new Atrybutygrupy();
		$Object->id = $id;
		$Object->cache = $this->cache;
		$Object->cachename = 'atrybuty_grupy';
		$Object->lang = $this->lang;
		$ObjectA = new Atrybuty();
		$ObjectA->id = $id;
		
		$this->view->r_zapisz = 'jQuery_zapisz_btn';
		$this->view->r_dodaj = 'admin/atrybuty/dodaj/';
		$this->view->r_usun = 'admin/atrybuty/usun/id/'.$id;
		$this->view->r_lista = 'admin/atrybuty/dodaj/';

		$this->view->temp = $Object->getRow();
		$this->view->opcje = $ObjectA->getRowsGroup();

		if($this->_request->isPost())
		{
			$post = $this->_request->getPost('post');
			$post['lang'] = $this->lang;
			$kategorieall = $this->_request->getPost('kategorieall');
			$kategorie = $this->_request->getPost('kategorie');
			if(strlen($kategorieall) >= 0) $post['id_kat'] = "";
			if(!empty($kategorie) && empty($kategorieall))
			$post['id_kat'] = implode(';', $kategorie);
			
			if(!empty($post['nazwa']))
			{
				$o = 1;
				$Object->_update($post);
				$stareOpcje = $this->_request->getPost('stareOpcje');
				if($stareOpcje)
				{					
					//var_dump($noweOpcje);//die();
					if(count($stareOpcje) > 0)
					foreach($stareOpcje as $opcjaID => $staraOpcja)
					if(!empty($staraOpcja))
					{
						$ObjectA->id = $opcjaID;
						$staraOpcjaData['kolejnosc'] = $o++;
						$staraOpcjaData['nazwa'] = $staraOpcja;
						//var_dump($staraOpcjaData);
						$ObjectA->_update($staraOpcjaData);
					}
					else
					{
						$ObjectA->id = $opcjaID;
						$ObjectA->_delete();
					}
					//die();
				}
				$noweOpcje = $this->_request->getPost('noweOpcje');
				if($noweOpcje)
				{
					//var_dump($noweOpcje);//die();
					if(count($noweOpcje) > 0)
					foreach($noweOpcje as $nowaOpcja)
					if(!empty($nowaOpcja))
					{
						$nowaOpcjaData['kolejnosc'] = $o++;
						$nowaOpcjaData['id_gr'] = $id;
						$nowaOpcjaData['id_prod'] = 0;
						$nowaOpcjaData['nazwa'] = $nowaOpcja;
						//var_dump($nowaOpcjaData);
						$nowaOpcjaID = $ObjectA->_save($nowaOpcjaData);
					}
					//die();
				}
				$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/id/'.$id);
				$this->getResponse()->setHeader('Refresh','1; URL='.$this->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/id/'.$id);
			}
			else
			{
				$this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>
				<div id="lista_bledow">1. Pole nazwa jest wymagane.<br></div>';
				$this->view->temp = $post;
			}
		}
		if($this->obConfig->atrybutyKategorie)
		{
			$kategorie = new Kategorie();
			$kategorie->typ = 'kategorie';
			$kategorie->getAllWithDetails(-1);
			$kat = $kategorie->display_children_admin_select(0,0,0,$this->view->temp->id_kat);
			$this->view->kategorie = $kategorie->input;
			//var_dump($kategorie->input);die();
		}
		if($this->obConfig->atrybutyRodzaje)
		{
			$Object = new Atrybutyrodzaje();
			$rodzaje = $Object->getRows();
			$this->view->rodzaje = $this->common->sortByPole($rodzaje, 'id');
		}
	}

	function usunAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		(int) $id = $this->_request->getParam('id', 0);
		$Objectg = new Atrybutygrupy();
		$Objectg->id = $id;
		$Objectg->cache = $this->cache;
		$Objectg->cachename = 'atrybuty_grupy';
		$Objectg->lang = $this->lang;
		$Objectg->_delete();
		
		$Objecta = new Atrybuty();
		$Objecta->id = $id;
		$Objecta->cache = $this->cache;
		$Objecta->cachename = 'atrybuty_atrybuty';
		$Objecta->lang = $this->lang;
		$Objecta->_deleteGroup();
		
		$ObjectAp = new Atrybutypowiazania();
		$ObjectAp->id = $id;
		$ObjectAp->cache = $this->cache;
		$ObjectAp->cachename = 'atrybuty_powiazania';
		$ObjectAp->lang = $this->lang;
		$ObjectAp->_deleteGroup();
		
		$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/dodaj');
		$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/dodaj/');
	}

	function atrybutyAction()
	{
		$id = $this->_request->getParam('id');
		$Objectg = new Atrybutygrupy();
		$Objectg->id = $id;
		$Objecta = new Atrybuty();
		$Objecta->id = $id;
		$Objecta->cache = $this->cache;
		$Objecta->cachename = 'atrybuty_atrybuty';
		$Objecta->lang = $this->lang;

		if(!empty($id))
		{
			$this->view->htmlg = $Objectg->getRow();
			$this->view->htmla = $Objecta->getRowsGroup();

			if($this->_request->isPost())
			{
				$post = $this->_request->getPost('post');
				$post['lang'] = $this->lang;
				if(!empty($post['nazwa']))
				{
					$Objecta->_save($post);
					$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
					$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/id/'.$id);
					$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/id/'.$id);
				}
				else
				{
					$this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>
					<div id="lista_bledow">1. Pole nazwa jest wymagane.<br></div>';
					$this->view->temp = $post;
				}
			}
		}
		else
		{
			$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/dodaj/');
		}
	}

	function edytujatrAction()
	{
		(int) $id = $this->_request->getParam('id', 0);
		(int) $idgr = $this->_request->getParam('idgr', 0);
		$Objectg = new Atrybutygrupy();
		$Objectg->id = $idgr;
		$Objecta = new Atrybuty();
		$Objecta->id = $id;
		$Objecta->cache = $this->cache;
		$Objecta->cachename = 'atrybuty_atrybuty';
		$Objecta->lang = $this->lang;

		if(!empty($id) && !empty($idgr))
		{
			$this->view->htmlg = $Objectg->getRow();
			$this->view->temp = $Objecta->getRow();

			if($this->_request->isPost())
			{
				$post = $this->_request->getPost('post');
				$post['lang'] = $this->lang;

				$Objecta->_update($post);
				$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/idgr/'.$idgr.'/id/'.$id);
				$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/idgr/'.$idgr.'/id/'.$id);
			}
		}		
	}

	function usunatrAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		(int) $id = $this->_request->getParam('id', 0);
		(int) $idgr = $this->_request->getParam('idgr', 0);
		
		$Objecta = new Atrybuty();
		$Objecta->id = $id;
		$Objecta->cache = $this->cache;
		$Objecta->cachename = 'atrybuty_atrybuty';
		$Objecta->lang = $this->lang;
		$Objecta->_delete();
		
		$ObjectAp = new Atrybutypowiazania();
		$ObjectAp->id = $id;
		$ObjectAp->cache = $this->cache;
		$ObjectAp->cachename = 'atrybuty_powiazania';
		$ObjectAp->lang = $this->lang;
		$ObjectAp->_deleteAttr();
		
		$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/atrybuty/id/'.$idgr);
		$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/atrybuty/id/'.$idgr);
	}
	
	function rodzajeAction()
	{
		$id = intval($this->_request->getParam('id', 0));
		$Object = new Atrybutyrodzaje();
		$Object->id = $id;
		
		//$this->view->r_zapisz = 'jQuery_zapisz_btn';
		if($id > 0)
		{
			$this->view->r_dodaj = 'admin/atrybuty/rodzaje/';
			$this->view->r_usun = 'admin/atrybuty/rodzaje/del/'.$id;
		}
		$this->view->r_lista = 'admin/atrybuty/dodaj/';
		
		$del = intval($this->_request->getParam('del', 0));
		if($del > 0)
		{
			$Object->id = $del;
			$Object->_delete();
			$ObjectG = new Atrybutygrupy();
			$ObjectG->_updateRodzaj($del, 0);
			$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/id/'.$id);
		}
		
		if($this->_request->isPost())
		{
			$post = $this->_request->getPost('post');
			$post['lang'] = $this->lang;
			if(!empty($post['nazwa']))
			{
				if($id > 0) $Object->_update($post);
				else $Object->_save($post);
				$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/id/'.$id);
				//$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/id/'.$id);
			}
			else
			{
				$this->view->blad_edycji = '<div class="k_blad">1. Pole nazwa jest wymagane.<br></div>';
				$this->view->temp = $post;
			}
		}
		
		$this->view->temp = $Object->getRow();
		$this->view->rodzaje = $Object->getRows();
	}
}
?>