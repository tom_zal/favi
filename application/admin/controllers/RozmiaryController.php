<?php
class Admin_RozmiaryController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		$this->view->pokazDruga = false;
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}	
	function dodajAction()
	{
		$typ = $this->_request->getParam('typ', 1);
		$rozmiarowki = new Rozmiarowka();
		$rozmiarowki->id = $typ;
		$rozmiarowka = $rozmiarowki->wypiszPojedynczego();
		
		$kolory = new Rozmiary();		
		$del = $this->_request->getParam('delid', 0);
		if($del!=0)
		{
			$kolory->id = $del;
			$kolory->usunKolory();
			//$this->view->blad_edycji = '<div class="k_ok">Wybrany rozmiar został usunięty.</div>';
			$this->_redirect('/admin/rozmiary/dodaj/typ/'.$typ.'/');
			$this->getResponse()->setHeader('Refresh','1; URL='.$this->view->baseUrl.'/admin/rozmiary/dodaj/typ/'.$typ);
		}
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa');
			$wkladka = $this->_request->getPost('wkladka', '');
			$dane = array('typ' => $typ, 'nazwa' => $nazwa, 'wkladka' => $wkladka);
			$kolory->dodajKolory($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/rozmiary/dodaj/typ/'.$typ.'/');
			$this->getResponse()->setHeader('Refresh','1; URL='.$this->view->baseUrl.'/admin/rozmiary/dodaj/typ/'.$typ);
		}
			
		$this->view->kolory = $kolory->wypiszKolory($typ);
		$this->view->typ = $typ;
		$this->view->typNazwa = $rozmiarowka['nazwa'];
	}	
	function edytujAction()
	{
		$typ = $this->_request->getParam('typ', 1);
		$rozmiarowki = new Rozmiarowka();
		$rozmiarowki->id = $typ;
		$rozmiarowka = $rozmiarowki->wypiszPojedynczego();
		
		$kolory = new Rozmiary();
		if($this->_request->isPost())
		{
			$id = $this->_request->getPost('id');
			$nazwa = $this->_request->getPost('nazwa');
			$wkladka = $this->_request->getPost('wkladka', '');
			$dane = array('nazwa' => $nazwa, 'wkladka' => $wkladka);
			$kolory->id = $id;
			$kolory->edytujKolory($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/rozmiary/dodaj/typ/'.$typ.'/');
			$this->getResponse()->setHeader('Refresh','1; URL='.$this->view->baseUrl.'/admin/rozmiary/dodaj/typ/'.$typ);
		}
		$id = $this->_request->getParam('id', 0);
		
		$kolory->id = $id;
		$this->view->typ = $typ;
		$this->view->typNazwa = $rozmiarowka['nazwa'];
		$this->view->kolory = $kolory->wypiszKolory($typ);
		$this->view->pojedynczy = $kolory->wypiszKolor();
		$this->view->tytul = $this->view->pojedynczy->nazwa;
	}
}
?>