<?php
class Admin_RabatyprogiController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}	
	function dodajAction()
	{
		$id = $this->_request->getParam('id', 0);
		$rabaty = new Rabatyprogi();
		$rabaty->id = $id;
		$this->view->pojedynczy = $rabaty->wypiszID();
		
		$del = $this->_request->getParam('delid', 0);
		if($del!=0)
		{
			$rabaty->id = $del;
			$rabaty->usun();
			//$this->view->blad_edycji = '<div class="k_ok">Wybrany rozmiar został usunięty.</div>';
			$this->_redirect('/admin/rabatyprogi/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/rabatyprogi/dodaj');
		}
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa', '');
			$od = $this->_request->getPost('od', 0);
			$do = $this->_request->getPost('do', 0);
			$koszt = $this->_request->getPost('koszt', 0);
			$typ = $this->_request->getPost('typ', 'zamowienie');
			$dane = array('nazwa' => $nazwa, 'od' => $od, 'do' => $do, 'koszt' => $koszt, 'typ' => $typ);
			$rabaty->dodaj($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/rabatyprogi/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/rabatyprogi/dodaj');
		}
		
		$this->view->rabaty = $rabaty->wypisz();
	}	
	function edytujAction()
	{
		$id = $this->_request->getParam('id', 0);
		$rabaty = new Rabatyprogi();
		$rabaty->id = $id;
		$this->view->pojedynczy = $rabaty->wypiszID();
		
		if($this->_request->isPost())
		{
			$id = $this->_request->getPost('id', 0);
			$nazwa = $this->_request->getPost('nazwa', '');
			$od = $this->_request->getPost('od', 0);
			$do = $this->_request->getPost('do', 0);
			$koszt = $this->_request->getPost('koszt', 0);
			$typ = @!empty($this->view->pojedynczy['typ']) ? $this->view->pojedynczy['typ'] : 'zamowienie';
			$dane = array('nazwa' => $nazwa, 'od' => $od, 'do' => $do, 'koszt' => $koszt, 'typ' => $typ);
			$rabaty->id = $id;
			$rabaty->edytuj($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/rabatyprogi/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/rabatyprogi/dodaj');
		}
		
		$this->view->rabaty = $rabaty->wypisz();
	}
}
?>