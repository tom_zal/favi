<?php
class Admin_AtrybutyController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();

		$this->status = array(array('nazwa'=>'aktywny', 'id'=>1), array('nazwa'=>'nieaktywny', 'id'=>0));
		$this->view->status = $this->status; 
		
		$Object = new Atrybuty();
		$this->view->typatr = $this->typatr = $Object->typatr;
		$this->view->typpola = $this->typpola = $Object->typpola;
	}
	function __call($method, $args)
	{
        $this->_redirect('/admin/index/panel/');
	}	
	function dodajAction()
	{
		$Object = new Atrybutygrupy();
		$Object->cache = $this->obConfig->cache;
		$Object->cachename = 'atrybuty_grupy';
		$Object->lang = $this->lang;

		if($this->_request->isPost())
		{
			$post = $this->_request->getPost('post');
			$post['lang'] = $this->lang;

			if(!empty($post['nazwa']))
			{
				$Object->_save($post);
				$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/');
				$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/');
			}
			else
			{
				$this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>
				<div id="lista_bledow">1. Pole nazwa jest wymagane.<br></div>';
				$this->view->temp = $post;
			}
		}

		$this->view->rows = $Object->getRows();
	}
	
	function edytujAction()
	{
		$id = $this->_request->getParam('id');
		$Object = new Atrybutygrupy();
		$Object->id = $id;
		$Object->cache = $this->cache;
		$Object->cachename = 'atrybuty_grupy';
		$Object->lang = $this->lang;

		$this->view->temp = $Object->getRow();

		if($this->_request->isPost())
		{
			$post = $this->_request->getPost('post');
			$post['lang'] = $this->lang;

			if(!empty($post['nazwa']))
			{
				$Object->_update($post);
				$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/id/'.$id);
				$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/id/'.$id);
			}
			else
			{
				$this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>
				<div id="lista_bledow">1. Pole nazwa jest wymagane.<br></div>';
				$this->view->temp = $post;
			}
		}
	}

	function usunAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		(int) $id = $this->_request->getParam('id', 0);
		$Objectg = new Atrybutygrupy();
		$Objectg->id = $id;
		$Objectg->cache = $this->cache;
		$Objectg->cachename = 'atrybuty_grupy';
		$Objectg->lang = $this->lang;
		$Objectg->_delete();
		
		$Objecta = new Atrybuty();
		$Objecta->id = $id;
		$Objecta->cache = $this->cache;
		$Objecta->cachename = 'atrybuty_atrybuty';
		$Objecta->lang = $this->lang;
		$Objecta->_deleteGroup();
		
		$ObjectAp = new Atrybutypowiazania();
		$ObjectAp->id = $id;
		$ObjectAp->cache = $this->cache;
		$ObjectAp->cachename = 'atrybuty_powiazania';
		$ObjectAp->lang = $this->lang;
		$ObjectAp->_deleteGroup();
		
		$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/dodaj');
		$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/dodaj/');
	}

	function atrybutyAction()
	{
		$id = $this->_request->getParam('id');
		$Objectg = new Atrybutygrupy();
		$Objectg->id = $id;
		$Objecta = new Atrybuty();
		$Objecta->id = $id;
		$Objecta->cache = $this->cache;
		$Objecta->cachename = 'atrybuty_atrybuty';
		$Objecta->lang = $this->lang;

		if(!empty($id))
		{
			$this->view->htmlg = $Objectg->getRow();
			$this->view->htmla = $Objecta->getRowsGroup();

			if($this->_request->isPost())
			{
				$post = $this->_request->getPost('post');
				$post['lang'] = $this->lang;
				if(!empty($post['nazwa']))
				{
					$Objecta->_save($post);
					$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
					$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/id/'.$id);
					$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/id/'.$id);
				}
				else
				{
					$this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>
					<div id="lista_bledow">1. Pole nazwa jest wymagane.<br></div>';
					$this->view->temp = $post;
				}
			}
		}
		else
		{
			$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/dodaj/');
		}
	}

	function edytujatrAction()
	{
		(int) $id = $this->_request->getParam('id', 0);
		(int) $idgr = $this->_request->getParam('idgr', 0);
		$Objectg = new Atrybutygrupy();
		$Objectg->id = $idgr;
		$Objecta = new Atrybuty();
		$Objecta->id = $id;
		$Objecta->cache = $this->cache;
		$Objecta->cachename = 'atrybuty_atrybuty';
		$Objecta->lang = $this->lang;

		if(!empty($id) && !empty($idgr))
		{
			$this->view->htmlg = $Objectg->getRow();
			$this->view->temp = $Objecta->getRow();

			if($this->_request->isPost())
			{
				$post = $this->_request->getPost('post');
				$post['lang'] = $this->lang;

				$Objecta->_update($post);
				$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/idgr/'.$idgr.'/id/'.$id);
				$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/'.$this->_action.'/idgr/'.$idgr.'/id/'.$id);
			}
		}		
	}

	function usunatrAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		(int) $id = $this->_request->getParam('id', 0);
		(int) $idgr = $this->_request->getParam('idgr', 0);
		
		$Objecta = new Atrybuty();
		$Objecta->id = $id;
		$Objecta->cache = $this->cache;
		$Objecta->cachename = 'atrybuty_atrybuty';
		$Objecta->lang = $this->lang;
		$Objecta->_delete();
		
		$ObjectAp = new Atrybutypowiazania();
		$ObjectAp->id = $id;
		$ObjectAp->cache = $this->cache;
		$ObjectAp->cachename = 'atrybuty_powiazania';
		$ObjectAp->lang = $this->lang;
		$ObjectAp->_deleteAttr();
		
		$this->_redirect('/'.$this->_module.'/'.$this->_controller.'/atrybuty/id/'.$idgr);
		$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/'.$this->_module.'/'.$this->_controller.'/atrybuty/id/'.$idgr);
	}
}
?>