<?php
class Admin_PoradyController extends Ogolny_Controller_Admin 
{
    public function init() 
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
    }
	
    function __call($method, $args) 
	{
        $this->_redirect('/admin/');
    }
	
    public function dodajAction() 
	{
		$this->view->r_zapisz = 'zapisz';
		$this->view->r_lista = 'admin/aktualnosci/lista/';
		
        $this->view->editor = $this->fcKeditor('tekst', "", 400);
        $this->view->seditor = $this->fcKeditor('short', "", 300);
        $this->view->rodzaj = $this->_request->getParam('typ');
        if($this->_request->isPost()) 
		{
            $filter = new Zend_Filter_StripTags();
            $nazwa = $filter->filter($this->_request->getPost('temat'));
            $skrot = $this->_request->getPost('short','');
            $tekst = $this->_request->getPost('tekst');
            $wyswietl = $filter->filter($this->_request->getPost('wyswietl', 1));
            $glowna = $filter->filter($this->_request->getPost('glowna', 1));
            $typ = $filter->filter($this->_request->getPost('typ', 1));

            if(empty($nazwa)) 
			{
                $error['err'] = '<div class="k_blad">Pole temat jest wymagane</div>';
            }
			
			if(isset($_FILES['img']) && !empty($_FILES['img']['tmp_name']))
			{
				$handle = @new upload($_FILES['img']);
				
				if($handle->uploaded) 
				{
					$handle->file_new_name_body = 'porady';
					$handle->image_resize = false;
					$handle->image_x = $this->ImageDir->PoradyX;
					$handle->image_y = $this->ImageDir->PoradyY;
					$handle->image_ratio_x = true;
					$handle->process($this->ImageDir->Porady);
					
					if($handle->processed) 
					{
						$handle->clean();
						$img = $handle->file_dst_name;
					}
					else 
					{
						$error['err'] = '<div class="k_blad">Upload nieudany. Nie można utworzyć miniaturki.</div>';
					}
				}
				else 
				{
					$error['err'] = '<div class="k_blad">Upload nieudany. Nie można przenieść zdjęcia na serwer.</div>';
				}
			}

            if(empty($error)) 
			{
                $date = date("Y-m-d H:i:s");
                $addArray = array
				(
                    'typ' => $typ, 
					'temat' => $nazwa,
                    'skrot' => $skrot,
                    'tekst' => $tekst,
                    'wyswietl' => $wyswietl,
                    'data' => $date,
                    'status' => 0,
                    'glowna' => $glowna
                );
				if(isset($img)) $addArray['img'] = $img;
                $nowaAktualnosc = new Porady();
                $id = $nowaAktualnosc->dodaj($addArray, $this->lang);
                $this->_redirect("admin/porady/edytuj/id/" . $id);
                //$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegla pomyslnie</div>';
            } 
			else 
			{
                $this->view->blad_edycji = $error['err'];
            }
        }
    }

    public function listaAction() 
	{
		$this->view->r_dodaj = 'admin/porady/dodaj';
		
        $wszystkie = new Porady();
        $rodzaj = $this->_request->getParam('typ');
        if($this->_request->getParam('delid') > 0) 
		{
            $filter = new Zend_Filter_StripTags();
            $id = $filter->filter($this->_request->getParam('delid'));

            $db = $wszystkie->getAdapter();
            $where = $db->quoteInto('id = ?', $id);
            $rows_affected = $wszystkie->delete($where);
        }
		if(($pokazid = $this->_request->getParam('pokazid')) > 0)
		{
			$widoczny = @intval($this->_request->getParam('widoczny')?1:0);
            $wszystkie->edytuj(array('wyswietl' => $widoczny), $pokazid);
			$this->_redirect('admin/porady/lista');
        }
		if(($glownaid = $this->_request->getParam('glownaid')) > 0)
		{
			$glowna = @intval($this->_request->getParam('glowna')?1:0);
            $wszystkie->edytuj(array('glowna' => $glowna), $glownaid);
			$this->_redirect('admin/porady/lista');
        }

        $all = $wszystkie->wypiszAktualnosciAdmin($this->lang);
        $this->view->realizacje = $all;
    }

    public function edytujAction() 
	{
        $wybrana = new Porady();

        $filter = new Zend_Filter_StripTags;
        $id = $filter->filter($this->_request->getParam('id'));
        $this->view->rodzaj = $filter->filter($this->_request->getParam('typ'));

        if($this->_request->isPost()) 
		{
            $filter = new Zend_Filter_StripTags();
            $nazwa = $filter->filter($this->_request->getPost('temat'));
            $skrot = $this->_request->getPost('short','');
            $tekst = $this->_request->getPost('tekst');
            $wyswietl = $filter->filter($this->_request->getPost('wyswietl', 1));
            $glowna = $filter->filter($this->_request->getPost('glowna', 0));
            $typ = $filter->filter($this->_request->getPost('typ', 1));
			$data = $this->_request->getPost('data', date('Y-m-d H:i:s'));
            
			if(empty($nazwa)) 
			{
                $error['err'] = '<div class="k_blad">Pole temat jest wymagane</div>';
            }
			
			if($this->obConfig->poradyZdjecia)
			if(isset($_FILES['img']) && !empty($_FILES['img']['tmp_name']))
			{
				$handle = @new upload($_FILES['img']);				
				if($handle->uploaded) 
				{
					$handle->file_new_name_body = 'porady';
					$handle->image_resize = false;
					$handle->image_x = $this->ImageDir->PoradyX;
					$handle->image_y = $this->ImageDir->PoradyY;
					$handle->image_ratio_x = true;
					$handle->process($this->ImageDir->Porady);					
					if($handle->processed) 
					{
						$handle->clean();
						$img = $handle->file_dst_name;
					}
					else 
					{
						$error['err'] = '<div class="k_blad">Upload nieudany. Nie można utworzyć miniaturki.</div>';
					}
				}
				else 
				{
					$error['err'] = '<div class="k_blad">Upload nieudany. Nie można przenieść zdjęcia na serwer.</div>';
				}
			}

            if(empty($error)) 
			{
                $editArray = array
				(
                    'typ' => $typ,
                    'temat' => $nazwa,
                    'skrot' => $skrot,
                    'tekst' => $tekst,
                    'wyswietl' => $wyswietl,
                    'glowna' => $glowna,
					'data' => $data
                );
				if(isset($img)) $editArray['img'] = $img;
                $edycjaAktualnosc = new Porady();
                $db = $edycjaAktualnosc->edytuj($editArray, $id);
                //$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie </div>';
            } 
			else 
			{
                $this->view->blad_edycji = $error['err'];
            }
        }
        $print = $wybrana->pojedyncza($id);
        $this->view->dane = $print;

        $this->view->editor = $this->fcKeditor('tekst', stripslashes($print->tekst), 400);
        $this->view->seditor = $this->fcKeditor('short', stripslashes($print->skrot), 300);
		
		$this->view->lista_bledow = '';
		$this->view->typ = $typ = 'porady';
		if($this->obConfig->podstronyGaleria)
		{
			$galeria = new Galeria();
			$this->view->lista_bledow .= $this->common->galeriaSupport($id, $typ, $this->_request);
			$imgEdit = intval($this->_request->getParam('imgEdit', 0));
			if($imgEdit > 0) $this->view->img = $galeria->pobierzNazwe($imgEdit);
			$this->view->galeria = $galeria->wyswietlGalerie($id, 'desc', 0, $typ);
		}
		if($this->obConfig->podstronyVideo)
		{
			$video = new Video();
			$this->view->lista_bledow .= $this->common->videoSupport($id, $typ, $this->_request);
			$vidEdit = $video->id = intval($this->_request->getParam('vidEdit', 0));
			if($vidEdit > 0) $this->view->video = $video->wypiszPojedynczego();
			$this->view->videos = $video->wypisz(false, $id, $typ);
		}
		//var_dump($this->view->blad_edycji);var_dump($this->view->lista_bledow);
		$this->view->link = 'admin/porady/edytuj/id/'.$id;
		if($this->_request->isPost())
		if(@empty($this->view->blad_edycji))
		if(@empty($this->view->lista_bledow))
		$this->_redirect($this->view->link);
		
		$this->view->r_zapisz = 'zapisz';
		$this->view->r_dodaj = 'admin/porady/dodaj';
		$this->view->r_usun = 'admin/porady/lista/delid/'.$id;
		$this->view->r_lista = 'admin/porady/lista';
    }
}
?>