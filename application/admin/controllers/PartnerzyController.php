<?php
class Admin_PartnerzyController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}	
	function dodajAction()
	{
		$partnerzy = new Partnerzy();
		$this->view->typ = $typ = $this->_request->getParam('typ', 'partnerzy');
		$this->view->type = $type = $this->common->makeLink($typ, false);
		$del = $this->_request->getParam('delid', 0);
		if($del!=0)
		{
			$partnerzy->id = $del;
			$partnerzy->usun();
			//$this->view->blad_edycji = '<div class="k_ok">Wybrany kolor został usunięty.</div>';
			$this->_redirect('/admin/partnerzy/dodaj/typ/'.$typ);
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/partnerzy/dodaj/typ/'.$typ);
		}
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa');
			$link = $this->_request->getPost('link');
			$opis = $this->_request->getPost('opis', '');
			
			if(isset($_FILES['logo']) && !empty($_FILES['logo']['tmp_name']))
			{	
				$handle = @new upload($_FILES['logo']);
				if ($handle->uploaded) 
				{
					$handle->file_new_name_body = $typ;
					$handle->image_resize = true;
					$handle->image_x = $this->ImageDir->PartnerzyX;
					$handle->image_y = $this->ImageDir->PartnerzyY;
					$handle->image_ratio_x = true;
					$handle->process($this->ImageDir->Partnerzy);
					
					if ($handle->processed) 
					{			
						$logo = $handle->file_dst_name;
						if($this->ImageDir->PartnerzyGreyScale)
						{
							$handle->file_new_name_body = $handle->file_dst_name_body;
							$handle->image_resize = true;
							$handle->image_x = $this->ImageDir->PartnerzyX;
							$handle->image_y = $this->ImageDir->PartnerzyY;
							$handle->image_ratio_x = true;
							$handle->image_greyscale = true;
							$handle->process($this->ImageDir->Partnerzy.'/greyscale');
							if($handle->processed) $handle->clean();
						}
						else $handle->clean();
					}
					else 
					{
						$this->view->blad_edycji = '<div class="k_blad">Upload nieudany. Nie można utworzyć miniaturki.</div>';
					}
				}
				else 
				{
					$this->view->blad_edycji = '<div class="k_blad">Upload nieudany. Nie można przenieść zdjęcia na serwer.</div>';
				}			
			}
			
			$dane['typ'] = $typ;
			$dane['nazwa'] = $nazwa;
			$dane['link'] = $link;
			$dane['opis'] = $opis;
			if(isset($logo)) $dane['logo'] = $logo;
			
			$partnerzy->dodaj($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/partnerzy/dodaj/typ/'.$typ);
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/partnerzy/dodaj/typ/'.$typ);
		}
			
		$this->view->partnerzy = $partnerzy->wypiszAll($typ);
	}
	
	function edytujAction()
	{
		$partnerzy = new Partnerzy();
		$this->view->typ = $typ = $this->_request->getParam('typ', 'partnerzy');
		$this->view->type = $type = $this->common->makeLink($typ, false);
		if($this->_request->isPost())
		{
			$id = $this->_request->getPost('id');
			$nazwa = $this->_request->getPost('nazwa');
			$link = $this->_request->getPost('link');
			$opis = $this->_request->getPost('opis', '');
			
			if(isset($_FILES['logo']) && !empty($_FILES['logo']['tmp_name']))
			{		
				$handle = @new upload($_FILES['logo']);
				
				if ($handle->uploaded) 
				{
					$handle->file_new_name_body = $typ;
					$handle->image_resize = true;
					$handle->image_x = $this->ImageDir->PartnerzyX;
					$handle->image_y = $this->ImageDir->PartnerzyY;
					$handle->image_ratio_x = true;
					$handle->process($this->ImageDir->Partnerzy);
					
					if ($handle->processed) 
					{
						$logo = $handle->file_dst_name;
						if($this->ImageDir->PartnerzyGreyScale)
						{
							$handle->file_new_name_body = $handle->file_dst_name_body;
							$handle->image_resize = true;
							$handle->image_x = $this->ImageDir->PartnerzyX;
							$handle->image_y = $this->ImageDir->PartnerzyY;
							$handle->image_ratio_x = true;
							$handle->image_greyscale = true;
							$handle->process($this->ImageDir->Partnerzy.'/greyscale');
							if($handle->processed) $handle->clean();
						}
						else $handle->clean();
					}
					else 
					{
						$this->view->blad_edycji = '<div class="k_blad">Upload nieudany. Nie można utworzyć miniaturki.</div>';
					}
				}
				else 
				{
					$this->view->blad_edycji = '<div class="k_blad">Upload nieudany. Nie można przenieść zdjęcia na serwer.</div>';
				}			
			}
			
			$dane['nazwa'] = $nazwa;
			$dane['link'] = $link;
			$dane['opis'] = $opis;
			if(isset($logo)) $dane['logo'] = $logo;
			
			$partnerzy->id = $id;
			$partnerzy->edytuj($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/partnerzy/dodaj/typ/'.$typ);
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/partnerzy/dodaj/typ/'.$typ);
			//$this->_redirect($this->view->baseUrl.'/admin/partnerzy/dodaj/typ/'.$typ);
		}
		$id = $this->_request->getParam('id', 0);
		
		$this->view->r_zapisz = 'jQuery_zapisz_btn';
		$this->view->r_dodaj = 'admin/partnerzy/dodaj/typ/'.$typ;
		$this->view->r_usun = 'admin/partnerzy/dodaj/typ/'.$typ.'/delid/'.$id;
		$this->view->r_lista = 'admin/partnerzy/dodaj/typ/'.$typ;
		
		$partnerzy->id = $id;
		$this->view->partnerzy = $partnerzy->wypiszAll($typ);
		$this->view->pojedynczy = $partnerzy->wypisz();
		$this->view->tytul = $this->view->pojedynczy->nazwa;
	}
}
?>