<?php
class Admin_RozmiarowkaController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		//echo $this->view->baseUrl;
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}	
	function dodajAction()
	{
		$rozmiarowka = new Rozmiarowka();
		
		$del = $this->_request->getParam('delid', 0);
		if($del!=0)
		{
			$rozmiarowka->id = $del;
			$rozmiarowka->usunRozmiar();
			//$this->view->blad_edycji = '<div class="k_ok">Wybrana rozmiarowka została usunięta.</div>';
			$this->_redirect('/admin/rozmiarowka/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/rozmiarowka/dodaj/');
		}
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa');
			$jedn_miary = $this->_request->getPost('jedn_miary');
			$jedn_miary_typ = $this->_request->getPost('jedn_miary_typ');
			$dane = array('nazwa' => $nazwa, 'jedn_miary' => $jedn_miary, 'jedn_miary_typ' => $jedn_miary_typ);
			$rozmiarowka->dodajRozmiar($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/rozmiarowka/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/rozmiarowka/dodaj/');
		}
			
		$this->view->rozmiarowki = $rozmiarowka->wypiszRozmiar();
	}	
	function edytujAction()
	{
		$rozmiarowka = new Rozmiarowka();
		if($this->_request->isPost())
		{
			$id = $this->_request->getPost('id');
			$nazwa = $this->_request->getPost('nazwa');
			$jedn_miary = $this->_request->getPost('jedn_miary');
			$jedn_miary_typ = $this->_request->getPost('jedn_miary_typ');
			$dane = array('nazwa' => $nazwa, 'jedn_miary' => $jedn_miary, 'jedn_miary_typ' => $jedn_miary_typ);
			$rozmiarowka->id = $id;
			$rozmiarowka->edytujRozmiar($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/rozmiarowka/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/rozmiarowka/dodaj/');
		}
		$id = $this->_request->getParam('id', 0);
		
		$rozmiarowka->id = $id;
		$this->view->rozmiarowki = $rozmiarowka->wypiszRozmiar();
		$this->view->pojedynczy = $rozmiarowka->wypiszPojedynczego();
		$this->view->tytul = $this->view->pojedynczy->nazwa;
	}
}
?>