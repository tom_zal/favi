<?php
class Admin_WiadomosciController extends Ogolny_Controller_Admin
{	
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();				
	}
    function __call($method, $args)
	{
        $this->_redirect('/admin/');
    }
	public function zmienAction()
	{		
		$wybrany = new Wiadomosci();		
		$wybrany->id = null;
		$wybrany->tekst = null;
		$wybrany->temat = null;
		
		$wybierz = $this->_request->getPost('edytuj');
		
		if(isset($wybierz))
		{
			$this->edytuj();
		}
		 
		$this->view->list = $wybrany->wypisz();
		if($this->_request->getPost('id') > 0)
		{
			$where = 'id = '.$this->_request->getPost('id');		
			$wybrany = $wybrany->fetchRow($where);
			$this->view->pliki = @unserialize($wybrany['zalaczniki']);
			$this->view->r_zapisz = 'zapisz';
		}
		
		$this->view->wybrany = $wybrany;		
		$this->view->editor = $this->fcKeditor('pole', stripslashes($wybrany->tekst));		
	}
	
	public function edytuj()
	{
		$edycja = new Wiadomosci();
		$valid = new Zend_Validate();
		$mainValid = $valid->addValidator(new Zend_Validate_NotEmpty());
		$err = 0;
		if($mainValid->isValid($this->_request->getPost('temat')))
		{
			$temat = $this->_request->getPost('temat');
		}
		else $err = 1;
		if($mainValid->isValid($this->_request->getPost('pole')))
		{
			$tekst = $this->_request->getPost('pole');
		}
		else $err = 1;
		
		$id = $this->_request->getPost('id');
		if(!$err)
		{
			$zalaczniki = @serialize($this->_request->getPost('pliki', ''));
			$array = array('temat'=>$temat, 'tekst'=>$tekst, 'zalaczniki'=>$zalaczniki);
			$where = 'id = '.$id;			
			$edycja->update($array,$where);			
			//$this->view->error = '<div class="k_ok"> Edycja przebiegła pomyślnie </div>';
		}
		else
		{
			$this->view->error = '<div class="k_blad"> Wszystkie pola są wymagane </div>';
		}
	}
}
?>