<?php
class Admin_IndexController extends Ogolny_Controller_Admin
{
    public function init()
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
		$this->_initloginredirect = $this->baseUrl.'/admin/lojalnosc/drzewo/klient/0';
        //$this->_initloginredirect = $this->baseUrl.'/admin/lojalnosc/dodajzakup/';
    }

    function indexAction()
	{
        $this->view->error_logowania = '';
        if ($this->_request->isPost())
		{
            $akcja = new Login();

            $filter = new Zend_Filter_StripTags();

            $login = trim($filter->filter($this->_request->getPost('nick')));
            $pass = trim($filter->filter($this->_request->getPost('pass')));

            $pass = md5($pass);

            $dane = array('login' => $login, 'pass' => $pass);

			$user = $akcja->zaloguj($dane);
            if ($user != null)
			{
                session_regenerate_id();
                $info = new Zend_Session_Namespace('admin');
                $info->zalogowany = 'ok';
				$info->id = $user['id'];
				$info->login = $login;
				$info->nazwa = $user['nazwa'];
				$info->prawa = $user['uprawnienia'];
                $info->adres_ip = $_SERVER['REMOTE_ADDR'];
				$logowanie = new KontrahenciLogin();
				$logowanie->dodaj(array('id_kontr' => $user['id'], 'typ' => 'login'));
				if($pass == md5('admin'))
				{
					$info->zmianaHasla = true;
					$this->_redirect('/admin/index/zmienhaslo/zakladka/3');
				}
				$mail = new Confmail;
				$dane = $mail->showData();
				if(empty($dane[0]->FromName) || empty($dane[0]->From))
				$this->_redirect('/admin/confmail/edytuj/zakladka/4/');
				
				if(@!empty($user['last_link']))
				{
					$page = new Zend_Session_Namespace('zakladka');
					$page->wybrana = $this->obConfig->sklep ? 2 : 1;
					$akcja->id = $user['id'];
					$akcja->edytuj(array('last_link' => ''));
					$this->_redirect($user['last_link']);
				}
                
				if($this->obConfig->sklep)
				$this->_redirect('/admin/index/panel/zakladka/2/');
				else 
				$this->_redirect('/admin/index/panel/zakladka/1/');
            }
			else
			{
                $this->view->blad_edycji = '<div class="k_blad">Nieprawidłowy login lub hasło</div>';
                $this->view->error_logowania = '';
            }
        }
    }
	function altindexAction()
    {
        if($this->_request->isPost())
        {
            $filter = new Zend_Filter_StripTags();
            $login = trim($filter->filter($this->_request->getPost('nick')));
            $pass = trim($filter->filter($this->_request->getPost('pass')));
            
            Zend_Session::regenerateId();
            if(!empty($login))
            {
                Zend_Session::rememberMe(259200);
                $db = Zend_Db_Table::getDefaultAdapter();
                try
                {
                    $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'Login', 'login', 'haslo');
                    $authAdapter->setIdentity($login);
                    $authAdapter->setCredential(md5($pass));
                    $authAdapter->setCredentialTreatment("1");

                    $result = $authAdapter->authenticate();
                    if($result->isValid())
                    {
                        $stdClass2 = $authAdapter->getResultRowObject();
                        $stdClass2->role = 'admin';

                        $storage = $this->auth->getStorage();
                        $storage->write($stdClass2);
                        $this->_redirect($this->_initloginredirect);
                    }
                    else
                    {
                        $this->view->blad_edycji = '<div class="k_blad">Błędny login lub hasło.</div>';
                    }
                }
                catch(Exception $e)
                {
                    $this->view->blad_edycji = '<div class="k_blad">Nieudana próba logowania.</div>';
                }
            }
            else
            {
                $this->view->blad_edycji = '<div class="k_blad">Nieudana próba logowania.</div>';
            }
        }
		elseif(!empty($this->user) && isset($this->user->role) && in_array($this->user->role,$this->_inituserrole))
		$this->_redirect($this->_initloginredirect);        
    }
	
    function panelAction()
	{
		$info = new Zend_Session_Namespace('admin');
		if(isset($info->zalogowany))
		{
			$zakladka = $this->_request->getParam('zakladka', 0);
			unset($_SESSION['lojalnosc']);
			
			switch($zakladka):
				case 1: $this->pageStart();
				break;
				case 2: $this->pageStart();//shopStart();		
				break;
				default: $this->pageStart();//startStart();
			endswitch;			
		}		
    }

    function wylogujAction()
	{
        $info = new Zend_Session_Namespace('admin');
        unset($info->zalogowany);
		$zakl = new Zend_Session_Namespace('zakladka');
		unset($page->wybrana);
		Zend_Session::destroy(true, true);
        $this->_redirect('/admin/index/');
		Zend_Session::rememberMe(0);
        $this->auth->clearIdentity();
        return $this->_redirect($this->view->baseUrl.'/admin/index/');
    }

    function zmienhasloAction()
	{
        $this->view->rozne = '';
        $this->view->puste = '';
		
		$this->view->r_zapisz = 'zapisz';

        if ($this->_request->isPost())
		{
            $filter = new Zend_Filter_StripTags();

            $new_pass_1 = trim($filter->filter($this->_request->getPost('new_pass_1')));
            $new_pass_2 = trim($filter->filter($this->_request->getPost('new_pass_2')));
            $old_pass = trim($filter->filter($this->_request->getPost('old_pass')));
            if ($new_pass_1 != $new_pass_2)
			{
                $this->view->rozne = '<div class="k_blad">Podałeś dwa rózne nowe hasła<br></div>';
            }
            if ($new_pass_1 == '' || $new_pass_2 == '')
			{
                $this->view->puste = '<div class="k_blad">Hasło nie może być puste<br></div>';
            }
            if ($this->view->rozne == '' && $this->view->puste == '')
			{
                $akcja = new Login();
                $wynik = $akcja->ZmienHaslo($new_pass_1, $new_pass_2, $old_pass, $this->view->adminNazwa);
                if ($wynik == 0)
				{
                    $this->view->haslo = '<div class="k_blad">Podałeś nieprawidłowe stare hasło.</div>';
                } 
				if ($wynik == -1)
				{
                    $this->view->haslo = '<div class="k_blad">Podałeś to samo hasło.</div>';
                } 
				if ($wynik == -2)
				{
                    $this->view->haslo = '<div class="k_blad">Hasłem nie może być słowo "admin".</div>';
                }
				elseif ($wynik == 1)
				{
                    $this->view->haslo = '<div class="k_ok">Hasło zostało zmienione.</div>';
					$zakladka = $this->_request->getParam('zakladka', 0);
					if($zakladka == 3)
					{
						$mail = new Confmail;
						$dane = $mail->showData();
						if(empty($dane[0]->FromName) || empty($dane[0]->From))
						$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/confmail/edytuj/zakladka/4/');
						else 
						{
							if($this->obConfig->sklep)
							$this->_redirect('/admin/index/panel/zakladka/2/');
							else 
							$this->_redirect('/admin/index/panel/zakladka/1/');
						}
					}
                }
            }
        }
    }
	
	function shopStart()
	{		
		$this->_redirect('/admin/oferta/lista/od/0');
		$this->_redirect('/admin/lojalnosc/drzewo/klient/0');
		
		$all = new Zamowienia();
        $sort = array('data asc');
		$mode = 'all';
		$this->view->mode = $mode;
		$this->view->all = $all->selectAll('all', $sort);		
	}
	
	function pageStart()
	{
		
	}
	
	function startStart()
	{
		/***STARTOWY EKRAN***/
	}	
	
	function edytujAction()
	{
		if(!$this->obConfig->uzytkownicy || ($this->adminNazwa != 'admin' && $this->adminNazwa != 'bigcom'))
		$this->_redirect('/admin/index/');//panel/zakladka/1/');
		
		$users = new Login();
		$userDel = (int) $this->_request->getParam('userDel', 0);            
		if ($userDel > 0)
		{
			$user = new Login();
			$user->id = $userDel;
			$userDel = $user->wypiszPojedynczego();
			if(@$userDel['login'] != 'admin' && @$userDel['login'] != 'bigcom')
			$user->usun();
			//$this->view->blad_edycji = '<div class="k_ok">Zdjęcie zostało usunięte.</div>';
			$this->_redirect('/admin/index/edytuj/');
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/index/edytuj/');
		}
		$dodajUser = $this->_request->getPost('dodajUser');				
		if ($dodajUser)
		{
			$dane['nazwa'] = $this->_request->getPost('userNazwa');
			$dane['login'] = $this->_request->getPost('userLogin');
			$dane['haslo'] = md5($this->_request->getPost('userHaslo'));
			$dane['uprawnienia'] = @implode(',', $this->_request->getPost('userPrawa'));
			//var_dump($_POST);
			if(empty($dane['nazwa']) || empty($dane['login']) || empty($dane['haslo']))
			{
				$this->view->blad_edycji = 'Wpisz wszystkie dane!';
			}
			else
			{
				$user = new Login();
				$user->dodaj($dane);
				//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/admin/index/edytuj/');
				$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/index/edytuj/');
			}
		}
		$edycjaUser = $this->_request->getPost('edycjaUser');				
		if ($edycjaUser)
		{
			$dane['nazwa'] = $this->_request->getPost('userNazwa');
			if($this->_request->getPost('userLogin'))
			{
				$dane['login'] = $this->_request->getPost('userLogin');
				//$dane['haslo'] = md5($this->_request->getPost('userHaslo'));
				$dane['uprawnienia'] = @implode(',', $this->_request->getPost('userPrawa'));
			}
			//var_dump($_POST);
			if(empty($dane['nazwa']) || ($this->_request->getPost('userLogin') && empty($dane['login'])))
			{
				$this->view->blad_edycji = 'Wpisz wszystkie dane!';
			}
			else
			{
				$user = new Login();
				$user->id = $this->_request->getPost('userID');
				$user->edytuj($dane);
				//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/admin/index/edytuj/');
				$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/index/edytuj/');
			}
		}
		$id = $this->_request->getParam('id', 0);
		if($id > 0)
		{
			$users->id = $id;
			$this->view->user = $users->wypiszPojedynczego();
		}
		$this->view->users = $users->wypisz();
		$this->view->tytul = 'Użytkownicy';
	}
	
	function ustawieniaAction()
	{
		$ustawienia = $this->_request->getPost('ustawienia');
		if($ustawienia)
		{
			$dane = $this->_request->getPost('ustaw');
			if(false)
			{
				$ceny = '';
				foreach($this->ceny as $nazwa => $label)
				$ceny .= $this->_request->getPost($nazwa, '') == 'on' ? $nazwa.';' : '';
				$dane['ceny'] = $ceny;
			}
			$settings = new Ustawienia();
			$settings->updateData($dane);
			$this->view->error = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/index/ustawienia/');
		}
		$settings = new Ustawienia();
		$ustawienia = $settings->showData()->toArray();
		$this->view->dane = $ustawienia[0];
	}
	
	function polecAction()
	{ 
	 	$id = $this->_request->getParam('nr', 0);
		
		$conf = new Confmail();
		$data = $conf->showData();
		$this->view->email = $data[0]['From'];
		
		if($id > 0)
		{
			$produkt = new Produkty();
			$produkt->id = $id;
			$prod = $produkt->wypiszPojedyncza();
			$this->view->link = 'http://'.$_SERVER['HTTP_HOST'].$this->view->baseUrl.'/'.$prod['link'];
			$gal = new Galeria();
			$this->view->galeria = $gal->wyswietlGalerie($id);
		}

		if($this->_request->getPost('polecZapytanie'))
		{
			$danePolec['link'] = $this->view->link;
			$danePolec['opis'] = $this->_request->getPost('opis');
			$danePolec['email'] = $this->_request->getPost('email');
			$danePolec['nazwa'] = $this->_request->getPost('nazwa');
			
			if(empty($danePolec['email']) || empty($danePolec['nazwa']))
			{
				$this->view->blad_edycji = 'Wypełnij wszystkie pola!';
			}
			else
			{
				$klienci = new Kontrahenci();
				$result = $klienci->wypisz();
				foreach($result as $row) $maile[] = $row['email'];
				
				$znajomy = new Znajomi();
				$result = $znajomy->wypiszZnajomych();
				foreach($result as $row) $maile[] = $row['mail'];
				
				if(isset($maile))
				foreach($maile as $mail)
				{
					$danePolec['znajomy'] = $mail;
					
					$wiadomosci = new Wiadomosci();
					$wiadomosci->polecZapytanie($danePolec, $this->view->galeria);
				}
				$this->view->lista_bledow = 'E-mail został wysłany!';
			}
		}		
	}
	
	function onlineAction()
	{	
		$edycjaSync = $this->_request->getPost('edycjaSync');
		if($edycjaSync)
		{
			$daneOnline['adres'] = $this->_request->getPost('adres');
			$daneOnline['serwer'] = $this->_request->getPost('serwer');
			$daneOnline['login'] = $this->_request->getPost('login');
			$daneOnline['haslo'] = $this->_request->getPost('haslo');
			$daneOnline['remote_path'] = $this->_request->getPost('remote_path');
			foreach($daneOnline as $dana)
			if(empty($dana))
			{
				$this->view->blad_edycji = 'Uzupełnij wszystkie pola!';
			}
			else
			{
				$synchr = new Serwer();
				$synchr->updateData($daneOnline);
			}
		}
		$synchr = new Serwer();
		$daneOnline = $synchr->showData()->toArray();
		$this->view->dane = $daneOnline[0];
		
		$sync = $this->_request->getParam('sync');
		if($sync)
		{
			$tab = $this->_request->getParam('sync');
			
			$server = $daneOnline[0]['adres'];
			
			$url = $server.'/ajax/index/synchronizuj/tab/'.$tab.'/';
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_CRLF, false);
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, false);
			curl_setopt($ch, CURLOPT_HEADER, false);
			//@curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
			//curl_setopt($ch, CURLOPT_HTTPGET, false);
			//curl_setopt($ch, CURLOPT_POST, true);
			//curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($ch, CURLOPT_TIMEOUT, 90);

			$result = curl_exec($ch);
			if(curl_errno($ch)) 
			{ 
				$error = true;
				$result = 'ERROR -> '.curl_errno($ch).' : '.curl_error($ch);
			}
			curl_close($ch);
			
			if(strpos($result, 'Fatal error') !== false) $error = true;
			
			//echo $result;
			if(!isset($error))
			{
				parse_str($result, $dane);
				//var_dump($dane);
				//$synchr = new Serwer();
				//$synchr->truncate($tab);
				//$synchr->insertAll($tab, $dane);
			}
			//echo count($dane);
			//var_dump($dane);
			//if(false)
			if(!isset($error))
			switch($tab)
			{
				case 'kategorie':
				$techn = new Kategorie();
				$producenci = $techn->wypiszKategorie();
				foreach($producenci as $prod)
				{
					$techn->id = $prod['id'];
					$techn->usunKategorie();
				}
				$route = new Routers();
				$route->usunKategorie();
				if(count($dane > 0))
				foreach($dane as $row)
				{
					$id = $techn->dodajKategorie($row);
					$route = new Routers();
					$sprawdz = $route->dodaj($row['link'], $id, 'default', 'oferta', 'kategorie');
					$techn->id = $id;
					$data['link'] = $sprawdz['link'];
					$data['route_id'] = $sprawdz['id'];
					$techn->edytuj($id, $data);
				}
				break;
				
				case 'katprod':
				$techn = new Katprod();
				$producenci = $techn->wypiszAll();
				foreach($producenci as $prod)
				{
					$techn->id = $prod['id_kat_prod'];
					$techn->delete($prod['id_kat_prod']);
				}
				foreach($dane as $row)
				$techn->add($row['id_kat'], $row['id_prod'], $row['id_kat_prod']);
				break;
				
				case 'produkt':
				$techn = new Produkty();
				$produkty = $techn->wypiszProdukty();
				foreach($produkty as $prod)
				{
					$techn->id = $prod['id'];
					$techn->usunProdukty();
				}
				$route = new Routers();
				$route->usunProdukty();
				foreach($dane as $row)
				{
					$id = $techn->dodajProdukty($row);
					$route = new Routers();
					$sprawdz = $route->dodaj($row['nazwa'].'-'.$id, $id);
					$techn->id = $id;
					$data['link'] = $sprawdz['link'];
					$data['route_id'] = $sprawdz['id'];
					$techn->edytujProdukty($data);
				}
				break;
				
				case 'galeria':
				$techn = new Galeria();
				$producenci = $techn->wypiszAll();
				foreach($producenci as $prod)
				{
					$techn->id = $prod['id'];
					$techn->usun();
				}
				foreach($dane as $row)
				$techn->dodaj($row);
				break;
				
				case 'rozmprod':
				$techn = new Rozmiarproduktu();
				$producenci = $techn->wypiszAll();
				foreach($producenci as $prod)
				{
					$techn->id = $prod['id'];
					$techn->usun();
				}
				foreach($dane as $row)
				$techn->dodaj($row);
				break;
				
				case 'kolprod':
				$techn = new Kolorproduktu();
				$producenci = $techn->wypiszAll();
				foreach($producenci as $prod)
				{
					$techn->id = $prod['id'];
					$techn->usun();
				}
				foreach($dane as $row)
				$techn->dodaj($row);
				break;
				
				case 'producent':
				$techn = new Producent();
				$producenci = $techn->wypiszProducentow();
				foreach($producenci as $prod)
				{
					$techn->id = $prod['id'];
					$techn->usunProducenta();
				}
				foreach($dane as $row)
				$techn->dodajProducenta($row);
				break;
				
				case 'kolory':
				$techn = new Kolory();
				$producenci = $techn->wypiszKolory();
				foreach($producenci as $prod)
				{
					$techn->id = $prod['id'];
					$techn->usunKolory();
				}
				foreach($dane as $row)
				$techn->dodajKolory($row);
				break;
				
				case 'rozmiary':
				$techn = new Rozmiary();
				$producenci = $techn->wypiszKolory();
				foreach($producenci as $prod)
				{
					$techn->id = $prod['id'];
					$techn->usunKolory();
				}
				foreach($dane as $row)
				$techn->dodajKolory($row);
				break;
			}
			
			if(!isset($error)) $this->view->lista_bledow = 'Zakończono synchronizację';
		}
		
		$syncImgs = $this->_request->getParam('syncimgs');
		if($syncImgs)
		{
			$ftp_server = $daneOnline[0]['serwer'];
			$ftp_user_name = $daneOnline[0]['login'];
			$ftp_user_pass = $daneOnline[0]['haslo'];
			$server = $daneOnline[0]['adres'];
			$local_path = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].$this->view->baseUrl);
			$remote_path = $daneOnline[0]['remote_path'];
			$paths = array('public/admin/zdjecia/miniaturki');
			$error = '';
			$message = '';
			$conn_id = ftp_connect($ftp_server);
			if($conn_id)
			{
				$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
				if($login_result)
				{
					ftp_pasv($conn_id, true);
					$i = 0;
					$ile = 0;
					$ileExist = 0;
					$ileNonExist = 0;
					$ileError = 0;
					//if(false)
					
					foreach($paths as $path)
					{
					$fullPath = str_replace('//', '/', $remote_path.'/'.$path);
					$imgsAll = ftp_nlist($conn_id, $fullPath);
					foreach($imgsAll as $imgPath)		
					{
						//if($i > 0) break;
						$i++;
						if($imgPath == '.' || $imgPath == '..') continue;
						set_time_limit(60);
						$img = substr($imgPath, strrpos($imgPath, '/') + 1);
						//$file = str_replace('_', '-', $img['img']);
						$local_file = $local_path.'/'.$path.'/'.$img;
						$remote_file = str_replace('//', '/', $remote_path.'/'.$path.'/'.$img);
						$remote_file_path = $server.str_replace('//', '/',  '/'.$path.'/').$img;
						//$error .= 'Plik offline '.$local_file.'<br/>';
						//$error .= 'Plik online '.$remote_file.'<br/>';
						//$error .= 'Plik online '.$remote_file_path.'<br/>';
						if(file_exists($local_file))
						{
							//$error .= 'Plik '.$local_file.' już istnieje<br/>';
							$ileExist++;
							continue;
						}
						$headers = @get_headers($remote_file_path);
						//var_dump($headers);
						if(!$headers || !preg_match('/200/', $headers[0]))
						{
							//$error .= 'Plik '.$remote_file_path.' nie istnieje<br/>';
							$ileNonExist++;
							continue;
						}
						if(false && !file_exists($remote_file_path))
						{
							//$error .= 'Plik '.$remote_file_path.' nie istnieje<br/>';
							$ileNonExist++;
							continue;
						}
						
						if(!ftp_get($conn_id, $local_file, $remote_file, FTP_BINARY))
						{
							//echo $remote_file.' '.$local_file;
							//$error .= 'Błąd ze skopiowaniem pliku '.$local_file.' dla produktu '.$nazwa.'<br/>';
							$ileError++;
						}
						else $ile++;
					}
					}
					//var_dump(ftp_nlist($conn_id, ftp_pwd($conn_id)));
					if($ileNonExist > 0)
					$message .= 'Pominięto '.$ileNonExist.' zdjęć nie istniejących online<br/>';
					if($ileExist > 0)
					$message .= 'Pominięto '.$ileExist.' zdjęć już istniejących offline<br/>';
					if($ileError > 0)
					$message .= 'Nie skopiowano '.$ileError.' zdjęć z powodu błędu kopiowania<br/>';
					$message .= 'Skopiowano '.$ile.' zdjęć<br/>';
				}
				else $error .= 'Błąd z zalogowaniem do FTP<br/>/';
				ftp_close($conn_id);
			}
			else $error .= 'Błąd z połączeniem z FTP<br/>/';
			
			$this->view->blad_edycji = $error;
			$this->view->lista_bledow = $message;
		}
	}
}
?>