<?php
class Admin_PocztapolskaController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
	}
    function __call($method, $args) 
	{
        $this->_redirect('/admin');
    }
	public function indexAction()
	{
		
	}
	
    function ksiazkanadawczaAction()
	{
		$list = new Zend_Session_Namespace('listPoczta');
		//var_dump($_POST);
		$data = $this->_request->getPost('data');
		if($data)
		{
			$data = $data['rok'].'-'.$data['mc'].'-'.$data['day'];
			if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $data)) $list->data = $data;
		}
		else $data = $this->_request->getParam('data');
		$dataDo = $this->_request->getPost('dataDo');
		if($dataDo)
		{
			$dataDo = $dataDo['rok'].'-'.$dataDo['mc'].'-'.$dataDo['day'];
			if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $dataDo)) $list->dataDo = $dataDo;
		}
		else $dataDo = $this->_request->getParam('dataDo');
		
		//if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $data)) $list->data = $data;
		//if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $dataDo)) $list->data = $dataDo;
		if(!isset($list->data)) $list->data = date('Y-m-d');
		if(!isset($list->dataDo)) $list->dataDo = date('Y-m-d');
		$data = $list->data;
		$dataDo = $list->dataDo;
		$this->view->data = $data;
		$this->view->dataDo = $dataDo;		
		
		$pocztapolska = new Pocztapolska();
		if(false)
		{
			$adminPoczta = $this->view->adminID;
			if(@in_array($adminPoczta, $this->pocztaPrzekierujAdminow))
			$adminPoczta = $this->pocztaPrzekierujDoAdmina;
		}
		$poczta = $pocztapolska->wypiszOne(1);
		$this->view->nadawca = 'Imię i nazwisko (nazwa) oraz adres nadawcy: ';
		$poczta['nadawcza_nagl1'] = str_replace(' r.', '&nbsp;r.', $poczta['nadawcza_nagl1']);
		$poczta['nadawcza_nagl2'] = str_replace(' r.', '&nbsp;r.', $poczta['nadawcza_nagl2']);
		$poczta['nadawcza_nagl1'] = str_replace('ul. ', 'ul.&nbsp;', $poczta['nadawcza_nagl1']);
		$poczta['nadawcza_nagl2'] = str_replace('ul. ', 'ul.&nbsp;', $poczta['nadawcza_nagl2']);
		$poczta['nadawcza_nagl1'] = str_replace('z dnia', 'z&nbsp;dnia', $poczta['nadawcza_nagl1']);
		$poczta['nadawcza_nagl2'] = str_replace('z dnia', 'z&nbsp;dnia', $poczta['nadawcza_nagl2']);
		$this->view->nadawca.= '<br/>'.$poczta['nadawcza_nagl1'].'<br/>'.$poczta['nadawcza_nagl2'].' ';		
		$this->view->naklad = $poczta['nadawcza_stopka'];
		
		$listy = new Listy();
		$allegro = new Allegroaukcje();
		$zakupy = new Zamowienia();
		//$users = new Allegrousers();
		
		if($this->_request->getParam('delid'))
		{
			$delid = intval($this->_request->getParam('delid'));
			//$buyer = intval($this->_request->getParam('buyer'));
			//$zakupy->edytujDeal($this->view->adminID, $buyer, $delid, array('wyslano' => 0));
			$listy->edytujZam(array('wyslano' => 0), $delid);
			$this->_redirect('admin/pocztapolska/ksiazkanadawcza');
		}
		
		$raport = $listy->wypiszRaport($this->view->adminID, $data, $dataDo);
		$this->view->dane = null;
		if(count($raport) > 0)
		{
			$raporty = null;
			foreach($raport as $rap)
			{
				$deal = $zakupy->selectWybranyKontrahent($rap['deal_id']);
				//print_r($deal);
                               
				if(count($deal) > 0)
				{
					$dane[] = $deal;
				}
				//$raporty[$buyer]['id'] = $id;
			}
			$this->view->dane = $dane;
		}
		//var_dump($raporty);
	}
	
	public function edytujAction()
	{
		$pocztapolska = new Pocztapolska();
		if($this->_request->getPost('poczta'))
		{
			$poczta = $this->_request->getPost('poczta');
			
                        if(empty($poczta['zwrot_niezwlocznie'])){
                            $poczta['zwrot_niezwlocznie']=0;
                        }
                        if(empty($poczta['zwrot_pozniej'])){
                            $poczta['zwrot_pozniej']=0;
                        }
                        if(empty($poczta['priorytetowa'])){
                            $poczta['priorytetowa']=0;
                        }
                        if(empty($poczta['poste'])){
                            $poczta['poste']=0;
                        }
                        if(empty($poczta['wartosc'])){
                            $poczta['wartosc']=0;
                        }
                        if(empty($poczta['czyrachunek'])){
                            $poczta['czyrachunek']=0;
                        }
                        if(empty($poczta['czyadres'])){
                            $poczta['czyadres']=0;
                        }
                        if(empty($poczta['ostroznie'])){
                            $poczta['ostroznie']=0;
                        }
                        if(empty($poczta['sprawdzenie'])){
                            $poczta['sprawdzenie']=0;
                        }
                        if(empty($poczta['potwierdzenie'])){
                            $poczta['potwierdzenie']=0;
                        }
                        foreach($poczta as $d => $dana)
			{
				 $poczta[$d] = htmlspecialchars($dana);
			}
                        
                        
			if(!isset($this->view->error))
			{
				$pocztapolska->edytujID($this->view->adminID, $poczta);
				$this->_redirect($this->URI);
			}
		}
		
		if(!isset($poczta)) $poczta = $pocztapolska->wypiszOne(1);
		
		$login = new Login();
		$login->id = $this->view->adminID;
		$logon = $login->wypiszPojedynczego();
		
		$arrLocales = array('pl_PL', 'pl', 'Polish_Poland.28592');
		setlocale(LC_ALL, $arrLocales);
		$date = iconv('ISO-8859-2', 'UTF-8',strftime('%d %B %Y'));
		
		if(empty($poczta['nadawcza_nagl1']))
		{
			$settings = new Ustawienia();
			$ustawienia = $settings->wypisz();
			$dane_firmy = @unserialize($ustawienia['dane_firmy']);
                
			$nagl1 = '';
			if(!empty($dane_firmy['nazwa_firmy'])) $nagl1.= $dane_firmy['nazwa_firmy'].' ';
			$nagl1.= $dane_firmy['imie'].' '.$dane_firmy['nazwisko'].', ';
			$nagl1.= 'ul. '.$dane_firmy['ulica'].' '.$dane_firmy['nr'].($dane_firmy['mieszkanie']?' '.$dane_firmy['mieszkanie']:'').', ';
			$nagl1.= $dane_firmy['kod'].' '.$dane_firmy['miasto'];
			$poczta['nadawcza_nagl1'] = $nagl1;
		}
		if(empty($poczta['nadawcza_nagl2']))
		{
			$nagl2 = 'umowa nr 000/X/0/CP RH 00 - 0/'.date('Y').' z dnia '.date('d.m.Y').' r.';
			$poczta['nadawcza_nagl2'] = $nagl2;
		}
		if(empty($poczta['nadawcza_stopka']))
		{
			$stopka = 'Nakład własny - zgoda CP RH Rzeszów z dnia '.$date.' roku';
			$poczta['nadawcza_stopka'] = $stopka;
		}
		if(empty($poczta['pieczatka']))
		{
			$pieczatka = 'OPŁATA POBRANA'.PHP_EOL;
			$pieczatka.= 'TAXE PERÇUE - POLOGNE'.PHP_EOL;
			$pieczatka.= 'umowa nr 000/X/0/CP RH00-0/'.date('Y').PHP_EOL;
			$pieczatka.= 'z Pocztą Polską S.A. z dnia '.date('d-m-Y').' r.'.PHP_EOL;
			$pieczatka.= 'Nadano w UP Rzeszów 0';
			$poczta['pieczatka'] = $pieczatka;
		}
		
		$this->view->poczta = $poczta;
	}
    public function dodajAction()
	{
            $pocztapolska = new Pocztapolska();
            $zamowienia = new Zamowienia();
            if($this->_request->getParam('zamowienie')){
                $zamowienie = $this->_request->getParam('zamowienie');
                $dane = $zamowienia->selectWybranyKontrahent($zamowienie);
                
            }
            
		if($this->_request->getPost('poczta'))
		{
			$poczta = $this->_request->getPost('poczta');
			
                        if(empty($poczta['zwrot_niezwlocznie'])){
                            $poczta['zwrot_niezwlocznie']=0;
                        }
                        if(empty($poczta['zwrot_pozniej'])){
                            $poczta['zwrot_pozniej']=0;
                        }
                        if(empty($poczta['priorytetowa'])){
                            $poczta['priorytetowa']=0;
                        }
                        if(empty($poczta['poste'])){
                            $poczta['poste']=0;
                        }
                        if(empty($poczta['wartosc'])){
                            $poczta['wartosc']=0;
                        }
                        if(empty($poczta['czyrachunek'])){
                            $poczta['czyrachunek']=0;
                        }
                        if(empty($poczta['czyadres'])){
                            $poczta['czyadres']=0;
                        }
                        if(empty($poczta['ostroznie'])){
                            $poczta['ostroznie']=0;
                        }
                        if(empty($poczta['sprawdzenie'])){
                            $poczta['sprawdzenie']=0;
                        }
                        if(empty($poczta['potwierdzenie'])){
                            $poczta['potwierdzenie']=0;
                        }
                        foreach($poczta as $d => $dana)
			{
				 $poczta[$d] = htmlspecialchars($dana);
			}
                        
                        
			
		}
		
		if(!isset($poczta)) $poczta = $pocztapolska->wypiszOne(1);
		
		if($this->_request->getPost('dane')){
                    $dane[0] = $this->_request->getPost('dane');
                }
		
		
		$this->view->poczta = $poczta;
                $this->view->dane = $dane[0];
        }
        public function drukujAction(){
            $pocztapolska = new Pocztapolska();
            
            $ustawienia = $pocztapolska->wypiszOne(1);
            
            $zamowienia = new Zamowienia();
            $listy = new Listy();
            if($this->_request->getPost('dane'))
		{
                
                
			$poczta = $this->_request->getPost('dane');
			
                        $dane['admin_id']=1;
                        $dane['deal_id']=$poczta['id'];
                        $dane['wyslano']=1;
                        $listy->dodaj($dane);
                        
                        if(empty($poczta['zwrot_niezwlocznie'])){
                            $poczta['zwrot_niezwlocznie']=0;
                        }
                        if(empty($poczta['zwrot_pozniej'])){
                            $poczta['zwrot_pozniej']=0;
                        }
                        if(empty($poczta['priorytetowa'])){
                            $poczta['priorytetowa']=0;
                        }
                        if(empty($poczta['poste'])){
                            $poczta['poste']=0;
                        }
                        if(empty($poczta['wartosc'])){
                            $poczta['wartosc']=0;
                        }
                        if(empty($poczta['czyrachunek'])){
                            $poczta['czyrachunek']=0;
                        }
                        if(empty($poczta['czyadres'])){
                            $poczta['czyadres']=0;
                        }
                        if(empty($poczta['ostroznie'])){
                            $poczta['ostroznie']=0;
                        }
                        if(empty($poczta['sprawdzenie'])){
                            $poczta['sprawdzenie']=0;
                        }
                        if(empty($poczta['potwierdzenie'])){
                            $poczta['potwierdzenie']=0;
                        }
                        foreach($poczta as $d => $dana)
			{
				 $poczta[$d] = htmlspecialchars($dana);
                                
			}
                        
                        
			
		}
                
                $this->view->poczta = $poczta;
                $this->view->ustawienia = $ustawienia;
                
                
        }
		
		
    public function ustawieniawebapiAction() {
        $info = new Pocztapolskakonto($module = 'admin');
        $en = new ElektronicznyNadawca();

        //print_r($czas);
        $zmiana = $this->_request->getPost('konto');
        if (isset($zmiana) && !empty($zmiana)) {
            if (isset($zmiana['login'])) {
                $dane['login'] = $zmiana['login'];
                $dane['konto'] = str_replace(' ', '', $zmiana['konto']);

                if (isset($zmiana['check'])) {
                    $dane['password'] = $zmiana['haslo'];
                }

                $edit = $info->edytuj($dane, $zmiana['id']);
            }
            if (isset($zmiana['checkreset'])) {
                try {
                    $haslo = new changePassword();
                    $haslo->newPassword = $zmiana['reset'];

                    $reset = $en->changePassword($haslo);
                } catch (SoapFault $error) {
                    $this->view->error = '<div class="k_blad">Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
                }
                //print_r($reset);
                $this->view->error = '<br />';
                if (isset($reset['error'][0])) {
                    for ($i = 0; $i < count($reset['error']); $i++) {
                        $this->view->error .= 'Błąd numer: ' . $reset['error'][$i]['errorNumber'] . ': ' . $reset['error'][$i]['errorDesc'] . '<br />';
                    }
                } elseif (count($reset['error']) > 0) {
                    $this->view->error .= 'Błąd numer: ' . $reset['error']['errorNumber'] . ': ' . $reset['error']['errorDesc'] . '<br />';
                }

                if (count($reset['error']) < 1) {
                    $dane['password'] = $zmiana['reset'];
                    $edit = $info->edytuj($dane, $zmiana['id']);
                }
            }
        }
        try {
            $data = new getPasswordExpiredDate();
			//var_dump($data);die();
            $czas = $en->getPasswordExpiredDate($data);
			//var_dump($czas);die();
            $urz = new getUrzedyNadania();
            $urzedy = $en->getUrzedyNadania($urz);
            $this->view->urzedy = $urzedy['urzedyNadania'];
            //var_dump($this->view->urzedy);
            $this->view->blad_edycji = 'Data ważności hasła: ' . $czas['dataWygasniecia'] . '. Zresetuj hasło przed zakończeniem, aby przedłużyć ważność.';
        } catch (SoapFault $error) {
            $this->view->error = '<div class="k_blad">Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
        }


        $display = $info->pojedyncza(1);
        $this->view->display = $display;
        //print_r($display);
    }

    public function dodajwebapiAction()
	{
        $pocztapolska = new Pocztapolska();
        $zamowienia = new Zamowienia();
        $ppk = new Pocztapolskakonto();
        $en = new ElektronicznyNadawca();
        //if ($this->_request->getParam('zamowienie'))
		$zamowienie = intval($this->_request->getParam('zamowienie', 0));
		$dane = $zamowienie ? $zamowienia->selectWybraneZamowienie($zamowienie) : null;

        if (!isset($poczta))
            $poczta = $pocztapolska->wypiszOne(1);

        //if ($this->_request->getPost('dane')) {
        //    $dane[0] = $this->_request->getPost('dane');
        //}

        $dane1 = $this->_request->getPost('dane');
        if (isset($dane1) && !empty($dane1)) {

            $adres = new adresType();
            if (strlen($dane1['nazwa_firmy1']) > 1) {
                $nazwa = $dane1['nazwa_firmy1'];
                $nazwa2 = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
            } else {
                $nazwa = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
                $nazwa2 = '';
            }
            $adres->nazwa = $nazwa; // nazwaType
            $adres->nazwa2 = $nazwa2; // nazwa2Type
            $adres->ulica = $dane1['ulica1']; // ulicaType
            $adres->numerDomu = $dane1['nr1']; // numerDomuType
            $adres->numerLokalu = $dane1['mieszkanie1']; // numerLokaluType
            $adres->miejscowosc = $dane1['miasto1']; // miejscowoscType
            $adres->kodPocztowy = str_replace('-', '', $dane1['kod1']); // kodPocztowyType
            $adres->kraj = 'Polska'; // krajType
            $adres->telefon = str_replace(' ', '', $dane1['telefon1']); // telefonType
            $adres->email = $dane1['email']; // emailType

            if (strlen($dane1['komorka1']) > 9) {
                $ile = strlen(str_replace(' ', '', $dane1['komorka1'])) - 9;
                $num = substr(str_replace(' ', '', $dane1['komorka1']), $ile, 9);
            } else {
                $num = $dane1['komorka1'];
            }
            $adres->mobile = $num; // mobileType
            //print_r($adres);
            if ($dane1['typ'] == 'paczka') {
                $paczka = $this->_request->getPost('paczka');
                $typ = new paczkaPocztowaType();
                $typ->posteRestante = 0; // boolean
                if (isset($paczka['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $paczka['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $paczka['typ']; // kategoriaType
                $typ->gabaryt = $paczka['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $paczka['masa']) * 1000); // masaType
                if (isset($paczka['dwartosc'])) {
                    $liczba = (0 + str_replace(',', '.', $paczka['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                } else {
                    $typ->wartosc = ''; // wartoscType
                }
                if (isset($paczka['zwrot']))
                    $typ->zwrotDoslanie = 1; // boolean
                if (isset($paczka['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($paczka['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
            }
            elseif ($dane1['typ'] == 'pobraniowa') {
                $pobranie = $this->_request->getPost('pobraniowa');
                $typ = new przesylkaPobraniowaType();
                $typ1 = new pobranieType();
                $typ1->sposobPobrania = @$pobranie['sposob']; // sposobPobraniaType
                $typ1->kwotaPobrania = (0 + str_replace(',', '.', $pobranie['kwota']) * 100); // kwotaPobraniaType
                $typ1->nrb = $pobranie['rachunek']; // anonymous51
                $typ1->tytulem = $pobranie['tytul']; // anonymous52
                if (isset($pobranie['sprawdz']))
                    $typ1->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = 1; // boolean


                $typ->pobranie = $typ1; // pobranieType
                $typ->posteRestante = 0; // boolean
                if (isset($pobranie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $pobranie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }


                $typ->kategoria = $pobranie['typ']; // kategoriaType
                $typ->gabaryt = $pobranie['gabaryt']; // gabarytType
                if (isset($pobranie['ostroznie']))
                    $typ->ostroznie; // boolean
                if (isset($pobranie['dwartosc'])) {
                    $liczba = (0 + str_replace(',', '.', $pobranie['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                } else {
                    $typ->wartosc = ''; // wartoscType
                }

                $typ->masa = (0 + str_replace(',', '.', $pobranie['masa']) * 1000); // masaType
            } elseif ($dane1['typ'] == 'polecona') {
                $polecenie = $this->_request->getPost('polecona');
                $typ = new przesylkaPoleconaKrajowaType();
                $typ->epo = new EPOSimpleType(); // EPOType
                $typ->posteRestante = 0; // boolean
                if (isset($polecenie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $polecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $polecenie['typ']; // kategoriaType
                $typ->gabaryt = $polecenie['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $polecenie['masa']) * 1000); // masaType
                if (isset($polecenie['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($polecenie['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
            }
            elseif ($dane1['typ'] == 'deklaracja') {
                $deklaracje = $this->_request->getPost('deklaracja');
                $typ = new przesylkaListowaZadeklarowanaWartoscType();
                $typ->posteRestante = 0; // boolean
                $liczba = (0 + str_replace(',', '.', $deklaracje['wartosc']));
                $typ->wartosc = ($liczba * 100); // wartoscType

                if (isset($deklaracje['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $deklaracje['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $deklaracje['typ']; // kategoriaType
                $typ->gabaryt = $deklaracje['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $deklaracje['masa']) * 1000); // masaType
                if (isset($deklaracje['zwrot']))
                    $typ->zwrotDoslanie = 1; // boolean
                if (isset($deklaracje['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($deklaracje['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
            }


            // print_r($typ);
            try {
                $guid = new getGuid();
                $guid->ilosc = 1;
                $gui = $en->getGuid($guid);
                $typ->guid = $gui['guid'];
				$typ->opis = $dane1['opis'];
                //$typ->guid = $gui;
                $typ->adres = $adres;
                $add = new addShipment();
                $add->przesylki[] = $typ;

                $wyslij = $en->addShipment($add);
                //var_dump($wyslij);
                if (empty($wyslij['retval']['error'])) {
                    $this->view->blad_edycji = '<div class="k_ok">Poprawnie wysłano przesyłkę.</div>';
                    $this->view->error = '';
                    $this->view->error .= 'Numer nadania: ' . $wyslij['retval']['numerNadania'];
                    $this->view->error .= '<br />Numer GUID: ' . $wyslij['retval']['guid'];

                    //$zam['przesylka'] = $wyslij['retval']['numerNadania'];
                    //$zamow = new Zamowienia($module = 'admin');
                    //$edit = $zamow->edytuj($zamowienie, $zam);
					$pocztapolskadane = new Pocztapolskadane();
					$przesylka['id_zam'] = @intval($zamowienie);
					$przesylka['nr_nad'] = $wyslij['retval']['numerNadania'];
					$przesylka['guid'] = $wyslij['retval']['guid'];
					$pocztapolskadane->zapisz($przesylka);
                    
                    $this->getResponse()->setHeader('Refresh', '1; URL=' . $this->baseUrl . '/admin/zamowienia/wypisz/tryb/aktualne/mode/all');
                } else {
                    $this->view->blad_edycji = 'Błąd danych. Przesyłka nie została zatwierdzona.';
                    $this->view->error = '';
                    if (isset($wyslij['retval']['error']['0'])) {
                        for ($i = 0; $i < count($wyslij['retval']['error']); $i++) {
                            $this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error'][$i]['errorNumber'] . ': ' . $wyslij['retval']['error'][$i]['errorDesc'];
                        }
                    } else {
                        $this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error']['errorNumber'] . ': ' . $wyslij['retval']['error']['errorDesc'];
                    }
                }
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad">Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
        $this->view->ppk = $ppk->pojedyncza('1');
        $this->view->poczta = $poczta;
        $this->view->dane = @$dane;
    }

    public function buforAction() {
        $en = new ElektronicznyNadawca();
        
        //print_r($bufor);
        if ($this->_request->getParam('iddel')) {
            try {
                $guid = $this->_request->getParam('iddel');
                $type = new clearEnvelopeByGuids();
                $type->guid = $guid;
                $usun = $en->clearEnvelopeByGuids($type);
				$pocztapolskadane = new Pocztapolskadane();
				$pocztapolskadane->usunGUID($guid);

                $this->view->error = '<div class="k_ok">Poprawnie usunięto pozycję z bufora.</div>';
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad">Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
        if ($this->_request->getParam('idwydruk')) {
            try {
                $guid = $this->_request->getParam('idwydruk');
                $type = new getAddresLabelByGuid();
                $type->guid = $guid;
                $usun = $en->getAddresLabelByGuid($type);
                print_r($usun);
                // $hash = $usun['content']['pdfContent'];
                $this->parsePdfPage(base64_decode($usun['content']['pdfContent']), 'druk_' . $guid);
                //$OUTPUT = "output_decoded.pdf";
                //unset($usun);
                //$bin = base64_decode($usun['content']['pdfContent']);
                //file_put_contents($OUTPUT, $bin);
                //print base64_encode(trim($usun['content']['pdfContent']));
                $this->view->error = '<div class="k_ok">Pobrano plik.</div>';
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad">Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
        if ($this->_request->getPost('wyslijwszystko')) {
            try {
                $wyslij = new sendEnvelope();
                $wyslij->urzadNadania = $this->_request->getPost('urzad');
				$parametry = new getEnvelopeBufor();
				$bufor = $en->getEnvelopeBufor($parametry);
				$bufor = @$bufor['przesylka'];
				$wyslano = $en->sendEnvelope($wyslij);
				
				$pocztapolskadane = new Pocztapolskadane();
				if(count($bufor) > 0)
				{
					if(!isset($bufor[0])) $bufor = array(0 => $bufor);
					foreach($bufor as $przes)
					{
						$zam = $pocztapolskadane->wypiszNumer(@$przes['numerNadania']);
						$przesylka['id_zam'] = @intval($zam['id_zam']);
						$przesylka['bufor'] = @intval($wyslano['idEnvelope']);
						$przesylka['data_wysylka'] = date('Y-m-d H:i:s');
						$pocztapolskadane->zapisz($przesylka);
					}
				}
                //var_dump($wyslano);var_dump($bufor);var_dump($zam);var_dump($przesylka);die();

                if (empty($wyslano['error'])) {
                    $this->view->blad_edycji = '<div class="k_ok">Poprawnie wysłano przesyłki.</div>';
                } else {
                    $this->view->blad_edycji = '<div class="k_blad">Błąd danych. Przesyłka nie została zatwierdzona.</div>';
                    $this->view->error = '';
                    if (isset($wyslano['error']['0'])) {
                        for ($i = 0; $i < count($wyslano['error']); $i++) {
                            $this->view->error .= '<br />Błąd numer: ' . $wyslano['error'][$i]['errorNumber'] . ': ' . $wyslano['error'][$i]['errorDesc'];
                        }
                    } else {
                        $this->view->error .= '<br />Błąd numer: ' . $wyslano['error']['errorNumber'] . ': ' . $wyslano['error']['errorDesc'];
                    }
                }
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
        if ($this->_request->getPost('kasujwszystko')) {
            try {
                $czysc = new clearEnvelope();
				$parametry = new getEnvelopeBufor();
				$bufor = $en->getEnvelopeBufor($parametry);
				$bufor = @$bufor['przesylka'];
                $wyczysc = $en->clearEnvelope($czysc);
				$pocztapolskadane = new Pocztapolskadane();
				if(count($bufor) > 0)
				{
					if(!isset($bufor[0])) $bufor = array(0 => $bufor);
					foreach($bufor as $przes)
					{
						$pocztapolskadane->usunGUID($przes['guid']);
					}
				}
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }


        try {
            $ur = new getUrzedyNadania();
            $urzedy = $en->getUrzedyNadania($ur);
            $parametry = new getEnvelopeBufor();
        $bufor = $en->getEnvelopeBufor($parametry);
            //var_dump($bufor['przesylka']);die();
        } catch (SoapFault $error) {
            $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
        }
        
        $this->view->urzedy = @$urzedy['urzedyNadania'];
        $this->view->bufor = @$bufor['przesylka'];
    }
    
    public function wyslaneAction(){
        $en = new ElektronicznyNadawca();
        $parametry = new getEnvelopeList();
        $parametry->endDate = date('Y-m-d');
        $parametry->startDate = date('Y-m-d', strtotime(date('Y-m-d').' -30 day'));
        try
        {
            $bufor = $en->getEnvelopeList($parametry);
        }
        catch (SoapFault $error)
        {
            $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            return;
        }
		//var_dump($bufor['envelopes']);die();
        //print_r($bufor);
        $this->view->bufor = $bufor['envelopes'];
        if ($this->_request->getParam('idnalepki')) {
            try {
                $guid = $this->_request->getParam('idnalepki');
                $type = new getAddresLabelCompact();
                $type->idEnvelope = $guid;
                $usun = $en->getAddresLabelCompact($type);
                print_r($usun);
                //print_r($usun['content']['pdfContent']);
                // $hash = $usun['content']['pdfContent'];
                if(empty($usun['content'][0])){
                    $this->parsePdfPage(base64_decode($usun['content']['pdfContent']), 'nalepki_adresowe_' . $guid);
                }else{
                    for($i=0;$i<count($usun['content']);$i++){
                        //$this->parsePdfPage(base64_decode($usun['content'][$i]['pdfContent']), 'nalepki_adresowe_' . $guid);
                    //print_r($usun['content'][$i]['pdfContent']);
                        
                        $value = base64_decode($usun['content'][$i]['pdfContent'], true);
                        $file = fopen('../public/etykieta_adresowa_' . $i . '.pdf', 'w');
                        fwrite($file, $value);
                       
                        
                    }
                }
                
                
                //$OUTPUT = "output_decoded.pdf";
                //unset($usun);
                //$bin = base64_decode($usun['content']['pdfContent']);
                //file_put_contents($OUTPUT, $bin);
                //print base64_encode(trim($usun['content']['pdfContent']));
                $this->view->error = '<div class="k_ok">Pobrano plik.</div>';
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
        if ($this->_request->getParam('idksiazka')) {
            try {
                $guid = $this->_request->getParam('idksiazka');
                $type = new getOutboxBook();
                $type->idEnvelope = $guid;
                $usun = $en->getOutboxBook($type);
                print_r($usun);
                // $hash = $usun['content']['pdfContent'];
                $this->parsePdfPage(base64_decode($usun['content']['pdfContent']), 'ksiazka_nadawcza_' . $guid);
                //$OUTPUT = "output_decoded.pdf";
                //unset($usun);
                //$bin = base64_decode($usun['content']['pdfContent']);
                //file_put_contents($OUTPUT, $bin);
                //print base64_encode(trim($usun['content']['pdfContent']));
                $this->view->error = '<div class="k_ok">Pobrano plik.</div>';
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
        if ($this->_request->getParam('idpoczta')) {
            try {
                $guid = $this->_request->getParam('idpoczta');
                $type = new getFirmowaPocztaBook();
                $type->idEnvelope = $guid;
                $usun = $en->getFirmowaPocztaBook($type);
                print_r($usun);
                // $hash = $usun['content']['pdfContent'];
                $this->parsePdfPage(base64_decode($usun['content']['pdfContent']), 'poczta_firmowa_' . $guid);
                //$OUTPUT = "output_decoded.pdf";
                //unset($usun);
                //$bin = base64_decode($usun['content']['pdfContent']);
                //file_put_contents($OUTPUT, $bin);
                //print base64_encode(trim($usun['content']['pdfContent']));
                $this->view->error = '<div class="k_ok">Pobrano plik.</div>';
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
        
    }

    protected function parsePdfPage($xml, $filename = 'file') {
        Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        header("Content-Disposition: inline; filename=$filename.pdf");
        header("Content-type: application/x-pdf");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Description: File Transfer");

        echo $xml;
    }
    
     public function dodajwebapiallegroAction() {
        $pocztapolska = new Pocztapolska();
        $zamowienia = new AllegroZamowienia();
        $ppk = new Pocztapolskakonto();
        $en = new ElektronicznyNadawca();
        if ($this->_request->getParam('zamowienie')) {
            $zamowienie = $this->_request->getParam('zamowienie');
            $dane = $zamowienia->selectWybraneZamowienie($zamowienie);
        }
        if (!isset($poczta))
            $poczta = $pocztapolska->wypiszOne(1);

        //if ($this->_request->getPost('dane')) {
        //    $dane[0] = $this->_request->getPost('dane');
        //}

        $dane1 = $this->_request->getPost('dane');
        if (isset($dane1) && !empty($dane1)) {

            $adres = new adresType();
            if (strlen($dane1['nazwa_firmy1']) > 1) {
                $nazwa = $dane1['nazwa_firmy1'];
                $nazwa2 = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
            } else {
                $nazwa = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
                $nazwa2 = '';
            }
            $adres->nazwa = $nazwa; // nazwaType
            $adres->nazwa2 = $nazwa2; // nazwa2Type
            $adres->ulica = $dane1['ulica1']; // ulicaType
            $adres->numerDomu = $dane1['nr1']; // numerDomuType
            $adres->numerLokalu = $dane1['mieszkanie1']; // numerLokaluType
            $adres->miejscowosc = $dane1['miasto1']; // miejscowoscType
            $adres->kodPocztowy = str_replace('-', '', $dane1['kod1']); // kodPocztowyType
            $adres->kraj = 'Polska'; // krajType
            $adres->telefon = str_replace(' ', '', $dane1['telefon1']); // telefonType
            $adres->email = $dane1['email']; // emailType

            if (strlen($dane1['komorka1']) > 9) {
                $ile = strlen(str_replace(' ', '', $dane1['komorka1'])) - 9;
                $num = substr(str_replace(' ', '', $dane1['komorka1']), $ile, 9);
            } else {
                $num = $dane1['komorka1'];
            }
            $adres->mobile = $num; // mobileType
            //print_r($adres);
            if ($dane1['typ'] == 'paczka') {
                $paczka = $this->_request->getPost('paczka');
                $typ = new paczkaPocztowaType();
                $typ->posteRestante = 0; // boolean
                if (isset($paczka['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $paczka['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $paczka['typ']; // kategoriaType
                $typ->gabaryt = $paczka['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $paczka['masa']) * 1000); // masaType
                if (isset($paczka['dwartosc'])) {
                    $liczba = (0 + str_replace(',', '.', $paczka['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                } else {
                    $typ->wartosc = ''; // wartoscType
                }
                if (isset($paczka['zwrot']))
                    $typ->zwrotDoslanie = 1; // boolean
                if (isset($paczka['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($paczka['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
            }
            elseif ($dane1['typ'] == 'pobraniowa') {
                $pobranie = $this->_request->getPost('pobraniowa');
                $typ = new przesylkaPobraniowaType();
                $typ1 = new pobranieType();
                $typ1->sposobPobrania = $pobranie['sposob']; // sposobPobraniaType
                $typ1->kwotaPobrania = (0 + str_replace(',', '.', $pobranie['kwota']) * 100); // kwotaPobraniaType
                $typ1->nrb = $pobranie['rachunek']; // anonymous51
                $typ1->tytulem = $pobranie['tytul']; // anonymous52
                if (isset($pobranie['sprawdz']))
                    $typ1->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = 1; // boolean


                $typ->pobranie = $typ1; // pobranieType
                $typ->posteRestante = 0; // boolean
                if (isset($pobranie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $pobranie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }


                $typ->kategoria = $pobranie['typ']; // kategoriaType
                $typ->gabaryt = $pobranie['gabaryt']; // gabarytType
                if (isset($pobranie['ostroznie']))
                    $typ->ostroznie; // boolean
                if (isset($pobranie['dwartosc'])) {
                    $liczba = (0 + str_replace(',', '.', $pobranie['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                } else {
                    $typ->wartosc = ''; // wartoscType
                }

                $typ->masa = (0 + str_replace(',', '.', $pobranie['masa']) * 1000); // masaType
            } elseif ($dane1['typ'] == 'polecona') {
                $polecenie = $this->_request->getPost('polecona');
                $typ = new przesylkaPoleconaKrajowaType();
                $typ->epo = new EPOSimpleType(); // EPOType
                $typ->posteRestante = 0; // boolean
                if (isset($polecenie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $polecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $polecenie['typ']; // kategoriaType
                $typ->gabaryt = $polecenie['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $polecenie['masa']) * 1000); // masaType
                if (isset($polecenie['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($polecenie['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
            }
            elseif ($dane1['typ'] == 'deklaracja') {
                $deklaracje = $this->_request->getPost('deklaracja');
                $typ = new przesylkaListowaZadeklarowanaWartoscType();
                $typ->posteRestante = 0; // boolean
                $liczba = (0 + str_replace(',', '.', $deklaracje['wartosc']));
                $typ->wartosc = ($liczba * 100); // wartoscType

                if (isset($deklaracje['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $deklaracje['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $deklaracje['typ']; // kategoriaType
                $typ->gabaryt = $deklaracje['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $deklaracje['masa']) * 1000); // masaType
                if (isset($deklaracje['zwrot']))
                    $typ->zwrotDoslanie = 1; // boolean
                if (isset($deklaracje['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($deklaracje['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
            }


            // print_r($typ);
            try {
                $guid = new getGuid();
                $guid->ilosc = 1;
                $gui = $en->getGuid($guid);
                $typ->guid = $gui['guid'];
                //$typ->guid = $gui;
                $typ->adres = $adres;
                $add = new addShipment();
                $add->przesylki[] = $typ;

                $wyslij = $en->addShipment($add);
                //print_r($wyslij);
                if (empty($wyslij['retval']['error'])) {
                    $this->view->blad_edycji = '<div class="k_ok">Poprawnie wysłano przesyłkę.</div>';
                    $this->view->error = '';
                    $this->view->error .= 'Numer nadania: ' . $wyslij['retval']['numerNadania'];
                    $this->view->error .= '<br />Numer GUID: ' . $wyslij['retval']['guid'];

                    $zam['przesylka'] = $wyslij['retval']['numerNadania'];
                    $zamow = new AllegroZamowienia($module = 'admin');
                    $edit = $zamow->edytuj($zamowienie, $zam);
                    
                    $this->getResponse()->setHeader('Refresh', '1; URL=' . $this->baseUrl . '/admin/zamowienia/wypisz/tryb/aktualne/mode/all');
                } else {
                    $this->view->blad_edycji = 'Błąd danych. Przesyłka nie została zatwierdzona.';
                    $this->view->error = '';
                    if (isset($wyslij['retval']['error']['0'])) {
                        for ($i = 0; $i < count($wyslij['retval']['error']); $i++) {
                            $this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error'][$i]['errorNumber'] . ': ' . $wyslij['retval']['error'][$i]['errorDesc'];
                        }
                    } else {
                        $this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error']['errorNumber'] . ': ' . $wyslij['retval']['error']['errorDesc'];
                    }
                }
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad">Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
        $this->view->ppk = $ppk->pojedyncza('1');
        $this->view->poczta = $poczta;
        $this->view->dane = $dane;
    }

}
?>