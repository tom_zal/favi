<?php
class Admin_DostepnoscController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}	
	function dodajAction()
	{
		$id = $this->_request->getParam('id', 0);
		$dostepnosc = new Dostepnosc();
		$dostepnosc->id = $id;
		$this->view->pojedynczy = $dostepnosc->wypiszID();
		
		$del = $this->_request->getParam('delid', 0);
		if($del > 0)
		{
			$dostepnosc->id = $del;
			$dostepnosc->usun();
			//$this->view->blad_edycji = '<div class="k_ok">Wybrany rozmiar został usunięty.</div>';
			$this->_redirect('/admin/dostepnosc/dodaj/');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/dostepnosc/dodaj');
		}
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa', '');
			$min = $this->_request->getPost('min', 0);
			$max = $this->_request->getPost('max', 0);
			$kolor = $this->_request->getPost('kolor', 'ffffff');
			$dane = array('nazwa' => $nazwa, 'min' => $min, 'max' => $max, 'kolor' => $kolor);
			$id = $dostepnosc->dodaj($dane);			
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/dostepnosc/dodaj/');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/dostepnosc/dodaj');
		}
		
		$this->view->dostepnosci = $dostepnosc->wypisz();
	}	
	function edytujAction()
	{
		$id = $this->_request->getParam('id', 0);
		$dostepnosc = new Dostepnosc();
		$dostepnosc->id = $id;
		$this->view->pojedynczy = $dostepnosc->wypiszID();
		
		if($this->_request->isPost())
		{
			$id = $this->_request->getPost('id', 0);
			$nazwa = $this->_request->getPost('nazwa', '');
			$min = $this->_request->getPost('min', 0);
			$max = $this->_request->getPost('max', 0);
			$kolor = $this->_request->getPost('kolor', 'ffffff');
			$dane = array('nazwa' => $nazwa, 'min' => $min, 'max' => $max, 'kolor' => $kolor);
			$dostepnosc->id = $id;
			$dostepnosc->edytuj($dane);
			
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/dostepnosc/dodaj/');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/dostepnosc/dodaj');
		}
		
		$this->view->dostepnosci = $dostepnosc->wypisz();
	}
}
?>