<?php

class Admin_PocztapolskaController extends Ogolny_Controller_Admin {

    public function init() {
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
    }

    function __call($method, $args) {
        $this->_redirect('/admin');
    }

    public function indexAction() {
        
    }

    function ksiazkanadawczaAction() {
        $list = new Zend_Session_Namespace('listPoczta');
        //var_dump($_POST);
        $data = $this->_request->getPost('data');
        if ($data) {
            $data = $data['rok'] . '-' . $data['mc'] . '-' . $data['day'];
            if (preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $data))
                $list->data = $data;
        }
        else
            $data = $this->_request->getParam('data');
        $dataDo = $this->_request->getPost('dataDo');
        if ($dataDo) {
            $dataDo = $dataDo['rok'] . '-' . $dataDo['mc'] . '-' . $dataDo['day'];
            if (preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $dataDo))
                $list->dataDo = $dataDo;
        }
        else
            $dataDo = $this->_request->getParam('dataDo');
        $typprzesylki = $this->_request->getPost('typprzesylki', 'polecona');
        $this->view->typprzesylki = $typprzesylki;
        //if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $data)) $list->data = $data;
        //if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $dataDo)) $list->data = $dataDo;
        if (!isset($list->data))
            $list->data = date('Y-m-d');
        if (!isset($list->dataDo))
            $list->dataDo = date('Y-m-d');
        $data = $list->data;
        $dataDo = $list->dataDo;
        $this->view->data = $data;
        $this->view->dataDo = $dataDo;

        $pocztapolska = new Pocztapolska();
        $adminPoczta = $this->view->adminID;
        if (@in_array($adminPoczta, $this->pocztaPrzekierujAdminow))
            $adminPoczta = $this->pocztaPrzekierujDoAdmina;
        $poczta = $pocztapolska->wypiszOne($adminPoczta);
        $this->view->nadawca = 'Imię i nazwisko (nazwa) oraz adres nadawcy: ';
        $poczta['nadawcza_nagl1'] = str_replace(' r.', '&nbsp;r.', $poczta['nadawcza_nagl1']);
        $poczta['nadawcza_nagl2'] = str_replace(' r.', '&nbsp;r.', $poczta['nadawcza_nagl2']);
        $poczta['nadawcza_nagl1'] = str_replace('ul. ', 'ul.&nbsp;', $poczta['nadawcza_nagl1']);
        $poczta['nadawcza_nagl2'] = str_replace('ul. ', 'ul.&nbsp;', $poczta['nadawcza_nagl2']);
        $poczta['nadawcza_nagl1'] = str_replace('z dnia', 'z&nbsp;dnia', $poczta['nadawcza_nagl1']);
        $poczta['nadawcza_nagl2'] = str_replace('z dnia', 'z&nbsp;dnia', $poczta['nadawcza_nagl2']);
        $this->view->nadawca.= '<br/>' . $poczta['nadawcza_nagl1'] . '<br/>' . $poczta['nadawcza_nagl2'] . ' ';
        $this->view->naklad = $poczta['nadawcza_stopka'];

        $listy = new Listy();
        //$allegro = new Allegroaukcje();
        //$zakupy = new Zamowienia();
        //$users = new Allegrousers();

        if ($this->_request->getParam('delid')) {
            $delid = intval($this->_request->getParam('delid'));
            //$buyer = intval($this->_request->getParam('buyer'));
            //$zakupy->edytujDeal($this->view->adminID, $buyer, $delid, array('wyslano' => 0));
            $listy->usun($delid);
            $this->_redirect('admin/pocztapolska/ksiazkanadawcza');
        }

        $raport = $listy->wypiszRaport($this->view->adminID, $data, $dataDo, $typprzesylki);
        $this->view->dane = null;
        
            $this->view->dane = $raport;
        
        //var_dump($raporty);
    }

    public function edytujAction() {
        $pocztapolska = new Pocztapolska();
        if ($this->_request->getPost('poczta')) {
            $poczta = $this->_request->getPost('poczta');

           
            foreach ($poczta as $d => $dana) {
                $poczta[$d] = htmlspecialchars($dana);
            }


            if (!isset($this->view->error)) {
                $pocztapolska->edytujID($this->view->adminID, $poczta);
                $this->_redirect($this->URI);
            }
        }

        if (!isset($poczta))
            $poczta = $pocztapolska->wypiszOne($this->view->adminID);

        $login = new Login();
        $login->id = $this->view->adminID;
        $logon = $login->wypiszPojedynczego();

        $arrLocales = array('pl_PL', 'pl', 'Polish_Poland.28592');
        setlocale(LC_ALL, $arrLocales);
        $date = iconv('ISO-8859-2', 'UTF-8', strftime('%d %B %Y'));

        if (empty($poczta['nadawcza_nagl1'])) {
            $settings = new Ustawienia();
            $ustawienia = $settings->wypisz();
            $dane_firmy = @unserialize($ustawienia['dane_firmy']);

            $nagl1 = '';
            if (!empty($dane_firmy['nazwa_firmy']))
                $nagl1.= $dane_firmy['nazwa_firmy'] . ' ';
            $nagl1.= $dane_firmy['imie'] . ' ' . $dane_firmy['nazwisko'] . ', ';
            $nagl1.= 'ul. ' . $dane_firmy['ulica'] . ' ' . $dane_firmy['nr'] . ($dane_firmy['mieszkanie'] ? ' ' . $dane_firmy['mieszkanie'] : '') . ', ';
            $nagl1.= $dane_firmy['kod'] . ' ' . $dane_firmy['miasto'];
            $poczta['nadawcza_nagl1'] = $nagl1;
        }
        if (empty($poczta['nadawcza_nagl2'])) {
            $nagl2 = 'umowa nr 000/X/0/CP RH 00 - 0/' . date('Y') . ' z dnia ' . date('d.m.Y') . ' r.';
            $poczta['nadawcza_nagl2'] = $nagl2;
        }
        if (empty($poczta['nadawcza_stopka'])) {
            $stopka = 'Nakład własny - zgoda CP RH Rzeszów z dnia ' . $date . ' roku';
            $poczta['nadawcza_stopka'] = $stopka;
        }
        if (empty($poczta['pieczatka'])) {
            $pieczatka = 'OPŁATA POBRANA' . PHP_EOL;
            $pieczatka.= 'TAXE PERÇUE - POLOGNE' . PHP_EOL;
            $pieczatka.= 'umowa nr 000/X/0/CP RH00-0/' . date('Y') . PHP_EOL;
            $pieczatka.= 'z Pocztą Polską S.A. z dnia ' . date('d-m-Y') . ' r.' . PHP_EOL;
            $pieczatka.= 'Nadano w UP Rzeszów 0';
            $poczta['pieczatka'] = $pieczatka;
        }

        $this->view->poczta = $poczta;
    }

    public function dodajAction() {
        $pocztapolska = new Pocztapolska();
        $zamowienia = new Zamowienia();
        if ($this->_request->getParam('zamowienie')) {
            $zamowienie = $this->_request->getParam('zamowienie');
            $dane1 = $zamowienia->selectWybraneZamowienie($zamowienie);
        }
        
        if($this->_request->getPost('dane')){
            $adres = $this->_request->getPost('dane');
            
            $dane['nazwisko'] = $adres['nazwisko1'];
            $dane['typ'] = $adres['typ'];
            $dane['mail'] = $adres['email'];
            $dane['nazwa'] = $adres['nazwa_firmy1'];
            $dane['imie'] = $adres['imie1'];
            $dane['telefon'] = $adres['telefon1'];
            $dane['ulica'] = $adres['ulica1'];
            $dane['nr'] = $adres['nr1'];
            $dane['mieszkanie'] = $adres['mieszkanie1'];
            $dane['kod'] = $adres['kod1'];
            $dane['miasto'] = $adres['miasto1'];
            $dane['komorka'] = $adres['komorka1'];
            
            if($adres['typ']=='paczka'){
                $paczka = $this->_request->getPost('paczka');
                //die();
                $dane['kategoria'] = $paczka['typ'];
                if(isset($paczka['dwartosc']))
                $dane['wartosc'] = $paczka['dwartosc'];
                $dane['deklaracjawar']=$paczka['deklaracjawar'];
                if(isset($paczka['dpotwierdzenie']))
                $dane['potwierdzenie'] = $paczka['dpotwierdzenie'];
                if(isset($paczka['zwrot']))
                $dane['zwrot'] = $paczka['zwrot'];
            }
            elseif($adres['typ']=='pobraniowa'){
                $paczka = $this->_request->getPost('pobraniowa');
                //print_r($paczka);
                //die();
                $dane['kwota'] = $paczka['kwota'];
                $dane['pobranietyp']=$paczka['sposob'];
                $dane['rachunek'] = $paczka['rachunek'];
                $dane['tytul'] = $paczka['tytul'];
                $dane['kategoria'] = $paczka['typ'];
                if(isset($paczka['dwartosc']))
                $dane['wartosc'] = $paczka['dwartosc'];
                $dane['deklaracjawar']=$paczka['deklaracjawar'];
                if(isset($paczka['dpotwierdzenie']))
                $dane['potwierdzenie'] = $paczka['dpotwierdzenie'];
                if(isset($paczka['sprawdz']))
                $dane['sprawdz'] = $paczka['sprawdz'];
                if(isset($paczka['ostroznie']))
                $dane['ostroznie']=$paczka['ostroznie'];
                
                
            }
            elseif($adres['typ']=='polecona'){
                $paczka=$this->_request->getPost('polecona');
                //die();
                $dane['kategoria'] = $paczka['typ'];
                 if(isset($paczka['dpotwierdzenie']))
                $dane['potwierdzenie'] = $paczka['dpotwierdzenie'];
            }
            $listy = new Listy();
            $dodaj = $listy->dodaj($dane);
            //print_r($dane);
            $this->_redirect('/admin/pocztapolska/drukuj/id/'.$dodaj);
            //$dane1=$dane;
        }

        $poczta = $pocztapolska->wypiszOne();
        $this->view->poczta = $poczta;
        $this->view->dane = $dane1;
    }
    public function dodajallegroAction() {
        $pocztapolska = new Pocztapolska();
        $zamowienia = new AllegroZamowienia();
        if ($this->_request->getParam('zamowienie')) {
            $zamowienie = $this->_request->getParam('zamowienie');
            $dane1 = $zamowienia->selectWybraneZamowienie($zamowienie);
			
        }
        
        if($this->_request->getPost('dane')){
            $adres = $this->_request->getPost('dane');
      
			$dane['deal_id'] = $adres['deal_id'];
            $dane['nazwisko'] = $adres['nazwisko1'];
            $dane['typ'] = $adres['typ'];
            $dane['mail'] = $adres['email'];
            $dane['nazwa'] = $adres['nazwa_firmy1'];
            $dane['imie'] = $adres['imie1'];
            $dane['telefon'] = $adres['telefon1'];
            $dane['ulica'] = $adres['ulica1'];
            $dane['nr'] = $adres['nr1'];
            $dane['mieszkanie'] = $adres['mieszkanie1'];
            $dane['kod'] = $adres['kod1'];
            $dane['miasto'] = $adres['miasto1'];
            $dane['komorka'] = $adres['komorka1'];
            
            if($adres['typ']=='paczka'){
                $paczka = $this->_request->getPost('paczka');
                //die();
                $dane['kategoria'] = $paczka['typ'];
                if(isset($paczka['dwartosc']))
                $dane['wartosc'] = $paczka['dwartosc'];
                $dane['deklaracjawar']=$paczka['deklaracjawar'];
                if(isset($paczka['dpotwierdzenie']))
                $dane['potwierdzenie'] = $paczka['dpotwierdzenie'];
                if(isset($paczka['zwrot']))
                $dane['zwrot'] = $paczka['zwrot'];
            }
            elseif($adres['typ']=='pobraniowa'){
                $paczka = $this->_request->getPost('pobraniowa');
                //print_r($paczka);
                //die();
                $dane['kwota'] = $paczka['kwota'];
                $dane['pobranietyp']=$paczka['sposob'];
                $dane['rachunek'] = $paczka['rachunek'];
                $dane['tytul'] = $paczka['tytul'];
                $dane['kategoria'] = $paczka['typ'];
                if(isset($paczka['dwartosc']))
                $dane['wartosc'] = $paczka['dwartosc'];
                $dane['deklaracjawar']=$paczka['deklaracjawar'];
                if(isset($paczka['dpotwierdzenie']))
                $dane['potwierdzenie'] = $paczka['dpotwierdzenie'];
                if(isset($paczka['sprawdz']))
                $dane['sprawdz'] = $paczka['sprawdz'];
                if(isset($paczka['ostroznie']))
                $dane['ostroznie']=$paczka['ostroznie'];
                
                
            }
            elseif($adres['typ']=='polecona'){
                $paczka=$this->_request->getPost('polecona');
                //die();
                $dane['kategoria'] = $paczka['typ'];
                 if(isset($paczka['dpotwierdzenie']))
                $dane['potwierdzenie'] = $paczka['dpotwierdzenie'];
            }
            $listy = new Listy();
			if(!empty($dane['deal_id']))
			{
				$dodaj = $listy->dodaj($dane);
			} else {
				$dodaj = $listy->dodaj($dane, $zamowienie);
			}
        
            $this->_redirect('/admin/pocztapolska/drukuj/id/'.$dodaj);
           
        }

        $poczta = $pocztapolska->wypiszOne();
        $this->view->poczta = $poczta;
        $this->view->dane = $dane1;
    }

    public function drukujAction() {
        $pocztapolska = new Pocztapolska();

        $ustawienia = $pocztapolska->wypiszOne($this->view->adminID);
        //print_r($ustawienia);
        $id = $this->_request->getParam('id');
        //$zamowienia = new Zamowienia();
        $listy = new Listy();
        $listy->id = $id;
        $poczta = $listy->wypiszJeden();
        //print_r($poczta);
        $slownie = new AmountInWords();
        $slow = $slownie->get($poczta['deklaracjawar']);
        $slow2 = $slownie->get($poczta['kwota']);
        $this->view->slow = $slow;
        $this->view->slow2 = $slow2;
        $this->view->poczta = $poczta;
        $this->view->ustaw = $ustawienia;
    }
	
	public function dodajhurtallegroAction() {
		$ids = $_SESSION['drukuj_przesylka_pobraniowa'];
		unset($_SESSION['drukuj_przesylka_pobraniowa_listy']);
		$_SESSION['drukuj_przesylka_pobraniowa_listy'] = array();
        $pocztapolska = new Pocztapolska();
        $zamowienia = new AllegroZamowienia();
        if (!empty($ids)) {
			foreach($ids as $id)
			{
				$dane1[$id] = $zamowienia->selectWybraneZamowienie($id);
			}
        }
        
        if($this->_request->getPost('dane'))
		{
            $adresy = $this->_request->getPost('dane');
			
			foreach($adresy as $key => $adres)
			{
				$dane['deal_id'] = $adres['deal_id'];
				$dane['nazwisko'] = $adres['nazwisko1'];
				$dane['typ'] = $adres['typ'];
				$dane['mail'] = $adres['email'];
				$dane['nazwa'] = $adres['nazwa_firmy1'];
				$dane['imie'] = $adres['imie1'];
				$dane['telefon'] = $adres['telefon1'];
				$dane['ulica'] = $adres['ulica1'];
				$dane['nr'] = $adres['nr1'];
				$dane['mieszkanie'] = $adres['mieszkanie1'];
				$dane['kod'] = $adres['kod1'];
				$dane['miasto'] = $adres['miasto1'];
				$dane['komorka'] = $adres['komorka1'];
				
				if($adres['typ']=='pobraniowa')
				{
					$paczka = $this->_request->getPost('pobraniowa');
					$dane['kwota'] = $paczka[$key]['kwota'];
					$dane['pobranietyp']=$paczka[$key]['sposob'];
					$dane['rachunek'] = $paczka[$key]['rachunek'];
					$dane['tytul'] = $paczka[$key]['tytul'];
					$dane['kategoria'] = $paczka[$key]['typ'];
					if(isset($paczka[$key]['dwartosc']))
					$dane['wartosc'] = $paczka[$key]['dwartosc'];
					$dane['deklaracjawar']=$paczka[$key]['deklaracjawar'];
					if(isset($paczka[$key]['dpotwierdzenie']))
					$dane['potwierdzenie'] = $paczka[$key]['dpotwierdzenie'];
					if(isset($paczka[$key]['sprawdz']))
					$dane['sprawdz'] = $paczka[$key]['sprawdz'];
					if(isset($paczka[$key]['ostroznie']))
					$dane['ostroznie']=$paczka[$key]['ostroznie'];
				}
				$listy = new Listy();
				
				if(!empty($dane['deal_id']))
				{
					$dodaj = $listy->dodaj($dane);
				} else {
					$dodaj = $listy->dodaj($dane, $key);
				}
				
				array_push($_SESSION['drukuj_przesylka_pobraniowa_listy'], $dodaj);
				unset($dane);
				
			}
			unset($_SESSION['drukuj_przesylka_pobraniowa']);
			$this->_redirect('/admin/pocztapolska/drukujhurt');
        }

        $poczta = $pocztapolska->wypiszOne();
        $this->view->poczta = $poczta;
        $this->view->dane1 = $dane1;
    }
	
	public function drukujhurtAction() {
		$ids = $_SESSION['drukuj_przesylka_pobraniowa_listy'];
		
        $pocztapolska = new Pocztapolska();

        $ustawienia = $pocztapolska->wypiszOne($this->view->adminID);
       
    
        $listy = new Listy();
		$slownie = new AmountInWords();
		//print_r($ids);die();
		foreach($ids as $id)
		{
			$listy->id = $id;
			$poczta[$id] = $listy->wypiszJeden();
			
			$slow[$id] = $slownie->get($poczta[$id]['deklaracjawar']);
			$slow2[$id] = $slownie->get($poczta[$id]['kwota']);
		}
        
        
        $this->view->slow = $slow;
        $this->view->slow2 = $slow2;
        $this->view->poczta2 = $poczta;
        $this->view->ustaw = $ustawienia;
		
		$this->view->pocztatyp = "pobraniowa";
    }

    public function ustawieniawebapiAction() {
        $info = new Pocztapolskakonto($module = 'admin');
        $en = new ElektronicznyNadawca();

        //print_r($czas);
        $zmiana = $this->_request->getPost('konto');
        if (isset($zmiana) && !empty($zmiana)) {
            if (isset($zmiana['login'])) {
                $dane['login'] = $zmiana['login'];
                $dane['konto'] = str_replace(' ', '', $zmiana['konto']);

                if (isset($zmiana['check'])) {
                    $dane['password'] = $zmiana['haslo'];
                }

                $edit = $info->edytuj($dane, $zmiana['id']);
            }
            if (isset($zmiana['checkreset'])) {
                try {
                    $haslo = new changePassword();
                    $haslo->newPassword = $zmiana['reset'];

                    $reset = $en->changePassword($haslo);
                } catch (SoapFault $error) {
                    $this->view->error = 'Błąd systemu Elektronicznego Nadawcy. Sprapoprawność danychwdź .';
                }
                //print_r($reset);
                $this->view->error = '<br />';
                if (isset($reset['error'][0])) {
                    for ($i = 0; $i < count($reset['error']); $i++) {
                        $this->view->error .= 'Błąd numer: ' . $reset['error'][$i]['errorNumber'] . ': ' . $reset['error'][$i]['errorDesc'] . '<br />';
                    }
                } elseif (count($reset['error']) > 0) {
                    $this->view->error .= 'Błąd numer: ' . $reset['error']['errorNumber'] . ': ' . $reset['error']['errorDesc'] . '<br />';
                }

                if (count($reset['error']) < 1) {
                    $dane['password'] = $zmiana['reset'];
                    $edit = $info->edytuj($dane, $zmiana['id']);
                }
            }
        }
        $wybranyurzad = $this->_getParam('wybranyurzad', 0);
        if($wybranyurzad) {
            $edit = $info->edytuj(array('urzad'=>$wybranyurzad), '1');
            $this->_redirect($this->baseUrl.'/admin/pocztapolska/ustawieniawebapi/');
        }
        
        try {
            $data = new getPasswordExpiredDate();
            $czas = $en->getPasswordExpiredDate($data);
            $urz = new getUrzedyNadania();
			$urzedy = $en->getUrzedyNadania($urz);
			
            $this->view->urzedy = $urzedy['urzedyNadania'];
            //print_r($this->view->urzedy);
            $this->view->blad_edycji = 'Data ważności hasła: ' . $czas['dataWygasniecia'] . '. Zresetuj hasło przed zakończeniem, aby przedłużyć ważność.';
        } catch (SoapFault $error) {
            $this->view->error = 'Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.';
        }


        $display = $info->pojedyncza(1);
        $this->view->display = $display;
        //print_r($display);
    }

    public function dodajwebapiAction() {
        $pocztapolska = new Pocztapolska();
        $zamowienia = new Zamowienia();
        $ppk = new Pocztapolskakonto();
        $en = new ElektronicznyNadawca();
        if ($this->_request->getParam('zamowienie')) {
            $zamowienie = $this->_request->getParam('zamowienie');
            $dane = $zamowienia->selectWybraneZamowienie($zamowienie);
			$dane['kwotazaplata'] = $dane['wartosc_zamowienia'] + $dane['koszt_dostawy'];
        }
        if (!isset($poczta))
            $poczta = $pocztapolska->wypiszOne($this->view->adminID);

        //if ($this->_request->getPost('dane')) {
        //    $dane[0] = $this->_request->getPost('dane');
        //}

        $dane1 = $this->_request->getPost('dane');
        if (isset($dane1) && !empty($dane1)) {

            $adres = new adresType();
            if (strlen($dane1['nazwa_firmy1']) > 1) {
                $nazwa = $dane1['nazwa_firmy1'];
                $nazwa2 = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
            } else {
                $nazwa = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
                $nazwa2 = '';
            }
            $adres->nazwa = $nazwa; // nazwaType
            $adres->nazwa2 = $nazwa2; // nazwa2Type
            $adres->ulica = $dane1['ulica1']; // ulicaType
            $adres->numerDomu = $dane1['nr1']; // numerDomuType
            $adres->numerLokalu = $dane1['mieszkanie1']; // numerLokaluType
            $adres->miejscowosc = $dane1['miasto1']; // miejscowoscType
            $adres->kodPocztowy = str_replace('-', '', $dane1['kod1']); // kodPocztowyType
            $adres->kraj = 'Polska'; // krajType
            $adres->telefon = str_replace(' ', '', $dane1['telefon1']); // telefonType
            $adres->email = $dane1['email']; // emailType

            if (strlen($dane1['komorka1']) > 9) {
                $ile = strlen(str_replace(' ', '', $dane1['komorka1'])) - 9;
                $num = substr(str_replace(' ', '', $dane1['komorka1']), $ile, 9);
            } else {
                $num = $dane1['komorka1'];
            }
            $adres->mobile = $num; // mobileType
            //print_r($adres);
            if ($dane1['typ'] == 'paczka') {
                $paczka = $this->_request->getPost('paczka');
                $typ = new paczkaPocztowaType();
                $typ->posteRestante = 0; // boolean
                if (isset($paczka['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $paczka['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $paczka['typ']; // kategoriaType
                $typ->gabaryt = $paczka['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $paczka['masa']) * 1000); // masaType
                if (isset($paczka['dwartosc'])) {
                    $liczba = (0 + str_replace(',', '.', $paczka['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                } else {
                    $typ->wartosc = ''; // wartoscType
                }
                if (isset($paczka['zwrot']))
                    $typ->zwrotDoslanie = 1; // boolean
                if (isset($paczka['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($paczka['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
				if(!empty($paczka['opis']))
				{
					$typ->opis = $paczka['opis'];
				}
            }
            elseif ($dane1['typ'] == 'pobraniowa') {
                $pobranie = $this->_request->getPost('pobraniowa');
                $typ = new przesylkaPobraniowaType();
                $typ1 = new pobranieType();
                $typ1->sposobPobrania = $pobranie['sposob']; // sposobPobraniaType
                $typ1->kwotaPobrania = (0 + str_replace(',', '.', $pobranie['kwota']) * 100); // kwotaPobraniaType
                $typ1->nrb = $pobranie['rachunek']; // anonymous51
                $typ1->tytulem = $pobranie['tytul']; // anonymous52
                if (isset($pobranie['sprawdz']))
                    $typ1->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = 1; // boolean


                $typ->pobranie = $typ1; // pobranieType
                $typ->posteRestante = 0; // boolean
                if (isset($pobranie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $pobranie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }


                $typ->kategoria = $pobranie['typ']; // kategoriaType
                $typ->gabaryt = $pobranie['gabaryt']; // gabarytType
                if (isset($pobranie['ostroznie']))
                    $typ->ostroznie; // boolean
                if (isset($pobranie['dwartosc'])) {
                    $liczba = (0 + str_replace(',', '.', $pobranie['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                } else {
                    $typ->wartosc = ''; // wartoscType
                }

                $typ->masa = (0 + str_replace(',', '.', $pobranie['masa']) * 1000); // masaType
				
				if(!empty($pobranie['opis']))
				{
					$typ->opis = $pobranie['opis'];
				}
            } 
			elseif ($dane1['typ'] == 'polecona') {
                $polecenie = $this->_request->getPost('polecona');
                $typ = new przesylkaPoleconaKrajowaType();
              
                $typ->posteRestante = 0; // boolean
                if (isset($polecenie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $polecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $polecenie['typ']; // kategoriaType
                $typ->gabaryt = $polecenie['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $polecenie['masa']) * 1000); // masaType
                if (isset($polecenie['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($polecenie['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
				if(!empty($polecenie['opis']))
				{
					$typ->opis = $polecenie['opis'];
				}
            } 
			elseif ($dane1['typ'] == 'firmowapolecona') {
                $firmowapolecenie = $this->_request->getPost('firmowapolecona');
                $typ = new przesylkaFirmowaPoleconaType();
                $typ->epo = new EPOSimpleType(); // EPOType
                $typ->posteRestante = 0; // boolean
                if (isset($firmowapolecenie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $firmowapolecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                
                $typ->masa = (0 + str_replace(',', '.', $firmowapolecenie['masa']) * 1000); // masaType
                if (isset($firmowapolecenie['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($firmowapolecenie['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
				if(!empty($firmowapolecenie['opis']))
				{
					$typ->opis = $firmowapolecenie['opis'];
				}
            }
            elseif ($dane1['typ'] == 'deklaracja') {
                $deklaracje = $this->_request->getPost('deklaracja');
                $typ = new przesylkaListowaZadeklarowanaWartoscType();
                $typ->posteRestante = 0; // boolean
                $liczba = (0 + str_replace(',', '.', $deklaracje['wartosc']));
                $typ->wartosc = ($liczba * 100); // wartoscType

                if (isset($deklaracje['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $deklaracje['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $deklaracje['typ']; // kategoriaType
                $typ->gabaryt = $deklaracje['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $deklaracje['masa']) * 1000); // masaType
                if (isset($deklaracje['zwrot']))
                    $typ->zwrotDoslanie = 1; // boolean
                if (isset($deklaracje['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($deklaracje['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
				if(!empty($deklaracje['opis']))
				{
					$typ->opis = $deklaracje['opis'];
				}
            }
			elseif ($dane1['typ'] == 'nierejestrowana') {
                $nierejestrowana = $this->_request->getPost('nierejestrowana');
                $typ = new listZwyklyType();
                $typ->posteRestante = 0; // boolean
                

                
                $typ->ilosc = 1;
                $typ->kategoria = $nierejestrowana['typ']; // kategoriaType
                $typ->gabaryt = $nierejestrowana['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $nierejestrowana['masa']) * 1000); // masaType
                if(!empty($nierejestrowana['opis']))
				{
					$typ->opis = $nierejestrowana['opis'];
				}
            }
			elseif ($dane1['typ'] == 'firmowanierejestrowana') {
                $firmowanierejestrowana = $this->_request->getPost('firmowanierejestrowana');
                $typ = new listZwyklyType();
                $typ->posteRestante = 0; // boolean
                $typ->ilosc = 1;
                
                $typ->masa = (0 + str_replace(',', '.', $firmowanierejestrowana['masa']) * 1000); // masaType
                if(!empty($firmowanierejestrowana['opis']))
				{
					$typ->opis = $firmowanierejestrowana['opis'];
				}
            }
			elseif ($dane1['typ'] == 'uslugakurierska'){
				$uslugakurierska = $this->_request->getPost('uslugakurierska');
				$typ = new uslugaKurierskaType();
				$typ->doreczenie = new doreczenieUslugaKurierskaType();
				$typ->termin = $uslugakurierska['termin'];
				if(!empty($uslugakurierska['masa']))
				{
					$typ->masa = new masaType();
					$typ->masa = (0 + str_replace(',', '.', $uslugakurierska['masa']) * 1000); // masaType
				}
				if(!empty($uslugakurierska['uiszczaOplate']))
				{
					switch($uslugakurierska['uiszczaOplate'])
					{
						case 'A':
							$typ->uiszczaOplate = uiszczaOplateType::ADRESAT;
						break;
						case 'N':
							$typ->uiszczaOplate = uiszczaOplateType::NADAWCA;
						break;
					}
				}
				if(!empty($uslugakurierska['godzdoreczenia']))
				{
						$typ->doreczenie->oczekiwanaGodzinaDoreczenia = $uslugakurierska['godzinaDoreczenia'];
				}
				$typ->zawartosc = $uslugakurierska['zawartosc'];
				if(!empty($uslugakurierska['pobranie']))
				{
					$typ->pobranie = new pobranieType();
					if(!empty($uslugakurierska['sposob']))
					{
						$typ->pobranie->sposobPobrania = $uslugakurierska['sposob'];
						$liczba = (0 + str_replace(',', '.', $uslugakurierska['kwota']));
						$typ->pobranie->kwotaPobrania = ($liczba * 100); // wartoscType
						
						$typ->pobranie->nrb = $uslugakurierska['rachunek'];
						$typ->pobranie->tytulem = $uslugakurierska['tytul'];
					}
				}
				if(!empty($uslugakurierska['wartosc']))
				{
					$wartosc = (0 + str_replace(',', '.', $uslugakurierska['wartosc']));
					$typ->wartosc = ($wartosc * 100); // wartoscType
				}
				if(!empty($uslugakurierska['potwierdzenieOdbioru']))
				{
					$typ->potwierdzenieOdbioru = new potwierdzenieOdbioruKurierskaType();
					$typ->potwierdzenieOdbioru->sposob = $uslugakurierska['potwierdzenieOdbioru'];
					$typ->potwierdzenieOdbioru->ilosc = $uslugakurierska['potwierdzenieOdbioruIlosc'];
				}
				if(!empty($uslugakurierska['potwierdzenieDoreczenia']))
				{
					$typ->potwierdzenieDoreczenia = new potwierdzenieDoreczeniaType();
					$typ->potwierdzenieDoreczenia->sposob = $uslugakurierska['potwierdzenieDoreczenia'];
					$typ->potwierdzenieDoreczenia->kontakt = $uslugakurierska['potwierdzenieDoreczeniaKontakt'];
				}
				if(!empty($uslugakurierska['ostroznie']))
				{
					$typ->ostroznie = '1';
				}
				if(!empty($uslugakurierska['ponadgabaryt']))
				{
					$typ->ponadgabaryt = '1';
				}
				if(!empty($uslugakurierska['sprZawrPrzezOdbiorce']))
				{
					$typ->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = '1';
				}
				if(!empty($uslugakurierska['doreczenieWeWskaznymDniu']))
				{
					$typ->doreczenie->oczekiwanyTerminDoreczenia = $uslugakurierska['doreczenieWeWskaznymDniuData'];
				}
				if(!empty($uslugakurierska['doRakWlasnych']))
				{
					$typ->doreczenie->doRakWlasnych = '1';
				}
				if(!empty($uslugakurierska['wSobote']))
				{
					$typ->doreczenie->wSobote = '1';
					$typ->doreczenie->oczekiwanaGodzinaDoreczenia = null;
				}
				if(!empty($uslugakurierska['odbiorPrzesylkiOdNadawcyType']))
				{
					$typ->odbiorPrzesylkiOdNadawcy = new odbiorPrzesylkiOdNadawcyType();
					$typ->odbiorPrzesylkiOdNadawcy->wSobote = '1';
				}
				if(!empty($uslugakurierska['ubezpieczenie']))
				{
					$typ->ubezpieczenie = new ubezpieczenieType();
					$typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
					$typ->ubezpieczenie->kwota = $uslugakurierska['ubezpieczenieKwota'];
				}
				if(!empty($uslugakurierska['dokZwrotDiv']))
				{
					$typ->zwrotDokumentow = new zwrotDokumentowKurierskaType();
					$typ->zwrotDokumentow->rodzajList = new rodzajListType();
					//$typ->zwrotDokumentow->rodzajPaczka = new terminZwrotDokumentowPaczkowaType();
					//$typ->zwrotDokumentow->rodzajPocztex = new terminZwrotDokumentowPaczkowaType();
					switch($uslugakurierska['zwrotDokumentow'])
					{
						case 'LZP':
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
						break;
						case 'LZE':
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
						break;
						case 'LPP':
							$typ->zwrotDokumentow->rodzajList->polecony = '1';
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
						break;
						case 'LPE':
							$typ->zwrotDokumentow->rodzajList->polecony = '1';
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
						break;
						case 'P24':
							$typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_24;
						break;
						case 'P48':
							$typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_48;
						break;
						case 'KE24':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::EKSPRES24;
						break;
						case 'KMD3GD5KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_5KM;
						break;
						case 'KMD3GD10KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_10KM;
						break;
						case 'KMD3GD15KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_15KM;
						break;
						case 'KMD3GP15KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_POWYZEJ_15KM;
						break;
						case 'KBD20KG':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::BEZPOSREDNI_DO_20KG;
						break;
						
					}
				}
				if(!empty($uslugakurierska['numerPrzesylkiKlienta']))
				{
					$typ->numerPrzesylkiKlienta = $uslugakurierska['numerPrzesylkiKlienta'];
				}
				if(!empty($uslugakurierska['opis']))
				{
					$typ->opis = $uslugakurierska['opis'];
				}
			}
			elseif ($dane1['typ'] == 'przesylkakurierska48'){ //przesylka biznesowa
				$przesylkakurierska48 = $this->_request->getPost('przesylkakurierska48');
				$typ = new przesylkaBiznesowaType();
				if(!empty($przesylkakurierska48['pobranie']))
				{
					$typ->pobranie = new pobranieType();
					if(!empty($przesylkakurierska48['sposob']))
					{
						$typ->pobranie->sposobPobrania = $przesylkakurierska48['sposob'];
						$kwota = (0 + str_replace(',', '.', $przesylkakurierska48['kwota']));
						$typ->pobranie->kwotaPobrania = ($kwota * 100); // wartoscType
						
						$typ->pobranie->nrb = $przesylkakurierska48['rachunek'];
						$typ->pobranie->tytulem = $przesylkakurierska48['tytul'];
					}
				}
				if(!empty($przesylkakurierska48['gabaryt']))
				{
					switch($przesylkakurierska48['gabaryt'])
					{
						case 'XXL':
							$typ->gabaryt = gabarytBiznesowaType::XXL;
						break;
					}
				}
				if(!empty($przesylkakurierska48['wartosc']) && !empty($przesylkakurierska48['masa']))
				{
					$wartosc = (0 + str_replace(',', '.', $przesylkakurierska48['wartosc']));
					$typ->wartosc = ($wartosc * 100); // wartoscType
					$typ->masa = (0 + str_replace(',', '.', $przesylkakurierska48['masa']) * 1000); // masaType
				}
				if(!empty($przesylkakurierska48['ostroznie']))
				{
					$typ->ostroznie = '1';
				}
				if(!empty($przesylkakurierska48['ubezpieczenie']))
				{
					$typ->ubezpieczenie = new ubezpieczenieType();
					$typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
					$typ->ubezpieczenie->kwota = $przesylkakurierska48['ubezpieczenieKwota'];
				}
				if(!empty($przesylkakurierska48['opis']))
				{
					$typ->opis = $przesylkakurierska48['opis'];
				}
			}

            // print_r($typ);
            try {
				$oPoczta = new Pocztapolskakonto();
				$Poczta = $oPoczta->pojedyncza(1);
				if(!empty($Poczta['data_nowego_zbioru']))
				{
					if($Poczta['data_nowego_zbioru'] !== date('Y-m-d') || empty($Poczta['idBufor']))
					{
						$getUrzedyNadania = new getUrzedyNadania();
						$UrzedyNadania = $en->getUrzedyNadania($getUrzedyNadania);
						//print_r($UrzedyNadania['urzedyNadania']['urzadNadania']); die();
						$buforType = new buforType();
						$buforType->dataNadania = date('Y-m-d');
						$buforType->active = '1';
						$buforType->urzadNadania = $UrzedyNadania['urzedyNadania']['urzadNadania'];
						$createBufor = new createEnvelopeBufor();
						$createBufor->bufor = $buforType;
						$newBufor = $en->createEnvelopeBufor($createBufor);
						
					
						$zmiany['data_nowego_zbioru'] = $newBufor['createdBufor']['dataNadania'];
						$zmiany['idBufor'] = $newBufor['createdBufor']['idBufor'];
						$Poczta = $oPoczta->edytuj($zmiany , 1);
					}
				}
			
				$Poczta = $oPoczta->pojedyncza(1);
			
                $guid = new getGuid();
                $guid->ilosc = 1;
                $gui = $en->getGuid($guid);
                $typ->guid = $gui['guid'];
                
                //$typ->guid = $gui;
                
                    $typ->adres = $adres;
               
                $add = new addShipment();
                
                $add->przesylki[] = $typ;
				$add->idBufor = $Poczta['idBufor'];
                //print_r($add);
                //die();
                $wyslij = $en->addShipment($add);
                //print_r($wyslij);
                if (empty($wyslij['retval']['error'])) {
                    $this->view->blad_edycji = '<div class="k_ok">Poprawnie wysłano przesyłkę.</div>';
                    $this->view->error = '';
                    $this->view->error .= 'Numer nadania: ' . $wyslij['retval']['numerNadania'];
                    $this->view->error .= '<br />Numer GUID: ' . $wyslij['retval']['guid'];

                    $zam['przesylka'] = $wyslij['retval']['numerNadania'];
                    $zamow = new Zamowienia($module = 'admin');
                    $edit = $zamow->edytuj($zamowienie, $zam);
                    
                    $this->getResponse()->setHeader('Refresh', '1; URL=' . $this->baseUrl . '/admin/zamowienia/wypisz/tryb/aktualne/mode/all');
                } else {
                    $this->view->blad_edycji = 'Błąd danych. Przesyłka nie została zatwierdzona.';
                    $this->view->error = '';
                    if (isset($wyslij['retval']['error']['0'])) {
                        for ($i = 0; $i < count($wyslij['retval']['error']); $i++) {
                            $this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error'][$i]['errorNumber'] . ': ' . $wyslij['retval']['error'][$i]['errorDesc'];
                        }
                    } else {
                        $this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error']['errorNumber'] . ': ' . $wyslij['retval']['error']['errorDesc'];
                    }
                }
            } catch (SoapFault $error) {
                $this->view->error = 'Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.';
            }
        }
		if(!empty($dane['kurier']))
		{
			$this->view->error .= 'Przesyłka dla tego zamówienia była już tworzona. Sprawdź to żeby nie wysłać jeszcze raz tego samego.';
		}
        $this->view->ppk = $ppk->pojedyncza('1');
        $this->view->poczta = $poczta;
        $this->view->dane = $dane;
    }

    public function buforAction() {
        $en = new ElektronicznyNadawca();
		//$getEnvelopeBuforList = new getEnvelopeBuforList();
		//$EnvelopeBuforList = $en->getEnvelopeBuforList($getEnvelopeBuforList);
        //print_r($EnvelopeBuforList);
        //print_r($bufor);
        if ($this->_request->getParam('iddel')) {
            try {
                $guid = $this->_request->getParam('iddel');
				$oPoczta = new Pocztapolskakonto();
				$Poczta = $oPoczta->pojedyncza(1);
                $type = new clearEnvelopeByGuids();
                $type->guid = $guid;
				$type->idBufor = $Poczta['idBufor'];
                $usun = $en->clearEnvelopeByGuids($type);

                $this->view->error = '<div class="k_ok">Poprawnie usunięto pozycję z bufora.</div>';
            } catch (SoapFault $error) {
                $this->view->error = 'Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.';
            }
        }
        if ($this->_request->getParam('idwydruk')) {
            try {
				//$this->_helper->viewRenderer->setNoRender();
				$oPoczta = new Pocztapolskakonto();
				$Poczta = $oPoczta->pojedyncza(1);
                $guid = $this->_request->getParam('idwydruk');
                $type = new getAddresLabelByGuid();
                $type->guid = $guid;
				$type->idBufor = $Poczta['idBufor'];
                $usun = $en->getAddresLabelByGuid($type);
				
                $this->parsePdfPage($usun['content']['pdfContent'], 'druk_' . $guid);
                //$OUTPUT = "output_decoded.pdf";
                //unset($usun);
                //$bin = base64_decode($usun['content']['pdfContent']);
                //file_put_contents($OUTPUT, $bin);
                //print base64_encode(trim($usun['content']['pdfContent']));
                $this->view->error = '<div class="k_ok">Pobrano plik.</div>';
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.</div>';
            }
        }
        if ($this->_request->getPost('wyslijwszystko')) {
            try {
				$oPoczta = new Pocztapolskakonto();
				$Poczta = $oPoczta->pojedyncza(1);
                $wyslij = new sendEnvelope();
                $wyslij->urzadNadania = $this->_request->getPost('urzad');
				$wyslij->idBufor = $Poczta['idBufor'];
                $wyslano = $en->sendEnvelope($wyslij);

                //print_r($wyslano);

                if (empty($wyslano['error'])) {
                    $this->view->blad_edycji = '<div class="k_ok">Poprawnie wysłano przesyłki.</div>';
                } else {
                    $this->view->blad_edycji = '<div class="k_blad">Błąd danych. Przesyłka nie została zatwierdzona.</div>';
                    $this->view->error = '';
                    if (isset($wyslano['error']['0'])) {
                        for ($i = 0; $i < count($wyslano['error']); $i++) {
                            $this->view->error .= '<br />Błąd numer: ' . $wyslano['error'][$i]['errorNumber'] . ': ' . $wyslano['error'][$i]['errorDesc'];
                        }
                    } else {
                        $this->view->error .= '<br />Błąd numer: ' . $wyslano['error']['errorNumber'] . ': ' . $wyslano['error']['errorDesc'];
                    }
                }
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.</div>';
            }
        }
        if ($this->_request->getPost('kasujwszystko')) {
            try {
				$oPoczta = new Pocztapolskakonto();
				$Poczta = $oPoczta->pojedyncza(1);
                $czysc = new clearEnvelope();
				$czysc->idBufor = $Poczta['idBufor'];
                $wyczysc = $en->clearEnvelope($czysc);
				$getEnvelopeBuforList = new getEnvelopeBuforList();
				$BuforList = $en->getEnvelopeBuforList($getEnvelopeBuforList);
				$Poczta = $oPoczta->edytuj(array('idBufor' => null),1);
				$this->view->error = '<div class="k_ok" >Bufor został skasowany.</div>';
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.</div>';
            }
        }


        try {
            $ur = new getUrzedyNadania();
            $urzedy = $en->getUrzedyNadania($ur);
            $parametry = new getEnvelopeBufor();
        $bufor = $en->getEnvelopeBufor($parametry);
            //print_r($bufor);
        } catch (SoapFault $error) {
            $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.</div>';
        }
        
        $this->view->urzedy = $urzedy['urzedyNadania'];
        $this->view->bufor = $bufor['przesylka'];
    }
    
    public function wyslaneAction(){
        $en = new ElektronicznyNadawca();
        $parametry = new getEnvelopeList();
        $parametry->endDate = date('Y-m-d');
        $parametry->startDate = date('Y-m-d', strtotime(date('Y-m-d').' -30 day')); ;
        $bufor = $en->getEnvelopeList($parametry);
        //print_r($bufor);
        $this->view->bufor = $bufor['envelopes'];
        if ($this->_request->getParam('idnalepki')) {
            try {
                $guid = $this->_request->getParam('idnalepki');
                $type = new getAddresLabelCompact();
                $type->idEnvelope = $guid;
                $usun = $en->getAddresLabelCompact($type);
                //print_r($usun);die();
                //print_r($usun['content']['pdfContent']);
                // $hash = $usun['content']['pdfContent'];
                if(empty($usun['content'][0])){
                    $this->parsePdfPage($usun['pdfContent'], 'nalepki_adresowe_' . $guid);
                }else{
                    for($i=0;$i<count($usun['content']);$i++){
                        //$this->parsePdfPage(base64_decode($usun['content'][$i]['pdfContent']), 'nalepki_adresowe_' . $guid);
                    //print_r($usun['content'][$i]['pdfContent']);
                        
                        $value = base64_decode($usun['content'][$i]['pdfContent'], true);
                        $file = fopen('../public/etykieta_adresowa_' . $i . '.pdf', 'w');
                        fwrite($file, $value);
                       
                        
                    }
                }
                
                
                //$OUTPUT = "output_decoded.pdf";
                //unset($usun);
                //$bin = base64_decode($usun['content']['pdfContent']);
                //file_put_contents($OUTPUT, $bin);
                //print base64_encode(trim($usun['content']['pdfContent']));
                $this->view->error = '<div class="k_ok">Pobrano plik.</div>';
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.</div>';
            }
        }
        if ($this->_request->getParam('idksiazka')) {
            try {
                $guid = $this->_request->getParam('idksiazka');
                $type = new getOutboxBook();
                $type->idEnvelope = $guid;
                $usun = $en->getOutboxBook($type);
                //print_r($usun);
                // $hash = $usun['content']['pdfContent'];
                $this->parsePdfPage($usun['pdfContent'], 'ksiazka_nadawcza_' . $guid);
                //$OUTPUT = "output_decoded.pdf";
                //unset($usun);
                //$bin = base64_decode($usun['content']['pdfContent']);
                //file_put_contents($OUTPUT, $bin);
                //print base64_encode(trim($usun['content']['pdfContent']));
                $this->view->error = '<div class="k_ok">Pobrano plik.</div>';
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.</div>';
            }
        }
        if ($this->_request->getParam('idpoczta')) {
            try {
                $guid = $this->_request->getParam('idpoczta');
                $type = new getFirmowaPocztaBook();
                $type->idEnvelope = $guid;
                $usun = $en->getFirmowaPocztaBook($type);
                //print_r($usun);
                // $hash = $usun['content']['pdfContent'];
                $this->parsePdfPage($usun['pdfContent'], 'poczta_firmowa_' . $guid);
                //$OUTPUT = "output_decoded.pdf";
                //unset($usun);
                //$bin = base64_decode($usun['content']['pdfContent']);
                //file_put_contents($OUTPUT, $bin);
                //print base64_encode(trim($usun['content']['pdfContent']));
                $this->view->error = '<div class="k_ok">Pobrano plik.</div>';
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.</div>';
            }
        }
        
    }

    protected function parsePdfPage($xml, $filename = 'file') {
        Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        header("Content-Disposition: inline; filename=$filename.pdf");
        header("Content-type: application/x-pdf");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Description: File Transfer");

        echo $xml;
    }
    
    public function dodajwebapiallegroAction() {
        $pocztapolska = new Pocztapolska();
        $zamowienia = new AllegroZamowienia();
        $ppk = new Pocztapolskakonto();
        $en = new ElektronicznyNadawca();
        if ($this->_request->getParam('zamowienie')) {
            $zamowienie = $this->_request->getParam('zamowienie');
            $dane = $zamowienia->selectWybraneZamowienie($zamowienie);
        }
        if (!isset($poczta))
            $poczta = $pocztapolska->wypiszOne($this->view->adminID);

        //if ($this->_request->getPost('dane')) {
        //    $dane[0] = $this->_request->getPost('dane');
        //}

        $dane1 = $this->_request->getPost('dane');
        if (isset($dane1) && !empty($dane1)) {

            $adres = new adresType();
            if (strlen($dane1['nazwa_firmy1']) > 1) {
                $nazwa = $dane1['nazwa_firmy1'];
                $nazwa2 = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
            } else {
                $nazwa = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
                $nazwa2 = '';
            }
            $adres->nazwa = $nazwa; // nazwaType
            $adres->nazwa2 = $nazwa2; // nazwa2Type
            $adres->ulica = $dane1['ulica1']; // ulicaType
            $adres->numerDomu = $dane1['nr1']; // numerDomuType
            $adres->numerLokalu = $dane1['mieszkanie1']; // numerLokaluType
            $adres->miejscowosc = $dane1['miasto1']; // miejscowoscType
            $adres->kodPocztowy = str_replace('-', '', $dane1['kod1']); // kodPocztowyType
            $adres->kraj = 'Polska'; // krajType
            $adres->telefon = str_replace(' ', '', $dane1['telefon1']); // telefonType
            $adres->email = $dane1['email']; // emailType

            if (strlen($dane1['komorka1']) > 9) {
                $ile = strlen(str_replace(' ', '', $dane1['komorka1'])) - 9;
                $num = substr(str_replace(' ', '', $dane1['komorka1']), $ile, 9);
            } else {
                $num = $dane1['komorka1'];
            }
            $adres->mobile = $num; // mobileType
            //print_r($adres);
            if ($dane1['typ'] == 'paczka') {
                $paczka = $this->_request->getPost('paczka');
                $typ = new paczkaPocztowaType();
                $typ->posteRestante = 0; // boolean
                if (isset($paczka['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $paczka['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $paczka['typ']; // kategoriaType
                $typ->gabaryt = $paczka['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $paczka['masa']) * 1000); // masaType
                if (isset($paczka['dwartosc'])) {
                    $liczba = (0 + str_replace(',', '.', $paczka['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                } else {
                    $typ->wartosc = ''; // wartoscType
                }
                if (isset($paczka['zwrot']))
                    $typ->zwrotDoslanie = 1; // boolean
                if (isset($paczka['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($paczka['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
            }
            elseif ($dane1['typ'] == 'pobraniowa') {
                $pobranie = $this->_request->getPost('pobraniowa');
                $typ = new przesylkaPobraniowaType();
                $typ1 = new pobranieType();
                $typ1->sposobPobrania = $pobranie['sposob']; // sposobPobraniaType
                $typ1->kwotaPobrania = (0 + str_replace(',', '.', $pobranie['kwota']) * 100); // kwotaPobraniaType
                $typ1->nrb = $pobranie['rachunek']; // anonymous51
                $typ1->tytulem = $pobranie['tytul']; // anonymous52
                if (isset($pobranie['sprawdz']))
                    $typ1->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = 1; // boolean


                $typ->pobranie = $typ1; // pobranieType
                $typ->posteRestante = 0; // boolean
                if (isset($pobranie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $pobranie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }


                $typ->kategoria = $pobranie['typ']; // kategoriaType
                $typ->gabaryt = $pobranie['gabaryt']; // gabarytType
                if (isset($pobranie['ostroznie']))
                    $typ->ostroznie; // boolean
                if (isset($pobranie['dwartosc'])) {
                    $liczba = (0 + str_replace(',', '.', $pobranie['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                } else {
                    $typ->wartosc = ''; // wartoscType
                }

                $typ->masa = (0 + str_replace(',', '.', $pobranie['masa']) * 1000); // masaType
				if(!empty($pobranie['opis']))
				{
					$typ->opis = $pobranie['opis'];
				}
            } 
			elseif ($dane1['typ'] == 'polecona') {
                $polecenie = $this->_request->getPost('polecona');
                $typ = new przesylkaPoleconaKrajowaType();
                $typ->epo = new EPOSimpleType(); // EPOType
                $typ->posteRestante = 0; // boolean
                if (isset($polecenie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $polecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $polecenie['typ']; // kategoriaType
                $typ->gabaryt = $polecenie['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $polecenie['masa']) * 1000); // masaType
                if (isset($polecenie['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($polecenie['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
				if(!empty($polecenie['opis']))
				{
					
					$typ->opis = $polecenie['opis'];
				}
				
            }
			elseif ($dane1['typ'] == 'firmowapolecona') {
                $firmowapolecenie = $this->_request->getPost('firmowapolecona');
                $typ = new przesylkaFirmowaPoleconaType();
                $typ->posteRestante = 0; // boolean
                if (isset($firmowapolecenie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $firmowapolecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->masa = (0 + str_replace(',', '.', $firmowapolecenie['masa']) * 1000); // masaType
                if (isset($firmowapolecenie['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($firmowapolecenie['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
				if(!empty($firmowapolecenie['opis']))
				{
					$typ->opis = $firmowapolecenie['opis'];
				}
            }
            elseif ($dane1['typ'] == 'deklaracja') {
                $deklaracje = $this->_request->getPost('deklaracja');
                $typ = new przesylkaListowaZadeklarowanaWartoscType();
                $typ->posteRestante = 0; // boolean
                $liczba = (0 + str_replace(',', '.', $deklaracje['wartosc']));
                $typ->wartosc = ($liczba * 100); // wartoscType

                if (isset($deklaracje['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $deklaracje['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $deklaracje['typ']; // kategoriaType
                $typ->gabaryt = $deklaracje['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $deklaracje['masa']) * 1000); // masaType
                if (isset($deklaracje['zwrot']))
                    $typ->zwrotDoslanie = 1; // boolean
                if (isset($deklaracje['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($deklaracje['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
				if(!empty($deklaracje['opis']))
				{
					$typ->opis = $deklaracje['opis'];
				}
            }
			elseif ($dane1['typ'] == 'nierejestrowana') {
                $nierejestrowana = $this->_request->getPost('nierejestrowana');
                $typ = new listZwyklyType();
                $typ->posteRestante = 0; // boolean
                $typ->ilosc = 1;
                $typ->kategoria = $nierejestrowana['typ']; // kategoriaType
                $typ->gabaryt = $nierejestrowana['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $nierejestrowana['masa']) * 1000); // masaType
                if(!empty($nierejestrowana['opis']))
				{
					$typ->opis = $nierejestrowana['opis'];
					
				}
            }
			elseif ($dane1['typ'] == 'firmowanierejestrowana') {
                $firmowanierejestrowana = $this->_request->getPost('firmowanierejestrowana');
                $typ = new listZwyklyType();
                $typ->posteRestante = 0; // boolean
				$typ->ilosc = 1;
                
                $typ->masa = (0 + str_replace(',', '.', $firmowanierejestrowana['masa']) * 1000); // masaType
                if(!empty($firmowanierejestrowana['opis']))
				{
					$typ->opis = $firmowanierejestrowana['opis'];
				}
            }
			elseif ($dane1['typ'] == 'uslugakurierska'){
				$uslugakurierska = $this->_request->getPost('uslugakurierska');
				$typ = new uslugaKurierskaType();
				$typ->doreczenie = new doreczenieUslugaKurierskaType();
				$typ->termin = $uslugakurierska['termin'];
				if(!empty($uslugakurierska['masa']))
				{
					$typ->masa = new masaType();
					$typ->masa = (0 + str_replace(',', '.', $uslugakurierska['masa']) * 1000); // masaType
				}
				if(!empty($uslugakurierska['uiszczaOplate']))
				{
					switch($uslugakurierska['uiszczaOplate'])
					{
						case 'A':
							$typ->uiszczaOplate = uiszczaOplateType::ADRESAT;
						break;
						case 'N':
							$typ->uiszczaOplate = uiszczaOplateType::NADAWCA;
						break;
					}
				}
				if(!empty($uslugakurierska['godzdoreczenia']))
				{
						$typ->doreczenie->oczekiwanaGodzinaDoreczenia = $uslugakurierska['godzinaDoreczenia'];
				}
				$typ->zawartosc = $uslugakurierska['zawartosc'];
				if(!empty($uslugakurierska['pobranie']))
				{
					$typ->pobranie = new pobranieType();
					if(!empty($uslugakurierska['sposob']))
					{
						$typ->pobranie->sposobPobrania = $uslugakurierska['sposob'];
						$liczba = (0 + str_replace(',', '.', $uslugakurierska['kwota']));
						$typ->pobranie->kwotaPobrania = ($liczba * 100); // wartoscType
						
						$typ->pobranie->nrb = $uslugakurierska['rachunek'];
						$typ->pobranie->tytulem = $uslugakurierska['tytul'];
					}
				}
				if(!empty($uslugakurierska['wartosc']))
				{
					$wartosc = (0 + str_replace(',', '.', $uslugakurierska['wartosc']));
					$typ->wartosc = ($wartosc * 100); // wartoscType
				}
				if(!empty($uslugakurierska['potwierdzenieOdbioru']))
				{
					$typ->potwierdzenieOdbioru = new potwierdzenieOdbioruKurierskaType();
					$typ->potwierdzenieOdbioru->sposob = $uslugakurierska['potwierdzenieOdbioru'];
					$typ->potwierdzenieOdbioru->ilosc = $uslugakurierska['potwierdzenieOdbioruIlosc'];
				}
				if(!empty($uslugakurierska['potwierdzenieDoreczenia']))
				{
					$typ->potwierdzenieDoreczenia = new potwierdzenieDoreczeniaType();
					$typ->potwierdzenieDoreczenia->sposob = $uslugakurierska['potwierdzenieDoreczenia'];
					$typ->potwierdzenieDoreczenia->kontakt = $uslugakurierska['potwierdzenieDoreczeniaKontakt'];
				}
				if(!empty($uslugakurierska['ostroznie']))
				{
					$typ->ostroznie = '1';
				}
				if(!empty($uslugakurierska['ponadgabaryt']))
				{
					$typ->ponadgabaryt = '1';
				}
				if(!empty($uslugakurierska['sprZawrPrzezOdbiorce']))
				{
					$typ->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = '1';
				}
				if(!empty($uslugakurierska['doreczenieWeWskaznymDniu']))
				{
					$typ->doreczenie->oczekiwanyTerminDoreczenia = $uslugakurierska['doreczenieWeWskaznymDniuData'];
				}
				if(!empty($uslugakurierska['doRakWlasnych']))
				{
					$typ->doreczenie->doRakWlasnych = '1';
				}
				if(!empty($uslugakurierska['wSobote']))
				{
					$typ->doreczenie->wSobote = '1';
					$typ->doreczenie->oczekiwanaGodzinaDoreczenia = null;
				}
				if(!empty($uslugakurierska['odbiorPrzesylkiOdNadawcyType']))
				{
					$typ->odbiorPrzesylkiOdNadawcy = new odbiorPrzesylkiOdNadawcyType();
					$typ->odbiorPrzesylkiOdNadawcy->wSobote = '1';
				}
				if(!empty($uslugakurierska['ubezpieczenie']))
				{
					$typ->ubezpieczenie = new ubezpieczenieType();
					$typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
					$typ->ubezpieczenie->kwota = $uslugakurierska['ubezpieczenieKwota'];
				}
				if(!empty($uslugakurierska['dokZwrotDiv']))
				{
					$typ->zwrotDokumentow = new zwrotDokumentowKurierskaType();
					$typ->zwrotDokumentow->rodzajList = new rodzajListType();
					//$typ->zwrotDokumentow->rodzajPaczka = new terminZwrotDokumentowPaczkowaType();
					//$typ->zwrotDokumentow->rodzajPocztex = new terminZwrotDokumentowPaczkowaType();
					switch($uslugakurierska['zwrotDokumentow'])
					{
						case 'LZP':
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
						break;
						case 'LZE':
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
						break;
						case 'LPP':
							$typ->zwrotDokumentow->rodzajList->polecony = '1';
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
						break;
						case 'LPE':
							$typ->zwrotDokumentow->rodzajList->polecony = '1';
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
						break;
						case 'P24':
							$typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_24;
						break;
						case 'P48':
							$typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_48;
						break;
						case 'KE24':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::EKSPRES24;
						break;
						case 'KMD3GD5KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_5KM;
						break;
						case 'KMD3GD10KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_10KM;
						break;
						case 'KMD3GD15KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_15KM;
						break;
						case 'KMD3GP15KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_POWYZEJ_15KM;
						break;
						case 'KBD20KG':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::BEZPOSREDNI_DO_20KG;
						break;
						
					}
				}
				if(!empty($uslugakurierska['numerPrzesylkiKlienta']))
				{
					$typ->numerPrzesylkiKlienta = $uslugakurierska['numerPrzesylkiKlienta'];
				}
				if(!empty($uslugakurierska['opis']))
				{
					$typ->opis = $uslugakurierska['opis'];
				}
			}
			elseif ($dane1['typ'] == 'przesylkakurierska48'){ //przesylka biznesowa
				$przesylkakurierska48 = $this->_request->getPost('przesylkakurierska48');
				$typ = new przesylkaBiznesowaType();
				if(!empty($przesylkakurierska48['pobranie']))
				{
					$typ->pobranie = new pobranieType();
					if(!empty($przesylkakurierska48['sposob']))
					{
						$typ->pobranie->sposobPobrania = $przesylkakurierska48['sposob'];
						$kwota = (0 + str_replace(',', '.', $przesylkakurierska48['kwota']));
						$typ->pobranie->kwotaPobrania = ($kwota * 100); // wartoscType
						
						$typ->pobranie->nrb = $przesylkakurierska48['rachunek'];
						$typ->pobranie->tytulem = $przesylkakurierska48['tytul'];
					}
				}
				if(!empty($przesylkakurierska48['gabaryt']))
				{
					switch($przesylkakurierska48['gabaryt'])
					{
						case 'XXL':
							$typ->gabaryt = gabarytBiznesowaType::XXL;
						break;
					}
				}
				if(!empty($przesylkakurierska48['wartosc']) && !empty($przesylkakurierska48['masa']))
				{
					$wartosc = (0 + str_replace(',', '.', $przesylkakurierska48['wartosc']));
					$typ->wartosc = ($wartosc * 100); // wartoscType
					$typ->masa = (0 + str_replace(',', '.', $przesylkakurierska48['masa']) * 1000); // masaType
				}
				if(!empty($przesylkakurierska48['ostroznie']))
				{
					$typ->ostroznie = '1';
				}
				if(!empty($przesylkakurierska48['ubezpieczenie']))
				{
					$typ->ubezpieczenie = new ubezpieczenieType();
					$typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
					$typ->ubezpieczenie->kwota = $przesylkakurierska48['ubezpieczenieKwota'];
				}
				if(!empty($przesylkakurierska48['opis']))
				{
					$typ->opis = $przesylkakurierska48['opis'];
				}
			}


             //print_r($typ);die();
            try {
				$oPoczta = new Pocztapolskakonto();
				$Poczta = $oPoczta->pojedyncza(1);
				if(!empty($Poczta['data_nowego_zbioru']))
				{
					if($Poczta['data_nowego_zbioru'] !== date('Y-m-d') || empty($Poczta['idBufor']))
					{
						$getUrzedyNadania = new getUrzedyNadania();
						$UrzedyNadania = $en->getUrzedyNadania($getUrzedyNadania);
						//print_r($UrzedyNadania['urzedyNadania']['urzadNadania']); die();
						$buforType = new buforType();
						$buforType->dataNadania = date('Y-m-d');
						$buforType->active = '1';
						$buforType->urzadNadania = $UrzedyNadania['urzedyNadania']['urzadNadania'];
						$createBufor = new createEnvelopeBufor();
						$createBufor->bufor = $buforType;
						$newBufor = $en->createEnvelopeBufor($createBufor);
						
					
						$zmiany['data_nowego_zbioru'] = $newBufor['createdBufor']['dataNadania'];
						$zmiany['idBufor'] = $newBufor['createdBufor']['idBufor'];
						$Poczta = $oPoczta->edytuj($zmiany , 1);
					}
				}
			
				$Poczta = $oPoczta->pojedyncza(1);
			/*
				$getEnvelopeBuforList = new getEnvelopeBuforList();
				$BuforList = $en->getEnvelopeBuforList($getEnvelopeBuforList);
				print_r($BuforList);die();
			*/	
                $guid = new getGuid();
                $guid->ilosc = 1;
                $gui = $en->getGuid($guid);
                $typ->guid = $gui['guid'];
                //$typ->guid = $gui;
                $typ->adres = $adres;
				//print_r($typ);die();
                $add = new addShipment();
                $add->przesylki[] = $typ;
				$add->idBufor = $Poczta['idBufor'];
			
				
				
				
                $wyslij = $en->addShipment($add);
                //print_r($wyslij);die();
                if (empty($wyslij['retval']['error'])) {
                    $this->view->blad_edycji = '<div class="k_ok">Poprawnie wysłano przesyłkę.</div>';
                    $this->view->error = '';
                    $this->view->error .= 'Numer nadania: ' . $wyslij['retval']['numerNadania'];
                    $this->view->error .= '<br />Numer GUID: ' . $wyslij['retval']['guid'];

                    $zam['przesylka'] = $wyslij['retval']['numerNadania'];
                    $zam['kurier'] = 'Poczta Polska';
                    $zamow = new AllegroZamowienia($module = 'admin');
                    $edit = $zamow->edytuj($zamowienie, $zam);
                    
                    //$this->getResponse()->setHeader('Refresh', '1; URL=' . $this->baseUrl . '/admin/allegrozamowienia/wypisz/tryb/aktualne/mode/all');
                } else {
                    $this->view->blad_edycji = 'Błąd danych. Przesyłka nie została zatwierdzona.';
                    $this->view->error = '';
                    if (isset($wyslij['retval']['error']['0'])) {
                        for ($i = 0; $i < count($wyslij['retval']['error']); $i++) {
                            $this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error'][$i]['errorNumber'] . ': ' . $wyslij['retval']['error'][$i]['errorDesc'];
                        }
                    } else {
                        $this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error']['errorNumber'] . ': ' . $wyslij['retval']['error']['errorDesc'];
                    }
                }
            } catch (SoapFault $error) {
                $this->view->error = 'Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.';
            }
        }
        $this->view->ppk = $ppk->pojedyncza('1');
        $this->view->poczta = $poczta;
		$tmp = explode(' ', str_replace('  ', ' ', $dane['imie_korespondencja'])); //rozbij na imie i nazwisko
		$dane['imie_korespondencja'] = $tmp[0];
		$dane['nazwisko_korespondencja'] = $tmp[1];
		$tmp2 = explode(' ', $dane['ulica_korespondencja']); //rozbij  na ulice i numer
		$tmp3 = explode('/', end($tmp2)); //rozbij na nr domu i lokalu
		if(!empty($tmp3[1]))
		{
			$dane['nr_korespondencja'] = $tmp3[0];
			$dane['mieszkanie_korespondencja'] = $tmp3[1];
			array_pop($tmp2);
			$dane['ulica_korespondencja'] = implode(' ', $tmp2);
		} else {
			$dane['nr_korespondencja'] = end($tmp2);
			array_pop($tmp2);
			$dane['ulica_korespondencja'] = implode(' ', $tmp2);
		}
		$dane['kwotazaplata'] = str_replace('.', ',', $dane['kwotazaplata']);
		if(!empty($dane['kurier']))
		{
			$this->view->error .= 'Przesyłka dla tego zamówienia była już tworzona. Sprawdź to żeby nie wysłać jeszcze raz tego samego.';
		}
        $this->view->dane = $dane;
    }
	
/* dodawanie hurtowo listow przewozowych dla Poczty polskiej */
	public function dodajwebapiallegrohurtAction() 
	{
		$idsy = $_SESSION['poczta_polska_hurtem'];
		//print_r($idsy);die();
		if(!empty($idsy))
		{
			
			
				$pocztapolska = new Pocztapolska();
				$zamowienia = new AllegroZamowienia();
				$ppk = new Pocztapolskakonto();
				$en = new ElektronicznyNadawca();
				
				if (!empty($idsy[0])) {
					$zamowienie = $idsy[0];
					$dane = $zamowienia->selectWybraneZamowienie($zamowienie);
				}
				
				if (!isset($poczta))
					$poczta = $pocztapolska->wypiszOne($this->view->adminID);

				

				$dane1 = $this->_request->getPost('dane');
				if (isset($dane1) && !empty($dane1)) 
				{

					$adres = new adresType();
					if (strlen($dane1['nazwa_firmy1']) > 1) {
						$nazwa = $dane1['nazwa_firmy1'];
						$nazwa2 = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
					} else {
						$nazwa = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
						$nazwa2 = '';
					}
					$adres->nazwa = $nazwa; // nazwaType
					$adres->nazwa2 = $nazwa2; // nazwa2Type
					$adres->ulica = $dane1['ulica1']; // ulicaType
					$adres->numerDomu = $dane1['nr1']; // numerDomuType
					$adres->numerLokalu = $dane1['mieszkanie1']; // numerLokaluType
					$adres->miejscowosc = $dane1['miasto1']; // miejscowoscType
					$adres->kodPocztowy = str_replace('-', '', $dane1['kod1']); // kodPocztowyType
					$adres->kraj = 'Polska'; // krajType
					$adres->telefon = str_replace(' ', '', $dane1['telefon1']); // telefonType
					$adres->email = $dane1['email']; // emailType

					if (strlen($dane1['komorka1']) > 9) {
						$ile = strlen(str_replace(' ', '', $dane1['komorka1'])) - 9;
						$num = substr(str_replace(' ', '', $dane1['komorka1']), $ile, 9);
					} else {
						$num = $dane1['komorka1'];
					}
					$adres->mobile = $num; // mobileType
					//print_r($adres);
					if ($dane1['typ'] == 'paczka') {
						$paczka = $this->_request->getPost('paczka');
						$typ = new paczkaPocztowaType();
						$typ->posteRestante = 0; // boolean
						if (isset($paczka['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $paczka['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}

						$typ->kategoria = $paczka['typ']; // kategoriaType
						$typ->gabaryt = $paczka['gabaryt']; // gabarytType
						$typ->masa = (0 + str_replace(',', '.', $paczka['masa']) * 1000); // masaType
						if (isset($paczka['dwartosc'])) {
							$liczba = (0 + str_replace(',', '.', $paczka['wartosc']));
							$typ->wartosc = ($liczba * 100); // wartoscType
						} else {
							$typ->wartosc = ''; // wartoscType
						}
						if (isset($paczka['zwrot']))
							$typ->zwrotDoslanie = 1; // boolean
						if (isset($paczka['biblioteka']))
							$typ->egzemplarzBiblioteczny = 1; // boolean
						if (isset($paczka['ociemniali']))
							$typ->dlaOciemnialych = 1; // boolean
					}
					elseif ($dane1['typ'] == 'pobraniowa') {
						$pobranie = $this->_request->getPost('pobraniowa');
						$typ = new przesylkaPobraniowaType();
						$typ1 = new pobranieType();
						$typ1->sposobPobrania = $pobranie['sposob']; // sposobPobraniaType
						$typ1->kwotaPobrania = (0 + str_replace(',', '.', $pobranie['kwota']) * 100); // kwotaPobraniaType
						$typ1->nrb = $pobranie['rachunek']; // anonymous51
						$typ1->tytulem = $pobranie['tytul']; // anonymous52
						if (isset($pobranie['sprawdz']))
							$typ1->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = 1; // boolean


						$typ->pobranie = $typ1; // pobranieType
						$typ->posteRestante = 0; // boolean
						if (isset($pobranie['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $pobranie['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}


						$typ->kategoria = $pobranie['typ']; // kategoriaType
						$typ->gabaryt = $pobranie['gabaryt']; // gabarytType
						if (isset($pobranie['ostroznie']))
							$typ->ostroznie; // boolean
						if (isset($pobranie['dwartosc'])) {
							$liczba = (0 + str_replace(',', '.', $pobranie['wartosc']));
							$typ->wartosc = ($liczba * 100); // wartoscType
						} else {
							$typ->wartosc = ''; // wartoscType
						}

						$typ->masa = (0 + str_replace(',', '.', $pobranie['masa']) * 1000); // masaType
						if(!empty($pobranie['opis']))
						{
							$typ->opis = $pobranie['opis'];
						}
					} 
					elseif ($dane1['typ'] == 'polecona') {
						$polecenie = $this->_request->getPost('polecona');
						$typ = new przesylkaPoleconaKrajowaType();
						$typ->epo = new EPOSimpleType(); // EPOType
						$typ->posteRestante = 0; // boolean
						if (isset($polecenie['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $polecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}

						$typ->kategoria = $polecenie['typ']; // kategoriaType
						$typ->gabaryt = $polecenie['gabaryt']; // gabarytType
						$typ->masa = (0 + str_replace(',', '.', $polecenie['masa']) * 1000); // masaType
						if (isset($polecenie['biblioteka']))
							$typ->egzemplarzBiblioteczny = 1; // boolean
						if (isset($polecenie['ociemniali']))
							$typ->dlaOciemnialych = 1; // boolean
						if(!empty($polecenie['opis']))
						{
							$typ->opis = $polecenie['opis'];
						}
						if(!empty($polecenie['opis']))
						{
							$typ->opis = $polecenie['opis'];
						}
					}
					elseif ($dane1['typ'] == 'firmowapolecona') {
						$firmowapolecenie = $this->_request->getPost('firmowapolecona');
						$typ = new przesylkaFirmowaPoleconaType();
						$typ->posteRestante = 0; // boolean
						if (isset($firmowapolecenie['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $firmowapolecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}

						$typ->masa = (0 + str_replace(',', '.', $firmowapolecenie['masa']) * 1000); // masaType
						if (isset($firmowapolecenie['biblioteka']))
							$typ->egzemplarzBiblioteczny = 1; // boolean
						if (isset($firmowapolecenie['ociemniali']))
							$typ->dlaOciemnialych = 1; // boolean
						if(!empty($firmowapolecenie['opis']))
						{
							$typ->opis = $firmowapolecenie['opis'];
						}
					}
					elseif ($dane1['typ'] == 'deklaracja') {
						$deklaracje = $this->_request->getPost('deklaracja');
						$typ = new przesylkaListowaZadeklarowanaWartoscType();
						$typ->posteRestante = 0; // boolean
						$liczba = (0 + str_replace(',', '.', $deklaracje['wartosc']));
						$typ->wartosc = ($liczba * 100); // wartoscType

						if (isset($deklaracje['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $deklaracje['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}

						$typ->kategoria = $deklaracje['typ']; // kategoriaType
						$typ->gabaryt = $deklaracje['gabaryt']; // gabarytType
						$typ->masa = (0 + str_replace(',', '.', $deklaracje['masa']) * 1000); // masaType
						if (isset($deklaracje['zwrot']))
							$typ->zwrotDoslanie = 1; // boolean
						if (isset($deklaracje['biblioteka']))
							$typ->egzemplarzBiblioteczny = 1; // boolean
						if (isset($deklaracje['ociemniali']))
							$typ->dlaOciemnialych = 1; // boolean
						if(!empty($deklaracje['opis']))
						{
							$typ->opis = $deklaracje['opis'];
						}
					}
					elseif ($dane1['typ'] == 'nierejestrowana') {
						$nierejestrowana = $this->_request->getPost('nierejestrowana');
						$typ = new listZwyklyType();
						$typ->posteRestante = 0; // boolean
						$typ->ilosc = 1;
						$typ->kategoria = $nierejestrowana['typ']; // kategoriaType
						$typ->gabaryt = $nierejestrowana['gabaryt']; // gabarytType
						$typ->masa = (0 + str_replace(',', '.', $nierejestrowana['masa']) * 1000); // masaType
						if(!empty($nierejestrowana['opis']))
						{
							$typ->opis = $nierejestrowana['opis'];
						}
					}
					elseif ($dane1['typ'] == 'firmowanierejestrowana') {
						$firmowanierejestrowana = $this->_request->getPost('firmowanierejestrowana');
						$typ = new listZwyklyType();
						$typ->posteRestante = 0; // boolean
						$typ->ilosc = 1;
						
						$typ->masa = (0 + str_replace(',', '.', $firmowanierejestrowana['masa']) * 1000); // masaType
						if(!empty($firmowanierejestrowana['opis']))
						{
							$typ->opis = $firmowanierejestrowana['opis'];
						}
					}
					elseif ($dane1['typ'] == 'uslugakurierska'){
						$uslugakurierska = $this->_request->getPost('uslugakurierska');
						$typ = new uslugaKurierskaType();
						$typ->doreczenie = new doreczenieUslugaKurierskaType();
						$typ->termin = $uslugakurierska['termin'];
						if(!empty($uslugakurierska['masa']))
						{
							$typ->masa = new masaType();
							$typ->masa = (0 + str_replace(',', '.', $uslugakurierska['masa']) * 1000); // masaType
						}
						if(!empty($uslugakurierska['uiszczaOplate']))
						{
							switch($uslugakurierska['uiszczaOplate'])
							{
								case 'A':
									$typ->uiszczaOplate = uiszczaOplateType::ADRESAT;
								break;
								case 'N':
									$typ->uiszczaOplate = uiszczaOplateType::NADAWCA;
								break;
							}
						}
						if(!empty($uslugakurierska['godzdoreczenia']))
						{
								$typ->doreczenie->oczekiwanaGodzinaDoreczenia = $uslugakurierska['godzinaDoreczenia'];
						}
						$typ->zawartosc = $uslugakurierska['zawartosc'];
						if(!empty($uslugakurierska['pobranie']))
						{
							$typ->pobranie = new pobranieType();
							if(!empty($uslugakurierska['sposob']))
							{
								$typ->pobranie->sposobPobrania = $uslugakurierska['sposob'];
								$liczba = (0 + str_replace(',', '.', $uslugakurierska['kwota']));
								$typ->pobranie->kwotaPobrania = ($liczba * 100); // wartoscType
								
								$typ->pobranie->nrb = $uslugakurierska['rachunek'];
								$typ->pobranie->tytulem = $uslugakurierska['tytul'];
							}
						}
						if(!empty($uslugakurierska['wartosc']))
						{
							$wartosc = (0 + str_replace(',', '.', $uslugakurierska['wartosc']));
							$typ->wartosc = ($wartosc * 100); // wartoscType
						}
						if(!empty($uslugakurierska['potwierdzenieOdbioru']))
						{
							$typ->potwierdzenieOdbioru = new potwierdzenieOdbioruKurierskaType();
							$typ->potwierdzenieOdbioru->sposob = $uslugakurierska['potwierdzenieOdbioru'];
							$typ->potwierdzenieOdbioru->ilosc = $uslugakurierska['potwierdzenieOdbioruIlosc'];
						}
						if(!empty($uslugakurierska['potwierdzenieDoreczenia']))
						{
							$typ->potwierdzenieDoreczenia = new potwierdzenieDoreczeniaType();
							$typ->potwierdzenieDoreczenia->sposob = $uslugakurierska['potwierdzenieDoreczenia'];
							$typ->potwierdzenieDoreczenia->kontakt = $uslugakurierska['potwierdzenieDoreczeniaKontakt'];
						}
						if(!empty($uslugakurierska['ostroznie']))
						{
							$typ->ostroznie = '1';
						}
						if(!empty($uslugakurierska['ponadgabaryt']))
						{
							$typ->ponadgabaryt = '1';
						}
						if(!empty($uslugakurierska['sprZawrPrzezOdbiorce']))
						{
							$typ->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = '1';
						}
						if(!empty($uslugakurierska['doreczenieWeWskaznymDniu']))
						{
							$typ->doreczenie->oczekiwanyTerminDoreczenia = $uslugakurierska['doreczenieWeWskaznymDniuData'];
						}
						if(!empty($uslugakurierska['doRakWlasnych']))
						{
							$typ->doreczenie->doRakWlasnych = '1';
						}
						if(!empty($uslugakurierska['wSobote']))
						{
							$typ->doreczenie->wSobote = '1';
							$typ->doreczenie->oczekiwanaGodzinaDoreczenia = null;
						}
						if(!empty($uslugakurierska['odbiorPrzesylkiOdNadawcyType']))
						{
							$typ->odbiorPrzesylkiOdNadawcy = new odbiorPrzesylkiOdNadawcyType();
							$typ->odbiorPrzesylkiOdNadawcy->wSobote = '1';
						}
						if(!empty($uslugakurierska['ubezpieczenie']))
						{
							$typ->ubezpieczenie = new ubezpieczenieType();
							$typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
							$typ->ubezpieczenie->kwota = $uslugakurierska['ubezpieczenieKwota'];
						}
						if(!empty($uslugakurierska['dokZwrotDiv']))
						{
							$typ->zwrotDokumentow = new zwrotDokumentowKurierskaType();
							$typ->zwrotDokumentow->rodzajList = new rodzajListType();
							//$typ->zwrotDokumentow->rodzajPaczka = new terminZwrotDokumentowPaczkowaType();
							//$typ->zwrotDokumentow->rodzajPocztex = new terminZwrotDokumentowPaczkowaType();
							switch($uslugakurierska['zwrotDokumentow'])
							{
								case 'LZP':
									$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
								break;
								case 'LZE':
									$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
								break;
								case 'LPP':
									$typ->zwrotDokumentow->rodzajList->polecony = '1';
									$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
								break;
								case 'LPE':
									$typ->zwrotDokumentow->rodzajList->polecony = '1';
									$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
								break;
								case 'P24':
									$typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_24;
								break;
								case 'P48':
									$typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_48;
								break;
								case 'KE24':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::EKSPRES24;
								break;
								case 'KMD3GD5KM':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_5KM;
								break;
								case 'KMD3GD10KM':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_10KM;
								break;
								case 'KMD3GD15KM':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_15KM;
								break;
								case 'KMD3GP15KM':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_POWYZEJ_15KM;
								break;
								case 'KBD20KG':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::BEZPOSREDNI_DO_20KG;
								break;
								
							}
						}
						if(!empty($uslugakurierska['numerPrzesylkiKlienta']))
						{
							$typ->numerPrzesylkiKlienta = $uslugakurierska['numerPrzesylkiKlienta'];
						}
						if(!empty($uslugakurierska['opis']))
						{
							$typ->opis = $uslugakurierska['opis'];
						}
					}
					elseif ($dane1['typ'] == 'przesylkakurierska48'){ //przesylka biznesowa
						$przesylkakurierska48 = $this->_request->getPost('przesylkakurierska48');
						$typ = new przesylkaBiznesowaType();
						if(!empty($przesylkakurierska48['pobranie']))
						{
							$typ->pobranie = new pobranieType();
							if(!empty($przesylkakurierska48['sposob']))
							{
								$typ->pobranie->sposobPobrania = $przesylkakurierska48['sposob'];
								$kwota = (0 + str_replace(',', '.', $przesylkakurierska48['kwota']));
								$typ->pobranie->kwotaPobrania = ($kwota * 100); // wartoscType
								
								$typ->pobranie->nrb = $przesylkakurierska48['rachunek'];
								$typ->pobranie->tytulem = $przesylkakurierska48['tytul'];
							}
						}
						if(!empty($przesylkakurierska48['gabaryt']))
						{
							switch($przesylkakurierska48['gabaryt'])
							{
								case 'XXL':
									$typ->gabaryt = gabarytBiznesowaType::XXL;
								break;
							}
						}
						if(!empty($przesylkakurierska48['wartosc']) && !empty($przesylkakurierska48['masa']))
						{
							$wartosc = (0 + str_replace(',', '.', $przesylkakurierska48['wartosc']));
							$typ->wartosc = ($wartosc * 100); // wartoscType
							$typ->masa = (0 + str_replace(',', '.', $przesylkakurierska48['masa']) * 1000); // masaType
						}
						if(!empty($przesylkakurierska48['ostroznie']))
						{
							$typ->ostroznie = '1';
						}
						if(!empty($przesylkakurierska48['ubezpieczenie']))
						{
							$typ->ubezpieczenie = new ubezpieczenieType();
							$typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
							$typ->ubezpieczenie->kwota = $przesylkakurierska48['ubezpieczenieKwota'];
						}
						if(!empty($przesylkakurierska48['opis']))
						{
							$typ->opis = $przesylkakurierska48['opis'];
						}
					}


					 //print_r($typ);die();
					try {
						$oPoczta = new Pocztapolskakonto();
						$Poczta = $oPoczta->pojedyncza(1);
						if(!empty($Poczta['data_nowego_zbioru']))
						{
							if($Poczta['data_nowego_zbioru'] !== date('Y-m-d') || empty($Poczta['idBufor']))
							{
								$getUrzedyNadania = new getUrzedyNadania();
								$UrzedyNadania = $en->getUrzedyNadania($getUrzedyNadania);
								//print_r($UrzedyNadania['urzedyNadania']['urzadNadania']); die();
								$buforType = new buforType();
								$buforType->dataNadania = date('Y-m-d');
								$buforType->active = '1';
								$buforType->urzadNadania = $UrzedyNadania['urzedyNadania']['urzadNadania'];
								$createBufor = new createEnvelopeBufor();
								$createBufor->bufor = $buforType;
								$newBufor = $en->createEnvelopeBufor($createBufor);
								
							
								$zmiany['data_nowego_zbioru'] = $newBufor['createdBufor']['dataNadania'];
								$zmiany['idBufor'] = $newBufor['createdBufor']['idBufor'];
								$Poczta = $oPoczta->edytuj($zmiany , 1);
							}
						}
					
						$Poczta = $oPoczta->pojedyncza(1);
					/*
						$getEnvelopeBuforList = new getEnvelopeBuforList();
						$BuforList = $en->getEnvelopeBuforList($getEnvelopeBuforList);
						print_r($BuforList);die();
					*/	
						$guid = new getGuid();
						$guid->ilosc = 1;
						$gui = $en->getGuid($guid);
						$typ->guid = $gui['guid'];
						//$typ->guid = $gui;
						$typ->adres = $adres;
						//print_r($typ);die();
						$add = new addShipment();
						$add->przesylki[] = $typ;
						$add->idBufor = $Poczta['idBufor'];
					
						
						
						
						$wyslij = $en->addShipment($add);
						//print_r($wyslij);die();
						if (empty($wyslij['retval']['error'])) {
							$this->view->blad_edycji = '<div class="k_ok">Poprawnie wysłano przesyłkę.</div>';
							$this->view->error = '';
							$this->view->error .= 'Numer nadania: ' . $wyslij['retval']['numerNadania'];
							$this->view->error .= '<br />Numer GUID: ' . $wyslij['retval']['guid'];

							$zam['przesylka'] = $wyslij['retval']['numerNadania'];
							$zam['kurier'] = 'Poczta Polska';
							$zamow = new AllegroZamowienia($module = 'admin');
							$edit = $zamow->edytuj($zamowienie, $zam);
							
							$res_idsy = array_shift($idsy);
							$_SESSION['poczta_polska_hurtem'] = $idsy;
							if(!empty($res_idsy)){
								$this->view->error .= '<br />Zostało jeszcze przesyłek do nadania: <strong>'.count($idsy).'</strong>';
								if (!empty($idsy[0])) {
									$zamowienie = $idsy[0];
									$dane = $zamowienia->selectWybraneZamowienie($zamowienie);
								}
							}
							//$this->getResponse()->setHeader('Refresh', '1; URL=' . $this->baseUrl . '/admin/allegrozamowienia/wypisz/tryb/aktualne/mode/all');
						} else {
							$this->view->blad_edycji = 'Błąd danych. Przesyłka nie została zatwierdzona.';
							$this->view->error = '';
							if (isset($wyslij['retval']['error']['0'])) {
								for ($i = 0; $i < count($wyslij['retval']['error']); $i++) {
									$this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error'][$i]['errorNumber'] . ': ' . $wyslij['retval']['error'][$i]['errorDesc'];
								}
							} else {
								$this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error']['errorNumber'] . ': ' . $wyslij['retval']['error']['errorDesc'];
							}
						}
					} catch (SoapFault $error) {
						$this->view->error = 'Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.';
					}
				}
				$this->view->ppk = $ppk->pojedyncza('1');
				$this->view->poczta = $poczta;
				$tmp = explode(' ', str_replace('  ', ' ', $dane['imie_korespondencja'])); //rozbij na imie i nazwisko
				$dane['imie_korespondencja'] = $tmp[0];
				$dane['nazwisko_korespondencja'] = $tmp[1];
				$tmp2 = explode(' ', $dane['ulica_korespondencja']); //rozbij  na ulice i numer
				$tmp3 = explode('/', end($tmp2)); //rozbij na nr domu i lokalu
				if(!empty($tmp3[1]))
				{
					$dane['nr_korespondencja'] = $tmp3[0];
					$dane['mieszkanie_korespondencja'] = $tmp3[1];
					array_pop($tmp2);
					$dane['ulica_korespondencja'] = implode(' ', $tmp2);
				} else {
					$dane['nr_korespondencja'] = end($tmp2);
					array_pop($tmp2);
					$dane['ulica_korespondencja'] = implode(' ', $tmp2);
				}
				$dane['kwotazaplata'] = str_replace('.', ',', $dane['kwotazaplata']);
				if(!empty($dane['kurier']))
				{
					$this->view->error .= 'Przesyłka dla tego zamówienia była już tworzona. Sprawdź to żeby nie wysłać jeszcze raz tego samego.';
				}
				$this->view->dane = $dane;
			
		
		} else {
			$this->view->error = '<br />Brak elementow';
		}
	}
}

?>