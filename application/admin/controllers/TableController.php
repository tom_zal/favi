<?php

class Admin_TableController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
	}
	
	function tableAction()
	{
		if($this->_request->isPost())
		{
			$gal = new Table;
			$gal->dbname = $this->obConfig->db->config->dbname;
			$main = $gal->getTabele();

			for($i=0; $i < count($main); $i++)
			{
				$nameTable = !in_array($main[$i]['table'], $gal->noTableChange) ? ucfirst($main[$i]['table']) : $main[$i]['table'];
				//pomija nazwy ktore musza byc male
				$data[] = array('table' => $nameTable);
			}
			
			$male = $main;
			$duze = $data;
			$gal->zapiszTabele($male, $duze);

			$this->view->blad_edycji =  '<div class="k_ok">Nazwy tabel na duże litery zostały zmienione.</div>';
		}
	}
}

?>