<?php
class Admin_WizytowkaController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		//echo $this->view->baseUrl;
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}	
	function dodajAction()
	{
		$wizytowka = new Wizytowka();
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa');
			$wizyt = $this->_request->getPost('wizytowka');
			$strona = $this->_request->getPost('tekst');
			$email = $this->_request->getPost('email');
			if($nazwa)
			{
				$dane = array('nazwa' => $nazwa, 'email' => $email, 
					'wizytowka' => $wizyt, 'tekst' => $strona, 'domyslny' => 'N');
				$dane['link'] = $dane['nazwa'];
				$nowa = $wizytowka->dodajWizytowke($dane);
				if($nowa['id'] == 0)
				{
					$this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>';
					$this->view->lista_bledow = '<div id="lista_bledow">Istnieje już taka podstrona</div>';
				}
				else $this->_redirect('/admin/wizytowka/edytuj/id/'.$nowa['link']);
				//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';				
				//$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/wizytowka/edytuj/id/'.$nowa['link']);
			}
			else 
			{
                $this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>';
				$this->view->lista_bledow = '<div id="lista_bledow">1. Pole nazwa jest wymagane.</div>';
            }
		}
		$this->view->edytor = $this->fcKeditor('tekst', '');
		$this->view->short = $this->fcKeditor('wizytowka', '', 200, 'short');
	}	
	function edytujAction()
	{
		$wizytowka = new Wizytowka();
		if($this->_request->isPost())
		{
			$id = $this->_request->getPost('id');
			$link = $this->_request->getPost('link');
			$nazwa = $this->_request->getPost('nazwa');
			$wizyt = $this->_request->getPost('wizytowka');
			$strona = $this->_request->getPost('tekst');
			$email = $this->_request->getPost('email');
			if($nazwa)
			{
				$dane = array('nazwa' => $nazwa, 'email' => $email, 'wizytowka' => $wizyt, 'tekst' => $strona);
				$wizytowka->link = $link;
				$wizytowka->edytujWizytowke($dane);
				//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				//$this->_redirect('/admin/wizytowka/edytuj/id/'.$nowa['link']);
				//$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/wizytowka/edytuj/id/'.$nowa['link']);
			}
			else 
			{
                $this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>';
				$this->view->lista_bledow = '<div id="lista_bledow">1. Pole nazwa jest wymagane.</div>';
            }			
		}
		$id = $this->_request->getParam('id');
		
		$wizytowka->link = $id;
		$this->view->pojedynczy = $wizytowka->getWizytowka();
		$this->view->edytor = 
			$this->fcKeditor('tekst', stripcslashes($this->view->pojedynczy['tekst']));
		$this->view->short = 
			$this->fcKeditor('wizytowka', stripcslashes($this->view->pojedynczy['wizytowka']), 200, 'short');
		$this->view->tytul = $this->view->pojedynczy['nazwa'];
	}
	
	function listaAction() 
	{
        $wizytowka = new Wizytowka();
		$delid = $this->_request->getParam('delid');
        if($delid) 
		{
            $wizytowka->link = $delid;
            $wizytowka->usunWizytowke();
        }
		$domyslny = $this->_request->getParam('domyslny');
        if($domyslny) 
		{
            $wizytowka->link = $domyslny;
            $wizytowka->ustawDomyslny();
        }
        $this->view->wizytowki = $wizytowka->wypiszWizytowki();
    }
}
?>