<?php
class Admin_PocztapolskaController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
	}
    function __call($method, $args) 
	{
        $this->_redirect('/admin');
    }
	
	function indexAction()
	{
		
	}
	
    function ksiazkanadawczaAction()
	{
		$list = new Zend_Session_Namespace('listPoczta');
		//var_dump($_POST);
		$data = $this->_request->getPost('data');
		if($data)
		{
			$data = $data['rok'].'-'.$data['mc'].'-'.$data['day'];
			if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $data)) $list->data = $data;
		}
		else $data = $this->_request->getParam('data');
		$dataDo = $this->_request->getPost('dataDo');
		if($dataDo)
		{
			$dataDo = $dataDo['rok'].'-'.$dataDo['mc'].'-'.$dataDo['day'];
			if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $dataDo)) $list->dataDo = $dataDo;
		}
		else $dataDo = $this->_request->getParam('dataDo');
		
		//if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $data)) $list->data = $data;
		//if(preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $dataDo)) $list->data = $dataDo;
		if(!isset($list->data)) $list->data = date('Y-m-d');
		if(!isset($list->dataDo)) $list->dataDo = date('Y-m-d');
		$data = $list->data;
		$dataDo = $list->dataDo;
		$this->view->data = $data;
		$this->view->dataDo = $dataDo;		
		
		$pocztapolska = new Pocztapolska();
		if(false)
		{
			$adminPoczta = $this->view->adminID;
			if(@in_array($adminPoczta, $this->pocztaPrzekierujAdminow))
			$adminPoczta = $this->pocztaPrzekierujDoAdmina;
		}
		$poczta = $pocztapolska->wypiszOne(1);
		$this->view->nadawca = 'Imię i nazwisko (nazwa) oraz adres nadawcy: ';
		$poczta['nadawcza_nagl1'] = str_replace(' r.', '&nbsp;r.', $poczta['nadawcza_nagl1']);
		$poczta['nadawcza_nagl2'] = str_replace(' r.', '&nbsp;r.', $poczta['nadawcza_nagl2']);
		$poczta['nadawcza_nagl1'] = str_replace('ul. ', 'ul.&nbsp;', $poczta['nadawcza_nagl1']);
		$poczta['nadawcza_nagl2'] = str_replace('ul. ', 'ul.&nbsp;', $poczta['nadawcza_nagl2']);
		$poczta['nadawcza_nagl1'] = str_replace('z dnia', 'z&nbsp;dnia', $poczta['nadawcza_nagl1']);
		$poczta['nadawcza_nagl2'] = str_replace('z dnia', 'z&nbsp;dnia', $poczta['nadawcza_nagl2']);
		$this->view->nadawca.= '<br/>'.$poczta['nadawcza_nagl1'].'<br/>'.$poczta['nadawcza_nagl2'].' ';		
		$this->view->naklad = $poczta['nadawcza_stopka'];
		
		$listy = new Listy();
		$allegro = new Allegroaukcje();
		$zakupy = new Zamowienia();
		//$users = new Allegrousers();
		
		if($this->_request->getParam('delid'))
		{
			$delid = intval($this->_request->getParam('delid'));
			//$buyer = intval($this->_request->getParam('buyer'));
			//$zakupy->edytujDeal($this->view->adminID, $buyer, $delid, array('wyslano' => 0));
			$listy->edytujZam(array('wyslano' => 0), $delid);
			$this->_redirect('admin/pocztapolska/ksiazkanadawcza');
		}
		
		$raport = $listy->wypiszRaport($this->view->adminID, $data, $dataDo);
		$this->view->dane = null;
		if(count($raport) > 0)
		{
			$raporty = null;
			foreach($raport as $rap)
			{
				$deal = $zakupy->selectWybranyKontrahent($rap['deal_id']);
				//print_r($deal);
                               
				if(count($deal) > 0)
				{
					$dane[] = $deal;
				}
				//$raporty[$buyer]['id'] = $id;
			}
			$this->view->dane = $dane;
		}
		//var_dump($raporty);
	}
	
	public function edytujAction()
	{
		$pocztapolska = new Pocztapolska();
		if($this->_request->getPost('poczta'))
		{
			$poczta = $this->_request->getPost('poczta');
			
			if(empty($poczta['zwrot_niezwlocznie'])) $poczta['zwrot_niezwlocznie']=0;
			if(empty($poczta['zwrot_pozniej'])) $poczta['zwrot_pozniej']=0;
			if(empty($poczta['priorytetowa'])) $poczta['priorytetowa']=0;
			if(empty($poczta['poste'])) $poczta['poste']=0;
			if(empty($poczta['wartosc'])) $poczta['wartosc']=0;
			if(empty($poczta['czyrachunek'])) $poczta['czyrachunek']=0;
			if(empty($poczta['czyadres'])) $poczta['czyadres']=0;
			if(empty($poczta['ostroznie'])) $poczta['ostroznie']=0;
			if(empty($poczta['sprawdzenie'])) $poczta['sprawdzenie']=0;
			if(empty($poczta['potwierdzenie'])) $poczta['potwierdzenie']=0;
			
			foreach($poczta as $d => $dana)
			{
				$poczta[$d] = htmlspecialchars($dana);
			}
  
			if(!isset($this->view->error))
			{
				$pocztapolska->edytujID($this->view->adminID, $poczta);
				$this->_redirect($this->URI);
			}
		}
		
		if(!isset($poczta)) $poczta = $pocztapolska->wypiszOne(1);
		
		$login = new Login();
		$login->id = $this->view->adminID;
		$logon = $login->wypiszPojedynczego();
		
		$arrLocales = array('pl_PL', 'pl', 'Polish_Poland.28592');
		setlocale(LC_ALL, $arrLocales);
		$date = iconv('ISO-8859-2', 'UTF-8',strftime('%d %B %Y'));
		
		if(empty($poczta['nadawcza_nagl1']))
		{
			$settings = new Ustawienia();
			$ustawienia = $settings->wypisz();
			$dane_firmy = @unserialize($ustawienia['dane_firmy']);
                
			$nagl1 = '';
			if(!empty($dane_firmy['nazwa_firmy'])) $nagl1.= $dane_firmy['nazwa_firmy'].' ';
			$nagl1.= $dane_firmy['imie'].' '.$dane_firmy['nazwisko'].', ';
			$nagl1.= 'ul. '.$dane_firmy['ulica'].' '.$dane_firmy['nr'].($dane_firmy['mieszkanie']?' '.$dane_firmy['mieszkanie']:'').', ';
			$nagl1.= $dane_firmy['kod'].' '.$dane_firmy['miasto'];
			$poczta['nadawcza_nagl1'] = $nagl1;
		}
		if(empty($poczta['nadawcza_nagl2']))
		{
			$nagl2 = 'umowa nr 000/X/0/CP RH 00 - 0/'.date('Y').' z dnia '.date('d.m.Y').' r.';
			$poczta['nadawcza_nagl2'] = $nagl2;
		}
		if(empty($poczta['nadawcza_stopka']))
		{
			$stopka = 'Nakład własny - zgoda CP RH Rzeszów z dnia '.$date.' roku';
			$poczta['nadawcza_stopka'] = $stopka;
		}
		if(empty($poczta['pieczatka']))
		{
			$pieczatka = 'OPŁATA POBRANA'.PHP_EOL;
			$pieczatka.= 'TAXE PERÇUE - POLOGNE'.PHP_EOL;
			$pieczatka.= 'umowa nr 000/X/0/CP RH00-0/'.date('Y').PHP_EOL;
			$pieczatka.= 'z Pocztą Polską S.A. z dnia '.date('d-m-Y').' r.'.PHP_EOL;
			$pieczatka.= 'Nadano w UP Rzeszów 0';
			$poczta['pieczatka'] = $pieczatka;
		}
		
		$this->view->poczta = $poczta;
	}
    public function dodajAction()
	{
		$pocztapolska = new Pocztapolska();
		$zamowienia = new Zamowienia();
		
		if($this->_request->getParam('zamowienie'))
		{
			$zamowienie = $this->_request->getParam('zamowienie');
			$dane = $zamowienia->selectWybranyKontrahent($zamowienie);
		}
            
		if($this->_request->getPost('poczta'))
		{
			$poczta = $this->_request->getPost('poczta');
			
			if(empty($poczta['zwrot_niezwlocznie'])){
				$poczta['zwrot_niezwlocznie']=0;
			}
			if(empty($poczta['zwrot_pozniej'])){
				$poczta['zwrot_pozniej']=0;
			}
			if(empty($poczta['priorytetowa'])){
				$poczta['priorytetowa']=0;
			}
			if(empty($poczta['poste'])){
				$poczta['poste']=0;
			}
			if(empty($poczta['wartosc'])){
				$poczta['wartosc']=0;
			}
			if(empty($poczta['czyrachunek'])){
				$poczta['czyrachunek']=0;
			}
			if(empty($poczta['czyadres'])){
				$poczta['czyadres']=0;
			}
			if(empty($poczta['ostroznie'])){
				$poczta['ostroznie']=0;
			}
			if(empty($poczta['sprawdzenie'])){
				$poczta['sprawdzenie']=0;
			}
			if(empty($poczta['potwierdzenie'])){
				$poczta['potwierdzenie']=0;
			}
			foreach($poczta as $d => $dana)
			{
				$poczta[$d] = htmlspecialchars($dana);
			}
		}
		
		if(!isset($poczta)) $poczta = $pocztapolska->wypiszOne(1);
		
		if($this->_request->getPost('dane'))
		{
			$dane[0] = $this->_request->getPost('dane');
		}
		
		$this->view->poczta = $poczta;
        $this->view->dane = $dane[0];
    }
	
	public function dodajallegroAction()
	{
        $pocztapolska = new Pocztapolska();
        $zamowienia = new AllegroZamowienia();
        if ($this->_request->getParam('zamowienie'))
		{
            $zamowienie = $this->_request->getParam('zamowienie');
            $dane1 = $zamowienia->selectWybraneZamowienie($zamowienie);
        }
        
        if($this->_request->getPost('dane'))
		{
            $adres = $this->_request->getPost('dane');
      
			$dane['deal_id'] = $adres['deal_id'];
            $dane['nazwisko'] = $adres['nazwisko1'];
            $dane['typ'] = $adres['typ'];
            $dane['mail'] = $adres['email'];
            $dane['nazwa'] = $adres['nazwa_firmy1'];
            $dane['imie'] = $adres['imie1'];
            $dane['telefon'] = $adres['telefon1'];
            $dane['ulica'] = $adres['ulica1'];
            $dane['nr'] = $adres['nr1'];
            $dane['mieszkanie'] = $adres['mieszkanie1'];
            $dane['kod'] = $adres['kod1'];
            $dane['miasto'] = $adres['miasto1'];
            $dane['komorka'] = $adres['komorka1'];
            
            if($adres['typ']=='paczka')
			{
                $paczka = $this->_request->getPost('paczka');
                //die();
                $dane['kategoria'] = $paczka['typ'];
                if(isset($paczka['dwartosc']))
                $dane['wartosc'] = $paczka['dwartosc'];
                $dane['deklaracjawar']=$paczka['deklaracjawar'];
                if(isset($paczka['dpotwierdzenie']))
                $dane['potwierdzenie'] = $paczka['dpotwierdzenie'];
                if(isset($paczka['zwrot']))
                $dane['zwrot'] = $paczka['zwrot'];
            }
            elseif($adres['typ']=='pobraniowa')
			{
                $paczka = $this->_request->getPost('pobraniowa');
                //print_r($paczka);
                //die();
                $dane['kwota'] = $paczka['kwota'];
                $dane['pobranietyp']=$paczka['sposob'];
                $dane['rachunek'] = $paczka['rachunek'];
                $dane['tytul'] = $paczka['tytul'];
                $dane['kategoria'] = $paczka['typ'];
                if(isset($paczka['dwartosc']))
                $dane['wartosc'] = $paczka['dwartosc'];
                $dane['deklaracjawar']=$paczka['deklaracjawar'];
                if(isset($paczka['dpotwierdzenie']))
                $dane['potwierdzenie'] = $paczka['dpotwierdzenie'];
                if(isset($paczka['sprawdz']))
                $dane['sprawdz'] = $paczka['sprawdz'];
                if(isset($paczka['ostroznie']))
                $dane['ostroznie']=$paczka['ostroznie'];                
            }
            elseif($adres['typ']=='polecona')
			{
                $paczka=$this->_request->getPost('polecona');
                //die();
                $dane['kategoria'] = $paczka['typ'];
                 if(isset($paczka['dpotwierdzenie']))
                $dane['potwierdzenie'] = $paczka['dpotwierdzenie'];
            }
            $listy = new Listy();
			if(!empty($dane['deal_id']))
			{
				$dodaj = $listy->dodaj($dane);
			}
			else
			{
				$dodaj = $listy->dodaj($dane, $zamowienie);
			}
        
            $this->_redirect('/admin/pocztapolska/drukuj/id/'.$dodaj);
           
        }

        $poczta = $pocztapolska->wypiszOne();
        $this->view->poczta = $poczta;
        $this->view->dane = $dane1;
    }
		
    public function drukujAction()
	{
		$pocztapolska = new Pocztapolska();		
		$ustawienia = $pocztapolska->wypiszOne(1);
		
		$zamowienia = new Zamowienia();
		$listy = new Listy();
		
		if($this->_request->getPost('dane'))
		{
			$poczta = $this->_request->getPost('dane');
			
			$dane['admin_id']=1;
			$dane['deal_id']=$poczta['id'];
			$dane['wyslano']=1;
			$listy->dodaj($dane);
			
			if(empty($poczta['zwrot_niezwlocznie'])){
				$poczta['zwrot_niezwlocznie']=0;
			}
			if(empty($poczta['zwrot_pozniej'])){
				$poczta['zwrot_pozniej']=0;
			}
			if(empty($poczta['priorytetowa'])){
				$poczta['priorytetowa']=0;
			}
			if(empty($poczta['poste'])){
				$poczta['poste']=0;
			}
			if(empty($poczta['wartosc'])){
				$poczta['wartosc']=0;
			}
			if(empty($poczta['czyrachunek'])){
				$poczta['czyrachunek']=0;
			}
			if(empty($poczta['czyadres'])){
				$poczta['czyadres']=0;
			}
			if(empty($poczta['ostroznie'])){
				$poczta['ostroznie']=0;
			}
			if(empty($poczta['sprawdzenie'])){
				$poczta['sprawdzenie']=0;
			}
			if(empty($poczta['potwierdzenie'])){
				$poczta['potwierdzenie']=0;
			}
			foreach($poczta as $d => $dana)
			{
				$poczta[$d] = htmlspecialchars($dana);
			}
		}
		$this->view->poczta = $poczta;
		$this->view->ustawienia = $ustawienia;                
    }
	
	public function drukuj2Action()
	{
        $pocztapolska = new Pocztapolska();
        $ustawienia = $pocztapolska->wypiszOne($this->view->adminID);
        //print_r($ustawienia);
        $id = $this->_request->getParam('id');
        //$zamowienia = new Zamowienia();
        $listy = new Listy();
        $listy->id = $id;
        $poczta = $listy->wypiszJeden();
        //print_r($poczta);
        $slownie = new AmountInWords();
        $slow = $slownie->get($poczta['deklaracjawar']);
        $slow2 = $slownie->get($poczta['kwota']);
        $this->view->slow = $slow;
        $this->view->slow2 = $slow2;
        $this->view->poczta = $poczta;
        $this->view->ustaw = $ustawienia;
    }
	
	public function dodajhurtallegroAction()
	{
		$ids = $_SESSION['drukuj_przesylka_pobraniowa'];
		unset($_SESSION['drukuj_przesylka_pobraniowa_listy']);
		$_SESSION['drukuj_przesylka_pobraniowa_listy'] = array();
        $pocztapolska = new Pocztapolska();
        $zamowienia = new AllegroZamowienia();
        if (!empty($ids))
		{
			foreach($ids as $id)
			{
				$dane1[$id] = $zamowienia->selectWybraneZamowienie($id);
			}
        }
        
        if($this->_request->getPost('dane'))
		{
            $adresy = $this->_request->getPost('dane');
			
			foreach($adresy as $key => $adres)
			{
				$dane['deal_id'] = $adres['deal_id'];
				$dane['nazwisko'] = $adres['nazwisko1'];
				$dane['typ'] = $adres['typ'];
				$dane['mail'] = $adres['email'];
				$dane['nazwa'] = $adres['nazwa_firmy1'];
				$dane['imie'] = $adres['imie1'];
				$dane['telefon'] = $adres['telefon1'];
				$dane['ulica'] = $adres['ulica1'];
				$dane['nr'] = $adres['nr1'];
				$dane['mieszkanie'] = $adres['mieszkanie1'];
				$dane['kod'] = $adres['kod1'];
				$dane['miasto'] = $adres['miasto1'];
				$dane['komorka'] = $adres['komorka1'];
				
				if($adres['typ']=='pobraniowa')
				{
					$paczka = $this->_request->getPost('pobraniowa');
					$dane['kwota'] = $paczka[$key]['kwota'];
					$dane['pobranietyp']=$paczka[$key]['sposob'];
					$dane['rachunek'] = $paczka[$key]['rachunek'];
					$dane['tytul'] = $paczka[$key]['tytul'];
					$dane['kategoria'] = $paczka[$key]['typ'];
					if(isset($paczka[$key]['dwartosc']))
					$dane['wartosc'] = $paczka[$key]['dwartosc'];
					$dane['deklaracjawar']=$paczka[$key]['deklaracjawar'];
					if(isset($paczka[$key]['dpotwierdzenie']))
					$dane['potwierdzenie'] = $paczka[$key]['dpotwierdzenie'];
					if(isset($paczka[$key]['sprawdz']))
					$dane['sprawdz'] = $paczka[$key]['sprawdz'];
					if(isset($paczka[$key]['ostroznie']))
					$dane['ostroznie']=$paczka[$key]['ostroznie'];
				}
				$listy = new Listy();
				
				if(!empty($dane['deal_id']))
				{
					$dodaj = $listy->dodaj($dane);
				}
				else
				{
					$dodaj = $listy->dodaj($dane, $key);
				}
				
				array_push($_SESSION['drukuj_przesylka_pobraniowa_listy'], $dodaj);
				unset($dane);
				
			}
			unset($_SESSION['drukuj_przesylka_pobraniowa']);
			$this->_redirect('/admin/pocztapolska/drukujhurt');
        }

        $poczta = $pocztapolska->wypiszOne();
        $this->view->poczta = $poczta;
        $this->view->dane1 = $dane1;
    }
	
	public function drukujhurtAction()
	{
		$ids = $_SESSION['drukuj_przesylka_pobraniowa_listy'];
		
        $pocztapolska = new Pocztapolska();
        $ustawienia = $pocztapolska->wypiszOne($this->view->adminID);       
    
        $listy = new Listy();
		$slownie = new AmountInWords();
		//print_r($ids);die();
		foreach($ids as $id)
		{
			$listy->id = $id;
			$poczta[$id] = $listy->wypiszJeden();
			
			$slow[$id] = $slownie->get($poczta[$id]['deklaracjawar']);
			$slow2[$id] = $slownie->get($poczta[$id]['kwota']);
		}        
        
        $this->view->slow = $slow;
        $this->view->slow2 = $slow2;
        $this->view->poczta2 = $poczta;
        $this->view->ustaw = $ustawienia;
		
		$this->view->pocztatyp = "pobraniowa";
    }
		
    public function ustawieniawebapiAction()
	{
        $info = new Pocztapolskakonto($module = 'admin');
        $en = new ElektronicznyNadawca();

        //print_r($czas);
        $zmiana = $this->_request->getPost('konto');
        if (isset($zmiana) && !empty($zmiana))
		{
            if (isset($zmiana['login']))
			{
				$dane['test'] = $zmiana['test'];
                $dane['login'] = $zmiana['login'];
                $dane['konto'] = str_replace(' ', '', $zmiana['konto']);

                if (isset($zmiana['check']))
				{
                    $dane['password'] = $zmiana['haslo'];
                }

                $edit = $info->edytuj($dane, $zmiana['id']);
            }
            if (isset($zmiana['checkreset']))
			{
                try
				{
                    $haslo = new changePassword();
                    $haslo->newPassword = $zmiana['reset'];
                    $reset = $en->changePassword($haslo);
                }
				catch (SoapFault $error)
				{
                    $this->view->error = '<div class="k_blad">Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
                }
                //print_r($reset);
                $this->view->error = '<br />';
                if (isset($reset['error'][0]))
				{
                    for ($i = 0; $i < count($reset['error']); $i++)
					{
                        $this->view->error .= 'Błąd numer: ' . $reset['error'][$i]['errorNumber'] . ': ' . $reset['error'][$i]['errorDesc'] . '<br />';
                    }
                }
				elseif (count($reset['error']) > 0)
				{
                    $this->view->error .= 'Błąd numer: ' . $reset['error']['errorNumber'] . ': ' . $reset['error']['errorDesc'] . '<br />';
                }

                if (count($reset['error']) < 1)
				{
                    $dane['password'] = $zmiana['reset'];
                    $edit = $info->edytuj($dane, $zmiana['id']);
                }
            }
			if(@empty($this->view->error))
			$this->_redirect($this->baseUrl.'/admin/pocztapolska/ustawieniawebapi/');
        }
		
		$wybranyurzad = $this->_getParam('wybranyurzad', 0);
        if($wybranyurzad)
		{
            $edit = $info->edytuj(array('urzad'=>$wybranyurzad), '1');
            $this->_redirect($this->baseUrl.'/admin/pocztapolska/ustawieniawebapi/');
        }
		
        try
		{
            $data = new getPasswordExpiredDate();
			//var_dump($data);die();
            $czas = $en->getPasswordExpiredDate($data);
			//var_dump($czas);die();
            $urz = new getUrzedyNadania();
            $urzedy = $en->getUrzedyNadania($urz);
            $this->view->urzedy = $urzedy['urzedyNadania'];
            //var_dump($urzedy);die();
            $this->view->blad_edycji = '<div class="k_uwaga">Data ważności hasła: ' . $czas['dataWygasniecia'] . '.<br/>Zresetuj hasło przed zakończeniem, aby przedłużyć ważność.</div>';
        }
		catch (SoapFault $error)
		{
            $this->view->error = '<div class="k_blad">Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
        }

        $display = $info->pojedyncza(1);
        $this->view->display = $display;
        //print_r($display);
    }

    public function dodajwebapiAction()
	{
        $pocztapolska = new Pocztapolska();
        $zamowienia = new Zamowienia();
        $ppk = new Pocztapolskakonto();
        $en = new ElektronicznyNadawca();
        //if ($this->_request->getParam('zamowienie'))
		$this->view->zamowID = $zamowienie = intval($this->_request->getParam('zamowienie', 0));
		$dane = $zamowienie ? $zamowienia->selectWybraneZamowienie($zamowienie) : null;
		//var_dump($dane);die();//wartosc_zamowienia kwotazaplata koszt_dostawy

		$this->view->typy = $typy = $pocztapolska->typy;
		
		//$terminy = array('NA_DZIS_KRAJOWY','NA_DZIS_MIEJSKI','NA_DZIS_MIEJSKI_SUPER','NA_DZIS_AGLOMERACJA',
												//'NA_JUTRO_STANDARD','NA_JUTRO_PORANEK','NA_JUTRO_POLUDNIE');
		$terminy = array('MIEJSKI_DO_3H_DO_5KM','MIEJSKI_DO_3H_DO_10KM','MIEJSKI_DO_3H_DO_15KM','MIEJSKI_DO_3H_POWYZEJ_15KM',
						'MIEJSKI_DO_4H_DO_10KM','MIEJSKI_DO_4H_DO_15KM','MIEJSKI_DO_4H_DO_20KM','MIEJSKI_DO_4H_DO_30KM','MIEJSKI_DO_4H_DO_40KM',
						'KRAJOWY','BEZPOSREDNI_DO_30KG','BEZPOSREDNI_OD_30KG_DO_100KG','EKSPRES24');
		$this->view->terminy = $terminy;

        $poczta = $pocztapolska->wypiszOne(1);
		
		if($zamowienie >= 0)
		{
			if($zamowienie == 0)
			$this->view->dane1 = array('typ' => $poczta['typ']);
			foreach($typy as $typ => $nazwa)
			$this->view->{$typ} = @unserialize($poczta[$typ]);
		}

		//var_dump($_POST);die();
        $dane1 = $this->_request->getPost('dane');
		
		if(false)
		if(@$poczta['typ'] == 'biznesowa' || $dane1['typ'] == 'biznesowa')
		{
			if(!isset($_SESSION['urzadWydaniaEPrzesylki']))
			{
				try
				{
					$en = new ElektronicznyNadawca();
					$ur = new getUrzedyWydajaceEPrzesylki();
					$urzedy = $en->getUrzedyWydajaceEPrzesylki($ur);
					$pocztapolskadane = new Pocztapolskadane();
					$urzadWydaniaEPrzesylki = $urzedy['urzadWydaniaEPrzesylki'];
					//$urzadWydaniaEPrzesylki = $pocztapolskadane->urzadWydaniaEPrzesylkiSort($urzadWydaniaEPrzesylki);
					$_SESSION['urzadWydaniaEPrzesylki'] = $urzadWydaniaEPrzesylki;
					//var_dump(count($_SESSION['urzadWydaniaEPrzesylki']));die();
				}
				catch(SoapFault $error)
				{
					//4270
					/*
						'id' => int 346935
						'wojewodztwo' => string 'mazowieckie' (length=11)
						'powiat' => string 'Warszawa' (length=8)
						'miejsce' => string 'śródmieście' (length=14)
						'kodPocztowy' => string '00001' (length=5)
						'miejscowosc' => string 'Warszawa' (length=8)
						'ulica' => string 'Mikołaja Kopernika' (length=19)
						'numerDomu' => string '36/40' (length=5)
					*/
					$this->view->error = '<div class="k_blad">Nie można pobrać punktów odbioru przesyłki<br/>'.$error->getMessage().'</div>';
				}
			}
			$biznesowa = $this->_request->getPost('biznesowa');
			if(@mb_strlen($biznesowa['urzadWydaniaEPrzesylkiInput'], "UTF-8") >= 3)
			{
				$urzadWydaniaEPrzesylki = null;
				$input = $biznesowa['urzadWydaniaEPrzesylkiInput'];
				if(@count($_SESSION['urzadWydaniaEPrzesylki']) > 0)
				foreach($_SESSION['urzadWydaniaEPrzesylki'] as $urzad)
				{
					if(mb_stripos($urzad['miejscowosc'], $input, 0, "UTF-8") !== false
					//|| mb_stripos($urzad['wojewodztwo'], $input, 0, "UTF-8") !== false
					//|| mb_stripos($urzad['powiat'], $input, 0, "UTF-8") !== false
					//|| mb_stripos($urzad['miejsce'], $input, 0, "UTF-8") !== false
					|| mb_stripos($urzad['kodPocztowy'], $input, 0, "UTF-8") !== false)				
					//|| @mb_stripos($urzad['ulica'], $input, 0, "UTF-8") !== false
					//|| @mb_stripos($urzad['numerDomu'], $input, 0, "UTF-8") !== false
					$urzadWydaniaEPrzesylki[$urzad['id']] = $urzad;
				}
				$pocztapolskadane = new Pocztapolskadane();
				$urzadWydaniaEPrzesylki = $pocztapolskadane->urzadWydaniaEPrzesylkiSort($urzadWydaniaEPrzesylki);
				$this->view->urzadWydaniaEPrzesylki = $urzadWydaniaEPrzesylki;
			}
		}
		
		if(isset($dane1) && !empty($dane1) && $zamowienie == 0)
		{
			$typ = @$dane1['typ'];
			if(!empty($typ))
			{
				$pocztapolska->updateData(array('typ' => $typ));
				$$typ = $this->_request->getPost($typ);
				if(is_array($$typ) && count($$typ) > 0)
				$pocztapolska->updateData(array($typ => @serialize($$typ)));
			}
			$this->_redirect('admin/pocztapolska/dodajwebapi');
		}
		
        if(isset($dane1) && !empty($dane1) && $zamowienie > 0)
		{
            $adres = new adresType();
            if(strlen($dane1['nazwa_firmy']) > 1)
			{
                $nazwa = $dane1['nazwa_firmy'];
                $nazwa2 = $dane1['imie'] . ' ' . $dane1['nazwisko'];
            }
			else
			{
                $nazwa = $dane1['imie'] . ' ' . $dane1['nazwisko'];
                $nazwa2 = '';
            }
            $adres->nazwa = $nazwa; // nazwaType
            $adres->nazwa2 = $nazwa2; // nazwa2Type
            $adres->ulica = $dane1['ulica']; // ulicaType
            $adres->numerDomu = $dane1['nr']; // numerDomuType
            $adres->numerLokalu = $dane1['mieszkanie']; // numerLokaluType
            $adres->miejscowosc = $dane1['miasto']; // miejscowoscType
            $adres->kodPocztowy = str_replace('-', '', $dane1['kod']); // kodPocztowyType
            $adres->kraj = 'Polska'; // krajType
            $adres->telefon = str_replace(' ', '', $dane1['telefon']); // telefonType
            $adres->email = $dane1['email']; // emailType

            if(strlen($dane1['komorka']) > 9)
			{
                $ile = strlen(str_replace(' ', '', $dane1['komorka'])) - 9;
                $num = substr(str_replace(' ', '', $dane1['komorka']), $ile, 9);
            }
			else
			{
                $num = $dane1['komorka'];
            }
            $adres->mobile = $num; // mobileType
            //print_r($adres);
			
            if($dane1['typ'] == 'paczka')
			{
                $this->view->paczka = $paczka = $this->_request->getPost('paczka');
                $typ = new paczkaPocztowaType();
                $typ->posteRestante = 0;
                
				if(isset($paczka['dpotwierdzenie']))
				{
                    $typ->iloscPotwierdzenOdbioru = $paczka['potwierdzenie']; // iloscPotwierdzenOdbioruType
                }
				else
				{
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $paczka['typ']; // kategoriaType
                $typ->gabaryt = $paczka['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $paczka['masa']) * 1000); // masaType
                if(isset($paczka['dwartosc']))
				{
                    $liczba = (0 + str_replace(',', '.', $paczka['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                }
				else
				{
                    $typ->wartosc = ''; // wartoscType
                }
				
                if(isset($paczka['zwrot'])) $typ->zwrotDoslanie = 1;
                if(isset($paczka['biblioteka'])) $typ->egzemplarzBiblioteczny = 1;
                if(isset($paczka['ociemniali'])) $typ->dlaOciemnialych = 1;
            }
            else if($dane1['typ'] == 'pobraniowa')
			{
                $this->view->pobraniowa = $pobranie = $this->_request->getPost('pobraniowa');
                
				$typ = new przesylkaPobraniowaType();
				$typ->posteRestante = 0;
				
                $typ1 = new pobranieType();
                $typ1->sposobPobrania = @$pobranie['sposob']; // sposobPobraniaType
                $typ1->kwotaPobrania = (0 + str_replace(',', '.', $pobranie['kwota']) * 100); // kwotaPobraniaType
                $typ1->nrb = $pobranie['rachunek']; // anonymous51
                $typ1->tytulem = $pobranie['tytul']; // anonymous52
                if(isset($pobranie['sprawdz'])) $typ1->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = 1;
                $typ->pobranie = $typ1; // pobranieType
                
                if(isset($pobranie['dpotwierdzenie']))
				{
                    $typ->iloscPotwierdzenOdbioru = $pobranie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                }
				else
				{
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $pobranie['typ']; // kategoriaType
                $typ->gabaryt = $pobranie['gabaryt']; // gabarytType
                if(isset($pobranie['ostroznie'])) $typ->ostroznie;
                if(isset($pobranie['dwartosc']))
				{
                    $liczba = (0 + str_replace(',', '.', $pobranie['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                }
				else
				{
                    $typ->wartosc = ''; // wartoscType
                }

                $typ->masa = (0 + str_replace(',', '.', $pobranie['masa']) * 1000); // masaType
            }
			else if($dane1['typ'] == 'polecona')
			{
                $this->view->polecona = $polecenie = $this->_request->getPost('polecona');
                
				$typ = new przesylkaPoleconaKrajowaType();
                $typ->epo = new EPOSimpleType(); // EPOType
                $typ->posteRestante = 0;
                
				if(isset($polecenie['dpotwierdzenie']))
				{
                    $typ->iloscPotwierdzenOdbioru = $polecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                }
				else
				{
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $polecenie['typ']; // kategoriaType
                $typ->gabaryt = $polecenie['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $polecenie['masa']) * 1000); // masaType
				
                if(isset($polecenie['biblioteka'])) $typ->egzemplarzBiblioteczny = 1;
                if(isset($polecenie['ociemniali'])) $typ->dlaOciemnialych = 1;
            }
			else if($dane1['typ'] == 'firmowa_polecona')
			{
                $this->view->firmowa_polecona = $firmowa_polecona = $this->_request->getPost('firmowa_polecona');
                
				$typ = new przesylkaFirmowaPoleconaType();
                $typ->epo = new EPOSimpleType(); // EPOType
                $typ->posteRestante = 0;
                
				if(isset($firmowa_polecona['dpotwierdzenie']))
				{
                    $typ->iloscPotwierdzenOdbioru = $firmowa_polecona['potwierdzenie']; // iloscPotwierdzenOdbioruType
                }
				else
				{
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $firmowa_polecona['typ']; // kategoriaType
                $typ->miejscowa = $firmowa_polecona['miejscowa']; // Miejscowa
                $typ->masa = (0 + str_replace(',', '.', $firmowa_polecona['masa']) * 1000); // masaType
				//var_dump($typ);die();
                if(isset($firmowa_polecona['biblioteka'])) $typ->egzemplarzBiblioteczny = 1;
                if(isset($firmowa_polecona['ociemniali'])) $typ->dlaOciemnialych = 1;
            }
            else if($dane1['typ'] == 'deklaracja')
			{
                $this->view->deklaracja = $deklaracje = $this->_request->getPost('deklaracja');
                
				$typ = new przesylkaListowaZadeklarowanaWartoscType();
                $typ->posteRestante = 0;
                
				$liczba = (0 + str_replace(',', '.', $deklaracje['wartosc']));
                $typ->wartosc = ($liczba * 100); // wartoscType

                if(isset($deklaracje['dpotwierdzenie']))
				{
                    $typ->iloscPotwierdzenOdbioru = $deklaracje['potwierdzenie']; // iloscPotwierdzenOdbioruType
                }
				else
				{
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $deklaracje['typ']; // kategoriaType
                $typ->gabaryt = $deklaracje['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $deklaracje['masa']) * 1000); // masaType
                
				if(isset($deklaracje['zwrot'])) $typ->zwrotDoslanie = 1;
                if(isset($deklaracje['biblioteka'])) $typ->egzemplarzBiblioteczny = 1;
                if(isset($deklaracje['ociemniali'])) $typ->dlaOciemnialych = 1;
			}
            else if($dane1['typ'] == 'pocztex')
			{
                $this->view->pocztex = $pocztex = $this->_request->getPost('pocztex');
                $typ = new pocztexKrajowyType();
                $typ->posteRestante = 0;
				
				if(isset($pocztex['dwartosc']))
				{
                    $liczba = (0 + str_replace(',', '.', $pocztex['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                }
				else $typ->wartosc = ''; // wartoscType
				
				//$typ->typ = $pocztex['typ'];
				$typ->terminRodzaj = $pocztex['terminRodzaj'];
				$typ->uiszczaOplate = $pocztex['uiszczaOplate'];
				$typ->odleglosc = @intval($pocztex['odleglosc']);
				$typ->zawartosc = $pocztex['zawartosc'];
				
				if(isset($pocztex['ubezpieczenie']))
				{
					$typ1 = new ubezpieczenieType();					
					$typ1->rodzaj = @strval($pocztex['ubezpieczenie_rodzaj']);
					$typ1->kwota = @floatval(str_replace(",",".",$pocztex['ubezpieczenie_kwota']));
					$typ->ubezpieczenie = $typ1;
                }

                if(isset($pocztex['dpotwierdzenie']))
				{
					$typ1 = new potwierdzenieOdbioruType();
					$typ1->ilosc = @intval($pocztex['potwierdzenie']);
					$typ1->sposob = @strval($pocztex['potwierdzenie_sposob']);
                    //$typ->iloscPotwierdzenOdbioru = @intval($pocztex['potwierdzenie']); // iloscPotwierdzenOdbioruType
					//$typ->sposobPrzekazaniaPotwierdzeniaOdbioru = $pocztex['sposobPrzekazaniaPotwierdzeniaOdbioru'];
					//$typ->terminPrzekazaniaPotwierdzeniaOdbioru = $pocztex['terminPrzekazaniaPotwierdzeniaOdbioru'];
					$typ->potwierdzenieOdbioru = $typ1;
                }
				//$typ->terminZwrotuPodpisanychDokumentow = $pocztex['terminZwrotuPodpisanychDokumentow'];
				
				if(isset($pocztex['potwierdzenieDoreczenia']))
				{
					$typ1 = new potwierdzenieDoreczeniaType();					
					$typ1->sposob = @strval($pocztex['doreczenie_sposob']);
					$typ1->kontakt = @strval($pocztex['doreczenie_kontakt']);
					$typ->potwierdzenieDoreczenia = $typ1;
                }
				
				if(@!empty($pocztex['sposob']))
				{
					$typ1 = new pobranieType();
					$typ1->sposobPobrania = @$pocztex['sposob']; // sposobPobraniaType
					$typ1->kwotaPobrania = (0 + str_replace(',', '.', $pocztex['kwota']) * 100); // kwotaPobraniaType
					$typ1->nrb = $pocztex['rachunek']; // anonymous51
					$typ1->tytulem = $pocztex['tytul']; // anonymous52
					if(isset($pocztex['sprawdz'])) $typ1->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = 1;
					$typ->pobranie = $typ1; // pobranieType
				}
				
				if(@$pocztex['odbiorPrzesylkiOdNadawcy'])
				{
					$typ1 = new odbiorPrzesylkiOdNadawcyType();
					$typ1->wSobote = @intval($pocztex['odbior_wSobote']);
					$typ1->wNiedzieleLubSwieto = @intval($pocztex['odbior_wNiedzieleLubSwieto']);
					$typ1->wGodzinachOd20Do7 = @intval($pocztex['odbior_wGodzinachOd20Do7']);
					$typ->odbiorPrzesylkiOdNadawcy = $typ1;
				}
				if(@$pocztex['doreczenie'])
				{
					$typ1 = new doreczenieType();
					$typ1->wSobote = @intval($pocztex['doreczenie_wSobote']);
					$typ1->wNiedzieleLubSwieto = @intval($pocztex['doreczenie_wNiedzieleLubSwieto']);
					$typ1->wGodzinachOd20Do7 = @intval($pocztex['doreczenie_wGodzinachOd20Do7']);
					$typ1->w90Minut = @intval($pocztex['doreczenie_w90Minut']);
					$typ1->doRakWlasnych = @intval($pocztex['doreczenie_doRakWlasnych']);
					$typ1->oczekiwanyTerminDoreczenia = @strval($pocztex['oczekiwanyTerminDoreczenia']);
					$typ1->oczekiwanaGodzinaDoreczenia = @strval($pocztex['oczekiwanaGodzinaDoreczenia']);
					$typ->doreczenie = $typ1;
				}
				if(@$pocztex['zwrotDokumentow'])
				{
					$typ1 = new zwrotDokumentowType();
					if(@!empty($pocztex['rodzajPocztex']))
					{
						$typ2 = new terminRodzajType();
						$typ2->terminRodzaj = @strval($pocztex['rodzajPocztex']);
						$typ->rodzajPocztex = $typ2;
					}
					else
					{
						$typ2 = new rodzajListType();
						$typ2->polecony = @intval($pocztex['rodzajList_polecony']);
						$typ3 = new kategoriaType();
						$typ3->kategoria = @strval($pocztex['rodzajList_kategoria']);
						$typ2->kategoria = $typ3;
						//$typ2->kategoria = @strval($pocztex['rodzajList_kategoria']);
						$typ1->rodzajList = $typ2;
					}
					$typ->zwrotDokumentow = $typ1;
				}

                $typ->masa = (0 + str_replace(',', '.', $pocztex['masa']) * 1000); // masaType
                //if(isset($pocztex['doręczenieDoRakWlasnych'])) $typ->doręczenieDoRakWlasnych = 1;
                //if(isset($pocztex['czy_doręczeniePrzesylkiWeWskazanymDniu']))
				//$typ->doręczeniePrzesylkiWeWskazanymDniu = $pocztex['doręczeniePrzesylkiWeWskazanymDniu'];
                //if(isset($pocztex['doręczenieWNajblizszaSobote'])) $typ->doręczenieWNajblizszaSobote = 1;
				if(isset($pocztex['chroniona'])) $typ->ostroznie = 1;
				if(isset($pocztex['kopertaFirmowa'])) $typ->kopertaFirmowa = 1;
				if(isset($pocztex['ponadgabaryt'])) $typ->ponadgabaryt = 1;
            }
			else if($dane1['typ'] == 'biznesowa')
			{
                $this->view->biznesowa = $biznesowa = $this->_request->getPost('biznesowa');
                $typ = new przesylkaBiznesowaType();
                $typ->posteRestante = 0;
				
				if(isset($biznesowa['dwartosc']))
				{
                    $liczba = (0 + str_replace(',', '.', $biznesowa['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                }
				else $typ->wartosc = ''; // wartoscType
				
				//$typ->typ = $biznesowa['typ'];
				if(@!empty($biznesowa['gabaryt']))
				$typ->gabaryt = $biznesowa['gabaryt']; //gabarytBiznesowaType::
				
				if(isset($biznesowa['ubezpieczenie']))
				{
					$typ1 = new ubezpieczenieType();					
					$typ1->rodzaj = @strval($biznesowa['ubezpieczenie_rodzaj']);
					$typ1->kwota = @floatval(str_replace(",",".",$biznesowa['ubezpieczenie_kwota']));
					$typ->ubezpieczenie = $typ1;
                }
				
				if(@!empty($biznesowa['sposob']))
				{
					$typ1 = new pobranieType();
					$typ1->sposobPobrania = @$biznesowa['sposob']; // sposobPobraniaType
					$typ1->kwotaPobrania = (0 + str_replace(',', '.', $biznesowa['kwota']) * 100); // kwotaPobraniaType
					$typ1->nrb = $biznesowa['rachunek']; // anonymous51
					$typ1->tytulem = $biznesowa['tytul']; // anonymous52
					if(isset($biznesowa['sprawdz'])) $typ1->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = 1;
					$typ->pobranie = $typ1; // pobranieType
				}

                $typ->masa = (0 + str_replace(',', '.', $biznesowa['masa']) * 1000); // masaType
				if(isset($biznesowa['ostroznie'])) $typ->ostroznie = 1;
				
				$urzadWydaniaEPrzesylki = @intval($biznesowa['urzadWydaniaEPrzesylki']);
				if($urzadWydaniaEPrzesylki > 0)
				{
					$typ->urzadWydaniaEPrzesylki = new urzadWydaniaEPrzesylkiType();
					$typ->urzadWydaniaEPrzesylki->id = $urzadWydaniaEPrzesylki; 
					unset($typ->subPrzesylka);
				}
            }
			else if($dane1['typ'] == 'nierejestrowana')
			{
                $this->view->nierejestrowana = $nierejestrowana = $this->_request->getPost('nierejestrowana');
				$typ = new listZwyklyType();//przesylkaNieRejestrowanaType
                //$typ = new przesylkaNieRejestrowanaType();
                $typ->posteRestante = 0; // boolean
                $typ->ilosc = 1;
                $typ->kategoria = $nierejestrowana['typ']; // kategoriaType
				$typ->gabaryt = $nierejestrowana['gabaryt']; // gabarytType ;
                $typ->egzemplarzBiblioteczny = 0; // egzemplarzBiblioteczny
				$typ->dlaOciemnialych = 0; // dlaOciemnialych
                $typ->masa = (0 + str_replace(',', '.', $nierejestrowana['masa']) * 1000); // masaType
                if(!empty($nierejestrowana['opis']))
				{
					$typ->opis = $nierejestrowana['opis'];					
				}
            }
			else if($dane1['typ'] == 'firmowa_nierejestrowana')
			{
                $this->view->firmowa_nierejestrowana = $firmowa_nierejestrowana = $this->_request->getPost('firmowa_nierejestrowana');
                $typ = new listZwyklyType();
                $typ->posteRestante = 0; // boolean
				$typ->ilosc = 1;
				$typ->miejscowa = 0;
                $typ->kategoria = $firmowa_nierejestrowana['typ']; // kategoriaType
                $typ->masa = (0 + str_replace(',', '.', $firmowa_nierejestrowana['masa']) * 1000); // masaType
                if(!empty($firmowa_nierejestrowana['opis']))
				{
					$typ->opis = $firmowa_nierejestrowana['opis'];
				}
				//if (isset($paczka['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 0; // boolean
                //if (isset($paczka['ociemniali']))
                    $typ->dlaOciemnialych = 0; // boolean
            }
			else if($dane1['typ'] == 'usluga_kurierska')
			{
				$this->view->usluga_kurierska = $uslugakurierska = $this->_request->getPost('usluga_kurierska');
				$typ = new uslugaKurierskaType();
				$typ->doreczenie = new doreczenieUslugaKurierskaType();
				$typ->termin = $uslugakurierska['termin'];
				if(!empty($uslugakurierska['masa']))
				{
					$typ->masa = new masaType();
					$typ->masa = (0 + str_replace(',', '.', $uslugakurierska['masa']) * 1000); // masaType
				}
				if(!empty($uslugakurierska['uiszczaOplate']))
				{
					switch($uslugakurierska['uiszczaOplate'])
					{
						case 'A':
							$typ->uiszczaOplate = uiszczaOplateType::ADRESAT;
						break;
						case 'N':
							$typ->uiszczaOplate = uiszczaOplateType::NADAWCA;
						break;
					}
				}
				if(!empty($uslugakurierska['godzdoreczenia']))
				{
					$typ->doreczenie->oczekiwanaGodzinaDoreczenia = $uslugakurierska['godzinaDoreczenia'];
				}
				$typ->zawartosc = $uslugakurierska['zawartosc'];
				if(!empty($uslugakurierska['pobranie']))
				{
					$typ->pobranie = new pobranieType();
					if(!empty($uslugakurierska['sposob']))
					{
						$typ->pobranie->sposobPobrania = $uslugakurierska['sposob'];
						$liczba = (0 + str_replace(',', '.', $uslugakurierska['kwota']));
						$typ->pobranie->kwotaPobrania = ($liczba * 100); // wartoscType
						
						$typ->pobranie->nrb = $uslugakurierska['rachunek'];
						$typ->pobranie->tytulem = $uslugakurierska['tytul'];
					}
				}
				if(!empty($uslugakurierska['wartosc']))
				{
					$wartosc = (0 + str_replace(',', '.', $uslugakurierska['wartosc']));
					$typ->wartosc = ($wartosc * 100); // wartoscType
				}
				if(!empty($uslugakurierska['potwierdzenieOdbioru']))
				{
					$typ->potwierdzenieOdbioru = new potwierdzenieOdbioruKurierskaType();
					$typ->potwierdzenieOdbioru->sposob = $uslugakurierska['potwierdzenieOdbioru'];
					$typ->potwierdzenieOdbioru->ilosc = $uslugakurierska['potwierdzenieOdbioruIlosc'];
				}
				if(!empty($uslugakurierska['potwierdzenieDoreczenia']))
				{
					$typ->potwierdzenieDoreczenia = new potwierdzenieDoreczeniaType();
					$typ->potwierdzenieDoreczenia->sposob = $uslugakurierska['potwierdzenieDoreczenia'];
					$typ->potwierdzenieDoreczenia->kontakt = $uslugakurierska['potwierdzenieDoreczeniaKontakt'];
				}
				if(!empty($uslugakurierska['ostroznie']))
				{
					$typ->ostroznie = '1';
				}
				if(!empty($uslugakurierska['ponadgabaryt']))
				{
					$typ->ponadgabaryt = '1';
				}
				if(!empty($uslugakurierska['sprZawrPrzezOdbiorce']))
				{
					$typ->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = '1';
				}
				if(!empty($uslugakurierska['doreczenieWeWskaznymDniu']))
				{
					$typ->doreczenie->oczekiwanyTerminDoreczenia = $uslugakurierska['doreczenieWeWskaznymDniuData'];
				}
				if(!empty($uslugakurierska['doRakWlasnych']))
				{
					$typ->doreczenie->doRakWlasnych = '1';
				}
				if(!empty($uslugakurierska['wSobote']))
				{
					$typ->doreczenie->wSobote = '1';
					$typ->doreczenie->oczekiwanaGodzinaDoreczenia = null;
				}
				if(!empty($uslugakurierska['odbiorPrzesylkiOdNadawcyType']))
				{
					$typ->odbiorPrzesylkiOdNadawcy = new odbiorPrzesylkiOdNadawcyType();
					$typ->odbiorPrzesylkiOdNadawcy->wSobote = '1';
				}
				if(!empty($uslugakurierska['ubezpieczenie']))
				{
					$typ->ubezpieczenie = new ubezpieczenieType();
					$typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
					$typ->ubezpieczenie->kwota = $uslugakurierska['ubezpieczenieKwota'];
				}
				if(!empty($uslugakurierska['dokZwrotDiv']))
				{
					$typ->zwrotDokumentow = new zwrotDokumentowKurierskaType();
					$typ->zwrotDokumentow->rodzajList = new rodzajListType();
					//$typ->zwrotDokumentow->rodzajPaczka = new terminZwrotDokumentowPaczkowaType();
					//$typ->zwrotDokumentow->rodzajPocztex = new terminZwrotDokumentowPaczkowaType();
					switch($uslugakurierska['zwrotDokumentow'])
					{
						case 'LZP':
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
						break;
						case 'LZE':
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
						break;
						case 'LPP':
							$typ->zwrotDokumentow->rodzajList->polecony = '1';
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
						break;
						case 'LPE':
							$typ->zwrotDokumentow->rodzajList->polecony = '1';
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
						break;
						case 'P24':
							$typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_24;
						break;
						case 'P48':
							$typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_48;
						break;
						case 'KE24':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::EKSPRES24;
						break;
						case 'KMD3GD5KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_5KM;
						break;
						case 'KMD3GD10KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_10KM;
						break;
						case 'KMD3GD15KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_15KM;
						break;
						case 'KMD3GP15KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_POWYZEJ_15KM;
						break;
						case 'KBD20KG':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::BEZPOSREDNI_DO_20KG;
						break;						
					}
				}
				if(!empty($uslugakurierska['numerPrzesylkiKlienta']))
				{
					$typ->numerPrzesylkiKlienta = $uslugakurierska['numerPrzesylkiKlienta'];
				}
				if(!empty($uslugakurierska['opis']))
				{
					$typ->opis = $uslugakurierska['opis'];
				}
			}

            //var_dump($typ);die();
            try
			{
                $guid = new getGuid();
                $guid->ilosc = 1;
                $gui = $en->getGuid($guid);
                $typ->guid = $gui['guid'];
				$typ->opis = $dane1['opis'];
                //$typ->guid = $gui;
                $typ->adres = $adres;
                $add = new addShipment();
                $add->przesylki[] = $typ;

                $wyslij = $en->addShipment($add);
                //var_dump($wyslij['retval']);//die();
                if(empty($wyslij['retval']['error']))
				{
                    $this->view->blad_edycji = '<div class="k_ok">Poprawnie wysłano przesyłkę.</div>';
                    $this->view->error = '';
                    $this->view->error .= 'Numer nadania: ' . $wyslij['retval']['numerNadania'];
                    $this->view->error .= '<br />Numer GUID: ' . $wyslij['retval']['guid'];

                    //$zam['przesylka'] = $wyslij['retval']['numerNadania'];
                    //$zamow = new Zamowienia($module = 'admin');
                    //$edit = $zamow->edytuj($zamowienie, $zam);
					$pocztapolskadane = new Pocztapolskadane();
					$przesylka['id_zam'] = @intval($zamowienie);
					$przesylka['nr_nad'] = $wyslij['retval']['numerNadania'];
					$przesylka['guid'] = $wyslij['retval']['guid'];
					$pocztapolskadane->zapisz($przesylka);
                    //die();
                    $this->getResponse()->setHeader('Refresh', '1; URL=' . $this->baseUrl . '/admin/zamowienia/wypisz/tryb/aktualne/mode/all');
                }
				else
				{
					$this->view->dane1 = @$dane1;
                    $this->view->blad_edycji = '<div class="k_blad">Błąd danych. Przesyłka nie została zatwierdzona.</div>';
                    $this->view->error = '';
                    if(isset($wyslij['retval']['error']['0']))
					{
                        for ($i = 0; $i < count($wyslij['retval']['error']); $i++)
						{
                            $this->view->error .= '<br />Błąd numer: <b>' . $wyslij['retval']['error'][$i]['errorNumber'] . '</b>: ' . $wyslij['retval']['error'][$i]['errorDesc'];
                        }
                    }
					else
					{
                        $this->view->error .= '<br /> Błąd numer: <b>' . $wyslij['retval']['error']['errorNumber'] . '</b>: ' . $wyslij['retval']['error']['errorDesc'];
                    }
                }
            }
			catch(SoapFault $error)
			{
                $this->view->error = '<div class="k_blad">Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
		
        $this->view->ppk = $ppk->pojedyncza('1');
        $this->view->poczta = $poczta;
        $this->view->dane = @$dane;
    }

    public function buforAction()
	{
        $en = new ElektronicznyNadawca();

        if($this->_request->getParam('iddel'))
		{
            try
			{
                $guid = $this->_request->getParam('iddel');
                $type = new clearEnvelopeByGuids();
                $type->guid = $guid;
                $usun = $en->clearEnvelopeByGuids($type);
				$pocztapolskadane = new Pocztapolskadane();
				$pocztapolskadane->usunGUID($guid);

                $this->view->error = '<div class="k_ok">Poprawnie usunięto pozycję z bufora.</div>';
            }
			catch (SoapFault $error)
			{
                $this->view->error = '<div class="k_blad">Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
        if($this->_request->getParam('idwydruk'))
		{
            try
			{
                $guid = $this->_request->getParam('idwydruk');
                $type = new getAddresLabelByGuid();
                $type->guid = $guid;
                $usun = $en->getAddresLabelByGuid($type);
                //print_r($usun);
                // $hash = $usun['content']['pdfContent'];
                $this->parsePdfPage(base64_decode($usun['content']['pdfContent']), 'druk_' . $guid);
                //$OUTPUT = "output_decoded.pdf";
                //unset($usun);
                //$bin = base64_decode($usun['content']['pdfContent']);
                //file_put_contents($OUTPUT, $bin);
                //print base64_encode(trim($usun['content']['pdfContent']));
                $this->view->error = '<div class="k_ok">Pobrano plik.</div>';
            }
			catch (SoapFault $error)
			{
                $this->view->error = '<div class="k_blad">Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
        if($this->_request->getPost('wyslijwszystko'))
		{
            try
			{
                $wyslij = new sendEnvelope();
                $wyslij->urzadNadania = $this->_request->getPost('urzad');
				$parametry = new getEnvelopeBufor();
				$bufor = $en->getEnvelopeBufor($parametry);
				$bufor = @$bufor['przesylka'];
				$wyslano = $en->sendEnvelope($wyslij);
				
				$pocztapolskadane = new Pocztapolskadane();
				if(count($bufor) > 0)
				{
					if(!isset($bufor[0])) $bufor = array(0 => $bufor);
					foreach($bufor as $przes)
					{
						$zam = $pocztapolskadane->wypiszNumer(@$przes['numerNadania']);
						$przesylka['id_zam'] = @intval($zam['id_zam']);
						$przesylka['bufor'] = @intval($wyslano['idEnvelope']);
						$przesylka['data_wysylka'] = date('Y-m-d H:i:s');
						$pocztapolskadane->zapisz($przesylka);
					}
				}
                //var_dump($wyslano);var_dump($bufor);var_dump($zam);var_dump($przesylka);die();

                if (empty($wyslano['error']))
				{
                    $this->view->blad_edycji = '<div class="k_ok">Poprawnie wysłano przesyłki.</div>';
                }
				else
				{
                    $this->view->blad_edycji = '<div class="k_blad">Błąd danych. Przesyłka nie została zatwierdzona.</div>';
                    $this->view->error = '';
                    if (isset($wyslano['error']['0']))
					{
                        for ($i = 0; $i < count($wyslano['error']); $i++)
						{
                            $this->view->error .= '<br />Błąd numer: ' . $wyslano['error'][$i]['errorNumber'] . ': ' . $wyslano['error'][$i]['errorDesc'];
                        }
                    }
					else
					{
                        $this->view->error .= '<br />Błąd numer: ' . $wyslano['error']['errorNumber'] . ': ' . $wyslano['error']['errorDesc'];
                    }
                }
            }
			catch(SoapFault $error)
			{
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
        if($this->_request->getPost('kasujwszystko'))
		{
            try
			{
                $czysc = new clearEnvelope();
				$parametry = new getEnvelopeBufor();
				$bufor = $en->getEnvelopeBufor($parametry);
				$bufor = @$bufor['przesylka'];
                $wyczysc = $en->clearEnvelope($czysc);
				$pocztapolskadane = new Pocztapolskadane();
				if(count($bufor) > 0)
				{
					if(!isset($bufor[0])) $bufor = array(0 => $bufor);
					foreach($bufor as $przes)
					{
						$pocztapolskadane->usunGUID($przes['guid']);
					}
				}
            }
			catch (SoapFault $error)
			{
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }

        try
		{
            $ur = new getUrzedyNadania();
            $urzedy = $en->getUrzedyNadania($ur);
            $parametry = new getEnvelopeBufor();
			$bufor = $en->getEnvelopeBufor($parametry);
            if(@$_GET['buf']){var_dump($bufor['przesylka']);die();}
        }
		catch (SoapFault $error)
		{
            $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
        }
        
        $this->view->urzedy = @$urzedy['urzedyNadania'];
        $this->view->bufor = @$bufor['przesylka'];
    }
    
    public function wyslaneAction()
	{
        $en = new ElektronicznyNadawca();
        $parametry = new getEnvelopeList();
        $parametry->endDate = date('Y-m-d');
        $parametry->startDate = date('Y-m-d', strtotime(date('Y-m-d').' -30 day'));
        try
        {
            $bufor = $en->getEnvelopeList($parametry);
			if(@count($bufor['envelopes']) > 0)
			if(!isset($bufor['envelopes'][0]))
			$bufor['envelopes'] = array(0 => $bufor['envelopes']);
			//var_dump($bufor);die();			
			if(false && @count($bufor['envelopes']) > 0)
			foreach($bufor['envelopes'] as $p => $przes)
			{
				$type = new getEnvelopeContentFull();//getEnvelopeContentShort
				$type->idEnvelope = $przes['idEnvelope'];
				$przesylka = $en->getEnvelopeContentFull($type);
				$bufor['envelopes'][$p]['przesylka'] = $przesylka['przesylka'];
				//var_dump($bufor['envelopes'][0]);die();
			}
        }
        catch(SoapFault $error)
        {
            $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            return;
        }
		//var_dump($bufor['envelopes']);die();
        //print_r($bufor);
        $this->view->bufor = $bufor['envelopes'];
		
        if($this->_request->getParam('idnalepki'))
		{
            try
			{
                $guid = $this->_request->getParam('idnalepki');
                $type = new getAddresLabelCompact();
                $type->idEnvelope = $guid;
                $usun = $en->getAddresLabelCompact($type);
                //print_r($usun);
                //print_r($usun['content']['pdfContent']);
                // $hash = $usun['content']['pdfContent'];
                if(empty($usun['content'][0]))
				{
                    $this->parsePdfPage(base64_decode($usun['content']['pdfContent']), 'nalepki_adresowe_' . $guid);
                }
				else
				{
                    for($i=0;$i<count($usun['content']);$i++)
					{
                        //$this->parsePdfPage(base64_decode($usun['content'][$i]['pdfContent']), 'nalepki_adresowe_' . $guid);
						//print_r($usun['content'][$i]['pdfContent']);
                        $value = base64_decode($usun['content'][$i]['pdfContent'], true);
                        $file = fopen('../public/etykieta_adresowa_' . $i . '.pdf', 'w');
                        fwrite($file, $value);
                    }
                }
                //$OUTPUT = "output_decoded.pdf";
                //unset($usun);
                //$bin = base64_decode($usun['content']['pdfContent']);
                //file_put_contents($OUTPUT, $bin);
                //print base64_encode(trim($usun['content']['pdfContent']));
                $this->view->error = '<div class="k_ok">Pobrano plik.</div>';
            }
			catch (SoapFault $error)
			{
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
        if($this->_request->getParam('idksiazka'))
		{
            try
			{
                $guid = $this->_request->getParam('idksiazka');
                $type = new getOutboxBook();
                $type->idEnvelope = $guid;
                $usun = $en->getOutboxBook($type);
                //print_r($usun);
                // $hash = $usun['content']['pdfContent'];
                $this->parsePdfPage(base64_decode($usun['content']['pdfContent']), 'ksiazka_nadawcza_' . $guid);
                //$OUTPUT = "output_decoded.pdf";
                //unset($usun);
                //$bin = base64_decode($usun['content']['pdfContent']);
                //file_put_contents($OUTPUT, $bin);
                //print base64_encode(trim($usun['content']['pdfContent']));
                $this->view->error = '<div class="k_ok">Pobrano plik.</div>';
            }
			catch (SoapFault $error)
			{
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
        if($this->_request->getParam('idpoczta'))
		{
            try
			{
                $guid = $this->_request->getParam('idpoczta');
                $type = new getFirmowaPocztaBook();
                $type->idEnvelope = $guid;
                $usun = $en->getFirmowaPocztaBook($type);
                //print_r($usun);
                // $hash = $usun['content']['pdfContent'];
                $this->parsePdfPage(base64_decode($usun['content']['pdfContent']), 'poczta_firmowa_' . $guid);
                //$OUTPUT = "output_decoded.pdf";
                //unset($usun);
                //$bin = base64_decode($usun['content']['pdfContent']);
                //file_put_contents($OUTPUT, $bin);
                //print base64_encode(trim($usun['content']['pdfContent']));
                $this->view->error = '<div class="k_ok">Pobrano plik.</div>';
            }
			catch(SoapFault $error)
			{
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
		if($this->_request->getParam('idstatus'))
		{
            try
			{
                $guid = $this->_request->getParam('idstatus');
				$type = new getEnvelopeStatus();
                $type->idEnvelope = $guid;
                $status = $en->getEnvelopeStatus($type);
                print_r($status);
            }
			catch(SoapFault $error)
			{
                $this->view->error = '<div class="k_blad" >Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
    }

    protected function parsePdfPage($xml, $filename = 'file')
	{
        Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        header("Content-Disposition: inline; filename=$filename.pdf");
        header("Content-type: application/x-pdf");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Description: File Transfer");
        echo $xml;
    }
    
     public function dodajwebapiallegrooldAction()
	 {
        $pocztapolska = new Pocztapolska();
        $zamowienia = new AllegroZamowienia();
        $ppk = new Pocztapolskakonto();
        $en = new ElektronicznyNadawca();
        if ($this->_request->getParam('zamowienie')) {
            $zamowienie = $this->_request->getParam('zamowienie');
            $dane = $zamowienia->selectWybraneZamowienie($zamowienie);
        }
        if (!isset($poczta))
            $poczta = $pocztapolska->wypiszOne(1);

        //if ($this->_request->getPost('dane')) {
        //    $dane[0] = $this->_request->getPost('dane');
        //}

        $dane1 = $this->_request->getPost('dane');
        if (isset($dane1) && !empty($dane1)) {

            $adres = new adresType();
            if (strlen($dane1['nazwa_firmy1']) > 1) {
                $nazwa = $dane1['nazwa_firmy1'];
                $nazwa2 = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
            } else {
                $nazwa = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
                $nazwa2 = '';
            }
            $adres->nazwa = $nazwa; // nazwaType
            $adres->nazwa2 = $nazwa2; // nazwa2Type
            $adres->ulica = $dane1['ulica1']; // ulicaType
            $adres->numerDomu = $dane1['nr1']; // numerDomuType
            $adres->numerLokalu = $dane1['mieszkanie1']; // numerLokaluType
            $adres->miejscowosc = $dane1['miasto1']; // miejscowoscType
            $adres->kodPocztowy = str_replace('-', '', $dane1['kod1']); // kodPocztowyType
            $adres->kraj = 'Polska'; // krajType
            $adres->telefon = str_replace(' ', '', $dane1['telefon1']); // telefonType
            $adres->email = $dane1['email']; // emailType

            if (strlen($dane1['komorka1']) > 9) {
                $ile = strlen(str_replace(' ', '', $dane1['komorka1'])) - 9;
                $num = substr(str_replace(' ', '', $dane1['komorka1']), $ile, 9);
            } else {
                $num = $dane1['komorka1'];
            }
            $adres->mobile = $num; // mobileType
            //print_r($adres);
            if ($dane1['typ'] == 'paczka') {
                $paczka = $this->_request->getPost('paczka');
                $typ = new paczkaPocztowaType();
                $typ->posteRestante = 0;
                if (isset($paczka['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $paczka['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $paczka['typ']; // kategoriaType
                $typ->gabaryt = $paczka['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $paczka['masa']) * 1000); // masaType
                if (isset($paczka['dwartosc'])) {
                    $liczba = (0 + str_replace(',', '.', $paczka['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                } else {
                    $typ->wartosc = ''; // wartoscType
                }
                if (isset($paczka['zwrot']))
                    $typ->zwrotDoslanie = 1;
                if (isset($paczka['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1;
                if (isset($paczka['ociemniali']))
                    $typ->dlaOciemnialych = 1;
            }
            elseif ($dane1['typ'] == 'pobraniowa') {
                $pobranie = $this->_request->getPost('pobraniowa');
                $typ = new przesylkaPobraniowaType();
                $typ1 = new pobranieType();
                $typ1->sposobPobrania = $pobranie['sposob']; // sposobPobraniaType
                $typ1->kwotaPobrania = (0 + str_replace(',', '.', $pobranie['kwota']) * 100); // kwotaPobraniaType
                $typ1->nrb = $pobranie['rachunek']; // anonymous51
                $typ1->tytulem = $pobranie['tytul']; // anonymous52
                if (isset($pobranie['sprawdz']))
                    $typ1->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = 1;


                $typ->pobranie = $typ1; // pobranieType
                $typ->posteRestante = 0;
                if (isset($pobranie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $pobranie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }


                $typ->kategoria = $pobranie['typ']; // kategoriaType
                $typ->gabaryt = $pobranie['gabaryt']; // gabarytType
                if (isset($pobranie['ostroznie']))
                    $typ->ostroznie;
                if (isset($pobranie['dwartosc'])) {
                    $liczba = (0 + str_replace(',', '.', $pobranie['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                } else {
                    $typ->wartosc = ''; // wartoscType
                }

                $typ->masa = (0 + str_replace(',', '.', $pobranie['masa']) * 1000); // masaType
            } elseif ($dane1['typ'] == 'polecona') {
                $polecenie = $this->_request->getPost('polecona');
                $typ = new przesylkaPoleconaKrajowaType();
                $typ->epo = new EPOSimpleType(); // EPOType
                $typ->posteRestante = 0;
                if (isset($polecenie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $polecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $polecenie['typ']; // kategoriaType
                $typ->gabaryt = $polecenie['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $polecenie['masa']) * 1000); // masaType
                if (isset($polecenie['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1;
                if (isset($polecenie['ociemniali']))
                    $typ->dlaOciemnialych = 1;
            }
            elseif ($dane1['typ'] == 'deklaracja') {
                $deklaracje = $this->_request->getPost('deklaracja');
                $typ = new przesylkaListowaZadeklarowanaWartoscType();
                $typ->posteRestante = 0;
                $liczba = (0 + str_replace(',', '.', $deklaracje['wartosc']));
                $typ->wartosc = ($liczba * 100); // wartoscType

                if (isset($deklaracje['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $deklaracje['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $deklaracje['typ']; // kategoriaType
                $typ->gabaryt = $deklaracje['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $deklaracje['masa']) * 1000); // masaType
                if (isset($deklaracje['zwrot']))
                    $typ->zwrotDoslanie = 1;
                if (isset($deklaracje['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1;
                if (isset($deklaracje['ociemniali']))
                    $typ->dlaOciemnialych = 1;
            }


            // print_r($typ);
            try {
                $guid = new getGuid();
                $guid->ilosc = 1;
                $gui = $en->getGuid($guid);
                $typ->guid = $gui['guid'];
                //$typ->guid = $gui;
                $typ->adres = $adres;
                $add = new addShipment();
                $add->przesylki[] = $typ;

                $wyslij = $en->addShipment($add);
                //print_r($wyslij);
                if (empty($wyslij['retval']['error'])) {
                    $this->view->blad_edycji = '<div class="k_ok">Poprawnie wysłano przesyłkę.</div>';
                    $this->view->error = '';
                    $this->view->error .= 'Numer nadania: ' . $wyslij['retval']['numerNadania'];
                    $this->view->error .= '<br />Numer GUID: ' . $wyslij['retval']['guid'];

                    $zam['przesylka'] = $wyslij['retval']['numerNadania'];
                    $zamow = new AllegroZamowienia($module = 'admin');
                    $edit = $zamow->edytuj($zamowienie, $zam);
                    
                    $this->getResponse()->setHeader('Refresh', '1; URL=' . $this->baseUrl . '/admin/zamowienia/wypisz/tryb/aktualne/mode/all');
                } else {
                    $this->view->blad_edycji = 'Błąd danych. Przesyłka nie została zatwierdzona.';
                    $this->view->error = '';
                    if (isset($wyslij['retval']['error']['0'])) {
                        for ($i = 0; $i < count($wyslij['retval']['error']); $i++) {
                            $this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error'][$i]['errorNumber'] . ': ' . $wyslij['retval']['error'][$i]['errorDesc'];
                        }
                    } else {
                        $this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error']['errorNumber'] . ': ' . $wyslij['retval']['error']['errorDesc'];
                    }
                }
            } catch (SoapFault $error) {
                $this->view->error = '<div class="k_blad">Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych<br/>'.$error->getMessage().'</div>';
            }
        }
        $this->view->ppk = $ppk->pojedyncza('1');
        $this->view->poczta = $poczta;
        $this->view->dane = $dane;
    }
	
	public function dodajwebapiallegroAction()
	{
        $pocztapolska = new Pocztapolska();
        $zamowienia = new AllegroZamowienia();
        $ppk = new Pocztapolskakonto();
        $en = new ElektronicznyNadawca();
		if($this->_request->getParam('ppwaga', 0))
		{
			$ppwaga = $this->_request->getParam('ppwaga'); //waga podana na liscie zamowien
		}

        if ($this->_request->getParam('zamowienie')) {
            $zamowienie = $this->_request->getParam('zamowienie');
            $dane = $zamowienia->selectWybraneZamowienie($zamowienie);
			if(!empty($ppwaga))
			{
				$dane['masa'] = $ppwaga / 1000;
			}
        }
        if (!isset($poczta))
            $poczta = $pocztapolska->wypiszOne($this->view->adminID);

        //if ($this->_request->getPost('dane')) {
        //    $dane[0] = $this->_request->getPost('dane');
        //}

        $dane1 = $this->_request->getPost('dane');
        if (isset($dane1) && !empty($dane1)) {

            $adres = new adresType();
            if (strlen($dane1['nazwa_firmy1']) > 1) {
                $nazwa = $dane1['nazwa_firmy1'];
                $nazwa2 = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
            } else {
                $nazwa = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
                $nazwa2 = '';
            }
            $adres->nazwa = $nazwa; // nazwaType
            $adres->nazwa2 = $nazwa2; // nazwa2Type
            $adres->ulica = $dane1['ulica1']; // ulicaType
            $adres->numerDomu = $dane1['nr1']; // numerDomuType
            $adres->numerLokalu = $dane1['mieszkanie1']; // numerLokaluType
            $adres->miejscowosc = $dane1['miasto1']; // miejscowoscType
            $adres->kodPocztowy = str_replace('-', '', $dane1['kod1']); // kodPocztowyType
            $adres->kraj = 'Polska'; // krajType
            $adres->telefon = str_replace(' ', '', $dane1['telefon1']); // telefonType
            $adres->email = $dane1['email']; // emailType

            if (strlen($dane1['komorka1']) > 9) {
                $ile = strlen(str_replace(' ', '', $dane1['komorka1'])) - 9;
                $num = substr(str_replace(' ', '', $dane1['komorka1']), $ile, 9);
            } else {
                $num = $dane1['komorka1'];
            }
            $adres->mobile = $num; // mobileType
            //print_r($adres);
            if ($dane1['typ'] == 'paczka') {
                $paczka = $this->_request->getPost('paczka');
                $typ = new paczkaPocztowaType();
                $typ->posteRestante = 0; // boolean
                if (isset($paczka['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $paczka['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $paczka['typ']; // kategoriaType
                $typ->gabaryt = $paczka['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $paczka['masa']) * 1000); // masaType
                if (isset($paczka['dwartosc'])) {
                    $liczba = (0 + str_replace(',', '.', $paczka['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                } else {
                    $typ->wartosc = ''; // wartoscType
                }
                if (isset($paczka['zwrot']))
                    $typ->zwrotDoslanie = 1; // boolean
                if (isset($paczka['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($paczka['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
            }
            elseif ($dane1['typ'] == 'pobraniowa') {
                $pobranie = $this->_request->getPost('pobraniowa');
                $typ = new przesylkaPobraniowaType();
                $typ1 = new pobranieType();
                $typ1->sposobPobrania = $pobranie['sposob']; // sposobPobraniaType
                $typ1->kwotaPobrania = (0 + str_replace(',', '.', $pobranie['kwota']) * 100); // kwotaPobraniaType
                $typ1->nrb = $pobranie['rachunek']; // anonymous51
                $typ1->tytulem = $pobranie['tytul']; // anonymous52
                if (isset($pobranie['sprawdz']))
                    $typ1->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = 1; // boolean


                $typ->pobranie = $typ1; // pobranieType
                $typ->posteRestante = 0; // boolean
                if (isset($pobranie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $pobranie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }


                $typ->kategoria = $pobranie['typ']; // kategoriaType
                $typ->gabaryt = $pobranie['gabaryt']; // gabarytType
                if (isset($pobranie['ostroznie']))
                    $typ->ostroznie; // boolean
                if (isset($pobranie['dwartosc'])) {
                    $liczba = (0 + str_replace(',', '.', $pobranie['wartosc']));
                    $typ->wartosc = ($liczba * 100); // wartoscType
                } else {
                    $typ->wartosc = ''; // wartoscType
                }

                $typ->masa = (0 + str_replace(',', '.', $pobranie['masa']) * 1000); // masaType
				if(!empty($pobranie['opis']))
				{
					$typ->opis = $pobranie['opis'];
				}
            } 
			elseif ($dane1['typ'] == 'polecona') {
                $polecenie = $this->_request->getPost('polecona');
                $typ = new przesylkaPoleconaKrajowaType();
                $typ->epo = new EPOSimpleType(); // EPOType
                $typ->posteRestante = 0; // boolean
                if (isset($polecenie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $polecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $polecenie['typ']; // kategoriaType
                $typ->gabaryt = $polecenie['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $polecenie['masa']) * 1000); // masaType
                if (isset($polecenie['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($polecenie['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
				if(!empty($polecenie['opis']))
				{
					
					$typ->opis = $polecenie['opis'];
				}
				
            }
			elseif ($dane1['typ'] == 'firmowapolecona') {
                $firmowapolecenie = $this->_request->getPost('firmowapolecona');
                $typ = new przesylkaFirmowaPoleconaType();
                $typ->posteRestante = 0; // boolean
                $typ->miejscowa = 0; // boolean
				$typ->kategoria = $firmowapolecenie['typ']; // kategoriaType
                if (isset($firmowapolecenie['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $firmowapolecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }
				
                $typ->masa = (0 + str_replace(',', '.', $firmowapolecenie['masa']) * 1000); // masaType
                if (isset($firmowapolecenie['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($firmowapolecenie['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
				if(!empty($firmowapolecenie['opis']))
				{
					$typ->opis = $firmowapolecenie['opis'];
				}
            }
            elseif ($dane1['typ'] == 'deklaracja') {
                $deklaracje = $this->_request->getPost('deklaracja');
                $typ = new przesylkaListowaZadeklarowanaWartoscType();
                $typ->posteRestante = 0; // boolean
                $liczba = (0 + str_replace(',', '.', $deklaracje['wartosc']));
                $typ->wartosc = ($liczba * 100); // wartoscType

                if (isset($deklaracje['dpotwierdzenie'])) {
                    $typ->iloscPotwierdzenOdbioru = $deklaracje['potwierdzenie']; // iloscPotwierdzenOdbioruType
                } else {
                    $typ->iloscPotwierdzenOdbioru = 0;
                }

                $typ->kategoria = $deklaracje['typ']; // kategoriaType
                $typ->gabaryt = $deklaracje['gabaryt']; // gabarytType
                $typ->masa = (0 + str_replace(',', '.', $deklaracje['masa']) * 1000); // masaType
                if (isset($deklaracje['zwrot']))
                    $typ->zwrotDoslanie = 1; // boolean
                if (isset($deklaracje['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 1; // boolean
                if (isset($deklaracje['ociemniali']))
                    $typ->dlaOciemnialych = 1; // boolean
				if(!empty($deklaracje['opis']))
				{
					$typ->opis = $deklaracje['opis'];
				}
            }
			elseif ($dane1['typ'] == 'nierejestrowana') {
                $nierejestrowana = $this->_request->getPost('nierejestrowana');
				$typ = new listZwyklyType();//przesylkaNieRejestrowanaType
                //$typ = new przesylkaNieRejestrowanaType();
                $typ->posteRestante = 0; // boolean
                $typ->ilosc = 1;
                $typ->kategoria = $nierejestrowana['typ']; // kategoriaType
				$typ->gabaryt = $nierejestrowana['gabaryt']; // gabarytType ;
                $typ->egzemplarzBiblioteczny = 0; // egzemplarzBiblioteczny
				$typ->dlaOciemnialych = 0; // dlaOciemnialych
                $typ->masa = (0 + str_replace(',', '.', $nierejestrowana['masa']) * 1000); // masaType
                if(!empty($nierejestrowana['opis']))
				{
					$typ->opis = $nierejestrowana['opis'];
					
				}
            }
			elseif ($dane1['typ'] == 'firmowanierejestrowana') {
                $firmowanierejestrowana = $this->_request->getPost('firmowanierejestrowana');
                $typ = new listZwyklyType();
                $typ->posteRestante = 0; // boolean
				$typ->ilosc = 1;
				$typ->miejscowa = 0;
                $typ->kategoria = $firmowanierejestrowana['typ']; // kategoriaType
                $typ->masa = (0 + str_replace(',', '.', $firmowanierejestrowana['masa']) * 1000); // masaType
                if(!empty($firmowanierejestrowana['opis']))
				{
					$typ->opis = $firmowanierejestrowana['opis'];
				}
				//if (isset($paczka['biblioteka']))
                    $typ->egzemplarzBiblioteczny = 0; // boolean
                //if (isset($paczka['ociemniali']))
                    $typ->dlaOciemnialych = 0; // boolean
            }
			elseif ($dane1['typ'] == 'uslugakurierska'){
				$uslugakurierska = $this->_request->getPost('uslugakurierska');
				$typ = new uslugaKurierskaType();
				$typ->doreczenie = new doreczenieUslugaKurierskaType();
				$typ->termin = $uslugakurierska['termin'];
				if(!empty($uslugakurierska['masa']))
				{
					$typ->masa = new masaType();
					$typ->masa = (0 + str_replace(',', '.', $uslugakurierska['masa']) * 1000); // masaType
				}
				if(!empty($uslugakurierska['uiszczaOplate']))
				{
					switch($uslugakurierska['uiszczaOplate'])
					{
						case 'A':
							$typ->uiszczaOplate = uiszczaOplateType::ADRESAT;
						break;
						case 'N':
							$typ->uiszczaOplate = uiszczaOplateType::NADAWCA;
						break;
					}
				}
				if(!empty($uslugakurierska['godzdoreczenia']))
				{
						$typ->doreczenie->oczekiwanaGodzinaDoreczenia = $uslugakurierska['godzinaDoreczenia'];
				}
				$typ->zawartosc = $uslugakurierska['zawartosc'];
				if(!empty($uslugakurierska['pobranie']))
				{
					$typ->pobranie = new pobranieType();
					if(!empty($uslugakurierska['sposob']))
					{
						$typ->pobranie->sposobPobrania = $uslugakurierska['sposob'];
						$liczba = (0 + str_replace(',', '.', $uslugakurierska['kwota']));
						$typ->pobranie->kwotaPobrania = ($liczba * 100); // wartoscType
						
						$typ->pobranie->nrb = $uslugakurierska['rachunek'];
						$typ->pobranie->tytulem = $uslugakurierska['tytul'];
					}
				}
				if(!empty($uslugakurierska['wartosc']))
				{
					$wartosc = (0 + str_replace(',', '.', $uslugakurierska['wartosc']));
					$typ->wartosc = ($wartosc * 100); // wartoscType
				}
				if(!empty($uslugakurierska['potwierdzenieOdbioru']))
				{
					$typ->potwierdzenieOdbioru = new potwierdzenieOdbioruKurierskaType();
					$typ->potwierdzenieOdbioru->sposob = $uslugakurierska['potwierdzenieOdbioru'];
					$typ->potwierdzenieOdbioru->ilosc = $uslugakurierska['potwierdzenieOdbioruIlosc'];
				}
				if(!empty($uslugakurierska['potwierdzenieDoreczenia']))
				{
					$typ->potwierdzenieDoreczenia = new potwierdzenieDoreczeniaType();
					$typ->potwierdzenieDoreczenia->sposob = $uslugakurierska['potwierdzenieDoreczenia'];
					$typ->potwierdzenieDoreczenia->kontakt = $uslugakurierska['potwierdzenieDoreczeniaKontakt'];
				}
				if(!empty($uslugakurierska['ostroznie']))
				{
					$typ->ostroznie = '1';
				}
				if(!empty($uslugakurierska['ponadgabaryt']))
				{
					$typ->ponadgabaryt = '1';
				}
				if(!empty($uslugakurierska['sprZawrPrzezOdbiorce']))
				{
					$typ->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = '1';
				}
				if(!empty($uslugakurierska['doreczenieWeWskaznymDniu']))
				{
					$typ->doreczenie->oczekiwanyTerminDoreczenia = $uslugakurierska['doreczenieWeWskaznymDniuData'];
				}
				if(!empty($uslugakurierska['doRakWlasnych']))
				{
					$typ->doreczenie->doRakWlasnych = '1';
				}
				if(!empty($uslugakurierska['wSobote']))
				{
					$typ->doreczenie->wSobote = '1';
					$typ->doreczenie->oczekiwanaGodzinaDoreczenia = null;
				}
				if(!empty($uslugakurierska['odbiorPrzesylkiOdNadawcyType']))
				{
					$typ->odbiorPrzesylkiOdNadawcy = new odbiorPrzesylkiOdNadawcyType();
					$typ->odbiorPrzesylkiOdNadawcy->wSobote = '1';
				}
				if(!empty($uslugakurierska['ubezpieczenie']))
				{
					$typ->ubezpieczenie = new ubezpieczenieType();
					$typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
					$typ->ubezpieczenie->kwota = $uslugakurierska['ubezpieczenieKwota'];
				}
				if(!empty($uslugakurierska['dokZwrotDiv']))
				{
					$typ->zwrotDokumentow = new zwrotDokumentowKurierskaType();
					$typ->zwrotDokumentow->rodzajList = new rodzajListType();
					//$typ->zwrotDokumentow->rodzajPaczka = new terminZwrotDokumentowPaczkowaType();
					//$typ->zwrotDokumentow->rodzajPocztex = new terminZwrotDokumentowPaczkowaType();
					switch($uslugakurierska['zwrotDokumentow'])
					{
						case 'LZP':
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
						break;
						case 'LZE':
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
						break;
						case 'LPP':
							$typ->zwrotDokumentow->rodzajList->polecony = '1';
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
						break;
						case 'LPE':
							$typ->zwrotDokumentow->rodzajList->polecony = '1';
							$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
						break;
						case 'P24':
							$typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_24;
						break;
						case 'P48':
							$typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_48;
						break;
						case 'KE24':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::EKSPRES24;
						break;
						case 'KMD3GD5KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_5KM;
						break;
						case 'KMD3GD10KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_10KM;
						break;
						case 'KMD3GD15KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_15KM;
						break;
						case 'KMD3GP15KM':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_POWYZEJ_15KM;
						break;
						case 'KBD20KG':
							$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::BEZPOSREDNI_DO_20KG;
						break;
						
					}
				}
				if(!empty($uslugakurierska['numerPrzesylkiKlienta']))
				{
					$typ->numerPrzesylkiKlienta = $uslugakurierska['numerPrzesylkiKlienta'];
				}
				if(!empty($uslugakurierska['opis']))
				{
					$typ->opis = $uslugakurierska['opis'];
				}
			}
			elseif ($dane1['typ'] == 'przesylkakurierska48'){ //przesylka biznesowa
				$przesylkakurierska48 = $this->_request->getPost('przesylkakurierska48');
				$typ = new przesylkaBiznesowaType();
				if(!empty($przesylkakurierska48['pobranie']))
				{
					$typ->pobranie = new pobranieType();
					if(!empty($przesylkakurierska48['sposob']))
					{
						$typ->pobranie->sposobPobrania = $przesylkakurierska48['sposob'];
						$kwota = (0 + str_replace(',', '.', $przesylkakurierska48['kwota']));
						$typ->pobranie->kwotaPobrania = ($kwota * 100); // wartoscType
						
						$typ->pobranie->nrb = $przesylkakurierska48['rachunek'];
						$typ->pobranie->tytulem = $przesylkakurierska48['tytul'];
					}
				}
				if(!empty($przesylkakurierska48['gabaryt']))
				{
					switch($przesylkakurierska48['gabaryt'])
					{
						case 'XXL':
							$typ->gabaryt = gabarytBiznesowaType::XXL;
						break;
					}
				}
				if(!empty($przesylkakurierska48['wartosc']) && !empty($przesylkakurierska48['masa']))
				{
					$wartosc = (0 + str_replace(',', '.', $przesylkakurierska48['wartosc']));
					$typ->wartosc = ($wartosc * 100); // wartoscType
					$typ->masa = (0 + str_replace(',', '.', $przesylkakurierska48['masa']) * 1000); // masaType
				}
				if(!empty($przesylkakurierska48['ostroznie']))
				{
					$typ->ostroznie = '1';
				}
				if(!empty($przesylkakurierska48['ubezpieczenie']))
				{
					$typ->ubezpieczenie = new ubezpieczenieType();
					$typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
					$typ->ubezpieczenie->kwota = $przesylkakurierska48['ubezpieczenieKwota'];
				}
				if(!empty($przesylkakurierska48['opis']))
				{
					$typ->opis = $przesylkakurierska48['opis'];
				}
			}


             //print_r($typ);die();
            try {
				$oPoczta = new Pocztapolskakonto();
				$Poczta = $oPoczta->pojedyncza(1);
				if(!empty($Poczta['data_nowego_zbioru']))
				{
					if($Poczta['data_nowego_zbioru'] !== date('Y-m-d') || empty($Poczta['idBufor']))
					{
						$getUrzedyNadania = new getUrzedyNadania();
						$UrzedyNadania = $en->getUrzedyNadania($getUrzedyNadania);
						//print_r($UrzedyNadania['urzedyNadania']['urzadNadania']); die();
						$buforType = new buforType();
						$buforType->dataNadania = date('Y-m-d');
						$buforType->opis = 'BigCom-'.date('Y-m-d');
						$buforType->active = '1';
						$buforType->urzadNadania = $UrzedyNadania['urzedyNadania']['urzadNadania'];
						$createBufor = new createEnvelopeBufor();
						$createBufor->bufor = $buforType;
						$newBufor = $en->createEnvelopeBufor($createBufor);
						
					
						$zmiany['data_nowego_zbioru'] = $newBufor['createdBufor']['dataNadania'];
						$zmiany['idBufor'] = $newBufor['createdBufor']['idBufor'];
						$Poczta = $oPoczta->edytuj($zmiany , 1);
					}
				}
			
				$Poczta = $oPoczta->pojedyncza(1);
			/*
				$getEnvelopeBuforList = new getEnvelopeBuforList();
				$BuforList = $en->getEnvelopeBuforList($getEnvelopeBuforList);
				print_r($BuforList);die();
			*/	
                $guid = new getGuid();
                $guid->ilosc = 1;
                $gui = $en->getGuid($guid);
                $typ->guid = $gui['guid'];
                //$typ->guid = $gui;
                $typ->adres = $adres;
				//print_r($typ);die();
                $add = new addShipment();
                $add->przesylki[] = $typ;
				$add->idBufor = $Poczta['idBufor'];
			
				
				
				
                $wyslij = $en->addShipment($add);
                //print_r($wyslij);die();
                if (empty($wyslij['retval']['error'])) {
                    $this->view->blad_edycji = '<div class="k_ok">Poprawnie wysłano przesyłkę.</div>';
                    $this->view->error = '';
                    $this->view->error .= 'Numer nadania: ' . $wyslij['retval']['numerNadania'];
                    $this->view->error .= '<br />Numer GUID: ' . $wyslij['retval']['guid'];

                    $zam['przesylka'] = $wyslij['retval']['numerNadania'];
                    $zam['kurier'] = 'Poczta Polska';
                    $zamow = new AllegroZamowienia($module = 'admin');
                    $edit = $zamow->edytuj($zamowienie, $zam);
                    
                    //$this->getResponse()->setHeader('Refresh', '1; URL=' . $this->baseUrl . '/admin/allegrozamowienia/wypisz/tryb/aktualne/mode/all');
                } else {
                    $this->view->blad_edycji = 'Błąd danych. Przesyłka nie została zatwierdzona.';
                    $this->view->error = '';
                    if (isset($wyslij['retval']['error']['0'])) {
                        for ($i = 0; $i < count($wyslij['retval']['error']); $i++) {
                            $this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error'][$i]['errorNumber'] . ': ' . $wyslij['retval']['error'][$i]['errorDesc'];
                        }
                    } else {
                        $this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error']['errorNumber'] . ': ' . $wyslij['retval']['error']['errorDesc'];
                    }
                }
            } catch (SoapFault $error) {
                $this->view->error = 'Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.';
            }
        }
        $this->view->ppk = $ppk->pojedyncza('1');
        $this->view->poczta = $poczta;
		$tmp = explode(' ', str_replace('  ', ' ', $dane['imie_korespondencja'])); //rozbij na imie i nazwisko
		$dane['imie_korespondencja'] = $tmp[0];
		$dane['nazwisko_korespondencja'] = $tmp[1];
		$tmp2 = explode(' ', $dane['ulica_korespondencja']); //rozbij  na ulice i numer
		$tmp3 = explode('/', end($tmp2)); //rozbij na nr domu i lokalu
		if(!empty($tmp3[1]))
		{
			$dane['nr_korespondencja'] = $tmp3[0];
			$dane['mieszkanie_korespondencja'] = $tmp3[1];
			array_pop($tmp2);
			$dane['ulica_korespondencja'] = implode(' ', $tmp2);
		} else {
			$dane['nr_korespondencja'] = end($tmp2);
			array_pop($tmp2);
			$dane['ulica_korespondencja'] = implode(' ', $tmp2);
		}
		$dane['kwotazaplata'] = str_replace('.', ',', $dane['kwotazaplata']);
		if(!empty($dane['kurier']))
		{
			$this->view->error .= 'Przesyłka dla tego zamówienia była już tworzona. Sprawdź to żeby nie wysłać jeszcze raz tego samego.';
		}
        $this->view->dane = $dane;
		//print_r($dane);
    }
	
	/* dodawanie hurtowo listow przewozowych dla Poczty polskiej */
	public function dodajwebapiallegrohurtAction() 
	{
		$idsy = $_SESSION['poczta_polska_hurtem'];
		$ppwaga = $_SESSION['pocztapolska_waga'];
		//print_r($ppwaga);
		if(!empty($idsy))
		{
			
			
				$pocztapolska = new Pocztapolska();
				$zamowienia = new AllegroZamowienia();
				$ppk = new Pocztapolskakonto();
				$en = new ElektronicznyNadawca();
				
				if (!empty($idsy[0])) {
					$zamowienie = $idsy[0];
					//print_r($zamowienie);
					$dane = $zamowienia->selectWybraneZamowienie($zamowienie);
					if(!empty($ppwaga[$zamowienie]))
					{
						$dane['masa'] = $ppwaga[$zamowienie] / 1000;
					}
				}
				
				if (!isset($poczta))
					$poczta = $pocztapolska->wypiszOne($this->view->adminID);

				

				$dane1 = $this->_request->getPost('dane');
				if (isset($dane1) && !empty($dane1)) 
				{

					$adres = new adresType();
					if (strlen($dane1['nazwa_firmy1']) > 1) {
						$nazwa = $dane1['nazwa_firmy1'];
						$nazwa2 = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
					} else {
						$nazwa = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
						$nazwa2 = '';
					}
					$adres->nazwa = $nazwa; // nazwaType
					$adres->nazwa2 = $nazwa2; // nazwa2Type
					$adres->ulica = $dane1['ulica1']; // ulicaType
					$adres->numerDomu = $dane1['nr1']; // numerDomuType
					$adres->numerLokalu = $dane1['mieszkanie1']; // numerLokaluType
					$adres->miejscowosc = $dane1['miasto1']; // miejscowoscType
					$adres->kodPocztowy = str_replace('-', '', $dane1['kod1']); // kodPocztowyType
					$adres->kraj = 'Polska'; // krajType
					$adres->telefon = str_replace(' ', '', $dane1['telefon1']); // telefonType
					$adres->email = $dane1['email']; // emailType

					if (strlen($dane1['komorka1']) > 9) {
						$ile = strlen(str_replace(' ', '', $dane1['komorka1'])) - 9;
						$num = substr(str_replace(' ', '', $dane1['komorka1']), $ile, 9);
					} else {
						$num = $dane1['komorka1'];
					}
					$adres->mobile = $num; // mobileType
					//print_r($adres);
					if ($dane1['typ'] == 'paczka') {
						$paczka = $this->_request->getPost('paczka');
						$typ = new paczkaPocztowaType();
						$typ->posteRestante = 0; // boolean
						if (isset($paczka['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $paczka['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}

						$typ->kategoria = $paczka['typ']; // kategoriaType
						$typ->gabaryt = $paczka['gabaryt']; // gabarytType
						$typ->masa = (0 + str_replace(',', '.', $paczka['masa']) * 1000); // masaType
						if (isset($paczka['dwartosc'])) {
							$liczba = (0 + str_replace(',', '.', $paczka['wartosc']));
							$typ->wartosc = ($liczba * 100); // wartoscType
						} else {
							$typ->wartosc = ''; // wartoscType
						}
						if (isset($paczka['zwrot']))
							$typ->zwrotDoslanie = 1; // boolean
						if (isset($paczka['biblioteka']))
							$typ->egzemplarzBiblioteczny = 1; // boolean
						if (isset($paczka['ociemniali']))
							$typ->dlaOciemnialych = 1; // boolean
					}
					elseif ($dane1['typ'] == 'pobraniowa') {
						$pobranie = $this->_request->getPost('pobraniowa');
						$typ = new przesylkaPobraniowaType();
						$typ1 = new pobranieType();
						$typ1->sposobPobrania = $pobranie['sposob']; // sposobPobraniaType
						$typ1->kwotaPobrania = (0 + str_replace(',', '.', $pobranie['kwota']) * 100); // kwotaPobraniaType
						$typ1->nrb = $pobranie['rachunek']; // anonymous51
						$typ1->tytulem = $pobranie['tytul']; // anonymous52
						if (isset($pobranie['sprawdz']))
							$typ1->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = 1; // boolean


						$typ->pobranie = $typ1; // pobranieType
						$typ->posteRestante = 0; // boolean
						if (isset($pobranie['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $pobranie['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}


						$typ->kategoria = $pobranie['typ']; // kategoriaType
						$typ->gabaryt = $pobranie['gabaryt']; // gabarytType
						if (isset($pobranie['ostroznie']))
							$typ->ostroznie; // boolean
						if (isset($pobranie['dwartosc'])) {
							$liczba = (0 + str_replace(',', '.', $pobranie['wartosc']));
							$typ->wartosc = ($liczba * 100); // wartoscType
						} else {
							$typ->wartosc = ''; // wartoscType
						}

						$typ->masa = (0 + str_replace(',', '.', $pobranie['masa']) * 1000); // masaType
						if(!empty($pobranie['opis']))
						{
							$typ->opis = $pobranie['opis'];
						}
					} 
					elseif ($dane1['typ'] == 'polecona') {
						$polecenie = $this->_request->getPost('polecona');
						$typ = new przesylkaPoleconaKrajowaType();
						$typ->epo = new EPOSimpleType(); // EPOType
						$typ->posteRestante = 0; // boolean
						if (isset($polecenie['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $polecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}

						$typ->kategoria = $polecenie['typ']; // kategoriaType
						$typ->gabaryt = $polecenie['gabaryt']; // gabarytType
						$typ->masa = (0 + str_replace(',', '.', $polecenie['masa']) * 1000); // masaType
						if (isset($polecenie['biblioteka']))
							$typ->egzemplarzBiblioteczny = 1; // boolean
						if (isset($polecenie['ociemniali']))
							$typ->dlaOciemnialych = 1; // boolean
						if(!empty($polecenie['opis']))
						{
							$typ->opis = $polecenie['opis'];
						}
						if(!empty($polecenie['opis']))
						{
							$typ->opis = $polecenie['opis'];
						}
					}
					elseif ($dane1['typ'] == 'firmowapolecona') {
						$firmowapolecenie = $this->_request->getPost('firmowapolecona');
						$typ = new przesylkaFirmowaPoleconaType();
						$typ->posteRestante = 0; // boolean
						if (isset($firmowapolecenie['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $firmowapolecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}
						$typ->kategoria = $firmowapolecenie['typ']; // kategoriaType
						$typ->masa = (0 + str_replace(',', '.', $firmowapolecenie['masa']) * 1000); // masaType
						if (isset($firmowapolecenie['biblioteka']))
							$typ->egzemplarzBiblioteczny = 1; // boolean
						if (isset($firmowapolecenie['ociemniali']))
							$typ->dlaOciemnialych = 1; // boolean
						if(!empty($firmowapolecenie['opis']))
						{
							$typ->opis = $firmowapolecenie['opis'];
						}
					}
					elseif ($dane1['typ'] == 'deklaracja') {
						$deklaracje = $this->_request->getPost('deklaracja');
						$typ = new przesylkaListowaZadeklarowanaWartoscType();
						$typ->posteRestante = 0; // boolean
						$liczba = (0 + str_replace(',', '.', $deklaracje['wartosc']));
						$typ->wartosc = ($liczba * 100); // wartoscType

						if (isset($deklaracje['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $deklaracje['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}

						$typ->kategoria = $deklaracje['typ']; // kategoriaType
						$typ->gabaryt = $deklaracje['gabaryt']; // gabarytType
						$typ->masa = (0 + str_replace(',', '.', $deklaracje['masa']) * 1000); // masaType
						if (isset($deklaracje['zwrot']))
							$typ->zwrotDoslanie = 1; // boolean
						if (isset($deklaracje['biblioteka']))
							$typ->egzemplarzBiblioteczny = 1; // boolean
						if (isset($deklaracje['ociemniali']))
							$typ->dlaOciemnialych = 1; // boolean
						if(!empty($deklaracje['opis']))
						{
							$typ->opis = $deklaracje['opis'];
						}
					}
					elseif ($dane1['typ'] == 'nierejestrowana') {
						$nierejestrowana = $this->_request->getPost('nierejestrowana');
						$typ = new listZwyklyType();
						$typ->posteRestante = 0; // boolean
						$typ->ilosc = 1;
						$typ->kategoria = $nierejestrowana['typ']; // kategoriaType
						$typ->gabaryt = $nierejestrowana['gabaryt']; // gabarytType
						$typ->masa = (0 + str_replace(',', '.', $nierejestrowana['masa']) * 1000); // masaType
						if(!empty($nierejestrowana['opis']))
						{
							$typ->opis = $nierejestrowana['opis'];
						}
					}
					elseif ($dane1['typ'] == 'firmowanierejestrowana') {
						$firmowanierejestrowana = $this->_request->getPost('firmowanierejestrowana');
						$typ = new listZwyklyType();
						$typ->posteRestante = 0; // boolean
						$typ->ilosc = 1;
						$typ->kategoria = $firmowanierejestrowana['typ']; // kategoriaType
						$typ->masa = (0 + str_replace(',', '.', $firmowanierejestrowana['masa']) * 1000); // masaType
						if(!empty($firmowanierejestrowana['opis']))
						{
							$typ->opis = $firmowanierejestrowana['opis'];
						}
					}
					elseif ($dane1['typ'] == 'uslugakurierska'){
						$uslugakurierska = $this->_request->getPost('uslugakurierska');
						$typ = new uslugaKurierskaType();
						$typ->doreczenie = new doreczenieUslugaKurierskaType();
						$typ->termin = $uslugakurierska['termin'];
						if(!empty($uslugakurierska['masa']))
						{
							$typ->masa = new masaType();
							$typ->masa = (0 + str_replace(',', '.', $uslugakurierska['masa']) * 1000); // masaType
						}
						if(!empty($uslugakurierska['uiszczaOplate']))
						{
							switch($uslugakurierska['uiszczaOplate'])
							{
								case 'A':
									$typ->uiszczaOplate = uiszczaOplateType::ADRESAT;
								break;
								case 'N':
									$typ->uiszczaOplate = uiszczaOplateType::NADAWCA;
								break;
							}
						}
						if(!empty($uslugakurierska['godzdoreczenia']))
						{
								$typ->doreczenie->oczekiwanaGodzinaDoreczenia = $uslugakurierska['godzinaDoreczenia'];
						}
						$typ->zawartosc = $uslugakurierska['zawartosc'];
						if(!empty($uslugakurierska['pobranie']))
						{
							$typ->pobranie = new pobranieType();
							if(!empty($uslugakurierska['sposob']))
							{
								$typ->pobranie->sposobPobrania = $uslugakurierska['sposob'];
								$liczba = (0 + str_replace(',', '.', $uslugakurierska['kwota']));
								$typ->pobranie->kwotaPobrania = ($liczba * 100); // wartoscType
								
								$typ->pobranie->nrb = $uslugakurierska['rachunek'];
								$typ->pobranie->tytulem = $uslugakurierska['tytul'];
							}
						}
						if(!empty($uslugakurierska['wartosc']))
						{
							$wartosc = (0 + str_replace(',', '.', $uslugakurierska['wartosc']));
							$typ->wartosc = ($wartosc * 100); // wartoscType
						}
						if(!empty($uslugakurierska['potwierdzenieOdbioru']))
						{
							$typ->potwierdzenieOdbioru = new potwierdzenieOdbioruKurierskaType();
							$typ->potwierdzenieOdbioru->sposob = $uslugakurierska['potwierdzenieOdbioru'];
							$typ->potwierdzenieOdbioru->ilosc = $uslugakurierska['potwierdzenieOdbioruIlosc'];
						}
						if(!empty($uslugakurierska['potwierdzenieDoreczenia']))
						{
							$typ->potwierdzenieDoreczenia = new potwierdzenieDoreczeniaType();
							$typ->potwierdzenieDoreczenia->sposob = $uslugakurierska['potwierdzenieDoreczenia'];
							$typ->potwierdzenieDoreczenia->kontakt = $uslugakurierska['potwierdzenieDoreczeniaKontakt'];
						}
						if(!empty($uslugakurierska['ostroznie']))
						{
							$typ->ostroznie = '1';
						}
						if(!empty($uslugakurierska['ponadgabaryt']))
						{
							$typ->ponadgabaryt = '1';
						}
						if(!empty($uslugakurierska['sprZawrPrzezOdbiorce']))
						{
							$typ->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = '1';
						}
						if(!empty($uslugakurierska['doreczenieWeWskaznymDniu']))
						{
							$typ->doreczenie->oczekiwanyTerminDoreczenia = $uslugakurierska['doreczenieWeWskaznymDniuData'];
						}
						if(!empty($uslugakurierska['doRakWlasnych']))
						{
							$typ->doreczenie->doRakWlasnych = '1';
						}
						if(!empty($uslugakurierska['wSobote']))
						{
							$typ->doreczenie->wSobote = '1';
							$typ->doreczenie->oczekiwanaGodzinaDoreczenia = null;
						}
						if(!empty($uslugakurierska['odbiorPrzesylkiOdNadawcyType']))
						{
							$typ->odbiorPrzesylkiOdNadawcy = new odbiorPrzesylkiOdNadawcyType();
							$typ->odbiorPrzesylkiOdNadawcy->wSobote = '1';
						}
						if(!empty($uslugakurierska['ubezpieczenie']))
						{
							$typ->ubezpieczenie = new ubezpieczenieType();
							$typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
							$typ->ubezpieczenie->kwota = $uslugakurierska['ubezpieczenieKwota'];
						}
						if(!empty($uslugakurierska['dokZwrotDiv']))
						{
							$typ->zwrotDokumentow = new zwrotDokumentowKurierskaType();
							$typ->zwrotDokumentow->rodzajList = new rodzajListType();
							//$typ->zwrotDokumentow->rodzajPaczka = new terminZwrotDokumentowPaczkowaType();
							//$typ->zwrotDokumentow->rodzajPocztex = new terminZwrotDokumentowPaczkowaType();
							switch($uslugakurierska['zwrotDokumentow'])
							{
								case 'LZP':
									$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
								break;
								case 'LZE':
									$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
								break;
								case 'LPP':
									$typ->zwrotDokumentow->rodzajList->polecony = '1';
									$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
								break;
								case 'LPE':
									$typ->zwrotDokumentow->rodzajList->polecony = '1';
									$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
								break;
								case 'P24':
									$typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_24;
								break;
								case 'P48':
									$typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_48;
								break;
								case 'KE24':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::EKSPRES24;
								break;
								case 'KMD3GD5KM':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_5KM;
								break;
								case 'KMD3GD10KM':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_10KM;
								break;
								case 'KMD3GD15KM':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_15KM;
								break;
								case 'KMD3GP15KM':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_POWYZEJ_15KM;
								break;
								case 'KBD20KG':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::BEZPOSREDNI_DO_20KG;
								break;
								
							}
						}
						if(!empty($uslugakurierska['numerPrzesylkiKlienta']))
						{
							$typ->numerPrzesylkiKlienta = $uslugakurierska['numerPrzesylkiKlienta'];
						}
						if(!empty($uslugakurierska['opis']))
						{
							$typ->opis = $uslugakurierska['opis'];
						}
					}
					elseif ($dane1['typ'] == 'przesylkakurierska48'){ //przesylka biznesowa
						$przesylkakurierska48 = $this->_request->getPost('przesylkakurierska48');
						$typ = new przesylkaBiznesowaType();
						if(!empty($przesylkakurierska48['pobranie']))
						{
							$typ->pobranie = new pobranieType();
							if(!empty($przesylkakurierska48['sposob']))
							{
								$typ->pobranie->sposobPobrania = $przesylkakurierska48['sposob'];
								$kwota = (0 + str_replace(',', '.', $przesylkakurierska48['kwota']));
								$typ->pobranie->kwotaPobrania = ($kwota * 100); // wartoscType
								
								$typ->pobranie->nrb = $przesylkakurierska48['rachunek'];
								$typ->pobranie->tytulem = $przesylkakurierska48['tytul'];
							}
						}
						if(!empty($przesylkakurierska48['gabaryt']))
						{
							switch($przesylkakurierska48['gabaryt'])
							{
								case 'XXL':
									$typ->gabaryt = gabarytBiznesowaType::XXL;
								break;
							}
						}
						if(!empty($przesylkakurierska48['wartosc']) && !empty($przesylkakurierska48['masa']))
						{
							$wartosc = (0 + str_replace(',', '.', $przesylkakurierska48['wartosc']));
							$typ->wartosc = ($wartosc * 100); // wartoscType
							$typ->masa = (0 + str_replace(',', '.', $przesylkakurierska48['masa']) * 1000); // masaType
						}
						if(!empty($przesylkakurierska48['ostroznie']))
						{
							$typ->ostroznie = '1';
						}
						if(!empty($przesylkakurierska48['ubezpieczenie']))
						{
							$typ->ubezpieczenie = new ubezpieczenieType();
							$typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
							$typ->ubezpieczenie->kwota = $przesylkakurierska48['ubezpieczenieKwota'];
						}
						if(!empty($przesylkakurierska48['opis']))
						{
							$typ->opis = $przesylkakurierska48['opis'];
						}
					}


					 //print_r($typ);die();
					try {
						$oPoczta = new Pocztapolskakonto();
						$Poczta = $oPoczta->pojedyncza(1);
						if(!empty($Poczta['data_nowego_zbioru']))
						{
							if($Poczta['data_nowego_zbioru'] !== date('Y-m-d') || empty($Poczta['idBufor']))
							{
								$getUrzedyNadania = new getUrzedyNadania();
								$UrzedyNadania = $en->getUrzedyNadania($getUrzedyNadania);
								//print_r($UrzedyNadania['urzedyNadania']['urzadNadania']); die();
								$buforType = new buforType();
								$buforType->dataNadania = date('Y-m-d');
								$buforType->opis = 'BigCom-'.date('Y-m-d');
								$buforType->active = '1';
								$buforType->urzadNadania = $UrzedyNadania['urzedyNadania']['urzadNadania'];
								$createBufor = new createEnvelopeBufor();
								$createBufor->bufor = $buforType;
								$newBufor = $en->createEnvelopeBufor($createBufor);
								
							
								$zmiany['data_nowego_zbioru'] = $newBufor['createdBufor']['dataNadania'];
								$zmiany['idBufor'] = $newBufor['createdBufor']['idBufor'];
								$Poczta = $oPoczta->edytuj($zmiany , 1);
							}
						}
					
						$Poczta = $oPoczta->pojedyncza(1);
					/*
						$getEnvelopeBuforList = new getEnvelopeBuforList();
						$BuforList = $en->getEnvelopeBuforList($getEnvelopeBuforList);
						print_r($BuforList);die();
					*/	
						$guid = new getGuid();
						$guid->ilosc = 1;
						$gui = $en->getGuid($guid);
						$typ->guid = $gui['guid'];
						//$typ->guid = $gui;
						$typ->adres = $adres;
						//print_r($typ);die();
						$add = new addShipment();
						$add->przesylki[] = $typ;
						$add->idBufor = $Poczta['idBufor'];
					
						
						
						
						$wyslij = $en->addShipment($add);
						//print_r($wyslij);
						if (empty($wyslij['retval'][0]['error'])) {
							$this->view->blad_edycji = '<div class="k_ok">Poprawnie wysłano przesyłkę.</div>';
							$this->view->error = '';
							$this->view->error .= 'Numer nadania: ' . $wyslij['retval'][0]['numerNadania'];
							$this->view->error .= '<br />Numer GUID: ' . $wyslij['retval'][0]['guid'];

							$zam['przesylka'] = $wyslij['retval'][0]['numerNadania'];
							$zam['kurier'] = 'Poczta Polska';
							$zamow = new AllegroZamowienia($module = 'admin');
							$edit = $zamow->edytuj($zamowienie, $zam);
							
							$res_idsy = array_shift($idsy);
							$_SESSION['poczta_polska_hurtem'] = $idsy;
							if(!empty($res_idsy)){
								$this->view->error .= '<br />Zostało jeszcze przesyłek do nadania: <strong>'.count($idsy).'</strong>';
								if (!empty($idsy[0])) {
									$zamowienie = $idsy[0];
									$dane = $zamowienia->selectWybraneZamowienie($zamowienie);
									if (!empty($idsy[0])) {
										$zamowienie = $idsy[0];
										//print_r($zamowienie);
										$dane = $zamowienia->selectWybraneZamowienie($zamowienie);
										if(!empty($ppwaga[$zamowienie]))
										{
											$dane['masa'] = $ppwaga[$zamowienie] / 1000;
										}
									}
								}
							}
							//$this->getResponse()->setHeader('Refresh', '1; URL=' . $this->baseUrl . '/admin/allegrozamowienia/wypisz/tryb/aktualne/mode/all');
						} else {
							$this->view->blad_edycji = 'Błąd danych. Przesyłka nie została zatwierdzona.';
							$this->view->error = '';
							if (isset($wyslij['retval']['error']['0'])) {
								for ($i = 0; $i < count($wyslij['retval']['error']); $i++) {
									$this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error'][$i]['errorNumber'] . ': ' . $wyslij['retval']['error'][$i]['errorDesc'];
								}
							} else {
								$this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error']['errorNumber'] . ': ' . $wyslij['retval']['error']['errorDesc'];
							}
						}
					} catch (SoapFault $error) {
						$this->view->error = 'Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.';
					}
				}
				$this->view->ppk = $ppk->pojedyncza('1');
				$this->view->poczta = $poczta;
				$tmp = explode(' ', str_replace('  ', ' ', $dane['imie_korespondencja'])); //rozbij na imie i nazwisko
				$dane['imie_korespondencja'] = $tmp[0];
				$dane['nazwisko_korespondencja'] = $tmp[1];
				$tmp2 = explode(' ', $dane['ulica_korespondencja']); //rozbij  na ulice i numer
				$tmp3 = explode('/', end($tmp2)); //rozbij na nr domu i lokalu
				if(!empty($tmp3[1]))
				{
					$dane['nr_korespondencja'] = $tmp3[0];
					$dane['mieszkanie_korespondencja'] = $tmp3[1];
					array_pop($tmp2);
					$dane['ulica_korespondencja'] = implode(' ', $tmp2);
				} else {
					$dane['nr_korespondencja'] = end($tmp2);
					array_pop($tmp2);
					$dane['ulica_korespondencja'] = implode(' ', $tmp2);
				}
				$dane['kwotazaplata'] = str_replace('.', ',', $dane['kwotazaplata']);
				if(!empty($dane['kurier']))
				{
					$this->view->error .= '<br />Przesyłka dla tego zamówienia była już tworzona. Sprawdź to żeby nie wysłać jeszcze raz tego samego.';
				}
				$this->view->dane = $dane;
			
		
		} else {
			$this->view->error = '<br />Brak elementow';
		}
	}
	
	/* dodawanie hurtowo listow przewozowych dla Poczty polskiej versja 2 */
	public function dodajwebapiallegrohurtv2Action() 
	{
		$idsy = $_SESSION['poczta_polska_hurtem'];
		$ppwaga = $_SESSION['pocztapolska_waga'];
		
		if(!empty($idsy))
		{			
				$pocztapolska = new Pocztapolska();
				$zamowienia = new AllegroZamowienia();
				$ppk = new Pocztapolskakonto();
				$en = new ElektronicznyNadawca();
				
				/*
				if (!empty($idsy[0])) {
					$zamowienie = $idsy[0];
					//print_r($zamowienie);
					$dane = $zamowienia->selectWybraneZamowienie($zamowienie);
					if(!empty($ppwaga[$zamowienie]))
					{
						$dane['masa'] = $ppwaga[$zamowienie] / 1000;
					}
				}
				*/
		
		
				if (!isset($poczta))
					$poczta = $pocztapolska->wypiszOne($this->view->adminID);

		
		
			$dane2 = $this->_request->getPost('dane');
			if (isset($dane2) && !empty($dane2)) 
			{
				foreach($dane2 as $k => $dane1)
				{
				
					$adres = new adresType();
					if (strlen($dane1['nazwa_firmy1']) > 1) {
						$nazwa = $dane1['nazwa_firmy1'];
						$nazwa2 = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
					} else {
						$nazwa = $dane1['imie1'] . ' ' . $dane1['nazwisko1'];
						$nazwa2 = '';
					}
					$adres->nazwa = $nazwa; // nazwaType
					$adres->nazwa2 = $nazwa2; // nazwa2Type
					$adres->ulica = $dane1['ulica1']; // ulicaType
					$adres->numerDomu = $dane1['nr1']; // numerDomuType
					$adres->numerLokalu = $dane1['mieszkanie1']; // numerLokaluType
					$adres->miejscowosc = $dane1['miasto1']; // miejscowoscType
					$adres->kodPocztowy = str_replace('-', '', $dane1['kod1']); // kodPocztowyType
					$adres->kraj = 'Polska'; // krajType
					$adres->telefon = str_replace(' ', '', $dane1['telefon1']); // telefonType
					$adres->email = $dane1['email']; // emailType

					if (strlen($dane1['komorka1']) > 9) {
						$ile = strlen(str_replace(' ', '', $dane1['komorka1'])) - 9;
						$num = substr(str_replace(' ', '', $dane1['komorka1']), $ile, 9);
					} else {
						$num = $dane1['komorka1'];
					}
					$adres->mobile = $num; // mobileType
					//print_r($adres);
					if ($dane1['typ'] == 'paczka') {
						$paczka = $this->_request->getPost('paczka');
						$paczka = $paczka[$k];
						$typ = new paczkaPocztowaType();
						$typ->posteRestante = 0; // boolean
						if (isset($paczka['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $paczka['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}

						$typ->kategoria = $paczka['typ']; // kategoriaType
						$typ->gabaryt = $paczka['gabaryt']; // gabarytType
						$typ->masa = (0 + str_replace(',', '.', $paczka['masa']) * 1000); // masaType
						if (isset($paczka['dwartosc'])) {
							$liczba = (0 + str_replace(',', '.', $paczka['wartosc']));
							$typ->wartosc = ($liczba * 100); // wartoscType
						} else {
							$typ->wartosc = ''; // wartoscType
						}
						if (isset($paczka['zwrot']))
							$typ->zwrotDoslanie = 1; // boolean
						if (isset($paczka['biblioteka']))
							$typ->egzemplarzBiblioteczny = 1; // boolean
						if (isset($paczka['ociemniali']))
							$typ->dlaOciemnialych = 1; // boolean
					}
					elseif ($dane1['typ'] == 'pobraniowa') {
						$pobranie = $this->_request->getPost('pobraniowa');
						$pobranie = $pobranie[$k];
						$typ = new przesylkaPobraniowaType();
						$typ1 = new pobranieType();
						$typ1->sposobPobrania = $pobranie['sposob']; // sposobPobraniaType
						$typ1->kwotaPobrania = (0 + str_replace(',', '.', $pobranie['kwota']) * 100); // kwotaPobraniaType
						$typ1->nrb = $pobranie['rachunek']; // anonymous51
						$typ1->tytulem = $pobranie['tytul']; // anonymous52
						if (isset($pobranie['sprawdz']))
							$typ1->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = 1; // boolean


						$typ->pobranie = $typ1; // pobranieType
						$typ->posteRestante = 0; // boolean
						if (isset($pobranie['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $pobranie['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}


						$typ->kategoria = $pobranie['typ']; // kategoriaType
						$typ->gabaryt = $pobranie['gabaryt']; // gabarytType
						if (isset($pobranie['ostroznie']))
							$typ->ostroznie; // boolean
						if (isset($pobranie['dwartosc'])) {
							$liczba = (0 + str_replace(',', '.', $pobranie['wartosc']));
							$typ->wartosc = ($liczba * 100); // wartoscType
						} else {
							$typ->wartosc = ''; // wartoscType
						}

						$typ->masa = (0 + str_replace(',', '.', $pobranie['masa']) * 1000); // masaType
						if(!empty($pobranie['opis']))
						{
							$typ->opis = $pobranie['opis'];
						}
					} 
					elseif ($dane1['typ'] == 'polecona') {
						$polecenie = $this->_request->getPost('polecona');
						$polecenie = $polecenie[$k];
						$typ = new przesylkaPoleconaKrajowaType();
						$typ->epo = new EPOSimpleType(); // EPOType
						$typ->posteRestante = 0; // boolean
						if (isset($polecenie['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $polecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}

						$typ->kategoria = $polecenie['typ']; // kategoriaType
						$typ->gabaryt = $polecenie['gabaryt']; // gabarytType
						$typ->masa = (0 + str_replace(',', '.', $polecenie['masa']) * 1000); // masaType
						if (isset($polecenie['biblioteka']))
							$typ->egzemplarzBiblioteczny = 1; // boolean
						if (isset($polecenie['ociemniali']))
							$typ->dlaOciemnialych = 1; // boolean
						if(!empty($polecenie['opis']))
						{
							$typ->opis = $polecenie['opis'];
						}
						if(!empty($polecenie['opis']))
						{
							$typ->opis = $polecenie['opis'];
						}
					}
					elseif ($dane1['typ'] == 'firmowapolecona') {
						$firmowapolecenie = $this->_request->getPost('firmowapolecona');
						$firmowapolecenie = $firmowapolecenie[$k];
						$typ = new przesylkaFirmowaPoleconaType();
						$typ->posteRestante = 0; // boolean
						if (isset($firmowapolecenie['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $firmowapolecenie['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}
						$typ->kategoria = $firmowapolecenie['typ']; // kategoriaType
						$typ->masa = (0 + str_replace(',', '.', $firmowapolecenie['masa']) * 1000); // masaType
						if (isset($firmowapolecenie['biblioteka']))
							$typ->egzemplarzBiblioteczny = 1; // boolean
						if (isset($firmowapolecenie['ociemniali']))
							$typ->dlaOciemnialych = 1; // boolean
						if(!empty($firmowapolecenie['opis']))
						{
							$typ->opis = $firmowapolecenie['opis'];
						}
					}
					elseif ($dane1['typ'] == 'deklaracja') {
						$deklaracje = $this->_request->getPost('deklaracja');
						$deklaracje = $deklaracje[$k];
						$typ = new przesylkaListowaZadeklarowanaWartoscType();
						$typ->posteRestante = 0; // boolean
						$liczba = (0 + str_replace(',', '.', $deklaracje['wartosc']));
						$typ->wartosc = ($liczba * 100); // wartoscType

						if (isset($deklaracje['dpotwierdzenie'])) {
							$typ->iloscPotwierdzenOdbioru = $deklaracje['potwierdzenie']; // iloscPotwierdzenOdbioruType
						} else {
							$typ->iloscPotwierdzenOdbioru = 0;
						}

						$typ->kategoria = $deklaracje['typ']; // kategoriaType
						$typ->gabaryt = $deklaracje['gabaryt']; // gabarytType
						$typ->masa = (0 + str_replace(',', '.', $deklaracje['masa']) * 1000); // masaType
						if (isset($deklaracje['zwrot']))
							$typ->zwrotDoslanie = 1; // boolean
						if (isset($deklaracje['biblioteka']))
							$typ->egzemplarzBiblioteczny = 1; // boolean
						if (isset($deklaracje['ociemniali']))
							$typ->dlaOciemnialych = 1; // boolean
						if(!empty($deklaracje['opis']))
						{
							$typ->opis = $deklaracje['opis'];
						}
					}
					elseif ($dane1['typ'] == 'nierejestrowana') {
						$nierejestrowana = $this->_request->getPost('nierejestrowana');
						$nierejestrowana = $nierejestrowana[$k];
						$typ = new listZwyklyType();
						$typ->posteRestante = 0; // boolean
						$typ->ilosc = 1;
						$typ->kategoria = $nierejestrowana['typ']; // kategoriaType
						$typ->gabaryt = $nierejestrowana['gabaryt']; // gabarytType
						$typ->masa = (0 + str_replace(',', '.', $nierejestrowana['masa']) * 1000); // masaType
						if(!empty($nierejestrowana['opis']))
						{
							$typ->opis = $nierejestrowana['opis'];
						}
					}
					elseif ($dane1['typ'] == 'firmowanierejestrowana') {
						$firmowanierejestrowana = $this->_request->getPost('firmowanierejestrowana');
						$firmowanierejestrowana = $firmowanierejestrowana[$k];
						$typ = new listZwyklyType();
						$typ->posteRestante = 0; // boolean
						$typ->ilosc = 1;
						$typ->kategoria = $firmowanierejestrowana['typ']; // kategoriaType
						$typ->masa = (0 + str_replace(',', '.', $firmowanierejestrowana['masa']) * 1000); // masaType
						if(!empty($firmowanierejestrowana['opis']))
						{
							$typ->opis = $firmowanierejestrowana['opis'];
						}
					}
					elseif ($dane1['typ'] == 'uslugakurierska'){
						$uslugakurierska = $this->_request->getPost('uslugakurierska');
						$uslugakurierska = $uslugakurierska[$k];
						$typ = new uslugaKurierskaType();
						$typ->doreczenie = new doreczenieUslugaKurierskaType();
						$typ->termin = $uslugakurierska['termin'];
						if(!empty($uslugakurierska['masa']))
						{
							$typ->masa = new masaType();
							$typ->masa = (0 + str_replace(',', '.', $uslugakurierska['masa']) * 1000); // masaType
						}
						if(!empty($uslugakurierska['uiszczaOplate']))
						{
							switch($uslugakurierska['uiszczaOplate'])
							{
								case 'A':
									$typ->uiszczaOplate = uiszczaOplateType::ADRESAT;
								break;
								case 'N':
									$typ->uiszczaOplate = uiszczaOplateType::NADAWCA;
								break;
							}
						}
						if(!empty($uslugakurierska['godzdoreczenia']))
						{
								$typ->doreczenie->oczekiwanaGodzinaDoreczenia = $uslugakurierska['godzinaDoreczenia'];
						}
						$typ->zawartosc = $uslugakurierska['zawartosc'];
						if(!empty($uslugakurierska['pobranie']))
						{
							$typ->pobranie = new pobranieType();
							if(!empty($uslugakurierska['sposob']))
							{
								$typ->pobranie->sposobPobrania = $uslugakurierska['sposob'];
								$liczba = (0 + str_replace(',', '.', $uslugakurierska['kwota']));
								$typ->pobranie->kwotaPobrania = ($liczba * 100); // wartoscType
								
								$typ->pobranie->nrb = $uslugakurierska['rachunek'];
								$typ->pobranie->tytulem = $uslugakurierska['tytul'];
							}
						}
						if(!empty($uslugakurierska['wartosc']))
						{
							$wartosc = (0 + str_replace(',', '.', $uslugakurierska['wartosc']));
							$typ->wartosc = ($wartosc * 100); // wartoscType
						}
						if(!empty($uslugakurierska['potwierdzenieOdbioru']))
						{
							$typ->potwierdzenieOdbioru = new potwierdzenieOdbioruKurierskaType();
							$typ->potwierdzenieOdbioru->sposob = $uslugakurierska['potwierdzenieOdbioru'];
							$typ->potwierdzenieOdbioru->ilosc = $uslugakurierska['potwierdzenieOdbioruIlosc'];
						}
						if(!empty($uslugakurierska['potwierdzenieDoreczenia']))
						{
							$typ->potwierdzenieDoreczenia = new potwierdzenieDoreczeniaType();
							$typ->potwierdzenieDoreczenia->sposob = $uslugakurierska['potwierdzenieDoreczenia'];
							$typ->potwierdzenieDoreczenia->kontakt = $uslugakurierska['potwierdzenieDoreczeniaKontakt'];
						}
						if(!empty($uslugakurierska['ostroznie']))
						{
							$typ->ostroznie = '1';
						}
						if(!empty($uslugakurierska['ponadgabaryt']))
						{
							$typ->ponadgabaryt = '1';
						}
						if(!empty($uslugakurierska['sprZawrPrzezOdbiorce']))
						{
							$typ->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = '1';
						}
						if(!empty($uslugakurierska['doreczenieWeWskaznymDniu']))
						{
							$typ->doreczenie->oczekiwanyTerminDoreczenia = $uslugakurierska['doreczenieWeWskaznymDniuData'];
						}
						if(!empty($uslugakurierska['doRakWlasnych']))
						{
							$typ->doreczenie->doRakWlasnych = '1';
						}
						if(!empty($uslugakurierska['wSobote']))
						{
							$typ->doreczenie->wSobote = '1';
							$typ->doreczenie->oczekiwanaGodzinaDoreczenia = null;
						}
						if(!empty($uslugakurierska['odbiorPrzesylkiOdNadawcyType']))
						{
							$typ->odbiorPrzesylkiOdNadawcy = new odbiorPrzesylkiOdNadawcyType();
							$typ->odbiorPrzesylkiOdNadawcy->wSobote = '1';
						}
						if(!empty($uslugakurierska['ubezpieczenie']))
						{
							$typ->ubezpieczenie = new ubezpieczenieType();
							$typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
							$typ->ubezpieczenie->kwota = $uslugakurierska['ubezpieczenieKwota'];
						}
						if(!empty($uslugakurierska['dokZwrotDiv']))
						{
							$typ->zwrotDokumentow = new zwrotDokumentowKurierskaType();
							$typ->zwrotDokumentow->rodzajList = new rodzajListType();
							//$typ->zwrotDokumentow->rodzajPaczka = new terminZwrotDokumentowPaczkowaType();
							//$typ->zwrotDokumentow->rodzajPocztex = new terminZwrotDokumentowPaczkowaType();
							switch($uslugakurierska['zwrotDokumentow'])
							{
								case 'LZP':
									$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
								break;
								case 'LZE':
									$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
								break;
								case 'LPP':
									$typ->zwrotDokumentow->rodzajList->polecony = '1';
									$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
								break;
								case 'LPE':
									$typ->zwrotDokumentow->rodzajList->polecony = '1';
									$typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
								break;
								case 'P24':
									$typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_24;
								break;
								case 'P48':
									$typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_48;
								break;
								case 'KE24':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::EKSPRES24;
								break;
								case 'KMD3GD5KM':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_5KM;
								break;
								case 'KMD3GD10KM':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_10KM;
								break;
								case 'KMD3GD15KM':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_15KM;
								break;
								case 'KMD3GP15KM':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_POWYZEJ_15KM;
								break;
								case 'KBD20KG':
									$typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::BEZPOSREDNI_DO_20KG;
								break;
								
							}
						}
						if(!empty($uslugakurierska['numerPrzesylkiKlienta']))
						{
							$typ->numerPrzesylkiKlienta = $uslugakurierska['numerPrzesylkiKlienta'];
						}
						if(!empty($uslugakurierska['opis']))
						{
							$typ->opis = $uslugakurierska['opis'];
						}
					}
					elseif ($dane1['typ'] == 'przesylkakurierska48'){ //przesylka biznesowa
						$przesylkakurierska48 = $this->_request->getPost('przesylkakurierska48');
						$przesylkakurierska48 = $przesylkakurierska48[$k];
						$typ = new przesylkaBiznesowaType();
						if(!empty($przesylkakurierska48['pobranie']))
						{
							$typ->pobranie = new pobranieType();
							if(!empty($przesylkakurierska48['sposob']))
							{
								$typ->pobranie->sposobPobrania = $przesylkakurierska48['sposob'];
								$kwota = (0 + str_replace(',', '.', $przesylkakurierska48['kwota']));
								$typ->pobranie->kwotaPobrania = ($kwota * 100); // wartoscType
								
								$typ->pobranie->nrb = $przesylkakurierska48['rachunek'];
								$typ->pobranie->tytulem = $przesylkakurierska48['tytul'];
							}
						}
						if(!empty($przesylkakurierska48['gabaryt']))
						{
							switch($przesylkakurierska48['gabaryt'])
							{
								case 'XXL':
									$typ->gabaryt = gabarytBiznesowaType::XXL;
								break;
							}
						}
						if(!empty($przesylkakurierska48['wartosc']) && !empty($przesylkakurierska48['masa']))
						{
							$wartosc = (0 + str_replace(',', '.', $przesylkakurierska48['wartosc']));
							$typ->wartosc = ($wartosc * 100); // wartoscType
							$typ->masa = (0 + str_replace(',', '.', $przesylkakurierska48['masa']) * 1000); // masaType
						}
						if(!empty($przesylkakurierska48['ostroznie']))
						{
							$typ->ostroznie = '1';
						}
						if(!empty($przesylkakurierska48['ubezpieczenie']))
						{
							$typ->ubezpieczenie = new ubezpieczenieType();
							$typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
							$typ->ubezpieczenie->kwota = $przesylkakurierska48['ubezpieczenieKwota'];
						}
						if(!empty($przesylkakurierska48['opis']))
						{
							$typ->opis = $przesylkakurierska48['opis'];
						}
					}


					 //print_r($typ);die();
					try {
						$oPoczta = new Pocztapolskakonto();
						$Poczta = $oPoczta->pojedyncza(1);
						if(!empty($Poczta['data_nowego_zbioru']))
						{
							if($Poczta['data_nowego_zbioru'] !== date('Y-m-d') || empty($Poczta['idBufor']))
							{
								$getUrzedyNadania = new getUrzedyNadania();
								$UrzedyNadania = $en->getUrzedyNadania($getUrzedyNadania);
								//print_r($UrzedyNadania['urzedyNadania']['urzadNadania']); die();
								$buforType = new buforType();
								$buforType->dataNadania = date('Y-m-d');
								$buforType->opis = 'BigCom-'.date('Y-m-d');
								$buforType->active = '1';
								$buforType->urzadNadania = $UrzedyNadania['urzedyNadania']['urzadNadania'];
								$createBufor = new createEnvelopeBufor();
								$createBufor->bufor = $buforType;
								$newBufor = $en->createEnvelopeBufor($createBufor);
								
							
								$zmiany['data_nowego_zbioru'] = $newBufor['createdBufor']['dataNadania'];
								$zmiany['idBufor'] = $newBufor['createdBufor']['idBufor'];
								$Poczta = $oPoczta->edytuj($zmiany , 1);
							}
						}
					
						$Poczta = $oPoczta->pojedyncza(1);
					/*
						$getEnvelopeBuforList = new getEnvelopeBuforList();
						$BuforList = $en->getEnvelopeBuforList($getEnvelopeBuforList);
						print_r($BuforList);die();
					*/	
						$guid = new getGuid();
						$guid->ilosc = 1;
						$gui = $en->getGuid($guid);
						$typ->guid = $gui['guid'];
						//$typ->guid = $gui;
						$typ->adres = $adres;
						//print_r($typ);die();
						$add = new addShipment();
						$add->przesylki[] = $typ;
						$add->idBufor = $Poczta['idBufor'];
					
						
						
						
						$wyslij = $en->addShipment($add);
						//print_r($wyslij);
						if (empty($wyslij['retval']['error'])) {
							$this->view->error .= '<br /><div class="k_ok">Poprawnie wysłano przesyłkę o ID '.$k.'.</div>';
							$this->view->error .= '';
							$this->view->error .= 'Numer nadania: ' . $wyslij['retval']['numerNadania'];
							$this->view->error .= '<br />Numer GUID: ' . $wyslij['retval']['guid'];

							$zam['przesylka'] = $wyslij['retval']['numerNadania'];
							$zam['kurier'] = 'Poczta Polska';
							$zamow = new AllegroZamowienia($module = 'admin');
							$edit = $zamow->edytuj($k, $zam);
							
							//$res_idsy = array_shift($idsy);
							$klucz = array_search($k, $idsy);
							unset($idsy[$klucz]);
							$_SESSION['poczta_polska_hurtem'] = $idsy;
							if(!empty($idsy)){
								$this->view->error .= '<br />Zostało jeszcze przesyłek do nadania: <strong>'.count($idsy).'</strong>';
								/*
								if (!empty($idsy[0])) {
									$zamowienie = $idsy[0];
									$dane = $zamowienia->selectWybraneZamowienie($zamowienie);
									if (!empty($idsy[0])) {
										$zamowienie = $idsy[0];
										//print_r($zamowienie);
										$dane = $zamowienia->selectWybraneZamowienie($zamowienie);
										if(!empty($ppwaga[$zamowienie]))
										{
											$dane['masa'] = $ppwaga[$zamowienie] / 1000;
										}
									}
								}
								*/
							} else {
								$this->view->error .= '<br />Brak przesyłek do nadania.';
							}
							//$this->getResponse()->setHeader('Refresh', '1; URL=' . $this->baseUrl . '/admin/allegrozamowienia/wypisz/tryb/aktualne/mode/all');
						} else {
							$this->view->error .= '<div class="k_blad">Błąd danych. Przesyłka o ID <a href="#'.$k.'">'.$k.'</a> nie została zatwierdzona.</div>';
							$this->view->error .= '';
							if (isset($wyslij['retval']['error']['0'])) {
								for ($i = 0; $i < count($wyslij['retval']['error']); $i++) {
									$this->view->error .= 'Błąd numer: ' . $wyslij['retval']['error'][$i]['errorNumber'] . ': ' . $wyslij['retval']['error'][$i]['errorDesc'];
								}
							} else {
								$this->view->error .= '<br />Błąd numer: ' . $wyslij['retval']['error']['errorNumber'] . ': ' . $wyslij['retval']['error']['errorDesc'];
							}
						}
					} catch (SoapFault $error) {
						$this->view->error .= 'Błąd systemu Elektronicznego Nadawcy. Sprawdź poprawność danych.';
					}
				} // koniec foreach
			}
			if(!empty($idsy))
			{
				$dane_array = $zamowienia->selectWybraneZamowieniaForIds($idsy);
				$ile_w_array = count($dane_array);
				for($i = 0; $i < $ile_w_array; $i++)
				{
					$dane_array[$i]['masa'] = $ppwaga[$dane_array[$i]['id']] / 1000;
					
						$this->view->ppk = $ppk->pojedyncza('1');
						$this->view->poczta = $poczta;
						$tmp = explode(' ', str_replace('  ', ' ', $dane_array[$i]['imie_korespondencja'])); //rozbij na imie i nazwisko
						$dane_array[$i]['imie_korespondencja'] = $tmp[0];
						$dane_array[$i]['nazwisko_korespondencja'] = $tmp[1];
						$tmp2 = explode(' ', $dane_array[$i]['ulica_korespondencja']); //rozbij  na ulice i numer
						$tmp3 = explode('/', end($tmp2)); //rozbij na nr domu i lokalu
						if(!empty($tmp3[1]))
						{
							$dane_array[$i]['nr_korespondencja'] = $tmp3[0];
							$dane_array[$i]['mieszkanie_korespondencja'] = $tmp3[1];
							array_pop($tmp2);
							$dane_array[$i]['ulica_korespondencja'] = implode(' ', $tmp2);
						} else {
							$dane_array[$i]['nr_korespondencja'] = end($tmp2);
							array_pop($tmp2);
							$dane_array[$i]['ulica_korespondencja'] = implode(' ', $tmp2);
						}
						$dane_array[$i]['kwotazaplata'] = str_replace('.', ',', $dane_array[$i]['kwotazaplata']);
						if(!empty($dane_array[$i]['kurier']))
						{
							$dane_array[$i]['error'] = 'Przesyłka dla tego zamówienia była już tworzona. Sprawdź to żeby nie wysłać jeszcze raz tego samego.';
						}
						$this->view->dane_array = $dane_array;
				}
			} else {
				$this->view->error .= '<br />Brak elementow';
			}
		} else {
			$this->view->error .= '<br />Brak elementow';
		}
	}
}
?>