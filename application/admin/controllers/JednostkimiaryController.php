<?php
class Admin_JednostkimiaryController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		//echo $this->view->baseUrl;
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}	
	function dodajAction()
	{
		$jedn = new Jednostkimiary();
		
		$del = intval($this->_request->getParam('delid', 0));
		if($del > 0)
		{
			$jedn->id = $del;
			$jedn->usun();
			//$this->view->blad_edycji = '<div class="k_ok">Wybrana jednostka miary została usunięta.</div>';
			$this->_redirect('/admin/jednostkimiary/dodaj/');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/jednostkimiary/dodaj/');
		}
		$glowna = intval($this->_request->getParam('glowna', 0));
		if($glowna > 0)
		{
			$jedn->id = $glowna;
			$jedn->edytujLang(array('glowna' => '0'));
			$jedn->edytuj(array('glowna' => '1'));
			//$this->view->blad_edycji = '<div class="k_ok">Wybrana jednostka miary została usunięta.</div>';
			$this->_redirect('/admin/jednostkimiary/dodaj/');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/jednostkimiary/dodaj/');
		}
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa');
			$typ = $this->_request->getPost('typ');
			$dane = array('nazwa' => $nazwa, 'typ' => $typ);
			$jedn->dodaj($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/jednostkimiary/dodaj/');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/jednostkimiary/dodaj/');
		}			
		$this->view->jednostki = $jedn->wypisz();
	}	
	function edytujAction()
	{
		$jedn = new Jednostkimiary();
		if($this->_request->isPost())
		{
			$id = $this->_request->getPost('id');
			$nazwa = $this->_request->getPost('nazwa');
			$typ = $this->_request->getPost('typ');
			$dane = array('nazwa' => $nazwa, 'typ' => $typ);
			$jedn->id = $id;
			$jedn->edytuj($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/jednostkimiary/dodaj/');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/jednostkimiary/dodaj/');
		}
		$id = $this->_request->getParam('id', 0);		
		$jedn->id = $id;
		$this->view->jednostki = $jedn->wypisz();
		$this->view->pojedynczy = $jedn->wypiszPojedynczego();
		$this->view->tytul = $this->view->pojedynczy->nazwa;
	}
}
?>