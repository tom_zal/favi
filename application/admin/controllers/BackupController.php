<?php
class Admin_BackupController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->baseUrl = $this->_request->getBaseUrl();
		$this->buConfig = new Zend_Config_Ini('../application/config.ini', 'backup');
	}
    function __call($method, $args)
	{
        $this->_redirect('/admin/index/panel/');
    }
	public function backupAction()
	{
		set_time_limit(0);
		
		$bu = new Backup();			
		$this->view->realizacje = $bu->pobierzLista();    
			
		$data = $this->obConfig;
		$sciezka = $this->buConfig;
		
		$file = $this->_request->getParam('file'); 
		$base = $this->_request->getParam('base');  
		$delid = $this->_request->getParam('delid');

		if(isset($delid))
		{
			$bu->kasuj($delid);
			$this->view->error = '<div class="k_ok">Kopia zapasowa została pomyślnie usunięta</div>'; 
			$this->_redirect('/admin/backup/backup/');
			$this->getResponse()->setHeader('Refresh', '2; URL='.$this->view->baseUrl.'/admin/backup/backup/');
		}
		if(isset($file))
		{
			$error = $bu->fileBackup($sciezka->fileDir, $this->baseUrl);
			if(!empty($error)) $this->view->error = '<div class="k_blad">'.$error.'</div>'; 
			else $this->view->error = '<div class="k_ok">Kopia zapasowa plików została pomyślnie utworzona</div>';
			$this->_redirect('/admin/backup/backup/');
			//$this->getResponse()->setHeader('Refresh', '2; URL='.$this->view->baseUrl.'/admin/backup/backup/');
		}		
		if(isset($base))
		{
			$error = $bu->baseBackup($data->db->config, $sciezka->dbaseDir, $this->baseUrl);
			if(!empty($error)) $this->view->error = '<div class="k_blad">'.$error.'</div>'; 
			else $this->view->error = '<div class="k_ok">Kopia zapasowa bazy danych została pomyślnie utworzona</div>';
			$this->_redirect('/admin/backup/backup/');
			//$this->getResponse()->setHeader('Refresh', '2; URL='.$this->view->baseUrl.'/admin/backup/backup/');
		}
	}
}
?>