<?php
class Admin_UstawieniaController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		$this->common->getConfig();
	}
    function __call($method, $args) 
	{
        $this->_redirect('/admin');
    }
	
	public function edytujAction()
	{
		$this->view->r_zapisz = 'zapisz';
		$ustawienia = $this->_request->getPost('ustawienia');
		if($ustawienia)
		{
			$dane = $this->_request->getPost('ustaw');
			if(count($dane) > 0)
			{
				$ceny = '';
				if(false && $this->zakladka_admin == 2)
				{
					foreach($this->ceny as $nazwa => $label)
					$ceny .= $this->_request->getPost($nazwa, '') == 'on' ? $nazwa.';' : '';
					$dane['ceny'] = $ceny;
				}
				$watermark = $this->_request->getPost('watermark');
				$dane['watermark'] = @serialize($watermark);
				$settings = new Ustawienia();
				$settings->updateData($dane);
			}
			
			$danelang = $this->_request->getPost('ustawlang');
			if(count($danelang) > 0)
			{
				$settingslang = new Ustawienialang();
				$settingslang->updateData($danelang);
			}
			unset($_SESSION[$this->common->dbName]['ustawienia']);
			$this->_redirect('admin/ustawienia/edytuj');
		}
		
		$settings = new Ustawienia();
		$ustawienia = $settings->wypisz();
		$this->view->dane = $ustawienia;
		
		$settingslang = new Ustawienialang();
		$ustawienialang = $settingslang->wypisz();
		$this->view->danelang = $ustawienialang;
		
		if(false)
		foreach($this->ceny as $nazwa => $label)
		$this->view->dane[$nazwa] = (strpos($ustawienia['ceny'], $nazwa) !== false);
	}
	
	public function firmaAction()
	{
		$this->view->r_zapisz = 'zapisz';
		
		$kraje = new Allegrokraje();
		$this->view->kraje = $this->kraje = $kraje->getCountries();
		
		$dane_firmy = $this->_request->getPost('dane_firmy');
		if($dane_firmy)
		{
			$dane_firmy = array('dane_firmy' => @serialize($dane_firmy));
			if(true)
			{
				$settings = new Ustawienia();
				$settings->updateData($dane_firmy);
				unset($_SESSION[$this->common->dbName]['ustawienia']);
			}
			$this->_redirect('admin/ustawienia/firma');
		}
		
		$settings = new Ustawienia();
		$ustawienia = $settings->wypisz();
		$this->view->dane_firmy = $dane_firmy = @unserialize($ustawienia['dane_firmy']);
		
		if($this->obConfig->kraje)
		{
			$kraj = @!empty($dane_firmy['kraj']) ? $dane_firmy['kraj'] : 'Polska';
			$kraje = new Allegrokraje();
			$this->view->wojew = $this->wojew = $kraje->getRegions($kraj);
		}
	}
	
	public function reklamaAction()
	{
		$this->view->r_zapisz = 'zapisz';
		
		$this->view->folder = $this->folder = 'admin/reklama';
		$this->view->rozsz = array
		(
			'name' => '*.jpg;*.jpeg;*.png;*.gif',
			'desc' => 'Pliki graficzne (*.jpg;*.png;*.gif)'
		);
		
		$ustawienia = $this->_request->getPost('ustawienia');
		if($ustawienia)
		{
			if(true)
			{
				$settings = new Ustawienia();
				$settings->updateData($ustawienia);
			}
			$this->_redirect('admin/ustawienia/reklama');
		}
		
		$settings = new Ustawienia();
		$ustawienia = $settings->wypisz();
		$this->view->ustawienia = $ustawienia;
	}
	
	public function tlumaczeniaAction()
	{
		$this->view->r_zapisz = 'zapisz';
		
		$podstrony = array('kontakt','o_nas','o_firmie','regulamin','download','sklep_online');
		$oferta = array('Oferta','Nowosci','Promocje','Bestsellery','Wyprzedaz','Specjalne');//,'wyniki_wyszukiwania');
		$klienci = array('logowanie','rejestracja','edycja_konta','koszyk','zamowienie','zamowienia','wyloguj');
		$strony = array('Aktualnosci','Aktualnosc','Artykuly','Artykul','Porady','Porada','Realizacje','Realizacja');
		$linki = array_merge($podstrony, $oferta, $klienci, $strony);
		
		$tlumaczeniaAll = $this->_request->getPost('tlumaczenia');
		//var_dump($tlumaczenia);die();
		if(count($tlumaczeniaAll) > 0)
		{
			$tlumaczenia = new Tlumaczenia();
			$this->tlumaczenia = $tlumaczenia->showData(true);
			if(true || $this->obConfig->podstrony) $podstrony = new Podstrony();
			if(false || $this->obConfig->kategorie) $kategorie = new Kategorie();
			if(false || $this->obConfig->menuOferta) $menu = new Menu();
			if(true) $routers = new Routers();
			foreach($tlumaczeniaAll as $fraza => $dane)
			{
				$tlumaczenia->updateData($dane, $fraza);
				if(true)
				if(@in_array($fraza, $oferta))
				if(count($dane) > 0)
				foreach($dane as $lang => $wartosc)
				{
					$link = $this->common->makeLink($wartosc);
					$valueOrg = @$this->tlumaczenia[$fraza][$lang];
					$linkOrg = $this->common->makeLink($valueOrg);
					if(empty($wartosc)) continue;
					if(empty($valueOrg)) continue;
					if($valueOrg == $wartosc) continue;
					if(true || $this->obConfig->podstrony)
					{
						$data['tytul'] = $wartosc;
						//var_dump($data);die();
						$podstrony->setTytul($data, $valueOrg, $lang);
						unset($data);
					}
				}
				
				if(false)
				if(@in_array($fraza, $linki))
				if(count($dane) > 0)
				foreach($dane as $lang => $wartosc)
				{
					$link = $this->common->makeLink($wartosc);
					$valueOrg = @$this->tlumaczenia[$fraza][$lang];
					$linkOrg = $this->common->makeLink($valueOrg);
					if(empty($wartosc)) continue;
					if(empty($valueOrg)) continue;
					if($valueOrg == $wartosc) continue;
					if(true || $this->obConfig->podstrony)
					{
						$data['temat'] = $wartosc;
						$data['link'] = $link;
						//var_dump($data);die();
						$podstrony->setLink($data, $linkOrg, $lang);
						unset($data);
					}
					if(false || $this->obConfig->kategorie)
					{
						$data['nazwa'] = $wartosc;
						$data['link'] = $link;
						$kategorie->setLink($data, $linkOrg, $lang);
						unset($data);
					}
					if(false || $this->obConfig->menuOferta)
					{
						$data['nazwa'] = $wartosc;
						$data['link'] = $link;
						$menu->setLink($data, $linkOrg, $lang);
						unset($data);
					}
					if(true)
					{
						$data['route'] = $link;
						$routers->setLink($data, $linkOrg, $lang);
						unset($data);
					}
				}
			}
			unset($_SESSION[$this->common->dbName]['tlumaczenia']);
			$this->_redirect('admin/ustawienia/tlumaczenia');
		}
		$tlumaczenia = new Tlumaczenia();
		$this->view->tlumaczenia = $tlumaczenia->showData(false);
	}
}
?>