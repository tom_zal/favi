<?php
class Admin_KoloryController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}
	
	function dodajAction()
	{
		$kolory = new Kolory();
		
		$del = $this->_request->getParam('delid', 0);
		if($del > 0)
		{
			$kolory->id = $del;
			$kolory->usunKolory();
			//$this->view->blad_edycji = '<div class="k_ok">Wybrany kolor został usunięty.</div>';
			$this->_redirect('/admin/kolory/dodaj/');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/kolory/dodaj/');
		}
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa');
			$kolor = $this->_request->getPost('kolor', '000000');
			
			if(isset($_FILES['img']) && !empty($_FILES['img']['tmp_name']))
			{			
				$handle = @new upload($_FILES['img']);
				
				if ($handle->uploaded) 
				{
					$handle->file_new_name_body = 'kolor';
					$handle->image_resize = true;
					$handle->image_x = $this->ImageDir->KoloryX;
					$handle->image_y = $this->ImageDir->KoloryY;
					$handle->image_ratio_y = false;
					$handle->process($this->ImageDir->Kolory);
					
					if ($handle->processed) 
					{
						$handle->clean();
						$img = $handle->file_dst_name;
					}
					else 
					{
						$this->view->blad_edycji = '<div class="k_blad">Upload nieudany. Nie można utworzyć miniaturki.</div>';
					}
				}
				else 
				{
					$this->view->blad_edycji = '<div class="k_blad">Upload nieudany. Nie można przenieść zdjęcia na serwer.</div>';
				}			
			}
			
			$dane = array('nazwa' => $nazwa, 'color' => $kolor);
			if(isset($img)) $dane['img'] = $img;
			$dane['lang'] = $this->lang;
			$kolory->dodajKolory($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/kolory/dodaj/');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/kolory/dodaj/');
		}			
		$this->view->kolory = $kolory->wypiszKolory();
	}
	
	function edytujAction()
	{
		$kolory = new Kolory();
		if($this->_request->isPost())
		{
			$id = $this->_request->getPost('id');
			$nazwa = $this->_request->getPost('nazwa');
			$kolor = $this->_request->getPost('kolor', '000000');
			//var_dump($_FILES['img']);
			if(isset($_FILES['img']) && !empty($_FILES['img']['tmp_name']))
			{			
				$handle = @new upload($_FILES['img']);
				
				if ($handle->uploaded) 
				{
					$handle->file_new_name_body = 'kolor';
					$handle->image_resize = true;
					$handle->image_x = $this->ImageDir->KoloryX;
					$handle->image_y = $this->ImageDir->KoloryY;
					$handle->image_ratio_y = false;
					$handle->process($this->ImageDir->Kolory);
					
					if ($handle->processed) 
					{
						$handle->clean();					
						$img = $handle->file_dst_name;
					}
					else 
					{
						$this->view->blad_edycji = '<div class="k_blad">Upload nieudany. Nie można utworzyć miniaturki.</div>';
					}
				}
				else 
				{
					$this->view->blad_edycji = '<div class="k_blad">Upload nieudany. Nie można przenieść zdjęcia na serwer.</div>';
				}			
			}
			
			$dane = array('nazwa' => $nazwa, 'color' => $kolor);
			if(isset($img)) $dane['img'] = $img;
			$kolory->id = $id;
			$kolory->edytujKolory($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			//$this->_redirect('/admin/kolory/dodaj/');
			//$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/kolory/dodaj/');
		}
		
		$id = $this->_request->getParam('id', 0);		
		$kolory->id = $id;
		$this->view->kolory = $kolory->wypiszKolory();
		$this->view->pojedynczy = $kolory->wypiszKolor();
		$this->view->tytul = $this->view->pojedynczy->nazwa;
	}
}
?>