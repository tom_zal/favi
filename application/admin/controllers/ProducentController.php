<?php
class Admin_ProducentController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		//echo $this->view->baseUrl;
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}	
	function dodajAction()
	{
		$producent = new Producent();
		
		$del = $this->_request->getParam('delid', 0);
		if($del!=0)
		{
			$producent->id = $del;
			$producent->usunProducenta();
			//$this->view->blad_edycji = '<div class="k_ok">Wybrany producent został usunięty.</div>';
			$this->_redirect('/admin/producent/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/producent/dodaj/');
		}
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa');
			$link = $this->_request->getPost('link', '');
			$rabat = $this->_request->getPost('rabat', 0);
			
			if(isset($_FILES['img']) && !empty($_FILES['img']['tmp_name']))
			{
				$handle = @new upload($_FILES['img']);
				
				if($handle->uploaded) 
				{
					$handle->file_new_name_body = 'producent';
					$handle->image_resize = true;
					$handle->image_x = $this->ImageDir->ProducenciX;
					$handle->image_y = $this->ImageDir->ProducenciY;
					$handle->image_ratio_x = true;
					$handle->process($this->ImageDir->Producenci);
					if($handle->processed) 
					{						
						$img = $handle->file_dst_name;
						if($this->ImageDir->ProducenciGreyScale)
						{
							$handle->file_new_name_body = $handle->file_dst_name_body;
							$handle->image_resize = true;
							$handle->image_x = $this->ImageDir->ProducenciX;
							$handle->image_y = $this->ImageDir->ProducenciY;
							$handle->image_ratio_x = true;
							$handle->image_greyscale = true;
							$handle->process($this->ImageDir->Producenci.'/greyscale');
							if($handle->processed) $handle->clean();
						}
						else $handle->clean();
					}
					else 
					{
						$this->view->blad_edycji = '<div class="k_blad">Upload nieudany. Nie można utworzyć miniaturki.</div>';
					}
				}
				else 
				{
					$this->view->blad_edycji = '<div class="k_blad">Upload nieudany. Nie można przenieść zdjęcia na serwer.</div>';
				}			
			}
			
			$dane = array('nazwa' => $nazwa, 'link' => $link, 'rabat' => $rabat);
			if(isset($img)) $dane['logo'] = $img;
			$producent->dodajProducenta($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			if(@empty($this->view->blad_edycji)) $this->_redirect('/admin/producent/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/producent/dodaj/');
		}
			
		$this->view->producenci = $producent->wypiszProducentow();
	}	
	function edytujAction()
	{
		$producent = new Producent();
		$id = $this->_request->getParam('id', 0);
		
		$this->view->r_zapisz = 'jQuery_zapisz_btn';
		$this->view->r_dodaj = 'admin/producent/dodaj/';
		$this->view->r_usun = 'admin/producent/dodaj/delid/'.$id;
		$this->view->r_lista = 'admin/producent/dodaj/';
		
		if($this->_request->isPost())
		{
			$id = $this->_request->getPost('id');
			$nazwa = $this->_request->getPost('nazwa');
			$link = $this->_request->getPost('link', '');
			$rabat = $this->_request->getPost('rabat', 0);
			if($this->_request->getPost('usun_logo')) $img = "";
			//var_dump($_FILES['img']);
			if(isset($_FILES['img']) && !empty($_FILES['img']['tmp_name']))
			{	
				$handle = @new upload($_FILES['img']);
				
				if($handle->uploaded) 
				{
					$handle->file_new_name_body = 'producent';
					$handle->image_resize = true;
					$handle->image_x = $this->ImageDir->ProducenciX;
					$handle->image_y = $this->ImageDir->ProducenciY;
					$handle->image_ratio_x = true;
					$handle->process($this->ImageDir->Producenci);
					if($handle->processed) 
					{		
						$img = $handle->file_dst_name;
						if($this->ImageDir->ProducenciGreyScale)
						{
							$handle->file_new_name_body = $handle->file_dst_name_body;
							$handle->image_resize = true;
							$handle->image_x = $this->ImageDir->ProducenciX;
							$handle->image_y = $this->ImageDir->ProducenciY;
							$handle->image_ratio_x = true;
							$handle->image_greyscale = true;
							$handle->process($this->ImageDir->Producenci.'/greyscale');
							if($handle->processed) $handle->clean();
						}
						else $handle->clean();
					}
					else 
					{
						$this->view->blad_edycji = '<div class="k_blad">Upload nieudany. Nie można utworzyć miniaturki.</div>';
					}
				}
				else 
				{
					$this->view->blad_edycji = '<div class="k_blad">Upload nieudany. Nie można przenieść zdjęcia na serwer.</div>';
				}			
			}
			
			$dane = array('nazwa' => $nazwa, 'link' => $link, 'rabat' => $rabat);
			if(isset($img)) $dane['logo'] = $img;
			$producent->id = $id;
			$producent->edytujProducenta($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			if(@empty($this->view->blad_edycji)) $this->_redirect('/admin/producent/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/producent/dodaj/');
		}		
		
		$producent->id = $id;
		$this->view->producenci = $producent->wypiszProducentow();
		$this->view->pojedynczy = $producent->wypiszPojedynczego();
		$this->view->tytul = $this->view->pojedynczy->nazwa;
	}
}
?>