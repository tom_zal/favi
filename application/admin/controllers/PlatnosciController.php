<?php
class Admin_PlatnosciController extends Ogolny_Controller_Admin
{
    public function init()
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
    }
    function __call($method, $args)
	{
        $this->_redirect('/admin/index/panel/');
    }
    function dodajAction()
	{
        $platnosci = new Platnosci();
        $delid = $this->_request->getParam('delid',0);
        if($delid > 0)
        {
            $platnosci->deleteWybrany($delid);
			//$this->view->blad_edycji = '<div class="k_ok">Wybrany koszt dostawy został usunięty.</div>';
			$this->_redirect('/admin/platnosci/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/platnosci/dodaj/');
        }
        if($this->_request->isPost())
		{
            $platnosc = $this->_request->getPost('platnosc');
            $dostawa = $this->_request->getPost('dostawa');
            $wartosc = $this->_request->getPost('wartosc');
            $dane = array('platnosc' => $platnosc, 'dostawa' => $dostawa, 'wartosc' => $wartosc);
            $platnosci->addWartosc($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/platnosci/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/platnosci/dodaj/');
        }
        $this->view->platnosci = $platnosci->wypiszWszystkie();
        $this->view->dostawy = $platnosci->selectdostawyAll();
        $this->view->typyplatnosci = $platnosci->selectplatnosciAll();
    }
	
	function dostawyplatnosciAction()
	{
        $platnosci = new Platnosci();
		$this->view->platnosciAll = $platnosci->selectplatnosciAll();
		//var_dump($this->view->platnosciAll);
		
		$wagi = new Wagi();
		$this->view->wagi = $wagi->wypisz();
		
		$kategorie = new Kategorie();
        $this->view->glowne = $kategorie->start();
		
        $delid = $this->_request->getParam('delidDostawa', 0);
        if($delid > 0)
        {
            $platnosci->deleteDostawy($delid);
			//$this->view->blad_edycji = '<div class="k_ok">Wybrana dostawa została usunięta.</div>';
			$this->_redirect('/admin/platnosci/dostawyplatnosci');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/platnosci/dostawyplatnosci');
        }
		$editidDostawa = $this->_request->getParam('editidDostawa',0);
        if($editidDostawa > 0)
        {
			$this->view->editDostawa = $platnosci->selectdostawy($editidDostawa);
			$this->view->platnoscDlaDostawy = $platnosci->WypiszPlatnosciDlaDostawy($editidDostawa);
			//var_dump($this->view->platnoscDlaDostawy);			
			if($this->view->platnoscDlaDostawy != null)
			foreach($this->view->platnosciAll as $platnosc)
			foreach($this->view->platnoscDlaDostawy as $dostawa)
			if($platnosc['id'] == $dostawa['id']) 
			{ $platnoscDlaExist[$platnosc['id']] = true; break; }
			//var_dump($platnoscDlaExist);
			$this->view->platnoscDlaExist = isset($platnoscDlaExist) ? $platnoscDlaExist : null;
			$this->view->platnosciAll = $platnosci->selectplatnosciAllForDostawa($editidDostawa);
			//var_dump($this->view->platnosciAll);
			$wagi = new Wagi();
			$this->view->wagi = $wagi->wypiszWagiDlaDostawy($editidDostawa);
			//var_dump($this->view->wagi);
        }
		if($this->_request->isPost() && $this->_request->getPost('edytujDostawa'))
        {
			$id = $this->_request->getPost('id');
			$nazwa = $this->_request->getPost('nazwa');
			$wartosc = $this->_request->getPost('wartosc', 0);
			$opis = $this->_request->getPost('opis', '');
			$apikurier = $this->_request->getPost('apikurier', '');
            $dane = array('nazwa' => $nazwa, 'wartosc' => $wartosc, 'apikurier' => $apikurier, 'opis' => addslashes($opis));
			$platnosci->id = $id;
            $platnosci->changeSposobdostawy($dane);
			
			$platnosciDla = $this->_request->getPost('platnosc');
			//var_dump($platnosciDla);
			$this->view->platnoscDlaDostawy = $platnosci->WypiszPlatnosciDlaDostawy($id);
			
			$platnosciprodukty = new PlatnosciProdukty();
			foreach($this->view->platnosciAll as $platnosc)
			{
				if($this->view->platnoscDlaDostawy != null)
				foreach($this->view->platnoscDlaDostawy as $platnoscID => $platnoscDla)
				{
					$exist = false;
					if($platnosc['id'] == $platnoscDla['id'])
					{ 
						$exist[$platnosc['id']] = $platnosc['id'];
						if($this->_request->getPost('dodajPlatnosciDlaProduktow'))
						{
							$platnosciprodukty->deleteWybrany($platnoscID);
							$platnosciprodukty->dodajPlatnoscProduktyAll($platnoscID);
						}
						break;
					}
				}	
				
				if(@!empty($platnosciDla[$platnosc['id']]['on']) && !isset($exist[$platnosc['id']]))
				{
					$dane = array('platnosc' => $platnosc['id'], 'dostawa' => $id,
						'koszt' => @floatval($platnosciDla[$platnosc['id']]['wartosc']));
					$platnoscID = $platnosci->addWartosc($dane);
					if($this->_request->getPost('dodajPlatnosciDlaProduktow'))
					$platnosciprodukty->dodajPlatnoscProduktyAll($platnoscID);
				}
				else if(isset($exist[$platnosc['id']]) && isset($platnosciDla[$platnosc['id']]['on']))
				{
					$platnosci->id = $platnosc['nr'];
					$dane = array('koszt' => @floatval($platnosciDla[$platnosc['id']]['wartosc']));
					$platnosci->changeWartosc($dane);
				}
				else if(isset($exist[$platnosc['id']]) && !isset($platnosciDla[$platnosc['id']]['on']))
				{
					$platnosci->deleteWybranyPlatnoscDostawa($platnosc['id'], $id);
				}
			}
			
			$platnoscDlaKat = intval($this->_request->getPost('platnoscDlaKat', 0));
			if($platnoscDlaKat > 0)
			{
				$kateg = 0;
				$kategs = $this->_request->getPost('kategs');
				if(count($kategs) > 0)
				for($i = count($kategs) - 1; $i >= 0; $i--)
				{
					if(intval($kategs[$i]) > 0)
					{
						$kateg = intval($kategs[$i]);
						break;
					}
				}
				if($kateg > 0)
				{
					$dlaKat = $this->_request->getPost('dlaKat');
					if($dlaKat == 'dodaj' || $dlaKat == 'usun')
					$platnosciprodukty->usunKategoriaPlatnosc($platnoscDlaKat, $kateg);
					if($dlaKat == 'dodaj')
					$platnosciprodukty->dodajKategoriaPlatnosc($platnoscDlaKat, $kateg);
				}
			}

			$WagiKosztDostawy = new WagiKosztDostawy();
			$wagi = $this->_request->getPost('wagi');
			if(count($wagi) > 0)
			foreach($wagi as $id_wagi => $koszt)
			{
				$waga = $WagiKosztDostawy->wypiszWagaDostawa($id, $id_wagi);
				if(count($waga) > 0)
				{
					$WagiKosztDostawy->edytujWagaDostawa($id, $id_wagi, array('koszt' => $koszt));
				}
				else
				{
					$dane = array('id_dostawa' => $id, 'id_waga' => $id_wagi, 'koszt' => $koszt);
					$WagiKosztDostawy->dodaj($dane);
				}
			}
			
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/platnosci/dostawyplatnosci');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/platnosci/dostawyplatnosci/');
        }
        if($this->_request->isPost() && $this->_request->getPost('dodajDostawa')) 
		{
            $nazwa = $this->_request->getPost('nazwa');
			$wartosc = floatval($this->_request->getPost('wartosc', 0));
			$apikurier = $this->_request->getPost('apikurier', '');
			$opis = $this->_request->getPost('opis', '');
            $dane = array('nazwa' => $nazwa, 'wartosc' => $wartosc, 'apikurier' => $apikurier, 'opis' => addslashes($opis));
            $id = $platnosci->addSposobdostawy($dane);
			
			if($this->obConfig->dostawyOdWagi)
			{
				$wagiDostawy = new WagiDostawy();
				if($this->obConfig->kosztPrzesylkiOdWagiDostawy)
				{
					$wagi = $this->_request->getPost('wagi');
					if(count($wagi) > 0)
					foreach($wagi as $id_wagi => $koszt)
					if(!empty($koszt))
					$wagiDostawy->dodaj(array('id_waga' => $id_wagi, 'id_dostawa' => $id));
				}
				else
				{
					if(count($this->view->wagi) > 0)
					foreach($this->view->wagi as $waga)
					$wagiDostawy->dodaj(array('id_waga' => $waga['id'], 'id_dostawa' => $id));
				}
				//var_dump($wagi);
			}
			
			if($this->obConfig->kosztPrzesylkiOdWagi)
			{
				$wagiKosztDostawy = new WagiKosztDostawy();
				$wagi = $this->_request->getPost('wagi');
				if(count($wagi) > 0)
				foreach($wagi as $id_wagi => $koszt)
				{
					$dane = array('id_dostawa' => $id, 'id_waga' => $id_wagi, 'koszt' => $koszt);
					$wagiKosztDostawy->dodaj($dane);
				}
			}
			
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/platnosci/dostawyplatnosci');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/platnosci/dostawyplatnosci');
        }
        $this->view->dostawy = $platnosci->selectdostawyAll();
		foreach($this->view->dostawy as $dostawa)
		$platnosciDlaDostawy[$dostawa['id']] = $platnosci->WypiszPlatnosciDlaDostawy($dostawa['id']);
		$this->view->platnosciDlaDostawy = @$platnosciDlaDostawy;
		//var_dump($this->view->platnosciDlaDostawy);

        $platnosci = new Platnosci();
        $delid = $this->_request->getParam('delidPlatnosc',0);
        if($delid>0)
        {
            $platnosci->deletePlatnosc($delid);
			//$this->view->blad_edycji = '<div class="k_ok">Wybrana płatność została usunięta.</div>';
			$this->_redirect('/admin/platnosci/dostawyplatnosci');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/platnosci/dostawyplatnosci/');
        }
        if($this->_request->isPost() && $this->_request->getPost('dodajPlatnosc')) 
		{
            $nazwa = $this->_request->getPost('nazwa', '');
            $rabat = floatval($this->_request->getPost('rabat', 0));
			$rodzaj = $this->_request->getPost('rodzaj', '');
            $dane = array('nazwa' => $nazwa, 'rabat' => $rabat, 'rodzaj' => $rodzaj);
            $platnosci->addSposobplatnosci($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/platnosci/dostawyplatnosci');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/platnosci/dostawyplatnosci/');
        }
		$editidPlatnosc = $this->_request->getParam('editidPlatnosc',0);
        if($editidPlatnosc>0)
        {
			$this->view->editPlatnosc = $platnosci->selectplatnosci($editidPlatnosc);
        }
		if($this->_request->isPost() && $this->_request->getPost('edytujPlatnosc')) 
		{
			$id = $this->_request->getPost('id');
            $nazwa = $this->_request->getPost('nazwa');
            $rabat = floatval($this->_request->getPost('rabat', 0));
			$rodzaj = $this->_request->getPost('rodzaj', '');
            $dane = array('nazwa' => $nazwa, 'rabat' => $rabat, 'rodzaj' => $rodzaj);
			$platnosci->id = $id;
            $platnosci->changeSposobplatnosci($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/platnosci/dostawyplatnosci');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/platnosci/dostawyplatnosci/');
        }
		$this->view->platnosci = $platnosci->selectplatnosciAll();
    }
	function vatAction() 
	{
        $vat = new Vat();
        $delid = $this->_request->getParam('delid',0);
        if($delid>0)
        {
            $vat->usunVat($delid);
			//$this->view->blad_edycji = '<div class="k_ok">Wybrana stawka VAT została usunięta.</div>';
			$this->_redirect('/admin/platnosci/vat');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/platnosci/vat/');
        }
		$editid = $this->_request->getParam('editid',0);
        if($editid>0)
        {
			$vat->id = $editid;
			$this->view->edit = $vat->wypiszVat();
        }
        if($this->_request->isPost()) 
		{
            $nazwa = $this->_request->getPost('nazwa');
			//$stawka = $this->_request->getPost('stawka');
			if($nazwa == '')
			{
				$this->view->blad_edycji = '<div class="k_ok">Nie wypełniono wszystkich wymaganych pól.</div>';
				$this->_redirect('/admin/platnosci/vat');
				$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/platnosci/vat/');
			}
			if(false && (!is_numeric($stawka) || $stawka < 0))
			{
				$this->view->blad_edycji = '<div class="k_ok">Stawka musi być liczbą >= 0.</div>';
				$this->_redirect('/admin/platnosci/vat');
				$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/platnosci/vat/');
			}
			if($this->_request->getPost('dodaj'))
			{
				$dane = array('nazwa' => $nazwa);
				$vat->dodajVat($dane);
				//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/admin/platnosci/vat');
				$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/platnosci/vat/');
			}
			if($this->_request->getPost('edytuj'))
			{
				$id = $this->_request->getPost('id');
				$dane = array('nazwa' => $nazwa);
				$vat->id = $id;
				$vat->edytujVat($dane);
				//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/admin/platnosci/vat');
				$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/platnosci/vat/');
			}
        }
        $this->view->vaty = $vat->wypiszVaty();
    }
}
?>