<?phpclass Admin_CVController extends Ogolny_Controller_Admin{    public function init() 	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
    }
    function __call($method, $args) 	{
        $this->_redirect('/admin/index/panel/');
    }
    function edytujAction() 	{
		$id = $this->_request->getParam('id');		$cv = new CV();
		if ($this->_request->isPost()) 		{
			$dane = $this->_request->getPost('cv');				
			$cv = new CV();
			$cv->id = $id;
			$cv->edytuj($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
		}		
		$cv->id = $id;
		$new_user = $cv->wypiszPojedynczy();
		$this->view->new_user = $new_user;    }		function komentarzAction() 	{		$id = 1;		$cv = new CV();		if ($this->_request->isPost()) 		{			$dane = $this->_request->getPost('cv');					$dane['data_ur_opis'] = $dane['data_ur'];						$cv = new CV();			$cv->id = $id;			$cv->edytuj($dane);			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';		}				$cv->id = $id;		$new_user = $cv->wypiszPojedynczy();		$new_user['data_ur'] = $new_user['data_ur_opis'];		$this->view->new_user = $new_user;    }		function odpowiedzAction() 	{		$cv = new CV();		$id = $this->_request->getParam('odpid', 0);		if($id > 0)		{			$cv->id = $id;			$user = $cv->wypiszPojedynczy();			$maile[] = $user['email'];		}		if($this->_request->getPost('usuwaj'))		{			$dane = $this->_request->getPost('usuwaj');				foreach($dane as $id)			{				$cv->id = $id;				$user = $cv->wypiszPojedynczy();				$maile[] = $user['email'];			}		}		if($this->_request->getPost('submit'))		{			$tekst = $this->_request->getPost('tekst');			$maile = $this->_request->getPost('maile');			$email = $this->_request->getPost('email');			$nazwa = $this->_request->getPost('nazwa');			if(empty($tekst) || empty($maile) || empty($email) || empty($nazwa))			{				$this->view->blad_edycji = 'Wypełnij wszystkie pola!';			}			else			{				foreach($maile as $mail)				{									$wiadomosci = new Wiadomosci();					$wiadomosci->cvOdpZapytanie($tekst, $mail, $email, $nazwa);				}				$this->view->lista_bledow = 'E-mail został wysłany!';			}		}		$conf = new Confmail();		$data = $conf->showData();		$this->view->email = $data[0]['From'];				$this->view->maile = isset($maile) ? $maile : null;    }
    function listaAction() 	{        $cv = new CV();		$delid = (int)$this->_request->getParam('delid', 0);
        if($delid > 0)		{
            $cv->id = $delid;
            $cv->usun();
        }		$uluid = (int)$this->_request->getParam('uluid', 0);        if($uluid > 0)		{            $cv->id = $uluid;			$user = $cv->wypiszPojedynczy();            $cv->edytuj(array('ulubione' => !$user['ulubione']));        }		$usunZazn = $this->_request->getPost('usuwaj');		if(count($usunZazn) > 0)		foreach($usunZazn as $delid => $cvid)		{			$cv->id = $delid;            $cv->usun();		}				$szukanie = new Zend_Session_Namespace('szukanieCVAdmin');		if ($this->_request->getParam('reset')) 		{			$szukanie->nazwa = '';						$szukanie->woj = 'all';			$szukanie->miasto = 'all';			$szukanie->plec = 'all';			$szukanie->ulubione = '';        }        if ($this->_request->isPost())		{			$szukanie->nazwa = $this->_request->getPost('nazwa');			$szukanie->woj = $this->_request->getPost('woj');			$szukanie->miasto = $this->_request->getPost('miasto');			$szukanie->plec = $this->_request->getPost('plec');			$szukanie->ulubione = $this->_request->getPost('ulubione');        }		if (!isset($szukanie->nazwa)) $szukanie->nazwa = '';		else if($szukanie->nazwa == 'Imię, Nazwisko') $szukanie->nazwa = '';		if (!isset($szukanie->woj)) $szukanie->woj = 'all';		if (!isset($szukanie->miasto)) $szukanie->miasto = 'all';		if (!isset($szukanie->plec)) $szukanie->plec = 'all';		if (!isset($szukanie->ulubione)) $szukanie->ulubione = '';		$sortowanie = new Zend_Session_Namespace('sortowanieCVAdmin');		if($this->_request->getParam('sort'))		{			$sortowanie->sort = $this->_request->getParam('sort', 'data_wysylki');			$sortowanie->order = $this->_request->getParam('order', 'desc');		}		if(!isset($sortowanie->sort))		{			$sortowanie->sort = 'data_wysylki';			$sortowanie->order = 'desc';		}					$this->view->miasta = $cv->wypiszMiasta();				$this->view->szukanie = $szukanie;		$this->view->sortowanie = $sortowanie;			
        $this->view->cv = $cv->wypisz($szukanie, $sortowanie);
    }
}
?>