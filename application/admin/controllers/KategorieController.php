<?php 
class Admin_KategorieController extends Ogolny_Controller_Admin
{
	private $base;
	
	public function init()
	{
        parent::init();
		
        $this->view->baseUrl = $this->_request->getBaseUrl();
		$this->base = $this->_request->getBaseUrl();
		
		//$this->view->headScript()->appendFile($this->_request->getBaseUrl().'/public/scripts/jquery.ui/js/jquery-ui-1.8.13.custom.min.js','text/javascript');
		
		$this->konto = new Allegrokonta();
		$this->konto->ID = 1;
		$this->config = $this->konto->klient();
		$this->view->config = $this->config;
		
		$this->view->typ = $this->typ = $this->_request->getParam('typ', 'kategorie');
    }
    function __call($method, $args)
	{
        $this->_redirect('/admin/index/panel/');
    }
	
    public function listaAction()
	{ 
		$this->view->r_dodaj = 'admin/kategorie/dodaj/typ/'.$this->typ;
		
        $lista = new Kategorie();
		$lista->allegro = $this->obConfig->allegro;
		$lista->eBay = $this->obConfig->eBay;
        $status = (int)$this->_request->getParam('status', -1);
        if($status == 1)
		{
            //$this->view->error = '<div class="k_ok">Edycja przebiegła pomyślnie</div>';
        }
        else
		{
            $this->view->error = '';
        }
        // usuwa kategorie
        if(($del = intval($this->_request->getParam('del'))) > 0)
		{
            $lista->kasuj($del);
            //$this->view->error = '<div class="k_ok">Wybrana kategoria została usunięta.</div>';
			$this->_redirect('/admin/kategorie/lista/typ/'.$this->typ);
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->base.'/admin/kategorie/lista/');
        }
        // zmienia pozycje kategrii
        if($this->_request->getPost('pozycja'))
		{
            $lista->changePozycja($this->_request->getPost('id'), $this->_request->getPost('pozycja'));
			//$this->view->error = '<div class="k_ok">Pozycja wybranej kategorii została zmieniona.</div>';
			$this->_redirect('/admin/kategorie/lista/typ/'.$this->typ);
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->base.'/admin/kategorie/lista/');
        }
		if(($pozid = intval($this->_request->getParam('pozid'))) > 0)
		{
			$pozycja = intval($this->_request->getParam('pozycja', 0));
            $lista->edytuj($pozid, array('pozycja' => $pozycja));
			$this->_redirect('admin/kategorie/lista/typ/'.$this->typ);
        }
		if(($pokazid = intval($this->_request->getParam('pokazid'))) > 0)
		{
			$widoczny = @intval($this->_request->getParam('widoczny')?1:0);
            $lista->edytuj($pokazid, array('widoczny' => $widoczny));
			$this->_redirect('admin/kategorie/lista/typ/'.$this->typ);
        }

        // przekazujemy sciezke bazową dla linków i obrazków
        $lista->link = $this->_request->getBaseUrl();
		$lista->edit = $this->edycjazablokowana;
		$lista->typ = $this->typ;
		$lista->getAllWithDetails();
        $lista->display_children_admin(0, 0);
        $this->view->kategorie = $lista->menu;
    }

    public function dodajAction()
	{
        $nowa = new Kategorie();
        $id = $this->_request->getParam('id', 0);
		
		if($id > 0)
		{
            $this->view->path = $path = $nowa->get_path($id);
			$kategoria = $nowa->showWybranaKategoria($id);
			$this->view->typ = $this->typ = $kategoria['typ'];
        }
		
		$this->view->r_zapisz = 'zapisz';
		$this->view->r_lista = 'admin/kategorie/lista/typ/'.$this->typ;

        if($this->_request->isPost())
		{
            $stringValidate = new Zend_Validate;
            $stringValidate
				->addValidator(new Zend_Validate_StringLength(1,null))
				->addValidator(new Zend_Validate_NotEmpty());

            if($stringValidate->isValid($this->_request->getPost('nazwa')))
			{
                $nazwa =  $this->_request->getPost('nazwa');
                $last_id = $nowa->dodaj_nowa($id, $nazwa, $this->typ);

                $route = new Routers();
                if(false && count($path) > 0)
                {
                    $nazwa = $path[count($path)-1]['link'].'-'.$nazwa;
                }
				else
				{
                    $nazwa = $nazwa;
                }

                $sprawdz = $route->dodaj($nazwa, $last_id, 'default', 'oferta', 'kategorie');
				if($sprawdz['id'] == 0)
				$sprawdz = $route->dodaj($nazwa.'-'.$last_id, $last_id, 'default', 'oferta', 'kategorie');
                if($sprawdz['id'] > 0)
				{
                    $data['link'] = $sprawdz['link'];
                    $data['route_id'] = $sprawdz['id'];
                    $nowa->edytuj($last_id, $data);
                    //print_r($data);
                    $this->_redirect('/admin/kategorie/lista/typ/'.$this->typ.'/status/1');
                }
				else
				{
                    $data['link'] = $sprawdz['link'];
                    $data['route_id'] = $sprawdz['id'];
                    $nowa->edytuj($last_id, $data);
                    $this->_redirect('/admin/kategorie/edytuj/id/'.$last_id.'/status/0');
                }
            }
			else
			{
                $this->view->error = '<div class="k_blad">Pole nazwa jest wymagane </div>';
            }
        }
    }

    public function edytujAction()
	{
		$zakladka_produkty = $this->_request->getParam('param', 0);				
		if(!empty($zakladka_produkty) || $zakladka_produkty != 0):
			switch($zakladka_produkty):
				case 'nazwa': $tab = 0; break;
				case 'link': $tab = 1; break;				
				case 'ebay': $tab = 2; break;
				case 'allegro': $tab = 3; break;
				case 'opis': $tab = 4; break;				
				case 'zdj': $tab = 5; break;
				case 'banery': $tab = 5; break;
				case 'wiz': $tab = 6; break;
				case 'sprz': $tab = 7; break;
				case 'galeria': $tab = 8; break;
				case 'allegroparams': $tab = 9; break;
				case 'video': $tab = 10; break;
				case 'atrybuty': $tab = 11; break;
				default:
				$tab = 0;
			endswitch;
		else:
			$tab = 0;
		endif;

		$this->view->wyswietlAll = $wyswietlAll = true;
		$this->view->zak_kat =  $tab;
		$this->view->zakladka =  $zakladka_produkty;
		
		$nowa = new Kategorie();
        $status = (int)$this->_request->getParam('status', -1);
        if($status == 1)
		{
            //$this->view->error = '<div class="k_ok">Edycja przebiegła pomyślnie</div>';
        }
        elseif($status == 0)
		{
            $this->view->error = '<div class="k_blad">Link już występuje w bazie</div>';
        }
        else
		{
            $this->view->error = '';
        }
		
		$id = intval($this->_request->getParam('id', 0));
		
        if($id > 0)
		{
            $path = $nowa->get_path($id);
			$kategoria = $nowa->showWybranaKategoria($id);
            $this->view->path = $path;
            //print_r($path);
			$this->view->typ = $this->typ = $kategoria['typ'];
			
			$idDel = (int) $this->_request->getParam('idDel', 0);
            $idEdit = (int) $this->_request->getParam('idEdit', 1);

            $img = new Kategorie();

            if($idDel > 0)
			{
                $img->usunZdjecie($idDel, $id);
				//$this->view->error = '<div class="k_ok">Zdjęcie zostało usunięte.</div>';
				$this->_redirect('/admin/kategorie/edytuj/id/'.$id.'/param/'.$zakladka_produkty.'/');
            	$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/kategorie/edytuj/id/'.$id.'/param/'.$zakladka_produkty.'/');
            }

            if($idEdit > 0)
			{
                $this->view->edycjaZdjecia = $idEdit;				
                //$this->view->nazwa_zdjecia = $img->pobierzNazwe($idEdit);
            }

            if($this->_request->isPost())
			{
				if($this->obConfig->opisKategorii)
				if($wyswietlAll || $this->_request->getPost('baner'))
				{
					$short = $this->_request->getPost('skrot', '');
					$opis = $this->_request->getPost('opis', '');
					$dane = array('short' => $short, 'opis' => $opis);
					$nowa->edytuj($id, $dane);
				}
				if($this->obConfig->wizytowkaKategorie)
				if($wyswietlAll || $this->_request->getPost('wizytowka'))
				{
					$opis = $this->_request->getPost('tekst', '');
					$dane = array('opis' => $opis);
					$nowa->edytuj($id, $dane);
				}
				if($this->obConfig->wizytowkaSprzedawcy)
				if($wyswietlAll || $this->_request->getPost('sprzedawcy'))
				{
					$sprzedawca = $this->_request->getPost('sprzedawca');
					if($sprzedawca == 'domyslny')
					{
						$wizytowka = new Wizytowka();
						$domyslny = $wizytowka->getDomyslny();
						$sprzedawca = $domyslny['id'];
					}
					$dane = array('sprzedawca' => $sprzedawca);
					$nowa->edytuj($id, $dane);
				}
				if($this->obConfig->allegro)
				if($wyswietlAll || $this->_request->getPost('paramsAllegro'))
				{
					$allegroparams = new Allegroparams();
					$params = $this->_request->getPost('params');
					//var_dump($params);
					if(count($params) > 0)
					foreach($params as $allegroKat => $paramsAllegro)
					if(count($paramsAllegro) > 0)
					foreach($paramsAllegro as $sellformID => $sellform)
					{
						$paramVals = null;
						if(count($sellform) > 0)
						{
							if(isset($sellform[0]))
							{
								if(count($sellform) > 1)
								{
									$val = 0;
									foreach($sellform as $p => $par)
									$val += floatval($par);
									//var_dump($val);
								}
								else $val = $sellform[0];
								$paramVals = array(0 => $val);
							}
							else $paramVals = $sellform;
						}
						
						if(count($paramVals) > 0)
						foreach($paramVals as $paramID => $val)
						{					
							$exist = $allegroparams->getOne($allegroKat, $sellformID, $paramID);
							if(count($exist) > 0)
							{
								$allegroparams->id = $exist['id'];
								$allegroparams->edytuj(array('value' => $val));
							}
							else
							{
								$paramNew['id_kat'] = $allegroKat;
								$paramNew['id_sell_form'] = $sellformID;
								$paramNew['id_param'] = $paramID;
								$paramNew['value'] = $val;
								$allegroparams->dodaj($paramNew);
							}
						}
					}
					//$this->_redirect($this->URI);
				}
			
                if(false && ($wyswietlAll || $this->_request->getPost('zmien')))
				{
                    $nowy_link = $this->_request->getPost('link');
                    $idlink = $this->_request->getPost('id_link');
                    $route = new Routers();
                    
					if(!empty($nowy_link))
					{
						if($idlink == 0)
						{
	                        $sprawdz_link = $route->dodaj($nowy_link, $id, 'default', 'oferta', 'kategorie');
	                    }
						else if($idlink > 0)
						{
	                        $sprawdz_link = $route->edytuj($nowy_link, $idlink);
	                    }
	                    if(@intval($sprawdz_link['id']) == 0)
						{
	                    	$this->view->error = '<div class="k_blad">Błąd edycji danych.</div>';
	                        $this->view->error .= '<div id="lista_bledow">Przyjazny link występuje już w bazie. Należy zmienić link.</div>';
	                    }
						else
						{
	                        $link_produkty = new Kategorie();
	                        $dane_link['link'] = $sprawdz_link['link'];
	                        $dane_link['route_id'] = $sprawdz_link['id'];
	
	                        $link_produkty->edytuj($id,$dane_link);	
	                        //$this->view->error ='<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
	                    }
					}
					else
					{
						$this->view->error = '<div class="k_blad">Błąd edycji danych.</div>';
	                    $this->view->error .= '<div id="lista_bledow">Pole nazwa url jest wymagane.</div>';
					} 
                }
				
				if($this->_request->getPost('nazwa'))
				{
                    $stringValidate = new Zend_Validate;
                    $stringValidate->addValidator(new Zend_Validate_StringLength(1, null))->addValidator(new Zend_Validate_NotEmpty());

                    if($stringValidate->isValid($this->_request->getPost('nazwa'))) 
					{
                        $data['nazwa'] = $this->_request->getPost('nazwa');
						$link = $this->_request->getPost('link', $data['nazwa']);
						if(@!empty($link))
						{
							//$link = $data['nazwa'];
							$route = new Routers();
							$spr = $route->sprawdzCzyIstnieje($link);
							if(!$spr) $link .= '-'.$id;
							if(@intval($kategoria['route_id']) > 0)
							$ret = $route->edytuj($link, $kategoria['route_id']);
							if(@$ret['id'] > 0) $data['link'] = $ret['link'];
						}
						$data['widoczny'] = $this->_request->getPost('widoczny', 0);
						$data['na_glownej'] = $this->_request->getPost('na_glownej', 0);
						$data['modele'] = $this->_request->getPost('modele', 0);
						$data['data'] = $this->_request->getPost('data', date('Y-m-d H:i:s'));
						$data['grupa'] = intval($this->_request->getPost('grupa', 0));
						$data['rabat'] = floatval($this->_request->getPost('rabat', 0));
						$data['marza'] = floatval($this->_request->getPost('marza', 0));
						$data['szablon'] = intval($this->_request->getPost('szablon', 0));
						if($this->obConfig->kategorieMarzaDetal)
						{
							$produkty = new Produkty();
							$produkty->cenaDetalFromHurtMarzaForKateg($id, $data['marza']);
							$rozmiary = new Rozmiarproduktu();
							$rozmiary->cenaDetalFromHurtMarzaForKateg($id, $data['marza']);
						}
                        $nowa->edytuj($id, $data);
                        //$this->_redirect('/admin/kategorie/lista/status/ok');
                    }
					else
					{
                        $this->view->error = '<div class="k_blad">Pole nazwa jest wymagane </div>';
                    }
                }
				
				/***ZMIEN NAZWE ZDJECIA***/
                $images = $this->_request->getPost('imgs');
				if(count($images) > 0)
				if($wyswietlAll || isset($images['zmienNazweZdjecia']))
				{
					if(!isset($images['nazwa'])) $images['nazwa'] = '';
					
					if(isset($_FILES['img']) && empty($_FILES['img']['tmp_name']))
					{
						//$this->view->error = '<div class="k_blad">Pole plik jest wymagane.</div>';
					}
					else 
					{
						$handle = @new upload($_FILES['img']);
						if($handle->uploaded) 
						{
							$body = $zakladka_produkty == 'zdj' || empty($zakladka_produkty) ? 'Kategorie' : ucfirst($zakladka_produkty);
							$handle->file_new_name_body = strtolower($body);
							$handle->image_resize = false;
							$handle->image_x = $this->ImageDir->KategorieX;
							$handle->image_y = $this->ImageDir->KategorieY;
							$handle->image_ratio_y = true;
							$handle->process($this->ImageDir->$body);

							if($handle->processed) 
							{
								$handle->clean();
								$img = new Kategorie();
								$img->zapiszDoBazy($images['id'], $handle->file_dst_name, $id, $images['nazwa']);
								//$this->view->error = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';	
							}
							else 
							{
								$this->view->error = '<div class="k_blad">Upload nieudany. <br/>.'.@$handle->error.'</div>';								
							}						
						}
					}
						
					//$img = new Kategorie();
					//$img->zmienNazwe($images['nazwa'], $id, $images['id']);

					unset($images['id']);
					$this->view->edycjaZdjecia = 0;					
					//$this->view->error = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
					if(false && @empty($this->view->error))
					$this->_redirect('/admin/kategorie/edytuj/id/'.$id.'/param/'.$zakladka_produkty.'/');
					//$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl. '/admin/kategorie/edytuj/id/'.$images['id'].'/param/'.$zakladka_produkty.'/');
				}
				
				//var_dump($_POST);die();
				if($this->obConfig->allegro)
				if($wyswietlAll || $this->_request->getPost('allegro'))
				{
					$path = $this->_request->getPost('kat0',-1).';'.$this->_request->getPost('kat1',-1).';'.$this->_request->getPost('kat2',-1).';'.$this->_request->getPost('kat3',-1).';'.$this->_request->getPost('kat4',-1).';'.$this->_request->getPost('kat5',-1);
					$dane = array('allegro' => $path);
					$nowa->edytuj($id, $dane);
				}
				if($this->obConfig->eBay)
				if($wyswietlAll || $this->_request->getPost('ebay'))
				{
					$path = $this->_request->getPost('kat0',-1).';'.$this->_request->getPost('kat1',-1).';'.$this->_request->getPost('kat2',-1).';'.$this->_request->getPost('kat3',-1).';'.$this->_request->getPost('kat4',-1).';'.$this->_request->getPost('kat5',-1);
					$dane = array('ebay' => $path);
					$nowa->edytuj($id, $dane);
				}
				
				if($this->_request->getPost('atr'))
				{
					$Object = new Atrybutygrupy();
					$atr = $this->_request->getPost('atr');
					//var_dump($atr);die();
					if(@!empty($atr['nazwa']))
					$Object->_save($atr);
				}
				if($this->_request->getPost('atr_new'))
				{
					$Object = new Atrybutygrupy();
					$atr_new = $this->_request->getPost('atr_new');
					//var_dump($atr_new);die();
					if(@intval($atr_new) > 0)
					$Object->dodajKategorie($atr_new, $id);
				}
            }
            $this->view->kategoria = $kategoria = $nowa->showWybranaKategoria($id);
			
			$this->view->edytor = $this->fcKeditor('opis', stripcslashes($kategoria['opis']), 500, 'short');
            $this->view->skrot = $this->fcKeditor('skrot', stripcslashes($kategoria['short']), 200, 'short');
			
			if($this->obConfig->zdjeciaKategorie)
			if($wyswietlAll || $zakladka_produkty == 'zdj')
			{
				$galeria[0]['img'] = $kategoria['img'];
				$galeria[0]['nazwa'] = $kategoria['img_opis'];
				$galeria[0]['id'] = 1;
			}
			if($this->obConfig->baneryKategorie)
			if($wyswietlAll || $zakladka_produkty == 'banery')			
			{
				$galeria[1]['img'] = $kategoria['img1'];
				$galeria[1]['nazwa'] = $kategoria['img1_opis'];
				$galeria[1]['id'] = 2;
				$galeria[2]['img'] = $kategoria['img2'];
				$galeria[2]['nazwa'] = $kategoria['img2_opis'];
				$galeria[2]['id'] = 3;
			}
			$this->view->zdjecia = @$galeria;
			//var_dump($galeria);die();
			
			if($this->obConfig->wizytowkaSprzedawcy)
			{
				$sprzedawca = new Wizytowka();
				$this->view->sprzedawcy = $sprzedawca->wypiszWizytowki();
				$this->view->sprzedawca = $kategoria['sprzedawca'];
			}			
			if($this->obConfig->kategorieGrupy)
			{
				$kategorieGrupy = new Kategoriegrupy();
				$this->view->grupy = $kategorieGrupy->wypisz();
			}
			if($this->obConfig->allegroSzablony)
			{
				$allegroSzablony = new Allegroszablony();
				$this->view->szablony = $allegroSzablony->wypisz();
			}
			
			if($this->obConfig->allegro)
			if($wyswietlAll || $zakladka_produkty == 'allegroparams')
			{
				$pola = new Allegropola();
				$this->view->params = $pola->getPolaForKat($kategoria['allegro']);
				//var_dump($this->view->params);
				
				if($this->obConfig->producent)
				{
					$producenci = new Producent();
					$param['Marka'] = $producenci->wypiszProducentow();
				}
				
				if($this->obConfig->kolory)
				{
					$kolor = new Kolory();
					$param['Kolor'] = $kolor->wypiszKolory();
				}
				
				if(false)
				if($this->obConfig->tryb == 'rozmiary')
				{
					$rozmiar = new Rozmiary();
					$paramRozmiar = $rozmiar->wypiszKoloryDistinctKateg($id, 'nazwa');
					//$paramRozmiar = $rozmiar->wypiszKoloryDistinct(null, 'nazwa');
					if(count($paramRozmiar) > 0)
					foreach($paramRozmiar as $ide => $rozm)
					{
						$paramsRozmiar[$ide]['id'] = $rozm['nazwa'];
						$paramsRozmiar[$ide]['nazwa'] = $rozm['nazwa'];
					}
					$param['Rozmiar'] = isset($paramsRozmiar) ? $paramsRozmiar : null;
					//var_dump($paramsRozmiar);
				}				
				
				$this->view->param = isset($param) ? $param : null;
				
				$allegroparams = new Allegroparams();
				if(count($this->view->params) > 0)
				foreach($this->view->params as $allegroKat => $params)
				{
					$allegroparam = $allegroparams->getAllForKat($allegroKat);
					if(count($allegroparam) > 0)
					foreach($allegroparam as $par)
					$paramsAllegro[$allegroKat][$par['id_sell_form']][$par['id_param']] = $par['value'];
				}			
				$this->view->paramsAllegro = isset($paramsAllegro) ? $paramsAllegro : null;
				//var_dump($this->view->paramsAllegro);
			}
			
			if($this->obConfig->allegro || $this->obConfig->eBay)
			if($wyswietlAll || $zakladka_produkty == 'allegro' || $zakladka_produkty == 'ebay')
			{
				if($this->obConfig->allegro || $zakladka_produkty == 'allegro')
				if(@!empty($kategoria['allegro']))
				$katsDefault = explode(';', $kategoria['allegro']);
				
				if($this->obConfig->eBay || $zakladka_produkty == 'ebay')
				if(@!empty($kategoria['ebay']))
				$katsDefault = explode(';', $kategoria['ebay']);

				$this->view->kat0 = isset($katsDefault[0]) ? $katsDefault[0] : -1;
				$this->view->kat1 = isset($katsDefault[1]) ? $katsDefault[1] : -1;
				$this->view->kat2 = isset($katsDefault[2]) ? $katsDefault[2] : -1;
				$this->view->kat3 = isset($katsDefault[3]) ? $katsDefault[3] : -1;
				$this->view->kat4 = isset($katsDefault[4]) ? $katsDefault[4] : -1;
				
				if($this->obConfig->allegro || ($zakladka_produkty === 'allegro')) $katsAllegro = new Allegrokategorie();
				if($this->obConfig->eBay || ($zakladka_produkty === 'ebay')) $katsAllegro = new Ebaykategorie();
				$katsLevel0 = $katsAllegro->getChildren(0);
				$katsLevel1 = $katsAllegro->getChildren($this->view->kat0);
				$katsLevel2 = $katsAllegro->getChildren($this->view->kat1);
				$katsLevel3 = $katsAllegro->getChildren($this->view->kat2);
				$katsLevel4 = $katsAllegro->getChildren($this->view->kat3);
				$katsLevel5 = $katsAllegro->getChildren($this->view->kat4);
				//var_dump($katsAllegro);die();
				
				$this->view->katsLevel0 = $katsLevel0;
				$this->view->katsLevel1 = isset($katsLevel1) ? $katsLevel1 : null;
				$this->view->katsLevel2 = isset($katsLevel2) ? $katsLevel2 : null;
				$this->view->katsLevel3 = isset($katsLevel3) ? $katsLevel3 : null;
				$this->view->katsLevel4 = isset($katsLevel4) ? $katsLevel4 : null;
				$this->view->katsLevel5 = isset($katsLevel5) ? $katsLevel5 : null;
			}
			
			if($this->obConfig->atrybuty)
			{
				$Object = new Atrybutygrupy();
				$atrRem = (int) $this->_request->getParam('atrRem', 0);
				if($atrRem > 0)
				{
					$Object->id = $atrRem;
					$Object->_delete();
					$this->_redirect('/admin/kategorie/edytuj/id/'.$id);
					//$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/kategorie/edytuj/id/'.$id);
				}
				$atrDel = (int) $this->_request->getParam('atrDel', 0);
				if($atrDel > 0)
				{
					$Object->usunKategorie($atrDel, $id);
					$this->_redirect('/admin/kategorie/edytuj/id/'.$id);
					//$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/kategorie/edytuj/id/'.$id);
				}
				$path[-1] = array('id'=>0,'nazwa'=>'Wszystkie');
				$path[-2] = array('id'=>-$id,'nazwa'=>'Inne');
				//ksort($path);
				if(@count($path) > 0)
				foreach($path as $k => $kat)
				{
					$atrybuty[$k] = $Object->getAtrybutyForKateg($kat['id']);
				}
				//var_dump($atrybuty);die();
				$this->view->atrybuty = $atrybuty;
				$this->view->paths = $path;
				
				$Object = new Atrybuty();
				$this->view->typatr = $this->typatr = $Object->typatr;
				$this->view->typpola = $this->typpola = $Object->typpola;
			}
			
			$this->view->typ = $typ = 'kategorie';
			if($this->obConfig->kategorieGaleria)
			{
				$galeria = new Galeria();
				$this->view->error = $this->common->galeriaSupport($id, $typ, $this->_request);
				$imgEdit = intval($this->_request->getParam('imgEdit', 0));
				if($imgEdit > 0) $this->view->img = $galeria->pobierzNazwe($imgEdit);
				$this->view->galeria = $galeria->wyswietlGalerie($id, 'desc', 0, $typ);
			}
			if($this->obConfig->kategorieVideo)
			{
				$video = new Video();
				$this->view->error = $this->common->videoSupport($id, $typ, $this->_request);
				$vidEdit = $video->id = intval($this->_request->getParam('vidEdit', 0));
				if($vidEdit > 0) $this->view->video = $video->wypiszPojedynczego();
				$this->view->videos = $video->wypisz(false, $id, $typ);
			}
			$this->view->link = 'admin/kategorie/edytuj/id/'.$id;
			if($this->_request->isPost())
			if(@empty($this->view->error))
			$this->_redirect($this->view->link);
			
			$this->view->r_zapisz = 'zapisz';
			$this->view->r_dodaj = 'admin/kategorie/dodaj/typ/'.$this->typ.'/id/'.$id;
			$this->view->r_usun = 'admin/kategorie/lista/typ/'.$this->typ.'/del/'.$id;
			$this->view->r_lista = 'admin/kategorie/lista/typ/'.$this->typ;
			$this->view->r_pokaz = $this->view->kategoria['link'];
        }
		else
		{
            $this->view->error = '<div class="k_blad">Nie wybrałeś kategorii do edycji</div>';
        }
    }
    public function powiazaniaAction()
	{
		$lista = new Twtabele();
        $lista->link = $this->view->baseUrl;
        $lista->WypiszProdukty(0);
        //print_r($lista->ostatniedzieci);
        if($this->_request->isPost())
		{
            $dane['idkat'] = (int)$this->_request->getParam('idkat');
            $dane['idtw'] = (int)$this->_request->getParam('idtw');
            $lista->dodajPowiazanie($dane);
            $this->view->error ='<div class="k_ok">Operacja przebiegła pomyślnie.</div>';
        }        
        $lista->Wyswietl_kategorie_twtabel(0, 0);
        $this->view->lista = $lista->menu;
    }
	
	function grupyAction()
	{
		$kategorieGrupy = new Kategoriegrupy();
		$id = intval($this->_request->getParam('id', 0));
		$kategorieGrupy->id = $id;			
		
		$del = intval($this->_request->getParam('delid', 0));
		if($del > 0)
		{
			$kategorieGrupy->id = $del;
			$kategorieGrupy->usun();
			//$this->view->error = '<div class="k_ok">Wybrana grupa została usunięta.</div>';
			$this->_redirect('/admin/kategorie/grupy/');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/kategorie/grupy/');
		}
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa', '');
			$dane = array('nazwa' => $nazwa);
			if($id > 0)
			$kategorieGrupy->edytuj($dane);
			else
			$kategorieGrupy->id = $id = $kategorieGrupy->dodaj($dane);
			//$this->view->error = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/kategorie/grupy/');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/kategorie/grupy/');
		}
		
		$this->view->grupy = $kategorieGrupy->wypisz();
		$this->view->pojedynczy = $kategorieGrupy->wypiszPojedynczego();
		$this->view->tytul = @$this->view->pojedynczy->nazwa;
	}
}
?>