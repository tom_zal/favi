<?php
class Admin_OnlineController extends Ogolny_Controller_Admin
{
    public function init()
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
    }
    function __call($method, $args)
	{
        $this->_redirect('/admin');
    }
    function edytujAction() 
	{
        $platnosci = new Online();
        if($this->_request->isPost()) 
		{
            $zapisz = $this->_request->getPost('zapisz');
            if($zapisz == 'Zapisz')
            {
				//$dane['enable'] = $this->_request->getPost('enable');
				//if($dane['enable']=='on') $dane['enable']=1; else $dane['enable']=0;
				$dane['pos_id'] = $this->_request->getPost('pos_id');
				$dane['pos_auth_key'] = $this->_request->getPost('pos_auth_key');
				$dane['key1'] = $this->_request->getPost('key1');
				$dane['key2'] = $this->_request->getPost('key2');
				$platnosci->edytujDanePos($dane);
            }
        }
        $this->view->dane = $platnosci->wypiszDanePos();
    }
    function listaAction() 
	{
        $platnosci = new Online();
        $zamowienia = $platnosci->wypiszZamowienia();
        if(count($zamowienia)>0)
        $this->view->zamowienia = $zamowienia;
        else $this->view->zamowienia = Null;
    }
}
?>