<?php
class Admin_NewsletterController extends Ogolny_Controller_Admin
{
    public function init()
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
    }
    function __call($method, $args)
	{
        $this->_redirect('/admin');
    }
    public function aktualnosciAction()
	{
        $this->view->tytul = 'Newsletter - Aktualności';
        if($this->_request->isPost())
		{
            $dane = $this->_request->getPost();
            unset($dane['wyslij']);
            $sub = new Subskrypcja();
            $bledy = '';
            $typ = 'aktualnosci';
            if (count($dane) > 0)
			{
                $bledy = $sub->wyslijSubskrypcje($typ, $dane, $this->lang);
                if (strlen($bledy) > 0)
				{
                    $this->view->blad_edycji = '<div class="k_blad">Wystąpiły błędy podczas wysyłania subskrypcji.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $bledy . '</div>';
                }
				else 
				{
                    $this->view->blad_edycji = '<div class="k_ok">Subskrypcja została wysłana pomyślnie.</div>';
					//$this->_redirect('/admin/newsletter/aktualnosci/');
					$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/newsletter/aktualnosci/');
                }
            }
			else
			{
                $this->view->blad_edycji = '<div class="k_blad">Wystąpiły błędy podczas wysyłania subskrypcji.</div>';
                $this->view->lista_bledow = '<div id="lista_bledow">Nie wybrano aktualności do wysłania w subskrypcji.</div>';
            }
        }
        $aktualnosci = new Aktualnosci();
        $aktualnosciNews = $aktualnosci->wypiszDoNewslettera($this->lang);
        $this->view->news = $aktualnosciNews;
    }
	public function artykulyAction()
	{
        $this->view->tytul = 'Newsletter - Artykuły';
        if ($this->_request->isPost())
		{
            $dane = $this->_request->getPost();
            unset($dane['wyslij']);
            $sub = new Subskrypcja();
            $bledy = '';
            $typ = 'artykuly';
            if (count($dane) > 0)
			{
                $bledy = $sub->wyslijSubskrypcje($typ, $dane, $this->lang);
                if (strlen($bledy) > 0)
				{
                    $this->view->blad_edycji = '<div class="k_blad">Wystąpiły błędy podczas wysyłania subskrypcji.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $bledy . '</div>';
                }
				else 
				{
                    $this->view->blad_edycji = '<div class="k_ok">Subskrypcja została wysłana pomyślnie.</div>';
					//$this->_redirect('/admin/newsletter/artykuly/');
					$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/newsletter/artykuly/');
                }
            }
			else
			{
                $this->view->blad_edycji = '<div class="k_blad">Wystąpiły błędy podczas wysyłania subskrypcji.</div>';
                $this->view->lista_bledow = '<div id="lista_bledow">Nie wybrano artykułów do wysłania w subskrypcji.</div>';
            }
        }
        $aktualnosci = new Artykuly();
        $aktualnosciNews = $aktualnosci->wypiszDoNewslettera($this->lang);
        $this->view->news = $aktualnosciNews;
    }
	public function poradyAction()
	{
        $this->view->tytul = 'Newsletter - Porady';
        if($this->_request->isPost())
		{
            $dane = $this->_request->getPost();
            unset($dane['wyslij']);
            $sub = new Subskrypcja();
            $bledy = '';
            $typ = 'porady';
            if (count($dane) > 0)
			{
                $bledy = $sub->wyslijSubskrypcje($typ, $dane, $this->lang);
                if (strlen($bledy) > 0)
				{
                    $this->view->blad_edycji = '<div class="k_blad">Wystąpiły błędy podczas wysyłania subskrypcji.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $bledy . '</div>';
                }
				else 
				{
                    $this->view->blad_edycji = '<div class="k_ok">Subskrypcja została wysłana pomyślnie.</div>';
					//$this->_redirect('/admin/newsletter/porady/');
					$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/newsletter/porady/');
                }
            }
			else
			{
                $this->view->blad_edycji = '<div class="k_blad">Wystąpiły błędy podczas wysyłania subskrypcji.</div>';
                $this->view->lista_bledow = '<div id="lista_bledow">Nie wybrano porad do wysłania w subskrypcji.</div>';
            }
        }
        $aktualnosci = new Porady();
        $aktualnosciNews = $aktualnosci->wypiszDoNewslettera($this->lang);
        $this->view->news = $aktualnosciNews;
    }

    public function nowosciAction()
	{
        $this->view->tytul = 'Newsletter - Nowości produkcyjne';
        if($this->_request->isPost())
		{
            $dane = $this->_request->getPost();
            unset($dane['wyslij']);
            $sub = new Subskrypcja();
            $bledy = '';
            $typ = 'nowosci';
            if (count($dane) > 0)
			{
                $bledy = $sub->wyslijSubskrypcje($typ, $dane, $this->lang);
                if (strlen($bledy) > 0)
				{
                    $this->view->blad_edycji = '<div class="k_blad">Wystąpiły błędy podczas wysyłania subskrypcji.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $bledy . '</div>';
                }
				else 
				{
                    $this->view->blad_edycji = '<div class="k_ok">Subskrypcja została wysłana pomyślnie.</div>';
					//$this->_redirect('/admin/newsletter/nowosci/');
					$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/newsletter/nowosci/');
                }
            }
			else
			{
                $this->view->blad_edycji = '<div class="k_blad">Wystąpiły błędy podczas wysyłania subskrypcji.</div>';
                $this->view->lista_bledow = '<div id="lista_bledow">Nie wybrano produktów do wysłania w subskrypcji.</div>';
            }
        }
        $nowosci = new Produkty();
        $nowosciNews = $nowosci->wypiszProduktyDoNewslettera('nowosc', $this->lang);
        $nowosciNews = $this->produktyGaleria($nowosciNews);
        $this->view->news = $nowosciNews;
    }
    public function promocjeAction()
	{
        $this->view->tytul = 'Newsletter - Promocje';
        if($this->_request->isPost())
		{
            $dane = $this->_request->getPost();
            unset($dane['wyslij']);
            $sub = new Subskrypcja();
            $bledy = '';
            $typ = 'promocje';
            if (count($dane) > 0)
			{
                $bledy = $sub->wyslijSubskrypcje($typ, $dane, $this->lang);
                if (strlen($bledy) > 0)
				{
                    $this->view->blad_edycji = '<div class="k_blad">Wystąpiły błędy podczas wysyłania subskrypcji.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $bledy . '</div>';
                } 
				else 
				{
                    $this->view->blad_edycji = '<div class="k_ok">Subskrypcja została wysłana pomyślnie.</div>';
					//$this->_redirect('/admin/newsletter/promocje/');
					$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/newsletter/promocje/');
                }
            }
			else
			{
                $this->view->blad_edycji = '<div class="k_blad">Wystąpiły błędy podczas wysyłania subskrypcji.</div>';
                $this->view->lista_bledow = '<div id="lista_bledow">Nie wybrano produktów do wysłania w subskrypcji.</div>';
            }
        }
        $promocje = new Produkty();
        $promocjeNews = $promocje->wypiszProduktyDoNewslettera('promocja', $this->lang);
        $promocjeNews = $this->produktyGaleria($promocjeNews);
        $this->view->news = $promocjeNews;
    }
	public function polecaneAction()
	{
        $this->view->tytul = 'Newsletter - Polecane';
        if($this->_request->isPost())
		{
            $dane = $this->_request->getPost();
            unset($dane['wyslij']);
            $sub = new Subskrypcja();
            $bledy = '';
            $typ = 'polecane';
            if (count($dane) > 0)
			{
                $bledy = $sub->wyslijSubskrypcje($typ, $dane, $this->lang);
                if (strlen($bledy) > 0)
				{
                    $this->view->blad_edycji = '<div class="k_blad">Wystąpiły błędy podczas wysyłania subskrypcji.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $bledy . '</div>';
                } 
				else 
				{
                    $this->view->blad_edycji = '<div class="k_ok">Subskrypcja została wysłana pomyślnie.</div>';
					//$this->_redirect('/admin/newsletter/polecane/');
					$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/newsletter/polecane/');
                }
            }
			else
			{
                $this->view->blad_edycji = '<div class="k_blad">Wystąpiły błędy podczas wysyłania subskrypcji.</div>';
                $this->view->lista_bledow = '<div id="lista_bledow">Nie wybrano produktów do wysłania w subskrypcji.</div>';
            }
        }
        $promocje = new Produkty();
        $promocjeNews = $promocje->wypiszProduktyDoNewslettera('polecane', $this->lang);
        $promocjeNews = $this->produktyGaleria($promocjeNews);
        $this->view->news = $promocjeNews;
    }
	public function wyprzedazAction()
	{
        $this->view->tytul = 'Newsletter - Wyprzedaż';
        if($this->_request->isPost())
		{
            $dane = $this->_request->getPost();
            unset($dane['wyslij']);
            $sub = new Subskrypcja();
            $bledy = '';
            $typ = 'wyprzedaz';
            if (count($dane) > 0)
			{
                $bledy = $sub->wyslijSubskrypcje($typ, $dane, $this->lang);
                if (strlen($bledy) > 0)
				{
                    $this->view->blad_edycji = '<div class="k_blad">Wystąpiły błędy podczas wysyłania subskrypcji.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $bledy . '</div>';
                } 
				else 
				{
                    $this->view->blad_edycji = '<div class="k_ok">Subskrypcja została wysłana pomyślnie.</div>';
					//$this->_redirect('/admin/newsletter/polecane/');
					$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/newsletter/wyprzedaz/');
                }
            }
			else
			{
                $this->view->blad_edycji = '<div class="k_blad">Wystąpiły błędy podczas wysyłania subskrypcji.</div>';
                $this->view->lista_bledow = '<div id="lista_bledow">Nie wybrano produktów do wysłania w subskrypcji.</div>';
            }
        }
        $promocje = new Produkty();
        $promocjeNews = $promocje->wypiszProduktyDoNewslettera('wyprzedaz', $this->lang);
        $promocjeNews = $this->produktyGaleria($promocjeNews);
        $this->view->news = $promocjeNews;
    }
	public function bestsellerAction()
	{
		$this->view->headScript()->appendFile($this->_request->getBaseUrl().'/public/scripts/functionadmin_events.js', 'text/javascript');
        $this->view->tytul = 'Najchętniej kupowane';
		$polecane = new Kontrahencipolecane();
        if($this->_request->getPost('wyslij'))
		{
            $bestsellery = $polecane->wypiszKlientTyp(0, 'bestseller');
			{
				$ids = @implode(',', $this->_request->getPost('bestseller'));
				$ids = empty($ids) ? ',' : ','.$ids.',';
				if(count($bestsellery) > 0)
				{
					$polecane->id = $bestsellery['id'];
					$polecane->edytuj(array('id_prod' => $ids));
				}
				else
				{
					$polecane->dodaj(array('id_kontr' => 0, 'id_prod' => $ids, 'typ' => 'bestseller'));
				}
			}
			$this->_redirect('/admin/newsletter/bestseller/');
        }
		if($this->_request->getPost('powiazanie'))
		{
            $bestseller = $this->_request->getPost('powiazanie');
			$polecane->dodajProdukt(0, $bestseller, 'bestseller');
			$this->_redirect('/admin/newsletter/bestseller/');
        }
		$delid = intval($this->_request->getParam('del'));
		if($delid > 0)
		{
			$bestsellery = $polecane->wypiszKlientTyp(0, 'bestseller');
			$polecane->id = $bestsellery['id'];
			$polecane->edytuj(array('id_prod' => str_replace(','.$delid.',', ',', $bestsellery['id_prod'])));
			$this->_redirect('/admin/newsletter/bestseller/');
		}
        $achiwum = new Archiwum();
		$bestsellery = $achiwum->bestsellery(99);
        $this->view->bestsellery = $this->produktyGaleria($bestsellery);
		$bestsellery = $polecane->wypiszKlientTyp(0, 'bestseller');
		$this->view->polec = @explode(',', $bestsellery['id_prod']);
		$bestselleryWlasne = $polecane->wypiszProdukty2(0, 'bestseller', 99, 'nazwa asc', true);
		$this->view->bestselleryWlasne = $this->produktyGaleria($bestselleryWlasne);
    }
	
	public function obserwowaneAction()
	{
        $this->view->tytul = 'Obserwowane';
		$polecane = new Kontrahencipolecane();
        if($this->_request->getPost('wyslij'))
		{
            $bestsellery = $polecane->wypiszKlientTyp(0, 'obserwowane');
			{
				$ids = @implode(',', $this->_request->getPost('obserwowane'));
				$ids = empty($ids) ? ',' : ','.$ids.',';
				if(count($bestsellery) > 0)
				{
					$polecane->id = $bestsellery['id'];
					$polecane->edytuj(array('id_prod' => $ids));
				}
				else
				{
					$polecane->dodaj(array('id_kontr' => 0, 'id_prod' => $ids, 'typ' => 'obserwowane'));
				}
			}
			$this->_redirect('/admin/newsletter/obserwowane/');
        }
		if($this->_request->getPost('powiazanie'))
		{
            $obserwowane = $this->_request->getPost('powiazanie');
			$polecane->dodajProdukt(0, $obserwowane, 'obserwowane');
			$this->_redirect('/admin/newsletter/obserwowane/');
        }
		$delid = intval($this->_request->getParam('del'));
		if($delid > 0)
		{
			$obserwowane = $polecane->wypiszKlientTyp(0, 'obserwowane');
			$polecane->id = $obserwowane['id'];
			$polecane->edytuj(array('id_prod' => str_replace(','.$delid.',', ',', $obserwowane['id_prod'])));
			$this->_redirect('/admin/newsletter/obserwowane/');
		}
        $kontrahenciPolecane = new Kontrahencipolecane();
		$obserwowane = $kontrahenciPolecane->wypiszProdukty2(0, 'obserwowane', 999, 'data desc');
        $obserwowane = $this->produktyGaleria($obserwowane);
		for($i=0; $i<count($obserwowane); $i++)
		{
			$obserwowaneByID[$obserwowane[$i]['id']] = $obserwowane[$i];		
		}
		$this->view->obserwowane = @$obserwowaneByID;
		$this->view->obserwujacy = $kontrahenciPolecane->wypiszKlientTyp(0, 'obserwowane');
    }
	
    public function adresyAction()
	{
        $id = (int) $this->_request->getParam('id', 0);
        $status = (int) $this->_request->getParam('status', 0);
        $delid = (int) $this->_request->getParam('delid', 0);
        if ($id > 0)
		{
            $dane['status'] = $status;
            $sub = new Subskrypcja();
            $sub->edytuj($id, $dane);
        }
        if ($delid > 0)
		{
            $sub = new Subskrypcja();
            $sub->usun($delid);
        }
        $lang = $this->lang;
        $subskrypcja = new Subskrypcja();
        $this->view->adresy = $subskrypcja->wypisz($lang);
		$kontrahent = new Kontrahenci();
		$newsletterZgodaIle = $kontrahent->iloscKlientowNewsletter();
		$this->view->blad_edycji = 'Do newslettera oprócz poniższej listy zapisało się również ';
		$this->view->blad_edycji.= '<a href="'.$this->baseUrl.'/admin/kontrahent/lista/newsletter/1">';
		$this->view->blad_edycji.= '<b>'.$newsletterZgodaIle. '</b> zarejestrowanych klientów</a>';
    }
	
	public function importAction()
	{
		if($this->_request->isPost()) 
		{
			$cale = $this->_request->getPost('nowe');
			if(isset($cale)) 
			{
				$adapter = new Zend_File_Transfer_Adapter_Http();
				$adapter->setDestination($this->ImageDir->ZipFileDir);
				$adapter->addValidator('Extension', false, 'txt')
						->addValidator('Count', false, 1);
				if($adapter->receive()) 
				{
					$filename = $adapter->getFileName();
					$subskrypcja = new Subskrypcja();
					$adresy = $subskrypcja->wypiszMaile($this->lang);
					$walidacja = new WalidacjaKontrahenta();
					$ile = 0;
					$new = 0;
					$old = 0;
					$bad = 0;
					if(file_exists($filename)) 
					{
						$file = fopen($filename, "r");
						while(($email = fgets($file)) !== false)
						{
							if(!empty($email))
							{
								$ile++;
								if(!$walidacja->sprawdzEmail($email)) $bad++;
								else if(isset($adresy[$email])) $old++;
								else if($subskrypcja->dodaj($email)) $new++;
							}
						}
						fclose($file);
					} 
					else 
					{
						$this->bledy .= 'Zła nazwa pliku txt lub brak pliku txt <br>';
					}
				} 
				else 
				{
					$this->bledy .= 'Zły typ pliku. Akceptowalny typ to txt (*.txt) <br>';
				}
				if (true && @strlen($this->bledy) > 1) 
				{
					$this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>';
					$this->view->lista_bledow = '<div id="lista_bledow" style="margin-left: 0px;">' . $this->bledy . '</div>';
				} 
				else 
				{
					//@unlink($filename);
					$this->view->blad_edycji = '<div class="k_ok">Operacja przebiegła pomyślnie.<br/>';
					$this->view->blad_edycji.= 'Znaleziono '.$ile.' adresów e-mail<br/>';
					$this->view->blad_edycji.= 'Znaleziono '.$bad.' niepoprawnych adresów e-mail<br/>';
					$this->view->blad_edycji.= 'Znaleziono '.$old.' istniejących adresów e-mail<br/>';
					$this->view->blad_edycji.= 'Zaimportowano '.$new.' adresów e-mail<br/>';
					$this->view->blad_edycji.= '</div>';
				}
			}
		}
	}

	public function exportAction()
	{
		//echo 's';die();
		//echo preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', "james890@gmail.com");die();
        if($this->_request->isPost())
		{
            $plik = 'e-maile';
            if(strlen($plik) > 1)
			{
                $filename = 'admin/bazy/' . $plik . '-' . date('Y-m-d') . '.txt';
                $filenamezip = 'admin/bazy/' . $plik . '-' . date('Y-m-d') . '.zip';

				if(true)
				{
					$subskrypcja = new Subskrypcja();
					$adresy = $subskrypcja->wypiszMaile($this->lang);
					$walidacja = new WalidacjaKontrahenta();
					$file = fopen($filename, "w");
					if(count($adresy) > 0)
					{
						//$zawartosc = $zawartosc->toArray();
						foreach($adresy as $mail => $adres)
						{
							$email = trim($adres['email']);
							if(!empty($email))
							if($walidacja->sprawdzEmail($email))
							fwrite($file, $adres['email'].PHP_EOL);
						}
					}
					fclose($file);

					if(file_exists($filename))
					{
						$bledy = '';
						//var_dump($bledy);
						if (strlen($bledy) > 0)
						{
							$this->view->blad_edycji = '<div class="k_blad">Błąd podczas exportu bazy.</div>';
							$this->view->lista_bledow = '<div id="lista_bledow">' . $bledy . '</div>';
						}
						else
						{
							$this->view->blad_edycji = '<div class="k_ok">Export bazy przebiegł pomyślnie.
							<a href="'.$this->view->baseUrl.'/public/'.$filename.'" target="_blank">Link do pliku txt</a></div>';
						}
					}
					else
					{
						$this->view->blad_edycji = '<div class="k_blad">Błąd podczas exportu bazy.</div>';
						$this->view->lista_bledow = '<div id="lista_bledow">
							Nie udało się stworzyć pliku '.$plik.'-'.date('Y-m-d').'.txt.</div>';
					}
				}
            }
			else
			{
                $this->view->blad_edycji = '<div class="k_blad">Błąd podczas exportu bazy.</div>';
                $this->view->lista_bledow = '<div id="lista_bledow">Zbyt krótka nazwa pliku lub brak nazwy.</div>';
            }
        }
    }
}
?>