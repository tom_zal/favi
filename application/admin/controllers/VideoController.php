<?php
class Admin_VideoController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		//echo $this->view->baseUrl;
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}	
	function dodajAction()
	{
		$video = new Video();
		
		$del = intval($this->_request->getParam('delid', 0));
		if($del > 0)
		{
			$video->id = $del;
			$video->usun();
			//$this->view->blad_edycji = '<div class="k_ok">Wybrane video zostało usunięte.</div>';
			$this->_redirect('admin/video/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/video/dodaj/');
		}
		if(($pokazid = $this->_request->getParam('pokazid')) > 0)
		{
			$widoczny = @intval($this->_request->getParam('widoczny')?1:0);
			$video->id = $pokazid;
            $video->edytuj(array('wyswietl' => $widoczny), $pokazid);
			$this->_redirect('admin/video/dodaj');
        }
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa', '');
			$www = $this->_request->getPost('www', '');
			$opis = $this->_request->getPost('opis', '');
			$dane = array('nazwa' => $nazwa, 'www' => $www, 'opis' => $opis, 'lang' => $this->lang);
			$video->dodaj($dane);
			$this->_redirect('admin/video/dodaj');
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/video/dodaj');
		}			
		$this->view->videos = $video->wypisz();
	}	
	function edytujAction()
	{
		$video = new Video();
		if($this->_request->isPost())
		{
			$id = $this->_request->getPost('id');
			$nazwa = $this->_request->getPost('nazwa', '');
			$www = $this->_request->getPost('www', '');	
			$opis = $this->_request->getPost('opis', '');
			$dane = array('nazwa' => $nazwa, 'www' => $www, 'opis' => $opis);
			$video->id = $id;
			$video->edytuj($dane);
			$this->_redirect('admin/video/dodaj');
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/video/dodaj');
		}
		$id = @intval($this->_request->getParam('id', 0));		
		$video->id = $id;
		$this->view->videos = $video->wypisz();
		$this->view->pojedynczy = $video->wypiszPojedynczego();
		$this->view->tytul = $this->view->pojedynczy->nazwa;
	}
}
?>