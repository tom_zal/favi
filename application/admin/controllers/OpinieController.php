<?php
class Admin_OpinieController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		//echo $this->view->baseUrl;
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}
	
	function edytujAction()
	{
		$this->view->headScript()->appendFile($this->_request->getBaseUrl().'/public/scripts/jquery.raty/js/jquery.raty.js', 'text/javascript');
		
		$opinie = new Opinie();
		$opiDel = (int) $this->_request->getParam('opiDel', 0);            
		if ($opiDel > 0)
		{
			$opinie = new Opinie();
			$opinie->id = $opiDel;
			$opinie->usunOpinie();
			//$this->view->blad_edycji = '<div class="k_ok">Zdjęcie zostało usunięte.</div>';
			$this->_redirect('/admin/opinie/edytuj');
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/opinie/edytuj/');
		}
		$edycjaOpinii = $this->_request->getPost('edycjaOpinii');				
		if ($edycjaOpinii)
		{
			$daneOpinie['imie'] = $this->_request->getPost('opiniaImie');
			$daneOpinie['czas'] = $this->_request->getPost('opinieCzasScore');
			$daneOpinie['kontakt'] = $this->_request->getPost('opinieKontaktScore');
			$daneOpinie['produkt'] = $this->_request->getPost('opinieProduktScore');
			$daneOpinie['opis'] = $this->_request->getPost('opiniaOpis');
			//var_dump($_POST);
			$opinie = new Opinie();
			$opinie->id = $this->_request->getPost('opiniaID');
			$opinie->edytujOpinie($daneOpinie);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/opinie/edytuj');
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/opinie/edytuj/');
		}
		$id = $this->_request->getParam('id', 0);
		if($id > 0)
		{
			$opinie->id = $id;
			$this->view->opinia = $opinie->wypiszPojedynczego();
		}
		$this->view->opinie = $opinie->wypiszOpinieForProdukt();
		$this->view->tytul = 'Opinie';
	}
}
?>