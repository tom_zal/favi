<?php
class Admin_OdwiedzinyController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}
	
	function logonklientAction()
	{
		$logowania = new KontrahenciLogin();
		$this->view->loginy = $logowania->wypiszForLastLogon();
		$this->view->ileLogin = count($this->view->loginy);
		$this->view->obecni = $logowania->wypiszForLastObecni();
		$this->view->ileObecni = count($this->view->obecni);
		
		$kontrahent = new Kontrahenci();
		$this->view->ileUsers = $kontrahent->wypiszKontr(true, 0, 99999, null, null, false);
	}
	function logonadminAction()
	{
		if(!$this->obConfig->uzytkownicy || ($this->adminNazwa != 'admin' && $this->adminNazwa != 'bigcom'))
		$this->_redirect('/admin/index/');//panel/zakladka/1/');
		
		$logowania = new KontrahenciLogin();
		$this->view->loginy = $logowania->wypiszForLastLogon(0, 'login');
		$this->view->ileLogin = count($this->view->loginy);
		$this->view->obecni = $logowania->wypiszForLastObecni(0, 0, 'login');
		$this->view->ileObecni = count($this->view->obecni);
		
		$login = new Login();
		$this->view->ileUsers = @count($login->wypisz());
	}
	
	function pokazAction()
	{
		if($this->obConfig->producent)
		{
			$producenci = new Producent();
			$this->view->producenci = $producenci->wypiszProducentow();
		}
		if($this->obConfig->tryb == "rozmiary")
		{
			$rozmiar = new Rozmiary();
			$this->view->rozmiary = $rozmiar->wypiszKoloryDistinct(null, 'nazwa');
		}
		if($this->obConfig->kolory)
		{
			$kolor = new Kolory();
			$this->view->kolory = $kolor->wypiszKolory();
		}
		
		$szukanie = new Zend_Session_Namespace('szukanieOdwiedzinyAdmin');
		if ($this->_request->getParam('reset')) 
		{
			$szukanie->nazwa = '';
			$szukanie->oznaczenie = '';
			$szukanie->grupa = 'all';
			$szukanie->rozmiar = 'all';
			$szukanie->wkladka = 'all';
			$szukanie->kolor = -1;
			$szukanie->cena_brutto = -1;
			$szukanie->cena_do = -1;
			$szukanie->cena_od = -1;
			$szukanie->producent = -1;
			$szukanie->katMain = -1;
			$szukanie->podkat1 = -1;
			$szukanie->podkat2 = -1;
			$szukanie->podkat3 = -1;
			$szukanie->kateg = -1;
			$szukanie->katInne = -1;
			$szukanie->katInne1 = -1;
			$szukanie->katInne2 = -1;
			$szukanie->katInne3 = -1;
			$szukanie->katInny = -1;
			$szukanie->odwiedziny = 'all';
			$szukanie->dataOdwiedzinOd = '2010-01-01';
			$szukanie->dataOdwiedzinDo = date('Y-m-d');
		}
		if ($this->_request->getParam('rok'))
		{
			$szukanie->odwiedziny = $this->_request->getParam('rok');
			if($szukanie->odwiedziny == 'all')
			{
				$szukanie->dataOdwiedzinOd = '2010-01-01';
				$szukanie->dataOdwiedzinDo = date('Y-m-d');
			}
			else
			{
				$szukanie->dataOdwiedzinOd = $szukanie->odwiedziny.'-01-01';
				$szukanie->dataOdwiedzinDo = $szukanie->odwiedziny.'-12-31';
			}
			//$szukanie->odwiedziny = 'all';
		}
		if ($this->_request->getPost('szukaj'))
		{
			$szukanie->nazwa = addslashes($this->_request->getPost('nazwa'));
			$szukanie->oznaczenie = addslashes($this->_request->getPost('kod'));
			$szukanie->grupa = $this->_request->getPost('grupa');
			$szukanie->rozmiar = $this->_request->getPost('rozmiar');
			$szukanie->wkladka = 'all';
			$szukanie->kolor = $this->_request->getPost('kolor');
			$szukanie->cena_brutto = -1;
			$szukanie->cena_od = floatval(str_replace(",",".",$this->_request->getPost('cenaOd',0)));
			$szukanie->cena_do = floatval(str_replace(",",".",$this->_request->getPost('cenaDo',0)));
			$szukanie->producent = $this->_request->getPost('prod');
			$szukanie->katMain = $this->_request->getPost('kateg');
			$szukanie->podkat1 = $this->_request->getPost('podkat1');
			$szukanie->podkat2 = $this->_request->getPost('podkat2');
			$szukanie->podkat3 = $this->_request->getPost('podkat3');
			$szukanie->kateg = $szukanie->podkat3>0?$szukanie->podkat3:
			($szukanie->podkat2>0?$szukanie->podkat2:($szukanie->podkat1>0?$szukanie->podkat1:$szukanie->katMain));
			$szukanie->katInne = $this->_request->getPost('katInne');
			$szukanie->katInne1 = $this->_request->getPost('katInne1');
			$szukanie->katInne2 = $this->_request->getPost('katInne2');
			$szukanie->katInne3 = $this->_request->getPost('katInne3');
			$szukanie->katInny = $szukanie->katInne3>0?$szukanie->katInne3:
			($szukanie->katInne2>0?$szukanie->katInne2:($szukanie->katInne1>0?$szukanie->katInne1:$szukanie->katInne));
			
			$szukanie->odwiedziny = 'all';
		}
		if ($this->_request->getPost('dataSzukaj'))
		{			
			$szukanie->dataOdwiedzinOd = $this->_request->getPost('dataOdwiedzinOd');
			$szukanie->dataOdwiedzinDo = $this->_request->getPost('dataOdwiedzinDo');
			$szukanie->odwiedziny = 'all';//$this->_request->getPost('odwiedziny');
		}
		if ($this->_request->getParam('ile'))
		{
			$szukanie->ile = $this->_request->getParam('ile');
		}		
		if ($this->_request->getParam('od') != '')
		{
			$szukanie->od = $this->_request->getParam('od');
		}
		
		if (!isset($szukanie->co)) $szukanie->co = '';
		if (!isset($szukanie->w)) $szukanie->w = 'nazwa';
		if (!isset($szukanie->nazwa)) $szukanie->nazwa = '';
		if (!isset($szukanie->oznaczenie)) $szukanie->oznaczenie = '';
		if (!isset($szukanie->grupa)) $szukanie->grupa = 'all';
		if (!isset($szukanie->rozmiar)) $szukanie->rozmiar = 'all';
		if (!isset($szukanie->wkladka)) $szukanie->wkladka = 'all';
		if (!isset($szukanie->kolor)) $szukanie->kolor = -1;
		if (!isset($szukanie->cena_brutto)) $szukanie->cena_brutto = -1;
		if (!isset($szukanie->cena_do)) $szukanie->cena_do = -1;
		if (!isset($szukanie->cena_od)) $szukanie->cena_od = -1;
		if (!isset($szukanie->producent)) $szukanie->producent = -1;
		if (!isset($szukanie->katMain)) $szukanie->katMain = -1;
		if (!isset($szukanie->podkat1)) $szukanie->podkat1 = -1;
		if (!isset($szukanie->podkat2)) $szukanie->podkat2 = -1;
		if (!isset($szukanie->podkat3)) $szukanie->podkat3 = -1;
		if (!isset($szukanie->kateg)) $szukanie->kateg = -1;
		if (!isset($szukanie->katInne)) $szukanie->katInne = -1;
		if (!isset($szukanie->katInne1)) $szukanie->katInne1 = -1;
		if (!isset($szukanie->katInne2)) $szukanie->katInne2 = -1;
		if (!isset($szukanie->katInne3)) $szukanie->katInne3 = -1;
		if (!isset($szukanie->katInny)) $szukanie->katInny = -1;
		if (!isset($szukanie->ile)) $szukanie->ile = 24;
		if (!isset($szukanie->od)) $szukanie->od = 0;
		//$szukanie->ile = 1;
		if (!isset($szukanie->odwiedziny)) $szukanie->odwiedziny = 'all';		
		if (!isset($szukanie->dataOdwiedzinOd)) $szukanie->dataOdwiedzinOd = '2010-01-01';
		if (!isset($szukanie->dataOdwiedzinDo)) $szukanie->dataOdwiedzinDo = date('Y-m-d');

		//$szukanie->ile = 22;
		$ile = $szukanie->ile;
		$od = floor($szukanie->od / $ile);
		$od = $szukanie->od;
		
		$sortowanie = new Zend_Session_Namespace('sortowanieOdwiedzinyAdmin');
		if ($this->_request->getParam('reset')) 
		{
			$sortowanie->sort = 'odwiedziny';
			$sortowanie->order = 'desc';
			$this->_redirect('admin/odwiedziny/pokaz');
		}
		if ($this->_request->getPost('specjalne')) 
		{
			$sortowanie->specjalne = $this->_request->getPost('specjalne');
		}
		if($this->_request->getParam('sort'))
		{
			$sortowanie->sort = $this->_request->getParam('sort', 'odwiedziny');
			$sortowanie->order = $this->_request->getParam('order', 'desc');
		}
		if(!isset($sortowanie->sort))
		{
			$sortowanie->sort = 'odwiedziny';
			$sortowanie->order = 'desc';
		}
		if(!isset($sortowanie->specjalne))
		{
			$sortowanie->specjalne = 'all';
		}
		
		if($sortowanie->sort == 'dostepnosc') $szukanie->kolor = -1;
		
		$all = null;
		$test = @intval($this->_request->getParam('test', false));
		if(intval($szukanie->kateg) != -1 || intval($szukanie->katInny) != -1)
		{
			$produkty = new Katprod();
			$produkty->id = intval($szukanie->kateg);
			$produkty->nr = intval($szukanie->katInny);
			$ilosc = $produkty->selectProdukt($od, $ile, $sortowanie, $szukanie, true, 0, null, $test);
			if($ilosc > 0)
			$all = $produkty->selectProdukt($od, $ile, $sortowanie, $szukanie, false, 0, null, $test);
		}
		else
		{
			$produkty = new Produkty();
			$ilosc = $produkty->wypiszProduktyPodzieloneStrona(true, $od, $ile, $sortowanie, $szukanie, 0, null, $test);
			if($ilosc > 0)
			$all = $produkty->wypiszProduktyPodzieloneStrona(false, $od, $ile, $sortowanie, $szukanie, 0, null, $test);
		}
		//$all = $this->produktyGaleria($all);
		$galeria = new Galeria();
		for($i=0;$i<count($all);$i++)
		{
			$main = $galeria->wyswietlGlowne($all[$i]['id']);
			$all[$i]['img'] = $main[0]['img'];
		}
		//var_dump($all);
					
		$kategoria = new Kategorie();
		$this->view->ilosc = ceil($ilosc / $ile);
		$this->view->realizacje = $all;
		$this->view->tytul = 'wszystkie';
		$this->view->typ = 'wszystkie';
		$this->view->szukanie = $szukanie;
		$this->view->sortowanie = $sortowanie;
		$this->view->od = $od;
		$this->view->strona = $szukanie->od;
		$this->view->ile = $ile;
		$this->view->strony = ceil($ilosc / $ile);
		$this->view->link = 'admin/odwiedziny/pokaz';
		$this->view->nazwa = $szukanie->nazwa;
		$this->view->kod = $szukanie->oznaczenie;
		$this->view->co = $szukanie->co;
		$this->view->w = $szukanie->w;
		$kategoria->typ = 'kategorie';
		$this->view->katMain = $this->view->glowneMain = $kategoria->wypiszDzieci(0);
		$this->view->podkat1 = $kategoria->wypiszDzieci($szukanie->katMain);
		$this->view->podkat2 = $kategoria->wypiszDzieci($szukanie->podkat1);
		$this->view->podkat3 = $kategoria->wypiszDzieci($szukanie->podkat2);
		$this->view->kateg = $szukanie->kateg;
		if($this->obConfig->kategorieInne)
		{
			$kategoria->typ = $this->obConfig->kategorieInneNazwa;
			$this->view->katInne = $this->view->glowneInne = $kategoria->wypiszDzieci(0);
			$this->view->katInne1 = $kategoria->wypiszDzieci($szukanie->katInne);
			$this->view->katInne2 = $kategoria->wypiszDzieci($szukanie->katInne1);
			$this->view->katInne3 = $kategoria->wypiszDzieci($szukanie->katInne2);
			$this->view->katInny = $szukanie->katInny;
		}
		$this->view->order = $sortowanie->order == 'asc' ? 'desc' : 'asc';
		
		$raport = $all;
		$this->view->ilosc3 = round(count($raport)/3);
		$this->view->raport = $raport;
		$max = 0;
		if(count($raport) > 0)
		$max = $sortowanie->order=='asc' ? $raport[$ilosc-1]['odwiedziny_suma'] : $raport[0]['odwiedziny_suma'];
		$jednostka = count($raport) > 0 ? ($max > 0 ? 800/$max : 0) : 0;
		$this->view->jednostka = $jednostka;
		
		$odwiedziny = new Odwiedziny();
		$lata = $odwiedziny->wypiszLataForAll();
		$this->view->lata = $lata;
		$this->view->rok = $szukanie->odwiedziny;
	}
	
	function listaAction()
	{
		$rok = $this->_request->getParam('rok', date('Y'));
		if($rok)
		{
			if($rok == 'all')
			{
				$dataOd = '2010-01-01';
				$dataDo = date('Y-m-d');
			}
			else
			{
				$dataOd = $rok.'-01-01';
				$dataDo = $rok.'-12-31';
			}
		}
		if($this->_request->isPost())
		{
			$dataOd = $this->_request->getPost('dataOd', $dataOd);
			$dataDo = $this->_request->getPost('dataDo', $dataDo);
		}
		
		$lista = new Zend_Session_Namespace('odwiedzinyLista');
		$typ = $this->_request->getParam('typ');
		if($typ) $lista->typ = $typ;
		if(!isset($lista->typ)) $lista->typ = 'kategorie';
		$this->view->typ = $typ = $lista->typ;
		if($typ == 'kategorie') $this->view->typek = 'kat';
		if($typ == 'producenci') $this->view->typek = 'prod';
		if($typ == 'kategorie') $this->view->typNazwa = 'kategorie';
		if($typ == 'producenci') $this->view->typNazwa = 'producenci';
		
		$lista->kateg = $this->_request->getPost('kat', $this->_request->getParam('kat', 'all'));
		$lista->prod = $this->_request->getPost('prod', $this->_request->getParam('prod', 'all'));

		$this->sorter = $this->_request->getParam('sort', 'odwiedziny');
		$this->sortOrder = $this->_request->getParam('order', 'desc');

		$odwiedziny = new Odwiedziny();
		if(true)
		{
			if($rok == 'all' && !$this->_request->isPost())
			{
				if($typ == 'kategorie')
				{
					$raport = $odwiedziny->wypiszForKategoriaSumaLata($lista->kateg);
					$raport2 = $odwiedziny->wypiszForKategoriaSumaLata2($lista->kateg);
				}
				if($typ == 'producenci')
				{
					$raport = $odwiedziny->wypiszForProducentSumaLata($lista->prod);
					$raport2 = $odwiedziny->wypiszForProducentSumaLata2($lista->prod);
				}
			}
			else
			{
				if($typ == 'kategorie')
				{
					$raport = $odwiedziny->wypiszForKategoriaSumaMiesiace($lista->kateg, $dataOd, $dataDo);
					$raport2 = $odwiedziny->wypiszForKategoriaSumaMiesiace2($lista->kateg, $dataOd, $dataDo);
				}
				if($typ == 'producenci')
				{
					$raport = $odwiedziny->wypiszForProducentSumaMiesiace($lista->prod, $dataOd, $dataDo);
					$raport2 = $odwiedziny->wypiszForProducentSumaMiesiace2($lista->prod, $dataOd, $dataDo);
				}
			}
			$kategorie = new Kategorie();
			for($i=0;$i<count($raport);$i++)
			{
				$rap = $raport[$i]->toArray();
				$rapID = $rap['id'];
				if($typ == 'kategorie')
				{
					$sciezka = $kategorie->get_path($rapID);
					$path = ''; foreach($sciezka as $kat) $path .= $kat['nazwa'].' - ';
					$rap['nazwa'] = $path;
				}
				$rap['odwiedziny2'] = 0;
				$raporty[$rapID] = $rap;
			}
			for($i=0;$i<count($raport2);$i++)
			{
				$rap = $raport2[$i]->toArray();
				$rapID = $rap['id'];
				if($typ == 'kategorie')
				{
					$sciezka = $kategorie->get_path($rapID);
					$path = ''; foreach($sciezka as $kat) $path .= $kat['nazwa'].' - ';
					$rap['nazwa'] = $path;
				}
				$rap['odwiedziny2'] = $rap['odwiedziny'];
				$rap['odwiedziny'] = 0;
				if(isset($raporty[$rapID])) $raporty[$rapID]['odwiedziny2'] = $rap['odwiedziny2'];
				else $raporty[$rapID] = $rap;
			}
			if(@count($raporty) > 0) usort($raporty, array($this, 'cmp')); //die();
			
			if($typ == 'kategorie' && $lista->kateg == 'all')
			{
				$this->view->id = 'all';
				$this->view->nazwa = 'Wszystkie kategorie';
				$lata = $odwiedziny->wypiszLataForAll();
			}
			elseif($typ == 'kategorie' && $lista->kateg == 0)
			{
				$this->view->id = 0;
				$this->view->nazwa = 'Kategorie podstawowe';
				//$lata = $odwiedziny->wypiszLataForRodzic($lista->kateg);
				$lata = $odwiedziny->wypiszLataForAll();
			}
			elseif($typ == 'producenci' && $lista->prod == 0)
			{
				$this->view->id = 0;
				$this->view->nazwa = 'Wszyscy producenci';
				//$lata = $odwiedziny->wypiszLataForProducent($lista->prod);
				$lata = $odwiedziny->wypiszLataForAll();
			}
			else
			{
				if($typ == 'kategorie')
				{
					$kategorie = new Kategorie();
					$kategoria = $kategorie->znajdzIdKat($lista->kateg);
					$this->view->id = $kategoria[0]['id'];
					$sciezka = $kategorie->get_path($lista->kateg);
					$nazwa = ''; foreach($sciezka as $kat) $nazwa .= $kat['nazwa'].' - ';
					$this->view->nazwa = $nazwa;
				}
				if($typ == 'producenci')
				{
					$producenci = new Producent();
					$producenci->id = $lista->prod;
					$producent = $producenci->wypiszPojedynczego();
					$this->view->id = $producent['id'];
					$this->view->nazwa = $producent['nazwa'];
				}
				//$lata = $odwiedziny->wypiszLataForRodzic($lista->kateg);
				$lata = $odwiedziny->wypiszLataForAll();
			}
			$this->view->lata = $lata;
			$this->view->rok = $rok;
			$this->view->dataOd = $dataOd;
			$this->view->dataDo = $dataDo;
		}
	    //var_dump($raport->toArray());
		$this->view->ilosc = @round(count($raporty)/3);
		$this->view->raport = @$raporty;
		//var_dump($raporty);die();
		$first = @reset($raporty);
		$jednostka = @count($raporty) > 0 ? ($first['odwiedziny'] > 0 ? 800/$first['odwiedziny'] : 0) : 0;
		$this->view->jednostka = $jednostka;
		
		$kategoria = new Kategorie();
		$kategoria->showAll(0);
		//var_dump($kategoria->katAll);die();
		$this->view->kategorie = $kategoria->katAll;
		$producenci = new Producent();
		$this->view->producenci = $producenci->wypiszProducentow();
		$this->view->kateg = $lista->kateg;
		$this->view->prod = $lista->prod;
		
		if($typ == 'kategorie')
		{
			$label[1] = 'Odwiedziny produktow w kategorii';
			if(@count($raport2) > 0) $label[2] = 'Odwiedziny kategorii';
		}
		if($typ == 'producenci')
		{
			if($lista->prod == 0)
			{
				$label[1] = 'Odwiedziny produktow producenta';
				if(@count($raport2) > 0) $label[2] = 'Odwiedziny producenta';
			}
			else
			{
				$label[1] = 'Odwiedziny produktow producenta';
				if(@count($raport2) > 0)
				$label[2] = 'Odwiedziny producenta';
			}
		}
		$this->view->label = $label;
	}
	
	public function pojedynczyAction() 
	{
		$this->view->r_lista = 'admin/odwiedziny/pokaz/';
		$this->view->headScript()->appendFile('https://www.google.com/jsapi', 'text/javascript');
		
		$id = $this->_request->getParam('id', 0);//1018
		$kat = $this->_request->getPost('kat', $this->_request->getParam('kat', 'all'));
		$prod = $this->_request->getPost('prod', $this->_request->getParam('prod', 'all'));
		$rok = $this->_request->getParam('rok', date('Y'));
		$odwiedziny = new Odwiedziny();
		$odwiedzinyNow = new Odwiedzinynow();
		
		$lista = new Zend_Session_Namespace('odwiedzinyPojedynczy');
		$typ = $this->_request->getParam('typ');
		if($typ) $lista->typ = $typ;
		if(!isset($lista->typ)) $lista->typ = 'kategorie';
		$typ = $lista->typ;
		$this->view->typ = $typ;
		if($typ == 'kategorie') $this->view->typNazwa = 'kategorie';
		if($typ == 'producenci') $this->view->typNazwa = 'producenci';
		if($id > 0) $this->view->typNazwa = 'produkty';
		
		if($id > 0) $aktualnie = $odwiedzinyNow->wypiszAktualnie($id, 'produkty');
		if($kat > 0) $aktualnie = $odwiedzinyNow->wypiszAktualnie($kat, 'kategorie');
		if($prod > 0) $aktualnie = $odwiedzinyNow->wypiszAktualnie($prod, 'producenci');
		$this->view->aktualnie = null;//@$aktualnie;
		
		if($id > 0)
		{
			if($rok == 'all')
			{
				$dane2 = $odwiedziny->wypiszForProduktLata($id);
			}
			else
			{
				$dane2 = $odwiedziny->wypiszForProduktMiesiace($id, $rok);
			}
			$lata = $odwiedziny->wypiszLataForProdukt($id);
			$produkty = new Produkty();
			$produkty->id = $id;
			$produkt = $produkty->wypiszPojedyncza();
			$this->view->id = $produkt['id'];
			$this->view->nazwa = $produkt['nazwa'];
			$this->view->tryb = 'id';
			$this->view->r_pokaz = $produkt['link'];
		}
		elseif($kat > 0)
		{
			if($rok == 'all')
			{
				$dane2 = $odwiedziny->wypiszForKategoriaLata($kat);
				$dane = $odwiedziny->wypiszForKategoriaLata2($kat);
			}
			else
			{
				$dane2 = $odwiedziny->wypiszForKategoriaMiesiace($kat, $rok);
				$dane = $odwiedziny->wypiszForKategoriaMiesiace2($kat, $rok);
			}
			//$lata = $odwiedziny->wypiszLataForKategoria($kat);
			$lata = $odwiedziny->wypiszLataForAll();
			$kategorie = new Kategorie();
			$kategoria = $kategorie->znajdzIdKat($kat);
			$this->view->id = $kategoria[0]['id'];
			$sciezka = $kategorie->get_path($kat);
			$nazwa = ''; foreach($sciezka as $kateg) $nazwa .= $kateg['nazwa'].' - ';
			$this->view->nazwa = $nazwa;
			$this->view->tryb = 'kat';
			$this->view->r_pokaz = $kategoria[0]['link'];
		}
		elseif($prod > 0)
		{
			if($rok == 'all')
			{
				$dane2 = $odwiedziny->wypiszForProducentLata($prod);
				$dane = $odwiedziny->wypiszFoProducentLata2($prod);
			}
			else
			{
				$dane2 = $odwiedziny->wypiszForProducentMiesiace($prod, $rok);
				$dane = $odwiedziny->wypiszForProducentMiesiace2($prod, $rok);
			}
			//$lata = $odwiedziny->wypiszLataForProducent($prod);
			$lata = $odwiedziny->wypiszLataForAll();
			$producenci = new Producent();
			$producenci->id = $prod;
			$producent = $producenci->wypiszPojedynczego();
			$this->view->id = $producent['id'];
			$this->view->nazwa = $producent['nazwa'];
			$this->view->tryb = 'prod';
		}
		else
		{
			if($rok == 'all')
			{
				$dane2 = $odwiedziny->wypiszForAllLata('produkty');
				$dane = $odwiedziny->wypiszForAllLata($typ);
			}
			else
			{
				$dane2 = $odwiedziny->wypiszForAllMiesiace($rok, 'produkty');
				$dane = $odwiedziny->wypiszForAllMiesiace($rok, $typ);
			}
			$lata = $odwiedziny->wypiszLataForAll();
			$this->view->id = 'all';
			if($typ == 'kategorie') $this->view->nazwa = 'Wszystkie kategorie';
			if($typ == 'producenci') $this->view->nazwa = 'Wszyscy producenci';
			if($typ == 'kategorie') $this->view->tryb = 'kat';
			if($typ == 'producenci') $this->view->tryb = 'prod';
		}
		$this->view->lata = $lata;
		$this->view->rok = $rok;
		
		//$dane = $dane->toArray();
		//if(count($dane) == 1) $dane[] = $dane[0];		
		//var_dump($dane);//die();
		$razem = 0;
		if(@count($dane2) > 0)
		foreach($dane2 as $dana)
		{
			$array[0] = $rok != 'all' ? $this->mce[$dana['czas']] : $dana['czas'];
			$array[1] = '';
			$array[2] = @intval($dana['odwiedziny']);
			$razem += @intval($dana['odwiedziny']);
			if(false && @count($dane) > 0) $array[3] = 0;
			//$ilosc = @count($data[$dana['czas']]);
			$data[$dana['czas']][$array[0]] = $array;
		}
		if(false && @count($dane) > 0)
		foreach($dane as $dana)
		{
			$array[0] = $rok != 'all' ? $this->mce[$dana['czas']] : $dana['czas'];
			$array[1] = '';
			$array[2] = 0;
			$array[3] = @intval($dana['odwiedziny']);
			$razem += @intval($dana['odwiedziny']);
			//$ilosc = @count($data[$dana['czas']]);
			if(isset($data[$dana['czas']][$array[0]]))
			$data[$dana['czas']][$array[0]][3] = $array[3];
			else
			$data[$dana['czas']][$array[0]] = $array;
		}
		//if(count($data) == 1) $data[key(reset($data))] = reset($data);
		@ksort($data);
		$this->view->dane = @$data;
		$this->view->razem = $razem;
		//var_dump($data);die();
		
		$label[1] = 'Miesi�c';
		if($id > 0)
		{
			$label[2] = 'Odwiedziny produktu';
		}
		elseif($typ == 'kategorie')
		{
			$label[2] = 'Odwiedziny produktow w kategorii';
			if(false && @count($dane) > 0) $label[3] = 'Odwiedziny kategorii';
		}
		elseif($typ == 'producenci')
		{
			if($prod == 0)
			{
				$label[2] = 'Odwiedziny produktow producenta';
				if(false && @count($dane) > 0) $label[3] = 'Odwiedziny producenta';
			}
			else
			{
				$label[2] = 'Odwiedziny produktow producenta';
				if(false && @count($dane) > 0)
				$label[3] = 'Odwiedziny producenta';
			}
		}
		$this->view->label = $label;
		
		$kategoria = new Kategorie();
		$kategoria->showAll(0);
		$this->view->kategorie = $kategoria->katAll;
		$producenci = new Producent();
		$this->view->producenci = $producenci->wypiszProducentow();
		$this->view->kateg = $kat;
		$this->view->prod = $prod;
	}
}
?>