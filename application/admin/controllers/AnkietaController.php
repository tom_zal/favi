<?php
class Admin_AnkietaController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		$this->baseUrl = $this->view->baseUrl;
		$this->view->headScript()->appendFile($this->_request->getBaseUrl() . '/public/scripts/admin/functionadmin.js','text/javascript');
		$this->view->headScript()->appendFile($this->_request->getBaseUrl() . '/public/scripts/admin/jqueryui.js','text/javascript');
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin/index/panel/');
	}
	function dodajAction()
	{
		$objAnkieta = new Ankieta();
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa');
			if(!empty($nazwa))
			{
				$dane = array('nazwa' => $nazwa);
				$id = $objAnkieta->dodaj($dane);
				$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/admin/ankieta/edytuj/id/'.$id);
				$this->getResponse()->setHeader('Refresh', '0; URL='.$this->baseUrl.'/admin/ankieta/edytuj/id/'.$id);
			}
			else
			{
				$this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>';
				$this->view->blad_edycji .= '<div id="lista_bledow">Pole pytanie ankieta jest wymagane.</div>';
			}
		}
	}
	function edytujAction()
	{
		$objAnkieta = new Ankieta();
		$objAnkietaodpowiedzi = new Ankietaodpowiedzi();
		$id = $this->_request->getParam('id', 0);
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa');
			if(!empty($nazwa))
			{
				$dane = array('nazwa' => $nazwa);
				$objAnkieta->id = $id;
				$objAnkieta->edytuj($dane);
				$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/admin/ankieta/edytuj/id/'.$id);
				$this->getResponse()->setHeader('Refresh', '1; URL='.$this->baseUrl.'/admin/ankieta/edytuj/id/'.$id);
			}
			else
			{
				$this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>';
				$this->view->blad_edycji .= '<div id="lista_bledow">Pole pytanie ankieta jest wymagane.</div>';
			}
		}

		$wyczysc = $this->_request->getParam('wyczysc');
		if(isset($wyczysc) && $wyczysc == 1)
		{
			$objAnkietaodpowiedzi->id = $id;
			$dane['glosy'] = 0;
			$objAnkietaodpowiedzi->usunGlosy($dane);
			$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/ankieta/edytuj/id/'.$id);
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->baseUrl.'/admin/ankieta/edytuj/id/'.$id);
		}

		$objAnkieta->id = $id;
		$this->view->pojedynczy = $objAnkieta->wypiszPojedynczy();
		$this->view->tytul = $this->view->pojedynczy->nazwa;

		$objAnkietaodpowiedzi->id = $id;
		$this->view->odpowiedzi = $objAnkietaodpowiedzi->wypiszWszystko();

	}
	function listaAction()
	{
		$objAnkieta = new Ankieta;
		if ($this->_request->getParam('delid') > 0)
		{
			$filter = new Zend_Filter_StripTags();
			$id = $filter->filter($this->_request->getParam('delid'));

			$objAnkieta->id = $id;
			$objAnkieta->usun();

			$objAnkietaOdpowiedzi = new Ankietaodpowiedzi;
			$objAnkietaOdpowiedzi->id = $id;
			$objAnkietaOdpowiedzi->usunPrzypisane();

			$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
		}
		(int) $widoczny = $this->_request->getPost('widoczny');
		if($this->_request->isPost())
		{
			if(!empty($widoczny))
			{
				$objAnkieta->id = $widoczny;
				$objAnkieta->ustawWidoczny();
				$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			}
			else
			{
				$this->view->blad_edycji = '<div class="k_blad">Wybierz ankietę do wyświetlenia.</div>';
			}
		}

		$ustawienia = $this->_request->getParam('ustaw');

		if(isset($ustawienia))
		{
			if($ustawienia == 1)
				$dane['widoczny'] = 'T';
			else
				$dane['widoczny'] = 'N';

			$objAnkieta->id = 0;
			$objAnkieta->edytuj($dane);
			$this->_redirect('/admin/ankieta/lista/');
		}

		$this->view->ankietau = $objAnkieta->wypiszUstawienia();

		$all = $objAnkieta->wypiszWszystko();
		$this->view->realizacje = $all;
	}
	function odpowiedziAction()
	{
		$id = $this->_request->getParam('id', 0);
		$objAnkieta = new Ankieta();
		$objAnkietaodpowiedzi = new Ankietaodpowiedzi();
		
		$this->view->idan = $id;
		$objAnkieta->id = $id;
		$this->view->pojedynczy = $objAnkieta->wypiszPojedynczy();
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa');
			$nazwa = array_filter($nazwa);
			$nazwa = array_merge($nazwa);

			if(isset($nazwa) && !empty($nazwa))
			{
				for($i=0; $i<count($nazwa); $i++)
				{
					$dane['nazwaod'] = $nazwa[$i];
					$dane['idan'] = $id;
					$objAnkietaodpowiedzi->dodaj($dane);
				}
				$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/admin/ankieta/edytuj/id/'.$id);
				$this->getResponse()->setHeader('Refresh', '1; URL='.$this->baseUrl.'/admin/ankieta/edytuj/id/'.$id);
			}
			else
			{
				$this->view->blad_edycji = '<div class="k_blad">Pole odpowiedź jest wymagane.</div>';
			}
		}
	}
	function odpowiedzAction()
	{
		$idan = $this->_request->getParam('idan', 0);
		$id = $this->_request->getParam('id', 0);

		$objAnkieta = new Ankieta();
		$objAnkietaodpowiedzi = new Ankietaodpowiedzi();

		$this->view->idan = $idan;
		$objAnkieta->id = $idan;
		$this->view->pojedynczy = $objAnkieta->wypiszPojedynczy();
		
		$objAnkietaodpowiedzi->id = $id;
		$this->view->pojedynczyo = $objAnkietaodpowiedzi->wypiszPojedynczy();

		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa');

			if(isset($nazwa) && !empty($nazwa))
			{
				$dane['nazwaod'] = $nazwa;
				$objAnkietaodpowiedzi->id = $id;
				$objAnkietaodpowiedzi->edytuj($dane);
				$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
				$this->_redirect('/admin/ankieta/edytuj/id/'.$idan);
				$this->getResponse()->setHeader('Refresh', '1; URL='.$this->baseUrl.'/admin/ankieta/edytuj/id/'.$idan);
			}
			else
			{
				$this->view->blad_edycji = '<div class="k_blad">Pole odpowiedź jest wymagane.</div>';
			}
		}
	}
	function odpowiedzusunAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$idan = $this->_request->getParam('idan', 0);
		$id = $this->_request->getParam('id', 0);

		$objAnkietaodpowiedzi = new Ankietaodpowiedzi();
		$objAnkietaodpowiedzi->id = $id;
		$objAnkietaodpowiedzi->usun();
		//echo $this->baseUrl.'/admin/ankieta/edytuj/id/'.$idan;
		$this->_redirect('/admin/ankieta/edytuj/id/'.$idan);
	}
	function wynikiAction()
	{
		$id = $this->_request->getParam('id', 0);
		$objAnkietaodpowiedzi = new Ankietaodpowiedzi();
		$objAnkieta = new Ankieta();

		$objAnkieta->id = $id;
		$this->view->pojedynczy = $objAnkieta->wypiszPojedynczy();

		$objAnkietaodpowiedzi->id = $id;
		$tabraport = $objAnkietaodpowiedzi->wypiszWszystko();
		$this->view->pojedynczyo = $tabraport;

		$tabtemp = array();
		$tabpom = array();
		$raport = array();

		for($i=0; $i<count($tabraport); $i++)
		{
			if(in_array($tabraport[$i]['id'], $tabpom))
			{
				$tabtemp[$tabraport[$i]['id']]['glosy'] += $tabraport[$i]['glosy'];
			}
			else
			{
				$tabtemp[$tabraport[$i]['id']]['nazwaod'] = $tabraport[$i]['nazwaod'];
				$tabtemp[$tabraport[$i]['id']]['glosy'] = $tabraport[$i]['glosy'];

				$tabpom[] .= $tabraport[$i]['id'];
			}
		}

		$j = 0;
		foreach($tabtemp as $key => $value)
		{
			$raport[$j]['id'] = $key;
			$raport[$j]['nazwa'] = $value['nazwaod'];
			$raport[$j]['ilosc'] = $value['glosy'];
			$j++;
		}

		foreach ($raport as $key => $value)
		{
			$tabpom[$key] = $value['ilosc'];
		}

		array_multisort($tabpom, SORT_DESC, $raport);

		$this->view->ilosc = round(count($raport)/3);
		$this->view->raport = $raport;

		if(!empty($raport))
		{
			if($raport[0]['ilosc'] != 0)
				$jednostka = 685/$raport[0]['ilosc'];
			else
				$jednostka = 0;
			$this->view->jednostka = $jednostka;
		}
	}
}
?>