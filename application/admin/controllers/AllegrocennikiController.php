<?php
class Admin_AllegrocennikiController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		//echo $this->view->baseUrl;
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}	
	function dodajAction()
	{
		//$this->view->r_zapisz = 'jQuery_zapisz_btn';
		$cenniki = new Allegrocenniki();
		
		$del = intval($this->_request->getParam('delid', 0));
		if($del > 0)
		{
			$cenniki->id = $del;
			$cenniki->usunID();
			//$this->view->blad_edycji = '<div class="k_ok">Wybrany cennik został usunięty.</div>';
			$this->_redirect('/admin/allegrocenniki/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/allegrocenniki/dodaj/');
		}
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa');
			$koszty = @serialize($this->_request->getPost('koszty'));
			$opcje_przesylki = $this->_request->getPost('opcje_przesylki');
			if($opcje_przesylki == null) $opcje_przesylki = 0;
			else 
			{
				$suma = 0;
				foreach($opcje_przesylki as $opcja) $suma += $opcja;
				$opcje_przesylki = $suma;
			}
			$dane = array('nazwa' => $nazwa, 'koszty' => $koszty, 'opcje_przesylki' => $opcje_przesylki);
			$cenniki->dodaj($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			if(@empty($this->view->blad_edycji)) $this->_redirect('/admin/allegrocenniki/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/allegrocenniki/dodaj/');
		}
		
		$this->view->cenniki = $cenniki->wypisz();
	}	
	function edytujAction()
	{
		$this->view->r_zapisz = 'jQuery_zapisz_btn';
		$this->view->r_lista = 'admin/allegrocenniki/dodaj/';
		$id = intval($this->_request->getParam('id', 0));
		$this->view->r_usun = 'admin/allegrocenniki/dodaj/delid/'.$id;
		$cenniki = new Allegrocenniki();
		if($this->_request->isPost())
		{
			$id = $this->_request->getPost('id');
			$nazwa = $this->_request->getPost('nazwa');
			$koszty = @serialize($this->_request->getPost('koszty'));
			$opcje_przesylki = $this->_request->getPost('opcje_przesylki');
			if($opcje_przesylki == null) $opcje_przesylki = 0;
			else 
			{
				$suma = 0;
				foreach($opcje_przesylki as $opcja) $suma += $opcja;
				$opcje_przesylki = $suma;
			}
			$dane = array('nazwa' => $nazwa, 'koszty' => $koszty, 'opcje_przesylki' => $opcje_przesylki);
			$cenniki->id = $id;
			$cenniki->edytuj($dane);
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			if(@empty($this->view->blad_edycji)) $this->_redirect('/admin/allegrocenniki/dodaj');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/allegrocenniki/dodaj/');
		}		
		$cenniki->id = $id;
		$this->view->cenniki = $cenniki->wypisz();
		$this->view->pojedynczy = $cenniki->wypiszPojedynczego();
		$this->view->koszty = @unserialize($this->view->pojedynczy['koszty']);
		$this->view->tytul = $this->view->pojedynczy->nazwa;
		$opcje_przesylki = $this->view->pojedynczy['opcje_przesylki'];
		for($i = 1; $i <= 4; $i *= 2) $opcje[$i] = $opcje_przesylki & $i;
		$this->view->opcje_przesylki = $opcje;
	}
}
?>