<?php 
class Admin_StronyController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		$this->view->headLink()->appendStylesheet($this->_request->getBaseUrl().'/public/styles/kategorie.css');		
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}
	
	public function listaAction()
	{
		$lista = new Strony();
		
		// usuwa kategorie
		$parent = $this->_request->getParam('id');
		$this->view->id = $parent;
		if($this->_request->getParam('del')>0)
        {
			$lista->kasuj($this->_request->getParam('del'));
			$this->_redirect('/admin/strony/lista/id/'.$parent);
		}
		// zmienia pozycje kategrii
		if($this->_request->isPost())
        {
			$lista->changePozycja($this->_request->getPost('id'), $this->_request->getPost('pozycja'));
		}
		
		// przekazujemy sciezke bazową dla linków i obrazków
		$lista->link = $this->_request->getBaseUrl();
		$lista->display_children_admin($parent,0);
		$this->view->kategorie = $lista->menue;		
	}
	
	public function dodajAction()
    {
		$nowa = new Strony();
		if($this->_request->getParam('id') > 0)
        {
			$id = $this->_request->getParam('id');
		}
        else
        {
			$id = 0;
		}
		
		if($this->_request->getParam('id') > 0)
		{
			$path = $nowa->get_path($this->_request->getParam('id'));
			$this->view->path = $path;
		}
		if($this->_request->isPost())
        {
			$stringValidate = new Zend_Validate; 
 			$stringValidate->addValidator(new Zend_Validate_StringLength(1, null))
							->addValidator(new Zend_Validate_NotEmpty());
 			
 			if($stringValidate->isValid($this->_request->getPost('nazwa')))
            {
                $dane['nazwa'] = $this->_request->getPost('nazwa');
				$dane['tekst'] = $this->_request->getPost('tekst');
                $nowa->dodaj_nowa($id,$dane);
				
				$this->view->error = '<div class="k_ok">Nowa kategoria została dodana</div>';
                $this->_redirect('/admin/strony/lista/id/'.$id);
 			}
			else
			{
 				$this->view->error = '<div class="k_blad">Pole nazwa jest wymagane </div>';
 			}
		}
        $this->view->edytor = $this->fcKeditor('tekst',"",600);
	}
	 
	public function edytujAction()
	{
		$nowa = new Strony;
		if($this->_request->getParam('id') > 0)
		{
			if($this->_request->isPost())
			{
				$stringValidate = new Zend_Validate; 
 				$stringValidate->addValidator(new Zend_Validate_StringLength(1, null))->addValidator(new Zend_Validate_NotEmpty());
				
 				if($stringValidate->isValid($this->_request->getPost('nazwa')))
				{
					$dane['nazwa'] = $this->_request->getPost('nazwa');
					$dane['tekst'] = $this->_request->getPost('tekst');

					$nowa->edytuj($this->_request->getParam('id'), $dane);					
					//$this->view->error = '<div class="k_ok">Edycja przebiegła pomyślnie</div>';
	 			}
				else
				{
	 				$this->view->error = '<div class="k_blad">Pole nazwa jest wymagane </div>';
	 			}
			}
            $data = $nowa->showWybranaKategoria($this->_request->getParam('id'));
			$this->view->kategoria = $data;
            $this->view->edytor = $this->fcKeditor('tekst',stripslashes($data['tekst']),600);			
		}
		else
		{
			$this->view->error = '<div class="k_blad">Nie wybrałeś kategorii do edycji</div>';
		}
	}
}
?>