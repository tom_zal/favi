<?php
class Admin_WagiController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}	
	function dodajAction()
	{
		$id = $this->_request->getParam('id', 0);
		$wagi = new Wagi();
		$wagi->id = $id;
		$this->view->pojedynczy = $wagi->wypiszID();
		
		$del = $this->_request->getParam('delid', 0);
		if($del!=0)
		{
			$wagi->id = $del;
			$wagi->usun();
			//$this->view->blad_edycji = '<div class="k_ok">Wybrany rozmiar został usunięty.</div>';
			$this->_redirect('/admin/wagi/dodaj/');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/wagi/dodaj');
		}
		if($this->_request->isPost())
		{
			$nazwa = $this->_request->getPost('nazwa', '');
			$od = $this->_request->getPost('od', 0);
			$do = $this->_request->getPost('do', 0);
			$koszt = $this->_request->getPost('koszt', 0);
			$dane = array('nazwa' => $nazwa, 'od' => $od, 'do' => $do, 'koszt' => $koszt);
			$id = $wagi->dodaj($dane);
			
			if($this->obConfig->dostawyOdWagi)
			{
				$dostawy = $this->_request->getPost('dostawa');
				$wagiDostawy = new WagiDostawy();
				//$wagiDostawy->usunWaga($id);
				if(count($dostawy) > 0)
				foreach($dostawy as $dostawa)
				$wagiDostawy->dodaj(array('id_waga' => $id, 'id_dostawa' => $dostawa));
			}
			
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/wagi/dodaj/');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/wagi/dodaj');
		}
		
		$this->view->wagi = $wagi->wypisz();
		
		if($this->obConfig->dostawyOdWagi)
		{
			$platnosci = new Platnosci();
			$this->view->dostawyAll = $platnosci->selectdostawyAll();
			
			if(count($this->view->wagi) > 0)
			foreach($this->view->wagi as $waga)
			$dostawyDlaWagi[$waga['id']] = $wagi->WypiszDostawyDlaWagi($waga['id']);
			$this->view->dostawyDlaWagi = @$dostawyDlaWagi;
			//var_dump($this->view->dostawyDlaWagi);
		}
	}	
	function edytujAction()
	{
		$id = $this->_request->getParam('id', 0);
		$wagi = new Wagi();
		$wagi->id = $id;
		$this->view->pojedynczy = $wagi->wypiszID();
		
		if($this->_request->isPost())
		{
			$id = $this->_request->getPost('id', 0);
			$nazwa = $this->_request->getPost('nazwa', '');
			$od = $this->_request->getPost('od', 0);
			$do = $this->_request->getPost('do', 0);
			$koszt = $this->_request->getPost('koszt', 0);
			$dane = array('nazwa' => $nazwa, 'od' => $od, 'do' => $do, 'koszt' => $koszt);
			$wagi->id = $id;
			$wagi->edytuj($dane);
			
			if($this->obConfig->dostawyOdWagi)
			{
				$dostawy = $this->_request->getPost('dostawa');
				$wagiDostawy = new WagiDostawy();
				$wagiDostawy->usunWaga($id);
				if(count($dostawy) > 0)
				foreach($dostawy as $dostawa)
				$wagiDostawy->dodaj(array('id_waga' => $id, 'id_dostawa' => $dostawa));
			}
			
			//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
			$this->_redirect('/admin/wagi/dodaj/');
			$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/wagi/dodaj');
		}
		
		$this->view->wagi = $wagi->wypisz();
		
		if($this->obConfig->dostawyOdWagi)
		{
			$platnosci = new Platnosci();
			$this->view->dostawyAll = $platnosci->selectdostawyAll();
			
			if(count($this->view->wagi) > 0)
			foreach($this->view->wagi as $waga)
			$dostawyDlaWagi[$waga['id']] = $wagi->WypiszDostawyDlaWagi($waga['id']);
			$this->view->dostawyDlaWagi = @$dostawyDlaWagi;
			//var_dump($this->view->dostawyDlaWagi);
		}
	}
}
?>