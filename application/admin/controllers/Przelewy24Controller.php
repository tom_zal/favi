<?php
class Admin_Przelewy24Controller extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		$this->view->title = 'Przelewy24';
	}
	
	function __call($method, $args)
	{
		$this->_redirect($this->view->baseUrl.'/admin');
	}
	
	function indexAction()
	{
		$platnosci = new Przelewy24();
		if($this->_request->isPost())
		{
			$platnosci->_update($this->_getParam('post'));
		}
		$this->view->dane = $platnosci->_get();
	}
}
?>