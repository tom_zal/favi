<?php
class Admin_ImportController extends Ogolny_Controller_Admin
{
    public $bledy;

    public function init()
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        $this->bledy = '';
		$this->view->folder = $this->folder = 'admin/zip';
		$this->view->rozsz2 = array('name' => ';*.zip', 'desc' => ' i ZIP (*.zip)');
        if (!ini_get('safe_mode'))
		{
            set_time_limit(0);
        }
    }

    function __call($method, $args)
	{
        $this->_redirect('/admin/');
    }
	
	function listaAction()
	{
        $import = new Import();
		
		$szukanie = new Zend_Session_Namespace('szukanieImportAdmin');
		if($this->_request->getParam('reset')) 
		{
			$szukanie->tryb = '';
			$szukanie->rodzaj = '';
			$szukanie->typ = '';
			$szukanie->akcja = 'all';
			$szukanie->dataOd = '1900-01-01';
			$szukanie->dataDo = date('Y-m-d');
			$szukanie->od = 0;
			$szukanie->ile = 20;
			$szukanie->ostatni = true;
        }
        if($this->_request->getPost('szukaj'))
		{
			$szukanie->tryb = $this->_request->getPost('tryb', '');
			$szukanie->rodzaj = $this->_request->getPost('rodzaj', '');
			$szukanie->typ = $this->_request->getPost('typ', '');
			$szukanie->akcja = $this->_request->getPost('akcja', 'all');
			if(false)
			{
				$od = $this->_request->getPost('dataOd');
				$szukanie->dataOd = $od['rok'].'-'.$od['mc'].'-'.$od['day'];
				$do = $this->_request->getPost('dataDo');
				$szukanie->dataDo = $do['rok'].'-'.$do['mc'].'-'.$do['day'];
			}
			$szukanie->od = 0;
			$szukanie->ile = 20;
			$szukanie->ostatni = true;
        }
		if($this->_request->getParam('ile'))
		{
			$szukanie->ile = $this->_request->getParam('ile');
			$szukanie->od = 0;
		}
		if($this->_request->getParam('od') != '')
		{
			$szukanie->od = $this->_request->getParam('od');
		}
		if($this->_request->getParam('akcja'))
		{
			$szukanie->akcja = $this->_request->getParam('akcja');
			$this->_redirect('admin/import/lista');
		}
		if (!isset($szukanie->tryb)) $szukanie->tryb = '';
		if (!isset($szukanie->rodzaj)) $szukanie->rodzaj = '';
		if (!isset($szukanie->typ)) $szukanie->typ = '';
		if (!isset($szukanie->akcja)) $szukanie->akcja = 'all';
		//if (!isset($szukanie->dataOd)) $szukanie->dataOd = '1900-01-01';
		//if (!isset($szukanie->dataDo)) $szukanie->dataDo = date('Y-m-d');		
		if (!isset($szukanie->ile)) $szukanie->ile = 20;
		if (!isset($szukanie->od)) $szukanie->od = 0;
		if (!isset($szukanie->ostatni))$szukanie->ostatni = true;
		//$szukanie->ile = 1;

		$ile = $szukanie->ile;
		$od = floor($szukanie->od / $ile);
		$od = $szukanie->od;
		
		$sortowanie = new Zend_Session_Namespace('sortowanieImportAdmin');
		if($this->_request->getParam('reset')) 
		{
			$sortowanie->sort = 'czas';
			$sortowanie->order = 'desc';
			$this->_redirect('admin/import/lista');
		}
		if($this->_request->getParam('sort'))
		{
			$sortowanie->sort = $this->_request->getParam('sort', 'czas');
			$sortowanie->order = $this->_request->getParam('order', 'desc');
		}
		if(!isset($sortowanie->sort))
		{
			$sortowanie->sort = 'czas';
			$sortowanie->order = 'desc';
		}
		
		if($this->_request->isPost()) $this->_redirect('admin/import/lista');

		$ilosc = $import->wypiszKontr(true, $od, $ile, $sortowanie, $szukanie);
		$all = $import->wypiszKontr(false, $od, $ile, $sortowanie, $szukanie);
		//var_dump($all);die();

		$this->view->ilosc = $ilosc;
		$this->view->szukanie = $szukanie;
		$this->view->sortowanie = $sortowanie;
		$this->view->od = $od;
		$this->view->strona = $szukanie->od;
		$this->view->ile = $ile;
		$this->view->strony = ceil($ilosc / $ile);
		$this->view->link = 'admin/import/lista';
		$this->view->order = $sortowanie->order == 'asc' ? 'desc' : 'asc';
		
		if(false)
		for($i=0;$i<count($all);$i++)
		{
			$ids[] = $all[$i]['id'];
		}

		if(false)
		for($i=0;$i<count($all);$i++)
		{
			$all[$i]['rabaty'] = @$rabatyAll[$all[$i]['id']];
		}
		//var_dump($all);
		$this->view->importy = $all;
    }
	
	public function importxlsAction()
	{
		$ajax = true;
		$tryb = $this->_request->getParam('tryb', 'panel');
		$this->view->tryb = $tryb;
		$import = new Import($tryb);
		$this->view->rozsz = array('name' => '*.xls;*.xlsx', 'desc' => 'Pliki Excela (*.xls, *.xlsx)');
		$this->view->czyPrzyklad = $import->generujPrzyklad(true);
		if($this->_request->getParam('test'))
		{
			$this->view->przyklad = $import->generujPrzyklad(false);
			if(!empty($this->view->przyklad))
			{
				$this->view->blad_edycji = '<div class="k_ok"><a href="'.$this->view->baseUrl.'/public/'.$this->view->przyklad.'">';
				$this->view->blad_edycji .= 'Pobierz przykładowy plik XLS</a></div>';
			}
		}
        if($this->_request->isPost())
		{
            $plik = $this->_request->getPost('plik');
			$file = $this->_request->getPost('file');
			$nowe = $this->_request->getPost('nowe');
            if(isset($plik))
			{
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->setDestination($this->ImageDir->ZipFileDir);
                //$adapter->addValidator('Extension', false, array('extension1' => 'zip,xls,xlsx', 'case' => false))
                        //->addValidator('Count', false, 1);
                if(!empty($file) || $adapter->receive())
				{
                    //$filename = $adapter->getFileName();
					$filename = $this->ImageDir->ZipFileDir.''.$file;
					$filename = str_replace('\\', '/', $filename);
					$fileinfo = pathinfo($filename);
					$dirname = $fileinfo['dirname'];//$this->ImageDir->ZipFileDir
					$name = $fileinfo['filename'];//substr($filename, strlen($this->ImageDir->ZipFileDir), -4);
					$basename = $fileinfo['basename'];//basename($filename);
					$extension = $fileinfo['extension'];
                    $nameZip = $name.'.zip';
					$nameXls = $name.'.'.$extension;
					$filenameXls = $this->ImageDir->ZipExtDir.''.$nameXls;
					//var_dump($fileinfo);
					if($extension == 'zip') // strpos($filename, '.zip') > 0
					{
						$nameXls = $name.'.xls';
						if(@$fileinfo['extension'] == 'ZIP') $nameXls = $name.'.XLS';
						$filenameXls = $this->ImageDir->ZipExtDir.''.$nameXls;
						$import->wypakuj($this->ImageDir->ZipExtDir, $filename, $nameXls);
						if(!empty($import->bledy)) // $this->bledy .= $import->bledy;
						{
							$import->bledy = '';
							$nameXls = $name.'.xlsx';
							if(@$fileinfo['extension'] == 'ZIP') $nameXls = $name.'.XLSX';
							$filenameXls = $this->ImageDir->ZipExtDir.''.$nameXls;
							$import->wypakuj($this->ImageDir->ZipExtDir, $filename, $nameXls);
							if(!empty($import->bledy)) $this->bledy .= $import->bledy;
						}
						//var_dump($x);
					}
					else @rename($filename, $filenameXls);
                    
					if(file_exists($filenameXls))
					{
						if(!$ajax)
						{
							$dane = $import->wczytajDaneXls($filenameXls);
							//var_dump($dane);die();
							if(count($dane) > 0)
							{
								$import->wgrajNoweProdukty($filename, $dane, 0, 0);
								if($nowe) $import->usunStareProdukty($dane);
								$this->bledy .= $import->bledy;
							}
						}
						else
						{
							$this->view->file = $nameXls;
							$this->view->nowe = $nowe;
							$this->view->opisy = $import->opisy;
						}
                    }
					else
					{
						if(@empty($this->bledy))
                        $this->bledy .= 'Zła nazwa pliku lub brak pliku '.$filename.'<br>';
                    }
                }
				else
				{
                    $this->bledy .= 'Zły typ pliku. Akceptowalny typ to XLS (*.xls,*.xlsx) lub archiwum ZIP (*.zip) <br>';
                }
                if(strlen($this->bledy) > 1)
				{
                    $this->view->blad_edycji = '<div class="k_uwaga">Wynik edycji danych.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $this->bledy . '</div>';
                }
				else
				{
					if(!$ajax)
                    $this->view->blad_edycji = '<div class="k_ok">Operacja przebiegła pomyślnie.</div>';
					else
					$this->view->blad_edycji = '<div class="k_uwaga">Trwa wykonywanie importu.</div>';
                }
            }
        }
    }
	
	public function exportxlsAction()
	{
		$ajax = true;
        $this->view->linkxls = '';
		$tryb = $this->_request->getParam('tryb', 'panel');
		$this->view->tryb = $tryb;
		$import = new Import($tryb);
		$this->view->opisy = $import->opisy;
        if($this->_request->isPost())
		{
            $filter = new Zend_Filter_StripTags();
            $plik = $filter->filter($this->_request->getPost('nazwa'));
            if(strlen($plik) > 1)
			{
                $filename = 'admin/bazy/' . $plik . '-' . date('Y-m-d') . '.xls';
                $filenamezip = 'admin/bazy/' . $plik . '-' . date('Y-m-d') . '.zip';
				
				$sort = $this->_request->getPost('sort');

				if(!$ajax)
				{
					$import = new Import($tryb);
					//$workbook = $import->getProduktyWorkbook();
					$workbook = new Spreadsheet_Excel_Writer($filename);
					$workbook->setVersion(8);
					$workbook->setCountry(48);

					$format_bold = & $workbook->addFormat();
					$format_bold->setBold();
					$format_bold->setAlign('center');
					$format_bold->setHAlign('center');
					$format_bold->setTextWrap();
					$format_center = & $workbook->addFormat();
					$format_center->setAlign('center');
					$format_left = & $workbook->addFormat();
					$format_left->setAlign('left');
					$format_right = & $workbook->addFormat();
					$format_right->setAlign('right');

					$worksheet = & $workbook->addWorksheet();
					$worksheet->setInputEncoding('UTF-8');
					
					$worksheet->setColumn(0, 0, 5);
					$worksheet->setColumn(1, 1, 50);
					$worksheet->setColumn(2, 30, 12);

					$i = 0;
					$worksheet->write(0, $i++, 'LP', $format_bold);
					foreach($import->opisy as $co => $nazwa)
					if(!isset($import->czyPole[$co]) || $import->czyPole[$co])
					$worksheet->write(0, $i++, ucfirst($nazwa), $format_bold);

					$produkty = new Produkty();
					$zawartosc = $produkty->wypiszProduktyEksport($sort);

					if(count($zawartosc) > 0)
					{
						//$zawartosc = $zawartosc->toArray();
						for($i = 0; $i < count($zawartosc); $i++)
						{
							$j = 0;
							$worksheet->write($i + 1, $j++, $i + 1, $format_center);
							foreach($import->opisy as $co => $nazwa)
							if(!isset($import->czyPole[$co]) || $import->czyPole[$co])
							{
								$format = $format_center;
								if(in_array($co, $import->leftAligns)) $format = $format_left;
								if(in_array($co, $import->rightAligns)) $format = $format_right;
								
								if(in_array($co, $import->strings))
								$worksheet->writeString($i + 1, $j++, $zawartosc[$i][$co], $format);
								else
								$worksheet->write($i + 1, $j++, $zawartosc[$i][$co], $format);
							}
						}
					}

					//$workbook->send('testxls.xls');
					$workbook->close();

					if(file_exists($filename))
					{
						$bledy = '';//$import->dodajDoArchiwum($plik, $zawartosc);
						//var_dump($bledy);
						if (strlen($bledy) > 0)
						{
							$this->view->blad_edycji = '<div class="k_blad">Błąd podczas exportu bazy.</div>';
							$this->view->lista_bledow = '<div id="lista_bledow">' . $bledy . '</div>';
						}
						else
						{
							$this->view->blad_edycji = '<div class="k_ok">Export bazy przebiegł pomyślnie.
							<a href="'.$this->view->baseUrl.'/public/'.$filename.'">Link do pliku zip</a></div>';
						}
					}
					else
					{
						$this->view->blad_edycji = '<div class="k_blad">Błąd podczas exportu bazy.</div>';
						$this->view->lista_bledow = '<div id="lista_bledow">
							Nie udało się stworzyć pliku '.$plik.'-'.date('Y-m-d').'.xls.</div>';
					}
				}
				else
				{
					$this->view->file = $plik;
					$this->view->sort = $sort;					
					$produkty = new Produkty();
					$this->view->count = $produkty->iloscProduktowAll();
					$this->view->blad_edycji = '<div class="k_uwaga">Trwa wykonywanie exportu.</div>';
				}
            }
			else
			{
                $this->view->blad_edycji = '<div class="k_blad">Błąd podczas exportu bazy.</div>';
                $this->view->lista_bledow = '<div id="lista_bledow">Zbyt krótka nazwa pliku lub brak nazwy.</div>';
            }
        }
    }
	
	public function exportxlsklienciAction()
	{
		$ajax = true;
        $this->view->linkxls = '';
		$tryb = $this->_request->getParam('tryb', 'panel');
		$this->view->tryb = $tryb;
		$import = new Import($tryb);
		$this->view->opisy = $import->opisy;
        if($this->_request->isPost())
		{
            $filter = new Zend_Filter_StripTags();
            $plik = $filter->filter($this->_request->getPost('nazwa'));
            if(strlen($plik) > 1)
			{
                $filename = 'admin/bazy/' . $plik . '-' . date('Y-m-d') . '.xls';
                $filenamezip = 'admin/bazy/' . $plik . '-' . date('Y-m-d') . '.zip';
				
				$sort = $this->_request->getPost('sort');
				if(true)
				{
					$this->view->file = $plik;
					$this->view->sort = $sort;					
					$klienci = new Kontrahenci();
					$this->view->count = $klienci->iloscKlientowAll();
					$this->view->blad_edycji = '<div class="k_uwaga">Trwa wykonywanie exportu.</div>';
				}
            }
			else
			{
                $this->view->blad_edycji = '<div class="k_blad">Błąd podczas exportu bazy.</div>';
                $this->view->lista_bledow = '<div id="lista_bledow">Zbyt krótka nazwa pliku lub brak nazwy.</div>';
            }
        }
    }
	
	public function exportxlsallAction() 
	{
		$ajax = false;
		$import = new Import();
        $this->view->linkxls = '';
        if ($this->_request->isPost() || $this->_request->getParam('nazwa'))
		{
            $filter = new Zend_Filter_StripTags();
            $nazwa = $filter->filter($this->_request->getPost('nazwa', $this->_request->getParam('nazwa')));
            if (strlen($nazwa) > 1) 
			{
                $filenamezip = 'admin/bazy/'.$nazwa.'-'.date('Y-m-d').'.zip';
				@mkdir('admin/bazy/'.$nazwa.'-'.date('Y-m-d'));
		
				$zip = new ZipArchive();				
				if($zip->open($filenamezip, ZIPARCHIVE::CREATE) !== TRUE) 
				{
					$this->view->blad_edycji = '<div class="k_blad">Błąd podczas exportu bazy.</div>';
					$this->view->lista_bledow = '<div id="lista_bledow">Nie można utworzyć archiwum zip</div>';
					break;
				}
				
				$tabele = $import->getTabele();
				if(count($tabele) > 0)
				foreach($tabele as $tabela)
				{
					if(in_array(strtolower($tabela['Name']), $import->notAllowed)) continue;
					
					$xls = $nazwa.'-'.date('Y-m-d').'-'.$tabela['Name'].'.xls';
					$filenamexls = 'admin/bazy/'.$nazwa.'-'.date('Y-m-d').'/'.$xls;					
					
					$workbook = new Spreadsheet_Excel_Writer($filenamexls);
					$workbook->setVersion(8);
					$workbook->setCountry(48);

					$format_bold = & $workbook->addFormat();
					$format_bold->setBold();
					
					$worksheet = & $workbook->addWorksheet();
					$worksheet->setInputEncoding('UTF-8');
					
					$kolumny = $import->getKolumny($tabela['Name']);
					for($j = 0; $j < count($kolumny); $j++)
					$worksheet->write(0, $j, $kolumny[$j]['Field'], $format_bold);

				    $wiersze = $import->getWiersze($tabela['Name']);
					for($i = 0; $i < count($wiersze); $i++)
					{
						for($j = 0; $j < count($kolumny); $j++)
						{
							if(strpos($kolumny[$j]['Type'], 'char') !== false
							|| strpos($kolumny[$j]['Type'], 'text') !== false)
							{
								$value = substr($wiersze[$i][$kolumny[$j]['Field']], 0, 2000);
								$worksheet->writeString($i + 1, $j, $value);
							}
							else
							$worksheet->write($i + 1, $j, $wiersze[$i][$kolumny[$j]['Field']]);
						}
						//break;
					}
					//$workbook->send('testxls.xls');
					$workbook->close();

					if(file_exists($filenamexls))
					{
						if($zip->addFile($filenamexls, $xls) !== TRUE) 
						{							
							$this->view->blad_edycji = '<div class="k_blad">Błąd podczas exportu bazy.</div>';
							$this->view->lista_bledow = '<div id="lista_bledow">Nie można dodać do archiwum</div>';
						} 
						else 
						{
							$this->view->blad_edycji = '<div class="k_ok">Export bazy przebiegł pomyślnie. <a href="'.$this->view->baseUrl.'/public/'.$filenamezip.'">Link do pliku zip</a></div>';
						}
					}
					else 
					{
						$bledy = 'Nie udało się stworzyć pliku '.$filenamexls.'<br>';
						$this->view->blad_edycji = '<div class="k_blad">Błąd podczas exportu bazy.</div>';
						$this->view->lista_bledow = '<div id="lista_bledow">'.$bledy.'</div>';
					}
				}
				$zip->close();
            } 
			else 
			{
                $this->view->blad_edycji = '<div class="k_blad">Błąd podczas exportu bazy.</div>';
                $this->view->lista_bledow = '<div id="lista_bledow" style="margin-left: 0px;">Zbyt krótka nazwa pliku lub brak nazwy.</div>';
            }
        }
    }
	
	public function exportsqlAction() 
	{
		$ajax = true;
		$multiInsert = true;
		$import = new Import();
        $this->view->linksql = '';
        if ($this->_request->isPost() || $this->_request->getParam('nazwa'))
		{
            $filter = new Zend_Filter_StripTags();
            $nazwa = $filter->filter($this->_request->getPost('nazwa', $this->_request->getParam('nazwa')));
            if (strlen($nazwa) > 1) 
			{
                $filenamezip = 'admin/bazy/'.$nazwa.'-'.date('Y-m-d').'.zip';
				@mkdir('admin/bazy/'.$nazwa.'-'.date('Y-m-d'));
				$this->view->file = $nazwa;

				if(!$ajax)
				{
					$zip = new ZipArchive();
					if($zip->open($filenamezip, ZIPARCHIVE::CREATE) !== TRUE) 
					{
						$this->view->blad_edycji = '<div class="k_blad">Błąd podczas exportu bazy.</div>';
						$this->view->lista_bledow = '<div id="lista_bledow">Nie można utworzyć archiwum zip</div>';
						break;
					}
				}
				
				$tabele = $import->getTabele();
				if($ajax) $this->view->tabele = $tabele;
				else
				if(count($tabele) > 0)
				foreach($tabele as $tabela)
				{
					if(in_array(strtolower($tabela['Name']), $import->notAllowed)) continue;
					
					$sql = $nazwa.'-'.date('Y-m-d').'-'.$tabela['Name'].'.sql';
					$filenamesql = 'admin/bazy/'.$nazwa.'-'.date('Y-m-d').'/'.$sql;

					$select = 'CREATE TABLE IF NOT EXISTS `'.$tabela['Name'].'` (';
					
					$kolumny = $import->getKolumny($tabela['Name']);
					if(count($kolumny) > 0)
					foreach($kolumny as $rows)
					{
						$select .= '`'.$rows['Field'].'`';
						$select .= ' '.$rows['Type'].' ';
						$select .= ($rows['Null'] == 'NO') ? 'NOT NULL' : 'NULL';
						$select .= ($rows['Default'] !== null) ? ' DEFAULT \''.$rows['Default'].'\'' : '';
						$select .= ($rows['Extra'] == 'auto_increment') ? ' AUTO_INCREMENT' : '';
						if($rows['Key'] == 'PRI') $key = $rows['Field'];
						$select .= ', ';
					}
					if(isset($key)) $select .= 'PRIMARY KEY (`'.$key.'`)';
					$select .= ')';
					$select .= ' ENGINE='.$tabela['Engine'];
					$charset = explode('_', $tabela['Collation']);
					$select .= ' DEFAULT CHARSET='.$charset[0];
					$select .= ' AUTO_INCREMENT=1';//.$tabela['Auto_increment'];
					$select .= ';';
					//echo $select.'<br>';
					$tableSql = $select;
					$select = '';
					unset($rowSqls);
					
				    $wiersze = $import->getWiersze($tabela['Name']);
					
					for($i = 0; $i < count($wiersze); $i++)
					{
						$wiersz = '';
						for($j = 0; $j < count($kolumny); $j++)
						{
							$int = (strpos($kolumny[$j]['Type'], 'int') !== false);
							$float = (strpos($kolumny[$j]['Type'], 'float') !== false);
							$decimal = (strpos($kolumny[$j]['Type'], 'decimal') !== false);
							$double = (strpos($kolumny[$j]['Type'], 'double') !== false);
							$numeric = ($int || $float || $decimal || $double);
							$value = $wiersze[$i][$kolumny[$j]['Field']];
							$null = ($value === NULL);
							if($null) $value = 'NULL';
							if(!$numeric && !$null) $wiersz .= '"';
							//$select .= str_replace('"', '\"', $value);
							$wiersz .= addslashes($value);
							if(!$numeric && !$null) $wiersz .= '"';
							if($j < count($kolumny) - 1) $wiersz .= ', ';
						}
						
						if($multiInsert)
						{
							//if($i % 100 == 0)
							if($i == 0 || empty($select) || (mb_strlen($select.$wiersz, 'UTF-8') + 2 >= 1024 * 1024))
							{
								if(!empty($select)) $rowSqls[] = $select.';';
								$select = 'INSERT IGNORE INTO `'.$tabela['Name'].'` VALUES';
							}
							else $select.= ',';
						}
						else $select = 'INSERT IGNORE INTO `'.$tabela['Name'].'` VALUES';
						
						$select.= '(';
						$select.= $wiersz;
						$select.= ')';
						
						if(!$multiInsert)
						{
							$select .= ';';
							$rowSqls[] = $select;
						}
						//break;
					}
					if(!empty($select)) $rowSqls[] = $select.';';
					//if(isset($rowSqls)) var_dump($rowSqls);
					//if(count($wiersze) > 0) break;
					
					$file = @fopen($filenamesql, 'w');
					{
						fwrite($file, $tableSql.'/*DELIM*/'.PHP_EOL);
						if(isset($rowSqls) && count($rowSqls) > 0)
						foreach($rowSqls as $row)
						fwrite($file, $row.'/*DELIM*/'.PHP_EOL);
					}
					fclose($file);

					if(file_exists($filenamesql))
					{
						if($zip->addFile($filenamesql, $sql) !== TRUE) 
						$this->bledy .= '<div id="lista_bledow">Nie można dodać '.$filenamesql.' do archiwum</div>';
					}
					else $this->bledy .= 'Nie udało się stworzyć pliku '.$filenamesql.'<br/>';
				}
				
				if(!$ajax) $zip->close();
				
				if(strlen($this->bledy) > 1) 
				{
                    $this->view->blad_edycji = '<div class="k_blad">Błąd podczas exportu bazy.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $this->bledy . '</div>';
                }
				else
				{
					if(!$ajax)
					$this->view->blad_edycji = '<div class="k_ok">Export bazy przebiegł pomyślnie.
						<a href="'.$this->view->baseUrl.'/public/'.$filenamezip.'">Link do pliku zip</a></div>';
					else 
					$this->view->blad_edycji = '<div class="k_uwaga">Trwa wykonywanie exportu.</div>';
				}
            }
			else 
			{
                $this->view->blad_edycji = '<div class="k_blad">Błąd podczas exportu bazy.</div>';
                $this->view->lista_bledow = '<div id="lista_bledow">Zbyt krótka nazwa pliku lub brak nazwy.</div>';
            }
        }
    }	
	
	public function importxlsallAction() 
	{
		$ajax = true;
        if($this->_request->isPost() || $this->_request->getParam('plik'))
		{
            $plik = $this->_request->getPost('plik', $this->_request->getParam('plik'));
            if(isset($plik))
			{
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->setDestination($this->ImageDir->ZipFileDir);
                $adapter->addValidator('Extension', false, 'zip')->addValidator('Count', false, 1);
                if($adapter->receive()) 
				{
                    $filenameZIP = $adapter->getFileName();
                    $import->wypakujAll($filenameZIP, $this->ImageDir->ZipExtDir);
					$file = str_replace('\\', '/', $filenameZIP);
					$this->view->file = str_replace($this->ImageDir->ZipFileDir, '', $file);

					$import = new Import();
					$tabele = $import->getTabele();
					if($ajax) $this->view->tabele = $tabele;
					else
					if(count($tabele) > 0)
					foreach($tabele as $tabela)
					{
						if(in_array(strtolower($tabela['Name']), $import->notAllowed)) continue;
						
						$filename = substr($filenameZIP, strlen($this->ImageDir->ZipFileDir), -4);
						$filenameXLS = $this->ImageDir->ZipExtDir.''.$filename.'-'.$tabela['Name'].'.xls';
						if(file_exists($filenameXLS))
						{
							$dane = $import->wczytajDaneXlsAll($filenameXLS);
							if(count($dane) > 0)
							{
								$import->wyczyscTabele($tabela['Name']);
								foreach($dane as $row)
								$import->insertRow($tabela['Name'], $row);
							}
						}
						else
						{
							$this->bledy .= 'Zła nazwa pliku '.$filename.' xls lub jego brak w archiwum zip <br>';
						}
					}
                } 
				else 
				{
                    $this->bledy .= 'Zły typ pliku. Akceptowalny typ to archiwum ZIP (*.zip) <br>';
                }
                if (strlen($this->bledy) > 1) 
				{
                    $this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $this->bledy . '</div>';
                }
				else 
				{
                    if(!$ajax)
					$this->view->blad_edycji = '<div class="k_ok">Operacja przebiegła pomyślnie.</div>';
					else
                    $this->view->blad_edycji = '<div class="k_uwaga">Trwa wykonywanie importu.</div>';
                }
            }
        }
    }
	
	public function importsqlAction() 
	{
		$ajax = true;
        if($this->_request->isPost() || $this->_request->getParam('plik'))
		{
            $plik = $this->_request->getPost('plik', $this->_request->getParam('plik'));
            if(isset($plik))
			{
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->setDestination($this->ImageDir->ZipFileDir);
                $adapter->addValidator('Extension', false, 'zip')->addValidator('Count', false, 1);
                if($adapter->receive()) 
				{
                    $filenameZIP = $adapter->getFileName();
                    $import->wypakujAll($filenameZIP, $this->ImageDir->ZipExtDir);
					$file = str_replace('\\', '/', $filenameZIP);
					$this->view->file = str_replace($this->ImageDir->ZipFileDir, '', $file);

					$import = new Import();
					$tabele = $import->getTabele();					
					if($ajax) $this->view->tabele = $tabele;					
					else 
					if(count($tabele) > 0)
					foreach($tabele as $tabela)
					{
						if(in_array(strtolower($tabela['Name']), $import->notAllowed)) continue;
						
						$filename = substr($filenameZIP, strlen($this->ImageDir->ZipFileDir), -4);
						$filenameSQL = $this->ImageDir->ZipExtDir.''.$filename.'-'.$tabela['Name'].'.sql';
						if(file_exists($filenameSQL))
						{
							$dane = $import->wczytajDaneSqlAll($filenameSQL);
							if(count($dane) > 0)
							{
								$import->wyczyscTabele($tabela['Name']);
								foreach($dane as $row)
								{
									$select = trim($row);
									if(!empty($select))
									$import->insertSql($tabela['Name'], $select);
								}
							}
						}
						else
						{
							$this->bledy .= 'Zła nazwa pliku '.$filename.' sql lub jego brak w archiwum zip <br>';
						}
					}
                } 
				else 
				{
                    $this->bledy .= 'Zły typ pliku. Akceptowalny typ to archiwum ZIP (*.zip) <br>';
                }
                if (strlen($this->bledy) > 1) 
				{
                    $this->view->blad_edycji = '<div class="k_blad">Błąd edycji danych.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $this->bledy . '</div>';
                }
				else 
				{
                    if(!$ajax)
					$this->view->blad_edycji = '<div class="k_ok">Operacja przebiegła pomyślnie.</div>';
					else
                    $this->view->blad_edycji = '<div class="k_uwaga">Trwa wykonywanie importu.</div>';
                }
            }
        }
    }
	
	public function importcsvAction()
	{
		$ajax = true;
		$this->view->r_zapisz = 'zapisz';
		$tryb = $this->_request->getParam('tryb', 'panel');
		$typ = $this->_request->getParam('typ', '');
		$this->view->tryb = $tryb;
		$this->view->typ = $typ;
		$this->view->rozsz = array('name' => '*.csv;*.cdn', 'desc' => 'Pliki CSV,CDN(*.csv,*.cdn)');
		$import = new Import($tryb, 'produkty', $typ);
		if(true)
		{
			if($tryb == 'fpp' && $typ == 'cennik')
			{
				$manual = '<b>Instrukcja przygotowania pliku z listą produktów w programie FPP</b>:
				<br/>1. Z Menu Głównego programu FPP wybierz <b>4...Magazyn -> Menu raportów -> Cennik : Imp/Exp</b>
				<br/>2. W nowym oknie wybierz Przeprowadzana akcja:<b> eksport</b>
				<br/>3. W nowym oknie wybierz następujące opcje:
				<br/>- Grupa towarów - wedle uznania (domyślnie cały magazyn)
				<br/>- Pozycje - wedle uznania (domyślnie wszystkie)
				<br/>- Uwzględniaj pozycje ze stanem zero: wedle uznania (domyślnie Tak)
				<br/>- Zaleca się zaznaczenie wszystkich cen sprzedaży, detalicznej oraz zakupu (wymagane jest zwłaszcza pozostawienie <b>ceny&nbsp;sprzedaży I</b>, inaczej wszystkie importowane produkty będą miały zerową cenę!)
				<br/>4. Po powrocie do poprzedniego okna wybierz:
				<br/>Katalog pliku <b>CENNIK.CDN</b> - wedle uznania np. dyskietka A lub na dysku komputera - opcja Inny: wpisz np. <b>C:\FPP\</b>
				<br/>Strona kodowa - <b>Win-1250</b>
				<br/>5. Wracając do panelu, po kliknięciu w przycisk <b>Przeglądaj</b>, wybierz wyeksportowany plik CENNIK.CDN z katalogu wybranego w poprzednim punkcie
				<br/>6. Kliknij <b>Zapisz</b>';
			}
			$this->view->manual = @$manual;
		}
		if($this->_request->getParam('test'))
		{
			$this->view->przyklad = $import->generujPrzyklad();
			$this->view->blad_edycji = '<a href="'.$this->view->baseUrl.'/public/'.$this->view->przyklad.'">';
			$this->view->blad_edycji .= 'Pobierz przykładowy plik CSV</a>';
		}
        if($this->_request->isPost())
		{
            $plik = $this->_request->getPost('plik');
			$file = $this->_request->getPost('file');
			$nowe = $this->_request->getPost('nowe');
            if(isset($plik))
			{
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->setDestination($this->ImageDir->ZipFileDir);
                $adapter->addValidator('Count', false, 1);
						//->addValidator('Extension', false, array('extension1' => 'zip,csv', 'case' => true));
                if(!empty($file) || $adapter->receive())
				{
                    //$filename = $adapter->getFileName();
					$filename = $this->ImageDir->ZipFileDir.''.$file;
					$filename = str_replace('\\', '/', $filename);
					$fileinfo = pathinfo($filename);
					$dirname = $fileinfo['dirname'];//$this->ImageDir->ZipFileDir
					$name = $fileinfo['filename'];//substr($filename, strlen($this->ImageDir->ZipFileDir), -4);
					$basename = $fileinfo['basename'];//basename($filename);
					$extension = @empty($fileinfo['extension']) ? 'csv' : strtolower($fileinfo['extension']);
					if($extension != 'csv' && $extension != 'cdn' && $extension != 'zip')
					$this->bledy .= 
						'Zły typ pliku. Akceptowalny typ to CSV (*.csv, *.cdn) lub archiwum ZIP (*.zip) <br>';
                    $nameZip = $name.'.zip';
					$nameCsv = $name.'.'.$extension;
					$filenameCsv = $this->ImageDir->ZipExtDir.''.$nameCsv;
					//var_dump($fileinfo);
					
					if(@empty($this->bledy))
					if($extension == 'zip') // strpos($filename, '.zip') > 0
					{
						$nameCsv = $name.'.csv';
						if(@$fileinfo['extension'] == 'ZIP') $nameCsv = $name.'.CSV';
						$filenameCsv = $this->ImageDir->ZipExtDir.''.$nameCsv;
						$import->wypakuj($this->ImageDir->ZipExtDir, $filename, $nameCsv);
						if(!empty($import->bledy))
						{
							$bledy = $import->bledy;
							$import->bledy = '';
							$nameCsv = $name.'.cdn';
							if(@$fileinfo['extension'] == 'ZIP') $nameCsv = $name.'.CDN';
							$import->wypakuj($this->ImageDir->ZipExtDir, $filename, $nameCsv);							
							if(!empty($import->bledy))
							{
								$this->bledy .= $bledy;
								$this->bledy .= $import->bledy;
							}
							else $filenameCsv = $this->ImageDir->ZipExtDir.''.$nameCsv;
						}
						else $filenameCsv = $this->ImageDir->ZipExtDir.''.$nameCsv;
					}
					else rename($filename, $filenameCsv);
                    
					if(file_exists($filenameCsv))
					{
						if($ajax)
						{
							$this->view->file = $nameCsv;
							$this->view->nowe = $nowe;
							$this->view->opisy = $import->opisy;
							$this->view->adres = 'sklep.pl';
							@file_put_contents('../public/ajax/importcsv.txt', 0);
						}
                    }
					else
					{
						if(@empty($this->bledy))
                        $this->bledy .= 'Zła nazwa pliku lub brak pliku '.$filename.'<br>';
                    }
                }
				else
				{
                    $this->bledy .= 'Zły typ pliku. Akceptowalny typ to CSV (*.csv) lub archiwum ZIP (*.zip) <br>';
                }
                if(strlen($this->bledy) > 1)
				{
                    $this->view->blad_edycji = '<div class="k_uwaga">Wynik edycji danych.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $this->bledy . '</div>';
                }
				else
				{
					if(!$ajax)
                    $this->view->blad_edycji = '<div class="k_ok">Operacja przebiegła pomyślnie.</div>';
					else
					$this->view->blad_edycji = '<div class="k_uwaga">Trwa wykonywanie importu.</div>';
                }
            }
        }
    }
	
	public function importcsvklienciAction()
	{
		$ajax = true;
		$this->view->r_zapisz = 'zapisz';
		$tryb = $this->_request->getParam('tryb', 'panel');
		$rodzaj = $this->_request->getParam('rodzaj', 'klienci');
		$this->view->tryb = $tryb;
		$this->view->rodzaj = $rodzaj;
		$this->view->rozsz = array('name' => '*.csv', 'desc' => 'Pliki CSV (*.csv)');
		$import = new Import($tryb, $rodzaj);
		if(true)
		{
			if($tryb == 'fpp')
			{
				$manual = '<b>Instrukcja przygotowania pliku z listą kontrahentów w programie FPP</b>:
				<br/>1. Z Menu Głównego programu FPP wybierz <b>6...Kontrahenci -> Eksport danych</b>
				<br/>2. W nowym oknie wybierz następujące opcje:
				<br/>- eksport grupy kontrahentów - wedle uznania (domyślnie wszystkie grupy czyli puste pole)
				<br/>- pozycje - wedle uznania (domyślnie wszystkie)
				<br/>- format danych: <b>COMMA</b>
				<br/>- strona kodowa: <b>Win-1250</b>
				<br/>- nazwa pliku: pełna ścieżka i własna nazwa pliku np. <b>C:\FPP\KLIENCI.CSV</b><br/>&nbsp;(nazwa pliku może mieć maksymalnie 8 znaków nie&nbsp;licząc rozszerzenia)
				<br/>3. Wracając do panelu, po kliknięciu w przycisk <b>Przeglądaj</b>,<br/>wybierz wyeksportowany plik o podanej nazwie z katalogu wybranego w poprzednim punkcie
				<br/>4. Kliknij <b>Zapisz</b>';
			}
			$this->view->manual = @$manual;
		}
		if($this->_request->getParam('test'))
		{
			$this->view->przyklad = $import->generujPrzyklad();
			$this->view->blad_edycji = '<a href="'.$this->view->baseUrl.'/public/'.$this->view->przyklad.'">';
			$this->view->blad_edycji .= 'Pobierz przykładowy plik CSV</a>';
		}
        if($this->_request->isPost())
		{
            $plik = $this->_request->getPost('plik');
			$file = $this->_request->getPost('file');
			$nowe = $this->_request->getPost('nowe');
            if(isset($plik))
			{
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->setDestination($this->ImageDir->ZipFileDir);
                $adapter->addValidator('Count', false, 1);
						//->addValidator('Extension', false, array('extension1' => 'zip,csv', 'case' => true));
                if(!empty($file) || $adapter->receive())
				{
                    //$filename = $adapter->getFileName();
					$filename = $this->ImageDir->ZipFileDir.''.$file;
					$filename = str_replace('\\', '/', $filename);
					$fileinfo = pathinfo($filename);
					$dirname = $fileinfo['dirname'];//$this->ImageDir->ZipFileDir
					$name = $fileinfo['filename'];//substr($filename, strlen($this->ImageDir->ZipFileDir), -4);
					$basename = $fileinfo['basename'];//basename($filename);
					$extension = @empty($fileinfo['extension']) ? 'csv' : strtolower($fileinfo['extension']);
					if($extension != 'csv' && $extension != 'zip')
					$this->bledy .= 
						'Zły typ pliku. Akceptowalny typ to CSV (*, *.csv) lub archiwum ZIP (*.zip) <br>';
                    $nameZip = $name.'.zip';
					$nameCsv = $name.'.'.$extension;
					$filenameCsv = $this->ImageDir->ZipExtDir.''.$nameCsv;
					//var_dump($fileinfo);
					
					if(@empty($this->bledy))
					if($extension == 'zip') // strpos($filename, '.zip') > 0
					{
						$nameCsv = $name.'.csv';
						if(@$fileinfo['extension'] == 'ZIP') $nameCsv = $name.'.CSV';
						$filenameCsv = $this->ImageDir->ZipExtDir.''.$nameCsv;
						$import->wypakuj($this->ImageDir->ZipExtDir, $filename, $nameCsv);
						if(!empty($import->bledy)) $this->bledy .= $import->bledy;
						else $filenameCsv = $this->ImageDir->ZipExtDir.''.$nameCsv;
					}
					else rename($filename, $filenameCsv);
                    
					if(file_exists($filenameCsv))
					{
						if($ajax)
						{
							$this->view->file = $nameCsv;
							$this->view->nowe = $nowe;
							$this->view->opisy = $import->opisy;
							$this->view->adres = 'sklep.pl';
							@file_put_contents('../public/ajax/importcsvklienci.txt', 0);
						}
                    }
					else
					{
						if(@empty($this->bledy))
                        $this->bledy .= 'Zła nazwa pliku lub brak pliku '.$filename.'<br>';
                    }
                }
				else
				{
                    $this->bledy .= 'Zły typ pliku. Akceptowalny typ to CSV (*.csv) lub archiwum ZIP (*.zip) <br>';
                }
                if(strlen($this->bledy) > 1)
				{
                    $this->view->blad_edycji = '<div class="k_uwaga">Wynik edycji danych.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $this->bledy . '</div>';
                }
				else
				{
					if(!$ajax)
                    $this->view->blad_edycji = '<div class="k_ok">Operacja przebiegła pomyślnie.</div>';
					else
					$this->view->blad_edycji = '<div class="k_uwaga">Trwa wykonywanie importu.</div>';
                }
            }
        }
    }
	
	public function importxlsgrupyAction()
	{
		$ajax = true;
		$this->view->r_zapisz = 'zapisz';
		$tryb = $this->_request->getParam('tryb', 'fpp');
		$typ = $this->_request->getParam('typ', '');
		$this->view->tryb = $tryb;
		$this->view->typ = $typ;
		$this->view->rozsz = array('name' => '*.xls;*.xlsx', 'desc' => 'Pliki Excela (*.xls, *.xlsx)');
		$import = new Import($tryb, 'produktygrupy', $typ);
		if(true)
		{
			if($tryb == 'fpp')
			{
				//http://www.pablosoftwaresolutions.com/download.php?id=12
				//http://storage.dobreprogramy.pl/archiwum/dbfexplorer(dobreprogramy.pl).zip				
				$manual = '<b>Instrukcja przygotowania pliku z listą grup produktów z programu FPP</b>:
				<br/>1. Ściągnij, zainstaluj i uruchom program <a href="http://tgsoft.pl/programy/setup_re.exe" target="_blank"><b>SQL Report Express</b></a>
				<br/>2. Kliknij ikonę <b>Otwórz</b>, wybierz typ pliku <b>"Baza Clarion (CDN)"</b> i wybierz plik <b>GRUPY.DAT</b> z katalogu gdzie jest zainstalowany program FPP np. <b>C:\FPP\DEMO,</b>			
				<br/>3. W nowym oknie z widokiem grup kliknij ikonę <b>"Arkusz"</b>,
				<br/>4. W nowo otwartym arkuszu <b>Excela</b> wybierz z menu głównego <b>"Plik -> Zapisz jako"</b> i zapisz plik <b>XLS</b> w wybranym przez siebie katalogu i o wybranej nazwie np. <b>GRUPY.XLS</b> (domyślną nazwą jest nazwa użytkownika Windows),<br/>
				- można już zamknąć otwarty arkusz Excela, <br/>
				- można już zamknąć program SQL Report Express, 
				<br/>5. Wracając do panelu, po kliknięciu w przycisk <b>Przeglądaj</b>,<br/>wybierz wyeksportowany plik o wybranej nazwie z katalogu wybranego w poprzednim punkcie
				<br/>6. Kliknij <b>Zapisz</b>';
			}
			$this->view->manual = @$manual;
		}
		if($this->_request->getParam('test'))
		{
			$this->view->przyklad = $import->generujPrzyklad();
			$this->view->blad_edycji = '<a href="'.$this->view->baseUrl.'/public/'.$this->view->przyklad.'">';
			$this->view->blad_edycji .= 'Pobierz przykładowy plik XLS</a>';
		}
        if($this->_request->isPost())
		{
            $plik = $this->_request->getPost('plik');
			$file = $this->_request->getPost('file');
			$nowe = $this->_request->getPost('nowe');
            if(isset($plik))
			{
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->setDestination($this->ImageDir->ZipFileDir);
                $adapter->addValidator('Count', false, 1);
						//->addValidator('Extension', false, array('extension1' => 'zip,xls', 'case' => true));
                if(!empty($file) || $adapter->receive())
				{
                    //$filename = $adapter->getFileName();
					$filename = $this->ImageDir->ZipFileDir.''.$file;
					$filename = str_replace('\\', '/', $filename);
					$fileinfo = pathinfo($filename);
					$dirname = $fileinfo['dirname'];//$this->ImageDir->ZipFileDir
					$name = $fileinfo['filename'];//substr($filename, strlen($this->ImageDir->ZipFileDir), -4);
					$basename = $fileinfo['basename'];//basename($filename);
					$extension = @empty($fileinfo['extension']) ? 'xls' : strtolower($fileinfo['extension']);
					if($extension != 'xls' && $extension != 'zip')
					$this->bledy .= 
						'Zły typ pliku. Akceptowalny typ to XLS (*.xls) lub archiwum ZIP (*.zip)<br>';
                    $nameZip = $name.'.zip';
					$nameXls = $name.'.'.$extension;
					$filenameXls = $this->ImageDir->ZipExtDir.''.$nameXls;
					//var_dump($fileinfo);
					
					if(@empty($this->bledy))
					if($extension == 'zip') // strpos($filename, '.zip') > 0
					{
						$nameXls = $name.'.xls';
						if(@$fileinfo['extension'] == 'ZIP') $nameXls = $name.'.XLS';
						$filenameXls = $this->ImageDir->ZipExtDir.''.$nameXls;
						$import->wypakuj($this->ImageDir->ZipExtDir, $filename, $nameXls);
						if(!empty($import->bledy)) $this->bledy .= $import->bledy;
						else $filenameXls = $this->ImageDir->ZipExtDir.''.$nameXls;
					}
					else rename($filename, $filenameXls);
                    
					if(file_exists($filenameXls))
					{
						if($ajax)
						{
							$this->view->file = $nameXls;
							$this->view->nowe = $nowe;
							$this->view->opisy = $import->opisy;
							$this->view->adres = 'sklep.pl';
							@file_put_contents('../public/ajax/importxlsgrupy.txt', 0);
						}
                    }
					else
					{
						if(@empty($this->bledy))
                        $this->bledy .= 'Zła nazwa pliku lub brak pliku '.$filename.'<br>';
                    }
                }
				else
				{
                    $this->bledy .= 'Zły typ pliku. Akceptowalny typ to XLS (*.xls) lub archiwum ZIP (*.zip) <br>';
                }
                if(strlen($this->bledy) > 1)
				{
                    $this->view->blad_edycji = '<div class="k_uwaga">Wynik edycji danych.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $this->bledy . '</div>';
                }
				else
				{
					if(!$ajax)
                    $this->view->blad_edycji = '<div class="k_ok">Operacja przebiegła pomyślnie.</div>';
					else
					$this->view->blad_edycji = '<div class="k_uwaga">Trwa wykonywanie importu.</div>';
                }
            }
        }
    }
	
	public function importcsvrabatyAction()
	{
		$ajax = true;
		$this->view->r_zapisz = 'zapisz';
		$tryb = $this->_request->getParam('tryb', 'panel');
		$rodzaj = $this->_request->getParam('rodzaj', 'rabaty');
		$this->view->tryb = $tryb;
		$this->view->rodzaj = $rodzaj;
		$this->view->rozsz = 'csv';
		$this->view->rozsz = array('name' => '*.csv', 'desc' => 'Pliki CSV (*.csv)');
		$import = new Import($tryb, $rodzaj);
		if(true)
		{
			if($tryb == 'fpp')
			{
				//http://www.pablosoftwaresolutions.com/download.php?id=12
				//http://storage.dobreprogramy.pl/archiwum/dbfexplorer(dobreprogramy.pl).zip				
				$manual = '<b>Instrukcja przygotowania pliku z listą rabatów z programu FPP</b>:
				<br/>1. Ściągnij, zainstaluj i uruchom program <a href="http://tgsoft.pl/programy/setup_re.exe" target="_blank"><b>SQL Report Express</b></a>,
				<br/>2. Z menu głównego wybierz <b>Funkcje -> Konwersja tabel do innych formatów</b>, 
				<br/>3. W nowym oknie wybierz następujące opcje:<br/>
				- Tabela żródłowa : <b>Clarion (CDN)</b>,<br/>
				- Tabela wynikowa : <b>DBF - (FOX)</b> lub <b>DBF - (Clipper)</b>,<br/>
				- Kopiuj do katalogu : <b>wybierz katalog</b> gdzie chcesz zapisać nowy plik <b>DBF</b>,<br/>
				- Konwersja polskich liter : <b>Mazovia</b> ===> <b>ANSI Windows</b>,<br/>
				- Kliknij <b>"Dodaj plik"</b> i wybierz plik <b>UPUSTY.DAT</b> z katalogu gdzie jest zainstalowany program FPP np. <b>C:\FPP\DEMO</b>,<br/>
				- Kliknij <b>"OK"</b> i potwierdź "Wykonać kopiowanie danych" klikając <b>"Tak"</b>,<br/>
				- w zależności od ilości różnych upustów, produktów, grup produktów i kontrahentów w FPP, operacja może troche potrwać, zakończy się ona komunikatem "Koniec kopiowania..." - kliknij <b>"OK"</b>,<br/>
				- można już zamknąć program SQL Report Express, 
				<br/>4. Ściągnij, rozpakuj i uruchom program <a href="http://www.pablosoftwaresolutions.com/download.php?id=12" target="_blank"><b>DBF Explorer</b></a>
				<br/>5. Kliknij ikonę <b>Open</b>, wybierz typ pliku <b>"dBase Files (*.dbf)"</b> i wybierz plik <b>UPUSTY.DBF</b> z katalogu wybranego wcześniej w punkcie 3 w opcji <b>"Kopiuj do katalogu"</b>,<br/>
				- operacja otwierania pliku z upustami może troche potrwać,
				<br/>6. Z menu głównego wybierz <b>File -> Export...</b> i zapisz nowy plik <b>CSV</b> (koniecznie dopisz do nazwy pliku rozszerzenie tak żeby nazwa pliku była <b>UPUSTY.CSV</b>),<br/>
				- operacja zapisywania pliku z upustami może troche potrwać,<br/>
				- można już zamknąć program DBF Explorer, 
				<br/>7. Wracając do panelu, po kliknięciu w przycisk <b>Przeglądaj</b>,<br/>wybierz wyeksportowany plik o podanej nazwie <b>UPUSTY.CSV</b> z katalogu wybranego w poprzednim punkcie
				<br/>8. Kliknij <b>Zapisz</b>';
			}
			$this->view->manual = @$manual;
		}
		if($this->_request->getParam('test'))
		{
			$this->view->przyklad = $import->generujPrzyklad();
			$this->view->blad_edycji = '<a href="'.$this->view->baseUrl.'/public/'.$this->view->przyklad.'">';
			$this->view->blad_edycji .= 'Pobierz przykładowy plik CSV</a>';
		}
		
        if($this->_request->isPost())
		{
            $plik = $this->_request->getPost('plik');
			$file = $this->_request->getPost('file');
			$nowe = $this->_request->getPost('nowe');
			//var_dump($_POST);var_dump($_FILES);die();
            if(isset($plik))
			{
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->setDestination($this->ImageDir->ZipFileDir);
                $adapter->addValidator('Count', false, 1);
						//->addValidator('Extension', false, array('extension1' => 'zip,csv', 'case' => true));
                if(!empty($file) || $adapter->receive())
				{
                    //$filename = $adapter->getFileName();
					$filename = $this->ImageDir->ZipFileDir.''.$file;
					$filename = str_replace('\\', '/', $filename);
					$fileinfo = pathinfo($filename);
					$dirname = $fileinfo['dirname'];//$this->ImageDir->ZipFileDir
					$name = $fileinfo['filename'];//substr($filename, strlen($this->ImageDir->ZipFileDir), -4);
					$basename = $fileinfo['basename'];//basename($filename);
					$extension = @empty($fileinfo['extension']) ? 'csv' : strtolower($fileinfo['extension']);
					if($extension != 'csv' && $extension != 'zip')
					$this->bledy .= 
						'Zły typ pliku. Akceptowalny typ to CSV (*.csv)<br>'; //lub archiwum ZIP (*.zip) <br>';
                    $nameZip = $name.'.zip';
					$nameCsv = $name.'.'.$extension;
					$filenameCsv = $this->ImageDir->ZipExtDir.''.$nameCsv;
					//var_dump($filename);var_dump($fileinfo);
					//die();
					if(@empty($this->bledy))
					if($extension == 'zip') // strpos($filename, '.zip') > 0
					{
						$nameCsv = $name.'.csv';
						if(@$fileinfo['extension'] == 'ZIP') $nameCsv = $name.'.CSV';
						$filenameZip = $this->ImageDir->ZipFileDir.''.$nameZip;
						$import->wypakuj($this->ImageDir->ZipExtDir, $filenameZip, $nameCsv);
						if(!empty($import->bledy)) $this->bledy .= $import->bledy;
						else $filenameCsv = $this->ImageDir->ZipExtDir.''.$nameCsv;
					}
					else @rename($filename, $filenameCsv);
                    
					if(file_exists($filenameCsv))
					{
						if($ajax)
						{
							$this->view->file = $nameCsv;
							$this->view->nowe = $nowe;
							$this->view->opisy = $import->opisy;
							$this->view->adres = 'sklep.pl';
							@file_put_contents('../public/ajax/importcsvrabaty.txt', 0);
						}
                    }
					else
					{
						if(@empty($this->bledy))
                        $this->bledy .= 'Zła nazwa pliku lub brak pliku '.$filename.'<br>';
                    }
                }
				else
				{
                    $this->bledy .= 'Zły typ pliku. Akceptowalny typ to CSV (*.csv)<br>';
					// lub archiwum ZIP (*.zip) <br>';
                }
                if(strlen($this->bledy) > 1)
				{
                    $this->view->blad_edycji = '<div class="k_uwaga">Wynik edycji danych.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $this->bledy . '</div>';
                }
				else
				{
					if(!$ajax)
                    $this->view->blad_edycji = '<div class="k_ok">Operacja przebiegła pomyślnie.</div>';
					else
					$this->view->blad_edycji = '<div class="k_uwaga">Trwa wykonywanie importu.</div>';
                }
            }
        }
    }
	
	public function importxmlAction()
	{
        $ajax = true;
        $tryb = $this->_request->getParam('tryb', 'panel');
        $this->view->tryb = $tryb;
        $import = new Import($tryb);
        $this->view->rozsz = array('name' => '*.xml', 'desc' => 'Pliki XML (*.xml)');
        if(false && $this->_request->getParam('test'))
		{
            $this->view->przyklad = $import->generujPrzyklad();
            $this->view->blad_edycji = '<a href="' . $this->view->baseUrl . '/public/' . $this->view->przyklad . '">';
            $this->view->blad_edycji .= 'Pobierz przykładowy plik XML</a>';
        }
        if($this->_request->isPost())
		{
            $plik = $this->_request->getPost('plik');
            $file = $this->_request->getPost('file');
            $nowe = $this->_request->getPost('nowe');
            if(isset($plik))
			{
                $adapter = new Zend_File_Transfer_Adapter_Http();
                $adapter->setDestination($this->ImageDir->ZipFileDir);
                //$adapter->addValidator('Extension', false, array('extension1' => 'zip,xml', 'case' => false))
                //->addValidator('Count', false, 1);
                if(!empty($file) || $adapter->receive())
				{
                    //$filename = $adapter->getFileName();
                    $filename = $this->ImageDir->ZipFileDir . '' . $file;
                    $filename = str_replace('\\', '/', $filename);
                    $fileinfo = pathinfo($filename);
                    $dirname = $fileinfo['dirname']; //$this->ImageDir->ZipFileDir
                    $name = $fileinfo['filename']; //substr($filename, strlen($this->ImageDir->ZipFileDir), -4);
                    $basename = $fileinfo['basename']; //basename($filename);
                    $extension = $fileinfo['extension'];
                    $nameZip = $name . '.zip';
                    $nameXml = $name . '.' . $extension;
                    $filenameXml = $this->ImageDir->ZipExtDir . '' . $nameXml;
                    //var_dump($fileinfo);
                    if($extension == 'zip')
					{
                        $nameXml = $name . '.xml';
                        if(@$fileinfo['extension'] == 'ZIP') $nameXml = $name . '.XLS';
                        $filenameXml = $this->ImageDir->ZipExtDir . '' . $nameXml;
                        $import->wypakuj($this->ImageDir->ZipExtDir, $filename, $nameXml);
                        if(!empty($import->bledy)) $this->bledy .= $import->bledy;
                        //var_dump($x);
                    }
                    else rename($filename, $filenameXml);

                    if(file_exists($filenameXml))
					{
                        if(!$ajax)
						{
                            $dane = $import->wczytajDaneXml($filenameXml);
                            //var_dump($dane);die();
                            if(count($dane) > 0)
							{
                                $import->wgrajNoweProdukty($filename, $dane, 0, 0);
                                if($nowe) $import->usunStareProdukty($dane);
                                $this->bledy .= $import->bledy;
                            }
                        }
                        else
						{
                            $this->view->file = $nameXml;
                            $this->view->nowe = $nowe;
                            $this->view->opisy = $import->opisy;
                        }
                    }
					else
					{
                        if(@empty($this->bledy))
                        $this->bledy .= 'Zła nazwa pliku lub brak pliku ' . $filename . '<br>';
                    }
                }
                else
				{
                    $this->bledy .= 'Zły typ pliku. Akceptowalny typ to XML (*.xml) lub archiwum ZIP (*.zip) <br>';
                }
                if(strlen($this->bledy) > 1)
				{
                    $this->view->blad_edycji = '<div class="k_uwaga">Wynik edycji danych.</div>';
                    $this->view->lista_bledow = '<div id="lista_bledow">' . $this->bledy . '</div>';
                }
				else
				{
                    if(!$ajax)
                        $this->view->blad_edycji = '<div class="k_ok">Operacja przebiegła pomyślnie.</div>';
                    else
                        $this->view->blad_edycji = '<div class="k_uwaga">Trwa wykonywanie importu.</div>';
                }
            }
        }
    }
	
	public function exportzamowAction()
	{
		$ajax = true;
		$this->view->r_zapisz = 'zapisz';
		$tryb = $this->_request->getParam('tryb', 'panel');
		$typ = $this->_request->getParam('typ', '');
		$this->view->tryb = $tryb;
		$this->view->typ = $typ;
		//$this->view->rozsz = array('name' => '*.csv;*.cdn', 'desc' => 'Pliki CSV,CDN(*.csv,*.cdn)');
		$import = new Import($tryb, 'produkty', $typ);
		
		if(true)
		{
			if($tryb == 'fpp' && $typ == '')
			{
				$manual = '<b>Instrukcja przygotowania plików z zamówieniami do importu w programie FPP</b>
				<br/>1. Kliknij przycisk <b>Eksportuj</b>
				<br/>2. Po chwili pojawią się linki do plików <b>HEAD.MM</b> oraz <b>ELEM.MM</b>				
				<br/>3. <b>Oba</b> te pliki są potrzebne, zapisz je gdzieś na dysku swojego komputera
				<br/>4. (Kliknij prawym klawiszem myszy na obu linkach i wybierz <b>Zapisz...</b>)
				<br/>5. Z Menu Głównego FPP wybierz <b>1...Transakcje -> Zamówienia (ZA) -> Zamówienia od odbiorców</b>
				<br/>6. Klikni <b>F6</b> i wpisz nazwę katalogu gdzie zapisałeś pliki w kroku nr 3 np. <b>C:\FPP\</b>
				<br/>7. Po naciśnięciu <b>Enter</b> pojawi się lista zamówień odczytana z tych plików
				<br/>8. Wybierz zamówienia jakie chcesz zaimportować klawiszem <b>Spacja</b>
				<br/>9. Po wciśnięciu <b>Control + Enter</b> nastąpi import zaznaczonych zamówień';
			}
			$this->view->manual = @$manual;
		}
        if($this->_request->isPost())
		{
			$head = '';
			$elem = '';
			$headFile = 'public/ajax/HEAD.MM';
			$elemFile = 'public/ajax/ELEM.MM';
			if($ajax)
			{
				$zamowienia = new Zamowienia();
				$zamow = $zamowienia->selectAll('system');
				if(count($zamow) > 0)
				{					
					foreach($zamow as $zam)
					{
						$netto = 0;
						$brutto = 0;
						$prods = $zamowienia->selectWybranyProduktAll($zam['id']);
						//var_dump($prods);die();
						if(count($prods) > 0)
						{
							$p = 1;
							foreach($prods as $prod)
							{
								$zb = ($prod['jedn'] == $prod['jedn_miary_zb']);
								$zb = $zb && intval($prod['jedn_miary_zb_ile']) > 1;
								
								$elem .= $zam['id'].',';
								$elem .= ($p++).',';
								$elem .= '"'.stripslashes($prod['oznaczenie']).'",';
								$elem .= '"'.stripslashes($prod['grupa']).'",';
								$elem .= '"'.stripslashes($prod['nazwa']).'",';
								$elem .= '"",';//cecha
								$elem .= '"",';//pkwiu
								$ilosc = floatval($prod['ilosc']);
								if($zb) $ilosc *= intval($prod['jedn_miary_zb_ile']);
								$elem .= $ilosc.',';
								$elem .= '"'.$prod['jedn_miary'].'",';
								$elem .= '"'.$prod['jedn_miary_zb'].'",';
								$elem .= $prod['jedn_miary_zb_ile'].',';
								$cenaNetto = floatval($prod['netto']);
								$cenaNettoRabat = $cenaNetto * (floatval($prod['rabat']) / 100);
								$cenaNettoRabat = round($cenaNettoRabat, 2);
								$cenaNettoPoRabacie = $cenaNetto - $cenaNettoRabat;
								$cenaNettoPoRabacie = round($cenaNettoPoRabacie, 2);
								$netto += $cenaNettoPoRabacie * $ilosc;
								$cenaBrutto = floatval($prod['brutto']);
								$cenaBruttoPoRabacie = $cenaNetto * (1 + floatval($prod['vat']) / 100);
								$cenaBruttoPoRabacie = round($cenaBruttoPoRabacie, 2);
								$brutto += $cenaBruttoPoRabacie * $ilosc;
								$elem .= $cenaNetto.',';//zakupu
								//if($zb) $cenaNetto /= intval($prod['jedn_miary_zb_ile']);
								$elem .= $cenaNettoPoRabacie.',';//sprz po rabacie
								$elem .= $cenaNettoPoRabacie.',';//sprz po rabacie
								$elem .= '"Z’",';
								$elem .= floatval($prod['vat']).',';
								$elem .= '0,';
								$elem .= $cenaNetto.',';//I
								$elem .= '"Z’",0.0,0,';
								$elem .= $cenaNetto.',';//II
								$elem .= '"Z’",0.0,0,';
								$elem .= $cenaNetto.',';//III
								$elem .= '"Z’",0.0,0,';
								$elem .= '0,0,0,';
								$elem .= floatval($prod['vat']).',';
								$elem .= '0.0000,0.0000,0.0000,';//STANY
								$elem .= '0,0,0.00,';
								$elem .= $cenaBrutto.',';//D (br)
								$elem .= '"",';//ean
								$elem .= '0,0,';
								$elem .= '"",';//uwagi
								$elem .= $cenaNetto.',';//zakupu
								$elem .= '0.00,"I",0.0000,0.00,""';
								$elem .= "\r\n"; //PHP_EOL; // 0D i 0A czyli #13#10
							}
						}
						
						$head .= $zam['id'].',';
						$head .= '30,';
						$head .= '"'.date('y', strtotime($zam['data'])).'",';
						$head .= $zam['id'].',';
						$head .= '"VAT",';
						$head .= '"'.date('y/m/d', strtotime($zam['data'])).'",';
						$head .= '"'.stripslashes($zam['id_ext']).'",';
						$head .= '"'.stripslashes($zam['nazwa_firmy']).'",';
						$head .= '"'.stripslashes($zam['nazwa']).'",';
						$head .= '"'.stripslashes($zam['ulica']).'",';
						$head .= '"'.stripslashes($zam['kod']).'",';
						$head .= '"'.stripslashes($zam['miasto']).'",';
						$head .= '"",';//wpis
						$head .= '"PL'.stripslashes($zam['nip']).'",';
						$razem = $zamowienia->selectWartoscZamowieniaNetto($zam['id'], $zam);
						//var_dump(round($netto, 2, PHP_ROUND_HALF_DOWN));die();
						//$netto = $razem['razem_netto'];
						//$brutto = $razem['razem_brutto'];
						$head .= round($netto, 2).',';
						$head .= round($brutto - $netto, 2).',';
						$head .= round($brutto, 2).',';
						$head .= '0.00,3,"",';//? i przelew i przelew uwagi
						$head .= '"HANA",0,"~DEMO","Wersja demonstracyjna programu Firma++",';
						$head .= '"(C) Comarch SA","Kraków, Al. Jana Pawła II 41g",';
						$head .= '"tel.(012) 681 43 00, fax (012) 687 71 00",';
						$head .= '1,'.count($prods).',0,';
						$start = strtotime('2010-01-01');
						$end = strtotime($zam['data']);
						$platnoscDni = 76340 + floor(abs($end - $start) / 86400) + 14;
						$head .= $platnoscDni.',';
						$head .= '28,1,1';//waznosc dni (28)
						$head .= "\r\n"; //PHP_EOL; // 0D i 0A czyli #13#10
						
						//break;
					}
				}
				//$plik = mb_convert_encoding($plik, 'Windows-1251', 'UTF-8');
				$head = $this->common->usunPolskieLitery($head);
				$head = iconv("UTF-8", "Windows-1250", $head);
				file_put_contents('../'.$headFile, $head);
				$elem = $this->common->usunPolskieLitery($elem);
				$elem = iconv("UTF-8", "Windows-1250", $elem);
				file_put_contents('../'.$elemFile, $elem);
				//die();
				$this->view->blad_edycji = '<div class="k_ok">';
				$this->view->blad_edycji.= '<a href="'.$this->view->baseUrl.'/'.$headFile.'">';
				$this->view->blad_edycji.= 'Pobierz plik z danymi zamawiających (HEAD.MM)';
				$this->view->blad_edycji.= '</a><br/>';
				$this->view->blad_edycji.= '<a href="'.$this->view->baseUrl.'/'.$elemFile.'">';
				$this->view->blad_edycji.= 'Pobierz plik z danymi zamawianych produktów (ELEM.MM)';
				$this->view->blad_edycji.= '</a><br/>';
				$this->view->blad_edycji.= '</div>';
			}
        }
    }
}
?>