<?php
class Admin_ConfmailController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
	}
    function __call($method, $args)
	{
        $this->_redirect('/admin');
    }
	public function edytujAction()
	{
		$this->view->r_zapisz = 'zapisz';
		$mail = new Confmail;
		$err=0;
		if($this->_request->isPost())
		{
			$valid = new Zend_Validate();
			$mainValid = $valid->addValidator( new Zend_Validate_NotEmpty() );
			if($mainValid->isValid($this->_request->getPost('From'))){
				$from = $this->_request->getPost('From');
			}else{ $err=1; }
			if($mainValid->isValid($this->_request->getPost('FromName'))){
				$FromName= $this->_request->getPost('FromName');
			}else{ $err=1; }
			//if($mainValid->isValid($this->_request->getPost('Host', ''))){
				$Host = $this->_request->getPost('Host', ''); 
			//}else{ $err=0; }
			//if($mainValid->isValid($this->_request->getPost('POP3', ''))){
				$POP3 = $this->_request->getPost('POP3', ''); 
			//}else{ $err=0; }
			//if($mainValid->isValid($this->_request->getPost('Username', ''))){
				$Username = $this->_request->getPost('Username', '');
			//}else{ $err=0; }
			//if($mainValid->isValid($this->_request->getPost('Password', ''))){
				$Password = $this->_request->getPost('Password', '');
			//}else{ $err=0; }
			//if($mainValid->isValid($this->_request->getPost('Mailer', ''))){
				$Mailer = $this->_request->getPost('Mailer', '');
			//}else{ $err=0; }
			//if($mainValid->isValid($this->_request->getPost('ToCV', ''))){
				$ToCV = $this->_request->getPost('ToCV', '');
			//}else{ $err=0; }
			$To = $this->_request->getPost('To', '');
			
			if(!$err)
			{
				$array = array('From'=>$from, 'FromName'=>$FromName, 'Host'=>$Host, 'POP3'=>$POP3, 'Username'=>$Username, 'Password'=>$Password, 'Mailer'=>$Mailer, 'To'=>$To, 'ToCV'=>$ToCV);
				$mail->updateData($array, 1);
				//$this->view->error = '<div class="k_ok"> Edycja przebiegła pomyślnie </div>';
				$zakladka = $this->_request->getParam('zakladka', 0);
				if($zakladka == 4)
				$this->getResponse()->setHeader('Refresh','1;URL='.$this->view->baseUrl.'/admin/index/panel/zakladka/2');
			}
			else
			{
				$this->view->error = '<div class="k_blad"> Wszystkie pola są wymagane </div>';
			}
		}
		$this->view->drukuj = $mail->showData();
	}
}
?>