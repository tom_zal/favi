<?php
class Admin_SliderController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		//echo $this->view->baseUrl;
	}
	function __call($method, $args)
	{
		$this->_redirect('/admin');
	}	
	function edytujAction()
	{
		$slider = new Slider();
		$id = intval($this->_request->getParam('id', 0));
		if($id > 0)
		{
			$slider->id = $id;
			$this->view->slajd = $slider->wypiszJeden();
			$this->view->editor = $this->fcKeditor('opis', stripcslashes($this->view->slajd['opis']), 300, 'short');
		}
		else $this->view->editor = $this->fcKeditor('opis', '', 300, 'short');
		
		$del = intval($this->_request->getParam('delid', 0));
		if($del > 0)
		{
			$slider->id = $del;
			$slider->usun();
			//$this->view->blad_edycji = '<div class="k_ok">Wybrana slider została usunięta.</div>';
			$this->_redirect('/admin/slider/edytuj/');
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/slider/edytuj/');
		}
		$glowny = intval($this->_request->getParam('glowny', 0));
		if($glowny > 0)
		{
			$slider->id = $glowny;
			$slider->ustawGlowne($glowny, 'slider');
			//$this->view->blad_edycji = '<div class="k_ok">Wybrana slider została usunięta.</div>';
			$this->_redirect('/admin/slider/edytuj/');
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/slider/edytuj/');
		}
		$nieaktywny = $this->_request->getParam('inactive', 0);
		if($nieaktywny)
		{
			$slider->id = $nieaktywny;
			$dane = array('aktywny' => 0);
			$slider->edytuj($dane);
			$this->_redirect('/admin/slider/edytuj/');
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/slider/edytuj/');
		}
		$aktywny = $this->_request->getParam('active', 0);
		if($aktywny)
		{
			$slider->id = $aktywny;
			$dane = array('aktywny' => 1);
			$slider->edytuj($dane);
			$this->_redirect('/admin/slider/edytuj/');
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/slider/edytuj/');
		}
		$up = $this->_request->getParam('up');
		if($up)
		{
			$prev = $this->_request->getParam('prev', 0);
			$slider->id = $up;
			$upSlajd = $slider->wypiszJeden();
			$slider->id = $prev;
			$prevSlajd = $slider->wypiszJeden();
			$dane = array('kolejnosc' => $upSlajd['kolejnosc']);
			$slider->edytuj($dane);
			$slider->id = $up;
			$dane = array('kolejnosc' => $prevSlajd['kolejnosc']);
			$slider->edytuj($dane);
			$this->_redirect('/admin/slider/edytuj/');
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/slider/edytuj/');
		}
		$down = $this->_request->getParam('down');
		if($down)
		{
			$next = $this->_request->getParam('next', 0);
			$slider->id = $down;
			$downSlajd = $slider->wypiszJeden();
			$slider->id = $next;
			$nextSlajd = $slider->wypiszJeden();
			$dane = array('kolejnosc' => $downSlajd['kolejnosc']);
			$slider->edytuj($dane);
			$slider->id = $down;
			$dane = array('kolejnosc' => $nextSlajd['kolejnosc']);
			$slider->edytuj($dane);
			$this->_redirect('/admin/slider/edytuj/');
			$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/admin/slider/edytuj/');
		}
		if($this->_request->getPost('slideLinkiZapisz'))
		{
			$linki = $this->_request->getPost('linki');
			if($this->obConfig->slajdyOpisShort)
			$opisy = $this->_request->getPost('opisy');
			$opisyPoz = $this->_request->getPost('opisyPoz');
			$czasy = $this->_request->getPost('czasy');
			$aktywny = $this->_request->getPost('aktywny');
			if(count($aktywny) > 0)
			foreach($aktywny as $id => $val)
			{
				$slider->id = $id;
				$dane['aktywny'] = @intval($aktywny[$id]);
				$dane['link'] = @strval($linki[$id]);
				if($this->obConfig->slajdyOpisShort)
				$dane['opis'] = @strval($opisy[$id]);
				$dane['opis_poz'] = @intval($opisyPoz[$id]);
				$dane['czas'] = isset($czasy[$id]) ? @intval($czasy[$id]) : 5;
				$slider->edytuj($dane);
			}
		}
		if($this->_request->getPost('slideZapisz'))
		{
			$opis = $this->_request->getPost('opis', '');
			$link = $this->_request->getPost('link', '');
			$czas = $this->_request->getPost('czas', 5);
			$opisPoz = $this->_request->getPost('opisPoz', 0);
			$aktywny = $this->_request->getPost('aktywny', 1);
			//var_dump($_FILES);die();
			for($i = 0; $i < count($_FILES['img']['tmp_name']); $i++)
			{
				//set_time_limit(60);				
				$array = array
				(
					'name' => $_FILES['img']['name'][$i], 
					'type' => $_FILES['img']['type'][$i],
					'tmp_name' => $_FILES['img']['tmp_name'][$i],
					'error' => $_FILES['img']['error'][$i],
					'size' => $_FILES['img']['size'][$i]
				);
				
				$handle = @new upload($array);							
				if($handle->uploaded) 
				{
					$handle->file_new_name_body = 'slider';
					$handle->image_resize = false;
					$handle->image_x = $this->ImageDir->SlajdyX;
					$handle->image_y = $this->ImageDir->SlajdyY;
					//$this->ImageDir->szerokosc;
					$handle->image_ratio_y = true;
					$handle->process($this->ImageDir->Slajdy);

					if($handle->processed) 
					{
						$handle->clean();						
						$dane['img'] = $handle->file_dst_name;						
						//$this->view->blad_edycji = '<div class="k_ok">Edycja przebiegła pomyślnie.</div>';
					}
					else 
					{
						$this->view->blad_edycji = '<div class="k_blad">Upload nieudany. Nie można przenieść zdjęćia na serwer.</div>';
					}
				}
			}
			
			if($id == 0 && @empty($dane['img']))
			$this->view->blad_edycji = '<div class="k_blad">Zdjęcie jest wymagane.</div>';
			
			if(@empty($this->view->blad_edycji))
			{
				$dane['aktywny'] = $aktywny;
				$dane['opis'] = $opis;
				$dane['opis_poz'] = $opisPoz;
				$dane['link'] = $link;
				$dane['czas'] = $czas;
				if($id == 0)
				{
					$dane['kolejnosc'] = $slider->maxKolejnosc() + 1;
					$dane['typ'] = 'slider';
					$dane['lang'] = $this->lang;
					$slider->dodaj($dane);	
				}
				else
				{
					$slider->id = $id;
					$slider->edytuj($dane);
				}
				$this->_redirect('/admin/slider/edytuj/');
			}
			//$this->getResponse()->setHeader('Refresh', '1; URL='.$this->view->baseUrl.'/admin/slider/edytuj/');
		}
		$this->view->slajdy = $slider->wypisz()->toArray();
		
		if($this->obConfig->slajdyUstawEdycja)
		{
			$ustaw = $this->_request->getPost('ustaw');
			if(count($ustaw) > 0)
			{
				$slider = array('slider' => @serialize($ustaw));
				$settings = new Ustawienia();
				$settings->updateData($slider);
				$this->_redirect('/admin/slider/edytuj/');
				$this->view->ustawienia = $this->ustawienia = $this->common->getUstawienia();
			}
			$this->view->ustaw = @unserialize($this->ustawienia['slider']);
		}
	}
	
	function dodajAction()
	{
		$slider = new Slider();
		$this->view->slajdy = $slider->wypisz();
	}
}
?>