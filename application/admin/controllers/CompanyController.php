<?php
    
    class Admin_CompanyController extends Ogolny_Controller_Admin
	{        
        public function init()
		{
            parent::init();
            $this->view->baseUrl = $this->_request->getBaseUrl();
            $this->view->controller_title = 'Zdarzenia użytkowników';
            $this->modul = $this->_request->getParam('module');
            $this->controller = $this->_request->getParam('controller');
            $this->view->action = $this->action = $this->_request->getParam('action');
            $this->view->controller = $this->controller;
            $this->class = ucfirst($this->controller);

            $this->sendmail = false;
            $this->rowset = 30;
        }
        function __call($method, $args)
        {
            $this->_redirect($this->view->baseUrl.'/'.$this->modul.'/index/panel/');
        }
		
		public function eventsindexAction()
		{
			$produkty = new Produkty();
			$this->view->all = $produkty->wypiszProduktyDrukuj($this->lang, 99999);
		}
        
        public function eventsglobalAction()
		{
			if(($this->adminNazwa != 'admin' && $this->adminNazwa != 'bigcom'))
			$this->_redirect('/admin/index/');//panel/zakladka/1/');
			
			$this->view->headScript()->appendFile($this->_request->getBaseUrl().'/public/scripts/functionadmin_events.js', 'text/javascript');
			
			$szukanie = new Zend_Session_Namespace('szukanieAdminEvents');
			
			if($this->_request->getPost('submit'))
			{
				$szukanie->prodID = intval($this->_request->getPost('prodID', 0));
				$szukanie->jezyk = $this->_request->getPost('jezyk', '');
				$szukanie->domena = $this->_request->getPost('domena', '');
				$szukanie->dataOd = $this->_request->getPost('dataOd', '2014-01-01');
				$szukanie->dataDo = $this->_request->getPost('dataDo', date("Y").'-12-31');
				$szukanie->autor = $this->_request->getPost('autor', '');
				$szukanie->adres_ip = $this->_request->getPost('adres_ip', '');
				$szukanie->adres_mac = $this->_request->getPost('adres_mac', '');
				$szukanie->user_agent = $this->_request->getPost('user_agent', '');
				$this->_redirect('/admin/company/eventsglobal/');
			}
			
			if(!isset($szukanie->prodID)) $szukanie->prodID = 0;
			if(!isset($szukanie->jezyk)) $szukanie->jezyk = "";
			if(!isset($szukanie->domena)) $szukanie->domena = "";
			if(!isset($szukanie->dataOd)) $szukanie->dataOd = "2014-01-01";
			if(!isset($szukanie->dataDo)) $szukanie->dataDo = date("Y").'-12-31';
			if(!isset($szukanie->autor)) $szukanie->autor = "";
			if(!isset($szukanie->adres_ip)) $szukanie->adres_ip = "";
			if(!isset($szukanie->adres_mac)) $szukanie->adres_mac = "";
			if(!isset($szukanie->user_agent)) $szukanie->user_agent = "";
			
			$this->view->szukanie = $szukanie;
			
			$produkty = new Produkty();
			$produkty->id = $id = $szukanie->prodID;
			$this->view->produkt = $id > 0 ? $produkty->wypiszPojedyncza() : null;
		
            (int) $page = $this->_request->getParam('od', 0);
            $this->Object = new Events;
            $this->Object->rowset = $this->rowset;
			$this->view->strona = $page;
            $this->Object->page = $page * $this->rowset;
			
			$this->view->autorzy = $this->Object->wypiszAutorow();

            $this->view->all = $this->Object->rowsAdmin($szukanie);
			//var_dump($this->view->all);die();
            $this->view->strony = $this->Object->pagerAdmin($szukanie);
            $this->view->link = 'admin/'.$this->controller.'/'.$this->action;
        }
    }
?>