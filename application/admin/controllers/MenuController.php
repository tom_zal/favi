<?php 
class Admin_MenuController extends Ogolny_Controller_Admin
{
	public function init()
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		$this->view->headLink()->appendStylesheet($this->_request->getBaseUrl().'/public/styles/kategorie.css');
	}
	
	public function indexAction()
	{
		$this->view->r_dodaj = 'admin/menu/dodaj';
		
		$lista = new Menu();
		
		if($this->_request->getParam('del') > 0)
        {
			$lista->kasuj($this->_request->getParam('del'));
			$this->_redirect('/admin/menu/');
		}
		if($this->_request->getPost('pozycja'))
        {
			$lista->changePozycja($this->_request->getPost('id'), $this->_request->getPost('pozycja'));
			$this->_redirect('admin/menu/index');
		}
		if(($pozid = intval($this->_request->getParam('pozid'))) > 0)
		{
			$pozycja = intval($this->_request->getParam('pozycja', 0));
            $lista->edytuj($pozid, array('pozycja' => $pozycja));
			$this->_redirect('admin/menu/index');
        }
		if(($pokazid = intval($this->_request->getParam('pokazid'))) > 0)
		{
			$widoczny = @intval($this->_request->getParam('widoczny')?1:0);
            $lista->edytuj($pokazid, array('wyswietl' => $widoczny));
			$this->_redirect('admin/menu/index');
        }
		
		$lista->link = $this->_request->getBaseUrl();
		$lista->display_children_admin(0, 0); 
		$this->view->kategorie = $lista->menue;
		
		if($this->obConfig->menuOfertaMainEdycja)
		{
			$ustaw = $this->_request->getPost('ustaw');
			if(count($ustaw) > 0)
			{
				$menu = array('menu' => @serialize($ustaw));
				$settings = new Ustawienia();
				$settings->updateData($menu);
				$this->_redirect('admin/menu/index');
				$this->view->ustawienia = $this->ustawienia = $this->common->getUstawienia();
			}
			$this->view->ustaw = @unserialize($this->ustawienia['menu']);
		}
	}
	
	public function dodajAction()
    {
		$this->view->r_zapisz = 'zapisz';
		$this->view->r_lista = 'admin/menu/index/';
		
		$nowa = new Menu();
		$id = @intval($this->_request->getParam('id', 0));
		
		if($id > 0)
		{
			$path = $nowa->get_path($id);
			$this->view->path = $path;
		}
		if($this->_request->isPost())
        {
			$stringValidate = new Zend_Validate; 
 			$stringValidate->addValidator(new Zend_Validate_StringLength(1, null))
							->addValidator(new Zend_Validate_NotEmpty());
 			
 			if($stringValidate->isValid($this->_request->getPost('nazwa')))
            {
                $dane['nazwa'] = $this->_request->getPost('nazwa');
				$dane['skrot'] = $this->_request->getPost('skrot','');
				$dane['tekst'] = $this->_request->getPost('tekst','');

				$nowy = $nowa->dodaj_nowa($id, $dane);
				if($nowy['id'] == 0)
				{
					$this->view->blad_edycji = '<div id="lista_bledow">Istnieje już taka podstrona</div>';
				}
				else 
				{
					$this->view->error = '<div class="k_ok">Nowa kategoria została dodana</div>';
					$this->_redirect('/admin/menu/');
				}
 			}
			else
			{
 				$this->view->error = '<div class="k_blad">Pole nazwa jest wymagane </div>';
 			}
		}
		if($this->obConfig->menuSkrot)
		$this->view->short = $this->fcKeditor('skrot',"",300,'posredni',700);
        $this->view->edytor = $this->fcKeditor('tekst',"",600,'posredni',700);
	}
	 
	public function edytujAction()
	{
		$nowa = new Menu();
		$id = $this->_request->getParam('id', 0);
		
		if($id > 0)
		{		
			if($this->_request->isPost())
			{
				$stringValidate = new Zend_Validate; 
 				$stringValidate->addValidator(new Zend_Validate_StringLength(1, null))
								->addValidator(new Zend_Validate_NotEmpty());
				
 				if($stringValidate->isValid($this->_request->getPost('nazwa')))
				{
					$data = $nowa->showWybranaKategoria($id);
					$dane['nazwa'] = $this->_request->getPost('nazwa');
					$dane['skrot'] = $this->_request->getPost('skrot','');
					$dane['tekst'] = $this->_request->getPost('tekst','');
					$dane['data'] = $this->_request->getPost('data', date('Y-m-d H:i:s'));
					
					if($dane['nazwa'] != $data['nazwa'])
					{
						$route = new Routers();
						$ret = $route->edytuj($dane['nazwa'], 0, $data['link'], $data['link'], true);
						if(!empty($ret['id']))
						{
							$dane['link'] = $ret['link'];
						}
					}

					$nowa->edytuj($id, $dane);
					
					//$this->view->error = '<div class="k_ok">Edycja przebiegła pomyślnie</div>';
	 			}
				else
				{
	 				$this->view->error = '<div class="k_blad">Pole nazwa jest wymagane </div>';
	 			}
			}
            $data = $nowa->showWybranaKategoria($id);
			$this->view->kategoria = $data;
			if($this->obConfig->menuSkrot)
			$this->view->short = $this->fcKeditor('skrot',stripslashes($data['skrot']),300,'posredni',700);
            $this->view->edytor = $this->fcKeditor('tekst',stripslashes($data['tekst']),600,'posredni',700);
			
			$this->view->typ = $typ = 'menu';
			if($this->obConfig->podstronyGaleria)
			{
				$galeria = new Galeria();
				$this->view->blad_edycji = $this->common->galeriaSupport($id, $typ, $this->_request);
				$imgEdit = intval($this->_request->getParam('imgEdit', 0));
				if($imgEdit > 0) $this->view->img = $galeria->pobierzNazwe($imgEdit);
				$this->view->galeria = $galeria->wyswietlGalerie($id, 'desc', 0, $typ);
			}
			if($this->obConfig->podstronyVideo)
			{
				$video = new Video();
				$this->view->blad_edycji = $this->common->videoSupport($id, $typ, $this->_request);
				$vidEdit = $video->id = intval($this->_request->getParam('vidEdit', 0));
				if($vidEdit > 0) $this->view->video = $video->wypiszPojedynczego();
				$this->view->videos = $video->wypisz(false, $id, $typ);
			}
			$this->view->link = 'admin/menu/edytuj/id/'.$id;
			if($this->_request->isPost())
			if(@empty($this->view->error))
			if(@empty($this->view->blad_edycji))
			$this->_redirect($this->view->link);
			
			$this->view->r_zapisz = 'zapisz';
			if($this->view->kategoria['rodzic'] > 0)
			$this->view->r_dodaj = 'admin/menu/dodaj/id/'.$id;
			$this->view->r_usun = 'admin/menu/index/del/'.$id;
			$this->view->r_lista = 'admin/menu/index';
			$this->view->r_pokaz = $this->view->kategoria['link'];
		}
		else
		{
			$this->view->error = '<div class="k_blad">Nie wybrałeś kategorii do edycji</div>';
		}
	}
}	
?>