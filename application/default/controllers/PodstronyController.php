<?php
class PodstronyController extends Ogolny_Controller_Sklep
{
    public function init()
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        if (true && $this->obConfig->slajdy) {
                    $slider = new Slider($this->_module);
                    $this->view->slajdy = $slider->wypiszAktywne();
                }
                 if (true) {
                    $main = new Podstrony($this->_module);
                    $main->link = 'Logotypy-blok';
                    $this->view->logotypy = $main->getPodstrona();
                }
    }

    function indexAction()
	{
        $id = $this->_request->getParam('id');
        $menu = new Strony($this->module);
        $menu->lang = $this->lang;
        $this->view->main = $menu->showWybranaKategoria($id);
        $this->view->main->tekst = stripslashes($this->view->main->tekst);
    }
	
	function mapaAction()
	{
        $kategorie = new Kategorie($this->_module);
		$menu = new Menu($this->_module);
		$this->view->podstrony = $kategorie->podstronyAdmin('lp');
		$kategorie->showAll(0);
		$this->view->kategorie = $kategorie->katAll;
		$this->view->menuOferta = $menu->start();
		$this->view->ustawienia['title'] = $this->lang('mapa').' - '.$this->view->ustawienia['title'];
    }
	
	function showAction()
	{
	 	$id = $this->_request->getParam('id');

		$display = new Podstrony($this->_module);
		$display->lang = $this->lang;
		$display->link = $id;
		$this->view->html = $html = $display->getPodstrona();
		if($html === null) { $this->_helper->viewRenderer->setNoRender(); return; }
		$this->view->site = $html['link'];
		$this->view->link = $html['link'];
		$this->view->nawigacja = '<a href="'.$this->baseUrl.'/'.trim($html['link']).'">'.$this->arrow.$html['temat'].'</a>';
		if($html['rodzic'] > 0 && $html['parent'] == 'podstrony')
		{
			$display = new Podstrony($this->_module);
			$display->id = $html['rodzic'];
			$parent = $display->getPodstrona();
			$nawigacja = '<a href="'.$this->baseUrl.'/'.trim($parent['link']).'">'.$this->arrow.$parent['temat'].'</a>';
			$this->view->nawigacja = $nawigacja.$this->view->nawigacja;
		}
		if($html['rodzic'] > 0 && $html['parent'] == 'kategorie')
		{
			$display = new Kategorie($this->_module);
			$parent = $display->showWybranaKategoria($html['rodzic']);
			$nawigacja = '<a href="'.$this->baseUrl.'/'.trim($parent['link']).'">'.$this->arrow.$parent['nazwa'].'</a>';
			$this->view->nawigacja = $nawigacja.$this->view->nawigacja;
		}
		$this->view->ustawienia['title'] = $html['temat'].' - '.$this->ustawienia['title'];
		if($id == 'NaszeSklepy') $this->_forward('sklep');
		if($id == 'Galeria') $this->_forward('galeria');
		//if($id == 'Galeria') $this->_redirect('Oferta/podstrona/galeria');
		//if($id == 'Newsletter') $this->_forward('newsletter');
		if($id == 'Newsletter')
		{
			echo $this->render('newsletter');
			$this->_helper->viewRenderer->setNoRender();
		}
		//if($id == $this->lang('Oferta')) $this->view->oferta = true;		
		
		$this->view->typ = $typ = 'podstrony';
		if($this->obConfig->podstronyGaleria)
		{
			$galeria = new Galeria($this->module);
			$this->view->galeria = $galeria->wyswietlGalerie($this->view->html['id'], 'desc', 0, $typ, true);
		}
		if($this->obConfig->podstronyVideo)
		{
			$video = new Video($this->module);
			$this->view->videos = $video->wypisz(true, $this->view->html['id'], $typ, true);
		}
                
		//if($id == $this->lang('kontakt')) $this->render('show');
		//if($id == $this->lang('kontakt')) $this->_forward('formularz');
	}
	
	function videosAction() 
	{
		$this->view->headScript()->appendFile($this->_request->getBaseUrl().'/public/scripts/function_video.js','text/javascript');
		
		$video = new Video($this->module);
		$videos = $video->wypisz(true, 0, '', true);
		$this->view->videos = $videos;
	}
	
	function szukajAction() 
	{
		$fraza = $this->_request->getPost('co', '');
		if($fraza == $this->fraza) $fraza = '';
		if(@!empty($fraza))
		{
			$_SESSION['fraza'] = $fraza;
			$this->_redirect($this->URI);
		}
		if(@!empty($_SESSION['fraza']))
		{
			$fraza = $_SESSION['fraza'];
		}
		$this->view->nazwa = $fraza;
		$this->view->wyniki = $this->common->szukajTresci($fraza);
	}
	
	function newsletterAction() 
	{

	}
	 
	function galeriaAction() 
	{
	 	$id = intval($this->_request->getParam('nr', 0));
	 	$slider = new Slider($this->_module);
		if($this->obConfig->galeriaRodzaje)
		{
			$sliderTypy = new Slidertypy($this->_module);
			if($id > 0)
			{
				$this->view->typ = $sliderTypy->pojedyncza($id);
				$this->view->galeria = $slider->wypiszAktywne('galeria', $id);
				$this->render('galeria');
			}
			else
			{
				$this->view->galerie = $sliderTypy->wypisz();
				$glowne = $slider->wypiszGlowneAll('galeria');
				$this->view->glowne = $this->common->sortByPole($glowne, 'rodzaj');
				//var_dump($this->view->glowne);die();
				$this->render('galerie');
			}
		}
		else $this->view->galeria = $slider->wypiszAktywne('galeria');
		//var_dump($this->view->galeria);
		$this->view->ustawienia['title'] = 'Galeria - '.@$this->view->ustawienia['title'];
	}
	 
	function stronaAction() 
	{
	 	$id = $this->_request->getParam('id', '');
		//$this->aktualnosciPage = $this->common->getPodstrona('Aktualności', true);
		//var_dump($id);var_dump($this->aktualnosciPage);die();
		$site = '';
		$title = '';
		$strona = strtolower($id);
		if($id == 'Aktualnosci')//$this->aktualnosciPage['link']
		{
			$display = new Aktualnosci($this->module);
			$title = $site = 'Aktualności';//$this->lang('Aktualnosci');
			$title = $site = $this->aktualnosciPage['temat'];
			$link = 'Aktualnosc';//$this->lang('Aktualnosc');
			$strona = 'aktualnosci';
                        $this->view->katNews = true;
                        
		}
		if($id == 'Artykuly')
		{
			$display = new Artykuly($this->module);
			$title = $site = $this->lang('Artykuly');
			$link = 'Artykul';//$this->lang('Artykul');
		}
		if($id == 'Porady')
		{
			$display = new Porady($this->module);
			$title = $site = $this->lang('Porady');
			$link = 'Porada';//$this->lang('Porada');
		}
		if($id == 'Realizacje')
		{
			$display = new Realizacje($this->_module);
			$title = $site = $this->lang('Realizacje');
			$link = 'Realizacja';//$this->lang('Realizacja');
		}
                
                
		$link = $this->common->makeLink($link);
		$site = $this->common->makeLink($site);
		$this->view->ustawienia['title'] = $title.' - '.$this->view->ustawienia['title'];
		$display->lang = $this->lang;
		$display->link = $id;
		$this->view->html = $display->wypiszAktualnosci($this->lang);
		//if($this->view->html == null) $this->_helper->viewRenderer->setNoRender();
		$this->view->nawigacja = '<a href="'.$this->view->baseUrl.'/'.$site.'/">'.$this->arrow.$title.'</a>';
		$this->view->id = @$link;
		$this->view->link = 'Aktualnosci';//@strtolower($id);
		$this->view->strona = @$strona;
		$this->view->site = @$site;
		$this->view->title = @$title;
                
                
		//if(true) $this->view->oferta = true;
	}
	 
	function podstronaAction() 
	{
		//if(true) $this->view->oferta = true;
		$id = $this->_request->getParam('id', 0);
	 	$nr = $this->_request->getParam('nr', 0);
		$site = '';
		$title = '';
		$strona = '';
               
		if($id == 'Aktualnosc') 
		{
			$display = new Aktualnosci($this->module);
			$this->aktualnosciPage = $this->common->getPodstrona('Aktualności', true);
			$title = $site = 'Aktualności';//$this->lang('Aktualnosci');
			$title = $site = $this->aktualnosciPage['temat'];
			$strona = 'Aktualnosci';
		}
		if($id == 'Artykul')
		{
			$display = new Artykuly($this->module);
			$title = $site = $this->lang('Artykuly');
			$strona = 'Artykuly';
		}
		if($id == 'Porada')
		{
			$display = new Porady($this->module);
			$title = $site = $this->lang('Porady');
			$strona = 'Porady';
		}
		if($id == 'Realizacja')
		{
			$display = new Realizacje($this->module);
			$title = $site = $this->lang('Realizacje');
			$strona = 'Realizacje';
		}
		$link = $this->common->makeLink($site);
		$display->lang = $this->lang;
		$display->link = $id;
		$this->view->html = $display->pojedyncza($nr);
		//var_dump($this->view->html->toArray());
		if($this->view->html == null)
		{
			$this->_helper->viewRenderer->setNoRender();
			$this->view->ustawienia['title'] = $site.' - '.$this->view->ustawienia['title'];
		}
		else
		{
			$this->view->ustawienia['title'] = $this->view->html['temat'].' - '.$this->view->ustawienia['title'];
			$html = $this->view->html->toArray();
			$html['nazwa'] = stripslashes($html['temat']);
			$html['skrot'] = stripslashes($html['skrot']);
			$html['tekst'] = stripslashes($html['tekst']);			
			$this->view->link = $id.'/nr/'.$nr;
			$this->view->nawigacja = '<a href="'.$this->view->baseUrl.'/'.$link.'/">'.$this->arrow.$title.'</a>';
			$this->view->nawigacja.= '<a href="'.$this->view->baseUrl.'/'.$this->view->link.'/">'.$this->arrow.$html['temat'].'</a>';
			$this->view->html = $html;
			
			$this->view->typ = $typ = strtolower($strona);
			if($this->obConfig->podstronyGaleria)
			{
				$galeria = new Galeria($this->module);
				$this->view->galeria = $galeria->wyswietlGalerie($this->view->html['id'], 'desc', 0, $typ, true, false);
			}
			if($this->obConfig->podstronyVideo)
			{
				$video = new Video($this->module);
				$this->view->videos = $video->wypisz(true, $this->view->html['id'], $typ, true);
			}
		
			$this->render('show');
		}
		$this->view->id = @$id;
		$this->view->nr = @$nr;
		$this->view->site = @$site;
		$this->view->link = @$link;
		$this->view->title = @$title;
		$this->view->strona = @$strona;
		if(false)
		{
			$this->view->title = $this->lang('pozostale').' '.@mb_strtolower($title, 'UTF-8');
			$this->view->html = $display->wypiszAktualnosci($this->lang, 1, $nr);
			$this->render('strona');
		}
	}
	 
	function sklepAction() 
	{
		$wojew = $this->_request->getParam('woj');
		if($wojew)
		{
			$wojew = $this->wojew[$wojew];
			$dealer = new Dealerzy($this->module);
			$this->view->dealerzy = $dealer->wypiszWoj($wojew);
			$this->view->woj = $wojew;
		}
		$this->view->link = 'Nasze sklepy';
		$this->view->ustawienia['title'] = 'Nasze sklepy - '.$this->view->ustawienia['title'];
	}
	 
	function menuAction()
	{
	 	$id = $this->_request->getParam('id');
		$url = $this->_request->getParam('paramurl');
			
		if($id == 'Menu')
		{
			$display = new Podstrony($this->_module);
			$display->lang = $this->lang;
			$display->link = $id;
			$html = $display->getPodstrona($id)->toArray();
			$html['nazwa'] = $html['temat'];
			$html['tekst'] = stripslashes($html['tekst']);
			$this->view->html = $html;
			$this->view->site = 'Menu';
			$this->view->link = 'Menu';
			$this->view->nawigacja = '<a href="'.$this->view->baseUrl.'/Menu/">'.$this->arrow.'Oferta</a>';
		}
		else
		{
			$display = new Menu($this->_module);
			$display->lang = $this->lang;
			$display->link = $id;
			$this->view->html = $kategoria = $menu = $display->showWybranaKategoriaLink($id);
			//if(@$this->view->html->disable) $this->_redirect('/');
			$this->view->site = 'Menu';
			$this->view->link = $this->view->html['link'];
			$this->view->nawigacja = 
			'<a href="'.$this->view->baseUrl.'/'.$menu['link'].'/">'.$this->arrow.$menu['nazwa'].'</a>';
			while($kategoria['rodzic'] > 0)
			{
				$kategoria = $display->showWybranaKategoria($kategoria['rodzic']);
				$this->view->nawigacja = '<a class="navi_bg" href="'.$this->view->baseUrl.'/'.$kategoria['link'].'/">'.$this->arrow.ucfirst($kategoria['nazwa']).'</a>'.$this->view->nawigacja;
				$this->view->nazwa_kategorii = $kategoria['nazwa'].' / '.$this->view->nazwa_kategorii;
			}
			//$this->view->nazwa_kategorii .= $nazwa;
			//if(true) $this->view->oferta = true;
			$id = str_replace('-'.$this->lang, '', $id);
			//if($id == $this->lang('Oferta')) $this->view->oferta = true;
			//var_dump($id);die();
			$this->view->typ = $typ = 'menu';
			if($this->obConfig->podstronyGaleria)
			{
				$galeria = new Galeria($this->module);
				$glowne = true;//($id == $this->lang('galeria') || $id == $this->lang('o_nas'));
				$this->view->galeria = $galeria->wyswietlGalerie($menu['id'], 'desc', 0, $typ, true, $glowne);
			}
			if($this->obConfig->podstronyVideo)
			{
				$video = new Video($this->module);
				$this->view->videos = $video->wypisz(true, $menu['id'], $typ, true);
			}
			//if($id == $this->lang('galeria')) $this->view->html = null;
			if($id == $this->lang('oferta2'))
			{
				$this->view->title = $this->lang('oferta');
				$this->view->html = @$this->menu->wypiszMenu($menu['id']);
				//var_dump($this->view->html);die();
				$this->view->link = $this->view->strona = 'menu';
				$this->render('strona');
				return;
			}
			$this->render('show');
			//if($id == $this->lang('kontakt')) $this->render('show');
			//if($id == $this->lang('kontakt')) $this->_forward('formularz');
		}
		$this->view->ustawienia['title'] = $this->lang('Oferta').' - '.$this->view->ustawienia['title'];
	}
	 
	function formularzAction() 
	{
	 	$id = $this->_request->getParam('id');
		$display = new Podstrony($this->_module);
		$display->lang = $this->lang;
		$display->link = $id;//$this->lang('kontakt');
		if(@empty($this->view->link))
		{
			$this->view->html = $html = $display->getPodstrona($id);
			$this->view->site = $html['link'];
			$this->view->link = 'Kontakt';
			$this->view->nawigacja = 
			'<a href="'.$this->view->baseUrl.'/'.$html['link'].'/">'.$this->arrow.$html['temat'].'</a>';
			$this->view->ustawienia['title'] = $this->lang('formularz').' - '.$this->ustawienia['title'];
		}
		//var_dump($this->view->link);
		
		$this->view->email = '';
		if($this->view->zalogowanyID > 0)
		{
			$kontr = new Kontrahenci($this->module);
			$kontr->id = $this->view->zalogowanyID;
			$kontrahent = $kontr->wypiszPojedynczyArray();
			$dane['nazwa'] = !empty($kontrahent['nazwa_firmy']) ? $kontrahent['nazwa_firmy'] : trim($kontrahent['nazwisko'].' '.$kontrahent['imie']);
			$dane['email'] = $kontrahent['email'];
			$dane['telefon'] = $kontrahent['telefon'];
			$this->view->dane = $dane;
		}
		else $this->view->dane = null;

		if($this->_request->getPost('sprzedawcaZapytanie'))
		{
			//var_dump($_POST);
			//$daneSprz['od'] = $this->view->zalogowanyID > 0 ? $this->view->zalogowany : 'gość';
			$daneSprz['nazwa'] = $this->_request->getPost('nazwa');
			$daneSprz['adres'] = $this->_request->getPost('adres');
			$daneSprz['email'] = $this->_request->getPost('email');
			$daneSprz['telefon'] = $this->_request->getPost('telefon');
			$daneSprz['temat'] = $this->_request->getPost('temat');
			$daneSprz['opis'] = $this->_request->getPost('opis');
			$daneSprz['wyslijDoSiebie'] = $this->_request->getPost('wyslijDoSiebie');
			$captchaKod = $this->_request->getPost('captchaKod', '');
			$captchaKodOK = $this->_request->getPost('captchaKodOK', '');
			
			if($this->obConfig->formCaptchaAudio)
			{
				include_once($_SERVER['DOCUMENT_ROOT'].$this->baseUrl.'/public/scripts/securimage/securimage.php');
				$securimage = new Securimage();
				$captchaOK = $securimage->check($captchaKod); //$_POST['captcha_code']
				$captchaKodOK = $captchaKod;
			}
			
			if(empty($daneSprz['nazwa']) || (empty($daneSprz['email']) && empty($daneSprz['telefon'])) || empty($daneSprz['opis']))
			{
				$this->view->error = $this->lang('pola_wymagane_wypelnij');
			}
			elseif($this->obConfig->formCaptcha)				
			{
				if(($this->obConfig->formCaptchaAudio && @!$captchaOK) || ($captchaKod != $captchaKodOK))
				$this->view->error = $this->lang('bledny_kod').'!';
			}
			if(@!empty($this->view->error))
			{
				$this->view->error = '<div class="k_blad">'.$this->view->error.'</div>';
				$this->view->dane = $daneSprz;
			}
			else
			{
				$wiadomosci = new Wiadomosci($this->module);
				$blad = $wiadomosci->sprzedawcaZapytanie($daneSprz);
				if(!empty($blad) && $daneSprz['wyslijDoSiebie'])
				$wiadomosci->sprzedawcaZapytanie($daneSprz, true);
				if(empty($blad))
				{
					$this->view->error = $this->lang('zapytanie_wyslane').'!';
					$this->view->error = '<div class="k_ok">'.$this->view->error.'</div>';
				}
				else
				{
					$this->view->error = $this->lang('blad').': '.$blad;
					$this->view->error = '<div class="k_blad">'.$this->view->error.'</div>';
				}
			}
		}
		
		if($this->obConfig->formCaptcha && !$this->obConfig->formCaptchaAudio)
		{
			$captcha = new Zend_Captcha_Image(array
			(
				'font' => $this->path.'/public/admin/captcha/font.ttf'
			));

			$captcha->setImgDir($this->path.'/public/admin/captcha/images');
			$captcha->setWordlen(6);
			$captcha->setWidth(200);
			$captcha->setHeight(40);
			$captcha->setLineNoiseLevel(5);
			$captcha->setDotNoiseLevel(5);

			$this->view->captchaImg = $captcha->generate();
			$this->view->captchaKod = $captcha->getWord();
		}
	}
	 
	function sprzedawcaAction() 
	{ 
	 	$id = $this->_request->getParam('id');
		$display = new Wizytowka($this->module);
		$display->lang = $this->lang;
		$display->link = $id;
		$this->view->html = $display->getWizytowka($id);
		$this->view->site = $this->view->html['link'];
		$this->view->ustawienia['title'] = 'Sprzedawca - '.$this->view->ustawienia['title'];
		
		$this->view->email = '';
		if($this->view->zalogowanyID > 0)
		{
			$kontr = new Kontrahenci($this->module);
			$kontr->id = $this->view->zalogowanyID;
			$kontrahent = $kontr->wypiszPojedynczyArray();
			$this->view->email = $kontrahent['email'];
		}

		if($this->_request->getPost('sprzedawcaZapytanie'))
		{
			//var_dump($_POST);
			$daneSprz['od'] = $this->view->zalogowanyID > 0 ? $this->view->zalogowany : 'gość';
			$daneSprz['temat'] = $this->_request->getPost('temat');
			$daneSprz['opis'] = $this->_request->getPost('opis');
			$daneSprz['email'] = $this->_request->getPost('email');
			
			if(empty($daneSprz['temat']) || empty($daneSprz['opis']) || empty($daneSprz['email']))
			{
				$this->view->sprzedawcaError = 'Wypełnij wszystkie pola!';
			}
			else
			{
				$wiadomosci = new Wiadomosci($this->module);
				$wiadomosci->sprzedawcaZapytanie($daneSprz, $this->view->html);
				
				$this->view->sprzedawcaError = 'Dziękujemy!';
			}
		}	
	}
	 
	function opinieAction()
	{ 
	 	$id = $this->_request->getParam('id');
		$this->view->ustawienia['title'] = 'Opinie - '.$this->view->ustawienia['title'];
		
		if($this->_request->getPost('dodajOpinie'))
		{
			//var_dump($_POST);
			$daneOpinie['id_prod'] = 0;
			$daneOpinie['id_kontr'] = $this->view->zalogowanyID;
			$daneOpinie['imie'] = $this->_request->getPost('opinieImie');
			$daneOpinie['czas'] = $this->_request->getPost('opinieCzasScore');
			$daneOpinie['kontakt'] = $this->_request->getPost('opinieKontaktScore');
			$daneOpinie['produkt'] = $this->_request->getPost('opinieProduktScore');
			$daneOpinie['opis'] = $this->_request->getPost('opiniaOpis');
			
			if(empty($daneOpinie['imie']))
			{
				$this->view->opiniaError = 'Wpisz swoje imię!';
			}
			else
			{
				$opinie = new Opinie($this->module);
				$opinie->dodajOpinie($daneOpinie);
				
				$wiadomosci = new Wiadomosci($this->module);
				$wiadomosci->nowaOpinia($daneOpinie);
				
				$this->view->opiniaError = 'Dziękujemy!';
			}
		}
			
		$display = new Opinie($this->module);
		$this->view->opinie = $display->wypiszOpinieForProdukt();
		$this->view->site = 'Opinie';
	}
	 
	function polecAction()
	{
	 	$id = $this->_request->getParam('nr', 0);
		$ids = $this->_request->getParam('ids', '');
		$this->view->ustawienia['title'] = 'Poleć - '.$this->view->ustawienia['title'];
		
		$settings = new Ustawienia($this->module);
		$ustawienia = $settings->showData()->toArray();
		
		$this->view->name = '';
		$this->view->email = '';
		if($this->view->zalogowanyID > 0)
		{
			$kontr = new Kontrahenci($this->module);
			$kontr->id = $this->view->zalogowanyID;
			$kontrahent = $kontr->wypiszPojedynczyArray();
			$this->view->name = $kontrahent['imie'].' '.$kontrahent['nazwisko'];
			$this->view->email = $kontrahent['email'];
			
			$znajomy = new Znajomi($this->module);
			$this->view->znajomi = $znajomy->wypiszZnajomychKlienta($this->view->zalogowanyID);
		}
		
		if($id > 0)
		{
			$produkt = new Produkty($this->module);
			$produkt->id = $id;
			$prod = $produkt->wypiszPojedyncza();
			$link[] = 'http://'.$_SERVER['HTTP_HOST'].$this->view->baseUrl.'/'.$prod['link'];
			$this->view->link = $link;
			$gal = new Galeria($this->module);
			$galeria[] = $gal->wyswietlGalerie($id);
			$this->view->galeria = $galeria;
		}
		if($ids)
		{
			$produkt = new Produkty($this->module);
			$gal = new Galeria($this->module);
			$ids = explode('-', $ids);
			foreach($ids as $id)
			if(intval($id) > 0)
			{
				$produkt->id = $id;
				$prod = $produkt->wypiszPojedyncza();
				$link[] = 'http://'.$_SERVER['HTTP_HOST'].$this->view->baseUrl.'/'.$prod['link'];				
				$galeria[] = $gal->wyswietlGalerie($id);			
			}
			$this->view->link = $link;
			$this->view->galeria = $galeria;
		}

		if($this->_request->getPost('polecZapytanie'))
		{
			//var_dump($_POST);
			$danePolec['link'] = $this->view->link;
			$danePolec['opis'] = $this->_request->getPost('opis');
			$danePolec['znajomy'] = $this->_request->getPost('znajomy');
			$danePolec['email'] = $this->_request->getPost('email');
			$danePolec['nazwa'] = $this->_request->getPost('nazwa');
			$daneMaile = $this->_request->getPost('maile');
			$daneMaileAll = $this->_request->getPost('all');
			
			if(empty($danePolec['znajomy']) && count($daneMaile) == 0 && !$daneMaileAll)
			{
				$this->view->polecError = 'Wpisz e-maila znajomego!';
			}
			if(empty($danePolec['email']) || empty($danePolec['nazwa']))
			{
				$this->view->polecError = 'Wypełnij wszystkie pola!';
			}
			if(!isset($this->view->polecError))
			{
				if($danePolec['znajomy'] == 'Wszyscy moi znajomi' || $daneMaileAll)
				{
					$znajomy = new Znajomi($this->module);
					$result = $znajomy->wypiszZnajomychKlienta($this->view->zalogowanyID);
					if(count($result) > 0) foreach($result as $row) $maile[] = $row['mail'];
				}
				else 
				{
					if(count($daneMaile) > 0) foreach($daneMaile as $mail => $on) $maile[] = $mail;
				}
				if(!empty($danePolec['znajomy']) && $danePolec['znajomy'] != 'Wszyscy moi znajomi') 
				$maile[] = $danePolec['znajomy'];
				
				$wiadomosci = new Wiadomosci($this->module);
				$znajomy = new Znajomi($this->module);
				//var_dump($maile);return;
				if(isset($maile))
				foreach($maile as $mail)
				{
					$danePolec['znajomy'] = $mail;
					$wiadomosci->polecZapytanie($danePolec, $this->view->galeria);
					
					if($this->view->zalogowanyID > 0)
					{			
						$istnieje = $znajomy->szukajZnajomego($danePolec['znajomy']);
						if($istnieje == null || count($istnieje) == 0)
						{
							$dane = array('id_kontr' => $this->view->zalogowanyID, 'mail' => $danePolec['znajomy']);
							$znajomy->dodajZnajomego($dane);
						}
					}
				}
				$this->view->polecError = 'E-mail został wysłany!';
			}
		}
		$this->view->site = 'Twój znajomy poleca Ci';
	}
	
	function polecsklepAction()
	{
		$this->view->ustawienia['title'] = 'Poleć sklep - '.$this->view->ustawienia['title'];
		
		$settings = new Lojalnoscustawienia($this->module);
		$ustawienia = $settings->wypisz();
		
		$display = new Podstrony($this->module);
		$display->lang = $this->lang;
		$display->link = $this->view->zalogowanyID ? 'Polec-sklep-zalogowany-lojalnosc' : 'Polec-sklep-lojalnosc';
		$this->view->polec = $display->getPodstrona();
        $this->view->polec->tekst = stripslashes($this->view->polec->tekst);
		$this->view->polec->tekst = $settings->changePodstrona($this->view->polec->tekst);
		
		$this->view->name = '';
		$this->view->email = '';
		if($this->view->zalogowanyID > 0)
		{
			$kontr = new Kontrahenci($this->module);
			$kontr->id = $this->view->zalogowanyID;
			$kontrahent = $kontr->wypiszPojedynczyArray();
			$this->view->name = $kontrahent['imie'].' '.$kontrahent['nazwisko'];
			$this->view->email = $kontrahent['email'];
		}

		if($this->_request->getPost('polecZapytanie'))
		{
			//var_dump($_POST);
			$danePolec['opis'] = $this->_request->getPost('opis');
			$danePolec['znajomy'] = $this->_request->getPost('znajomy');
			$danePolec['email'] = $this->_request->getPost('email');
			$danePolec['nazwa'] = $this->_request->getPost('nazwa');
			$this->view->name = $danePolec['nazwa'];
			$this->view->email = $danePolec['email'];
			$this->view->znajomy = @$danePolec['znajomy'][0];
			$this->view->opis = $danePolec['opis'];
			
			if(@empty($danePolec['znajomy'][0]))
			{
				$this->view->polecError = 'Wpisz e-maila osoby polecanej!';
			}
			if(empty($danePolec['email']) || empty($danePolec['nazwa']))
			{
				$this->view->polecError = 'Wypełnij wszystkie pola!';
			}
			if(!isset($this->view->polecError))
			{
				$daneMaile = $danePolec['znajomy'];
				//var_dump($maile);
				$wiadomosci = new Wiadomosci($this->module);
				$polecenia = new Lojalnoscpolecenia($this->module);				
				if(count($daneMaile) > 0)
				foreach($daneMaile as $mail)
				{
					if(empty($mail)) continue;
					$danePolec['znajomy'] = $mail;					
					//$polec = $polecenia->wypiszPolecenie($this->view->zalogowanyID, $mail);
					if(false && count($polec) > 0)
					{
						$blad = 'Już poleciłeś osobie z podanym adresem e-mail nasz sklep!';
						$blad.= '<br/>Osoba ta aby otrzymać rabat może zarejestrować się do '.$polec[0]['termin'];
					}
					else
					{
						$blad = $wiadomosci->polecSklep($danePolec, $kontrahent);
						if(false && @empty($blad))
						{
							$polecDane['polecajacy'] = $this->view->zalogowanyID;
							$polecDane['polecany'] = $mail;
							$termin = mktime(date('H'),date('i'),0,date('m'),
											date('d')+$ustawienia['waznosc_polecenia'],date('Y'));
							$polecDane['termin'] = date('Y-m-d', $termin);
							//$polecenia->dodaj($polecDane);
						}
					}
				}
				if(@!empty($blad)) $this->view->polecError = $blad;
				else $this->view->polecError = 'E-mail został wysłany!';
			}
		}		
		$this->view->site = 'Poleć nasz sklep swojemu znajomemu!';
		$this->view->nawigacja = '<a href="'.$this->view->baseUrl.'/Polec-sklep">'.$this->arrow.'poleć sklep</a>';
	}
}
?>