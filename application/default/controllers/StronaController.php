<?php
class StronaController extends Ogolny_Controller_Sklep
{
    public function init()
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
    }

    function regulaminAction()
	{
        $link = "regulamin";
        $main = new Podstrony();
        $main->link = trim($link);
        $this->view->main = $main->getPodstrona();
        $this->view->main->tekst = stripslashes($this->view->main->tekst);
    }

    function ofirmieAction()
	{
        $link = "ofirmie";
        $main = new Podstrony();
        $main->link = trim($link);
        $this->view->main = $main->getPodstrona();
        $this->view->main->tekst = stripslashes($this->view->main->tekst);
    }
	
	function cvAction() 
	{
		$cv = new CV();
		$cv->id = 1;
		$koment = $cv->wypiszPojedynczy();
		$koment['data_ur'] = $koment['data_ur_opis'];
		$this->view->koment = $koment;
		
	 	$id = $this->_request->getParam('id');
		$main = new Podstrony();
		$main->link = 'Cv';
		$this->view->html = $main->getPodstrona();
		$this->view->html->short = stripslashes($this->view->html->short);
		$this->view->html->tekst = stripslashes($this->view->html->tekst);
		
		$this->view->dane = null;
		if($this->view->zalogowanyID > 0)
		{
			$kontr = new Kontrahenci();
			$kontr->id = $this->view->zalogowanyID;
			$kontrahent = $kontr->wypiszPojedynczyArray();
			$kontrahent['data_ur'] = '';
			$kontrahent['woj'] = 'podkarpackie';
			$kontrahent['wyksztalcenie'] = '';
			$kontrahent['doswiadczenie'] = '';
			$kontrahent['umiejetnosci'] = '';
			$kontrahent['zainteresowania'] = '';
			$kontrahent['opis'] = '';
		}
		$kontrahent['woj'] = 'podkarpackie';
		$this->view->dane = $kontrahent;

		//var_dump($_FILES);
		//var_dump($_POST);		
		//$this->view->file = '';
		$cv = new Zend_Session_Namespace('cvFile');
		if($this->_request->isPost())
		$cv->file = $this->_request->getPost('fileCVName', '');
				
		if(isset($_FILES['fileCV']))
		{
			$handle = new upload($_FILES['fileCV']);						
			if ($handle->uploaded) 
			{			
				$handle->file_new_name_body = 'image';
				$handle->image_resize = true;
				$handle->image_x = 320;
				$handle->image_y = 240;
				$handle->image_ratio_y = true;
				$handle->process('../public/admin/cv/');
				$cv = new Zend_Session_Namespace('cvFile');
				$cv->file = $handle->file_dst_name;
			}
		}
		$cv = new Zend_Session_Namespace('cvFile');
		if(isset($cv->file)) $this->view->file = $cv->file;
		
		if($this->_request->getPost('cv'))
		$this->view->dane = $this->_request->getPost('cv');
		
		//if(false)
		if($this->_request->getPost('cvDane'))
		{
			//var_dump($_POST);
			$od = $this->view->zalogowanyID > 0 ? $this->view->zalogowany : 'gość';
			$daneCV = $this->_request->getPost('cv');
			$daneCV['data_ur'] = $daneCV['rok'].'-'.$daneCV['miesiac'].'-'.$daneCV['dzien'];
			unset($daneCV['rok']);
			unset($daneCV['miesiac']);
			unset($daneCV['dzien']);
			foreach($daneCV as $id => $dana)
			if($daneCV[$id] == $koment[$id]) $daneCV[$id] = '';
			
			if(empty($daneCV['imie']) || $daneCV['imie'] == $koment['imie'] 
			|| empty($daneCV['nazwisko']) || $daneCV['nazwisko'] == $koment['nazwisko'] 
			|| empty($daneCV['email']) || $daneCV['email'] == $koment['email'])
			{
				$this->view->cvError = 'Wypełnij wszystkie wymagane pola!';
			}
			if(empty($daneCV['plec']))
			{
				$this->view->cvError = 'Wybierz płeć!';
			}
			if(!isset($this->view->file) || empty($this->view->file))
			{
				$this->view->cvError = 'Dodaj zdjęcie - zdjęcie jest wymagane!';
			}
			if(!isset($this->view->cvError))
			{
				//var_dump($this->view->file);
				$srcFile =  '../public/admin/cv/_'.$this->view->file;
				list($width, $height) = getimagesize($srcFile);
				$newWidth = 320;
				$newHeight = ($newWidth / $width) * $height;
				$thumb = imagecreatetruecolor($newWidth, $newHeight);
				$source = imagecreatefromjpeg($srcFile);				
				$targetPath = $_SERVER['DOCUMENT_ROOT'].$this->view->baseUrl.'/public/admin/cv/';
				$targetFile =  str_replace('//','/',$targetPath).$this->view->file;
				imagecopyresized($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
				imagejpeg($thumb, $targetFile, 95);
		
				$wiadomosci = new Wiadomosci();
				$wiadomosci->cvZapytanie($daneCV, $od, $this->view->file);
				
				$daneCV['img'] = $this->view->file;
				$cv = new CV();
				$cv->dodaj($daneCV);
				
				$this->view->cvError = 'Dziękujemy!';
				//$this->_redirect('/Cv-odpowiedz/');
				//$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/Cv-odpowiedz/');
			}			
		}
	}
}
?>