<?php
class AnkietaController extends Ogolny_Controller_Sklep
{
	public function init() 
	{
		parent::init();
		$this->view->baseUrl = $this->_request->getBaseUrl();
	}
	
	public function indexAction() 
	{
		$this->view->nawigacja = '<a href="'.$this->view->baseUrl.'/Ankieta">'.$this->arrow.'Ankieta</a>';
		$this->view->ustawienia['title'] = 'Ankieta - '.$this->view->ustawienia['title'];

		//var_dump($_POST);
		if($this->_request->isPost()) 
		{
			$id = $this->_request->getPost('ankietaID');
			$odpowiedz = $this->_request->getPost('odpowiedz');
			$objAnkietaodpowiedzi = new Ankietaodpowiedzi($this->module);
			$objAnkietaodpowiedzi->id = $odpowiedz;
			$objAnkietaodpowiedzi->edytujGlosy();
			$objAnkieta = new Ankieta($this->module);

			$objAnkieta->id = $id;
			$this->view->ankieta = $objAnkieta->wypiszPojedynczy();

			$objAnkietaodpowiedzi->id = $id;
			$tabraport = $objAnkietaodpowiedzi->wypiszWszystko();
			//var_dump($tabraport);

			$tabtemp = array();
			$tabpom = array();
			$raport = array();

			for($i=0; $i<count($tabraport); $i++) 
			{
				if(in_array($tabraport[$i]['id'], $tabpom))
				{
					$tabtemp[$tabraport[$i]['id']]['glosy'] += $tabraport[$i]['glosy'];
				} 
				else 
				{
					$tabtemp[$tabraport[$i]['id']]['nazwaod'] = $tabraport[$i]['nazwaod'];
					$tabtemp[$tabraport[$i]['id']]['glosy'] = $tabraport[$i]['glosy'];

					$tabpom[] .= $tabraport[$i]['id'];
				}
			}

			$j = 0;
			foreach($tabtemp as $key => $value) 
			{
				$raport[$j]['id'] = $key;
				$raport[$j]['nazwa'] = $value['nazwaod'];
				$raport[$j]['ilosc'] = $value['glosy'];
				$j++;
			}

			$glosy = 0;
			foreach ($raport as $key => $value) 
			{
				$tabpom[$key] = $value['ilosc'];
				$glosy += $value['ilosc'];
			}

			$this->view->wszystkie = $glosy;
			//var_dump($this->view->wszystkie);

			array_multisort($tabpom, SORT_DESC, $raport);

			$this->view->ilosc = round(count($raport)/3);
			$this->view->raport = $raport;
			//var_dump($this->view->ilosc);
			//var_dump($this->view->raport);

			if(!empty($raport)) 
			{
				if($raport[0]['ilosc'] != 0)
					$jednostka = 718/$raport[0]['ilosc'];
				else
					$jednostka = 0;
				$this->view->jednostka = $jednostka;
			}
			//var_dump($this->view->jednostka);
		} 
		else 
		{
			$this->_redirect('/');
		}
	}
}    
?>