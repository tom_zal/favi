<?php

class OfertaController extends Ogolny_Controller_Sklep {

    public $sortowanie, $szukanie, $usedFilter;

    public function init() {
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        $this->view->site = 'Oferta';
        $this->view->oferta = true;
        $this->view->ilu = 4;

        $this->test = $this->_request->getParam('test', 0);

        $tryb = @strval($this->_request->getParam('action'));
        if ($tryb == 'index' || $tryb == 'szukaj' || $tryb == 'kategorie')
            $tryb = '';
        $this->view->tryb = $this->tryb = 'tryb' . strtolower($tryb);
        $this->tryby = array('', 'nowosci', 'promocje', 'promocjenet', 'specjalne', 'polecane', 'wyprzedaz', 'bestsellery');
        //var_dump($this->tryb);die();
        //$ile = 24;
        $sortowanie = new Zend_Session_Namespace('sortowanieStrona');
        if ($this->_request->getParam('sort')) {
            $sortowanie->sort = $this->_request->getParam('sort');
        }
//        dump($this->_request->getPost());die;
        if ($this->_request->getParam('order')) {
            $sortowanie->order = $this->_request->getParam('order');
            if ($sortowanie->sort == 'pozycja')
                $sortowanie->order = 'asc';
        }
        if ($this->_request->getPost('specjalne')) {
            $sortowanie->specjalne = $this->_request->getPost('specjalne');
        }
        if (!isset($sortowanie->sort))
            $sortowanie->sort = 'nazwa';
        if (!isset($sortowanie->order))
            $sortowanie->order = 'asc';
        if (!isset($sortowanie->specjalne))
            $sortowanie->specjalne = 'all';
        if ($sortowanie->order != 'asc' && $sortowanie->order != 'desc')
            $sortowanie->order = 'asc';

        $szukanie = new Zend_Session_Namespace('szukanieStrona');
        //if ($szukanie->isLocked()) $szukanie->unLock();
        $szukanie->typ_klienta = $this->view->typ_klienta;

        $id = @intval($this->_request->getParam('id', 0));
        $action = @strval($this->_request->getParam('action'));
        $kateg = new Zend_Session_Namespace('kategoria');
        $newKat = (isset($kateg->kategoria) && $kateg->kategoria != $id);
        $newAkcja = (isset($kateg->action) && $kateg->action != $action);
//        dump($this->_request->getPost());die;
        $kateg->kategoria = $id;
        $kateg->action = $action;
        $this->view->promocjeFiltr = $szukanie->promocja;
        $this->view->nowosciFiltr = $szukanie->nowosc;
        $this->view->zlotoFiltr = $szukanie->zloto;
        $this->view->srebroFiltr = $szukanie->srebro;

//        dump($this->_request->getPost());die;

        if ($this->_request->getParam('reset') || (($newKat || $newAkcja) && !$this->_request->isPost())) {
            $szukanie->nazwa = '';
            $szukanie->oznaczenie = '';
            $szukanie->marka = '';
            $szukanie->grupa = 'all';
            $szukanie->rozmiar = 'all';
            $szukanie->wkladka = 'all';
            $szukanie->kolor = -1;
            $szukanie->cena_od = 0;
            $szukanie->cena_do = 0;
            $szukanie->producent = -1;
            $szukanie->producenci = null;
            $szukanie->atrybuty = null;
            $szukanie->dostepne = false;
            $szukanie->promocja = 0;
            $szukanie->nowosc = 0;
            $szukanie->zloto = 0;
            $szukanie->srebro = 0;

            $sortowanie->sort = 'nazwa';
            $sortowanie->order = 'asc';

//            var_dump($this->URI);die();

            $this->_redirect(str_replace('reset/all', '', $this->URI));
        }

        if ($this->_request->getPost('del_param')) {
            if ($this->_request->getPost('del_nazwa'))
                $szukanie->nazwa = '';
            if ($this->_request->getPost('del_cena_od'))
                $szukanie->cena_od = 0;
            if ($this->_request->getPost('del_cena_do'))
                $szukanie->cena_do = 0;
            if ($this->_request->getPost('del_producent'))
                $szukanie->producent = -1;
            $this->_redirect($this->URI);
        }
//        dumpv($this->_request->getPost());die

        if ($this->_request->getParam('promocja') != '') {
            $szukanie->promocja = $this->_request->getParam('promocja');
        }
//        dumpv($this->view->promocjeFiltr);die;
        if ($this->_request->getParam('nowosc') != '') {
            $szukanie->nowosc = $this->_request->getParam('nowosc');
        }
        if ($this->_request->getParam('zloto') != '') {
            $szukanie->zloto = $this->_request->getParam('zloto');
        }
        if ($this->_request->getParam('srebro') != '') {
            $szukanie->srebro = $this->_request->getParam('srebro');
        }
        if ($this->_request->getPost('co') !== null) {
            $co = $this->_request->getPost('co', '');
            if ($co == $this->fraza)
                $co = '';
            $szukanie->nazwa = addslashes($co);
            //$szukanie->oznaczenie = $co;
        }
        if ($this->_request->getParam('co')) {
            $co = $this->_request->getParam('co');
            if ($co == $this->fraza)
                $co = '';
            $szukanie->nazwa = addslashes($co);
            //$szukanie->oznaczenie = $co;
        }
        if ($this->_request->getPost('model') !== null) {
            $kod = $this->_request->getPost('model', '');
            if ($kod == $this->frazaSymbol)
                $kod = '';
            $szukanie->oznaczenie = addslashes(str_replace(" ", " ", $kod));
        }
        if ($this->_request->getParam('model')) {
            $kod = $this->_request->getParam('model');
            if ($kod == $this->frazaSymbol)
                $kod = '';
            $szukanie->oznaczenie = addslashes(str_replace(" ", " ", $kod));
        }
        if ($this->_request->getPost('marka')) {
            $marka = $this->_request->getPost('marka');
            if ($marka == $this->frazaMarka)
                $marka = '';
            $szukanie->marka = addslashes($marka);
        }
        if ($this->_request->getParam('kod')) {
            $marka = $this->_request->getParam('marka');
            if ($marka == $this->frazaMarka)
                $marka = '';
            $szukanie->marka = addslashes($marka);
        }
        if (false) {
            //$strona = $this->_request->getParam($this->page, '');
            //if(empty($strona))
            if ($this->_request->getParam('action') == 'kategorie') {
                //var_dump($this->_request->getParams());
                $szukanie->nazwa = '';
                $szukanie->oznaczenie = '';
            }
        }
        if ($this->_request->getPost('rozmiar')) {
            $szukanie->rozmiar = $this->_request->getPost('rozmiar');
            if ($this->_request->getPost('rozmiar') != 'all')
                $szukanie->wkladka = 'all';
        }
        if ($this->_request->getPost('wkladka')) {
            $szukanie->wkladka = $this->_request->getPost('wkladka');
            if ($this->_request->getPost('wkladka') != 'all')
                $szukanie->rozmiar = 'all';
        }
        if ($this->_request->getParam('rozmiar')) {
            $szukanie->rozmiar = $this->_request->getParam('rozmiar');
            if ($this->_request->getParam('rozmiar') != 'all')
                $szukanie->wkladka = 'all';
        }
        if ($this->_request->getParam('wkladka')) {
            $szukanie->wkladka = $this->_request->getParam('wkladka');
            if ($this->_request->getParam('wkladka') != 'all')
                $szukanie->rozmiar = 'all';
        }
        if ($this->_request->getPost('kolor')) {
            $szukanie->kolor = $this->_request->getPost('kolor');
        }
        if ($this->_request->getParam('kolor') !== null) {
            $szukanie->kolor = intval($this->_request->getParam('kolor'));
        }
        if ($this->_request->getParam('cena_brutto')) {
            $szukanie->cena_od = 0;
            $szukanie->cena_do = 0;
        }
        if ($this->_request->getPost('cenaOd') != '') {
            $szukanie->cena_od = floatval(str_replace(",", ".", $this->_request->getPost('cenaOd', 0)));
            //$szukanie->cena_do = -1;
        }
        if ($this->_request->getPost('cenaDo') != '') {
            $szukanie->cena_do = floatval(str_replace(",", ".", $this->_request->getPost('cenaDo', 0)));
            //$szukanie->cena_od = -1;
        }
        if ($this->_request->getParam('cenaOd') != '') {
            $szukanie->cena_od = floatval(str_replace(",", ".", $this->_request->getParam('cenaOd', 0)));
            //$szukanie->cena_do = -1;
        }
        if ($this->_request->getParam('cenaDo') != '') {
            $szukanie->cena_do = floatval(str_replace(",", ".", $this->_request->getParam('cenaDo', 0)));
            //$szukanie->cena_od = -1;
        }
        if ($newKat || $newAkcja) {
            $szukanie->cena_do = 0;
        }
        if ($this->_request->getPost('producent') != null) {
            $szukanie->producent = intval($this->_request->getPost('producent'));
        }
        if ($this->_request->getParam('producent') != null) {
            $szukanie->producent = intval($this->_request->getParam('producent'));
        }
        if ($this->_request->getPost('producenci')) {
            $szukanie->producenci = $this->_request->getParam('producenci');
        }

        if ($this->_request->getPost('grupa') != null) {
            $szukanie->grupa = intval($this->_request->getPost('grupa'));
        }
        if ($this->_request->getParam('grupa') != null) {
            $szukanie->grupa = intval($this->_request->getParam('grupa'));
        }
        if ($this->_request->getPost('sortSubmit')) {
            $szukanie->atrybuty = $this->_request->getPost('atrybuty');
            if (@$szukanie->atrybuty[0] == 'atrybut')
                unset($szukanie->atrybuty[0]);
            if (@count($szukanie->atrybuty) > 0)
                foreach ($szukanie->atrybuty as $id_gr => $atrs)
                    if (@count($atrs) == @count($this->view->atrybutygrupy[$id_gr]))
                        unset($szukanie->atrybuty[$id_gr]);

            if (!$this->_request->getParam('promocja')) {
                $szukanie->promocja = 0;
            }
            if (!$this->_request->getParam('nowosc')) {
                $szukanie->nowosc = 0;
            }
        }

        if ($this->_request->getParam('atrybut')) {
            $atr = intval($this->_request->getParam('atrybut'));
            $szukanie->atrybuty[$atr] = intval($this->_request->getParam('atr'));
        }
        if ($this->_request->getParam('dostepne') != null) {
            $szukanie->dostepne = intval($this->_request->getParam('dostepne'));
        }        
        if ($this->_request->getParam('ile')) {
            $noweIle = intval($this->_request->getParam('ile', 0));
            if ($noweIle > 0)
                $szukanie->ile = $noweIle;
        }

        if (false) {
            if (@count($this->tryby) > 0)
                foreach ($this->tryby as $tryb) {
                    if ($this->_request->getParam($tryb)) {
                        $trybAll = $this->_request->getParam($tryb);
                        break;
                    }
                }
            if (@!empty($trybAll))
                foreach ($this->tryby as $tryb) {
                    $tryb = 'tryb' . $tryb;
                    $szukanie->{$tryb} = $trybAll;
                }
        }
        if (@count($this->tryby) > 0)
            foreach ($this->tryby as $tryb) {
                $tryb = 'tryb' . $tryb;
                if ($this->_request->getParam('tryb'))
                    $szukanie->{$tryb} = $this->_request->getParam('tryb');
                if ($this->_request->getParam($tryb))
                    $szukanie->{$tryb} = $this->_request->getParam($tryb);
                if ($this->_request->getPost($tryb))
                    $szukanie->{$tryb} = $this->_request->getPost($tryb);
                if (!isset($szukanie->{$this->tryb}) || !isset($this->listaProdIlosci[$szukanie->{$tryb}]))
                    $szukanie->{$tryb} = 'siatka_full';
                if ($this->mobile)
                    $szukanie->{$tryb} = 'lista_full';
            }
        if ($this->_request->getPost('tryb', $this->_request->getParam('tryb')))
            $szukanie->trybWybrany = true;
        if (@isset($szukanie->ile) && @isset($szukanie->{$this->tryb}))
            if (@!in_array($szukanie->ile, @$this->listaProdIlosci[$szukanie->{$this->tryb}])) {
                if ($szukanie->ile != 99999)
                    $szukanie->ile = $this->listaProdIlosci[$szukanie->{$this->tryb}][0];
            }

        //var_dump($szukanie->nazwa);die();
        if (true && $this->_request->getPost('kategoria')) {
            $kategoria = intval($this->_request->getPost('kategoria'));
            if ($kategoria > 0 && $kateg->kategoria != $kategoria) {
                $kateg->kategoria = $kategoria;
                $kategorie = new Kategorie($this->_module);
                $wybrana = $kategorie->showWybranaKategoria($kategoria);
                $this->_redirect($wybrana['link']);
            } else if ($kategoria == 0 && $kateg->kategoria > 0) {
                $kateg->kategoria = 0;
                $this->_redirect($this->ofertaLink);
            }
        }
        if (true && $this->_request->getParam('kategoria')) {
            $kategoria = intval($this->_request->getParam('kategoria'));
            if ($kategoria > 0 && $kateg->kategoria != $kategoria) {
                $kateg->kategoria = $kategoria;
                $kategorie = new Kategorie($this->_module);
                $wybrana = $kategorie->showWybranaKategoria($kategoria);
                $this->_redirect($wybrana['link']);
            } else if ($kategoria == 0 && $kateg->kategoria > 0) {
                $kateg->kategoria = 0;
                $this->_redirect($this->ofertaLink);
            }
        }
//dump($this->_request->getPost());die;
//        if ($this->_request->isPost()) {
//            if (($strona = $this->_request->getParam($this->page)) > 0)
//                $this->_redirect(str_replace($this->page . '/' . $strona, $this->page . '/0', $this->URI));
//            $this->_redirect($this->URI);
//        }
//dump($this->_request->getPost('sortSubmit'));die;
        //$szukanie->lock();		
        if (!isset($szukanie->nazwa))
            $szukanie->nazwa = '';
        if (!isset($szukanie->oznaczenie))
            $szukanie->oznaczenie = '';
        if (!isset($szukanie->marka))
            $szukanie->marka = '';
        if (!isset($szukanie->grupa))
            $szukanie->grupa = 'all';
        if (!isset($szukanie->rozmiar))
            $szukanie->rozmiar = 'all';
        if (!isset($szukanie->wkladka))
            $szukanie->wkladka = 'all';
        if (!isset($szukanie->kolor))
            $szukanie->kolor = -1;
        if (!isset($szukanie->cena_od))
            $szukanie->cena_od = 0;
        if (!isset($szukanie->cena_do))
            $szukanie->cena_do = 0;
        if (!isset($szukanie->producent))
            $szukanie->producent = -1;
        if (!isset($szukanie->producenci))
            $szukanie->producenci = null;
        if (!isset($szukanie->atrybuty))
            $szukanie->atrybuty = null;
        if (!isset($szukanie->dostepne))
            $szukanie->dostepne = false;
        if (!isset($szukanie->tryb))
            $szukanie->tryb = 'siatka_full';
        if ($this->mobile)
            $szukanie->tryb = 'lista_full';
        if (!isset($szukanie->ile))
            $szukanie->ile = $this->listaProdIlosci[$szukanie->tryb][0];            
        //$szukanie->ile = 3;
        $this->view->nazwa = empty($szukanie->nazwa) ? $this->fraza : $szukanie->nazwa;
        $this->view->marka = empty($szukanie->marka) ? $this->frazaMarka : $szukanie->marka;
        $this->view->model = empty($szukanie->oznaczenie) ? $this->frazaModel : $szukanie->oznaczenie;
        $this->view->producent = $szukanie->producent;
        $this->view->ile = $szukanie->ile;
        $this->view->id = $id;

        if (false && $this->obConfig->producent) {
            $this->view->producentDane = @$this->producenci[$szukanie->producent];
            //var_dump($this->view->producentDane);die();
        }
        if (false && $this->obConfig->tryb == 'rozmiary') {
            $rozmiar = new Rozmiary($this->_module);
            $this->view->rozmiar = $rozmiar->wypiszKoloryDistinct(null, 'nazwa');
            //var_dump($this->view->rozmiar);die();
        }

        if ($this->obConfig->atrybuty) {
            $atrybuty = new Atrybuty($this->_module);
            $this->view->atrybuty = $atrybuty->getAtrybuty(-1);
            $atrybutygrupy = new Atrybutygrupy($this->_module);
            $this->view->atrybutygrupy = $atrybutygrupy->getAtrybutySort();
            $this->view->atrybutytekst = $atrybutygrupy->getAtrybutyTekst();
        }

        $this->sort = $sortowanie->sort . ' ' . $sortowanie->order;
        $this->sortowanie = $sortowanie;
        $this->view->sortowanie = $sortowanie;
        $this->view->sort = $sortowanie->sort;
        $this->view->order = $sortowanie->order == 'asc' ? 'desc' : 'asc';

        $id = intval($this->_request->getParam('id'));
        if ($this->_request->getPost('id'))
            $id = 0;

        $cenaMax = 0;
        if (false) {
            if ($this->obConfig->tryb == 'produkt') {
                $produkty = new Produkty($this->_module);
                if ($this->obConfig->atrybutyCeny)
                    $cenaMaxAtrybut = floatval($produkty->cenaMax($id > 0 ? $id : 0, $this->view->typ_klienta, true));
                //else
                $cenaMaxProdukt = floatval($produkty->cenaMax($id > 0 ? $id : 0, $this->view->typ_klienta, false));
                $cenaMax = @max($cenaMaxProdukt, $cenaMaxAtrybut);
            }
            else {
                $produkty = new Produkty($this->_module);
                if ($this->obConfig->atrybutyCeny)
                    $cenaMaxProdukt = floatval($produkty->getCena(0, $this->view->typ_klienta, false, $id > 0 ? $id : 0));
                else
                    $cenaMaxProdukt = floatval($produkty->cenaMax($id > 0 ? $id : 0, $this->view->typ_klienta));
                $rozmiarproduktu = new Rozmiarproduktu($this->_module);
                $cenaMaxRozmiar = floatval($rozmiarproduktu->cenaMax($id > 0 ? $id : 0, $this->view->typ_klienta));
                $cenaMax = @max($cenaMaxProdukt, $cenaMaxRozmiar);
            }
        }
        $this->view->cenaMax = $this->cenaMax = $cenaMax;
        if ($szukanie->cena_do == 0 && $szukanie->cena_do > $cenaMax)
            $szukanie->cena_do = $cenaMax; //var_dump($cenaMax);

        $this->szukanie = $szukanie;
        $this->view->szukanie = $szukanie;

        if ($this->obConfig->filtry) {
            $filtry = false;
            if (!empty($szukanie->nazwa))
                $filtry = true;
            if ($szukanie->cena_od > 0)
                $filtry = true;
            if ($szukanie->cena_do > 0)
                $filtry = true;
            if ($szukanie->producent > 0)
                $filtry = true;
            if ($szukanie->zloto = 'on')
                $filtry = true;

            $this->view->filtry = $filtry;
        }
    }

    public function indexAction() {

        //$this->view->nawigacja = $this->arrow.'<a href="'.$this->view->baseUrl.'/'.$this->ofertaLink.'/">'.$this->ofertaLink.'</a>';
        $this->view->ustawienia['title'] = $this->ofertaLink . ' - ' . $this->view->ustawienia['title'];

        $podstrona = $this->_request->getParam('podstrona', 'oferta');

        if (true && $podstrona == 'oferta') {
            $main = new Podstrony($this->_module);
            $main->link = $this->ofertaLink;
            $this->view->opis = $page = $main->getPodstrona();
            $this->view->typ = $typ = 'podstrony';
            $this->view->galeria = $this->common->getGaleria($page['id'], $typ);
            $this->view->videos = $this->common->getVideos($page['id'], $typ);
        }

        $strona = $this->_request->getParam($this->page, 0);
        $ile = $this->szukanie->ile;
        if ($podstrona == 'galeria') {
            $strona = 0;
            $ile = 9999;
        }
        $od = $strona * $ile;

        $this->sortBy = true;
        $this->sorter = $this->obConfig->produktyPozycja ? 'pozycja' : 'nazwa';
        $this->sortOrder = 'asc';
        $sortowanie = (object) array();
        $sortowanie->sort = 'p.data';
        $sortowanie->order = 'desc';
        $sortowanie->specjalne = '';

        if ($this->obConfig->pokazKategorie) {
            $kategorie = new Kategorie($this->_module);
            $dzieci = $kategorie->wypiszDzieci(0);
            //var_dump($dzieci);
        }

        if (@count($dzieci) > 0) {
            $this->view->dzieci = true;
            $this->view->all = $dzieci;
            $this->view->nazwa_kategorii = '';
            $this->render('kategoriedzieci');
            $render = array('module' => 'default', 'controller' => 'index');
            //$this->_helper->viewRenderer->renderBySpec('kategoriedzieci', $render);
        } else {
            $produkty = new Produkty($this->_module);
            $ilosc = $produkty->wypiszProduktyPodzieloneStrona(true, $strona, $ile, $this->sortowanie, $this->szukanie, 0, null, $this->test);
            $prod = $produkty->wypiszProduktyPodzieloneStrona(false, $strona, $ile, $this->sortowanie, $this->szukanie, 0, null, $this->test);

            $all = $this->produktyGaleria($prod);
            //var_dump($all);die();
            $strony = ceil($ilosc / $ile);
            $this->view->all = $all;
//            dump($all);die;
            $this->view->strony = $strony;
            $this->view->strona = $strona;
            $this->view->ilosc = $ilosc;
        }

        $this->view->link = $this->ofertaLink;
        if ($podstrona != 'oferta')
            $this->view->link .= '/podstrona/' . $podstrona;
        $this->view->site = ucfirst($podstrona);
        $this->view->podstrona = $podstrona;
        $this->wynikiSite();
    }

    public function szukajAction() {        

        $this->view->nawigacja = '<a href="' . $this->view->baseUrl . '/' . $this->wynikiLink . '/">' . $this->arrow . $this->lang('wyniki_wyszukiwania') . '</a>';
        $this->view->ustawienia['title'] = $this->lang('wyniki_wyszukiwania') . ' - ' . $this->view->ustawienia['title'];

//        dump($this->_request->getPost());die;

        if (true) {
            $main = new Podstrony($this->_module);
            $main->link = $this->ofertaLink;
            $this->view->opis = $main->getPodstrona();
        }
        
        $strona = $this->_request->getParam($this->page, 0);
        $ile = $this->szukanie->ile;
//        $ile = 12;
        $od = $strona * $ile;

//        if (@empty($this->szukanie->nazwa) && @empty($this->szukanie->oznaczenie) && @intval($this->szukanie->producent) <= 0)
//            $this->_redirect($this->ofertaLink);

        $produkty = new Produkty($this->_module);
        $ilosc = $produkty->wypiszProduktyPodzieloneStrona(true, $strona, $ile, $this->sortowanie, $this->szukanie, 0, null, $this->test);
        $all = $produkty->wypiszProduktyPodzieloneStrona(false, $strona, $ile, $this->sortowanie, $this->szukanie, 0, null, $this->test);
        //if($ilosc == 1) header('Location: '.$this->view->baseUrl.'/'.$all[0]['link']);
        //$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/'.$all[0]['link']);

        $all = $this->produktyGaleria($all);

        $strony = ceil($ilosc / $ile);

        $this->view->link = $this->wynikiLink;
        $this->view->site = 'Wyniki wyszukiwania';
        $this->wynikiSite();

        $this->view->all = $all;
        $this->view->strony = $strony;
        $this->view->strona = $strona;
        $this->view->ilosc = $ilosc;
        $this->view->iloscProduktow = $ilosc;
        
        $producenci = new Producent();
        $showProducenci = $producenci->wypiszProducentowForProduktCount();
//        dump($showProducenci);die;
        $this->view->showProducenci = $showProducenci;
    }

    public function kategorieAction() {
        $filterChain = new Zend_Filter();
        $filterChain->appendFilter(new Zend_Filter_Int());
        $id = $filterChain->filter($this->_request->getParam('id'));
        $strona = $filterChain->filter($this->_request->getParam($this->page, 0));
        $ile = $this->szukanie->ile;
//        dump($ile);die;
//        $ile = 16;
        $podstrona = $this->_request->getParam('podstrona', 'oferta');
        $producent = @intval($this->_request->getParam('producent', 0));

        $kategorie = new Kategorie($this->_module);
        $kategorie->lang = $this->lang;
        $kategoria = $kateg = $kategorie->showWybranaKategoria($id);
        $this->view->myKat = true;

        if ($kategoria === null) {
            $this->_helper->viewRenderer->setNoRender();
            return;
        }
        if (false) {
            $kategoria = $kategoria->toArray();
            $kategoria['temat'] = $this->common->fixTekst($kategoria['nazwa']);
            $kategoria['tekst'] = $this->common->fixTekst($kategoria['opis']);
            $this->view->html = $kategoria;
        }
        if (@!$this->szukanie->trybWybrany) {
            $this->szukanie->tryb = $kategoria['rodzic'] < 0 ? 'siatka_full' : 'siatka_full';
            if ($this->mobile)
                $this->szukanie->tryb = 'lista_full';
            $this->view->szukanie = $this->szukanie;
        }

        $this->view->typ = $typ = 'kategorie';
        if ($this->obConfig->podstronyGaleria) {
            $galeria = new Galeria($this->_module);
            $this->view->galeria = $galeria->wyswietlGalerie($kategoria['id'], 'desc', 0, $typ, true);
        }
        if ($this->obConfig->podstronyVideo) {
            $video = new Video($this->_module);
            $this->view->videos = $video->wypisz(true, $kategoria['id'], $typ, true);
        }

        if ($this->obConfig->tryb == 'rozmiary') {
            $rozmiar = new Rozmiary($this->_module);
            $this->view->rozmiar = $rozmiar->wypiszKoloryDistinctKateg($id, 'nazwa', $this->szukanie->grupa);
            //var_dump($this->view->rozmiar);die();
        }

        $producenci = new Producent();
        $showProducenci = $producenci->wypiszProducentowForProduktCount();

        $this->view->showProducenci = $showProducenci;


        $this->view->nawigacja = '<a class="navi_bg" href="' . $this->view->baseUrl . '/' . $kategoria['link'] . '/">';
        $this->view->nawigacja .= $this->arrow;
        $this->view->nawigacja .= $kategoria['nazwa'] . '</a>';
        $nazwa = $kategoria['nazwa'];
        $this->view->ustawienia['title'] = $nazwa . ' - ' . $this->view->ustawienia['title'];

        if ($this->obConfig->baneryKategorie) {
            $banerMain['img'] = $kategoria['img1'];
            $banerMain['link'] = $kategoria['img1_opis'];
            if ($this->obConfig->baneryKategorieDwa) {
                $baner['img'] = $kategoria['img2'];
                $baner['link'] = $kategoria['img2_opis'];
            }
        }
        if ($this->obConfig->opisKategorii) {
            if ($this->obConfig->opisKategoriiShort)
                $short = $kategoria['short'];
            $opis = $kategoria['opis'];
        }

        $wizytowka = '';
        $wizytowki = new Wizytowka($this->_module);
        if ($this->obConfig->wizytowkaSprzedawcy)
            if ($kategoria['sprzedawca'] > 0) {
                $wizytowki->id = $kategoria['sprzedawca'];
                $sprzedawca = $wizytowki->wypiszPojedynczego();
                $wizytowka = $sprzedawca['wizytowka'];
                $wizytowkaLink = $sprzedawca['link'];
            }

        $this->view->linkLast = $kategoria['link'];
        $this->view->link = $kategoria['link'];
        if ($podstrona != 'oferta')
            $this->view->link .= '/podstrona/' . $podstrona;
        //if($producent > 0) $this->view->link .= '/producent/'.$producent;
        $this->view->site = $kategoria['nazwa'];
        $this->wynikiSite();
        $this->view->podstrona = $podstrona;
        $this->view->nazwa_kategorii = '';

        $modele = $kategoria['modele'];

        while ($kategoria['rodzic'] != 0) {
            $kategoria = $kategorie->showWybranaKategoria($kategoria['rodzic']);
            $this->view->nawigacja = '<a class="navi_bg" href="' . $this->view->baseUrl . '/' . $kategoria['link'] . '/">' . $this->arrow . $kategoria['nazwa'] . '</a>' . $this->view->nawigacja;
            //$this->view->nazwa_kategorii = $kategoria['nazwa'].' / '.$this->view->nazwa_kategorii;

            $modele = $modele || $kategoria['modele'];

            if ($this->obConfig->baneryKategorie) {
                if (empty($banerMain['img']) || $banerMain['img'] == 'brak.jpg') {
                    $banerMain['img'] = $kategoria['img1'];
                    $banerMain['link'] = $kategoria['img1_opis'];
                }
                if ($this->obConfig->baneryKategorieDwa)
                    if (empty($baner['img']) || $baner['img'] == 'brak.jpg') {
                        $baner['img'] = $kategoria['img2'];
                        $baner['link'] = $kategoria['img2_opis'];
                    }
            }
            if ($this->obConfig->opisKategorii) {
                if ($this->obConfig->opisKategoriiShort)
                    if (empty($short))
                        $short = $kategoria['short'];
                if (empty($opis))
                    $opis = $kategoria['opis'];
            }

            if ($this->obConfig->wizytowkaSprzedawcy)
                if (empty($wizytowka) && $kategoria['sprzedawca'] > 0) {
                    $wizytowki->id = $kategoria['sprzedawca'];
                    $sprzedawca = $wizytowki->wypiszPojedynczego();
                    $wizytowka = $sprzedawca['wizytowka'];
                    $wizytowkaLink = $sprzedawca['link'];
                }
        }
        if (isset($this->view->nazwa_kategorii))
            $this->view->nazwa_kategorii .= '';
        $this->view->nazwa_kategorii .= $nazwa;

        if ($this->obConfig->baneryKategorie) {
            if ($this->obConfig->baneryKategorieDwa)
                if (!empty($baner['img']) && $baner['img'] != 'brak.jpg')
                    $this->view->baner = $baner;
            if (!empty($banerMain['img']) && $banerMain['img'] != 'brak.jpg')
                $this->view->banerMain = $banerMain;
        }
        if ($this->obConfig->opisKategorii) {
            if ($this->obConfig->opisKategoriiShort)
                if (!empty($short))
                    $this->view->short = $this->common->fixTekst($short);
            if (!empty($opis))
                $this->view->opis = $this->common->fixTekst($opis);
        }

        if ($this->obConfig->wizytowkaSprzedawcy) {
            if (empty($wizytowka) || isset($wizytowkaDefault)) {
                $default = $wizytowki->getDomyslny();
                $wizytowka = $default['wizytowka'];
                $wizytowkaLink = $default['link'];
            }
            $this->view->wizytowka = $wizytowka;
            $this->view->wizytowkaLink = $wizytowkaLink;
        }

        //$this->view->link = $kategoria['link'];   
        $this->view->drzewo = '<a href="' . $this->view->baseUrl . '/' . $kategoria['link'] . '/">' . $kategoria['nazwa'] . '</a>';
        while ($kategoria['rodzic'] > 0) {
            $kategoria = $kategorie->showWybranaKategoria($kategoria['rodzic']);
            $this->view->drzewo = '<a href="' . $this->view->baseUrl . '/' . $kategoria['link'] . '/">' . $kategoria['nazwa'] . ' &raquo;</a>' . $this->view->drzewo;
            //$this->view->nazwa_kategorii .= ' - '.$kategoria['nazwa'];
        }

        if (false) {
            $render = array('module' => 'default', 'controller' => 'podstrony');
            $this->_helper->viewRenderer->renderBySpec('show', $render);
            return;
        }

        $produkty = new Katprod($this->_module);
        $produkty->id = $kategoria['typ'] == 'kategorie' ? $id : -1;
        $produkty->nr = $kategoria['typ'] == $this->obConfig->kategorieInneNazwa ? $id : -1;
        $produkty->typ = $kategoria['typ'];

        if (true) {
            $odwiedziny = new Odwiedziny($this->_module);
            $data['id_prod'] = $id;
            $data['typ'] = 'kategorie';
            $odw = $odwiedziny->dodajOdwiedziny($data);
        }

        if ($this->obConfig->pokazKategorie) {

            $dzieci = $kategorie->wypiszDzieci($id);

            $dzieciBezDzieci = $produkty->countProduktyWKatNieWPodKat($id);
        }

        $produkty = new Produkty($this->_module);
        $ilosc = $produkty->wypiszProduktyPodzieloneStrona(true, $strona, $ile, $this->sortowanie, $this->szukanie, 0, null, $this->test);
        $prod = $produkty->wypiszProduktyPodzieloneStrona(false, $strona, $ile, $this->sortowanie, $this->szukanie, 0, null, $this->test);
        $allProd = $this->produktyGaleria($prod);
        $strony = ceil($ilosc / $ile);

        $this->view->allProd = $allProd;
        $this->view->strony = $strony;
        $this->view->strona = $strona;
        $this->view->ilosc = $ilosc;





        if ($this->obConfig->pokazProducenci) {
            $producent = $this->szukanie->producent;
            if (false && $this->obConfig->pokazKategorie) {
                if ($producent == 0)
                    $dzieci = $kategorie->wypiszDzieci($id);
                else
                    $dzieci = $kategorie->wypiszDzieciProducent($id, $producent);
                $dzieciBezDzieci = $produkty->countProduktyWKatNieWPodKat($id);
            }
            if (false) {
                $galeria = new Galeria($this->_module);
                $this->view->galeriaCount = $galeria->getZdjeciaCountForKateg($id);
            }
            if ($podstrona == 'oferta') {
                if ($producent == 0) {
                    $producenci = new Producent($this->_module);
                    $this->view->all = $producenci->wypiszProducentowDlaKategorii($id);
                    $this->render('producencidzieci');
                    return;
                }
                if ($producent > 0) {
                    $producenci = new Producent($this->_module);
                    //$prod = $producenci->wypiszProduktyDlaKategorii($id);
                    if (true) {
                        $odwiedziny = new Odwiedziny($this->_module);
                        $data['id_prod'] = $producent;
                        $data['typ'] = 'producenci';
                        $odw = $odwiedziny->dodajOdwiedziny($data);
                    }
                }
            }
        }

//        if (@count($dzieci) > 0 && @$dzieciBezDzieci >= 0) {
            $this->view->dzieci = true;
            //$this->view->opis = $this->common->fixTekst($kategoria['opis']);

            $this->view->all = $dzieci;
            $kategoria = new Katprod($this->_module);
            $kategoria->id = $id;
//            $ile = 12;
            $prod = $kategoria->selectProdukt($strona, $ile, $this->sortowanie, $this->szukanie, false, 0, null);
            
            $prodAll = $kategoria->selectProdukt();
            
            $prod = $this->produktyGaleria($prod);
            $this->view->allProd = $prod;
            $ilosc = count($prodAll);
            $this->view->iloscProduktow = $ilosc;
            $strony = ceil($ilosc / $ile);
            if ($this->_request->getPost('sortSubmit')) {

                $strony = ceil($ilosc / $ile);
            }
            $this->view->strony = $strony;
            if ($this->_request->getParam('reset')) {
                
            }

            if ($this->_request->getPost('producenci')) {

                $produkty = new Produkty($this->_module);
                $ilosc = $produkty->wypiszProduktyPodzieloneStrona(true, $strona, $ile, $this->sortowanie, $this->szukanie, 0, null, $this->test);
                $all = $produkty->wypiszProduktyPodzieloneStrona(false, $strona, $ile, $this->sortowanie, $this->szukanie, 0, null, $this->test);
                //if($ilosc == 1) header('Location: '.$this->view->baseUrl.'/'.$all[0]['link']);
                //$this->getResponse()->setHeader('Refresh', '0; URL='.$this->view->baseUrl.'/'.$all[0]['link']);
                
                $all = $this->produktyGaleria($all);

                $strony = ceil($ilosc / $ile);

                $this->view->link = $this->params['paramurl'];
                
                $this->view->site = 'Wyniki wyszukiwania';
                $this->wynikiSite();

                $this->view->allProd = $all;
                $this->view->strony = $strony;
                $this->view->strona = $strona;
                $this->view->ilosc = $ilosc;
                
            }

            $this->render('kategoriedzieci');
//        }

//        if (@count($dzieci) == 0 || @$dzieciBezDzieci > 0) {
//
//            if (@$this->obConfig->pokazTylkoProduktyWKatNieWPodKat && @$dzieciBezDzieci > 0)
//                $this->szukanie->pokazTylkoProduktyWKatNieWPodKat = true;
//
//            if ($podstrona == 'galeria') {
//                $strona = 0;
//                $ile = 9999;
//            }
//            $ile = 12;
//
//            $kategoria = new Kategorie($this->_module);
//            $kategoria = $kategorie->showWybranaKategoria($id);
//
//            $kategoria = new Katprod($this->_module);
//            $kategoria->id = $id;
//
//
//            $prod = $kategoria->selectProdukt($strona, $ile, $this->sortowanie, $this->szukanie, false, 0, null);
//            $prodAll = $kategoria->selectProdukt();
//            $prod = $this->produktyGaleria($prod);
//
//            $ilosc = count($prodAll);
//
//            $strony = ceil($ilosc / $ile);
//            $this->view->strony = $strony;
//            $this->view->all = $prod;
//            $this->view->id = $id;
//
//
////            $this->render('kategoriedzieci');
////            $szuk = !(@empty($this->szukanie->nazwa) && @empty($this->szukanie->oznaczenie) && @intval($this->szukanie->producent) <= 0);
////
////            if (false and !$modele || $szuk) {
//////                $produkty = new Katprod($this->_module);
////                
////                $ilosc = $produkty->selectProdukt($strona, $ile, $this->sortowanie, $this->szukanie, true, 0, null, $this->test);
////
//////                $strony = ceil($ilosc / $ile);
//////
//////                $all = $produkty->selectProdukt($strona, $ile, $this->sortowanie, $this->szukanie, false, 0, null, $this->test);
//////                $all = $this->produktyGaleria($all);
////
////                $this->szukanie->pokazTylkoProduktyWKatNieWPodKat = false;
////
////                $this->view->all = $all;
////                $this->view->strony = $strony;
////                $this->view->strona = $strona;
////                $this->view->ilosc = $ilosc;
////                //$this->view->kat = $kategoria['nazwa'];
////                $this->view->id = $id;
////
////                if (!$modele)// || count($all) > 0)
////                    $this->render('kategorie');
////            }
////
////            if ($modele && (!$szuk || @count($all) == 0)) {
////                $this->view->produkt = $kateg;
////                //var_dump($kateg);die();
////                $this->view->szuk = $szuk;
////                $this->view->id = $id;
////
////                $sortowanie = (object) array();
////                $sortowanie->sort = 'p.oznaczenie';
////                $sortowanie->order = 'asc';
////                $sortowanie->specjalne = '';
////
////                $all2 = $produkty->selectProdukt(0, 99999, $sortowanie, null, false, 0, null, $this->test, true);
////                //var_dump($all);die();
////                if (count($all2) > 0) {
////                    $modele = $this->common->sortByPole($all2, 'producent', 'id');
////                    $this->view->modele = $modele;
////                    //var_dump($modele);die();
////                }
////
////                $this->render('kategoria');
////
////                if (@count($all) > 0)
////                    $this->render('kategorie');
////            }
//        }
    }

    public function promocjeAction() {
        $page = $this->params['action'];
        $site = $this->lang('Promocje');
        $link = $this->params['paramurl'];
        //$link = $this->common->makeLink($site).'-'.$this->lang;
        $podstrony = new Podstrony($this->_module);
        $podstrony->temat = $link;
        $this->view->link = $link;
        $this->view->site = 'Promocje';
        $this->view->opis = $podstrona = $podstrony->getPodstrona();
        $tytul = $podstrona['tytul'];
        $site = $podstrona['temat'];
        $link = $podstrona['link'];
        $this->view->nawigacja = '<a href="' . $this->view->baseUrl . '/' . $link . '/">' . $this->arrow . $site . '</a>';
        $this->view->ustawienia['title'] = $site . ' - ' . $this->view->ustawienia['title'];
        $this->view->site = $site;
        $this->wynikiSite();
        $this->view->tryb = $tryb = 'tryb' . strtolower($page);

        $this->view->typ = $typ = 'podstrony';
        $this->view->galeria = $this->common->getGaleria($podstrona['id'], $typ);
        $this->view->videos = $this->common->getVideos($podstrona['id'], $typ);

        $strona = $this->_request->getParam($this->page, 0);
        $specjalne = new Produkty($this->_module);
        $ile = $this->szukanie->ile;
        $od = $strona * $ile;

        $typ = @$this->obConfig->klienciPromocjeProduktyAll ? null : 'promocja';
        if ($this->obConfig->klienciPromocjeProdukty && $this->view->zalogowanyID > 0) {
            $kontrahenciPolecane = new Kontrahencipolecane($this->module);
            $promocje = $kontrahenciPolecane->wypiszKlientTyp($this->view->zalogowanyID, 'promocja');
            if (count($promocje) > 0)
                $ids = @unserialize($promocje['id_prod']);
        }
        $ilosc = //$specjalne->iloscProduktowSpecjalnych('promocja', $this->lang, 0, @$ids);
                $specjalne->wypiszProduktyPodzieloneStrona(1, $strona, $ile, $this->sortowanie, $this->szukanie, $typ, @$ids);
        $prod = //$specjalne->wypiszSpecjalne('promocja', $this->lang, 0, $od, $ile, $this->sort);
                $specjalne->wypiszProduktyPodzieloneStrona(0, $strona, $ile, $this->sortowanie, $this->szukanie, $typ, @$ids);

        $all = $this->produktyGaleria($prod);
        $strony = ceil($ilosc / $ile);
        $this->view->all = $all;
        $this->view->strony = $strony;
        $this->view->strona = $strona;
        $this->view->ilosc = $ilosc;
    }

    public function promocjeInternetoweAction() {
        $page = $this->params['action'];
        $site = $this->lang('Promocje_internetowe');
        $link = $this->params['paramurl'];
        //$link = $this->common->makeLink($site).'-'.$this->lang;
        $podstrony = new Podstrony($this->_module);
        $podstrony->temat = $link;
        $this->view->link = $link;
        $this->view->site = 'Promocje-internetowe';
        $this->view->opis = $podstrona = $podstrony->getPodstrona();
        $tytul = $podstrona['tytul'];
        $site = $podstrona['temat'];
        $link = $podstrona['link'];
        $this->view->nawigacja = '<a href="' . $this->view->baseUrl . '/' . $link . '/">' . $this->arrow . $site . '</a>';
        $this->view->ustawienia['title'] = $site . ' - ' . $this->view->ustawienia['title'];
        $this->view->site = $site;
        $this->wynikiSite();
        $this->view->tryb = $tryb = 'tryb' . strtolower($page);

        $this->view->typ = $typ = 'podstrony';
        $this->view->galeria = $this->common->getGaleria($podstrona['id'], $typ);
        $this->view->videos = $this->common->getVideos($podstrona['id'], $typ);

        $strona = $this->_request->getParam($this->page, 0);
        $specjalne = new Produkty($this->_module);
        $ile = $this->szukanie->ile;
        $od = $strona * $ile;

        $typ = @$this->obConfig->klienciPromocjeNetProduktyAll ? null : 'promocja_net';
        if ($this->obConfig->klienciPromocjeNetProdukty && $this->view->zalogowanyID > 0) {
            $kontrahenciPolecane = new Kontrahencipolecane($this->module);
            $promocjeNet = $kontrahenciPolecane->wypiszKlientTyp($this->view->zalogowanyID, 'promocja_net');
            if (count($promocjeNet) > 0)
                $ids = @unserialize($promocjeNet['id_prod']);
        }
        $ilosc = //$specjalne->iloscProduktowSpecjalnych('promocja_net', $this->lang, 0, @$ids);
                $specjalne->wypiszProduktyPodzieloneStrona(1, $strona, $ile, $this->sortowanie, $this->szukanie, $typ, @$ids);
        $prod = //$specjalne->wypiszSpecjalne('promocja_net', $this->lang, 0, $od, $ile, $this->sort);
                $specjalne->wypiszProduktyPodzieloneStrona(0, $strona, $ile, $this->sortowanie, $this->szukanie, $typ, @$ids);

        $all = $this->produktyGaleria($prod);
        $strony = ceil($ilosc / $ile);
        $this->view->all = $all;
        $this->view->strony = $strony;
        $this->view->strona = $strona;
        $this->view->ilosc = $ilosc;
    }

    public function polecaneAction() {
        $page = $this->params['action'];
        $site = $this->lang('Polecane');
        $link = $this->params['paramurl'];
        //$link = $this->common->makeLink($site).'-'.$this->lang;
        $podstrony = new Podstrony($this->_module);
        $podstrony->temat = $link;
        $this->view->link = $link;
        $this->view->site = 'Polecane';
        $this->view->opis = $podstrona = $podstrony->getPodstrona();
        $tytul = $podstrona['tytul'];
        $site = $podstrona['temat'];
        $link = $podstrona['link'];
        $this->view->nawigacja = '<a href="' . $this->view->baseUrl . '/' . $link . '/">' . $this->arrow . $site . '</a>';
        $this->view->ustawienia['title'] = $site . ' - ' . $this->view->ustawienia['title'];
        $this->view->site = $site;
        $this->wynikiSite();
        $this->view->tryb = $tryb = 'tryb' . strtolower($page);

        $this->view->typ = $typ = 'podstrony';
        $this->view->galeria = $this->common->getGaleria($podstrona['id'], $typ);
        $this->view->videos = $this->common->getVideos($podstrona['id'], $typ);

        $strona = $this->_request->getParam($this->page, 0);
        $specjalne = new Produkty($this->_module);
        $ile = $this->szukanie->ile;
        $od = $strona * $ile;

        $typ = @$this->obConfig->klienciPolecaneProduktyAll ? null : 'polecane';
        if ($this->obConfig->klienciPolecaneProdukty && $this->view->zalogowanyID > 0) {
            $kontrahenciPolecane = new Kontrahencipolecane($this->module);
            $polecane = $kontrahenciPolecane->wypiszKlientTyp($this->view->zalogowanyID, 'polecane');
            if (count($polecane) > 0)
                $ids = @unserialize($polecane['id_prod']);
        }
        $ilosc = //$specjalne->iloscProduktowSpecjalnych('polecane', $this->lang, 0, @$ids);
                $specjalne->wypiszProduktyPodzieloneStrona(1, $strona, $ile, $this->sortowanie, $this->szukanie, $typ, @$ids);
        $prod = //$specjalne->wypiszSpecjalne('polecane', $this->lang, 0, $od, $ile, $this->sort);
                $specjalne->wypiszProduktyPodzieloneStrona(0, $strona, $ile, $this->sortowanie, $this->szukanie, $typ, @$ids);

        $all = $this->produktyGaleria($prod);
        $strony = ceil($ilosc / $ile);
        $this->view->all = $all;
        $this->view->strony = $strony;
        $this->view->strona = $strona;
        $this->view->ilosc = $ilosc;
    }

    public function nowosciAction() {
        $page = $this->params['action'];
        $site = $this->lang('Nowosci');
        $link = $this->params['paramurl'];
        //$link = $this->common->makeLink($site).'-'.$this->lang;
        $podstrony = new Podstrony($this->_module);
        $podstrony->temat = $link;
        $this->view->link = $link;
        $this->view->site = 'Nowości';
        $this->view->opis = $podstrona = $podstrony->getPodstrona();
        $tytul = $podstrona['tytul'];
        $site = $podstrona['temat'];
        $link = $podstrona['link'];
        $this->view->nawigacja = '<a href="' . $this->view->baseUrl . '/' . $link . '/">' . $this->arrow . $site . '</a>';
        $this->view->ustawienia['title'] = $site . ' - ' . $this->view->ustawienia['title'];
        $this->view->site = $site;
        $this->wynikiSite();
        $this->view->tryb = $tryb = 'tryb' . strtolower($page);

        $this->view->typ = $typ = 'podstrony';
        $this->view->galeria = $this->common->getGaleria($podstrona['id'], $typ);
        $this->view->videos = $this->common->getVideos($podstrona['id'], $typ);

        $strona = $this->_request->getParam($this->page, 0);
        $specjalne = new Produkty($this->_module);
        $ile = $this->szukanie->ile;
        $od = $strona * $ile;

        $typ = @$this->obConfig->klienciNowosciProduktyAll ? null : 'nowosc';
        if ($this->obConfig->klienciNowosciProdukty && $this->view->zalogowanyID > 0) {
            $kontrahenciPolecane = new Kontrahencipolecane($this->module);
            $nowosci = $kontrahenciPolecane->wypiszKlientTyp($this->view->zalogowanyID, 'nowosc');
            if (count($nowosci) > 0)
                $ids = @unserialize($nowosci['id_prod']);;
        }
        $ilosc = //$specjalne->iloscProduktowSpecjalnych('nowosc', $this->lang, 0, @$ids);
                $specjalne->wypiszProduktyPodzieloneStrona(1, $strona, $ile, $this->sortowanie, $this->szukanie, $typ, @$ids);
        $prod = //$specjalne->wypiszSpecjalne('nowosc', $this->lang, 0, $od, $ile, $this->sort);
                $specjalne->wypiszProduktyPodzieloneStrona(0, $strona, $ile, $this->sortowanie, $this->szukanie, $typ, @$ids);

        $all = $this->produktyGaleria($prod);
        $strony = ceil($ilosc / $ile);
        $this->view->all = $all;
        $this->view->strony = $strony;
        $this->view->strona = $strona;
        $this->view->ilosc = $ilosc;
    }

    public function wyprzedazAction() {
        $page = $this->params['action'];
        $site = $this->lang('Wyprzedaz');
        $link = $this->params['paramurl'];
        //$link = $this->common->makeLink($site).'-'.$this->lang;
        $podstrony = new Podstrony($this->_module);
        $podstrony->temat = $link;
        $this->view->link = $link;
        $this->view->site = 'Wyprzedaż';
        $this->view->opis = $podstrona = $podstrony->getPodstrona();
        $tytul = $podstrona['tytul'];
        $site = $podstrona['temat'];
        $link = $podstrona['link'];
        $this->view->nawigacja = '<a href="' . $this->view->baseUrl . '/' . $link . '/">' . $this->arrow . $site . '</a>';
        $this->view->ustawienia['title'] = $site . ' - ' . $this->view->ustawienia['title'];
        $this->view->site = $site;
        $this->wynikiSite();
        $this->view->tryb = $tryb = 'tryb' . strtolower($page);

        $this->view->typ = $typ = 'podstrony';
        $this->view->galeria = $this->common->getGaleria($podstrona['id'], $typ);
        $this->view->videos = $this->common->getVideos($podstrona['id'], $typ);

        $strona = $this->_request->getParam($this->page, 0);
        $specjalne = new Produkty($this->_module);
        $ile = $this->szukanie->ile;
        $od = $strona * $ile;

        $typ = @$this->obConfig->klienciWyprzedazProduktyAll ? null : 'wyprzedaz';
        if ($this->obConfig->klienciWyprzedazProdukty && $this->view->zalogowanyID > 0) {
            $kontrahenciPolecane = new Kontrahencipolecane($this->module);
            $wyprzedaze = $kontrahenciPolecane->wypiszKlientTyp($this->view->zalogowanyID, 'wyprzedaz');
            if (count($wyprzedaze) > 0)
                $ids = @unserialize($wyprzedaze['id_prod']);
        }
        $ilosc = //$specjalne->iloscProduktowSpecjalnych('wyprzedaz', $this->lang, 0, @$ids);
                $specjalne->wypiszProduktyPodzieloneStrona(1, $strona, $ile, $this->sortowanie, $this->szukanie, $typ, @$ids);
        $prod = //$specjalne->wypiszSpecjalne('wyprzedaz', $this->lang, 0, $od, $ile, $this->sort);
                $specjalne->wypiszProduktyPodzieloneStrona(0, $strona, $ile, $this->sortowanie, $this->szukanie, $typ, @$ids);

        $all = $this->produktyGaleria($prod);
        $strony = ceil($ilosc / $ile);
        $this->view->all = $all;
        $this->view->strony = $strony;
        $this->view->strona = $strona;
        $this->view->ilosc = $ilosc;
    }

    public function specjalneAction() {
        $page = $this->params['action'];
        $site = $this->lang('Specjalne');
        $link = $this->params['paramurl'];
        //$link = $this->common->makeLink($site).'-'.$this->lang;
        $podstrony = new Podstrony($this->_module);
        $podstrony->temat = $link;
        $this->view->link = $link;
        $this->view->site = 'Specjalne';
        $this->view->opis = $podstrona = $podstrony->getPodstrona();
        $tytul = $podstrona['tytul'];
        $site = $podstrona['temat'];
        $link = $podstrona['link'];
        $this->view->nawigacja = '<a href="' . $this->view->baseUrl . '/' . $link . '/">' . $this->arrow . $site . '</a>';
        $this->view->ustawienia['title'] = $site . ' - ' . $this->view->ustawienia['title'];
        $this->view->site = $site;
        $this->wynikiSite();
        $this->view->tryb = $tryb = 'tryb' . strtolower($page);

        $this->view->typ = $typ = 'podstrony';
        $this->view->galeria = $this->common->getGaleria($podstrona['id'], $typ);
        $this->view->videos = $this->common->getVideos($podstrona['id'], $typ);

        $strona = $this->_request->getParam($this->page, 0);
        $specjalne = new Produkty($this->_module);
        $ile = $this->szukanie->ile;
        $od = $strona * $ile;

        $typ = @$this->obConfig->klienciSpecjalneProduktyAll ? null : 'specjalne';
        if ($this->obConfig->klienciSpecjalneProdukty && $this->view->zalogowanyID > 0) {
            $kontrahenciPolecane = new Kontrahencipolecane($this->module);
            $specjalne = $kontrahenciPolecane->wypiszKlientTyp($this->view->zalogowanyID, 'specjalne');
            if (count($specjalne) > 0)
                $ids = @unserialize($specjalne['id_prod']);
        }
        $ilosc = //$specjalne->iloscProduktowSpecjalnych('specjalne', $this->lang, 0, @$ids);
                $specjalne->wypiszProduktyPodzieloneStrona(1, $strona, $ile, $this->sortowanie, $this->szukanie, $typ, @$ids);
        $prod = //$specjalne->wypiszSpecjalne('specjalne', $this->lang, 0, $od, $ile, $this->sort);
                $specjalne->wypiszProduktyPodzieloneStrona(0, $strona, $ile, $this->sortowanie, $this->szukanie, $typ, @$ids);

        $all = $this->produktyGaleria($prod);
        $strony = ceil($ilosc / $ile);
        $this->view->all = $all;
        $this->view->strony = $strony;
        $this->view->strona = $strona;
        $this->view->ilosc = $ilosc;
    }

    public function bestselleryAction() {
        $page = $this->params['action'];
        $site = $this->lang('Bestsellery');
        $link = $this->params['paramurl'];
        //$link = $this->common->makeLink($site).'-'.$this->lang;
        $podstrony = new Podstrony($this->_module);
        $podstrony->lang = "pl";
        $podstrony->tytul = "Polecane";
        $this->view->link = $link;
        $this->view->site = 'Bestsellery';
        $this->view->opis = $podstrona = $podstrony->getPodstrona();
        $tytul = $podstrona['tytul'];
        //$site = $podstrona['temat'];
        $link = $podstrona['link'];
        $this->view->nawigacja = '<a href="' . $this->view->baseUrl . '/' . $link . '/">' . $this->arrow . $site . '</a>';
        $this->view->ustawienia['title'] = $site . ' - ' . $this->view->ustawienia['title'];
        $this->view->site = $site;
        $this->wynikiSite();
        $this->view->tryb = $tryb = 'tryb' . strtolower($page);

        $this->view->typ = $typ = 'podstrony';
        $this->view->galeria = $this->common->getGaleria($podstrona['id'], $typ);
        $this->view->videos = $this->common->getVideos($podstrona['id'], $typ);

        $ile = $this->listaProdIlosci[$this->szukanie->{$tryb}][0];
        $achiwum = new Archiwum($this->module);
        $prod = $achiwum->bestsellery(@intval($ile));
        $all = $this->produktyGaleria($prod);

        $this->view->all = $all;
        $this->view->strony = 1;
        $this->view->strona = 0;
        $this->view->ilosc = count($prod);
    }

    private function sort() {
        $order = $this->_request->getParam('order');
        $sort = $this->_request->getParam('sort');
        //echo $order.' q '.$sort;
        $sortowanie = new Zend_Session_Namespace('sortowanieStrona');
        $sortowanie->ok = 1;
        $sortowanie->sort = $sort;
        $sortowanie->order = $order;
        if ($sortowanie->sort == 'pozycja')
            $sortowanie->order = 'asc';
    }

    private function wynikiSite() {

        return null;


        if (@!empty($this->szukanie->nazwa)) {
            if ($this->view->site != $this->lang('wyniki_wyszukiwania'))
                $this->view->site .= ' - ' . $this->lang('wyniki_wyszukiwania') . ' "' . $this->szukanie->nazwa . '"';
            else
                $this->view->site .= ' - "' . $this->szukanie->nazwa . '"';
        }
    }

}

?>