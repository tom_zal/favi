<?php

    class XmlimportController extends Ogolny_Controller_Page {

        public $bledy;

        public function init() {
            parent::init();
			$this->config =  new Zend_Config_Ini('../application/config.ini', 'general');
            $this->view->baseUrl = $this->_request->getBaseUrl();
			$root = $_SERVER['DOCUMENT_ROOT'];
			//$root = str_replace("home/admin/domains/big01.pl/public_html/modelmaking", "", $root);
            $this->docroot = trim(str_replace("//","/",$root.'/'.$this->view->baseUrl),"/");
			//echo getcwd();die();
			//echo $_SERVER['DOCUMENT_ROOT'];die();
			/** sprawdzic obcinanie ostatniego znaku **/
            if(false) $this->docroot = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
            $this->error = array();
            if (!ini_get('safe_mode')) {
                set_time_limit(0);
            }
            /* if(!isset($this->pagestatus) || $this->pagestatus === false) {
                $this->_forward('page', 'index');
                return;
            }
			*/
            $this->_config =  new Zend_Config_Ini('../application/config.ini', 'xmlimport');
			$this->path = $this->docroot.''.$this->_config->path; //die();
			$this->pathLogi = $this->docroot.''.$this->_config->pathLogi;
			//echo $this->path = "http://www.modelmaking.big01.pl/public/admin/subiekt/";
			$this->docroot = "";
			if($_SERVER['HTTP_HOST'] != "www.local.d" && $_SERVER['HTTP_HOST'] != "localhost")
			{
				$this->path = "admin/subiekt/";
				$this->pathLogi = "admin/logi/";
			}
			//$file = fopen($this->path.'zam_0.xml', 'w');
			//fwrite($file, "test"); fclose($file);
			//file_put_contents($this->path.'zam_0.xml', "test"); die();
            $this->ImageDir = new Zend_Config_Ini('../application/config.ini', 'image');
            $this->SmallImageDir = $this->ImageDir->ImageDir . '' . $this->ImageDir->SmallImageDir;

            $this->jednostki = array('minut'=>60, 'godzin'=>3600, 'dni'=>86400, 'sekund'=>1);
            $this->wybranajednostka = 'minut';
            $this->exectime = 150;
            $this->savetime = 60;
            $this->createlog = false;
			$this->test = true;

        }
        
        
        public function getlogAction(){
            $file = $this->_getParam('file');
            $this->_helper->viewRenderer->setNoRender();
            Zend_Layout::getMvcInstance()->disableLayout();
            if(isset($file)) {
                $dirlog = $this->pathLogi;//$this->docroot.''.$this->_config->pathLogi;
                if(!empty($file)) $log = file_get_contents($dirlog.''.$file);
                else return;
                if(!empty($log)) dump(unserialize($log));
                else return;
            } else {return;}
        }
        
        public function testAction() {
            $this->_helper->viewRenderer->setNoRender();
            Zend_Layout::getMvcInstance()->disableLayout();
            die;
            for ($i = 1; $i <= 1000; $i++) {
               echo $i, " - przejscie petli: [", date("H:m:s"),"]";
               //sleep(10);
            }
            
        }
		
		public function importklienciAction()
		{
			set_time_limit(400);
            ini_set('max_execution_time', 0);
            ini_set('max_input_time', 0);
            ini_set('display_errors', 1);
			
			$this->_helper->viewRenderer->setNoRender();
            Zend_Layout::getMvcInstance()->disableLayout();
			$path = $this->path;//$this->docroot.''.$this->_config->path;
		}
		
		public function exportklienciAction()
		{
			set_time_limit(400);
            ini_set('max_execution_time', 0);
            ini_set('max_input_time', 0);
            ini_set('display_errors', 1);
			
			$this->_helper->viewRenderer->setNoRender();
            Zend_Layout::getMvcInstance()->disableLayout();
			$path = $this->path;//$this->docroot.''.$this->_config->path;
			
			$kontrahent = new Kontrahenci();
			$klienci = $kontrahent->wypisz();
			
			$filename = 'klienci.xml';
			$handle = fopen($this->path.''.$filename, "w");
			fwrite($handle, "");
			fclose($handle);
			
			if(count($klienci) > 0)
			foreach($klienci as $dane)
			{
				if(empty($dane['nip'])) continue;
				if(!empty($dane['id_ext'])) continue;
				if(!empty($dane['symbol'])) continue;
				echo 'export '.$dane['id'].' '.$dane['nip'].'<br/>';
				$this->fileXMLExport($dane->toArray(), $this->_config->path, $filename);
				$klient['id_ext'] = strtoupper(substr(md5($dane['ulica'].$dane['miasto'].$dane['id']).$dane['id'], 0, 4).$dane['id']);				
				$klient['symbol'] = strtoupper(substr($dane['faktura']?$dane['nazwa_firmy']:trim($dane['nazwisko'].' '.$dane['imie']), 0, 10));
				$klient['symbol'] = preg_replace('/[^0-9]/', '', $dane['nip']);
				$kontrahent->id = $dane['id'];
				$kontrahent->edytuj($klient);
				echo 'export '.$klient['id_ext'].' '.$klient['symbol'].'<br/>';
			}
		}		

        public function importAction() {
            set_time_limit(400);
            ini_set('max_execution_time', 0);
            ini_set('max_input_time', 0);
             ini_set('display_errors', 1);

            $objSync = new Synchro();
            $objSync->id = 1;
            $sync = $objSync->getRow();

            $dirlog = $this->pathLogi;//$this->docroot.''.$this->_config->pathLogi;

            if (!is_dir($dirlog)) {
                @mkdir($dirlog);
                @chmod($dirlog, 0777);
            }

            if($this->createlog) {
                $log = @fopen($dirlog.'log_'.str_replace(array(':',' '),'_',date('H:i:s d-m-Y')).'_cronccheck.txt', "a+");
                @fwrite($log, 'cron.on.'.date('H:i:s d-m-Y'));
            }
            $this->_helper->viewRenderer->setNoRender();
            Zend_Layout::getMvcInstance()->disableLayout();
            $path = $this->path;//$this->docroot.''.$this->_config->path;

            if($sync['status'] == 'wolny' || !empty($sync['cronstart']) && $this->timeUpCheck($sync['cronstart'], date('H:i:s d-m-Y')) > ($this->exectime + $this->savetime)) 
			{
                $newstart = date('H:i:s d-m-Y');
                $start = !empty($sync['start'])? $sync['start'] : $newstart;
                $this->watek =
                $this->time = time();
                $this->start = $start;
                if(@!$this->test)
				$objSync->_update(array('status' => 'praca','start' => $start,'cronstart'=> $newstart, 'watkow'=>++$sync['watkow']));

                $file = $this->getFileFromDir('*.zip', $path);
				//print_r($file);die();
                if(isset($file[0])) {
                    if($this->createlog) {
                        $log = @fopen($dirlog.'log_'.str_replace(array(':',' '),'_',$start).'_filecheck.txt', "a+");
                        @fwrite($log, 'file.on.'.$start);
                    }
                    $xmlfile = str_replace('.zip', '.xml', str_replace($path, '', $file[0]));
                    $this->extractFile($path, $file[0], $xmlfile);

                    $this->filezip = $file[0];

                    if(file_exists($path.''.$xmlfile)) {
                        $xml = file_get_contents($path.''.$xmlfile);
                        $xml = simplexml_load_string($xml);

                        $array = array();
                        $count = count($xml->Towar);
                        $i=0;
						$Klienci_count = count($xml->Item);
						
                        if($count > 0) {
                            $this->obecny = $sync['towarcaly'] == 1 && intval($sync['obecny']) < ($count-1) ? intval($sync['obecny'])+1 : intval($sync['obecny']);
                            $this->ilosc = $count;
                            for($j = $this->obecny; $j < $count; $j++) {
                                try {
                         
                                ob_flush();
                                $this->obecny = $j;
                                $this->syncbreak();
                                $towar = get_object_vars($xml->Towar[$j]);

                                $array['nazwa'] = str_replace(']]>','',str_replace('<![CDATA[', '', $towar['tw_nazwa']));
                                $array['oznaczenie'] = trim($towar['tw_symbol']);
								if(@empty($towar['tw_symbol'])) continue;
                                //$array['kod_dostawcy'] = trim($towar['tw_dostsymbol']);
                                //$array['kod_tecdoc'] = trim($towar['tw_symbol']);
                                $array['vat'] = $towar['vat_stawka'];
                                $array['cena_netto'] = number_format($this->checkOrd($towar['tc_cenanetto1']), 2, '.','');
                                $array['cena_brutto'] = number_format($this->checkOrd($towar['tc_cenabrutto1']), 2, '.','');
                                $array['cena_netto_hurt'] = number_format($this->checkOrd($towar['tc_cenanetto3']), 2, '.','');
                                $array['cena_brutto_hurt'] = number_format($this->checkOrd($towar['tc_cenabrutto3']), 2, '.','');								
                                $array['cena_promocji_n'] = number_format($this->checkOrd($towar['tc_cenanetto4']), 2, '.','');
                                $array['cena_promocji_b'] = number_format($this->checkOrd($towar['tc_cenabrutto4']), 2, '.','');
								$array['cena_promocji_n_hurt'] = number_format($this->checkOrd($towar['tc_cenanetto4']), 2, '.','');
                                $array['cena_promocji_b_hurt'] = number_format($this->checkOrd($towar['tc_cenabrutto4']), 2, '.','');
								if($array['cena_netto_hurt'] == 0) $array['cena_netto_hurt'] = $array['cena_netto'];
								if($array['cena_brutto_hurt'] == 0) $array['cena_brutto_hurt'] = $array['cena_brutto'];
								if($array['cena_promocji_n'] == 0) $array['cena_promocji_n'] = $array['cena_netto_hurt'];
								if($array['cena_promocji_b'] == 0) $array['cena_promocji_b'] = $array['cena_brutto_hurt'];
								if($array['cena_promocji_n_hurt'] == 0) $array['cena_promocji_n_hurt'] = $array['cena_netto_hurt'];
								if($array['cena_promocji_b_hurt'] == 0) $array['cena_promocji_b_hurt'] = $array['cena_brutto_hurt'];
                                $array['dostepnosc'] = number_format($this->checkOrd($towar['st_stan']), 0, '.','');
                                $array['dostepny'] = 1;
                                $array['widoczny'] = 1;
                                $array['zdjecia'] = $towar['zdjecie'];
                                $array['kategoria'] = is_object($towar['kategoria'])? null: $towar['kategoria'];
                                $array['model'] = is_object($towar['model'])? null: $towar['model'];
                                $array['zamienniki'] = is_object($towar['zamienniki'])? null: $towar['zamienniki'];
                                $array['powiazane'] = is_object($towar['powiazane'])? null: $towar['powiazane'];
                                $array['tekst'] = str_replace(']]>','',str_replace('<![CDATA[', '', $towar['tw_opis']));
								$array['id_subiekt'] = $towar['tw_id'];
								$array['ean'] = $towar['tw_podstkodkresk'];

                                $idsub = $towar['tw_id'];
								$kodkreskowysub = $towar['tw_podstkodkresk'];

                                $Object = new Realizacjexml();

                                $dane = $array;
                                $zdjecia = explode('|', trim($dane['zdjecia']));

                                $aktualizacja = null;
                                (int) $tabsize = $count;
                                (int) $pozycja = $i;
                                (int) $ileProd = 0;
                                (int) $ilePom = 0;

                                /*
                                 * Pobranie produktów
                                 */
                                $Object->cachename = 'all_product';
                                $all = $Object->getAllRowsCache($this->cache, $this->caching);
                         
                                $this->syncbreak();
                               
							    //var_dump($all);
                                $arr = array();
                                if(!empty($all)) { 
                                    $name = 'all_sort';
                                    /*
                                     * Posortowanie i dodanie do cache
                                     */
                                    if($this->cache) $arr = $this->cache->load('cache_'.$name);else $arr = false;
                                    if($arr == false || !$this->caching) {
                                        foreach($all as $row ) {
                                            $arr['pokod'][trim($row['oznaczenie'])] = $row['id'];
                                            $arr['poid'][$row['id']] = trim($row['oznaczenie']);
                                            
                                            $this->syncbreak();
                                        }
                                        if($this->cache) $this->cache->save($arr, 'cache_'.$name);
                                    }
                                    $this->arr = $arr;

                                    
                                    //$aktualizacja['kod'] = $dane['oznaczenie'];
                                    //$aktualizacja['kod_produkt'] = $dane['kod'];
                                    //$aktualizacja['kod_tecdoc'] = $dane['kod_tecdoc'];
                                    //$aktualizacja['kod_dostawcy'] = $dane['kod_dostawcy'];
                                    $aktualizacja['widoczny'] = 1;
                                    $aktualizacja['dostepnosc'] = $dane['dostepnosc'];
                                    $aktualizacja['dostepny'] = !empty($dane['dostepny'])? 1: 0;
                                    $aktualizacja['nazwa'] = $dane['nazwa'];
                                    $aktualizacja['id_subiekt'] = $dane['id_subiekt'];
                                    $aktualizacja['oznaczenie'] = $dane['oznaczenie'];
									$aktualizacja['ean'] = $dane['ean'];

                                    if(true) {
										$aktualizacja['cena_netto'] = $dane['cena_netto'];
                                        $aktualizacja['cena_brutto'] = $dane['cena_brutto'];
										$aktualizacja['cena_netto_hurt'] = $dane['cena_netto_hurt'];
                                        $aktualizacja['cena_brutto_hurt'] = $dane['cena_brutto_hurt'];
										$aktualizacja['cena_promocji_n'] = $dane['cena_promocji_n'];
                                        $aktualizacja['cena_promocji_b'] = $dane['cena_promocji_b'];
                                        $aktualizacja['cena_promocji_n_hurt'] = $dane['cena_promocji_n_hurt'];
                                        $aktualizacja['cena_promocji_b_hurt'] = $dane['cena_promocji_b_hurt'];
                                        $aktualizacja['vat'] = $dane['vat'];
                                    }

                                    if(true) {
                                        $aktualizacja['tekst'] = $dane['tekst'];
										$aktualizacja['skrot'] = $dane['tekst'];
                                    }

                                    $this->syncbreak(); 
                                    /*
                                     * Sprawdzenie czy produkt istnieje
                                     */
                                    $id = false;
                                    $this->_fotop = false;

                                    if(isset($arr['pokod'][trim($dane['oznaczenie'])])) {
                                        $id = $arr['pokod'][trim($dane['oznaczenie'])];
                                    } elseif(!empty($dane['oznaczenie'])) {
                                       
                                        $this->syncbreak();

										//var_dump($aktualizacja);die();
                                        $id = $Object->dodajRealizacje($aktualizacja);

                                        $arr['pokod'][trim($aktualizacja['oznaczenie'])] = $id;
                                        $arr['poid'][$id] = trim($aktualizacja['oznaczenie']);

                                        $this->error['dodanonowe'][] = $id;

                                        if($this->cache) $this->cache->save($arr, 'cache_'.$name);

                                        $nazwa = $dane['nazwa'];

                                        $route = new Routers();
                                        $sprawdz = $route->dodaj($nazwa, $id, 'default', 'produkt', 'index');
                                        $odpowiedz = $sprawdz['id'];
                                        if ($sprawdz['id'] == 0) {
                                            $sprawdz = $route->dodaj($nazwa . '-' . $id, $id, 'default', 'produkt', 'index');
                                        }

                                        $aktualizacja['link'] = $sprawdz['link'];
                                        $aktualizacja['route_id'] = $sprawdz['id'];
                                        
                                        $Object->id = $id;
                                        $Object->edytujRealizacje($aktualizacja);
                                        
                                        $this->syncbreak();
                                        
                                        $this->_fotop = true;
                                    } else {
                                        $ilePom++;
                                        $this->error['pusty'] = 'Pusty kod katalogowy.';
                                    }

									//var_dump($id);
                                    if(isset($id) && $id != false) {

                                        $model = explode('|', $dane['model']);
                                        $zamienniki = explode('|', $dane['zamienniki']);
                                        $powiazane = explode('|', $dane['powiazane']);
                                        //$kategoria = explode('|', trim($dane['kategoria']));

                                        /*
                                         * SAMOCHODY
                                         */
                                        if(false) {
                                            $this->syncbreak();
                                            $sami = $this->samochody($model, $id);
                                            $aktualizacja['on_pojazdy'] = $sami['on_pojazdy'];
                                            if(!empty($sami['error'])) $this->error['samochody'][$id] = $sami['error'];
                                            $this->syncbreak();
                                        }
                                        /*
                                         * ZAMIENNIKI
                                         */
                                        if(false) {
                                            $this->syncbreak();
                                            $zami = $this->powiazania($zamienniki, $id, 'Zamiennikixml');
                                            $aktualizacja['on_zamienniki'] = $zami['on_zamienniki'];
                                            if(!empty($zami['error'])) $this->error['zamiennik'][$id] = $zami['error'];
                                            $this->syncbreak();
                                        }
                                       /*
                                        * POWIAZANE
                                        */
                                        if(false) {
                                            $this->syncbreak();
                                            $powi = $this->powiazania($powiazane, $id, 'Powiazanexml');
                                            if(!empty($powi['error'])) $this->error['powiazane'][$id] = $powi['error'];
                                            $this->syncbreak();
                                        }
                                        /*
                                         * KATEGORIE
                                         */
                                        if(false) {
                                            $this->syncbreak();
                                            $kati = $this->kategorie($kategoria, $id);
                                            //$aktualizacja['on_kategoria'] = $kati['on_kategoria'];
                                            if(!empty($kati['error'])) $this->error['kategoria'][$id] = $kati['error'];
                                            $this->syncbreak();
                                        }
										
										if(false)
										{
											$this->syncbreak();
											if(!empty($dane['kategoria']))
											{
												$oProducent = new Producentxml();
												$producent = $oProducent->findByName($dane['kategoria']);
												if(empty($producent))
												{
													$pro['nazwa'] = $dane['kategoria'];
													$pro['rabat'] = 0.00;
													$r = $oProducent->dodajProducenta($pro);
													if(empty($r))
													{
														$this->error['producent'][$id] = 'Blad dodawania producenta.';
													} else {
														$aktualizacja['producent'] = $r;
													}
												} else {
													$aktualizacja['producent'] = $producent['id'];
												}
											}
											$this->syncbreak();
										}
                                        /*
                                         * ZDJECIA
                                         */
                                        if(true) {
                                            $this->syncbreak();
											//echo $path.''.$this->_config->pathImg.''.$idsub.'_*.jpg';
                                            $zdjecia  = glob($path.''.$this->_config->pathImg.''.$idsub.'_*.jpg');
                                            //$zdjecia  = glob($path.''.$this->_config->pathImg.''.$kodkreskowysub.'.jpg');
											if(empty($zdjecia)) $zdjecia  = glob($path.''.$this->_config->pathImg.''.$idsub.'.JPG');

                                            if(is_array($zdjecia) && !empty($zdjecia)) {
								
                                                $zdje = $this->zdjecia($zdjecia, null, $id);
                                                if(!empty($zdje['error'])) $this->error['zdjecia'][$id] = $zdje['error'];
                                            }
                                            $this->syncbreak();
                                        }
                                        $Object->id = $id;
										unset($aktualizacja['nazwa']);
                                        $Object->edytujRealizacje($aktualizacja);
                                        $ileProd++;
                                        $objSync->_update(array('towarcaly'=> 1, 'obecny'=> $j));
                                        $this->syncbreak();
                                    }

                                }

                                if($this->obecny == ($this->ilosc -1)) {
                                    $this->cache->clean(Zend_Cache::CLEANING_MODE_ALL);
                                    if(@!$this->test) @unlink($this->filezip);
                                    if(@!$this->test) @unlink($path.''.$xmlfile);
                                    $koniec = date('H:i:s d-m-Y');
                                    $roznica = (strtotime($koniec) - strtotime($start));
                                    $czas = $roznica > 0 ? '~'.round($roznica / $this->jednostki[$this->wybranajednostka]).' '.$this->wybranajednostka: 'mniej niż 1 sekunda';
                                    $objSync->_update(array('status' => 'wolny','koniec' => $koniec, 'czas' => $czas));

                                    $this->error['koniec'] = 'Zakończono: '.$koniec;
                                    $this->error['czas_aktualizacji'] = 'Czas aktualizacji: '.$czas;
                                    if($this->createlog) {
                                        $log = @fopen($dirlog.'log_'.str_replace(array(':',' '),'_',$start).'.txt', "a+");
                                        $tresclog = @file_get_contents($log);
                                        @fwrite($log, $tresclog.''.serialize($this->error));
                                    }
                                    $objSync->_update(array('status' => 'wolny', 'start'=> null, 'cronstart'=> '', 'ilosc'=> 0, 'obecny'=>0,'towarcaly'=>0, 'koniec' => $koniec, 'zakonczony'=> 'Tak dnia:'.$start,'watkow'=>0, 'watkowakt'=>$sync['watkow']));
                                    $this->error = null;
                                }
                                
                                } catch (Exception $e) {
                                    echo 'Caught exception: ',  $e->getMessage(), "\n";
                                    $this->syncbreak();
                                }

                                $i++;
                            }
                        } else {
                            $this->error['no_product'] = 'Nie znaleziono produktów.';
                        }
						
						if($Klienci_count > 0)
						{
							//echo 'Klientów: '.$Klienci_count;
							$oKliencixml = new Kliencixml();
							for($k = 0; $k < $Klienci_count; $k++)
							{
								try {
									$kontrahent = array();
									$klient = get_object_vars($xml->Item[$k]);
									$kontrahent['id_subiekt'] = $klient['kh_id'];
									$kontrahent['symbol'] = $klient['kh_symbol'];
									if(@empty($klient['kh_symbol'])) continue;
									$kontrahent['nazwa'] = !empty($klient['Nazwa_krotka'])?$klient['Nazwa_krotka']:'';
									$kontrahent['nazwa_firmy'] = !empty($klient['nazwa_firmy'])?$klient['nazwa_firmy']:'';
									if($kontrahent['nazwa'] == $kontrahent['nazwa_firmy']) $kontrahent['nazwa'] = "";
									$kontrahent['imie'] = !empty($klient['imie'])?$klient['imie']:'';
									$kontrahent['nazwisko'] = !empty($klient['nazwisko'])?$klient['nazwisko']:'';
									$kontrahent['ulica'] = !empty($klient['ulica'])?$klient['ulica']:'';
									$kontrahent['nr'] = !empty($klient['numer'])?$klient['numer']:'';
									$kontrahent['mieszkanie'] = !empty($klient['mieszkanie'])?$klient['mieszkanie']:'';
									$kontrahent['kod'] = !empty($klient['kod'])?$klient['kod']:'';
									$kontrahent['miasto'] = !empty($klient['miasto'])?$klient['miasto']:'';
									//$kontrahent['wojew'] = "podkarpackie";
									$kontrahent['kraj'] = "Polska";
									$kontrahent['nip'] = !empty($klient['nip'])?$klient['nip']:'';
									$kontrahent['email'] = !empty($klient['email'])?$klient['email']:'';
									$kontrahent['rabat'] = !empty($klient['Rabat'])?$klient['Rabat']:'';
									$kontrahent['faktura'] = !empty($klient['faktura'])?$klient['faktura']:'';
									$kontrahent['widoczny'] = !empty($klient['widoczny'])?$klient['widoczny']:'';
									$kontrahent['telefon'] = !empty($klient['telefon'])?$klient['telefon']:'';
									$kontrahent['id_ext'] = strtoupper(substr(md5($klient['ulica'].$klient['miasto'].$klient['kh_id']).$klient['kh_id'], 0, 4).$klient['kh_id']);
									$kontrahent['rodzaj_klienta'] = 0;
									$kontrahent['weryfikacja'] = 1;
									$kontrahent['weryfikacja_admin'] = 0;
									$kontrahent['haslo'] = md5($kontrahent['symbol']);
									
									$rKlient = $oKliencixml->findByIdSubiekt($kontrahent['id_subiekt']);
									if(empty($rKlient))
									{
										$rDodaj = $oKliencixml->dodajKlient($kontrahent);
									} else {
										unset($kontrahent['id_ext']);
										unset($kontrahent['rodzaj_klienta']);
										unset($kontrahent['id_subiekt']);
										$oKliencixml->edytujKlientByIdSubiekt($kontrahent, $klient['kh_id']);
									}
								} catch (Exception $e) {
									echo 'Caught exception: ',  $e->getMessage(), "\n";
									$this->syncbreak();
								}							
							}
							if(@!$this->test) @unlink($this->filezip);
							if(@!$this->test) @unlink($path.''.$xmlfile);
							echo '<br />Dodano '.$oKliencixml->ileDodanych.' nowych klientów.';
							echo '<br />Aktualizowano '.$oKliencixml->ileAktualizowanych.' klientów.';
						} else {
							$this->error['no_product'] = 'Nie znaleziono klientów.';
						}
                    }

                }  else {
                    $this->error['file_not_found'] = 'Nie znaleziono pliku do importu.';
                }
                /*LOGI*/
                if(isset($this->error) && !empty($this->error)) {
                    $this->error['koniec'] = 'Zakończono: '.$koniec = date('H:i:s d-m-Y');
                    $roznica = (strtotime($koniec) - strtotime($start));
                    $czas = $roznica > 0 ? '~'.round($roznica / $this->jednostki[$this->wybranajednostka]).' '.$this->wybranajednostka: 'mniej niż 1 sekunda';
                    $this->error['czas_aktualizacji'] = 'Czas aktualizacji: '.$czas;

                    if($this->createlog) {
                        $log = @fopen($dirlog.'log_'.str_replace(array(':',' '),'_',$start).'.txt', "a+");
                        $tresclog = @file_get_contents($log);
                        @fwrite($log, $tresclog.''.serialize($this->error));
                    }
                    $objSync->_update(array('status' => 'wolny','obecny' => 0, 'start'=> null, 'cronstart'=> '', 'ilosc'=>0, 'czas' => $czas, 'towarcaly' => 0,'koniec'=> $koniec,'watkow'=>0));
                }
//print_r($this->error);
                $kom = 'Synchronizacja zako&#324;czono.';
                echo '<div style="text-align: center; padding: 10px;">'.$kom.'</div>';

            } else {
                die('Cron status : praca.');
            }
            die;
        }
		
		public function exportzamowienAction()
		{
			$this->_helper->viewRenderer->setNoRender();
            Zend_Layout::getMvcInstance()->disableLayout();
			$path = $this->path.'Zamowienia/';
			$oZamowieniakliencixml = new Zamowieniakliencixml();
			$oZamowieniaproduktyxml = new Zamowieniaproduktyxml();
			$Zamowieniakliencixml = $oZamowieniakliencixml->findAll();
			$xml = '';
			if(!empty($Zamowieniakliencixml))
			{
				foreach($Zamowieniakliencixml as $zam)
				{
					if(!empty($zam['id_subiekt']))
					{
						$xml = '<?xml version="1.0" standalone="yes"?>';
						$xml .= '<ZAM>';
						$xml .= '<NAG>';
						$xml .= '<id>'.$zam['id'].'</id>';
						$xml .= '<kh_id>'.$zam['id_subiekt'].'</kh_id>';
						$xml .= '<numer>'.$zam['id_ext'].'</numer>';
						$xml .= '<Data>'.date('Y-m-d',strtotime($zam['data'])).'</Data>';
						$xml .= '<data_realizacji>'.date('Y-m-d',strtotime($zam['data'])).'</data_realizacji>';
						$xml .= '<uwagi>'.$zam['uwagi'].'</uwagi>';
						$xml .= '<netto>0</netto>'; //0 - cena liczona z netto; 1 - cena liczona z burtto;
						$xml .= '</NAG>';
						$Zamowieniaproduktyxml = $oZamowieniaproduktyxml->findByIdZam($zam['id']);
						if(!empty($Zamowieniaproduktyxml))
						{
							foreach($Zamowieniaproduktyxml as $prod)
							{
								$xml .= '<POZ>';
								$xml .= '<tw_id>'.$prod['id_subiekt'].'</tw_id>';
								$xml .= '<cena>'.number_format($prod['cena_netto'],4).'</cena>';
								$xml .= '<rabat>'.number_format($prod['rabat'],4).'</rabat>';
								$xml .= '<ilosc>'.number_format($prod['ilosc'],4).'</ilosc>';
								$xml .= '</POZ>';
							}
						} else {
							
							echo '<br />Zamowienie bez produktow.';
						}
						$xml .= '</ZAM>';
						//echo '<br />'.$xml;
						$xml = iconv("UTF-8", "Windows-1250", $xml);
						file_put_contents($path.'zam_'.$zam['id'].'.xml', $xml);
					}
				}
				echo '<br />Eksport zamowien zakonczony.';
			} else {
				echo '<br />Brak zamowien';
			}
		}
		
		public function wypakujproduktyAction()
		{
			$this->_helper->viewRenderer->setNoRender();
            Zend_Layout::getMvcInstance()->disableLayout();
			$path = $this->path;//$this->docroot.''.$this->_config->path;
			$this->extractFile($path.''.$this->_config->pathImg, $path.''.$this->_config->pathImg.'produkty.zip', null);
		}

        /*Pobranie plików z katalogu*/
        protected function getFileFromDir($pattern='*.xml', $path = false, $flags = 0) {
            //print_r($path);
            $paths = @glob($path.'*', GLOB_MARK|GLOB_ONLYDIR|GLOB_NOSORT);
            $files = @glob($path.$pattern, $flags);

            if(false) foreach ($paths as $path) $files = array_merge($files, $this->getFileFromDir($pattern, $flags, $path));

            return $files;
        }

        /*Wypakowanie plików do katalogu*/
        protected function extractFile($Extras, $Zipfilename, $file) {
            $zip = new ZipArchive();
            if ($zip->open($Zipfilename) !== TRUE) {
                $this->error['open_zip_failed'][] = "1# Nie można otworzyć #archiwum " . $Zipfilename . "";
            } else {
                if ($zip->extractTo($Extras, $file) !== TRUE) {
                    $this->error['open_file_failed'][] = "2# Nie można wypakować pliku " . $file . "";
                }
                $zip->close();
            }
        }

        protected function powiazania($zamienniki, $id, $class = 'Zamiennikixml') {
            ob_flush();
            $zam = null;
            $error = null;
            $_arr['on_'.strtolower($class)] = 0;
            $this->syncbreak();

            if(isset($zamienniki[0]) && !empty($zamienniki[0]) && is_array($zamienniki)) {
                $zamienniki[] = $id;
                $zamiennik = array_merge(array_filter(array_unique($zamienniki)));

                for($j=0;$j<count($zamiennik);$j++) {
                    $this->syncbreak();
                    $array_tmp = array();
                    $array_tmp = $zamiennik;

                    $ID_prod = $array_tmp[$j];
                    unset($array_tmp[$j]);

                    /*
                     * Podzielenie zamiennikow na poszczegolne tablice
                     */
                    foreach($array_tmp as $sort) {
                        $zam2[$j][] = $sort;
                    }
                    $array_temp = $zam2;

                    $ObjectZam = new $class();

                    /***DODAWANIE ZAMIENNIKOW DO BAZY***/
                    for ($z = 0; $z < count($array_temp[$j]); $z++) {
                        $this->syncbreak();
                        $ObjectZam->cachename = 'all_pow_'.$class;

                        $all = $ObjectZam->getAllRowsCache($this->cache, $this->caching);
                        $name = 'all_pow_'.$class.'_sort';

                        /*
                         * Posortowanie i dodanie do cache
                         */
                        if($this->cache) $arr4 = $this->cache->load('cache_'.$name);else $arr4 = false;
                        if($arr4 == false || !$this->caching) {
                            foreach($all as $row ) {
                                $arr4[$row['id_prod']][$row['powiazany']] = $row['id_prod'];
                            }
                            $this->syncbreak();
                            if($this->cache) $this->cache->save($arr4, 'cache_'.$name);
                        }

                        if(!isset($arr4[$ID_prod][$array_temp[$j][$z]])) {
                            $this->syncbreak();
                            $zamDane = array(
                                'powiazany' => $array_temp[$j][$z],
                                'id_prod' => $ID_prod
                            );

                            if(isset($this->arr['poid'][$array_temp[$j][$z]]) && isset($this->arr['poid'][$ID_prod])) {
                                $this->syncbreak();
                                $add = $ObjectZam->_updateAdapter($zamDane);
                                if(!empty($add)) {
                                    $arr4[$ID_prod][$array_temp[$j][$z]] = $ID_prod;
                                    if($this->cache) $this->cache->save($arr4, 'cache_'.$name);

                                    $_arr['on_'.strtolower($class)] = 1;
                                }
                                $this->syncbreak();
                            } else {
                                $error .= '1#Nie odnaleziono produktu o id#'.$ID_prod.' lub id#'.$array_temp[$j][$z].'<br />';
                            }
                        } else {
                            $error .= '1#.'.strtolower($class).' o id#'.$array_temp[$j][$z].' są już przypisane dla części o id#'.$ID_prod.'<br />';
                        }
                    }
                    unset($array_temp);
                }
            }
            unset($zamiennik);
            unset($zamienniki);
            unset($zam);
            $_arr['error'] = $error;

            return $_arr;
        }

        protected function samochody($model, $id) {
            ob_flush();
            $error = null;
            $_arr['on_pojazdy'] = 0;

            if(!empty($model) && is_array($model)) {
                $ObjectKat = new Kategorie();
                for($i=0; $i<count($model); $i++) {
                    $this->syncbreak(); 
                    if(intval($model[$i])) {
                        $ObjectKat->cachename = 'all_samochody';
                        $all = $ObjectKat->getAllRowsCache($this->cache, $this->caching);
                        $name = 'all_sort_samochody';
                        $this->syncbreak(); 
                        /*
                         * Posortowanie i dodanie do cache
                         */
                        if($this->cache) $arr2 = $this->cache->load('cache_'.$name);else $arr2 = false;
                        if($arr2 == false || !$this->caching) {
                            foreach($all as $row ) {
                                $arr2[$row['id']] = $row['id'];
                            }
                            $this->syncbreak(); 
                            if($this->cache) $this->cache->save($arr2, 'cache_'.$name);
                        }

                        if(isset($arr2[trim($model[$i])])) {

                            $ObjectSam = new Samprodxml();
                            $ObjectSam->cachename = 'all_pow_samochody';

                            $all = $ObjectSam->getAllRowsCache($this->cache, $this->caching);
                            $name = 'all_pow_samochody_sort';
                            $this->syncbreak();
                            
                            /*
                             * Posortowanie i dodanie do cache
                             */
                            if($this->cache) $arr3 = $this->cache->load('cache_'.$name);else $arr3 = false;
                            if($arr3 == false || !$this->caching) {
                                foreach($all as $row ) {
                                    $arr3[$row['id_prod']][$row['id_sam']] = $row['id_prod'];
                                }
                                if($this->cache) $this->cache->save($arr3, 'cache_'.$name);
                                $this->syncbreak(); 
                            }

                            if(!isset($arr3[$id][trim($model[$i])])) {
                                $sam = array(
                                    'id_sam' => trim($model[$i]),
                                    'id_prod' => $id
                                );
                                $add = $ObjectSam->add($sam);
                                $this->syncbreak();
                                if($add) {
                                    $arr3[$id][trim($model[$i])];
                                    if($this->cache) $this->cache->save($arr3, 'cache_'.$name);
                                }
                            } else {
                                $error .= '1#Model o id#'.trim($model[$i]).' jest już przypisany dla części o id#'.$id.'<br />';
                            }
                            $_arr['on_pojazdy'] = 1;
                        } else {
                            $error .= '1#Nie odnaleziono modelu o id#'.trim($model[$i]).' dla części o id#'.$id.'<br />';
                        }
                    }
                }
            }
            $_arr['error'] = $error;
            return $_arr;
        }

        protected function kategorie($kategorie, $id) {
            ob_flush();
            $error = null;
            $_arr['on_kategoria'] = 0;
            $this->syncbreak();
            if(!empty($kategorie)){
                $ObjectKat = new Ofertaxml();

                for ($i = 0; $i < count($kategorie); $i++) {
                    if(intval($kategorie[$i])) {
                    $ObjectKat->cachename = 'all_kategorie';
                    $all = $ObjectKat->getAllRowsCache($this->cache, $this->caching);
                    $name = 'all_sort_kategorie';
                    $this->syncbreak();
                    /*
                     * Posortowanie i dodanie do cache
                     */
                    if($this->cache) $arr2 = $this->cache->load('cache_'.$name);else $arr2 = false;
                        if($arr2 == false || !$this->caching) {
                            foreach($all as $row ) {
                                $arr2[$row['id']] = $row['id'];
                                $this->syncbreak();
                            }
                            if($this->cache) $this->cache->save($arr2, 'cache_'.$name);
                        }
                        $this->syncbreak();
                        if(isset($arr2[trim($kategorie[$i])])) {

                            $ObjectKatProd = new Katprod();
                            $ObjectKatProd->cachename = 'all_pow_kategorie';

                            $all = $ObjectKatProd->getAllRowsCache($this->cache, $this->caching);
                            $name = 'all_pow_kategorie_sort';
                            $this->syncbreak();
                            /*
                             * Posortowanie i dodanie do cache
                             */
                            if($this->cache) $arr3 = $this->cache->load('cache_'.$name);else $arr3 = false;
                            if($arr3 == false || !$this->caching) {
                                foreach($all as $row ) {
                                    $arr3[$row['id_prod']][$row['id_kat']] = $row['id_prod'];
                                    $this->syncbreak();
                                }
                                if($this->cache) $this->cache->save($arr3, 'cache_'.$name);
                            }
                            $this->syncbreak();
                            if(!isset($arr3[$id][trim($kategorie[$i])])) {
                                $add = $ObjectKatProd->add(trim($kategorie[$i]), $id);
                                if($add) {
                                    $arr3[$id][trim($kategorie[$i])];
                                    if($this->cache) $this->cache->save($arr3, 'cache_'.$name);
                                }
                                $this->syncbreak();
                            } else {
                                $error .= '1#Kategoria o id#'.trim($kategorie[$i]).' jest już przypisany dla części o id#'.$id.'<br />';
                            }
                            $_arr['on_kategoria'] = 1;
                        } else {
                            $error .= '1#Nie odnaleziono kategorii o id#'.trim($kategorie[$i]).' dla części o id#'.$id.'<br />';
                        }
                    }
                }
                unset($kategorie);

            }
            $_arr['error'] = $error;
            return $_arr;
        }

        protected function zdjecia($zdjecia, $options = null, $id) {
            ob_flush();
            $error = null;
            $this->syncbreak();
            if(isset($zdjecia[0]) && !empty($zdjecia[0])) {
                $galeria = new Galeria();
                $galeria->cachename = 'all_galerie';
                if(false) $all = $galeria->getAllRowsCache($this->cache, $this->caching);
                $name = 'all_sort_galerie';
                $this->syncbreak();
                /*
                 * Posortowanie i dodanie do cache
                 */
                if($this->cache) $arr2 = $this->cache->load('cache_'.$name);else $arr2 = false;
                if($arr2 == false || !$this->caching) {
                   if(!empty($all))
                    foreach($all as $row ) {
                        $arr2[$row['wlasciciel']][] = $row['img'];
                        $this->syncbreak();
                    }
                    if($this->cache) $this->cache->save($arr2, 'cache_'.$name);
                }
                $this->syncbreak();
                
                if(false && isset($arr2[$id])) {
                    $galeria->usunGalerieCache($arr2[$id], $id);
                    unset($arr2[$id]);
                }

                $options['path_root'] = $this->path;//$this->docroot.''.$this->_config->path;
                $options['path_miniaturki'] = $this->ImageDir->ImageDir . '' . $this->ImageDir->SmallImageDir;
                $options['path_thumbs'] = $this->ImageDir->ImageDir . '' . $this->ImageDir->Thumbs;
				$options['path_tiny'] = $this->ImageDir->ImageDir . '' . $this->ImageDir->Tiny;
                $options['path_zip'] = $this->filezip;

                for ($j = 0; $j < count($zdjecia); $j++) {
                    $this->syncbreak();
                    if(false && !empty($zdjecia[$j])) $this->extractFile($options['path_root'], $options['path_zip'], trim($zdjecia[$j]));

                    $error .= isset($this->bledy)? $this->bledy : '';

                    if(!empty($zdjecia[$j]) && file_exists($zdjecia[$j])) {
                        $this->syncbreak();
                        $galeria->iduniq = $id.''.$j;
                        $gal = $galeria->_stworzMiniature(null, trim($zdjecia[$j]), $options['path_miniaturki'], $options['path_thumbs'], $options['path_tiny']);
               
						$galeria->glowne = ($this->_fotop == true && $j == 0) ? true : false;
                        
                        if(isset($arr2[$id]) && !empty($arr2[$id])) $galeria->glowne = false;
                        elseif($j == 0) $galeria->glowne = true;
						
						//var_dump($gal);die();
                   
                        $lastid = $galeria->zapiszDoBazy(trim($gal), $id);
						$galeria->ustawGlowne($lastid, $id);
                        $this->syncbreak();
                        $arr2[$id][] = $lastid;
                        if(@!$this->test) @unlink($options['path_root']. '' .$this->_config->pathImg.''.trim($gal));

                    } else {
                        $error .= '1#Nie odnaleziono zdjęcia o nazwie#'.$zdjecia[$j].'<br />';
                    }
                }
                $this->syncbreak();
                if($this->cache) $this->cache->save($arr2, 'cache_'.$name);
            }
            $_arr['error'] = $error;

            return $_arr;
        }

        public function rrmdir($dir) {
           if (is_dir($dir)) {
             $objects = scandir($dir);
             foreach ($objects as $object) {
               if ($object != "." && $object != "..") {
                 if (filetype($dir."".$object) == "dir") rrmdir($dir."".$object); else @unlink($dir."".$object);
               }
             }
             reset($objects);
             @rmdir($dir);
           }
        }

        public function syncbreak(){
            $objSync = new Synchro();
            $objSync->id = 1;
            if(time() - $this->time > $this->exectime) {
                $objSync->_update(array('status' => 'wolny','obecny' => $this->obecny, 'ilosc'=>$this->ilosc)); 
                
                $log = @fopen($this->pathLogi.'log_'.str_replace(array(':',' '),'_',$this->start).'.txt', "a+");
                $tresclog = @file_get_contents($log);
                @fwrite($log, $tresclog.''.serialize($this->error));
                die('timeout');
            }
        }

        public function timeUpCheck($_start, $_end) {
            $start = strtotime($_start);
            $end = strtotime($_end);
            return $end - $start;
        }
        
        public function checkOrd($temp) {
            $drugie = '';
            $tabl = array('177','230','234','179','241','243','182','188','191','161','198','202','163','209','211','166','172','175');
            for($w=0;$w<strlen($temp);$w++){
                $temp1 = ord(substr($temp, $w, 1));
                if(($temp1 >= 32 && $temp1 < 127) || in_array($temp1, $tabl)){
                    $drugie .= substr($temp, $w, 1);
                }
            }
            return $drugie;
        }
		
		function fileXMLExport($data, $path = '/public/admin/subiekt/', $filename ='klienci.xml')
		{
			$xml = new DOMDocument('1.0', 'utf-8');
			$xml->preservWhiteSpace = false;
			$xml->formatOutput = true;
			$items = $xml->createElement('items');
			$items = $xml->appendChild($items);

			$_file = @fopen($this->path.''.$filename, "r+");
			if(isset($_file)) {
				$_file = @file_get_contents($this->path.''.$filename);
				$xmlread = @simplexml_load_string($_file);

				if(isset($xmlread->item)) {
					foreach($xmlread->item as $tab) {
						$var = get_object_vars($tab);
						$item = $xml->createElement('item');
						$item = $items->appendChild($item);
						foreach($var as $key=>$value) {
							if($key == "komorka") $key = "telefon_komorkowy";
							$item->appendChild($xml->createElement($key, $value));
							if($key == "rodzaj_klienta") break;
						}
					}
				}
			}

			$item = $xml->createElement('item');
			$item = $items->appendChild($item);
			//var_dump($data);
			foreach($data as $key=>$value) {
				if($key == "komorka") $key = "telefon_komorkowy";
				$item->appendChild($xml->createElement($key, $value));
				if($key == "rodzaj_klienta") break;
			}
			$output = $xml->saveXML();

			$handle = fopen($this->path.''.$filename, "w");
			fwrite($handle, $output);
			fclose($handle);
		}

    }

?>