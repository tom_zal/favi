<?php
class PlatnosciController extends Ogolny_Controller_Sklep
{
    public function init()
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
    }
	
    public function indexAction()
	{
        $zamowienie = new Zamowienia();
        $online = new Online();
        $idzam = $this->_request->getParam('idzam',0);

        $this->view->koszyk = $zamowienie->selectWybranyProdukt($idzam);
        $kontrahent = $zamowienie->selectWybranyKontrahent($idzam);
        $this->view->kontrahent = $kontrahent[0];

        $this->view->pos = $online->wypiszDanePos();
    }
	
    function zamowienieokAction()
	{
        $id = (int)$this->_request->getParam('id', 0);
        $weryfikacja = $this->_request->getParam('weryfikacja', 0);
        if($id != 0 && $weryfikacja != '')
		{
            $zamowienie = new Zamowienia();
            $dane = $zamowienie->selectWybranyKontrahent($id);
            if(trim($dane[0]['potwierdzenie_kod'])==trim($weryfikacja))
			{
                $zamowienie->weryfikujZamowienie($id);
                $this->view->wiadomosc = '<div style="font-family: Tahoma; font-size: 15px; color: green; margin-top: 40px;">
			  	Weryfikacja zamówienia przebiegła pomyślnie.<br>
			  	Zapraszamy do dalszych zakupów.
  			  	</div>';				
            }
            else
			{
                $this->view->wiadomosc = '<div style="font-family: Tahoma; font-size: 15px; color: red; margin-top: 40px;">
			  	Wystąpił błąd podczas weryfikacji zamówienia.<br>
			  	Prosimy o kontakt telefoniczny z naszą firma<br>w celu weryfikacji zamówienia.
  			  	</div>';				
            }
        }
        else
		{
            $this->view->wiadomosc = '<div style="font-family: Tahoma; font-size: 15px; color: green; margin-top: 40px;">
			  	Zamówienie przebiegło pomyślnie na podany adres e-mail został wysłany list z potwierdzeniem.
			  	Prosimy o kliknięcie linku znadującego sie w e-mailu w celu potwierdzenia zamówienia.
  			  	</div>';
        }
    }
}
?>