<?php
    class Przelewy24Controller extends Ogolny_Controller_Sklep {
        public function init(){
            parent::init();
            $this->view->baseUrl = $this->baseUrl = $this->_request->getBaseUrl();
            define("DEBUG", 1);
            define("USE_SANDBOX", 0);
            define("LOG_FILE_PRZELEWY24", $_SERVER['DOCUMENT_ROOT']."/public/admin/logi/przelewy24Error.log");
            
            if(USE_SANDBOX == 1) $this->view->testbledow = ''; //wpisujemy w przypadku gdy chcemy testowac obsluge bledow zwrotnych w przelewy24 na testowym koncie - TEST_ERR04, TEST_ERR54, TEST_ERR102, TEST_ERR103 lub TEST_ERR110

            $this->view->onlineserviceformlink = USE_SANDBOX == true ? 'https://sandbox.przelewy24.pl/index.php' : 'https://secure.przelewy24.pl/index.php';
            $this->view->onlineservicescriptlink = USE_SANDBOX == true ? 'https://sandbox.przelewy24.pl/external/formy.php?id=' : 'https://secure.przelewy24.pl/external/formy.php?id=';
            $this->onlinetransactionlink = USE_SANDBOX == true ? 'https://sandbox.przelewy24.pl/transakcja.php' : 'https://secure.przelewy24.pl/transakcja.php';
        }

        public function indexAction() {
            $zamowienie = new Zamowienia($this->module);
            $przelewy24 = new Przelewy24($this->module);
            $id = $this->_request->getParam('id', 0);
            $kontrahent = $zamowienie->selectWybranyKontrahent($id);
            $razem = $zamowienie->selectWybranyProduktAll($id, false, false, true, true);
            $kontrahent[0]['wartosc_zamowienia_netto'] = $razem['wartosc_netto'];
            $kontrahent[0]['koszt_dostawy_netto'] = $this->common->obliczNettoFromBruttoVat($kontrahent[0]['koszt_dostawy'],23);

            $this->view->zamowienie = $kontrahent[0];
            $this->view->przelewy24 = $przelewy24->_get();
            $this->view->error = !isset($kontrahent[0]['email']) || (isset($kontrahentt[0]['email']) && empty($kontrahent[0]['email'])) ? '<div class="k_blad">Nie możesz dokończyć zamówienia, ponieważ nie podałes poprawnych danych. Adres e-mail jest wymagany.<br />Spróbuj ponownie lub skontaktuj się z obsługą sklepu.</div>' : '';
        }
        
        public function backAction() {
            $params = $this->_getAllParams();
            
            $koszyk = new Koszyk($this->module);
				    $koszyk->clean(true);
            
            if(isset($params['p24_session_id'])) {
                $przelewy24 = new Przelewy24($this->module);
                $platnosci = new Platnosci($this->module);
                $config = $przelewy24->_get();
                
                $prz24array = array( //tablica danych dla weryfikacja transakcji
                    'session_id'=>$params['p24_session_id'],
                    'order_id'=>$params['p24_order_id'],
                    'id_sprzedawcy'=>$config->idsprzedawcy,
                    'id_crc'=>$config->crc,
                    'url'=> $this->onlinetransactionlink
                );
                /*
                 * Pobranie zamowienie - w celu uzyskania kwoty dla weryfikacja zamowienia, potwierdzenie zamowienia
                 */
                 
                $zamowienie = new Zamowienia($this->module);
                $zam = $zamowienie->selectWybranyKontrahent($params['p24_session_id']); //pobranie zamowienia
                if(isset($zam[0]['id'])) {
                    $zamowienie->weryfikujZamowienie($params['p24_session_id']); //potwierdzenie
                    $kwota = $zam[0]['wartosc_zamowienia'] + $zam[0]['koszt_dostawy']; //Kwota z dostawa
                    if($zam[0]['rabat_wartosc'] > 0) $kwota -= $zam[0]['rabat_wartosc']; //Rabat
                    $kwota = ($kwota) * 100; //na grosze
                    /*
                     * Weryfikacja poprawnosci zamowienia w przelewy24
                     */
                    try {
                        $weryfikacja = $przelewy24->p24_weryfikuj($prz24array['url'], $prz24array['id_sprzedawcy'],$prz24array['session_id'],$prz24array['order_id'], $kwota, $prz24array['id_crc']); 
                        if($weryfikacja[0] == "TRUE") {
                            $status = $przelewy24->status('zakonczona'); //status tekstowy w przelewy24
                            $this->view->wiadomosc = '<div class="k_ok">Zamówienie przebiegło pomyślnie.<br/>Dziękujemy za zakupy w naszym sklepie.</div>';
                        }
                        else {
                            $status = $przelewy24->status($weryfikacja[1]); //status tekstowy w przelewy24
                            $this->view->wiadomosc = '<div class="k_blad">Wystąpił błąd podczas dokonywania płatności. <br/>Skontaktuj się z administratorem sklepu.<br/>'.$status.'</div>';
                        }
                    }
                    catch(Zend_Exception $e) { //nieoczekiwany blad w czasie wryfikacja transakcji
                        if(DEBUG == 1) {
                            error_log(date('[Y-m-d H:i e] '). "Problem z weryfikacją transakcji w przelewy24: " . $e . PHP_EOL, 3, LOG_FILE_PRZELEWY24);
                        }
                        $status = $przelewy24->status('bladserwis'); //status tekstowy
                        $this->view->wiadomosc = '<div class="k_blad">Wystąpił błąd podczas dokonywania płatności. <br/>Skontaktuj się z administratorem sklepu.<br/>'.$status.'</div>';
                    }
                    $platnosci->zmienStatusZamowienia($prz24array['session_id'], array('message' => $status), 'przelewy24', $prz24array['order_id']); //zmiana statusu w bazie
                }
                else {
                    if(DEBUG == 1) {
                        error_log(date('[Y-m-d H:i e] '). 'Nie odnaleziono zamowienia o id: '.$prz24array['session_id'].' podanego przez przelewy24. Id transakcji: **'.substr($prz24array['order_id'], 2). PHP_EOL, 3, LOG_FILE_PRZELEWY24);
                    }
                    $this->view->wiadomosc = '<div class="k_blad">Wystąpił błąd podczas dokonywania płatności. Nie odnaleziono zamówienia dla podanej transakcji.<br/>Skontaktuj się z administratorem sklepu.</div>';
                } 
            } 
            else {
                $this->_redirect($this->baseUrl.'/');
            }
        }
    }
?>