<?php

class IndexController extends Ogolny_Controller_Sklep {

    public function init() {
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
    }

    function indexAction() {
        $this->view->main = true;
        $this->view->site = 'main';
        $this->view->ilu = 4;
        $this->sortBy = true;
        $this->sorter = $this->obConfig->produktyPozycja ? 'pozycja' : 'nazwa';
        $this->sortOrder = 'asc';
        $sortowanie = (object) array();
        $sortowanie->sort = 'p.data';
        $sortowanie->order = 'desc';
        $sortowanie->specjalne = '';
        //$sortowanie->sort = 'rand()';
        //$sortowanie->order = '';
        $this->view->tryb = $tryb = 'tryb';
        $this->view->link = $link = 'index/index';

        $szukanie = new Zend_Session_Namespace('szukanieStrona');
        if ($this->_request->getParam($tryb))
            $szukanie->{$tryb} = $this->_request->getParam($tryb);
        if (!isset($szukanie->{$tryb}) || !isset($this->listaProdIlosci[$szukanie->{$tryb}]))
            $szukanie->{$tryb} = $this->view->listaProdTrybGlowna;
        $this->view->szukanie = $szukanie;
        //echo $szukanie->tryb; die();

        $user = new Zend_Session_Namespace('kontrahent');
        $produkty = new Produkty($this->module);
        $kontrahenciPolecane = new Kontrahencipolecane($this->module);

        if ($this->obConfig->glownaPromocja) {
            //$promocje = $produkty->wypiszSpecjalne('promocja', $this->lang, 0, 0, 6, 'data desc');
            $promocje = $produkty->wypiszProduktyPodzieloneStrona(false, 0, 8, $sortowanie, null, 'promocja');
            if (count($promocje) > 0) {
                if ($promocje instanceof Zend_Db_Table_Rowset)
                    $promocje = $promocje->toArray();
                if ($this->sortBy)
                    usort($promocje, array($this, 'cmp'));
            }
            $this->view->promocje = $this->produktyGaleria($promocje);
            $this->view->promocjeIlosc = $produkty->iloscProduktowSpecjalnych('promocja', $this->lang);
        }
        if ($this->obConfig->glownaPromocjaNet) {
            //$promocjeNet = $produkty->wypiszSpecjalne('promocja_net', $this->lang, 0, 0, 6, 'data desc');
            $promocjeNet = $produkty->wypiszProduktyPodzieloneStrona(false, 0, 6, $sortowanie, null, 'promocja_net');
            if (count($promocjeNet) > 0) {
                if ($promocjeNet instanceof Zend_Db_Table_Rowset)
                    $promocjeNet = $promocjeNet->toArray();
                if ($this->sortBy)
                    usort($promocjeNet, array($this, 'cmp'));
            }
            $this->view->promocjeNet = $this->produktyGaleria($promocjeNet);
            $this->view->promocjeNetIlosc = $produkty->iloscProduktowSpecjalnych('promocja_net', $this->lang);
        }
        if ($this->obConfig->glownaNowosc) {
            //$nowosci = $produkty->wypiszSpecjalne('nowosc', $this->lang, 0, 0, 6, 'data desc');
            $nowosci = $produkty->wypiszProduktyPodzieloneStrona(false, 0, 8, $sortowanie, null, 'nowosc');
            if (count($nowosci) > 0) {
                if ($nowosci instanceof Zend_Db_Table_Rowset)
                    $nowosci = $nowosci->toArray();
                if ($this->sortBy)
                    usort($nowosci, array($this, 'cmp'));
            }
            $this->view->nowosci = $this->produktyGaleria($nowosci);
            $this->view->nowosciIlosc = $produkty->iloscProduktowSpecjalnych('nowosc', $this->lang);
        }
        if ($this->obConfig->glownaPolecane) {
            //$polecane = $produkty->wypiszSpecjalne('polecane', $this->lang, 0, 0, 6, 'data desc');
            $polecane = $produkty->wypiszProduktyPodzieloneStrona(false, 0, 6, $sortowanie, null, 'polecane');
            if ($this->obConfig->klienciPolecaneProdukty) {
                if ($user->status == 'zalogowany') {
                    $kontrahenciPolecane = new Kontrahencipolecane($this->module);
                    $polecaneKlient = $kontrahenciPolecane->wypiszProdukty($user->id, 'polecane', 6, 'data desc');
                    if (count($polecaneKlient) > 0)
                        $polecane = $polecaneKlient;
                    $polecaneAll = $kontrahenciPolecane->wypiszKlientTyp($this->view->zalogowanyID, 'polecane');
                    if (count($polecaneAll) > 0)
                        $ids = @unserialize($polecaneAll['id_prod']);
                    $this->view->polecaneIlosc = //$produkty->iloscProduktowSpecjalnych('nowosc', $this->lang);
                            $produkty->wypiszProduktyPodzieloneStrona(1, 0, 99999, $sortowanie, $szukanie, null, @$ids);
                }
            }
            if (count($polecane) > 0) {
                if ($polecane instanceof Zend_Db_Table_Rowset)
                    $polecane = $polecane->toArray();
                if ($this->sortBy)
                    usort($polecane, array($this, 'cmp'));
            }
            $this->view->polecane = $this->produktyGaleria($polecane);
            $this->view->polecaneIlosc = $produkty->iloscProduktowSpecjalnych('polecane', $this->lang);
        }
        if ($this->obConfig->glownaProduktDnia) {
            //$produktdnia = $produkty->wypiszSpecjalne('produkt_dnia', $this->lang, 0, 0, 1, 'data desc');
            $produktdnia = $produkty->wypiszProduktyPodzieloneStrona(false, 0, 1, $sortowanie, null, 'produkt_dnia');
            $this->view->produktdnia = $this->produktyGaleria($produktdnia);
        }
        if (true || $this->obConfig->glownaBestseller) {
            $achiwum = new Archiwum($this->module);
            if (!$this->obConfig->wlasneBestseller)
                $bestsellery = $achiwum->bestsellery(8);
            else {
                $kontrahenciPolecane = new Kontrahencipolecane($this->module);
                $bestsellery = $kontrahenciPolecane->wypiszKlientTyp(0, 'bestseller');
                $ids = empty($bestsellery['id_prod']) ? null : @explode(',', $bestsellery['id_prod']);
                $bestsellery = $achiwum->bestsellery(8, $ids);
            }
            if (count($bestsellery) > 0) {
                if ($bestsellery instanceof Zend_Db_Table_Rowset)
                    $bestsellery = $bestsellery->toArray();
                if ($this->sortBy)
                    usort($bestsellery, array($this, 'cmp'));
            }
            $this->view->polecane = $this->produktyGaleria($bestsellery);
            $this->view->bestselleryIlosc = 0;
        }
        if ($this->obConfig->glownaWyprzedaz) {
            //$wyprzedaze = $produkty->wypiszSpecjalne('wyprzedaz', $this->lang, 0, 0, 6, 'data desc');
            $wyprzedaze = $produkty->wypiszProduktyPodzieloneStrona(false, 0, 8, $sortowanie, null, 'wyprzedaz');
            if (count($wyprzedaze) > 0) {
                if ($wyprzedaze instanceof Zend_Db_Table_Rowset)
                    $wyprzedaze = $wyprzedaze->toArray();
                if ($this->sortBy)
                    usort($wyprzedaze, array($this, 'cmp'));
            }
            $this->view->wyprzedaze = $this->produktyGaleria($wyprzedaze);
            $this->view->wyprzedazeIlosc = $produkty->iloscProduktowSpecjalnych('wyprzedaz', $this->lang);
        }
        if ($this->obConfig->glownaSpecjalne) {
            //$specjalne = $produkty->wypiszSpecjalne('specjalne', $this->lang, 0, 0, 6, 'data desc');
            $specjalne = $produkty->wypiszProduktyPodzieloneStrona(false, 0, 6, $sortowanie, null, 'specjalne');
            if (count($specjalne) > 0) {
                if ($specjalne instanceof Zend_Db_Table_Rowset)
                    $specjalne = $specjalne->toArray();
                if ($this->sortBy)
                    usort($specjalne, array($this, 'cmp'));
            }
            $this->view->specjalne = $this->produktyGaleria($specjalne);
            $this->view->specjalneIlosc = $produkty->iloscProduktowSpecjalnych('specjalne', $this->lang);
        }

        if (true && $this->obConfig->slajdy) {
            $slider = new Slider($this->_module);
            $this->view->slajdy = $slider->wypiszAktywne();
        }
        
        if (true) {
            $main = new Podstrony($this->_module);
            $main->link = 'Gazetka-blok';
            $this->view->blok2 = $main->getPodstrona();
        }
        if (true) {
            $main = new Podstrony($this->_module);
            $main->link = 'Kontakt-blok';
            $this->view->blok3 = $main->getPodstrona();
        }
        if (true) {
            $main = new Podstrony($this->_module);
            $main->link = 'Logotypy-blok';
            $this->view->logotypy = $main->getPodstrona();
        }
        if (false) {
            $main = new Podstrony($this->_module);
            $main->link = 'Banner';
            $this->view->boks = $main->getPodstrona();
            $this->view->boks->tekst = stripslashes($this->view->boks->tekst);
        }
        if (false) {
            $main = new Podstrony($this->_module);
            $main->link = 'Tekst-promocyjny';
            $this->view->tekstPromocyjny = $main->getPodstrona();
            $this->view->tekstPromocyjny->tekst = stripslashes($this->view->tekstPromocyjny->tekst);
        }
        if (false) {
            $main = new Podstrony($this->_module);
            $main->link = $this->lang('Oferta');
            $this->view->oferta = $main->getPodstrona();
            $this->view->oferta->short = stripslashes($this->view->oferta->short);
        }
        if (false) {
            $main = new Menu($this->module);
            $main->link = 'Oferta';
            $this->view->html = $main->showWybranaKategoria($this->lang == 'en' ? 2 : 1);
            $this->view->html->tekst = @stripslashes($this->view->html->tekst);
            if ($this->obConfig->podstronyGaleria) {
                $galeria = new Galeria();
                $this->view->galeria = $galeria->wyswietlGalerie($this->view->html->id, 'desc', 0, 'menu', true, false);
            }
            if ($this->obConfig->podstronyVideo) {
                $video = new Video($this->module);
                $this->view->videos = $video->wypisz(true, $this->view->html->id, 'menu', true);
            }
            $render = array('module' => 'default', 'controller' => 'podstrony');
            $this->_helper->viewRenderer->renderBySpec('show', $render);
        }
        if ($this->obConfig->aktualnosci) {
            $display = new Aktualnosci($this->module);
            $display->lang = $this->lang;
            $this->view->aktualnosci = $display->newsAll();
//            $this->view->aktualnosci = $display->wypiszAktualnosci($this->lang, 1, 0, 2, true);
//            dump($this->view->aktualnosci);die;
        }
        if ($this->obConfig->artykuly) {
            $display = new Artykuly($this->module);
            $display->lang = $this->lang;
            //$this->view->artykul = $display->wypiszAktualnoscNajnowsza();
            $this->view->artykuly = $display->wypiszAktualnosci($this->lang, 1, 0, 2, true);
            //var_dump($this->view->artykuly->toArray());
        }
        if ($this->obConfig->porady) {
            $display = new Porady($this->module);
            $display->lang = $this->lang;
            //$this->view->porada = $display->wypiszAktualnoscNajnowsza();
            $this->view->porady = $display->wypiszAktualnosci($this->lang, 1, 0, 2, true);
            //var_dump($this->view->porady->toArray());
        }
        if ($this->obConfig->realizacje) {
            $display = new Realizacje($this->_module);
            $display->lang = $this->lang;
            //$this->view->realizacja = $display->wypiszAktualnoscNajnowsza();
            $this->view->realizacje = $display->wypiszAktualnosci($this->lang, 1, 0, 2, true);
            //var_dump($this->view->realizacje->toArray());
        }
        if ($this->obConfig->video) {
            $display = new Video($this->_module);
            $display->lang = $this->lang;
            $this->view->video = $display->wypiszNajnowsze();
            //$this->view->videos = $display->wypisz();
        }
        if (false) {
            $katprod = new Katprod($this->_module);
            $prods = $katprod->wypiszAllFromKateg(0);
            $this->view->partnerzy = $this->produktyGaleria($prods);
            //var_dump($this->view->partnerzy);
        }

        if (false && $this->obConfig->pokazKategorie) {
            $kategorie = new Kategorie($this->_module);
            $this->view->all = $kategorie->wypiszDzieci(0);
            //var_dump($this->view->all);
            $this->render('kategoriedzieci');
            $render = array('module' => 'default', 'controller' => 'oferta');
            //$this->_helper->viewRenderer->renderBySpec('kategoriedzieci', $render);
        }

        if ($this->obConfig->ankiety) {
            $objAnkieta = new Ankieta($this->_module);
            $objAnkieta->cachename = 'ankieta_' . $this->lang;
            $this->view->ankieta = $objAnkieta->wypiszWidocznyCache(0, 0);
            //echo 'ankieta';//var_dump($this->view->ankieta);
            $odps = explode('-|-', $this->view->ankieta['odp']);
            if (count($odps) > 0)
                foreach ($odps as $odp) {
                    $vals = explode('*|*', $odp);
                    @$ankietaOdps[$vals[1]]['id'] = @$vals[2];
                    @$ankietaOdps[$vals[1]]['nazwa'] = @$vals[0];
                }
            ksort($ankietaOdps);
            $this->view->ankietaOdps = $ankietaOdps;

            $objAnkieta->cachename = 'ankieta_ustawienia_' . $this->lang;
            $ankietau = $objAnkieta->wypiszUstawieniaCache(0, 0);
            $this->view->ankietaUs = $ankietau;
            //var_dump($this->view->ankietaUs->toArray());
        }

        $lvl = -1;
        $i = 0;
        $x = '';
        $open = 0;
        if (false && count($this->view->kategorie) > 0)
            foreach ($this->view->kategorie as $kategoria) {
                $x.= '<option value="' . $kategoria['id'] . '">' . $kategoria['nazwa'] . '</option><br/>';
                if (!$open)
                    if (isset($this->view->kategorie[$i + 1]) && @$this->view->kategorie[$i + 1]['rodzic'] != $kategoria['rodzic']) {
                        $x.= '<optgroup label="' . $kategoria['id'] . '"> r1_' . @$this->view->kategorie[$i + 1]['rodzic'] . ' != r0_' . $kategoria['rodzic'] . '<br/>';
                        $open = true;
                    }

                if ($open && $kategoria['rodzic'] == $lvl && @$this->view->kategorie[$i + 1]['rodzic'] != $lvl) {
                    $x.= '</optgroup> r1_' . @$this->view->kategorie[$i + 1]['rodzic'] . ' - l_' . $lvl . '<br/>';
                    $open = false;
                }
                $lvl = $kategoria['rodzic'];
                $i++;
            }
        //echo str_replace('&lt;br/&gt;', '<br/>', htmlspecialchars($x));
        //die();
    }

    public function sitemapAction() {
        $this->_helper->viewRenderer->setNoRender();

        $eol = PHP_EOL;
        $tab = "\t";
        $tab2 = $tab . $tab;

        $staticLink = null;
        if ($this->obConfig->klienci)
            $staticLink = array
                (
                array('nazwa' => 'Twoje konto', 'link' => 'kontrahent/edycja/'),
                array('nazwa' => 'Zaloguj się', 'link' => 'kontrahent/logowanie/'),
                array('nazwa' => 'Rejestracja', 'link' => 'kontrahent/index/'),
                array('nazwa' => 'Przypomnienie hasła', 'link' => 'kontrahent/przypomnienie/')
            );
        if ($this->obConfig->sklepOnline)
            $staticLink[] = array('nazwa' => 'Koszyk', 'link' => 'Koszyk');
        if ($this->obConfig->sklepOnline)
            $staticLink[] = array('nazwa' => 'Zamówienia', 'link' => 'kontrahent/zamowienia/');
        if ($this->obConfig->porownywarka)
            $staticLink[] = array('nazwa' => 'Porównywarka', 'link' => 'kontrahent/porownywarka/');
        if ($this->obConfig->asortyment)
            $staticLink[] = array('nazwa' => 'Wyniki wyszukiwania', 'link' => 'Wyniki-wyszukiwania');

        $sitemap = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
            xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">' . $eol;

        $sitemap .=
                '<url>' . $eol . '
            ' . $tab . '<loc>http://' . $_SERVER['HTTP_HOST'] . '/</loc>' . $eol . '
            ' . $tab . '<lastmod>' . date('Y-m-d') . '</lastmod>' . $eol . '
            ' . $tab . '<changefreq>always</changefreq>' . $eol . '
            ' . $tab . '<priority>0.8</priority>' . $eol . '
            </url>' . $eol;

        if (true) {
            if (count($staticLink) > 0)
                foreach ($staticLink as $link) {
                    $sitemap .=
                            '<url>
				<loc>http://' . $_SERVER['HTTP_HOST'] . '/' . str_replace('&', '&amp;', $link['link']) . '</loc>
				<lastmod>' . date('Y-m-d') . '</lastmod>
				<changefreq>always</changefreq>
				<priority>0.8</priority>
				</url>';
                }

            if ($this->obConfig->podstrony) {
                $podstrony = new Podstrony($this->module);
                $arr = $podstrony->linkiSitemap();
                if (isset($arr[0]['id']))
                    foreach ($arr as $link) {
                        $sitemap .=
                                '<url>
					<loc>http://' . $_SERVER['HTTP_HOST'] . '/' . str_replace('&', '&amp;', $link['link']) . '</loc>
					<lastmod>' . date('Y-m-d') . '</lastmod>
					<changefreq>always</changefreq>
					<priority>0.8</priority>
					</url>';
                    }
            }

            if ($this->obConfig->aktualnosci) {
                $oAktualnosci = new Aktualnosci($this->module);
                $aktualnosci = $oAktualnosci->wypisz();
                if (count($aktualnosci) > 0)
                    foreach ($aktualnosci as $link) {
                        $sitemap .=
                                '<url>
					<loc>http://' . $_SERVER['HTTP_HOST'] . '/Aktualnosc/nr/' . str_replace('&', '&amp;', $link['id']) . '</loc>
					<lastmod>' . date('Y-m-d') . '</lastmod>
					<changefreq>always</changefreq>
					<priority>0.8</priority>
					</url>';
                    }
            }

            if ($this->obConfig->porady) {
                $display = new Porady($this->module);
                $porady = $display->wypiszAktualnosci($this->lang);
                if (count($porady) > 0)
                    foreach ($porady as $link) {
                        $sitemap .=
                                '<url>
					<loc>http://' . $_SERVER['HTTP_HOST'] . '/Porada/nr/' . str_replace('&', '&amp;', $link['id']) . '</loc>
					<lastmod>' . date('Y-m-d') . '</lastmod>
					<changefreq>always</changefreq>
					<priority>0.8</priority>
					</url>';
                    }
            }

            if ($this->obConfig->kategorie) {
                $kategorie = new Kategorie($this->module);
                $arr = $kategorie->linkiSitemap();
                if (isset($arr[0]['id']))
                    foreach ($arr as $link) {
                        $sitemap .=
                                '<url>
					<loc>http://' . $_SERVER['HTTP_HOST'] . '/' . str_replace('&', '&amp;', $link['link']) . '</loc>
					<lastmod>' . date('Y-m-d') . '</lastmod>
					<changefreq>always</changefreq>
					<priority>0.8</priority>
					</url>';
                    }
            }

            if ($this->obConfig->asortyment) {
                $produkty = new Produkty($this->module);
                $arr = $produkty->linkiSitemap();
                if (isset($arr[0]['id'])) {
                    for ($i = 0; $i < count($arr); $i++) {
                        $ids[] = $arr[$i]['id'];
                    }
                    if (true && @count($ids) >= 1) {
                        $galmain = new Galeria($this->module);
                        $zdjecia = $galmain->getZdjeciaForIds($ids);
                    }
                    //var_dump($zdjecia);die();
                    foreach ($arr as $link) {
                        $sitemap .=
                                '<url>
						<loc>http://' . $_SERVER['HTTP_HOST'] . '/' . str_replace('&', '&amp;', $link['link']) . '</loc>';
                        if (!empty($link['img']))
                            $sitemap .= '
						<image:image>
							<image:loc>http://' . $_SERVER['HTTP_HOST'] . '/public/admin/zdjecia/' . $link['img'] . '</image:loc>
							<image:title>' . $link['nazwa'] . '</image:title>
							<image:caption>' . $link['img_opis'] . '</image:caption>
						</image:image>';
                        if (isset($zdjecia[$link['id']][0]))
                        //if(@count($zdjecia[$link['id']][0]) > 1)
                            foreach ($zdjecia[$link['id']][0] as $z => $zdj)
                            //for($z=1;$z<count($zdjecia[$link['id']][0]);$z++)
                                if ($z > 0)
                                    $sitemap .= '
						<image:image>
							<image:loc>http://' . $_SERVER['HTTP_HOST'] . '/public/admin/zdjecia/' . $zdj['img'] . '</image:loc>
							<image:title>' . $link['nazwa'] . '</image:title>
							<image:caption>' . $zdj['nazwa'] . '</image:caption>
						</image:image>';
                        $sitemap .= '
						<lastmod>' . date('Y-m-d') . '</lastmod>
						<changefreq>always</changefreq>
						<priority>0.8</priority>
						</url>';
                    }
                }
            }
        }

        $sitemap .= '</urlset>';

        $xml = new DOMDocument('1.0', 'utf-8');
        $xml->loadXML($sitemap);
        $output = $xml->saveXML();

        Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();

        $this->_response->setHeader('Content-Type', 'text/xml; charset=utf-8')->setBody($output);
    }

    public function error404Action() {
        $this->getResponse()->setHttpResponseCode(404)->setRawHeader('HTTP/1.1 404 Not Found');
    }

}

?>