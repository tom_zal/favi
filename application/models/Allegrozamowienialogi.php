<?php
class Allegrozamowienialogi extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
    function wypisz()
	{
        $result = $this->fetchAll();
        return $result;
    }
    function dodaj($dane)
	{
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }
    function edytuj($dane, $id)
	{
        $where = 'id = "' . $id . '"';
        $this->update($dane, $where);
    }
    function pojedyncza($id)
	{
        $where = 'id = "'.$id.'"';
        $print = $this->fetchRow($where);
        return $print;
    }
	public function findByIdDeal($id_deal)
	{
		$where = 'id_deal = "'.$id_deal.'"';
		$order = 'data ASC';
        $r = $this->fetchAll($where,$order);
		if(!empty($r)) $r = $r->toArray();
        return $r;
	}
	public function findByIdZam($id_zam)
	{
		$where = 'id_zamowienia = "'.$id_zam.'"';
		$order = 'data ASC';
        $r = $this->fetchAll($where,$order);
		if(!empty($r)) $r = $r->toArray();
        return $r;
	}
	function findBy($id_zamowienia = null, $login = null, $id_transakcji = null, $id_deal = null)
	{
		if(empty($login) && empty($id_transakcji) && empty($id_deal))
		{
			$where = 'id_zamowienia = "'.$id_zamowienia.'"';
		} else {
			if(!empty($id_transakcji))
			{
				$where = 'id_transakcji = "'.$id_transakcji.'"';
			} else {
				$where = 'id_deal = "'.$id_deal.'" AND login = "'.$login.'"';
			}
		}	
			$r = $this->fetchAll($where);
			if(!empty($r)) $r = $r->toArray();
			return $r;
		
    }
 
}
?>