<?php

class Paczkomaty extends Zend_Db_Table
{
	public $id, $idP;
	
	public $url = 'https://api.paczkomaty.pl', $view, $configinpost = array();
    public $paczka, $nadawca = array(), $odbiorca = array(), $uslugi = array(), $autolabels = 0, $selfSend = 0, $packsData = array();
	public $dir = './../application/models/inpost/data';
    public $wybrany = 0, $miasto = 0;

    public function __construct($module = 'admin')
	{
        parent::__construct();
        $this->common = new Common(false, $module);
        $this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
        $this->lang = $this->common->getJezyk($module);
        try
		{
            $this->view = Zend_Layout::getMvcInstance()->getView();
        }
		catch(Zend_Exception $e)
		{
            die('Problem z pobraniem widoku');
        }
        global $inpost_data_dir, $inpost_api_url;
        $inpost_data_dir = $this->dir;
        $inpost_api_url = $this->url;
        require 'inpost/inpost.php';
    }
	
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujpackcode($dane, $packcode)
	{
		$where = 'packcode = "'.$packcode.'"';
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$result = $this->fetchAll();
		return $result;
	}
	function wypiszZam($id)
	{
		$result = $this->fetchAll('id_zam = '.$id);
		return $result;
	}
	function wypiszRaport($data)
	{
		$result = $this->fetchAll('data >= "'.$data.' 00:00:00" and data <= "'.$data.' 23:59:59"');
		return $result;
	}
	function wypiszJeden()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function usunJeden()
	{
		$result = $this->delete('id = '.$this->id);
	}
        
	function nadawca($dane)
	{
		if(!empty($dane))
		{
			if(isset($_POST['nadawca']))
			{
				$array = $_POST['nadawca'];
			}
			else
			$array = array
			(
				'name'=>$dane['name'],
				'surName'=>$dane['surName'],
				'phoneNum'=>$dane['phoneNum'],
				'street'=>$dane['street'],
				'buildingNo'=>$dane['buildingNo'],
				'flatNo'=>$dane['flatNo'],
				'town'=>$dane['town'],
				'zipCode'=>$dane['zipCode'],
				'province'=>$dane['province']
			);
			$this->view->nadawca = $array;
		}
	}
	
	function odbiorca($dane)
	{
		$array = array();
		foreach($dane as $d)
		{
			if($d['dostawca'] == 'paczkomaty')
			{
				$inny = $d['inny_adres_dostawy'];
				if(isset($_POST['inpostodbiorca'][$d['dostawca']][$d['id']]))
				{
					$array[$d['dostawca']][$d['id']] = $_POST['inpostodbiorca'][$d['dostawca']][$d['id']];
				}
				else
				{
					$array[$d['dostawca']][$d['id']] = array
					(
						'email'=> $d['email'],
						'phoneNum'=> !$inny ? $d['komorka'] : $d['komorka_korespondencja'],
						'name' => !$inny ? $d['imie'] : $d['imie_korespondencja'],
						'surName' => !$inny ? $d['nazwisko'] : $d['nazwisko_korespondencja'],
						'street' => !$inny ? $d['ulica'] : $d['ulica_korespondencja'],
						'companyName' => !$inny ? $d['nazwa_firmy'] : $d['nazwa_firmy_korespondencja'],
						'buildingNo' => !$inny ? $d['nr'] : $d['nr_korespondencja'],
						'flatNo' => !$inny ? $d['mieszkanie'] : $d['mieszkanie_korespondencja'],
						'town' => !$inny ? $d['miasto'] : $d['miasto_korespondencja'],
						'zipCode' => !$inny ? $d['kod'] : $d['kod_korespondencja'],
						'province' => !$inny ? $d['wojew'] : $d['wojew_korespondencja']
					);
					if(empty($array[$d['dostawca']][$d['id']]['phoneNum']))
					$array[$d['dostawca']][$d['id']]['phoneNum'] = !$inny ? $d['telefon'] : $d['telefon_korespondencja'];
				}
				
				if(@intval($array[$d['dostawca']][$d['id']]['flatNo']) == 0) $array[$d['dostawca']][$d['id']]['flatNo'] = '';
				
				$wysylka = $d['wysylka'];
				$allegro = (strpos($wysylka, 'Allegro') !== false);
				$polecony = (strpos($wysylka, 'Polecony') !== false);
				$czyPobranie = (strpos($wysylka, 'pobran') !== false) || (strpos($wysylka, 'Płatność przy odbiorze') !== false);
				$przedpłata = (strpos($wysylka, 'przedpł') !== false);
				$odbiorPunkt = (strpos($wysylka, 'Odbiór w punkcie') !== false);
				$czyPobranie = $czyPobranie || ($odbiorPunkt && !$przedpłata);
				$razem = $d['wartosc_zamowienia'] + $d['koszt_dostawy'] - $d['rabat_wartosc'];
				
				if(isset($_POST['inpostconfig'][$d['dostawca']][$d['id']]))
				{
					$this->configinpost[$d['dostawca']][$d['id']] = $_POST['inpostconfig'][$d['dostawca']][$d['id']];
				}
				else
				{
					$this->configinpost[$d['dostawca']][$d['id']]['onDeliveryAmount'] = $czyPobranie ? $razem : 0.00;
					$this->configinpost[$d['dostawca']][$d['id']]['insuranceAmount'] = $polecony ? 0 : $razem;
					$this->configinpost[$d['dostawca']][$d['id']]['Content'] = !empty($d['dok_DoDokNrPelny'])? 'Dokument: '.$d['dok_DoDokNrPelny']: '';;
					$this->configinpost[$d['dostawca']][$d['id']]['boxMachineName'] = !empty($d['paczkomat'])? $d['paczkomat'] : '';
					$this->configinpost[$d['dostawca']][$d['id']]['alternativeBoxMachineName'] = '';					
					$this->configinpost[$d['dostawca']][$d['id']]['senderBoxMachineName'] = $this->config['senderBoxMachineName'];
					$this->configinpost[$d['dostawca']][$d['id']]['autoLabels'] = $this->config['autoLabels'];
					$this->configinpost[$d['dostawca']][$d['id']]['selfSend'] = $this->config['selfSend'];
				}
				
				//modyfikacja na paczkomat wybierany w koszyku
				$this->configinpost[$d['dostawca']][$d['id']]['boxMachineNameList'] = $this->inpost_get_machine_list(!empty($d['paczkomat_miasto']) ? $d['paczkomat_miasto'] : $array[$d['dostawca']][$d['id']]['town'], $czyPobranie ? 't' : '');
				
				$this->configinpost[$d['dostawca']][$d['id']]['senderBoxMachineNameList'] = $this->paczkomatnadawcy;
				if(empty($d['paczkomat_miasto']))
				{
					$this->configinpost[$d['dostawca']][$d['id']]['alternativeBoxMachineNameList'] = 
						$this->inpost_find_nearest_machines($array[$d['dostawca']][$d['id']]['zipCode'], $czyPobranie ? 't' : '');
				}
				else
				{
					$this->configinpost[$d['dostawca']][$d['id']]['alternativeBoxMachineNameList'] = '';
				}
			}
		}
		$this->view->odbiorca = $array;
	}
	
	function inpost_get_machine_list($town = "", $paymentavailable = "")
	{
		//echo 'town='.$town.'<br>';
		//if(empty($town)) return null;
		if(!isset($_SESSION['inpost_machine_list'][$town][$paymentavailable]))
		{
			$inpost_machine_list = inpost_get_machine_list($town, $paymentavailable);
			$_SESSION['inpost_machine_list'][$town][$paymentavailable] = $inpost_machine_list;
			//var_dump($_SESSION['inpost_machine_list']);die();
		}
		return $_SESSION['inpost_machine_list'][$town][$paymentavailable];
		
		if(false)
		foreach($inpost_machine_list as $machine)
		{
			foreach($machine as $co => $val)
			{
				$len = strlen($val);
				if($len > @$lens[$co])
				{
					$lens[$co] = $len;
					$vals[$co] = $val;
				}
			}
		}
		//var_dump($inpost_machine_list);var_dump($vals);die();
	}
	function inpost_find_nearest_machines($postcode = "", $paymentavailable = "")
	{
		//if(empty($postcode)) return null;
		if(!isset($_SESSION['inpost_nearest_machines'][$postcode][$paymentavailable]))
		{
			$inpost_nearest_machines = inpost_find_nearest_machines($postcode, $paymentavailable);
			$_SESSION['inpost_nearest_machines'][$postcode][$paymentavailable] = $inpost_nearest_machines;
			//var_dump($_SESSION['inpost_nearest_machines']);die();
		}
		return $_SESSION['inpost_nearest_machines'][$postcode][$paymentavailable];
	}
	
	function getDefaultData($dane) {
		$paczkomatydane = new Paczkomatydane();
		$this->config = $ustawienia = $paczkomatydane->wypisz();
		$this->paczkomatnadawcy  = $this->inpost_get_machine_list($this->config['town'], '');

		$this->nadawca($ustawienia);
		$this->odbiorca($dane);
		$this->view->packType = $paczkomatydane->packType;
		$this->view->configinpost = $this->configinpost;
		//var_dump($this->configinpost);die();
	}
	
	function setData($dane) {
		if(isset($dane['inpostodbiorca']) && isset($dane['inpostconfig'])) {
			$this->odbiorca = $dane['inpostodbiorca']['paczkomaty'][$this->idP];
			$this->uslugi = $dane['inpostconfig']['paczkomaty'][$this->idP];
			$this->setDataApi();
		}
	}
	
	function setDataApi() {
		$paczkaID = $this->idP;
		$packsData['adreseeEmail'] = $this->odbiorca['email'];
		$packsData['senderEmail'] = $this->config['login'];
		$packsData['phoneNum'] = $this->odbiorca['phoneNum'];
		$packsData['boxMachineName'] = $this->uslugi['boxMachineName'];
		$packsData['alternativeBoxMachineName'] = $this->uslugi['alternativeBoxMachineName'];
		$packsData['customerDelivering'] = @intval($this->uslugi['customerDelivering']);
		$packsData['senderBoxMachineName'] = $this->uslugi['senderBoxMachineName'];
		$packsData['packType'] = $this->uslugi['packType'];
		$packsData['insuranceAmount'] = floatval(str_replace(',','.',$this->uslugi['insuranceAmount']));
		$packsData['onDeliveryAmount'] = floatval(str_replace(',','.',$this->uslugi['onDeliveryAmount']));
		$packsData['customerRef'] = $this->uslugi['Content'];
		$this->nadawca['email'] = $this->config['login'];
		$packsData['senderAddress'] = $this->nadawca;
		if(empty($packsData['boxMachineName']))
		$packsData['receiverAddress'] = $this->odbiorca;
		if(@strpos($this->zamowienie['wysylka'], 'Allegro') !== false) // && @!empty($this->zamowienie['deal_transaction_id']))
		{
			$packsData['insuranceAmount'] = "";
			$allegrokonta = new Allegrokonta();
			$konto = $allegrokonta->getLogin($this->zamowienie['sprzedawca']);
			$packsData['allegro']['userId'] = $konto['uid'];//$this->zamowienie['id_buyer'];
			$packsData['allegro']['transactionId'] = $this->zamowienie['deal_transaction_id'];
		}
		//var_dump($packsData);die();
		$this->autoLabels = @intval($this->config['autoLabels']);
		$this->selfSend = @intval($this->config['selfSend']);
		$this->packsData = array($paczkaID => $packsData);
	}
	
	function addPackage() {
		if(!empty($this->packsData)) {
			$response = inpost_send_packs($this->config['login'], $this->config['haslo'], $this->packsData, $this->autoLabels, $this->selfSend);
			return $this->getResponseApi($response);
		}
	} 
	function getResponseApi($response) {
		$error = '';
		if(@count($response > 0)) {
			foreach($response as $resPaczkaID => $result) {
				if(@!empty($result['error_key']) || @!empty($result['error_message'])) {
					return $this->getError($result);
				}
				else {
					return array(
						'guid'=>@strval($result['customerdeliveringcode']),
						'numerPrzesylka' => $result['packcode'],
						'error' => NULL
					);
				}
			}
		}
	} 
	function getError($error) {
		if(!empty($error['error_message'])) {
			return array(
				'error'=>$error['error_message']
			);
		}
	}
	function sortCode($dane) {
		$array = array();
		foreach($dane as $d) {
			if($d['numerPrzesylka']) $array[] = $d['numerPrzesylka'];
		}
		return $array;
	}
	
	function getConfig() {
		$paczkomat = new Paczkomatydane();
		$this->config = $paczkomat->wypisz();
	}
   
	function getStickers($packcode) {
		$this->getConfig();
		$response = inpost_get_stickers($this->config['login'], $this->config['haslo'], $packcode, 'A6P');
		return $this->getResponsePdfApi($response, 'etykieta');
	}
	function getPrintout($packcode) {
		$this->getConfig();
		$response = inpost_get_confirm_printout($this->config['login'], $this->config['haslo'], $packcode, 0);
		return $this->getResponsePdfApi($response, 'list');
	}
	
	function getResponsePdfApi($response, $filename ='list') {
		if(isset($response['error']) && isset($response['error']['message'])) {
			if(@$_GET['xxx']) var_dump($response); //die;
			return array('error'=>$response['error']['message']);
		} else {
			$fileName = $filename.'_inpost_'.$this->symbol.'.pdf';
			$file = fopen('./admin/pdf/'.$fileName, 'w');
			fwrite($file, $response);
			fclose($file);
			return array('wydruk_'.$filename.'_global' => $fileName);
		}
	}
	
	function setDataPaczkomat() {
		$paczkomaty = new Zend_Session_Namespace('paczkomaty');
		if(!isset($paczkomaty->miasto)) {
			$paczkomaty->miasto = '';
		}
		if(!isset($paczkomaty->paczkomat)) {
			$paczkomaty->paczkomat = '';
		}
		
		if(isset($this->_get['miasto'])) {
			$paczkomaty->miasto = $this->_get['miasto'];
		}
		if(isset($this->_get['paczkomat'])) {
			$paczkomaty->paczkomat = $this->_get['paczkomat'];
		}
		$this->view->paczkomat = $paczkomaty->paczkomat;
		$this->view->paczkomatmiasto = $paczkomaty->miasto;
		$this->view->paczkomaty = ($paczkomaty->miasto != '') ? $this->inpost_get_machine_list($paczkomaty->miasto , '') : '';
	}
	
	function inpost_get_towns()
	{
		if(!isset($_SESSION['inpost_get_towns']))
		{
			$_SESSION['inpost_get_towns'] = inpost_get_towns();
			//var_dump($_SESSION['inpost_get_towns']);die();
		}
		return $_SESSION['inpost_get_towns'];
	}
	
	function getToCart()
	{
		if($this->view->obConfig->paczkomaty)
		{
			$this->view->towns = $this->inpost_get_towns();
			$this->setDataPaczkomat();
		}
	}
}
?>