<?php
class Confmail extends Zend_Db_Table
{
	public $From, $FromName, $Host, $Mailer, $Username, $Password;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

	function updateData($array, $id)
	{
		$where = 'id = '.$id;
		$this->update($array, $where);		
	}
	
	function showData()
	{
		$result = $this->fetchAll();		
		return $result;
	}	
}
?>