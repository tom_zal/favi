<?php
class Kontrahencigrupy extends Zend_Db_Table
{
	public $link, $id;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$this->update($dane, 'id = '.$this->id);
	}
	function wypisz()
	{
		$sql = $this->select()->where('1')->order('nazwa');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszIle()
	{
		$select = $this->db->select()->from(array('kg' => 'Kontrahencigrupy'), array('*'))
			->joinleft(array('k' => 'Kontrahenci'), 'kg.id = k.typ', array('count(k.id) as ile'))
			->where('1')->group('kg.id')->order(array('kg.nazwa'));
		//echo $select;die();
        $stmt = $select->query();
		$result = $stmt->fetchAll();
        return $result;
	}
	
	function wypiszNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa = "'.$nazwa.'"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszWhereNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa like "%'.$nazwa.'%"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	
	function wypiszForAll($pole = 'id')
	{
		$result = $this->fetchAll();
		for($i = 0; $i < count($result); $i++)
		{
			$grupy[$result[$i][$pole]] = $result[$i][$pole == 'id' ? 'nazwa' : 'id'];
		}		
		return @$grupy;
	}
	
	function usun()
	{
		$result = $this->delete('id = '.$this->id);
	}
	function wypiszPojedynczego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
}
?>