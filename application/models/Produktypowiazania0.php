<?php
class Produktypowiazania extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
	function getPowiazaniaForExport($ids)
	{
		if(empty($ids) || count($ids) == 0) return null;
        $select = $this->db->select()->from(array('pp' => 'Produktypowiazania'), array('id_prod', 
			new Zend_Db_Expr('GROUP_CONCAT(DISTINCT powiazany ORDER BY powiazany ASC SEPARATOR "|") as powiaz')))
			->where('id_prod in('.implode(',',$ids).')')->group('id_prod')->order(array('id_prod asc'));
		//echo $select;die();
		$result = $this->db->fetchAll($select);
        for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i]['id_prod']] = $result[$i]['powiaz'];
		}
		return @$results;
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function wypiszAll()
	{
		$result = $this->fetchAll();
		return $result;
	}
	function wypiszID()
	{
		$sql = $this->select()->where('id = '.$this->id);
		$result = $this->fetchRow($sql);
		return $result;
	}
	function wypiszProd($id)
	{
		$sql = $this->select()->where('id_prod = '.$id);
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszProdukt($id)
	{
		$select = $this->db->select()->from(array('p' => 'Produkty'), array('nazwa'))
			->join(array('pp' => 'Produktypowiazania'), 'p.id = pp.powiazany', array('id'))
			->where('pp.id_prod = '.$id)->order('nazwa asc')->limit(9999);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszProdukty($id, $ile = 3)
	{
		$whereGal = 'p.id = g.wlasciciel and g.typ = "oferta" and g.glowne = "T"';
		$whereGal.= ' and g.lang = "'.$this->lang.'" and g.wyswietl = "1"';
		$select = $this->db->select()->from(array('p' => 'Produkty'), array('*'))
			->join(array('pp' => 'Produktypowiazania'), 'p.id = pp.powiazany', array(''))
			->joinleft(array('g' => 'Galeria'), $whereGal, array('img', 'img_opis' => 'nazwa'))
			->where('pp.id_prod = '.$id)->order('p.nazwa asc')->limit($ile);
		//echo $select;		
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function usun()
	{
		$result = $this->delete('id = '.$this->id);
	}
	function usunProdukt($id)
	{
		$result = $this->delete('id_prod = '.$id);	
	}
	function usunProduktyIds($ids)
	{
		$result = $this->delete('id_prod in ('.$ids.')');
	}
}
?>