<?php
class Opekdane extends Zend_Db_Table
{
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->module = $module;
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
	function updateData($array)
	{
		$where = 'id = 1';
		$this->update($array, $where);
	}
	
	function showData()
	{
		$result = $this->fetchAll();		
		return $result;
	}
	
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujAll($dane)
	{
		$where = '1';
		$this->update($dane, $where);
	}
	function edytujID($format, $dane)
	{
		$where = 'format = "'.$format.'"';
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$result = $this->select()->where('id = 1');
		return $this->fetchRow($result)->toArray();
	}
	function wypiszJeden()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function wypiszOne($format = 'pudelko')
	{
		$sql = $this->select()->where('format = "'.$format.'"');
		$result = $this->fetchRow($sql);
		if($result == null)
		{
			$dane['format'] = $format;
			$this->dodaj($dane);
			$result = $this->fetchRow($sql);
		}
		return $result->toArray();
	}
}
?>