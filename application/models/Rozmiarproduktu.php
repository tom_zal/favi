<?php
class Rozmiarproduktu extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function wypiszAll()
	{
		$sql = $this->select()->order(array('nazwa', 'wkladka'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypisz($typ = 1)
	{
		$select = $this->db->select()->from(array('rp' => 'Rozmiarproduktu'), array('*'))
			->join(array('r' => 'Rozmiary'), 'r.id = rp.id_koloru', array('nazwa', 'wkladka'))
			->where('rp.id_produktu = '.$this->id.' and r.typ = '.$typ)
			->group('r.id')->order(array('r.nazwa', 'r.wkladka'));
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszForIds($ids = null)
	{
		if(!$this->common->isArray($ids, true)) return null;
		$select = $this->db->select()->from(array('p' => 'Produkty'), array(''))
			->join(array('rp' => 'Rozmiarproduktu'), 'p.id = rp.id_produktu', array('*'))
			->join(array('r' => 'Rozmiary'), 'r.id = rp.id_koloru', array('nazwa', 'wkladka'))
			->where('rp.id_produktu in ('.implode(',', $ids).') and r.typ = p.rozmiarowka')
			->order(array('r.nazwa', 'r.wkladka'));
		//echo $select;
		$result = $this->db->fetchAll($select);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i]['id_produktu']][] = $result[$i];
		}
		return @$results;
	}
	function wypiszAukcje($null = false)
	{
		$where = '1';
		if($null)
		$where = 'a.wystawiono is not null and (DATE_ADD(a.wystawiono,INTERVAL a.na_ile DAY)-NOW()) > 0';
		$select = $this->db->select()->from(array('rp' => 'Rozmiarproduktu'), 
			array('id as rp.id', 'id_produktu', 'dostepnosc', 'dostepny'))
			->join(array('r' => 'Rozmiary'), 'r.id = rp.id_koloru', array('nazwa', 'wkladka'))
			->joinleft(array('a' => 'Allegroaukcje'), 'a.id_rozm = rp.id', 
					array('id as a.id', 'nr', 'sztuk_pozostalo', 'na_ile', 'sztuk_wystawiono', 'wystawiono'))
			->where('rp.id_produktu = '.$this->id.' and '.$where)->order(array('r.nazwa', 'r.wkladka'));
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszCena($typ = 1, $cena = 'cena_brutto', $co = 'min')
	{
		$select = $this->db->select()
			->from(array('rp' => 'Rozmiarproduktu'), array($co.'(rp.'.$cena.') as cena'))
			->join(array('r' => 'Rozmiary'), 'r.id = rp.id_koloru', array('nazwa', 'wkladka'))
			->where('rp.id_produktu = '.$this->id.' and r.typ = '.$typ)->group('rp.id_produktu');
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszCeny($ids = null, $hurt = false, $co = 'min')
	{
		if(!$this->common->isArray($ids, true)) return null;
		if(!$hurt) $cena = 'if(p.promocja, rp.cena_promocji_b, rp.cena_brutto)';
		else $cena = 'if(p.promocja, rp.cena_promocji_b_hurt, rp.cena_brutto_hurt)';
		$ceny = $this->common->getCeny(true);
		//$ceny[] = $cena.' as brutto';
		$select = $this->db->select()->from(array('p' => 'Produkty'), array('id as id_prod'))
			->join(array('rp' => 'Rozmiarproduktu'), 'p.id = rp.id_produktu', $ceny)
			->join(array('r' => 'Rozmiary'), 'r.id = rp.id_koloru', array(''))
			->where('rp.id_produktu in ('.implode(',', $ids).') and r.typ = p.rozmiarowka')
			->order(array('p.id asc', $cena.' '.($co == 'min' ? 'desc' : 'asc')));
		//echo $select;
		$result = $this->db->fetchAll($select);
		for ($i = 0; $i < count($result); $i++)
		{
			$prodID = $result[$i]['id_prod'];
			unset($result[$i]['id_prod']);
			$results[$prodID] = $result[$i];
		}
		return @$results;
	}
	function wypiszSumaDostepnosc($typ = 1)
	{
		$select = $this->db->select()
			->from(array('rp' => 'Rozmiarproduktu'), array('sum(rp.dostepnosc) as sumadost'))
			->join(array('r' => 'Rozmiary'), 'r.id = rp.id_koloru', array('nazwa', 'wkladka'))
			->where('rp.id_produktu = '.$this->id.' and r.typ = '.$typ)->group('rp.id_produktu');
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszJeden()
	{
		$select = $this->select()->where('id = '.$this->id);
		$result = $this->fetchAll($select);
		return $result;
	}
	function wypiszJedenNazwa()
	{
		$select = $this->select()->where('id = '.$this->id);
		$result = $this->fetchAll($select);
		if(count($result) == 0) return array(0=>array('nazwa'=>'','wkladka'=>''));
		$select = $this->db->select()->
			from(array('r' => 'Rozmiary'))->where('id = '.$result[0]['id_koloru'].'');
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function getNazwa($id)
	{
		$select = $this->db->select()->from(array('rp' => 'Rozmiarproduktu'), array('id'))
			->join(array('r' => 'Rozmiary'), 'r.id = rp.id_koloru', array('nazwa', 'wkladka'))
			->where('rp.id = '.$id);
			//echo $select;
		$result = $this->db->fetchAll($select);
		if(count($result) == 0) return '';
		$rozmiarNazwa = $result[0]['nazwa'];
		if(isset($result[0]['wkladka']) && !empty($result[0]['wkladka']))
		$rozmiarNazwa .= ' - wkł. '.$result[0]['wkladka'];
		return $rozmiarNazwa;
	}
	function szukaj($aukcja, $typ = 'aukcje')
	{
		$select = $this->select()->where($typ.' like "%'.$aukcja.'%"');
		$result = $this->fetchAll($select);
		return $result;
	}
	function szukajRozmiar($nazwa, $wkladka)
	{
		$select = $this->db->select()->from(array('rp' => 'Rozmiarproduktu'), 
			array('id', 'id_produktu', 'id_koloru', 'dostepnosc'))
			->join(array('r' => 'Rozmiary'), 'r.id = rp.id_koloru', array('nazwa', 'wkladka'))
			->where('rp.id_produktu = '.$this->id.' and r.nazwa = "'.$nazwa.'" and r.wkladka = "'.$wkladka.'"')
			->group('r.id')->order(array('r.nazwa', 'r.wkladka'));
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	
	function ileAukcjiDlaKategorii($id, $typ = 'aukcje')
	{
		$select = $this->db->select()->from(array('p' => 'Produkty'), array(''))
			->join(array('rp' => 'Rozmiarproduktu'), 'p.id = rp.id_produktu', array('aukcje', 'ebay'))
			->join(array('r' => 'Rozmiary'), 'r.id = rp.id_koloru', array(''))
			->join(array('kp' => 'Katprod'), 'p.id = kp.id_prod', array(''))
			->where('kp.id_kat = '.$id);
		//echo $select;
		$result = $this->db->fetchAll($select);
		foreach($result as $row)
		{
			$aukcje = explode(';', $row[$typ]);
			if(count($aukcje) == 0) continue;
			foreach($aukcje as $aukcja)
			if(!empty($aukcja)) $aukcjeAll[] = $aukcja;
		}
		return isset($aukcjeAll) ? $aukcjeAll : null;
	}
	
	function losoweAukcjeDlaKategorii($idKat, $idProd, $typ = 'aukcje', $limit = 3)
	{
		$select = $this->db->select()->from(array('p' => 'Produkty'), array('p.id'))
			->join(array('rp' => 'Rozmiarproduktu'), 'p.id = rp.id_produktu', array($typ))
			->join(array('r' => 'Rozmiary'), 'r.id = rp.id_koloru', array(''))
			->join(array('kp' => 'Katprod'), 'p.id = kp.id_prod', array(''))
			->where('kp.id_kat = '.$idKat.' and p.id != '.$idProd.' and '.$typ.' like "%;%"')
			->group('p.id')->order('rand()')->limit($limit);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	
	function produktyNaAukcjach($typ = 'aukcje')
	{
		$select = $this->db->select()->from(array('p' => 'Produkty'), array(''))
			->join(array('rp' => 'Rozmiarproduktu'), 'p.id = rp.id_produktu', array('aukcje', 'ebay'))
			->join(array('r' => 'Rozmiary'), 'r.id = rp.id_koloru', array(''))
			->where('rp.'.$typ.' like "%;%"')->group('p.id');
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	
	function zmien($dane)
	{
		$where = 'id = '.$this->id;
		return $this->update($dane, $where);
	}
	function znajdzKod($kod)
	{
		$select = $this->select()->where('id_ext = "'.$kod.'"');
		$result = $this->fetchRow($select);
		return $result;
	}
	
	function usun()
	{
		$result = $this->delete('id = '.$this->id);
	}
	function usunProdukt($id)
	{
		$result = $this->delete('id_produktu = '.$id);
	}
	function usunProduktyIds($ids)
	{
		$result = $this->delete('id_produktu in ('.$ids.')');
	}
			
	function znajdzKolor($id_prod,$id_kol)
	{
		$result = $this->fetchAll('id_produktu = '.$id_prod.' AND id_koloru = '.$id_kol)->toArray();
		return $result;
	}
	
	function cenaMax($kateg = 0, $hurt = false) 
	{
		$cena = 'if(promocja=1,max(r.cena_promocji_b),max(r.cena_brutto))';
		if($hurt) $cena = 'if(promocja=1,max(r.cena_promocji_b_hurt),max(r.cena_brutto_hurt))';
		
		$where = 'widoczny = 1';
		if($kateg > 0) $where .= ' and k.id_kat = '.$kateg;
		$sql = $this->select()->from(array('p' => 'Produkty'), array(''))
			->joinleft(array('k' => 'Katprod'), 'p.id = k.id_prod', array(''))
			->joinleft(array('r' => 'Rozmiarproduktu'), 'p.id = r.id_produktu', array($cena.' as cena'))
			->where($where)->group('p.id')->order('cena desc')->limit(1);
		//echo $sql;
		$result = $this->fetchAll($sql);
		return count($result) > 0 ? $result[0]['cena'] : 0;
	}
	
	function cenaDetalFromHurtMarzaForKateg($kateg = 0, $marza = 0)
	{
		$sql = 'UPDATE Rozmiarproduktu SET ';
		$sql.= 'cena_netto = if(cena_netto_hurt > 0, cena_netto_hurt * (1 + '.$marza.' / 100), cena_netto)';
		$sql.= ', ';
		$sql.= 'cena_brutto = if(cena_brutto_hurt > 0, cena_brutto_hurt * (1 + '.$marza.' / 100), cena_brutto)';
		$sql.= ', ';
		$sql.= 'cena_promocji_n=if(cena_promocji_n_hurt>0,cena_promocji_n_hurt*(1+'.$marza.'/100),cena_promocji_n)';
		$sql.= ', ';
		$sql.= 'cena_promocji_b=if(cena_promocji_b_hurt>0,cena_promocji_b_hurt*(1+'.$marza.'/100),cena_promocji_b)';
		//$sql.= ' WHERE id_produktu in (SELECT id_prod from Katprod where id_kat = '.$kateg.')';
		$sql.= ' WHERE id_produktu in (SELECT id_prod from Katprod group by id_prod having max(id_kat) = '.$kateg.')';
		//echo $sql;die();
		$this->db->query($sql);
		if($this->obConfig->caching) $this->common->cacheRemove($sql, 'Rozmiarproduktu');
	}
	
	function getRozmiaryString($id, $rozmiary = false, $rozmiarowka = 0, $sep = ',', $spa = ' ')
	{
		if($rozmiary === false)
		{
			$rozmiary->id = $id;
			$rozmiary = $this->wypisz($rozmiarowka);
		}
		$result = '';
        for($i = 0; $i < count($rozmiary); $i++)
		{
			$result .= $rozmiary[$i]['nazwa'];
			if($i < count($rozmiary) - 1) $result .= $sep.$spa;
		}
        return $result;
    }
	
	function wypiszRozmiaryImport($sort = 'nazwa', $caching = true)
	{
        $where = '1';
		if(true)
		{
			$session = new Zend_Session_Namespace();
			$cache = $session->cache;
			$cachename = 'import_rozmiary';
			if($cache) $result = $cache->load('cache_'.$cachename); else $result = false;
			if($result === false || !$caching)
			{
                $select = $this->db->select()
					->from(array('rp' => 'Rozmiarproduktu'),array('*', $sort))
					->where($where);//->order('nazwa asc');
				//echo $select;
				$result = $select->query()->fetchAll();
				//$result = $this->db->fetchAll($select);
				if(count($result) > 0)
				foreach($result as $id => $result)
				{
					$results[trim($result[$sort])] = $result;
					//unset($result[$id]);
				}
				$result = @$results;
                if($cache) $cache->save($result, 'cache_'.$cachename);
            }
            return $result;
		}
        $result = $this->fetchAll($where, 'nazwa asc');
        return $result;
    }
}
?>