<?php
class Kontrahenci extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db, $karty;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

    function dodaj($dane)
	{
		$dane['lang'] = $this->lang;
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }
    function edytuj($dane)
	{
        $where = 'id = '.$this->id;
        return $this->update($dane, $where);
    }
	function edytujIds($dane, $ids)
	{
        $where = 'id in ('.$ids.')';
		//echo $where.' '.$this->id;
        return $this->update($dane, $where);
    }
	function edytujEmail($dane, $email = '')
	{
        $where = 'email = "'.$email.'"';
        $this->update($dane, $where);
    }
    function wypisz()
	{
        $sql = $this->select()->order('nazwisko');
        $result = $this->fetchAll($sql);
        return $result;
    }
	function wypiszNazwa()
	{
		$nazwa = 'TRIM(CONCAT(nazwa_firmy, " ", nazwa, " ", nazwisko, " ", imie))';
        $sql = $this->db->select()->from(array('k' => 'Kontrahenci'), array('*', 'nazwa' => $nazwa))->order($nazwa);
        $result = $this->db->fetchAll($sql);
        return $result;
    }
	function szukaj($fraza = '')
	{
		$where = '1';
		$where.= ' AND (0';
		$where.= ' OR nazwa_firmy LIKE "%'.$fraza.'%"';
		$where.= ' OR nazwisko LIKE "%'.$fraza.'%"';
		$where.= ' OR nazwa LIKE "%'.$fraza.'%"';
		//$where .= ' OR email LIKE "%'.$fraza.'%"';
		$where .= ')';
        $sql = $this->select()->where($where)->order(array('nazwa_firmy','nazwa','nazwisko'));
        $result = $this->fetchAll($sql);
        return $result;
    }
    function usun()
	{
        $result = $this->delete('id = '.$this->id);
    }
	function usunIds($ids)
	{
		if($this->common->isArray($ids, true))
        $result = $this->delete('id in ('.implode(',', $ids).')');
    }
    function wypiszPojedynczy()
	{
        $result = $this->fetchRow('id = '.$this->id);
        return $result;
    }
    function wypiszPojedynczyArray()
	{
        $result = $this->fetchRow('id = '.$this->id);
        if(!empty($result)) $result = $result->toArray();
        return $result;
    }
	function wypiszPojedynczyEmail($email = '', $weryfikacja = false)
	{
		$where = 'email = "'.$email.'"';
		if($weryfikacja) $where .= ' AND weryfikacja = "1" AND weryfikacja_admin = "1"';
        $result = $this->fetchRow($where);
        return $result;
    }
    function pobierzLogin($login, $weryfikacja = true)
	{
		$where = $this->obConfig->klienciLogin.' = "'.$login.'"';
		if($weryfikacja) $where .= ' AND weryfikacja = "1" AND weryfikacja_admin = "1"';
        $result = $this->fetchRow($where);
        return $result;
    }
	function pobierzFacebookID($facebookID, $weryfikacja = false)
	{
        $where = 'facebookid = "'.$facebookID.'"';
		if($weryfikacja) $where .= ' AND weryfikacja = "1"';
        $result = $this->fetchRow($where);
        return $result;
    }
    function weryfikujKontrahenta($dane)
	{
        $where = 'id = '.$this->id;
        $this->update($dane, $where);
    }
	function getKontrahenci($pole = 'id', $where = 1)
	{
		$sql = $this->select()->where($where);
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i][$pole]] = $result[$i]->toArray();
		}
		return @$results;
	}
	public function getSumClient($pole = 'email')
	{
		$results = array();
		$result = $this->fetchAll($this->select()->from(array('ak' => 'Zamowieniakontrahenci'),
			array($pole, 'sum(kwotazaplata) as suma'))->setIntegrityCheck(false)->group($pole));
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i][$pole]] = floatval($result[$i]['suma']);
		}
		return $results;
	}
	
	function wypiszKlienciImport($sort = 'nazwa_firmy', $caching = true)
	{
        $where = 1;//'k.nazwa_firmy <> ""';
		if(true)
		{
			$session = new Zend_Session_Namespace();
			$cache = $session->cache;
			$cachename = 'import_klienci';
			if($cache) $result = $cache->load('cache_'.$cachename); else $result = false;
			if($result === false || !$caching)
			{
                $select = $this->db->select()
					->from(array('k' => 'Kontrahenci'), array('*', $sort))
					->where($where);//->order('nazwa_firmy asc');
				//echo $select;
				$result = $select->query()->fetchAll();
				//$result = $this->db->fetchAll($select);
				if(count($result) > 0)
				foreach($result as $id => $result)
				{
					$results[trim($result[$sort])] = $result;
					//unset($result[$id]);
				}
				$result = @$results;
                if($cache) $cache->save($result, 'cache_'.$cachename);
            }
            return $result;
		}
        $result = $this->fetchAll($where, 'nazwa_firmy asc');
        return $result;
    }
	
	function wypiszMiasta() 
	{
        $sql = $this->select()->distinct()->from(array('k' => 'Kontrahenci'), 'miasto')->where('miasto <> ""')->group('miasto')->order('miasto asc');
        $result = $this->fetchAll($sql);
        return $result;
    }
	function wypiszPowiaty() 
	{
        $sql = $this->select()->distinct()->from(array('k' => 'Kontrahenci'), 'powiat')->where('powiat <> ""')->group('powiat')->order('powiat asc');
        $result = $this->fetchAll($sql);
        return $result;
    }
	function wypiszGrupy() 
	{
        $sql = $this->select()->distinct()->from(array('k' => 'Kontrahenci'), 'grupa')->where('grupa > 0')->group('grupa')->order('grupa asc');
        $result = $this->fetchAll($sql);
        return $result;
    }
	
	function getCardLike()
	{
        $sql = $this->select()->distinct()->from(array('k' => 'Kontrahenci'), '*')
			->where('karta LIKE "%'.$this->karta.'%"')// AND kartaaktywna ="1"')
			->order('nazwisko asc');
        $result = $this->fetchAll($sql)->toArray();
        return $result;
    }
	function getFromKarta($karta, $md5 = false)
	{
		if(!$md5) $result = $this->fetchRow('karta = "'.$karta.'"');
        else $result = $this->fetchRow('md5(karta) = "'.$karta.'"');
        return $result;
    }
	function getNowyNumerKarty()
	{
        //$result = $this->fetchAll('karta <> ""');
		$sql = $this->db->select()->from(array('k' => 'Kontrahenci'), array('karta'))->where('karta <> ""');
        $result = $this->db->fetchAll($sql);
		if(count($result) > 0)
		foreach($result as $row)
		{
			$numeryKart[$row['karta']] = $row;
		}
		do $numerKarty = rand(10000, 32767).mt_rand(1000000000, 2147483647);
		while(isset($numeryKart[$numerKarty]));
        return $numerKarty;
    }
	function getNowyNumerKartyMulti()
    {
        if(empty($this->karta))
		{
            $sql = $this->db->select()
				->from(array('k' => 'Kontrahenci'), array('karta'))
				->where('karta <> ""')->order('karta DESC');
            $res = $this->db->fetchAll($sql);
            
            $k = 0;
            $this->kartaost = '';
            
            if(count($res) > 0)
            foreach($res as $row)
            {
                $this->karta[$row['karta']] = $row;
                if($k == 0) $this->kartaost = $row['karta']; 
                $k++;
            }
        }
        
        if(isset($this->kartaost))
		{
            $this->kartaost = $this->kartaost;
        }
		else
		{
            $this->kartaost = '00001';
        }
        
        if(!empty($this->numeruj))
        {
            $this->numeruj = intval($this->numeruj);
            $size = strlen($this->numeruj);
            $length = 5 - $size;
            $kk = '';
            for($k = 0; $k < $length; $k++)
            {
                $kk .= '0';
            }
            $numer = $kk.$this->numeruj;
            
            if($numer > $this->kartaost)
            {
                $this->kartaost = $numer;
            }
        }
		
        do
		{
            $this->kartaost = $this->kartaost+1;
            //$numerKarty = mt_rand(50000, 100000);
            $size = strlen($this->kartaost);
            $length = 5 - $size;
            $kk = '';
            for($k = 0; $k < $length; $k++)
            {
                $kk .= '0';
            }
            $numerKarty = $kk.''.$this->kartaost;
        }
		while(isset($this->karta[$numerKarty]));
		
        $this->karta[$numerKarty] = $numerKarty;
        return $numerKarty;
    }
	
	function wypiszKontr($ilosc, $od, $ile, $sortowanie, $szukanie, $lojalnosc = false, $ids = null)
	{
		$od = $od * $ile;
		$limit = $ile;
		if($ilosc) { $od = 0; $limit = 9999; }
		$where = '1';
		if($lojalnosc) $where .= ' and karta <> ""';//and k.kartaaktywna <> "0"
		if($this->common->isArray($ids, true)) $where .= ' and k.id in ('.implode(',',$ids).')';
		
		if($szukanie !== null)
		{
			if($szukanie->nazwa != '')
			{
				$where .= ' and (imie like "%'.$szukanie->nazwa.'%"';
				$where .= ' or nazwisko like "%'.$szukanie->nazwa.'%"';
				$where .= ' or id_ext like "%'.$szukanie->nazwa.'%"';
				$where .= ' or nazwa_firmy like "%'.$szukanie->nazwa.'%"';
				$where .= ' or nazwa like "%'.$szukanie->nazwa.'%"';
				$where .= ' or email like "%'.$szukanie->nazwa.'%"';
				$where .= ' or komorka like "%'.$szukanie->nazwa.'%"';
				$where .= ' or telefon like "%'.$szukanie->nazwa.'%")';
			}
			if($szukanie->id_ext != '') $where .= ' and id_ext like "%'.$szukanie->id_ext.'%"';
			if($szukanie->grupa != 'all') $where .= ' and grupa = '.$szukanie->grupa.'';
			if($szukanie->miasto != 'all') $where .= ' and miasto = "'.$szukanie->miasto.'"';
			if($szukanie->powiat != 'all') $where .= ' and powiat = "'.$szukanie->powiat.'"';
			if($szukanie->wojew != 'all') $where .= ' and wojew = "'.$szukanie->wojew.'"';
			if($szukanie->plec != 'all') $where .= ' and plec = "'.$szukanie->plec.'"';
			if($szukanie->karta != '') $where .= ' and karta like "%'.$szukanie->karta.'%"';
			if(false && $szukanie->data_ur != '') 
			$where .= ' and data_ur >= "'.$szukanie->dataOd.'" and data_ur <= "'.$szukanie->dataDo.'"';
			if($szukanie->zalogowano != '')
			{
				//$where .= ' and (zalogowano is null'; 
				$where .= ' and zalogowano >= "'.$szukanie->dataOd.' 00:00:00"';
				$where .= ' and zalogowano <= "'.$szukanie->dataDo.' 23:59:59"';
			}
			if($szukanie->data_rej != '')
			{
				$where .= ' and data_rej >= "'.$szukanie->dataOd.' 00:00:00"';
				$where .= ' and data_rej <= "'.$szukanie->dataDo.' 23:59:59"';
			}
			if(@$szukanie->newsletter) $where .= ' and newsletter';
			if(@$szukanie->widoczny) $where .= ' and widoczny = "0"';
			else if( true || false ) $where .= ' and widoczny = "1"';
			
			if(@strlen($szukanie->faktura) > 0) $where .= ' and faktura = "'.$szukanie->faktura.'"';
			if(@strlen($szukanie->rodzaj_klienta) > 0) $where .= ' and rodzaj_klienta = "'.$szukanie->rodzaj_klienta.'"';
			if(@strlen($szukanie->weryfikacja) > 0) $where .= ' and weryfikacja = "'.$szukanie->weryfikacja.'"';
			if(@strlen($szukanie->weryfikacja_admin) > 0) $where .= ' and weryfikacja_admin = "'.$szukanie->weryfikacja_admin.'"';
			if(@$szukanie->blokada) $where .= ' and blokada';
		}
		
		if($sortowanie == null) 
		{
			$sortowanie = (object)array();
			$sortowanie->sort = 'nazwa_firmy';
			$sortowanie->order = 'asc';
		}
		if($sortowanie->sort == 'nazwa_firmy') $sortowanie->sort = 'if(nazwa_firmy<>"",nazwa_firmy,nazwisko)';
		$order = array($sortowanie->sort.' '.$sortowanie->order, 'zalogowano desc');
		
		if($lojalnosc)
		{
			if(isset($szukanie->sortuj))
			{
				if($szukanie->sortuj == 'za')
				{
					$sql1 = $this->select()
						->from(array('zk' => 'Zamowieniakontrahenci'), 'sum(zk.kwotazaplata)')
						->where('zk.email = k.email');
					$order = 'sort DESC';
					$sql1->setIntegrityCheck(false);
				}
				if($szukanie->sortuj == 'zn')
				{
					$sql1 = $this->select()
						->from(array('zn' => 'Kontrahenci'), 'count(zn.id)')
						->where('zn.polecajacy = k.id');
					$order = 'sort DESC';
					$sql1->setIntegrityCheck(false);
				}
				if($szukanie->sortuj == 'zk')
				{
					$sql1 = '1';
					$order = 'k.konto DESC';
				}
				if($szukanie->sortuj == 'zr')
				{
					$sql1 = '1';
					$order = 'k.data_rej DESC';
				}
				if($szukanie->sortuj == 'zf')
				{
					$sql1 = '1';
					$order = 'if(k.nazwa_firmy<>"",k.nazwa_firmy,k.nazwisko) ASC';
				}
			}
			else
			{
				$sql1 = $this->select()
					->from(array('zk' => 'Zamowieniakontrahenci'), 'sum(zk.kwotazaplata)')
					->where('zk.email = k.email');
				$order = 'sort DESC';
				$sql1->setIntegrityCheck(false);
			}
		}
		
		if($lojalnosc)
		{
			$sql2 = $this->select()->from(array('zn' => 'Kontrahenci'), 'count(zn.id)')->where('zn.polecajacy = k.id');
			$sql2->setIntegrityCheck(false);
			$znajomych = new Zend_Db_Expr('('.$sql2.')');

			$sql = $this->select()->setIntegrityCheck(false)->from(array('k' => 'Kontrahenci'), 
				array('*', 'sort' => new Zend_Db_Expr('('.$sql1.')'), 'znajomych'=> $znajomych));
				
			if(false)
			$sql->joinleft(array('kl' => 'Kontrahencilogin'),'k.id = kl.id_kontr',
							array('zalogowano, max(zalogowano) as data_login'));
			if(false)
			$sql->joinleft(array('lp' => 'Lojalnoscpolecenia'),'k.email = lp.polecany',array('wykorzystano'));
			
			$sql->where($where)->group('k.id')->order($order)->limit($limit, $od);

			$result = $this->fetchAll($sql)->toArray();
			return $ilosc ? count($result) : $result;
		}
		
        $sql = $this->db->select()->from(array('k' => 'Kontrahenci'), array($ilosc?'count(distinct k.id)':'*'))
								->joinleft(array('kl' => 'Kontrahencilogin'),'k.id = kl.id_kontr',
								$ilosc ? '' : array('zalogowano, max(zalogowano) as data_login'));
		if($lojalnosc)
		$sql->joinleft(array('lp' => 'Lojalnoscpolecenia'),'k.email = lp.polecany',$ilosc?'':array('wykorzystano'));
		
		$sql->where($where);
		if(!$ilosc) $sql->group('k.id')->order($order);
		$sql->limit($limit, $od);
		
		//if(!$ilosc){echo $sql;return null;}
        $result = $this->db->fetchAll($sql);
        return $ilosc ? @intval($result[0]['count(distinct k.id)']) : $result;
    }
	
	public function getClientsWithDiscountTest($tylkoArchiwalne = true)
    {
		$where = '';
		if($tylkoArchiwalne)
		{
			$where .= ' and CONCAT(wartosc_zamowienia,status,data) not in';
			$where .= ' (SELECT CONCAT(wartosc_zamowienia,status,data)';
			$where .= ' FROM Zamowieniakontrahenci WHERE email = ak.email)';
		}
		
		//$sql = $this->select()->where('1')->fetchAll();
		
		$sql1 = 'select ifnull(sum(zk.wartosc_zamowienia),0) as zakupy';
		$sql1.= ' from Zamowieniakontrahenci as zk where zk.email = k.email';
		$sql0 = 'select ifnull(sum(ak.wartosc_zamowienia),0) as archiwum';
		$sql0.= ' from Archiwumkontrahenci as ak where ak.email = k.email '.$where;
		$sql3 = 'select ifnull(sum(zk.rabatpolecajacy),0) as polecenie';
		$sql3.= ' from Zamowieniakontrahenci as zk where zk.email = k.email';
		$sql2 = 'select count(ko.id) as num';
		$sql2.= ' from Kontrahenci as ko where ko.polecajacy = "'.$this->id.'"';
		
		$select = '('.$sql1.') as zakupy,';
		$select.= '('.$sql0.') as archiwum,';
		$select.= '('.$sql3.') as polecenie,';
		$select.= '('.$sql2.') as num';
		
		$sql = 'select *, '.$select.' from Kontrahenci as k';
		$sql.= ' where k.polecajacy = "'.$this->id.'"';
		$sql.= ' order by '.$this->order.' DESC';
		$sql.= ' limit '.$this->rowscount.'';
		$sql.= ' offset '.$this->page.'';		
		
		return $this->db->fetchAll($sql);
    }
	
	public function getClientsWithDiscount($tylkoArchiwalne = true)
    {
		$where = '';
		if($tylkoArchiwalne)
		{
			$where .= ' and CONCAT(wartosc_zamowienia,status,data) not in';
			$where .= ' (SELECT CONCAT(wartosc_zamowienia,status,data)';
			$where .= ' FROM Zamowieniakontrahenci WHERE email = ak.email)';
		}
		
        $sql1 = $this->select()->setIntegrityCheck(false)->from(array('zk' => 'Zamowieniakontrahenci'), 
		array('zakupy'=>'ifnull(sum(zk.wartosc_zamowienia),0)'))->where('zk.email = k.email');
		
		$sql0 = $this->select()->setIntegrityCheck(false)->from(array('ak' => 'Archiwumkontrahenci'), 
		array('archiwum'=>'ifnull(sum(ak.wartosc_zamowienia),0)'))->where('ak.email = k.email '.$where);

        $sql3 = $this->select()->setIntegrityCheck(false)->from(array('zk' => 'Zamowieniakontrahenci'), 
		array('polecenie'=>'ifnull(sum(zk.rabatpolecajacy),0)'))->where('zk.email = k.email');

        $sql2 = $this->select()->setIntegrityCheck(false)
            ->from(array('ko'=>'Kontrahenci'), array('num' => 'COUNT(ko.id)'))
            ->where('ko.polecajacy = "'.$this->id.'"');

        $sql = $this->select()->setIntegrityCheck(false)
			->from(array('k'=>'Kontrahenci'), array('*',
			'num'=>new Zend_Db_Expr('('.$sql2.')'), 
			'zakupy'=>new Zend_Db_Expr('('.$sql1.')'),
			'archiwum'=>new Zend_Db_Expr('('.$sql0.')'), 
			'polecenie'=>new Zend_Db_Expr('('.$sql3.')')))
            ->where('k.polecajacy = "'.$this->id.'"')
			->limitPage($this->page, $this->rowscount)
			->order($this->order.' DESC');
		//echo $sql;

        return $this->fetchAll($sql)->toArray();
    }

    public function cutRow()
    {
        $this->update(array('polecajacy'=> 0), 'id="'.$this->id.'" and polecajacy="'.$this->iduser.'"');
    }
    
    function getFriendToSend() 
    {
        $sql = $this->select()
                ->from(array('k' => 'Kontrahenci'), '*')
                ->where('k.polecajacy IN (?)',$this->sendlist)
                ->group('k.id');
        $result = $this->fetchAll($sql)->toArray();
        return $result;
    }
	
	function iloscKlientowAll()
	{
        $select = $this->db->select()->from(array('k' => 'Kontrahenci'), array('count(*)'))->where('1');
		//echo $select;
		$result = $this->db->fetchAll($select);
        return @intval($result[0]['count(*)']);
    }
	function iloscKlientowNewsletter()
	{
        $select = $this->db->select()->from(array('k' => 'Kontrahenci'), array('count(*)'))->where('newsletter and lang = "'.$this->lang.'"');
		//echo $select;
		$result = $this->db->fetchAll($select);
        return @intval($result[0]['count(*)']);
    }
	function wypiszKlienciNewsletter()
	{
        $select = $this->db->select()->from(array('k' => 'Kontrahenci'), array('email'))->where('newsletter and lang = "'.$this->lang.'"');
		//echo $select;
		$result = $this->db->fetchAll($select);
        return $result;
    }
	function wypiszKlienciDoNewslettera($typ = 0, $newsletter = "")
	{
		$pole = $typ ? 'komorka' : 'email';
		//array('CONCAT(komorka,",",email",imie,",",nazwisko,",",id)'))
        $select = $this->db->select()->from(array('k' => 'Kontrahenci'), array($pole))
			->where('widoczny and lang = "'.$this->lang.'" and '.(strlen($newsletter) == 1 ? 'newsletter = "'.intval($newsletter).'"' : "1"));
		//echo $select;
		$result = $this->db->fetchAll($select);
		if(count($result) > 0)
		foreach($result as $row)
		{
			if(@!empty($row[$pole]))
			$rows[] = $row[$pole];
		}
        return @$rows;
    }
	
	function wypiszKlienciEksport($sort = 'k.nazwa_firmy asc', $caching = true)
	{
        $where = 'k.'.$this->obConfig->klienciLogin.' <> ""';
		$sort = 'k.'.$sort.' asc';
		if(true)
		{
			$session = new Zend_Session_Namespace();
			$cache = $session->cache;
			$cachename = 'eksport_klienci';
			if($cache) $result = $cache->load('cache_'.$cachename); else $result = false;
			if($result === false || !$caching)
			{
                $select = $this->db->select()->from(array('k' => 'Kontrahenci'), array('*'))
				->where($where)->order($sort);
				//echo $select;
				$result = $select->query()->fetchAll();
				//$result = $this->db->fetchAll($select);
                if($cache) $cache->save($result, 'cache_'.$cachename);
            }
            return $result;
		}
        $result = $this->fetchAll($where, 'nazwa_firmy asc');
        return $result;
    }
}
?>