<?php
class Paczkomatydane extends Zend_Db_Table
{
	public $packType = array('A' => '8 x 38 x 64 cm', 'B' => '19 x 38 x 64 cm', 'C' => '41 x 38 x 64 cm');
	public $statusy = array();
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		
		$statusy['Created'] = 'Oczekuje na wysyłkę';
		$statusy['Prepared'] = 'Gotowa do wysyłki';
		$statusy['Sent'] = 'Przesyłka Nadana';
		$statusy['InTransit'] = 'W drodze';
		$statusy['Stored'] = 'Oczekuje na odbiór';
		$statusy['Avizo'] = 'Ponowne Avizo';
		$statusy['Expired'] = 'Nie odebrana';
		$statusy['Delivered'] = 'Dostarczona';
		$statusy['RetunedToAgency'] = 'Przekazana do Oddziału';
		$statusy['Cancelled'] = 'Anulowana';
		$statusy['Claimed'] = 'Przyjęto zgłoszenie reklamacyjne';
		$statusy['ClaimProcessed'] = 'Rozpatrzono zgłoszenie reklamacyjne';
		$this->statusy = $statusy;
    }

	function updateData($array)
	{
		$where = 'id = 1';
		$this->update($array, $where);
	}
	
	function showData()
	{
		$result = $this->fetchAll();		
		return $result;
	}
	
	function wypisz()
	{
		$result = $this->select()->where('id = 1');
		return $this->fetchRow($result)->toArray();
	}
}
?>