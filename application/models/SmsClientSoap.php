<?php
require_once 'smsapi.php';
require_once 'smsapi/http/sms.php';
require_once 'smsapi/httpclient.php';
require_once 'smsapi/soapclient.php';
class SmsClientSoap extends Zend_Soap_Client
{
	public function  __construct($module = 'admin')
	{
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		
		spl_autoload_register(array('smsAPI', '__autoload'));
	}
	
	public function kartaQR($karta)
	{
		try
		{
			require_once('../application/models/phpqrcode/qrlib.php');
			//require_once('../application/models/phpqrcode/phpqrcode.php');
			$QR = 'admin/qr/qr.png';
			QRcode::png($karta, $QR);
			//echo '<img src="'.$this->obConfig->www.'/public/'.$QR.'">';die();
			return array('error' => '', 'image' => $QR);
		}
		catch(Exception $e)
		{
		   return array('error' => $e->getMessage(), 'image' => '');
		}
	}

	public function send($message, $odbiorca = null, $typ = null, $nadawca = null)
	{
		$confSms = new Confsms();
		$confSms->id = 1;
		$config = $confSms->wyswietl()->toArray();
		try
		{
			//$smsapi = new smsapi_httpclient($config['login'], $config['password']);
			$smsapi = new smsapi_soapclient($config['login'], $config['password']);
			$msg = $smsapi->new_sms();

			if(empty($odbiorca)) $odbiorca = $config['admin_sms'];
			//$msg->add_to($odbiorca);
			$msg->recipient = $odbiorca;
			
			$msg->message = $message;
			$msg->eco = $typ !== null ? $typ : $config['typ_wiadomosci'];
			$msg->from = $nadawca !== null ? $nadawca : $config['pole_nadawca'];
			$msg->sender = $nadawca !== null ? $nadawca : $config['pole_nadawca'];
			$msg->date = time() + 10;
			$msg->encoding = 'utf-8';
			$msg->test = false; 
			$msg->details = true;
			$res = $msg->send();
			//var_dump($res);
			if(false && !empty($res[0]->error))
			{
				//var_dump($res);
				$blad = @intval($res[0]->error);
				return 'Błąd '.$blad.' - '.@smsapi_httpclient::$errors[$blad];
			}
			return null;
		}
		catch(Exception $e)
		{
			return "\n\nBłąd: ".$e->getMessage();
		}
	}
	
	public function wartoscKonta()
	{
		$confSms = new Confsms();
		$confSms->id = 1;
		$config = $confSms->wyswietl()->toArray();		
		try
		{
			$smsapi = new smsapi_httpclient($config['login'], $config['password']);
			//var_dump(smsapi_httpclient::$errors);
			if(@!empty($smsapi->get_points()->error))
			{
				$blad = @intval($smsapi->get_points()->error);
				return 'Błąd #'.$blad.' - '.@smsapi_httpclient::$errors[$blad];
			}
			return $ilosc = $smsapi->get_points()->points;
		}
		catch(Exception $e)
		{
			return "\n\nERROR ".$e->getMessage();
		}			
	}
	
	function smsKlient($message, $odbiorca = null, $typ = null, $nadawca = null)
	{
		$confSms = new Confsms();
		$confSms->id = 1;
		$dane = $confSms->wyswietl()->toArray();
		
		$odbiorca = str_replace('+48', '', $odbiorca);
		$odbiorca = str_replace('+', '', $odbiorca);
		$odbiorca = str_replace('-', '', $odbiorca);
		$odbiorca = str_replace('(', '', $odbiorca);
		$odbiorca = str_replace(')', '', $odbiorca);
		
		$odbiorca = preg_replace('/[^0-9,]/', '', $odbiorca);
		
		$soap = null;

		try
		{
			$soap = new SoapClient('https://ssl.smsapi.pl/webservices/v2/?wsdl' , array
				(
					'features'   => SOAP_SINGLE_ELEMENT_ARRAYS,
					'cache_wsdl' => WSDL_CACHE_NONE,
					'trace'      => true,
				)
			);

			//$odbiorca = '513701688';
			
			$client = array('username' => $dane['login'], 'password' => $dane['password']);
			$sms = array
			(
				'sender'    => $nadawca,
				'recipient' => $odbiorca,
				'eco'       => $typ,
				'date_send' => 0,
				'date_validate' => 1,
				'encoding'   => 'utf-8',
				'details'   => 1,
				'test'   => true,
				'message'   => $message,//.' [%1%] ',
				//'params'    => array('', 'parametr 2', 'parametr 3', 'parametr 4'),
				'idx'       => isset($idx) ? $idx : uniqid()
			);			

			$params = array
			(
				'client' => $client,
				'sms'    => $sms
			);

			$result = $soap->send_sms($params);
			//$result = $soap->send_sms_multi($params);
			//var_dump($result);
			return null;
			return $result;
		}
		catch(Exception $e) 
		{
			return $e->getMessage();
		}
		catch(SoapFault $error)
		{
			return $error->getMessage();
		}
	}
	
	function mms_send($params, $backup = false)
	{
		static $content;
		$url = $backup ? 'https://ssl2.smsapi.pl/mms.do' : 'https://ssl.smsapi.pl/mms.do';
		try
		{
			$c = curl_init();
			curl_setopt( $c, CURLOPT_URL, $url );
			curl_setopt( $c, CURLOPT_POST, true );
			curl_setopt( $c, CURLOPT_POSTFIELDS, $params );
			curl_setopt( $c, CURLOPT_RETURNTRANSFER, true );

			$content = curl_exec( $c );
			$http_status = curl_getinfo($c, CURLINFO_HTTP_CODE);

			if($http_status != 200 && $backup == false)
			{
				$backup = true;
				self::mms_send($params, $backup);
			}
			
			curl_close( $c );
			return $content;
		}
		catch(Zend_Exception $e)
		{
			return $e;
		}
	}

	function mmssend($message, $odbiorca = null, $typ = null, $nadawca = null, $subject = 'MMS')
	{
		$error = '';
		$confSms = new Confsms();
		$confSms->id = 1;
		$config = $confSms->wyswietl()->toArray();

		$nadawca = null;

		$smil= urlencode('<smil><head><layout><root-layout height="600" width="425"/>
		<region id="Image" top="0" left="0" height="100%" width="100%" fit="meet"/></layout></head>
		<body><par dur="5000ms"><img src="http://'.$_SERVER['HTTP_HOST'].''.$message.'" region="Image"></img></par>
		</body></smil>');

		$subject = mb_strlen($subject) > 30 ? substr($subject, 0, 28).'..' : $subject;

		$params = array
		(
		   'username' => $config['login'],
		   'password' => $config['password'],
		   'to' => $odbiorca,
		   'from' => $nadawca !== null ? $nadawca : $config['pole_nadawca'],
		   'subject' => urlencode($subject), //max 30 znakow
		   'smil' => $smil,
		   'eco' => $typ !== null ? $typ : $config['typ_wiadomosci']
		);

		$data = '?username=' . $params['username'] . '&password=' . $params['password'] . '&to=' . $params['to'] . '&subject=' . $params['subject'] . '&smil=' . $params['smil'];
		$plik = fopen('https://ssl.smsapi.pl/mms.do' . $data,'r');
		$mms = fread($plik,1024);
		fclose($plik);

		$checkstan = explode(':',$mms);

		if(isset($checkstan[0]))
		{
			switch($checkstan[0])
			{
				case 'ERROR':
					return array('error'=> self::smserror($checkstan[1]));
				break;
				case 'OK' :
					return array('points'=> (isset($checkstan[2])? $checkstan[2] : 0.00));
				break;
			}
		}
	}
	
	public function sendMMS($img, $odbiorca = null, $temat = null, $nadawca = null, $soap = false, $test = false)
	{
        $error = '';
		$confSms = new Confsms();
		$confSms->id = 1;
		$config = $confSms->wyswietl()->toArray();
		try
		{
			$username = $config['login'];
			$password = ($config['password']);

			if($odbiorca === null) $odbiorca = $config['admin_sms'];
			if($nadawca === null) $nadawca = $config['pole_nadawca'];
			if($temat === null) $temat = 'Twój numer karty rabatowej';
			$subject = urlencode($temat);

			if(@$img)
			{
				$size = @getimagesize('../'.$img);
				//var_dump($size);die();
				$width = @intval($size[0]);
				$height = @intval($size[1]);
			}
			$smil = '
			<smil>
				<head>
					<layout>
					<root-layout width="'.$width.'" height="'.$height.'" />';
			if(@$img)$smil.='<region id="Image" width="100%" height="100%" left="0" top="0" />'; //Rozmiar obrazka
			if(@$txt)$smil.='<region id="Text" width="100%" height="20" left="0" top="0" />'; //Rozmiar pola tekst
			if(@$aud)$smil.='<region id="Audio" width="100%" height="1" left="0" top="0" />'; //Rozmiar pola audio
			$smil.='</layout>
				</head>
				<body>
					<par dur="'.@intval($dur).'s">';
			if(@$img)$smil.='<img src="'.$this->obConfig->www.'/'.$img.'" region="Image" />'; //Adres do obrazka
			if(@$txt)$smil.='<text src="'.$this->obConfig->www.'/'.$txt.'" region="Text" />'; //Adres do pliku tekstowego
			if(@$aud)$smil.='<audio src="'.$this->obConfig->www.'/'.$aud.'" region="Audio" />'; //Adres do pliku audio
			$smil.='</par>
				</body>
			</smil>';
			//echo htmlspecialchars($smil);die();
			
			$smil = urlencode($smil);			
			
			$data = '?username='.$username.'&password='.$password.'&to='.$odbiorca.'&subject='.$subject.'&smil='.$smil;
			//echo htmlspecialchars($data);die();
			$plik = fopen('https://ssl.smsapi.pl/mms.do'.$data, 'r');
			$wynik = fread($plik, 1024);
			fclose($plik);
			//var_dump($wynik);die();
			
			$row = explode(':', $wynik);
			//var_dump($row);die();
			if(@strpos($row[0], 'OK') !== false)
			return array('id' => (int)@$row[1], 'points' => (float)@$row[2]); //OK:<ID>:<POINTS> 
			else
			return array('error' => @smsapi_httpclient::$errors[$row[1]]); //ERROR:<ERR>
		}
		catch(Exception $e)
		{
			return array('error' => $e->getMessage(), 'points' => 0);
		}
	}

	function smspoints($dane)
	{
		$soap = null;

		try 
		{
			$soap = new SoapClient('https://ssl.smsapi.pl/webservices/v2/?wsdl', array
				(
					'features'   => SOAP_SINGLE_ELEMENT_ARRAYS,
					'cache_wsdl' => WSDL_CACHE_NONE,
					'trace'      => true,
				)
			);

			$params = array
			(
				'username' => $dane['user'],
				'password' => $dane['haslo']
			);
			$result = $soap->get_points($params);
		    return   $result; 
		}
		catch(Exception $e) 
		{
			return $e;
		}
		catch(SoapFault $error)
		{
			return $error;
		}
	}

	function smsgetnumber($dane)
    {
		$soap = null;

		try 
		{
			$soap = new SoapClient( 'https://ssl.smsapi.pl/webservices/v2/?wsdl' , array(
						'features'   => SOAP_SINGLE_ELEMENT_ARRAYS,
						'cache_wsdl' => WSDL_CACHE_NONE,
						'trace'      => true,
					)
			);

			$params = array(
			   'client' => array ('username' => $dane['user'],
				'password' => md5($dane['haslo'])),'group_id' => $dane['id_group']
			);
			$result = $soap->get_numbers($params);
		   return   $result; 
		}
		catch(Exception $e) 
		{
			return $e; 
		}
		catch(SoapFault $error)
		{
			return $error;
		}
    }

	function smsgetgroup($dane)
    {
		$soap = null;

		try 
		{
			$soap = new SoapClient( 'https://ssl.smsapi.pl/webservices/v2/?wsdl' , array(
						'features'   => SOAP_SINGLE_ELEMENT_ARRAYS,
						'cache_wsdl' => WSDL_CACHE_NONE,
						'trace'      => true,
					)
			);

			$params = array(
			   'username' => $dane['user'],
				'password' => md5($dane['haslo'])
			);
			$result = $soap->get_groups($params);
		    return   $result; 
		}
		catch(Exception $e) 
		{
			return $e;
		}
		catch(SoapFault $error)
		{
			return $error;
		}
    }

	function smsaddgroup($dane)
    {
		$soap = null;

		try 
		{
			$soap = new SoapClient( 'https://ssl.smsapi.pl/webservices/v2/?wsdl' , array(
						'features'   => SOAP_SINGLE_ELEMENT_ARRAYS,
						'cache_wsdl' => WSDL_CACHE_NONE,
						'trace'      => true,
					)
			);

			$params = array(
			   'client' => array ('username' => $dane['user'],
				'password' => md5($dane['haslo'])),'name' => $dane['nazwa'],'info' => ''
			);
			$result = $soap->add_group($params);
			
		   return   $result; 
		}
		catch(Exception $e) 
		{		 
			return $e;    
		}
		catch(SoapFault $error)
		{
			return $error;
		}
    }

	function smsaddtel($dane)
    {
		$soap = null;

		try 
		{
			$soap = new SoapClient( 'https://ssl.smsapi.pl/webservices/v2/?wsdl' , array(
						'features'   => SOAP_SINGLE_ELEMENT_ARRAYS,
						'cache_wsdl' => WSDL_CACHE_NONE,
						'trace'      => true,
					)
			);

			$params = array(
			   'client' => array ('username' => $dane['user'],
				'password' => md5($dane['haslo'])),'number' => array('number'=>$odbiorca,'name'=>$dane['nazwa'],'group_id'=>$dane['gru'])
			);
			$result = $soap->add_number($params);

		   return   $result; 
		}
		catch(Exception $e) 
		{
			return $e;    
		}
		catch(SoapFault $error)
		{
			return $error;
		}
    }
	
	public function smserror($error)
	{
		$errors = array
		(
			8 => "Błąd w odwołaniu (Prosimy zgłośić)",
			11 => "Zbyt długa lub brak wiadomości lub ustawiono parametr nounicode i pojawiły się znaki specjalne. Dla wysyłki VMS błąd oznacz brak pliku WAV lub błąd tekstu TTS (brak tekstu lub inne niż UTF-8 kodowanie)",
			12 => "Wiadomość składa się z większej ilości części niż określono w parametrze &max_parts",
			13 => "Nieprawidłowy numer odbiorcy",
			14 => "Nieprawidłowe pole nadawcy",
			17 => "Nie można wysłać FLASH ze znakami specjalnymi",
			18 => "Nieprawidłowa liczba parametrów",
			19 => "Za dużo wiadomości w jednym odwołaniu",
			20 => "Nieprawidłowa liczba parametrów IDX",
			21 => "Wiadomość MMS ma za duży rozmiar (maksymalnie 100kB)",
			22 => "Błędny format SMIL",
			23 => "Błąd pobierania pliku dla wiadomości MMS lub VMS",
			24 => "Błędny format pobieranego pliku",
			25 => "Parametry &normalize oraz &datacoding nie mogą być używane jednocześnie.",
			26 => "Za długi temat wiadomości. Temat może zawierać makstymalnie 30 znaków.",
			27 => "Za długa wartość parametru idx, maksymalna długość to 36 znaków.",
			30 => "Brak parametru UDH jak podany jest datacoding=bin",
			31 => "Błąd konwersji TTS",
			32 => "Nie można wysyłać wiadomości Eco, MMS i VMS na zagraniczne numery.",
			33 => "Brak poprawnych numerów",
			35 => "Błędna wartość parametru tts_lector. Dostępne wartości: agnieszka, ewa, jacek, jan, maja",
			36 => "Nie można wysyłać wiadomości binarnych z ustawioną stopką",
			40 => "Brak grupy o podanej nazwie",
			41 => "Wybrana grupa jest pusta (brak kontaktów w grupie)",
			50 => "Nie można zaplanować wysyłki na więcej niż 3 miesiące w przyszłość",
			51 => "Ustawiono błędną godzinę wysyłki, wiadomość VMS mogą być wysyłane tylko pomiędzy godzinami 8 a 22 lub ustawiono kombinację parametrów try i interval powodującą możliwość próby połączenia po godzinie 22",
			52 => "Za dużo prób wysyłki wiadomości do jednego numeru (maksymalnie 10 prób w przeciągu 60sek do jednego numeru)",
			53 => "Nieunikalny parametr idx. Została już przyjęta wiadomość z identyczną wartością parametru idx przy wykorzystaniu parametru &check_idx=1.",
			54 => "Błędny format daty. Ustawiono sprawdzanie poprawności daty &date_validate=1",
			55 => "Brak numerów stacjonarnych w wysyłce i ustawiony parametr skip_gsm",
			56 => "Różnica pomiędzy datą wysyłki a datą wygaśnięcia nie może być mniejsza niż 1 godzina i większa niż 12 godzin",
			60 => "Grupa kodów o podanej nazwie nie istnieje",
			61 => "Data ważności grupy kodów minęła",
			62 => "Brak wolnych kodów w podanej grupie (wszystkie kody zostały już wykorzystane)",
			65 => "Brak wystarczającej liczby kodów rabatowych dla wysyłki. Liczba niewykorzystanych kodów w grupie musi być co najmniej równa liczbie numerów w wysyłce",
			66 => "W treści wiadomości brak jest znacznika [%kod%] dla wysyłki z parametrem &discount_group (znacznik taki jest wymagany)",
			70 => "Błędny adres CALLBACK w parametrze notify_url",
			72 => "Parametr notify_url może być używany tylko dla odwołań z jednym numerem (nie może być stosowany dla wysyłek masowych)",
			101 => "Niepoprawne lub brak danych autoryzacji. UWAGA! Hasło do API może być inne niż hasło do Panelu Klienta",
			102 => "Nieprawidłowy login lub hasło",
			103 => "Brak punków dla tego użytkownika",
			104 => "Brak szablonu",
			105 => "Błędny adres IP (włączony filtr IP dla interfejsu API)",
			110 => "Usługa (SMS, MMS, VMS lub HLR) nie jest dostępna na danym koncie",
			200 => "Nieudana próba wysłania wiadomości, prosimy ponowić odwołanie",
			201 => "Wewnętrzny błąd systemu (prosimy zgłosić)",
			202 => "Zbyt duża ilość jednoczesnych odwołań do serwisu, wiadomość nie została wysłana (prosimy odwołać się ponownie)",
			300 => "Nieprawidłowa wartość pola points (przy użyciu pola points jest wymagana wartość 1)",
			301 => "Wiadomość o podanym ID nie istnieje lub jest zaplanowana do wysłania w przeciągu najbliższych 60 sekund (nie można usunąć takiej wiadomości)",
			400 => "Nieprawidłowy ID statusu wiadomości",
			999 => "Wewnętrzny błąd systemu (prosimy zgłosić)"
		);
		return @$errors[$error];
	}
  
	public function smsyErrors()
	{
		return array
		(
			''	=> array('status' => 'Brak', 'opis' => 'Nieznany lub brak statusu'),
			401	=> array('status' => 'Nieznaleziona', 'opis' => 'Błędny numer ID lub raport wygasł'),
			402	=> array('status' => 'Przedawniona', 
						'opis' => 'Wiadomość niedostarczona z powodu zbyt długiego czasu niedostępność numeru'),
			403	=> array('status' => 'Wysłana', 
						'opis' => 'Wiadomość została wysłana ale operator nie zwrócił jeszcze raportudoręczenia'),
			404	=> array('status' => 'Dostarczona', 'opis' => 'Wiadomość dotarła do odbiorcy'),
			405	=> array('status' => 'Niedostarczona', 
						'opis' => 'Wiadomość niedostarczona (np.: błędny numer, numer niedostępny)'),
			406	=> array('status' => 'Nieudana', 'opis' => 'Błąd podczas wysyłki wiadomości - prosimy zgłosić'),
			408	=> array('status' => 'Nieznany', 'opis' => 
						'Brak raportu doręczenia dla wiadomości (wiadomość doręczona lub brak możliwości doręczenia)'),
			409	=> array('status' => 'Kolejka', 'opis' => 'Wiadomość czeka w kolejce na wysyłkę'),
			410	=> array('status' => 'Zaakceptowana', 'opis' => 'Wiadomość przyjęta przez operatora'),
			411	=> array('status' => 'Ponawianie', 
						'opis' => 'Wykonana była próba połączenia która nie została odebrana, połączenie zostanie ponowione.')
		);
	}
}
?>