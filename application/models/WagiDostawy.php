<?php
class WagiDostawy extends Zend_Db_Table
{
	public $link, $id, $typ;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujWagaDostawa($dostawa = 0, $waga = 0, $dane)
	{
		$where = 'id_dostawa = '.$dostawa.' and id_waga = '.$waga;
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$sql = $this->select()->where('1')->order(array('koszt'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszJeden($koszt)
	{
		$sql = $this->select()->where('koszt = "'.$koszt.'"')->order('koszt');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function usun()
	{
		$result = $this->delete('id = "'.$this->id.'"');
	}
	function usunWaga($waga = 0)
	{
		$result = $this->delete('id_waga = '.$waga);
	}
	function wypiszID()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function wypiszWagaDostawa($dostawa = 0, $waga = 0)
	{
		$result = $this->fetchRow('id_dostawa = '.$dostawa.' and id_waga = '.$waga);
		return $result;
	}
	function wypiszDostawa($waga = 0)
	{
		$result = $this->fetchAll('id_waga = '.$waga);
		if(count($result) > 0)
		foreach($result as $row)
		{
			$results[$row['id_dostawa']] = $row->toArray();
		}
		return @$results;
	}
}
?>