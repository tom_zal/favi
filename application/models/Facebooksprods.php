<?php
class Facebooksprods extends Zend_Db_Table
{
    public $ID, $login, $choose;
    protected $_name = 'Facebooksprods';
	public $link, $obConfig, $db;
	
	public function __construct()
	{
		parent::__construct();
    }

    function dodaj($dane)
	{
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }
    function edytuj($dane)
	{
        $where = 'id = "'.$this->ID.'"';
        $this->update($dane, $where);
    }
    function usun()
	{
        $where = 'id = "'.$this->ID.'"';
        $this->delete($where);
    }
	function usunStrona($strona = 0)
	{
        $where = 'id_strona = "'.$strona.'"';
        $this->delete($where);
    }
	function usunProdukt($produkt = 0)
	{
        $where = 'id_produkt = "'.$produkt.'"';
        $this->delete($where);
    }
	function wypiszStrona($strona = 0)
	{
        $where = 'id_strona = "'.$strona.'"';
		$result = $this->fetchAll($where);
        return $result;
    }
	function wypiszProdukt($produkt = 0, $sort = 'id_strona')
	{
        $where = 'id_produkt = "'.$produkt.'"';
		$result = $this->fetchAll($where);
		if(!empty($sort))
		{
			if(count($result) > 0)
			foreach($result as $row)
			{
				$rows[$row[$sort]] = $row->toArray();
			}
			return @$rows;
		}
        return $result;
    }
	function wypiszProdukty($produkty = null)
	{
		if(empty($produkty) || !is_array($produkty) || count($produkty) == 0) return null;
        $where = 'id_produkt in ('.implode(',', $produkty).')';
		$result = $this->fetchAll($where);
		if(true)
		{
			if(count($result) > 0)
			foreach($result as $row)
			{
				$rows[$row['id_produkt']][$row['id_strona']] = $row->toArray();
			}
			return @$rows;
		}
        return $result;
    }
	function wypiszPost($strona = 0, $produkt = 0)
	{
        $where = 'id_strona = "'.$strona.'" and id_produkt = "'.$produkt.'"';
		$result = $this->fetchRow($where);
        return $result;
    }
    function wypisz()
	{
        $where = 'id = "'.$this->ID.'"';
        $result = $this->fetchRow($where);
        //$res = $result->toArray();
        return $result;
    }
    function wypiszAll()
	{
        $result = $this->fetchAll();
        return $result;
    }	
}
?>