<?php
class Parametry extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->module = $module;
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$sql = $this->select()->where('lang = "'.$this->lang.'"')->order(array('pozycja asc', 'nazwa asc'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function get($pole = 'id')
	{
		$sql = $this->select()->where('lang = "'.$this->lang.'"')->order(array('pozycja asc', 'nazwa asc'));
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i][$pole]] = $result[$i]->toArray();
		}
		return @$results;
	}
	function wypiszWhereNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa like "%'.$nazwa.'%"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa = "'.$nazwa.'"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	
	function wypiszProdukt($produkt = 0)
	{
		$where = 'id_prod = '.$produkt;
		$result = $this->fetchAll($where);
		return $result;
	}
	function wypiszForProdukt($produkt = 0, $widoczny = false)
	{
		$where = 'id_prod = '.$produkt.' and id_atryb = 0';
		if($widoczny) $where .= ' and widoczny = 1';
		$result = $this->fetchAll($where, array('pozycja asc', 'nazwa asc'));
		return $result;
	}
	function wypiszMaxParamForProdukt($id = 0)
	{
		$select = $this->db->select()->from(array('p'=>'Parametry'),array('max(id_param)'))->where('id_prod='.$id);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return isset($result[0]['max(id_param)']) ? intval($result[0]['max(id_param)']) : 0;
    }
	function wypiszMaxAtrybForProduktParam($id = 0, $par = 0)
	{
		$where = 'id_prod = '.$id.' and id_param = '.$par;
		$select = $this->db->select()->from(array('p'=>'Parametry'),array('max(id_atryb)'))->where($where);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return isset($result[0]['max(id_atryb)']) ? intval($result[0]['max(id_atryb)']) : 0;
    }
	function wypiszForProduktAll($produkt = 0, $widoczny = false)
	{
		$where = 'id_prod = '.$produkt.' and id_atryb > 0';
		if($widoczny) $where .= ' and widoczny = 1';
		$result = $this->fetchAll($where, array('pozycja asc', 'nazwa asc'));
		for($i=0; $i<count($result); $i++)
		{
			$results[$result[$i]->id_param][] = $result[$i];		
		}		
		return @$results;		
	}
	
	function usunID($id = 0)
	{
		$result = $this->delete('id = '.$this->id);
	}
	function usunParam($id = 0)
	{
		$result = $this->delete('id_prod = '.$id.' and id_param = '.$this->id);
	}
	function wypiszPojedynczego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function wypiszParam()
	{
		$select = $this->db->select()
			->from(array('p' => 'Parametry'), array('*'))->joinleft(array('pp' => 'Parametry'),
			'p.id_param = pp.id_param and pp.id_atryb = 0 and p.lang = pp.lang', array('param' => 'nazwa'))
			->where('p.widoczny = 1 and p.id = '.$this->id)->group('p.id');
		//echo $select;
		$result = $this->db->fetchRow($select);
		return $result;
	}
	
	function wypiszDlaKategorii($kateg = 0)
	{
		$select = $this->db->select()->from(array('p' => 'Produkty'), array(''))
			->join(array('pp' => 'Parametry'), 'p.id = pp.id_prod', array('*'))
			->joinleft(array('kp' => 'Katprod'), 'p.id = kp.id_prod', array(''))
			->where('p.widoczny = 1 and kp.id_kat = '.$kateg)->group('pp.id')->order('pp.nazwa asc');
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszProduktyDlaKategorii($kateg = 0)
	{
		$select = $this->db->select()->from(array('p' => 'Produkty'), 
			array('group_concat(distinct p.id separator ",") as ids'))
			->join(array('pp' => 'Parametry'), 'p.id = pp.id_prod', array(''))
			->joinleft(array('kp' => 'Katprod'), 'p.id = kp.id_prod', array(''))
			->where('p.widoczny = 1 and kp.id_kat = '.$kateg)->order('p.nazwa asc');
		//echo $select;
		$result = $this->db->fetchRow($select);
		return @strval($result['ids']);
	}
}
?>