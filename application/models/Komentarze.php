<?php
class Komentarze extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }	
    
    function wypisz()
	{
        $result = $this->fetchAll();
        return $result;
    }
    function wypiszTyp($typ)
	{
        $where = 'typ = "'.$typ.'"';
        $result = $this->fetchAll($where);
        return $result;
    }
    function dodaj($dane)
	{        
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }
    function edytuj($dane, $id)
	{
        $where = 'id = "' . $id. '"';
        $this->update($dane, $where);
    }
    function pojedyncza($id)
	{
        $where = 'id = "'.$id.'"';
        $print = $this->fetchRow($where);
        return $print;
    }
    function usun($id)
    {
        $where = 'id = "'.$id.'"';
        $print = $this->delete($where);        
    }
}
?>