<?php
class Produktypowiazania extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
	function getPowiazaniaForExport($ids, $rodzaj = 'produkt')
	{
		if(empty($ids) || count($ids) == 0) return null;
        $select = $this->db->select()->from(array('pp' => 'Produktypowiazania'), array('id_prod', 
			new Zend_Db_Expr('GROUP_CONCAT(DISTINCT powiazany ORDER BY powiazany ASC SEPARATOR "|") as powiaz')))
			->where('id_prod in('.implode(',',$ids).') and pp.rodzaj = "'.$rodzaj.'"')->group('id_prod')->order(array('id_prod asc'));
		//echo $select;die();
		$result = $this->db->fetchAll($select);
        for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i]['id_prod']] = $result[$i]['powiaz'];
		}
		return @$results;
    }
		
	function dodaj($dane)
	{
		//$dane['rodzaj'] = 'produkt';
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function wypiszAll()
	{
		$result = $this->fetchAll();
		return $result;
	}
	function wypiszID()
	{
		$sql = $this->select()->where('id = '.$this->id);
		$result = $this->fetchRow($sql);
		return $result;
	}
	function wypiszProd($id, $rodzaj = 'produkt')
	{
		$sql = $this->select()->where('id_prod = '.$id.' and rodzaj = "'.$rodzaj.'"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszGratisy($ids = null, $implode = false)
	{
		if(empty($ids) || !is_array($ids) || count($ids) == 0) return null;
		$select = $this->db->select()->from(array('p' => 'Partnerzy'), 
			$implode ? 'GROUP_CONCAT(distinct p.id SEPARATOR ",") as gratisy' : array('nazwa', 'logo', 'opis'))
			->join(array('pp' => 'Produktypowiazania'), 'p.id = pp.powiazany', $implode ? '' : array('p.id'))
			->where('pp.id_prod in ('.implode(',',$ids).') and pp.rodzaj = "gratisy" and p.typ = "gratisy"');
		if(!$implode) $select->group('p.id')->order('nazwa asc')->limit(9999);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszProdukt($id = 0, $rodzaj = 'produkt', $ids = null)
	{
		$where = 'pp.rodzaj = "'.$rodzaj.'"';
		if(!empty($ids)) $where .= ' and p.id in ('.$ids.')';
		else if($id >= 0) $where .= ' and pp.id_prod = '.$id;
		
		if($rodzaj == 'gratisy')
		$select = $this->db->select()->from(array('p' => 'Partnerzy'), array('nazwa', 'logo', 'opis'))
			->join(array('pp' => 'Produktypowiazania'), 'p.id = pp.powiazany', array('p.id'))
			->where($where.' and p.typ = "gratisy"')->order('nazwa asc')->limit(9999);
		else
		$select = $this->db->select()->from(array('p' => 'Produkty'), array('p.nazwa'))
			->join(array('pp' => 'Produktypowiazania'), 'p.id = pp.powiazany', array('id'))
			->where($where)->order('nazwa asc')->limit(9999);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszProdukty($id = 0, $ile = 3, $rodzaj = 'produkt')
	{
		$whereGal = 'p.id = g.wlasciciel and g.typ = "oferta" and g.glowne = "T"';
		$whereGal.= ' and g.lang = "'.$this->lang.'" and g.wyswietl = "1"';
		$select = $this->db->select()->from(array('p' => 'Produkty'), array('*'))
			->join(array('pp' => 'Produktypowiazania'), 'p.id = pp.powiazany', array(''))
			->joinleft(array('g' => 'Galeria'), $whereGal, array('img', 'img_opis' => 'nazwa'))
			->where('pp.id_prod = '.$id.' and pp.rodzaj = "'.$rodzaj.'"')->order('p.nazwa asc')->limit($ile);
		//echo $select;		
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function usun()
	{
		$result = $this->delete('id = '.$this->id);
	}
	function usunProdukt($id)
	{
		$result = $this->delete('id_prod = '.$id);	
	}
	function usunProduktyIds($ids)
	{
		$result = $this->delete('id_prod in ('.$ids.')');
	}
}
?>