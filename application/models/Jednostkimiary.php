<?php
class Jednostkimiary extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{
		$dane['lang'] = $this->lang;
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujLang($dane)
	{
		$where = 'lang = "'.$this->lang.'"';
		$this->update($dane, $where);
	}
	function getDomyslna()
	{
		$sql = $this->select()->where('lang = "'.$this->lang.'" and glowna = "1"');
		$result = $this->fetchRow($sql);
		return $result;
	}
	function wypisz()
	{
		$sql = $this->select()->where('lang = "'.$this->lang.'"')->order('nazwa');
		$result = $this->fetchAll($sql);
		return $result;
	}

	function wypiszLikeNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa like "%'.$nazwa.'%"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa = "'.$nazwa.'"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	
	function getJednMiary($pole = 'id')
	{
		$sql = $this->select()->where('lang = "'.$this->lang.'"')->order('nazwa');
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i][$pole]] = $result[$i]->toArray();
		}
		return @$results;
	}
	
	function usun()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	function wypiszPojedynczego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
}
?>