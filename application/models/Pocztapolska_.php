<?php

class Pocztapolska extends Zend_Db_Table
{
	function updateData($array)
	{
		$where = 'id = 1';
		$this->update($array, $where);
	}	
	function showData()
	{
		$result = $this->fetchAll();		
		return $result;
	}	
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujID($adminID, $dane)
	{
		$where = 'id = '.$adminID;
		$this->update($dane, $where);
	}
	function wypiszJeden()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function wypiszOne($admin = 1)
	{
		$sql = $this->select()->where('id = '.$admin);
		$result = $this->fetchRow($sql);
		if($result == null)
		{
			$dane['id'] = $admin;
			$this->dodaj($dane);
			$result = $this->fetchRow($sql);
		}
		return $result->toArray();
	}
}
?>