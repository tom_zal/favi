<?php
class Dostepnosc extends Zend_Db_Table
{
	public $link, $id, $typ;
		
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$sql = $this->select()->where('1')->order(array('min', 'max'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszWaga($ilosc = 0)
	{
		$sql = $this->select()->where('min <= '.$ilosc.' and max >= '.$ilosc)->order(array('min', 'max'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszJeden($nazwa)
	{
		$sql = $this->select()->where('nazwa = "'.$nazwa.'"')->order('min');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function usun()
	{
		$result = $this->delete('id = '.$this->id);			
	}		
	function wypiszID()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function wypiszMax()
	{
		$select = $this->db->select()->from(array('d' => 'Dostepnosc'), array('max(max)'))->where(1);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return @floatval($result[0]['max(max)']); 
	}
}
?>