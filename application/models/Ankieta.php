<?php
class Ankieta extends Zend_Db_Table 
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = "'.$this->id.'"';
		$this->update($dane, $where);
	}
	function usun()
	{
		$result = $this->delete('id = "'.$this->id.'" AND id <> 0');
	}
	function wypiszWszystko()
	{
		$where = 'id  > 0';
		$order = 'nazwa ASC';
		$result = $this->fetchAll($where, $order);
		return $result;
	}
	function wypiszPojedynczy()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function wypiszWidoczny()
	{
		$result = $this->fetchRow('widoczny = "1" ');
		return $result;
	}
	function wypiszWidocznyCache($cache=false, $caching=false)
	{
		if($cache) $this->result = $cache->load('cache_'.$this->cachename);else $this->result = false;
		if($this->result === false || !$caching)
		{
			$sql = $this->select()
					   ->from(array('ao' => 'Ankietaodpowiedzi'), new Zend_Db_Expr('GROUP_CONCAT(DISTINCT ao.nazwaod,"*|*",ao.pozycja,"*|*", ao.id SEPARATOR "-|-")'))
					   ->setIntegrityCheck(false)
					   ->where('ao.idan = a.id')
					   ->order('ao.pozycja desc');

			$select = $this->select()
						   ->setIntegrityCheck(false)
						   ->from(array('a' => 'Ankieta'), array('*', 'odp'=>new Zend_Db_Expr('('.$sql.')')))
						   ->where('a.widoczny = 1');
			//echo $sql;echo $select;

			$this->result = $this->fetchRow($select);//->toArray();
			if($cache) $cache->save($this->result, 'cache_'.$this->cachename);
		}
		return $this->result;
	}
	function ustawWidoczny()
	{
		$dane = array('widoczny' => 0);
		$where = 'id <>"0"';
		$this->update($dane, $where);

		$dane = array('widoczny' => 1);
		$where = 'id = "'.$this->id.'"';
		$this->update($dane, $where);
	}
	function wypiszUstawienia()
	{
		$result = $this->fetchRow('id = 0');
		return $result;
	}
	function wypiszUstawieniaCache($cache=false, $caching=false)
	{
		if($cache) $this->result = $cache->load('cache_'.$this->cachename);else $this->result = false;
		if($this->result === false || !$caching)
		{
			$this->result = $this->fetchRow('id = 0');
			if($cache) $cache->save($this->result, 'cache_'.$this->cachename);
		}
		return $this->result;
	}
}
?>