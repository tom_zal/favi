<?php
class Rabaty extends Zend_Db_Table
{	
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{	
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		return $this->update($dane, $where);
	}
	function wypiszRabaty($ids, $grupuj = false)
	{
		if(!$this->common->isArray($ids, true)) return null;
		
		$sql = $this->db->select()
			->from(array('r' => 'Rabaty'), array('*', 'typ_rab' => 'if(typ_rab = "", "%", typ_rab)'))		    
			->joinLeft(array('pg' => 'Produktygrupy'), 'r.id_prod_grupa = pg.id', array('nazwa_prod_grupa' => 'nazwa'))
			->joinLeft(array('p' => 'Produkty'), 'r.id_prod = p.id', array('nazwa_produkt' => 'nazwa'))
			->joinLeft(array('po' => 'Producent'), 'r.producent_id = po.id', array('nazwa_producent' => 'nazwa'))
			->joinLeft(array('k' => 'Kategorie'), 'r.kategoria_rab = k.id', array('nazwa_kategoria' => 'nazwa'))
		    ->where('r.id_user in ('.implode(',', $ids).')')
			->order(array('r.id_user asc', 'p.nazwa asc', 'pg.nazwa asc', 'po.nazwa asc', 'k.nazwa asc'));
		
		$result = $this->db->fetchAll($sql);
		for($i = 0; $i < count($result); $i++)
		{
			if($grupuj)
			{
				$typ = 'klient';
				if($result[$i]['id_prod_grupa'] > 0) $typ = 'prod_grupa';
				if($result[$i]['id_prod'] > 0) $typ = 'produkt';
				if($result[$i]['kategoria_rab'] > 0) $typ = 'kategoria';
				if($result[$i]['producent_id'] > 0) $typ = 'producent';
				$results[$result[$i]['id_user']][$typ][] = $result[$i];
			}
			else $results[$result[$i]['id_user']][] = $result[$i];
		}
		//var_dump($results);
		return @$results;
	}
	function wypiszProdukty()
	{
		$select = $this->select()->setIntegrityCheck(false)
			->from(array('r' => 'Rabaty'), array('*', 'typ_rab' => 'if(typ_rab = "", "%", typ_rab)'))
		    ->joinLeft(array('p' => 'Produkty'), 'r.id_prod = p.id', 
			array('nazwa' => 'if(nazwa is null, "Wszystkie", nazwa)'))
		    ->where('r.id_user = '.$this->id.' and r.id_prod > 0');
		
		$result = $this->fetchAll($select)->toArray();
		return $result;
	}
	function wypiszProduktyGrupa()
	{
		$select = $this->select()->setIntegrityCheck(false)
			->from(array('r' => 'Rabaty'), array('*', 'typ_rab' => 'if(typ_rab = "", "%", typ_rab)'))
		    ->joinLeft(array('p' => 'Produktygrupy'), 'r.id_prod_grupa = p.id', 
			array('nazwa' => 'if(nazwa is null, "Wszystkie", nazwa)'))
		    ->where('r.id_user = '.$this->id.' and r.id_prod_grupa > 0');
		
		$result = $this->fetchAll($select)->toArray();
		return $result;
	}
	function wypiszProducent()
	{
		$select = $this->select()->setIntegrityCheck(false)
			->from(array('r' => 'Rabaty'), array('*', 'typ_rab' => 'if(typ_rab = "", "%", typ_rab)'))
		    ->joinLeft(array('p' => 'Producent'), 'r.producent_id = p.id', 
			array('nazwa' => 'if(nazwa is null, "Wszystkie", nazwa)'))
		    ->where('r.id_user = '.$this->id.' and r.producent_id > 0');
			
		$result = $this->fetchAll($select)->toArray();
		return $result;
	}
	function wypiszKategorie()
	{
		$select = $this->select()->setIntegrityCheck(false)
			->from(array('r' => 'Rabaty'), array('*', 'typ_rab' => 'if(typ_rab = "", "%", typ_rab)'))
		    ->joinLeft(array('k' => 'Kategorie'), 'r.kategoria_rab = k.id', 
		    array('nazwa' => 'if(nazwa is null, "Wszystkie", nazwa)'))
		    ->where('r.id_user = '.$this->id.' and r.kategoria_rab > 0');
		
		$result = $this->fetchAll($select)->toArray();
		return $result;
	}
	function usunID($id = 0)
	{
		return $this->delete('id = '.$id);			
	}
	function usun($id)
	{
		$result = $this->delete('id = '.$id.' AND id_user = '.$this->id);			
	}
	function wypiszWiersz()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function znajdzWiersz($dane)
	{
		$result = $this->fetchAll('wartosc_rab LIKE "'.$dane.'"');
		return $result;
	}
	function sprawdzKategorie($id_kat = 0, $id_user = 0)
	{
		$result = $this->fetchAll('id_user = '.intval($id_user).' AND kategoria_rab = '.$id_kat);
		return $result;
	}
	function sprawdzProduktGrupe($id_prod_grupa = 0, $id_user = 0)
	{
		$result = $this->fetchAll('id_user = '.intval($id_user).' AND id_prod_grupa = '.$id_prod_grupa);
		return $result;
	}
	function sprawdzProdukt($id_prod = 0, $id_user = 0)
	{
		$result = $this->fetchAll('id_user = '.intval($id_user).' AND id_prod = '.$id_prod);
		return $result;
	}
	function sprawdzProducenta($producent_id = 0, $id_user = 0)
	{
		$result = $this->fetchAll('id_user = '.intval($id_user).' AND producent_id = '.$producent_id);
		return $result;
	}
	function getUser($id_user = 0)
	{
		$result = $this->fetchAll('id_user = '.$id_user);
		return $result;
	}
	function getUserSort($id_user = 0, $typ = null)
	{
		if(empty($typ))
		{
			$result = $this->fetchAll('id_user = '.intval($id_user).'');
			for($i = 0; $i < count($result); $i++)
			{
				if($result[$i]['kategoria_rab'] > 0)
				$rabaty['kategorie'][$result[$i]['kategoria_rab']] = $result[$i]->toArray();
				if($result[$i]['id_prod_grupa'] > 0)
				$rabaty['produktyGrupy'][$result[$i]['id_prod_grupa']] = $result[$i]->toArray();
				if($result[$i]['id_prod'] > 0)
				$rabaty['produkty'][$result[$i]['id_prod']] = $result[$i]->toArray();
				if($result[$i]['producent_id'] > 0)
				$rabaty['producent'][$result[$i]['producent_id']] = $result[$i]->toArray();
			}
			return @$rabaty;
		}
		else
		{
			$result = $this->fetchAll('id_user = '.intval($id_user).' and '.$typ.' > 0');
			for($i = 0; $i < count($result); $i++)
			{
				$results[$result[$i][$typ]] = $result[$i]->toArray();
			}
			return @$results;
		}
	}
	
	function rabat($rabat)
	{
		$this->_name = 'Produkty';
		$where = 'id = '.$this->id;
		$this->update($rabat, $where);
	}
	
	function usunWszystkie($rabat)
	{
		$this->_name = 'Produkty';
		$this->update($rabat);			
	}
	
	function getRabaty($pole = 'id', $cache = true)
	{
		if(!$cache) $this->common->cacheRemove(null, 'Rabaty');
		
		$sql = $this->db->select()->from(array('r' => 'Rabaty'), array('*', $pole));
		$result = $this->db->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i][$pole]] = $result[$i];
		}
		return @$results;
	}
}	
?>