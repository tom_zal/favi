<?php
class Import extends Zend_Db_Table
{
    public $tabelaKontrahenci, $tabelaProdukty;
    private $db;
	public $notAllowed; 

    public function __construct($tryb = 'panel', $rodzaj = 'produkty', $typ = '', $module = 'admin', $plik = 'xls')
	{
		parent::__construct();
        $this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		$this->ustawienia = $this->common->getUstawienia();
		$this->ceny = $this->common->getCeny();
		$this->polaRozmiary = array('id_ext','oznaczenie','dostepny','dostepnosc','opis','skrot');
		
		$this->bledy = '';
		$this->tryb = $this->dane['tryb'] = $tryb;
		$this->rodzaj = $this->dane['rodzaj'] = $rodzaj;
		$this->typ = $this->dane['typ'] = $typ;
		$this->plik = $plik;
		$this->notAllowed = array('allegrokategorie', 'allegropola', 'ebat_cat');
		$this->czyPole = $this->getCzyPole();
		$this->opisy = $this->getOpisy();
		$this->property = $this->getProperty();
		$this->ints = $this->getInts();
		$this->floats = $this->getFloats();
		$this->strings = $this->getStrings();
		$this->leftAligns = $this->getLeftAligns();
		$this->rightAligns = $this->getRightAligns();
		
		$this->pole = 'nazwa';
		if($tryb == 'panel') $this->pole = 'id';
		if($tryb == 'hermes') $this->pole = 'oznaczenie';
		if($tryb == 'optima') $this->pole = 'oznaczenie';
		if($tryb == 'subiekt') $this->pole = 'oznaczenie';
		if($tryb == 'cdn_xl') $this->pole = 'oznaczenie';
		if($tryb == 'clickshop') $this->pole = 'nazwa';
		if($tryb == 'fpp' && $rodzaj == 'produkty') $this->pole = 'oznaczenie';
		//if($tryb=='fpp'&&$rodzaj=='produkty'&&typ=='inwentarz')$this->pole='concat(oznaczenie,nazwa,cena_brutto)';
		//if($tryb == 'fpp' && $rodzaj == 'klienci') $this->pole = 'concat(nazwa_firmy,nazwa,ulica)';
		if($tryb == 'fpp' && $rodzaj == 'klienci') $this->pole = 'id_ext';
		if($tryb == 'fpp' && $rodzaj == 'produktygrupy') $this->pole = 'nazwa';
		if($tryb == 'fpp' && $rodzaj == 'rabaty') $this->pole = 'concat(id_user,id_prod,id_prod_grupa,flaga)';
		
		$this->delim = ';';
		if($tryb == 'clickshop') $this->delim = '\t';
		if($tryb == 'fpp') $this->delim = ',';
		
		$this->poleID = 0;
		if($tryb == 'clickshop') $this->poleID = 8;
		if($tryb == 'fpp' && $rodzaj == 'produkty' && $typ == 'cennik') $this->poleID = 1;
		if($tryb == 'fpp' && $rodzaj == 'produkty' && $typ == 'inwentarz') $this->poleID = 1;
		//if($tryb == 'fpp' && $rodzaj == 'produkty' && $typ == 'inwentarz') $this->poleID = array(1, 2, 5);
		//if($tryb == 'fpp' && $rodzaj == 'klienci') $this->poleID = array(0, 1, 2);
		if($tryb == 'fpp' && $rodzaj == 'klienci') $this->poleID = 14;
		if($tryb == 'fpp' && $rodzaj == 'produktygrupy') $this->poleID = 0;
		if($tryb == 'fpp' && $rodzaj == 'rabaty') $this->poleID = array(0, 1, 2);
		
		$this->columnCount = 0;
		if($tryb == 'clickshop') $this->columnCount = 0;
		if($tryb == 'fpp' && $rodzaj == 'produkty' && $typ == 'cennik') $this->columnCount = 55;//54
		if($tryb == 'fpp' && $rodzaj == 'produkty' && $typ == 'inwentarz') $this->columnCount = 9;
		if($tryb == 'fpp' && $rodzaj == 'klienci') $this->columnCount = 51;//45
		if($tryb == 'fpp' && $rodzaj == 'produktygrupy') $this->columnCount = 2;
		if($tryb == 'fpp' && $rodzaj == 'rabaty') $this->columnCount = 9;
		
		$this->encoding = 'UTF-8';
		if($tryb == 'fpp') $this->encoding = 'Windows-1250';
		
		$this->arkuszNR = 0;
		
		$this->headersRow = 1;
		if($tryb == 'hermes') $this->headersRow = 2;
		if($tryb == 'cdn_xl') $this->headersRow = 2;
		if($tryb == 'fpp') $this->headersRow = 1;
		if($tryb == 'fpp' && $rodzaj == 'produkty' && $typ == 'cennik') $this->headersRow = 0;
		if($tryb == 'fpp' && $rodzaj == 'produktygrupy') $this->headersRow = 1;
		if($tryb == 'fpp' && $rodzaj == 'rabaty') $this->headersRow = 0;
		
		$this->dataColumn = 0;
		if($tryb == 'panel') $this->dataColumn = 1;
		
		$this->checkColumnCount = true;
		if($tryb == 'hermes') $this->checkColumnCount = false;
		if($tryb == 'fpp' && $rodzaj == 'produktygrupy') $this->checkColumnCount = false;
		
		$this->fixedColumnPosition = true;
		if($tryb == 'hermes') $this->fixedColumnPosition = true;		
    }
	
	function dodajWpis($id, $akcja, $old = null, $new = null, $info = null)
	{
		$this->dane['id_ext'] = $id;
		$this->dane['akcja'] = $akcja;
		if(is_array($info))
		{
			if(is_array($old)) $old = array_merge($old, $info);
			if(is_array($new)) $new = array_merge($new, $info);
		}
		$this->dane['old'] = $old != null ? @serialize($old) : '';
		$this->dane['new'] = $new != null ? @serialize($new) : '';		
		return $this->dodaj($this->dane);
	}
	function dodaj($dane)
	{
		$this->insert($dane);
		return $this->getAdapter()->lastInsertId();
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		return $this->update($dane, $where);
	}
	function usun()
	{
		return $this->delete('id = '.$this->id);
	}
	function wyczysc()
	{
		return $this->delete('1');
	}
	function wypisz()
	{
		return $this->fetchRow('id = '.$this->id);
	}
	
	function wypiszKontr($ilosc, $od, $ile, $sortowanie, $szukanie) 
	{
		$od = $od * $ile;
		$limit = $ile;
		if($ilosc) { $od = 0; $limit = 9999; }
		$where = '1';
		
		if($szukanie !== null)
		{
			if($szukanie->tryb != '') $where .= ' and tryb = "'.$szukanie->tryb.'"';
			if($szukanie->rodzaj != '') $where .= ' and rodzaj = "'.$szukanie->rodzaj.'"';
			if($szukanie->typ != '') $where .= ' and typ = "'.$szukanie->typ.'"';
			if($szukanie->akcja != 'all') $where .= ' and akcja = "'.$szukanie->akcja.'"';
			if($szukanie->ostatni) $where .= ' and data = (select max(data) from Import where '.$where.')';			
			if(false && $szukanie->data != '')
			{
				$where .= ' and czas >= "'.$szukanie->dataOd.' 00:00:00"';
				$where .= ' and czas <= "'.$szukanie->dataDo.' 23:59:59"';
			}
		}
		
		if($sortowanie == null) 
		{
			$sortowanie = (object)array();
			$sortowanie->sort = 'czas';
			$sortowanie->order = 'desc';
		}
		$order = array('data desc', $sortowanie->sort.' '.$sortowanie->order);
		
        $sql = $this->db->select()->from(array('i'=>'Import'),array($ilosc?'count(distinct i.id)':'*'));
			//$sql->joinleft(array('x' => 'XXX'),'x.id = i.id_ext',$ilosc?'':array(''));
		$sql->where($where);
		if(!$ilosc) $sql->group('i.id');
		if(!$ilosc) $sql->order($order);
		$sql->limit($limit, $od);
		
		//if($ilosc){echo $sql;die();return null;}
        $result = $this->db->fetchAll($sql);
        return $ilosc ? @intval($result[0]['count(distinct i.id)']) : $result;
    }
	
	function wypiszIDS($tryb = '', $rodzaj = '', $typ = '', $akcja = 'all') 
	{
		$where = '1';		
		if(true)
		{
			if($tryb != '') $where .= ' and tryb = "'.$tryb.'"';
			if($rodzaj != '') $where .= ' and rodzaj = "'.$rodzaj.'"';
			if($typ != '') $where .= ' and typ = "'.$typ.'"';
			if($akcja != 'all') $where .= ' and akcja = "'.$akcja.'"';
			if(true) $where .= ' and data = (select max(data) from Import where '.$where.')';			
		}
		$this->db->query('SET group_concat_max_len=1000000000');
        $sql = $this->db->select()->from(array('i'=>'Import'),array('GROUP_CONCAT(i.id_ext SEPARATOR ",") as ids'));
			//$sql->joinleft(array('x' => 'XXX'),'x.id = i.id_ext',$ilosc?'':array(''));
		$sql->where($where);
        $result = $this->db->fetchAll($sql);
		//echo($result[0]['ids']);die();
        return $result;
    }
	
	public function getInts($tryb = null)
    {
		if($tryb == null) $tryb = $this->tryb;
		$ints = array('id', 'id_ext', 'dostepnosc');
		//if($tryb == 'panel') return $ints;
		return $ints;
	}
	public function getFloats($tryb = null)
    {
		if($tryb == null) $tryb = $this->tryb;
		$floats = array('vat', 'rabat', 'waga');
		foreach($this->ceny as $nazwa => $label)
		if(strpos($this->ustawienia['ceny'], $nazwa) !== false)
		$floats[] = $nazwa;
		//if($tryb == 'panel') return $floats;
		return $floats;
	}
	public function getStrings($tryb = null)
    {
		if($tryb == null) $tryb = $this->tryb;
		$strings = array('nazwa', 'oznaczenie', 'producent', 'skrot', 'tekst', 'id_ext');
		$strings[] = 'kategorie';
		$strings[] = 'kolory';
		$strings[] = 'platnosci';
		$strings[] = 'powiazania';
		$strings[] = 'pkwiu';
		$strings[] = 'sww';
		if($tryb == 'hermes') $strings[] = 'jedn_miary';
		return $strings;
	}
	public function getLeftAligns($tryb = null)
    {
		if($tryb == null) $tryb = $this->tryb;
		$leftAligns = $this->strings;		
		if($tryb == 'hermes') $leftAligns[] = 'vat';
		return $leftAligns;
	}
	public function getRightAligns($tryb = null)
    {
		if($tryb == null) $tryb = $this->tryb;
		$rightAligns = array();
		foreach($this->ceny as $nazwa => $label)
		if(strpos($this->ustawienia['ceny'], $nazwa) !== false)
		$rightAligns[] = $nazwa;
		if($tryb == 'hermes') $rightAligns[] = 'id_ext';
		if($tryb == 'hermes') $rightAligns[] = 'dostepnosc';
		if($tryb == 'optima') $rightAligns[] = 'dostepnosc';
		if($tryb == 'cdn_xl') $rightAligns[] = 'dostepnosc';
		return $rightAligns;
	}
	
	public function getProperty($tryb = null, $rodzaj = null, $typ = null)
    {
		if($tryb == null) $tryb = $this->tryb;
		if($rodzaj == null) $rodzaj = $this->rodzaj;
		if($typ == null) $typ = $this->typ;
		
		if($rodzaj == 'produkty')
		{
			$property['ean'] = 'EAN';
			if($tryb == 'panel') return $property;
			
			return $property;
		}
	}
	
	public function getOpisy($tryb = null, $rodzaj = null, $typ = null)
    {
		if($tryb == null) $tryb = $this->tryb;
		if($rodzaj == null) $rodzaj = $this->rodzaj;
		if($typ == null) $typ = $this->typ;
		
		if($rodzaj == 'produkty')
		{
			$opisy['id'] = 'ID';
			$opisy['nazwa'] = 'nazwa';
			$opisy['oznaczenie'] = 'kod';
			$opisy['producent'] = 'producent';
			$opisy['vat'] = 'vat';
			
			foreach($this->ceny as $nazwa => $label)
			if(strpos($this->ustawienia['ceny'], $nazwa) !== false)
			$opisy[$nazwa] = $label;
			
			$opisy['widoczny'] = 'widoczność';
			$opisy['dostepny'] = 'dostępność';
			$opisy['promocja'] = 'promocja';
			$opisy['nowosc'] = 'nowość';
			$opisy['glowna'] = 'główna';
			$opisy['bestseller'] = 'polecane';
			$opisy['produkt_dnia'] = 'produkt dnia';
			$opisy['wyprzedaz'] = 'wyprzedaż';
			$opisy['specjalne'] = 'specjalne';
			$opisy['skrot'] = 'opis skrócony';
			$opisy['tekst'] = 'opis';
			//$opisy['rozmiarowka'] = 'rozmiarówka';
			$opisy['dostepnosc'] = 'stan magazynu';
			$opisy['jedn_miary'] = 'jednostka miary';
			$opisy['jedn_miary_typ'] = 'jedn. miary (typ)';
			$opisy['rabat'] = 'rabat';
			$opisy['waga'] = 'waga';
			$opisy['gabaryt'] = 'gabaryt';
			$opisy['pozycja'] = 'pozycja';
			//$opisy['trybCena'] = 'ceny';
			$opisy['pozycja'] = 'pozycja';
			$opisy['pkwiu'] = 'PKWiU';
			$opisy['sww'] = 'SWW/KU';
			$opisy['id_ext'] = 'ID zewn.';
			
			$opisy['kategorie'] = 'kategorie';
			$opisy['kolory'] = 'kolory';
			$opisy['platnosci'] = 'płatności';
			$opisy['powiazania'] = 'powiązania';		
			
			if($tryb == 'panel') return $opisy;
			
			if($typ == 1) // detex
			{
				$hermes['oznaczenie'] = 'INDEKS';
				$hermes['nazwa'] = 'ARTYKUL';
				$hermes['id_ext'] = 'ID';
				$hermes['dostepnosc'] = 'ILOSC';
				$hermes['cena_netto'] = 'CENA_NETTO';
				$hermes['cena_brutto'] = 'CEN_BRUTTO';
				$hermes['vat'] = 'VAT';
				$hermes['jedn_miary'] = 'JM';
				$hermes['waga'] = 'WAGA_JM';
				$hermes['pkwiu'] = 'PKWIU';
				$hermes['sww'] = 'SWW';
				//$hermes['ean'] = 'KRESKI';
				//$hermes['producent'] = 'PRODUCENT';
			}
			else
			{
				$hermes['oznaczenie'] = 'INDEKS';
				$hermes['nazwa'] = 'ARTYKUL';
				//$hermes['ARTYKUL'] = '';
				$hermes['ean'] = 'KRESKI';
				$hermes['id_ext'] = 'ID';
				$hermes['dostepnosc'] = 'ILOSC';
				$hermes['cena_brutto_hurt'] = 'CEN_BRUTTO';
				$hermes["C_BRUTTO2"] = '';
				$hermes["C_BRUTTO3"] = '';
				$hermes['cena_netto_hurt'] = 'CENA_NETTO';
				$hermes["WART_NETTO"] = '';
				$hermes["WAR_BRUTTO"] = '';
				$hermes["WART_ZAKUP"] = '';
				$hermes['vat'] = 'VAT';
				$hermes['jedn_miary'] = 'JM';
				$hermes["CENA_ZAKUP"] = '';
				$hermes["REZERWACJA"] = '';
				$hermes["STANMIN"] = '';
				$hermes['sww'] = 'SWW';
				$hermes['pkwiu'] = 'PKWIU';
				$hermes['DATA_ZAK'] = '';
				$hermes['DATA_SPR'] = '';
				$hermes['CENA_MIN'] = '';
				$hermes['LOGO'] = '';
				$hermes['LOKAL'] = '';
				$hermes['STATUS'] = '';
				$hermes['UWAGI'] = '';
				$hermes['TYP'] = '';
				$hermes['TERM_WAZN'] = '';
				$hermes['TERMDNI'] = '';
				$hermes['PRODUCENT'] = '';
				$hermes['WAGA_JM'] = '';
				$hermes['OST_ZAKUP'] = '';
				$hermes['OST_KALK'] = '';
				$hermes['C_NETTO1'] = '';
				$hermes['C_BRUTTO1'] = '';
				$hermes['C_NETTO2'] = '';
				$hermes['C_NETTO3'] = '';
				$hermes['C_NETTO4'] = '';
				$hermes['C_BRUTTO4'] = '';
				$hermes['C_NETTO5'] = '';
				$hermes['C_BRUTTO5'] = '';
				$hermes['cena_netto'] = 'C_NETTO6';
				$hermes['cena_brutto'] = 'C_BRUTTO6';
				//$hermes['waga'] = 'WAGA_JM';
				//$hermes['producent'] = 'PRODUCENT';
			}
			
			if($tryb == 'hermes') return $hermes;

			$optima['oznaczenie'] = "Kod";
			$optima['nazwa'] = "Nazwa";
			$optima['cena_brutto'] = "Cena";
			$optima['dostepnosc'] = "Ilość dostępna";
			$optima["Cena-PLN"] = '';
			$optima["Wartość-PLN"] = '';
			for($i=1;$i<=0;$i++)
			$optima[$i] = null;
			
			if($tryb == 'optima') return $optima;
			
			$subiekt['oznaczenie'] = "Symbol";
			$subiekt['nazwa'] = "Nazwa";
			$subiekt['tekst'] = "Opis";
			$subiekt['Nazwa Stawki VAT'] = "";
			$subiekt['vat'] = "Wartość stawki VAT";
			$subiekt["jedn_miary"] = 'JM';
			$subiekt["SWW"] = '';
			$subiekt["pkwiu"] = 'PKWiU';
			$subiekt["Dostawca"] = '';
			$subiekt['cena_netto'] = "Cena1 Netto";
			$subiekt['cena_brutto'] = "Cena1 Brutto";
			$subiekt['cena_netto_hurt'] = "Cena2 Netto";
			$subiekt['cena_brutto_hurt'] = "Cena2 Brutto";
			$subiekt['cena_promocji_n_hurt'] = "Cena3 Netto";
			$subiekt['cena_promocji_b_hurt'] = "Cena3 Brutto";
			$subiekt['cena_promocji_n'] = "Cena4 Netto";
			$subiekt['cena_promocji_b'] = "Cena4 Brutto";
			for($i=5;$i<=10;$i++)
			{
			$subiekt['Cena'.$i.' Netto'] = "";
			$subiekt['Cena'.$i.' Brutto'] = "";
			}
			
			if($tryb == 'subiekt') return $subiekt;
			
			$cdn_xl['oznaczenie'] = "Kod";
			$cdn_xl['nazwa'] = "Nazwa";
			$cdn_xl['cena_brutto'] = "Cena";
			$cdn_xl['Waluta'] = '';
			$cdn_xl['jedn_miary'] = "J.m.";
			$cdn_xl['dostepnosc'] = "Ilość do sprzedaży";
			$cdn_xl['Ilość magazynowa'] = '';
			$cdn_xl['Ilość zarezerwowana'] = '';
			$cdn_xl['Wartość księgowa'] = '';
			for($i=1;$i<=0;$i++)
			$cdn_xl[$i] = null;
			
			if($tryb == 'cdn_xl') return $cdn_xl;
			
			$clickshop['oznaczenie'] = "Kod produktu";
			$clickshop['promocja'] = "Promocja";
			$clickshop['producent'] = "Producent";
			$clickshop['waga'] = "Waga";
			$clickshop['dostepnosc'] = "Ilość w magazynie";
			$clickshop['cena_brutto'] = "Cena";
			$clickshop['vat'] = "Vat";
			$clickshop['nazwa'] = "Nazwa PL";
			$clickshop['tekst'] = "Opis PL";
			$clickshop['jedn_miary'] = "Jednostka miary PL";
			$clickshop['Kategoria'] = "";
			$clickshop['Aktywność PL'] = "";
			$clickshop['Opcje PL'] = "";
			$clickshop['Produkty powiązane'] = "";
			$clickshop['Zdjęcia'] = "";
			for($i=1;$i<=0;$i++)
			$clickshop[$i] = null;
			
			if($tryb == 'clickshop') return $clickshop;
			
			$fpp['oznaczenie'] = "Kod towaru";
			$fpp['nazwa'] = "Nazwa towaru";
			$fpp['jedn_miary'] = "j.m.";
			$fpp['dostepnosc'] = "Ilość";
			$fpp['cena_brutto'] = "Cena jednostkowa";
			$fpp['Lp.'] = "";
			$fpp['Wartość zakupu'] = "";
			$fpp['Wartość nabycia; Wsk. 0.00%'] = "";
			$fpp['Uwagi'] = "";
			for($i=1;$i<=0;$i++)
			$fpp[$i] = null;
			
			if($tryb == 'fpp' && $typ == 'inwentarz') return $fpp;
			
			$fpp['grupa'] = "grupa";
			$fpp['oznaczenie'] = "kod";
			$fpp['nazwa'] = "nazwa";
			//$fpp['zamiennik'] = "";
			$fpp['pkwiu'] = "PKWiU";
			$fpp['producent'] = "kod kontrahenta - dostawcy";
			$fpp['jedn_miary'] = "jednostka miary";
			$fpp['jedn_miary_zb'] = "opakowanie zbiorcze";
			$fpp['jedn_miary_zb_ile'] = "pakowanie zbiorcze";
			$fpp['vat'] = "VAT od sprzedaży";
			$fpp['cena_netto'] = "cena sprzedaży 1";
			//$fpp['widoczny'] = "Ukryty";
			$fpp['tekst'] = "opis";
			for($i=1;$i<=0;$i++)
			$fpp[$i] = null;
			
			if($tryb == 'fpp' && $typ == 'cennik') return $fpp;
			
			return $opisy;
		}
		
		if($rodzaj == 'klienci')
		{
			$opisy['id'] = 'ID';
			if($this->obConfig->klienciLogin != 'email')
			$opisy[$this->obConfig->klienciLogin] = 'Login';
			$opisy['nazwisko'] = 'Nazwisko';
			$opisy['imie'] = 'Imię';
			$opisy['email'] = 'E-mail';
			$opisy['telefon'] = 'Telefon';
			$opisy['komorka'] = 'Komórka';
			$opisy['nazwa_firmy'] = 'Nazwa firmy';
			$opisy['nip'] = 'NIP';
			$opisy['ulica'] = 'Ulica';
			$opisy['nr'] = 'Nr';
			$opisy['mieszkanie'] = 'Mieszkanie';
			$opisy['kod'] = 'Kod pocztowy';
			$opisy['miasto'] = 'Miasto';
			$opisy['wojew'] = 'Wojew.';
			$opisy['kraj'] = 'Kraj';			
			$opisy['inny_adres_dostawy'] = 'Adres dostawy';
			$opisy['nazwa_firmy_korespondencja'] = 'Nazwa firmy';
			$opisy['ulica_korespondencja'] = 'Ulica';
			$opisy['nr_korespondencja'] = 'Nr';
			$opisy['mieszkanie_korespondencja'] = 'Mieszkanie';
			$opisy['kod_korespondencja'] = 'Kod pocztowy';
			$opisy['miasto_korespondencja'] = 'Miasto';
			$opisy['wojew_korespondencja'] = 'Wojew';
			$opisy['kraj_korespondencja'] = 'Kraj';
			$opisy['widoczny'] = 'Widoczny';
			$opisy['data_rej'] = 'Data rejestracji';
			$opisy['lang'] = 'Język';
			$opisy['weryfikacja'] = 'Zweryfik. przez klienta';
			$opisy['weryfikacja_admin'] = 'Zweryfik. przez admina';
			
			if($tryb == 'panel') return $opisy;
			
			$fpp['nazwa_firmy'] = 'NAZWA1';
			$fpp['nazwa'] = 'NAZWA2';
			$fpp['ulica'] = 'ADRES1';
			$fpp['kod'] = 'KOD';
			$fpp['miasto'] = 'MIASTO';
			$fpp['nazwisko'] = 'OSOBA';
			$fpp['telefon'] = 'TELEFON';
			$fpp['nip'] = 'NIP';
			$fpp['email'] = 'Email';
			$fpp['www'] = 'Strona WWW';
			$fpp['id_ext'] = 'KOD';
			$fpp['rabat'] = 'Upust';
			
			if($tryb == 'fpp') return $fpp;
			
			return $opisy;
		}
		
		if($rodzaj == 'produktygrupy')
		{
			$opisy['nazwa'] = 'nazwa';
			
			$fpp['nazwa'] = 'N';
			$fpp['typ'] = 'TYP';
			
			if($tryb == 'fpp') return $fpp;
			
			return $opisy;
		}
		
		if($rodzaj == 'rabaty')
		{
			$opisy['nazwa'] = 'nazwa';
			
			$fpp['id_user'] = 'KON';
			$fpp['id_prod'] = 'KOD';
			$fpp['FLAG'] = '';
			$fpp['DATA'] = '';
			$fpp['TERMIN'] = '';
			$fpp['wartosc_rab'] = 'CENA';
			$fpp['typ_rab'] = 'WAL';
			$fpp['ILE'] = '';
			$fpp['OPIS'] = '';
			
			if($tryb == 'fpp') return $fpp;
			
			return $opisy;
		}
    }
	public function getCzyPole($tryb = null, $rodzaj = null, $typ = null)
    {
		if($tryb == null) $tryb = $this->tryb;
		if($rodzaj == null) $rodzaj = $this->rodzaj;
		if($typ == null) $typ = $this->typ;
		
		if($rodzaj == 'produkty')
		{
			$czyPole['id'] = ($tryb == 'panel');
			$czyPole['nazwa'] = true;
			$czyPole['oznaczenie'] = $this->obConfig->symbol;
			$czyPole['producent'] = $this->obConfig->producent;
			$czyPole['vat'] = $this->obConfig->vat;
			$czyPole['widoczny'] = true;
			$czyPole['dostepny'] = $this->obConfig->dostepny;
			$czyPole['promocja'] = $this->obConfig->oznaczPromocja;
			$czyPole['nowosc'] = $this->obConfig->oznaczNowosc;
			$czyPole['glowna'] = $this->obConfig->oznaczGlowna;
			$czyPole['bestseller'] = $this->obConfig->oznaczPolecane;
			$czyPole['produkt_dnia'] = $this->obConfig->oznaczProduktDnia;
			$czyPole['wyprzedaz'] = $this->obConfig->oznaczWyprzedaz;
			$czyPole['specjalne'] = $this->obConfig->oznaczSpecjalne;
			$czyPole['tekst'] = true;
			$czyPole['skrot'] = $this->ustawienia['skrot'];
			$czyPole['dostepnosc'] = $this->ustawienia['stany_mag'];
			$czyPole['jedn_miary'] = $this->obConfig->jednostki;
			$czyPole['jedn_miary_typ'] = $this->obConfig->jednostki;
			$czyPole['rabat'] = $this->obConfig->rabatyProdukty;
			$czyPole['waga'] = $this->obConfig->wagi;
			$czyPole['gabaryt'] = $this->obConfig->gabaryt;
			$czyPole['pozycja'] = $this->obConfig->produktyPozycja;
			$czyPole['pkwiu'] = $this->obConfig->pkwiu;
			$czyPole['sww'] = $this->obConfig->sww;
			$czyPole['id_ext'] = $this->obConfig->idExt;
			$czyPole['kategorie'] = true;
			$czyPole['kolory'] = $this->obConfig->kolory;
			$czyPole['platnosci'] = $this->obConfig->platnosciProdukt;
			$czyPole['powiazania'] = $this->obConfig->produktyPowiazania;
			
			foreach($this->ceny as $nazwa => $label)
			$czyPole[$nazwa] = ($this->obConfig->ceny && strpos($this->ustawienia['ceny'], $nazwa) !== false);
			
			if($tryb == 'panel') return $czyPole;
		}
		
		if($rodzaj == 'klienci')
		{
			$czyPole['id'] = true;
			if($this->obConfig->klienciLogin != 'email')
			$czyPole[$this->obConfig->klienciLogin] = true;
			$czyPole['nazwisko'] = true;
			$czyPole['imie'] = true;
			$czyPole['email'] = true;
			$czyPole['telefon'] = $this->obConfig->telefon;
			$czyPole['komorka'] = $this->obConfig->komorka;
			$czyPole['nazwa_firmy'] = true;
			$czyPole['nip'] = $this->obConfig->nip;
			$czyPole['ulica'] = true;
			$czyPole['nr'] = true;
			$czyPole['mieszkanie'] = true;
			$czyPole['kod'] = true;
			$czyPole['miasto'] = true;
			$czyPole['wojew'] = $this->obConfig->wojew;
			$czyPole['kraj'] = $this->obConfig->kraje;		
			$czyPole['inny_adres_dostawy'] = true;
			$czyPole['nazwa_firmy_korespondencja'] = true;
			$czyPole['ulica_korespondencja'] = true;
			$czyPole['nr_korespondencja'] = true;
			$czyPole['mieszkanie_korespondencja'] = true;
			$czyPole['kod_korespondencja'] = true;
			$czyPole['miasto_korespondencja'] = true;
			$czyPole['wojew_korespondencja'] = $this->obConfig->wojew;
			$czyPole['kraj_korespondencja'] = $this->obConfig->kraje;
			$czyPole['widoczny'] = true;
			$czyPole['data_rej'] = true;
			$czyPole['lang'] = $this->obConfig->jezyki;
			$czyPole['weryfikacja'] = $this->ustawienia['potwierdz_rejestr'];
			$czyPole['weryfikacja_admin'] = $this->ustawienia['potwierdz_rejestr_admin'];
			
			if($tryb == 'panel') return $czyPole;
		}
    }
	public function getPrzyklad($tryb = null)
    {
		if($tryb == null) $tryb = $this->tryb;
		$przyklad['id'] = 1;
		$przyklad['nazwa'] = 'Produkt przykładowy';
		$przyklad['producent'] = 'Firma Test';
		$przyklad['cena_netto'] = 100;
		$przyklad['cena_brutto'] = 122;
		$przyklad['cena_netto_hurt'] = 80;
		$przyklad['cena_brutto_hurt'] = 97.6;
		$przyklad['cena_promocji_n'] = 80;
		$przyklad['cena_promocji_b'] = 97.6;
		$przyklad['cena_promocji_n_hurt'] = 60;
		$przyklad['cena_promocji_b_hurt'] = 73.2;
		$przyklad['vat'] = 23;
		$przyklad['oznaczenie'] = 'Przykład';
		$przyklad['widoczny'] = 1;
		$przyklad['dostepny'] = 1;
		$przyklad['promocja'] = 1;
		$przyklad['nowosc'] = 0;
		$przyklad['glowna'] = 0;
		$przyklad['bestseller'] = 1;
		$przyklad['produkt_dnia'] = 1;
		$przyklad['wyprzedaz'] = 1;
		$przyklad['specjalne'] = 1;
		$przyklad['skrot'] = 'Opis skrócony';
		$przyklad['tekst'] = 'Pełny opis produktu';
		$przyklad['dostepnosc'] = 100;
		$przyklad['rabat'] = 5;
		$przyklad['jedn_miary'] = 'szt.';
		$przyklad['jedn_miary_typ'] = 'int';
		$przyklad['waga'] = 10;
		$przyklad['gabaryt'] = '';
		$przyklad['pozycja'] = 1;
		$przyklad['kategorie'] = '1|2';
		$przyklad['platnosci'] = '1|2';
		$przyklad['id_zewn'] = 2;
		$przyklad['pkwiu'] = '';
		$przyklad['sww'] = '';
		
		if($tryb == 'panel') return $przyklad;
		
		$hermes['oznaczenie'] = 'BI00.00';
		$hermes['nazwa'] = 'PRODUKT TESTOWY';
		$hermes['id_ext'] = '9999';
		$hermes['dostepnosc'] = 100;
		$hermes['cena_netto'] = 100;
		$hermes['cena_brutto'] = 123;
		$hermes['vat'] = 23;
		$hermes['jedn_miary'] = 'SZT';		
		$hermes['waga'] = 0;
		$hermes['pkwiu'] = '534545';
		$hermes['sww'] = '64754';
		
		if($tryb == 'hermes') return $hermes;
		
		$optima['oznaczenie'] = 'GR/POS';
		$optima['nazwa'] = 'PRODUKT TESTOWY';
		$optima['cena_brutto'] = 123;
		$optima['dostepnosc'] = 100;
		
		if($tryb == 'optima') return $optima;
		
		$cdn_xl['oznaczenie'] = 'test';
		$cdn_xl['nazwa'] = 'PRODUKT TESTOWY';
		$cdn_xl['cena_brutto'] = 123;
		$cdn_xl['jedn_miary'] = 'szt.';
		$cdn_xl['dostepnosc'] = 100;
		
		if($tryb == 'cdn_xl') return $cdn_xl;
		
		return null;
	}
	
	public function generujPrzyklad($check = false)
    {
		$przyklad = $this->getPrzyklad();
		if(count($przyklad) == 0) return null;
		if($check) return count($przyklad);
		
		$filename = 'admin/bazy/_przyklad_.xls';
			
		$workbook = new Spreadsheet_Excel_Writer($filename);
		$workbook->setVersion(8);
		$workbook->setCountry(48);

		//$format->setFont();//'Times New Roman', 'Arial', 'Courier'
		//$format->setBgColor('yellow');//string (like 'blue'), or integer (range is [8...63])
		$format_title = $workbook->addFormat();
		$format_title->setSize(12);
		$format_title->setBold(1);
		$format_title->setAlign('left');
		$format_header = $workbook->addFormat();		
		$format_header->setSize(10);
		$format_header->setBold(1);		
		$format_header->setAlign('center');
		$format_header->setHAlign('center');		
		$format_center = $workbook->addFormat();
		$format_center->setAlign('center');
		$format_left = $workbook->addFormat();
		$format_left->setAlign('left');
		$format_right = $workbook->addFormat();
		$format_right->setAlign('right');

		$worksheet = $workbook->addWorksheet();
		$worksheet->setInputEncoding('UTF-8');
		$size = 10;
		
		if($this->tryb == 'panel')
		{
			$worksheet->setColumn(0, 1, 5);
			$worksheet->setColumn(2, 2, 50);
			$worksheet->setColumn(3, 30, 12);
			$format_header->setTextWrap();
			$size = 10;
		}
		if($this->tryb == 'hermes' && $this->typ == 1)
		{
			$worksheet->setColumn(0, 30, 10);
			$title = 'Aktualny stan magazynu';
			$format_title->setSize(12);
			$format_title->setColor('green');
			$format_title->setBold(1);
			$format_header->setSize(8);
			$format_header->setFgColor('green');
			$format_header->setColor('yellow');
			$format_header->setAlign('left');
			$size = 8;
		}
		if($this->tryb == 'optima')
		{
			$worksheet->setColumn(0, 0, 30);
			$worksheet->setColumn(1, 1, 70);
			$worksheet->setColumn(2, 2, 10);
			$worksheet->setColumn(3, 3, 15);
			$worksheet->setColumn(4, 4, 10);
			$worksheet->setColumn(5, 5, 15);
			$format_header->setBold(0);	
			$format_header->setAlign('left');
			$size = 10;
		}
		if($this->tryb == 'cdn_xl')
		{
			$title = 'Lista towarów';
			$format_title->setBold(0);
			$format_title->setSize(10);
			$worksheet->setColumn(0, 0, 20);
			$worksheet->setColumn(1, 1, 55);
			$worksheet->setColumn(2, 2, 10);
			$worksheet->setColumn(3, 3, 7);
			$worksheet->setColumn(4, 4, 5);
			$worksheet->setColumn(5, 8, 17);
			$format_header->setBold(0);	
			$format_header->setAlign('left');
			$size = 10;
		}
		$format_center->setSize($size);
		$format_left->setSize($size);
		$format_right->setSize($size);
		
		$r = 0;
		if(@!empty($title))
		$worksheet->write($r++, 0, $title, $format_title);

		$i = 0;
		if($this->tryb == 'panel')
		$worksheet->write($r, $i++, 'LP', $format_header);
		foreach($this->opisy as $co => $nazwa)
		if($this->tryb != 'panel' || $this->czyPole[$co])
		{
			if($nazwa === '') $nazwa = $co;
			if($nazwa === null) $nazwa = '';
			$worksheet->write($r, $i++, ucfirst($nazwa), $format_header);
		}
		$r++;

		$zawartosc = array($przyklad);
		if(count($zawartosc) > 0)
		{
			//$zawartosc = $zawartosc->toArray();
			for($i = 0; $i < count($zawartosc); $i++)
			{
				$j = 0;
				if($this->tryb == 'panel')
				$worksheet->write($r + $i, $j++, $i + 1, $format_center);
				foreach($this->opisy as $co => $nazwa)
				if($this->tryb != 'panel' || $this->czyPole[$co])
				{
					if(!isset($zawartosc[$i][$co])) { $j++; continue; }
					
					$format = $format_center;
					if(in_array($co, $this->leftAligns)) $format = $format_left;
					if(in_array($co, $this->rightAligns)) $format = $format_right;
					
					if(in_array($co, $this->strings))
					$worksheet->writeString($r + $i, $j++, $zawartosc[$i][$co], $format);
					else
					$worksheet->write($r + $i, $j++, $zawartosc[$i][$co], $format);
				}
			}
		}

		//$workbook->send('testxls.xls');
		@$workbook->close();
		
		return $filename;
	}
	
	public function getTabele()
    {
		$where = '1';
		$where = 'SUBSTR(Name, 1, 6) <> "phpbb_"';
        $select = 'SHOW TABLE STATUS FROM '.$this->obConfig->db->config->dbname.' WHERE '.$where;
		$result = $this->db->fetchAll($select);
		return $result;
    }
	public function getTabela($Name = '')
    {
		$where = 'Name = "'.$Name.'"';
        $select = 'SHOW TABLE STATUS FROM '.$this->obConfig->db->config->dbname.' WHERE '.$where;
		$result = $this->db->fetchRow($select);
		return $result;
    }
	public function getTabeleProdukty()
    {
		$tabeleProdukty[] = '"Produkty"';
		$tabeleProdukty[] = '"Routers"';
		$tabeleProdukty[] = '"Katprod"';
		$tabeleProdukty[] = '"Galeria"';
		$tabeleProdukty[] = '"Rozmiarproduktu"';
		$tabeleProdukty[] = '"Kolorproduktu"';
		$tabeleProdukty[] = '"Atrybutypowiazania"';
		$tabeleProdukty[] = '"Produktypowiazania"';
		$tabeleProdukty[] = '"Platnosciprodukty"';
		$tabeleProdukty[] = '"Opinie"';
		$select = 'SHOW TABLE STATUS FROM '.$this->obConfig->db->config->dbname;
		$select.= ' WHERE Name in('.implode(',', $tabeleProdukty).')';
		$result = $this->db->fetchAll($select);
		return $result;
    }
	public function getKolumny($tabela)
    {
        $select = 'SHOW COLUMNS FROM `'.$tabela.'`';
		$result = $this->db->fetchAll($select);
		return $result;
    }
	public function getWiersze($tabela, $od = 0, $limit = 999999)
    {
        $select = 'SELECT * FROM `'.$tabela.'` WHERE 1 LIMIT '.$limit.' OFFSET '.$od;
		$result = $this->db->fetchAll($select);
		return $result;
    }
	public function wyczyscTabele($tabela)
    {
		if(strtolower($tabela) == 'routers')
        $this->db->query('DELETE FROM `'.$tabela.'` WHERE controller != "index"');
		else if(!in_array(strtolower($tabela), $this->notAllowed))
		$this->db->query('DELETE FROM `'.$tabela.'` WHERE 1');
    }
	public function wyczyscTabeleProdukty($tabela)
    {
		$routeWhere = 'module = "default" and controller = "produkt" and action = "index"';
		if(strtolower($tabela) == 'routers')
        $this->db->query('DELETE FROM `'.$tabela.'` WHERE '.$routeWhere);
		else if(strtolower($tabela) == 'opinie')
        $this->db->query('DELETE FROM `'.$tabela.'` WHERE id_prod > 0');
		else if(!in_array(strtolower($tabela), $this->notAllowed))
		$this->db->query('DELETE FROM `'.$tabela.'` WHERE 1');
    }
	public function usunProdukty($ids = null)
    {
		if(!$this->common->isArray($ids, true)) return null;
		$ids = implode(',', $ids);
		//var_dump($ids);die();
		$routeWhere = 'module = "default" and controller = "produkt" and action = "index"';
        $this->db->query('DELETE FROM Produkty WHERE id in('.$ids.')');
		$this->db->query('DELETE FROM Routers WHERE '.$routeWhere.' and params in('.$ids.')');
		$this->db->query('DELETE FROM Katprod WHERE id_prod in('.$ids.')');
		$this->db->query('DELETE FROM Galeria WHERE wlasciciel in('.$ids.')');
		$this->db->query('DELETE FROM Rozmiarproduktu WHERE id_produktu in('.$ids.')');
		$this->db->query('DELETE FROM Kolorproduktu WHERE id_produktu in('.$ids.')');
		$this->db->query('DELETE FROM Atrybutypowiazania WHERE id_og in('.$ids.')');
		$this->db->query('DELETE FROM Produktypowiazania WHERE id_prod in('.$ids.')');
		$this->db->query('DELETE FROM Platnosciprodukty WHERE id_produkt in('.$ids.')');
		$this->db->query('DELETE FROM Opinie WHERE id_prod in('.$ids.')');
    }
	public function wyczyscBazeAll($tabela)
    {
		$select = 'SHOW TABLE STATUS FROM '.$this->obConfig->db->config->dbname;
		$result = $this->db->fetchAll($select);
		foreach($result as $tabela)
		{
			$this->wyczyscTabele($tabela['Name']);
		}
    }
	function insertRow($table, $dane)
	{
		if(!(strtolower($table) == 'routers' && $dane['controller'] == 'index'))
		{
			$result = $this->db->insert($table, $dane);
			//$this->_name = $table;
			//$this->insert($dane);
		}
	}
	function insertAll($table, $dane)
	{
		foreach($dane as $row)
		{
			$this->insertRow($table['Name'], $row);
		}
	}
	function insertSql($table, $select)
	{
		//var_dump($select);
		//if(!(strtolower($table) == 'routers' && strpos($select, ':controller/:action') !== false))
		return $this->db->query($select);
	}	
	
	public function wyczyscBaze()
    {
		$tabeleProdukty = $this->getTabeleProdukty();
        if(count($tabeleProdukty) > 0)
		foreach($tabeleProdukty as $tabela)
		$this->wyczyscTabeleProdukty($tabela['Name']);
    }
	
	public function getTabelaCount($tabela)
    {
		if(empty($tabela)) return null;
        //return $this->db->query('SELECT COUNT(*) FROM '.$tabela.' WHERE 1');
		$select = $this->db->select()->from($tabela, 'count(*)')->where(time());
		//echo $select;
		$result = $this->db->fetchAll($select);
		return isset($result[0]['count(*)']) ? $result[0]['count(*)'] : 0;
    }
	
	public function wczytajDaneXlsOld($plik) 
	{
        $data = new Spreadsheet_Excel_Reader();
        $data->setUTFEncoder('iconv');
        $data->setOutputEncoding('UTF-8');

        $result = $data->read($plik);
		if(!empty($result)) $this->bledy .= $result.'<br/>';
		
		$import = new Import();
		
		$produkty = new Produkty();
		if($this->obConfig->produktyPozycja)
		$pozycja = $produkty->wypiszMaxPozycja();
		
		$row = 2;
		if(count($data->sheets) > 0)
        for ($i = $row; $i <= $data->sheets[$this->arkuszNR]['numRows']; $i++) 
		{
			$j = 2;
			$cells = @$data->sheets[$this->arkuszNR]['cells'][$i];
			//var_dump($cells);die();
			//$dane[$i - $row]['id'] = trim($cells[$j++]);
			if(count($cells) > 0)
			if(count($this->opisy) > 0)
			foreach($this->opisy as $co => $nazwa)
			{
				if(!isset($this->czyPole[$co]) || $this->czyPole[$co])
				{
					if(in_array($co, $this->floats))
					$dane[$i - $row][$co] = @trim(str_replace(',', '.', $cells[$j++]));
					else $dane[$i - $row][$co] = @trim($cells[$j++]);
					//$dane[$i - $row][$co] = @$this->win2utf(trim($cells[$j++]));

					if($this->obConfig->produktyPozycja && $co == 'pozycja')
					{
						if(empty($dane[$i - $row]['pozycja']))
						$dane[$i - $row]['pozycja'] = $pozycja++;
					}
				}
			}
			//if($i<20)
			//echo '<br><br>q= '.htmlspecialchars($dane[$i - $row]['tekst']);
			$dane[$i - $row]['nazwa'] = @$this->win2utf($dane[$i - $row]['nazwa']);
        }
        //var_dump($dane);
        return @$dane;
    }
	
	public function wczytajDaneXls($plik, $od = 0, $ile = 0, $ilosc = false)
	{
		$fileinfo = pathinfo($plik);
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
		$cacheSettings = array('memoryCacheSize' => '32MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
		if($fileinfo['extension'] == 'xls')
		{
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			//$objPHPExcel = @$objReader->load($plik);
		}
		if($fileinfo['extension'] == 'xlsx')
		{
			$objReader = PHPExcel_IOFactory::createReaderForFile($plik);
			//$objPHPExcel = PHPExcel_IOFactory::load($plik);
		}
		//$objReader->setReadDataOnly(true);
		if($ile > 0 && !$ilosc)
		{
			$chunkFilter = new chunkReadFilter($od, $ile, $this->headersRow);
			$objReader->setReadFilter($chunkFilter);
		}
		try
		{
			$objPHPExcel = $objReader->load($plik);
		}
		catch(Exception $e)
		{
			return null;
		}
		$sheet = $objPHPExcel->getActiveSheet();
		if($this->arkuszNR >= 0)
		$sheet = $objPHPExcel->setActiveSheetIndex($this->arkuszNR);
		if($ilosc) return max(0, $sheet->getHighestRow() - $this->headersRow);
		$rowIterator = $sheet->getRowIterator();
		
		$produkty = new Produkty();
		if($this->obConfig->produktyPozycja)
		$pozycja = $produkty->wypiszMaxPozycja();
		
		$pola = array();
		if(isset($pola[$this->pole]))
		$pola[$this->pole] = $this->opisy[$this->pole];
		if(count($this->opisy) > 0)
		foreach($this->opisy as $co => $nazwa)
		if(!isset($this->czyPole[$co]) || $this->czyPole[$co])
		$pola[$co] = $nazwa;
		//var_dump($pola);die();
		
		$spodziewanaIloscKolumn = $this->dataColumn;
		if($this->tryb != 'panel' && $this->fixedColumnPosition)
		$spodziewanaIloscKolumn += count($this->opisy);
		else $spodziewanaIloscKolumn += count($pola);
		if(!$this->fixedColumnPosition)
		$spodziewanaIloscKolumn = $this->columnCount;
		//var_dump($spodziewanaIloscKolumn);
		
		$i = 0;
		$r = $this->headersRow;
		$row = 0;
		//if(count($rowIterator) > 0)
        foreach($rowIterator as $rows)
		{
			$rowIndex = $rows->getRowIndex();
			//if($ile > 0 && $rowIndex != $r && ($rowIndex < $od)) continue;
			//if($ile > 0 && $rowIndex != $r && ($rowIndex > $od+$ile)) break;
			//if($i > 1) break;
			$j = $this->dataColumn;
			//if($rows->getRowIndex() == 1) continue;
			//if($rows->getRowIndex() == 3) break;
			$cellIterator = $rows->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(false);
			$cells = null;
			//if(count($cellIterator) > 0)
			$c = 0;
			foreach($cellIterator as $cell)
			//if($cell->getCalculatedValue() !== null)
			if($c++ < $spodziewanaIloscKolumn)
			try
			{
				$cells[] = $cell->getCalculatedValue();
			}
			catch(PHPExcel_Calculation_Exception $e)
			{
				$cells[] = "";
			}
			//var_dump($cells);//die();

			if($rows->getRowIndex() == $r)
			{
				//var_dump($cells);
				if($this->checkColumnCount && count($cells) != $spodziewanaIloscKolumn)
				{
					$this->bledy .= '<br/>';
					$this->bledy .= 'Spodziewana ilość kolumn = '.$spodziewanaIloscKolumn.'<br/>';
					$this->bledy .= 'Rzeczywista ilość kolumn = '.count($cells).'<br/>';
					if($this->rodzaj == 'produkty')
					{
						$this->bledy .= 
						'<a href="'.$this->obConfig->baseUrl.'/admin/import/importxls/tryb/'.$this->tryb.'/test/1">';
						$this->bledy .= 'Generuj przykładowy plik XLS</a>';
					}
					return null;
				}
				
				$kolumny = null;
				
				$zleKolumny = null;
				if($this->fixedColumnPosition)
				{
					if(count($cells) > 0)
					if(count($this->opisy) > 0)
					foreach($this->opisy as $co => $nazwa)
					if($this->tryb != 'panel' || $this->czyPole[$co])
					{
						if(empty($nazwa)) { $j++; continue; }
						$cell = @$cells[$j++];
						if($nazwa != $cell)
						$zleKolumny[$nazwa] = $cell;
						$kolumny[$nazwa] = $cell;
					}
				}
				$brakKolumn = null;
				if(!$this->fixedColumnPosition)
				{
					if(count($pola) > 0)
					foreach($pola as $co => $nazwa)
					if(!in_array($nazwa, $cells))
					$brakKolumn[$co] = $nazwa;
					$kolumny = $cells;
				}
				if(false)
				{
					var_dump($this->opisy);
					var_dump($pola);
					var_dump($cells);
					var_dump($brakKolumn);
					var_dump($zleKolumny);
					var_dump($kolumny);
					die();
				}
				if(count($zleKolumny) > 0 || count($brakKolumn) > 0)
				{
					$this->bledy .= '<br/>';
					$this->bledy .= '<span style="font-size:12px;">';
					if(count($zleKolumny) > 0)
					foreach($zleKolumny as $co => $nazwa)
					$this->bledy .= 'Spodziewana kolumna: '.$co.'; rzeczywista kolumna = '.$nazwa.'<br/>';
					if(count($brakKolumn) > 0)
					foreach($brakKolumn as $co => $nazwa)
					$this->bledy .= 'Brakujące pole: '.$co.'; brakująca kolumna = '.$nazwa.'<br/>';
					$this->bledy .= '</span>';
					if($this->rodzaj == 'produkty')
					{
						$this->bledy .= 
						'<a href="'.$this->obConfig->baseUrl.'/admin/import/importxls/tryb/'.$this->tryb.'/test/1">';
						$this->bledy .= 'Generuj przykładowy plik XLS</a>';
					}
					return null;
				}
			}
			
			if($rows->getRowIndex() > $r)
			{
				if($this->fixedColumnPosition)
				if(count($cells) > 0)
				if(count($this->opisy) > 0)
				foreach($this->opisy as $co => $nazwa)
				{
					//if(empty($nazwa)) break;
					if(empty($nazwa)) { $j++; continue; }
					if(!isset($this->czyPole[$co]) || $this->czyPole[$co])
					{
						if(in_array($co, $this->floats))
						$dane[$i - $row][$co] = @trim(str_replace(',', '.', $cells[$j++]));
						else $dane[$i - $row][$co] = @trim(strval($cells[$j++]));
						//$dane[$i - $row][$co] = @$this->win2utf(trim($cells[$j++]));
						
						if($co == 'nazwa' || $co == $this->pole)
						if(@empty($dane[$i - $row][$co]))
						{
							unset($dane[$i - $row]);
							break;
						}

						if($this->obConfig->produktyPozycja && $co == 'pozycja')
						{
							if(empty($dane[$i - $row]['pozycja']))
							$dane[$i - $row]['pozycja'] = $pozycja++;
						}
					}
					else if($this->tryb != 'panel') $j++;
				}
				
				//var_dump($kolumny);
				if(!$this->fixedColumnPosition)
				if(count($kolumny) > 0)
				foreach($kolumny as $j => $nazwa)
				{
					//if(empty($nazwa)) break;
					$co = array_search($nazwa, $pola);
					//echo $nazwa.' = '.$co.'<br>';
					if(!empty($co))
					{
						if(in_array($co, $this->floats))
						$dane[$i - $row][$co] = @trim(str_replace(',', '.', $cells[$j]));
						else $dane[$i - $row][$co] = @trim(strval($cells[$j]));
						//$dane[$i - $row][$co] = @$this->win2utf(trim($cells[$j]));
						
						if($co == 'nazwa' || $co == $this->pole)
						if(@empty($dane[$i - $row][$co]))
						{
							unset($dane[$i - $row]);
							break;
						}

						if($this->obConfig->produktyPozycja && $co == 'pozycja')
						{
							if(empty($dane[$i - $row]['pozycja']))
							$dane[$i - $row]['pozycja'] = $pozycja++;
						}
					}
				}
			}
			
			if($rows->getRowIndex() > $r && isset($dane[$i - $row])) $i++;
			//$dane[$i - $row]['nazwa'] = @$this->win2utf($dane[$i - $row]['nazwa']);
        }
        //var_dump($dane);die();
		@$objPHPExcel->disconnectWorksheets();
		unset($objPHPExcel);
        return @$dane;
    }
	
	function wczytajDaneXml($plik, $od = 0, $ile = 0, $ilosc = false)
	{
		$reader = new XMLReader();
		$xmlOK = $reader->open($plik, $this->encoding);
		if($xmlOK === false)
		{
			$this->bledy .= 'Błąd odczytu pliku XML';
			return null;
		}
		//$od = 1;
		//$ile = 1;
		//$test = 10;
		$counter = 0;
		//$ilosc = false;
		$vals = array();
		$opisy = @array_flip($this->opisy);
		while($reader->read())
		{
			if(!$ilosc && $counter >= $od && $counter < ($od + $ile))
			{
				if($reader->nodeType == XMLReader::ELEMENT) { $name = $reader->name; }
				if($reader->nodeType == XMLReader::ELEMENT && $reader->name == $this->pole) { $vals[$counter] = array(); }
				if($reader->nodeType == XMLReader::TEXT || $reader->nodeType == XMLReader::CDATA) 
				{
					$co = isset($opisy[$name]) ? $opisy[$name] : $name;
					$$co = trim($reader->value);
					//if(strlen($$co) == 0) continue;
					if(in_array($co, $this->ints)) $$co = intval($$co);
					if(in_array($co, $this->floats)) $$co = floatval(str_replace(',', '.', $$co));
					$vals[$counter][$co] = $$co;
				}
				if($reader->nodeType == XMLReader::ELEMENT && $reader->name == "property") 
				{
					if(@count($this->property) > 0)
					foreach($this->property as $co => $property)
					{
						$$co = trim($reader->getAttribute($property));
						if(strlen($$co) == 0) continue;
						if(in_array($co, $this->ints)) $$co = intval($$co);
						if(in_array($co, $this->floats)) $$co = floatval(str_replace(',', '.', $$co));
						$vals[$counter][$co] = $$co;
					}
				}
			}
			if($reader->nodeType == XMLReader::END_ELEMENT && $reader->name == $this->pole) { $counter++; }
			//if($test && $counter == $test) { break; var_dump($vals); die(); }
		}
		$reader->close();
		//var_dump($vals);die();
		return $ilosc ? $counter : $vals;
	}
	
	public function usunStareProdukty($dane)
	{		
		if(count($dane) > 0)
		foreach($dane as $prod)
		{
			$produktyNew[trim($prod[$this->pole])] = $prod;
			//var_dump($prod['nazwa']);
			//$prod['nazwa'] = str_replace('Ă', 'Ó', $prod['nazwa']);
			if(false && strpos($prod['nazwa'], 'REZERWACJAqqq') !== false)
			{
				var_dump($prod['nazwa']);
				echo '<br>';
				for($z=0;$z<strlen($prod['nazwa']);$z++)
				echo ord($prod['nazwa'][$z]).' ';
				echo '<br>';
			}
		}
		//die();
		
        $produkty = new Produkty();		
		$produktyAll = $produkty->wypiszProduktyImport($this->pole, false);
		//@var_dump($produktyAll);
		$old = 0;
		if(count($produktyAll) > 0)
		foreach($produktyAll as $prod)
		{
			//if($prod['id'] == '1365')
			if(false && strpos($prod['nazwa'], 'Doitsu hariwake 1700000000000') !== false)
			{
				var_dump($prod['id']);
				var_dump($prod['nazwa']);
				echo '<br>';
				for($z=0;$z<strlen($prod['nazwa']);$z++)
				echo ord($prod['nazwa'][$z]).' ';
				echo '<br>';
			}
			if(!isset($produktyNew[trim($prod[$this->pole])]))
			{
				$produkty->id = $prod['id'];
				//var_dump($prod['nazwa']);
				//$produkty->usunProdukty();
				$ids[] = $prod['id'];
				$old++;
			}
		}
		//@var_dump($ids);
		if(@count($ids) > 0) $this->usunProdukty($ids);
		$this->bledy .= 'Usunięto '.$old.' produkt(y)(ów)<br/>';
		//var_dump(count($dane));		
		return $old;
	}
	
	public function wgrajNoweProdukty($file, $dane, $od = 0, $do = 0)
	{
		$module = 'admin';
		//var_dump(count($dane));		
        //$import = new Import();
		$common = new Common(false, $module);
        $produkty = new Produkty();
		$route = new Routers();
		$kat = new Kategorie();
        $podkat = new Katprod();   
        $galeria = new Galeria();
		$producent = new Producent();
		$jednMiary = new Jednostkimiary();
        $kolor = new Kolory();
        $koloryprod = new Kolorproduktu();
        $rozmiary = new Rozmiary();
        $rozmiaryprod = new Rozmiarproduktu();             
		$platnosciprodukty = new PlatnosciProdukty();
		$produktypowiazania = new Produktypowiazania();
		
		$produktyAll = $produkty->wypiszProduktyImport($this->pole, $od > 0);
		if($this->obConfig->tryb == "rozmiary")
		if($this->pole == 'id_ext' || $this->pole == 'oznaczenie')
		$rozmiaryAll = $rozmiaryprod->wypiszRozmiaryImport($this->pole, $od > 0);
		$producenciAll = $producent->getProducenci('nazwa');
		$jednMiaryAll = $jednMiary->getJednMiary('nazwa');
		
		if($do == 0) $do = count($dane);
		
		if(count($dane) > 0)
        for($i = $od; $i < $do; $i++)
		{
			$produkt = @($produktyAll[trim($dane[$i][$this->pole])]);
			if(@intval($produkt['id']) > 0) $ids[] = $produkt['id'];
		}
		//var_dump($ids);
		if(@$this->czyPole['kategorie'])
		$kategorie = $podkat->getKategorieForExport(@$ids);
		if(@$this->czyPole['kolory'])
		$kolory = $koloryprod->getKoloryForExport(@$ids);
		if(@$this->czyPole['platnosci'])
		$platnosci = $platnosciprodukty->getPlatnosciForExport(@$ids);
		if(@$this->czyPole['powiazania'])
		$powiazania = $produktypowiazania->getPowiazaniaForExport(@$ids);

		$ile = 0;
		$nowe = 0;
		$exist = 0;
		$update = null;
		$bledy = '';
		//var_dump($dane);
		//if(false)
		if(count($dane) > 0)
        for($i = $od; $i < $do; $i++)
		{
			if(@empty($dane[$i]['nazwa'])) continue;
			if($this->pole != 'id' && @empty($dane[$i][$this->pole])) continue;
			//var_dump($dane[$i]['nazwa']);
			//continue;
			//if($dane[$i][$this->pole] != 'BN052') continue;
            //$produkt = $produkty->wyszukajPoKodzie(trim($dane[$i]['oznaczenie']));
			//$produkt = $produkty->szukajNazwa(trim($dane[$i]['nazwa']));
			$produkt = @($produktyAll[trim($dane[$i][$this->pole])]);
			if($this->obConfig->tryb == "rozmiary")
			$rozmiar = @($rozmiaryAll[trim($dane[$i][$this->pole])]);
			
			if($this->obConfig->producent)
			{
				if(!empty($dane[$i]['producent']))
				{
					//$existProd = $producent->wypiszProducentaNazwa($dane[$i]['producent']);
					$existProd = @$producenciAll[trim($dane[$i]['producent'])];
					if(count($existProd) > 0)
					{
						$dane[$i]['producent'] = intval($existProd['id']);
					}
					else
					{
						$daneProducent = array('nazwa' => trim($dane[$i]['producent']));
						$nowyProducent = $producent->dodajProducenta($daneProducent);
						$daneProducent['id'] = $nowyProducent;
						$producenciAll[trim($dane[$i]['producent'])] = $daneProducent;
						$dane[$i]['producent'] = $nowyProducent;
					}
				}
				else $dane[$i]['producent'] = 0;
			}
			if(isset($dane[$i]['jedn_miary']))
			{
				if(empty($dane[$i]['jedn_miary'])) $dane[$i]['jedn_miary'] = 'szt.';
				else
				{
					//$existJedn = $jednMiary->wypiszNazwa($dane[$i]['jedn_miary']);
					$existJedn = @$jednMiaryAll[trim($dane[$i]['jedn_miary'])];
					if(count($existJedn) == 0)
					{
						$daneJednMiary = array('nazwa' => trim($dane[$i]['jedn_miary']), 'typ' => 'int');
						$nowaJednMiary = $jednMiary->dodaj($daneJednMiary);
						$daneJednMiary['id'] = $nowaJednMiary;
						$jednMiaryAll[trim($dane[$i]['jedn_miary'])] = $daneJednMiary;
					}
					else $dane[$i]['jedn_miary_typ'] = $existJedn['typ'];
				}
			}
			if(isset($dane[$i]['jedn_miary_typ']))
			{
				if(empty($dane[$i]['jedn_miary_typ'])) $dane[$i]['jedn_miary_typ'] = 'int';
			}
			if(@count($produkt) > 0)
			{
				$produktID = $produkt['id'];
				if(isset($dane[$i]['kategorie']))
				{
					if(!isset($update['kategorie'])) $update['kategorie'] = 0;
					if($dane[$i]['kategorie'] != @strval($kategorie[$produktID]))
					{
						//if($dane[$i]['nazwa'] != '1') die();
						if(@!empty($kategorie[$produktID]))
						{
							$podkat->id = $produktID;
							$podkat->deleteProdukt();
						}
						$kategs = @explode('|', strval($dane[$i]['kategorie']));
						if(true && count($kategs) > 0)
						foreach($kategs as $kateg)
						if(!empty($kateg) && intval($kateg) > 0)
						$podkat->add(intval($kateg), $produktID);
						$update['kategorie']++;
					}
					unset($dane[$i]['kategorie']);
				}
				if(isset($dane[$i]['kolory']))
				{
					if(!isset($update['kolory'])) $update['kolory'] = 0;
					if($dane[$i]['kolory'] != @strval($kolory[$produktID]))
					{
						if(@!empty($kolory[$produktID]))
						$koloryprod->usunProdukt($produktID);
						$koloryAll = @explode('|', strval($dane[$i]['kolory']));
						if(@count($koloryAll) > 0)
						foreach($koloryAll as $kolor)
						if(!empty($kolor) && intval($kolor) > 0)
						{
							$koloryDane = array('id_produktu' => $produktID, 'id_koloru' => intval($kolor));
							$koloryprod->dodaj($koloryDane);
						}
						$update['kolory']++;
					}
					unset($dane[$i]['kolory']);
				}
				if(isset($dane[$i]['platnosci']))
				{
					if(!isset($update['platnosci'])) $update['platnosci'] = 0;
					if($dane[$i]['platnosci'] != @strval($platnosci[$produktID]))
					{
						if(@!empty($platnosci[$produktID]))
						$platnosciprodukty->usunProdukt($produktID);
						$platnosciAll = @explode('|', strval($dane[$i]['platnosci']));
						if(@count($platnosciAll) > 0)
						foreach($platnosciAll as $platnosc)
						if(!empty($platnosc) && intval($platnosc) > 0)
						{
							$platnosciDane = array('id_produkt' => $produktID, 'id_platnosci' => intval($platnosc));
							$platnosciprodukty->addWartosc($platnosciDane);
						}
						$update['platnosci']++;
					}
					unset($dane[$i]['platnosci']);
				}
				if(isset($dane[$i]['powiazania']))
				{
					if(!isset($update['powiazania'])) $update['powiazania'] = 0;
					if($dane[$i]['powiazania'] != @strval($powiazania[$produktID]))
					{
						if(@!empty($powiazania[$produktID]))
						$produktypowiazania->usunProdukt($produktID);
						$powiazaniaAll = @explode('|', strval($dane[$i]['powiazania']));
						if(@count($powiazaniaAll) > 0)
						foreach($powiazaniaAll as $powiazanie)
						if(!empty($powiazanie) && intval($powiazanie) > 0)
						{
							$powiazaniaDane = array('id_prod' => $produktID, 'powiazany' => intval($powiazanie));
							$produktypowiazania->dodaj($powiazaniaDane);
						}
						$update['powiazania']++;
					}
					unset($dane[$i]['powiazania']);
				}
			}
			
			foreach($this->ceny as $co => $cena)
			if(isset($dane[$i][$co]))
			$dane[$i][$co] = floatval(str_replace(",", ".", str_replace("PLN", "", $dane[$i][$co])));
			
			if(@floatval($dane[$i]['cena_promocji_n']) > 0 || @floatval($dane[$i]['cena_promocji_b']) > 0) $dane[$i]['promocja'] = 1;
			
			$vat = isset($dane[$i]['vat']) ? $dane[$i]['vat'] : (@isset($produkt['vat']) ? $produkt['vat'] : 23);
			if(isset($dane[$i]['cena_brutto_hurt']) && @floatval($dane[$i]['cena_netto_hurt']) == 0)
			$dane[$i]['cena_netto_hurt'] = $common->obliczNettoFromBruttoVat($dane[$i]['cena_brutto_hurt'], $vat);
			if(isset($dane[$i]['cena_brutto_hurt']) && @floatval($dane[$i]['cena_brutto']) == 0)
			$dane[$i]['cena_brutto'] = $dane[$i]['cena_brutto_hurt'];
			if(isset($dane[$i]['cena_brutto']) && @floatval($dane[$i]['cena_netto']) == 0)
			$dane[$i]['cena_netto'] = $common->obliczNettoFromBruttoVat($dane[$i]['cena_brutto'], $vat);
			if(isset($dane[$i]['cena_promocji_b_hurt']) && @floatval($dane[$i]['cena_promocji_n_hurt']) == 0)
			$dane[$i]['cena_promocji_n_hurt'] = $common->obliczNettoFromBruttoVat($dane[$i]['cena_promocji_b_hurt'],$vat);
			if(isset($dane[$i]['cena_promocji_b_hurt']) && @floatval($dane[$i]['cena_promocji_b']) == 0)
			$dane[$i]['cena_promocji_b'] = $dane[$i]['cena_promocji_b_hurt'];
			if(isset($dane[$i]['cena_promocji_b']) && @floatval($dane[$i]['cena_promocji_n']) == 0)
			$dane[$i]['cena_promocji_n'] = $common->obliczNettoFromBruttoVat($dane[$i]['cena_promocji_b'], $vat);
			
			$daneRozm = $dane[$i];	
			if(@count($rozmiar) > 0)
			//if(@$produkt['rozmiarowka'] > 0)
			if($this->obConfig->tryb == "rozmiary")
			{
				unset($daneRozm[$this->pole]);
				
				if(count($daneRozm) > 0)
				foreach($daneRozm as $co => $dana)
				{
					if(!in_array($co, $this->polaRozmiary)
					&& (!in_array($co, $this->ceny) || $produkt['trybCena'] != 'rozmiary'))
					{
						unset($daneRozm[$co]);
						continue;
					}
					if(@in_array($co, $this->strings))
					{
						//if($dana != $rozmiar[$co]) echo htmlspecialchars($rozmiar[$co]).'<br><br>';
						if(strlen($rozmiar[$co]) >= 32767) $rozmiar[$co] = strip_tags($rozmiar[$co]);
						$rozmiar[$co] = str_replace("\r", '', trim($rozmiar[$co]));// zamienia 13 10 na 10
						//if($dana != $rozmiar[$co]) echo htmlspecialchars($rozmiar[$co]).'<br><br>';
					}
					if(!isset($update[$co])) $update[$co] = 0;
					if($dana == $rozmiar[$co]) unset($daneRozm[$co]);
				}
				
				if(count($daneRozm) > 0)
				{
					//var_dump($daneRozm);die();
					$rozmiaryprod->id = @intval($rozmiar['id']);
					$updateOK = $rozmiaryprod->zmien($daneRozm);
					$exist += @intval($updateOK);
					if($updateOK)
					foreach($daneRozm as $co => $dana)
					{
						if($dana != $rozmiar[$co])
						{
							$update[$co]++;
						}
					}
				}
			}
			
			$daneProd = $dane[$i];
			if(@count($produkt) > 0)
			{
				//unset($dane[$i]['oznaczenie']);
				unset($daneProd[$this->pole]);
				unset($daneProd['nazwa']);
				
				if(@$this->obConfig->marzaKategorie)
				if(isset($daneProd['cena_brutto_hurt']))
				{
					$marza = 0;
					$kategs = $podkat->kategorieProduktu($produkt['id'], 'id_kat', 'desc');
					if(count($kategs) > 0)
					foreach($kategs as $kateg)
					{
						if(floatval($kateg['marza']) > 0)
						{
							$marza = floatval($kateg['marza']);
							break;
						}
					}
					$daneProd['cena_netto_hurt'] = $common->obliczNettoFromBruttoVat($daneProd['cena_brutto_hurt'],$vat);
					$daneProd['cena_brutto'] = floatval($daneProd['cena_brutto_hurt']) * (1 + $marza / 100);
					$daneProd['cena_netto'] = $common->obliczNettoFromBruttoVat($daneProd['cena_brutto'], $vat);
				}
				
				if(count($daneProd) > 0)
				foreach($daneProd as $co => $dana)
				{
					if($this->obConfig->tryb == "rozmiary")
					if(@count($rozmiar) > 0 || $produkt['rozmiarowka'] > 0)
					if(in_array($co, $this->polaRozmiary)
					|| (in_array($co, $this->ceny) && $produkt['trybCena'] == 'rozmiary'))
					{
						unset($daneProd[$co]);
						continue;
					}
					if(in_array($co, $this->strings))
					{
						//if($dana != $produkt[$co]) echo htmlspecialchars($produkt[$co]).'<br><br>';
						if(strlen($produkt[$co]) >= 32767) $produkt[$co] = strip_tags($produkt[$co]);
						$produkt[$co] = str_replace("\r", '', trim($produkt[$co]));// zamienia 13 10 na 10
						//if($dana != $produkt[$co]) echo htmlspecialchars($produkt[$co]).'<br><br>';
					}
					if(!isset($update[$co])) $update[$co] = 0;
					if($dana == $produkt[$co]) unset($daneProd[$co]);
				}
				
				if(count($daneProd) > 0)
				{
					//var_dump($dane[$i]);die();
					$produkty->id = @intval($produkt['id']);
					$updateOK = $produkty->edytujProdukty($daneProd);
					$exist += @intval($updateOK);
					if($updateOK)
					foreach($daneProd as $co => $dana)
					{
						if($dana != $produkt[$co])
						{
							$update[$co]++;
							if(false)
							if($co != 'pozycja')
							if(strlen($dana) < 32767 && strlen($produkt[$co] < 32767))
							{
								//$update['_'.$co] = $produkty->id;
								@var_dump($produktyAll[trim($daneProd['nazwa'])]);
								//$produkt['tekst'] = str_replace(PHP_EOL, '', $produkt['tekst']);//usuwa 10
								var_dump($produkty->id);
								var_dump($produkt['nazwa']);
								echo '<br><br>'.$co.'<br><br>';								
								var_dump(strlen($dana));
								echo '<br><br>';
								var_dump(strlen($produkt[$co]));
								echo '<br><br>';
								echo htmlspecialchars($dana).'<br><br>';
								echo htmlspecialchars($produkt[$co]).'<br><br>';
								var_dump($dana);
								echo '<br><br>';
								var_dump($produkt[$co]);
								echo '<br><br>';
								var_dump($dana != $produkt[$co]);
								echo '<br><br>';
								for($z=0;$z<strlen($dana);$z++)
								echo ord($dana[$z]).' ';
								echo '<br><br>';
								for($z=0;$z<strlen($produkt[$co]);$z++)
								echo ord($produkt[$co][$z]).' ';
								echo '<br><br>';
								//echo htmlspecialchars(chr(60));
								die();//</p> <p 13 + 10 zamiast 10 czyli spacja / enter ?
							}
						}
					}
				}
            }
			
			if(@count($produkt) == 0)
			if(@count($rozmiar) == 0)
			if(true && @count($dane[$i]) > 0)
			{
				//var_dump($dane[$i]);
				$daneKategorie = null;
				if(isset($dane[$i]['kategorie']))
				{
					$daneKategorie = $dane[$i]['kategorie'];
					unset($dane[$i]['kategorie']);
				}
				$daneKolory = null;
				if(isset($dane[$i]['kolory']))
				{
					$daneKolory = $dane[$i]['kolory'];
					unset($dane[$i]['kolory']);
				}
				$danePlatnosci = null;
				if(isset($dane[$i]['platnosci']))
				{
					$danePlatnosci = $dane[$i]['platnosci'];
					unset($dane[$i]['platnosci']);
				}
				$danePowiazania = null;
				if(isset($dane[$i]['powiazania']))
				{
					$danePowiazania = $dane[$i]['powiazania'];
					unset($dane[$i]['powiazania']);
				}
				unset($dane[$i]['id']);
				//var_dump($dane[$i]);die();
				$noweID = $produkty->dodajProdukty($dane[$i]);
				if($noweID > 0)
				{
					$sprawdz = $route->dodaj(trim($dane[$i]['nazwa']).'-'.$noweID, $noweID);
					$produkty->id = $noweID;
					$data['link'] = $sprawdz['link'];
					$data['route_id'] = $sprawdz['id'];
					$produkty->edytujProdukty($data);
					//$this->dodajRozmiaryAll($id, $this->produkt['rozmiarowka'], 0);
					//if(false && $this->obConfig->platnosciProdukt)
					//$this->dodajPlatnosciAll($id);
					$nowe++;
					
					if(!empty($daneKategorie))
					{
						$kategs = @explode('|', strval($daneKategorie));
						if(true && count($kategs) > 0)
						foreach($kategs as $kateg)
						if(!empty($kateg) && intval($kateg) > 0)
						$podkat->add(intval($kateg), $noweID);
					}
					if(!empty($daneKolory))
					{
						$koloryAll = @explode('|', strval($daneKolory));
						if(@count($koloryAll) > 0)
						foreach($koloryAll as $kolor)
						if(!empty($kolor) && intval($kolor) > 0)
						{
							$koloryDane = array('id_produktu' => $noweID, 'id_koloru' => intval($kolor));
							$koloryprod->dodaj($koloryDane);
						}
					}
					if(!empty($danePlatnosci))
					{
						$platnosciAll = @explode('|', strval($danePlatnosci));
						if(@count($platnosciAll) > 0)
						foreach($platnosciAll as $platnosc)
						if(!empty($platnosc) && intval($platnosc) > 0)
						{
							$platnosciDane = array('id_produkt' => $noweID, 'id_platnosci' => intval($platnosc));
							$platnosciprodukty->addWartosc($platnosciDane);
						}
					}
					if(!empty($danePowiazania))
					{
						$powiazaniaAll = @explode('|', strval($danePowiazania));
						if(@count($powiazaniaAll) > 0)
						foreach($powiazaniaAll as $powiazanie)
						if(!empty($powiazanie) && intval($powiazanie) > 0)
						{
							$powiazanieDane = array('id_prod' => $noweID, 'powiazany' => intval($powiazanie));
							$produktypowiazania->dodaj($powiazanieDane);
						}
					}
				}
				if($this->pole == 'id') $dane[$i]['id'] = $noweID;
				$produktyAll[trim($dane[$i][$this->pole])] = $dane[$i];
			}
			$ile++;
			//break;
        }

		$results['ile'] = $ile;
		$results['nowe'] = $nowe;
		$results['exist'] = $exist;
		$bledy .= 'Przetworzono '.$ile.' produkt(y)<br/>';
        $bledy .= 'Dodano '.$nowe.' nowych produktów<br/>';
		$bledy .= 'Zaktualizowano '.$exist.' produkt(y)(ów)<br/>';
		if(count($update) > 0)
		foreach($update as $co => $ilosc)
		if($ilosc > 0 && isset($this->opisy[$co]))
		{
			$bledy .= 'Zaktualizowano pole '.ucfirst($this->opisy[$co]).' dla '.$ilosc.' produktu(ów)<br/>';
			$results['pola'][$co] = $ilosc;
		}
		$this->bledy .= $bledy;
		return $results;
    }
	
	public function wczytajDaneXlsAll($plik) 
	{
        $data = new Spreadsheet_Excel_Reader();
        $data->setUTFEncoder('iconv');
        $data->setOutputEncoding('UTF-8');		
        
		$result = $data->read($plik);
		if(!empty($result)) $this->bledy .= $result.'<br/>';
		
		if(@count($data->sheets) > 0)
		$dane = $data->sheets[0];
		else return null;
		
		for($j = 1; $j <= $dane['numCols']; $j++)
		$kolumny[$j] = trim($dane['cells'][1][$j]);

        for($i = 2; $i <= $dane['numRows']; $i++)
		{
			for($j = 1; $j <= $dane['numCols']; $j++)
			{
				if(isset($dane['cells'][$i][$j]))
				$wiersze[$i - 2][$kolumny[$j]] = trim($dane['cells'][$i][$j]);                        
			}
        }
        return isset($wiersze) ? $wiersze : null;
    }
	
	public function wczytajDaneSqlAll($plik) 
	{
        $file = @fopen($plik, 'r');
		$sqlsAll = @fread($file, filesize($plik));
		@fclose($file);
		if(empty($sqlsAll)) return null;
		$sqls = explode('/*DELIM*/', $sqlsAll);
        return isset($sqls) ? $sqls : null;
    }
	
	function win2utf($tekst)
	{
		$tabelaAll = Array
		(
			"\xb9" => "\xc4\x85",
			"\xa5" => "\xc4\x84",
			"\xe6" => "\xc4\x87",
			"\xc6" => "\xc4\x86",
			"\xea" => "\xc4\x99",
			"\xca" => "\xc4\x98",
			"\xb3" => "\xc5\x82",
			"\xa3" => "\xc5\x81",
			"\xf3" => "\xc3\xb3",
			"\xd3" => "\xc3\x93",
			"\x9c" => "\xc5\x9b",
			"\x8c" => "\xc5\x9a",
			"\x9f" => "\xc5\xbc",
			"\xaf" => "\xc5\xbb",
			"\xbf" => "\xc5\xba",
			"\xac" => "\xc5\xb9",
			"\xf1" => "\xc5\x84",
			"\xd1" => "\xc5\x83"
		);
		$tabela = Array
		(
			"\xd3" => "\xc3\x93"
		);
		return strtr($tekst,$tabela);
	}
	
	public function wypakuj($Extras, $Zipfilename, $file) 
	{
        $zip = new ZipArchive();//echo filesize($Zipfilename);echo $file;
        if($zip->open($Zipfilename) !== TRUE) 
		{
            $this->bledy .= "Nie można otworzyć archiwum " . $Zipfilename . "<br>";
        }
        else if($zip->extractTo($Extras, $file) !== TRUE) 
		{
			if($zip->extractTo($Extras, strtoupper($file)) !== TRUE)
			{
				$this->bledy .= "Nie można wypakować pliku " . $file . " do ".$Extras."<br>";
			}
			else @$zip->close();
        }
        else @$zip->close();
    }
	
	public function wypakujAll($filenameZIP, $dir) 
	{
        $zip = new ZipArchive();
        if($zip->open($filenameZIP) !== TRUE) 
		{
            $this->bledy .= "Nie można otworzyć archiwum " . $filenameZIP . "<br>";
        }
        if($zip->extractTo($dir) !== TRUE) 
		{
            $this->bledy .= "Nie można wypakować pliku " . $filenameZIP . "<br>";
        }
        $zip->close();
    }

    public function dodajDoArchiwum($nazwa, $tab, $zdj = false) 
	{
        $bledy = '';
        $nazwaArchiwum = 'admin/bazy/' . $nazwa . '-' . date('Y-m-d') . '.zip';
        $zip = new ZipArchive();
        if ($zip->open($nazwaArchiwum, ZIPARCHIVE::CREATE) !== TRUE) 
		{
            $bledy .= 'Nie można utworzyć archiwum zip <br>';
        }
        $xls = 'admin/bazy/' . $nazwa . '-' . date('Y-m-d') . '.xls';
        $xlsname = $nazwa . '-' . date('Y-m-d') . '.xls';

        if ($zip->addFile($xls, $xlsname) !== TRUE) 
		{
            $bledy .= "Nie można dodać pliku " . $xlsname . " do archiwum<br>";
        }
		//var_dump($nazwa);

		if($zdj)
        for ($z = 0; $z < count($tab); $z++) 
		{
			//if($z > 2) break;
            $galeria = $tab[$z]['zdjecia'];
			if(empty($galeria)) continue;
            $pojedyncze = explode(",", $galeria);
            for ($g = 0; $g < count($pojedyncze); $g++)
			{
				if(empty($pojedyncze[$g])) continue;
                $zdjecie = 'admin/zdjecia/' . $pojedyncze[$g];
                $zdjeciename = $pojedyncze[$g];

                if ($zip->addFile($zdjecie, $zdjeciename) !== TRUE) 
				{
                    $bledy .= "Nie można dodać pliku " . $zdjeciename . " do archiwum<br>";
                }
            }
        }

        $zip->close();
        return $bledy;
    }
	
	function importDaty($rodzaj, $plik)
	{
		$settings = new Ustawienia();
		$ustawienia = $settings->wypisz();
		$importDaty = @unserialize($ustawienia['import']);
		$importDaty[$rodzaj]['data'] = date('Y-m-d H:i:s');
		$importDaty[$rodzaj]['plik'] = $plik;
		$importDaty = @serialize($importDaty);
		$settings->updateData(array('import' => $importDaty));
	}
}

class chunkReadFilter implements PHPExcel_Reader_IReadFilter
{
	private $_startRow = 0;
	private $_endRow = 0;
	private $headersRow = 1;

	/**  We expect a list of the rows that we want to read to be passed into the constructor  */
	public function __construct($startRow = 1, $chunkSize = 99999, $headersRow = 1)
	{
		$this->_startRow    = $startRow;
		$this->_endRow      = $startRow + $chunkSize;
		$this->headersRow    = $headersRow;
	}

	public function readCell($column, $row, $worksheetName = '')
	{
		//  Only read the heading row, and the rows that were configured in the constructor
		if(($row == $this->headersRow) || ($row >= $this->_startRow && $row < $this->_endRow))
		{
			return true;
		}
		return false;
	}
}
?>