<?php
class Allegroaukcje extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function wypiszAll()
	{
		$sql = $this->select()->order('na_ile');
		$result = $this->fetchAll($sql);
		return $result;
	}

	function wypiszJeden()
	{
		$select = $this->select()->where('id = "'.$this->id.'"');
		$result = $this->fetchAll($select);
		return $result;
	}
	function szukajAukcji($aukcja)
	{
		$select = $this->select()->where('nr = "'.$aukcja.'"');
		$result = $this->fetchRow($select);
		return $result;
	}
	function wypiszAukcjeProdukt()
	{
		$select = $this->select()->where('id_prod = "'.$this->id.'"');
		$result = $this->fetchAll($select);
		return $result;
	}
	function wypiszAukcjeRozmiar()
	{
		$select = $this->select()->where('id_rozm = "'.$this->id.'"');
		$result = $this->fetchAll($select);
		return $result;
	}
	
	function ileAukcjiDlaKategorii($id)
	{
		$select = $this->db->select()->from(array('a' => 'Allegroaukcje'), array('count(*)'))
			->join(array('kp' => 'Katprod'), 'a.id_prod = kp.id_prod', array(''))
			->group('a.nr')->where('kp.id_kat = '.$id);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return isset($result[0]['count(*)']) ? $result[0]['count(*)'] : 0;
	}
	
	function losoweAukcjeDlaKategorii($idKat, $idProd, $ile = 5)
	{
		$select = $this->db->select()->from(array('a' => 'Allegroaukcje'), array('nr'))
			->join(array('kp' => 'Katprod'), 'a.id_prod = kp.id_prod', array('id_prod'))
			->where('kp.id_kat = '.$idKat.' and a.id_prod != '.$idProd)
			->group('a.id_prod')->order('rand()')->limit($ile);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	
	function produktyNaAukcjach()
	{
		//$select = $this->select()->from(array('a' => 'Allegroaukcje'), array('count(*)'))->group('id_prod');
		$select = 'SELECT count(*) FROM (select id_prod from Allegroaukcje where (DATE_ADD(wystawiono, INTERVAL na_ile DAY)-NOW()) > 0 GROUP BY id_prod) as d';
		$result = $this->db->fetchAll($select);
		//echo $select;
		return isset($result[0]['count(*)']) ? $result[0]['count(*)'] : 0;
	}
	
	function konczace($godzin = 72)
	{
		//$select = $this->select()->from(array('a' => 'Allegroaukcje'), array('count(*)'))->group('id_prod');
		$timediff = 'TIMEDIFF(DATE_ADD(wystawiono, INTERVAL na_ile DAY), NOW())';
		$select = 'SELECT *, '.$timediff.' as do_konca FROM `Allegroaukcje` ';
		$select .= 'WHERE '.$timediff.' > "'.$godzin.':00:00" order by '.$timediff.' asc';
		$result = $this->db->fetchAll($select);
		//echo $select;
		return isset($result) ? $result : null;
	}
	function ileAukcji()
	{
		$select = 'SELECT count(*) FROM Allegroaukcje where (DATE_ADD(wystawiono, INTERVAL na_ile DAY)-NOW()) > 0';
		$result = $this->db->fetchAll($select);
		//echo $select;
		return isset($result[0]['count(*)']) ? $result[0]['count(*)'] : 0;
	}
	
	function zmien($dane)
	{
		$where = 'id = "' . $this->id.'"';
		$this->update($dane, $where);
	}		
	function usun()
	{
		$result = $this->delete('id = "'.$this->id.'"');
	}
	function usunAukcje($nr)
	{
		$result = $this->delete('nr = "'.$nr.'"');
	}
	function usunProdukt($id)
	{
		$result = $this->delete('id_prod = "'.$id.'"');			
	}
	function usunRozmiar($id)
	{
		$result = $this->delete('id_rozm = "'.$id.'"');			
	}
	
	function usunRozmiarAllegro($rozm)
	{
		$this->id = $rozm;
		$aukcje = $this->wypiszAukcjeRozmiar();
		if(count($aukcje) > 0)
		{
			$konto = new Allegrokonta();
			$konto->ID = 1;
			$config = $konto->klient();
			$webapi = $konto->zaloguj();
			if($webapi !== null)
			{								
				foreach($aukcje as $aukcja)
				{
					$this->finishItem($aukcja['nr'], $webapi);
				}
			}
		}
		$this->usunRozmiar($rozm);
	}
	function finishItem($item, $webapi = null)
	{
		$konto = new Allegrokonta();
		$konto->ID = 1;
		$config = $konto->klient();
		try
		{
			$opcje = array('item-id' => floatval($item), 'get-desc' => 0, 'get-image-url' => 0,
				'get-attribs' => 0, 'get-postage-options' => 0, 'get-company-info' => 0);
			if($webapi == null) $webapi = $konto->zaloguj();
			$info = $webapi->objectToArray($webapi->ShowItemInfoExt($opcje));
			$odwolaj = ($info["item-list-info-ext"]['it-bid-count'] > 0);
			$odwolaj = ($odwolaj && ($config['odwolaj'] == 'on')) ? 1 : 0;		
			if($info["item-list-info-ext"]['it-ending-info'] == 1)//aukcja trwa
			{
				$status = $webapi->FinishItem(array('finish-item-id' => floatval($item), 
					'finish-cancel-all-bids' => $odwolaj, 'finish-cancel-reason' => $config['odwolaj_powod']));
				if($status['finish-value'] == 1) return 'Aukcja została zakończona';
				if($status['finish-value'] == 0) return 'Allegro nie zakończyło aukcji ze skutkiem natychmiastowym, odśwież listę aukcji po pewnym czasie';
			}
			else return 'Aukcja została już wcześniej zakończona';
		}
		catch(SoapFault $error)
		{
			return 'Błąd w '.$item.' :'.$error->faultcode.': '.$error->faultstring."";
		}
	}
	function usunProduktAllegro($id)
	{
		$this->id = $id;
		$aukcje = $this->wypiszAukcjeProdukt();
		if(count($aukcje) > 0)
		{
			$konto = new Allegrokonta();
			$konto->ID = 1;
			$config = $konto->klient();
			$webapi = $konto->zaloguj();
			if($webapi !== null)
			{								
				foreach($aukcje as $aukcja)
				{
					$this->finishItem($aukcja['nr'], $webapi);
				}
			}
		}
		$this->usunProdukt($id);
	}
}
?>