<?php
class Rabatyprogi extends Zend_Db_Table
{
	public $link, $id, $typ;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = "'.$this->id.'"';
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$sql = $this->select()->where('1')->order(array('typ desc', 'od', 'do'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszWartosc($wartosc = 0, $typ = 'zamowienie')
	{
		$where = 'od <= '.$wartosc.' and do >= '.$wartosc.' and typ = "'.$typ.'"';
		$sql = $this->select()->where($where)->order(array('od', 'do'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszJeden($nazwa)
	{
		$sql = $this->select()->where('nazwa = "'.$nazwa.'"')->order('od');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function usun()
	{
		$result = $this->delete('id = "'.$this->id.'"');			
	}		
	function wypiszID()
	{
		$result = $this->fetchRow('id = "'.$this->id.'"');
		return $result;
	}	
}
?>