<?php
class Opinie extends Zend_Db_Table
{
	public $id;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
                $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodajOpinie($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	
	function edytujOpinie($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	
	function wypiszOpinie()
	{
		$result = $this->fetchAll();
		return $result;
	}
	
	function wypiszOpinieForProdukt($produkt = 0, $ile = 9999)
	{
		$where = 'id_prod = '.$produkt;
		$sql = $this->select()->where($where)->order('data desc')->limit($ile);
		$result = $this->fetchAll($sql);
		return $result;
	}
	
	function usunOpinie()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	function usunProdukt($id)
	{
		$result = $this->delete('id_prod = '.$id);
	}
	function usunProduktyIds($ids)
	{
		$result = $this->delete('id_prod in ('.$ids.')');
	}
	
	function wypiszPojedynczego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
}
?>