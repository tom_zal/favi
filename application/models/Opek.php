<?php
class Opek extends Zend_Db_Table
{
	public $id;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->module = $module;
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujZam($dane, $zam)
	{
		$where = 'deal_id = '.$zam;
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$result = $this->fetchAll();
		return $result;
	}
	function wypiszZam($id)
	{
		$result = $this->fetchAll('deal_id = '.$id);
		return $result;
	}
	function usunJeden()
	{
		$result = $this->delete('id = '.$this->id);
	}
	function wypiszRaport($data, $dataDo = null, $fraza = '')
	{
		$where = '1';
		if($dataDo == null) $dataDo = $data;
		$where.= ' and data >= "'.$data.' 00:00:00" and data <= "'.$dataDo.' 23:59:59"';
		if(!empty($fraza))
		{
			$where.= ' and (0';
			if(intval($fraza) > 0)
			{
				$where.= ' or buyer_id = '.$fraza;
				$where.= ' or deal_id = '.$fraza;
				$where.= ' or list = '.$fraza;
				$where.= ' or paczka = '.$fraza;
			}
			$where.= ' or config like "%'.$fraza.'%"';
			$where.= ' or user like "%'.$fraza.'%"';
			$where.= ')';
		}
		$where.= ' and user <> ""';
		$where.= ' and wyslano = 1';
		//echo $where;//return null;
		$sql = $this->select()->where($where)->group('list')->order('data');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function edytujData($dane, $data)
	{
		$where = '1';
		$where.= ' and data >= "'.$data.' 00:00:00" and data <= "'.$data.' 23:59:59"';
		//$where.= ' and wyslano = 1';
		$this->update($dane, $where);
	}

	function wypiszJeden()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	
	function getNrListu()
	{
		$sql = $this->select()->from(array('o' => 'Opek'), array('max(list)'));
		$result = $this->fetchRow($sql);
		$nr = intval($result['max(list)']);
		$opek = new Opekdane();
		$dane = $opek->wypisz();
		if($nr == 0) return intval($dane['list_min']);
		if($nr >= intval($dane['list_max'])) return -1;
		if($nr == -1) return -1;
		return ++$nr;
	}
	function getNrPaczki()
	{
		$sql = $this->select()->from(array('o' => 'Opek'), array('max(paczka)'));
		$result = $this->fetchRow($sql);
		$nr = intval($result['max(paczka)']);
		$opek = new Opekdane();
		$dane = $opek->wypisz();
		if($nr == 0) return intval($dane['paczka_min']);
		if($nr >= intval($dane['paczka_max'])) return -1;
		if($nr == -1) return -1;
		return ++$nr;
	}
	function getNrDealu()
	{
		$sql = $this->select()->from(array('o' => 'Opek'), array('min(deal_id)'))->where('deal_id < 0');
		$result = $this->fetchRow($sql);
		$nr = @intval($result['min(deal_id)']);
		return --$nr;
	}
	
	function plikWymianyDanych($dane)
	{
		$plik = '';//;/ 3137;WAW;POZ;N;P;19527658;…
		$plik.= ';/3137;';//&#22; w HTML
		$plik.= $dane['config']['StacjaN'].';';
		$plik.= $dane['config']['StacjaO'].';';
		$plik.= $dane['config']['KtoPlaci'].';';
		$plik.= $dane['config']['RodzajPlatnosci'].';';
		$plik.= $dane['list'].';';
		$plik.= '1;';//ilosc paczek
		$plik.= (intval($dane['config']['Waga']) * 100).';';
		$plik.= $dane['config']['DataN'].';';
		$plik.= intval(floatval($dane['config']['kwotaPobrania']) * 100).';';
		$plik.= intval(floatval($dane['config']['kwotaUbezpieczenia']) * 100).';';
		$plik.= $dane['config']['NrKonta'].';';
		$plik.= $dane['config']['NrKurieraN'].';';
		$plik.= $dane['config']['NrZewnetrzny'].';';
		$plik.= $dane['config']['NrZewnetrzny2'].';';
		$plik.= $dane['config']['Serial'].';';
		$plik.= $dane['config']['NrKlientaN'].';';
		$plik.= $dane['config']['NazwaFirmyN'].';';
		$plik.= $dane['config']['MiastoN'].';';
		$plik.= $dane['config']['UlicaN'].';';
		$plik.= str_replace('-', '', $dane['config']['KodPocztowyN']).';';
		$plik.= $dane['config']['TelefonN'].';';
		$plik.= $dane['config']['NazwiskoN'].';';
		$plik.= $dane['user']['NrKlientaO'].';';
		$plik.= mb_substr($dane['user']['NazwaFirmyO'], 0, 40, 'UTF-8').';';
		$plik.= mb_substr($dane['user']['MiastoO'], 0, 40, 'UTF-8').';';
		$plik.= mb_substr($dane['user']['UlicaO'], 0, 40, 'UTF-8').';';
		$plik.= substr(str_replace('-', '', $dane['user']['KodPocztowyO']), 0, 5).';';
		$plik.= mb_substr($dane['user']['TelefonO'], 0, 40, 'UTF-8').';';
		$plik.= mb_substr($dane['user']['NazwiskoO'], 0, 40, 'UTF-8').';';
		$TP = ($dane['config']['KtoPlaci'] == 'T');
		$plik.= ($TP ? $dane['config']['NrKlientaP'] : '').';';
		$plik.= ($TP ? $dane['config']['NazwaFirmyP'] : '').';';
		$plik.= ($TP ? $dane['config']['MiastoP'] : '').';';
		$plik.= ($TP ? $dane['config']['UlicaP'] : '').';';
		$plik.= ($TP ? str_replace('-', '', $dane['config']['KodPocztowyP']) : '').';';
		$plik.= ($TP ? $dane['config']['TelefonP'] : '').';';
		$plik.= ($TP ? $dane['config']['NazwiskoP'] : '').';';
		$plik.= $dane['config']['UslugiSpecjalne'].';';
		$plik.= $dane['config']['NumerFaktury'].';';
		$plik.= intval(floatval($dane['config']['CenaNetto']) * 100).';';
		$plik.= intval(floatval($dane['config']['CenaBrutto']) * 100).';';
		$plik.= intval(floatval($dane['config']['OpłataPaliwowaProcent']) * 10).';';
		$plik.= intval(floatval($dane['config']['OpłataPaliwowaCenaNetto']) * 100).';';
		$plik.= intval(floatval($dane['config']['OpłataPaliwowaCenaBrutto']) * 100).';';
		$plik.= mb_substr($dane['config']['opisZawartosci'], 0, 250, 'UTF-8').';';
		$plik.= '0;';//zastrzeżone 1
		$plik.= '0;';//zastrzeżone 2
		$plik.= '0;';//zastrzeżone 3
		$plik.= ';';//zastrzeżone 4
		$plik.= ';';//zastrzeżone 5
		$plik.= ';';//zastrzeżone 6
		$plik.= ';';//zastrzeżone 7
		$plik.= ';';//zastrzeżone 8
		$plik.= $dane['paczka'].'/;';//lista paczek
		$plik.= ';';//zakresy wagowe
		$plik.= "\r\n";//PHP_EOL;//0D i 0A czyli #13#10
		return $plik;
	}
	
	function etykietaOpekTest($zendPDF, $user, $config, $nrListu, $nrPaczki)
	{
		require_once 'tcpdf/tcpdf.php';
		$zendPDF = new Zend_Pdf();
					
		$listWidth = 420;//532;
		$listHeight = 756;//280;//228;//756;//252 * 3
		$pageNew = $zendPDF->newPage($listWidth.':'.$listHeight.':');
		
		$barcodeFile = 'admin/barcodes/test.png';
		
		$barcodeLeft = 0;
		$barcodeTop = 0;
		$barcodeWidth = 420;//org 115 x 62 skala 0.54
		$barcodeHeight = 756;
		//var_dump($barcodeFile);
		$barcodeImage = Zend_Pdf_Image::imageWithPath($barcodeFile);
		$pageNew->drawImage($barcodeImage, $barcodeLeft, $barcodeTop, $barcodeLeft + $barcodeWidth, $barcodeTop + $barcodeHeight);
		
		$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD);
		$pageNew->setFont($font, 12);
		$pageNew->drawText(2352353465, 100, 100, 'UTF-8');
		
		return $pageNew;
	}
	
	function etykietaOpek($zendPDF, $user, $config, $nrListu, $nrPaczki)
	{
		//require_once 'tcpdf/tcpdf.php';
		$zendPDF = new Zend_Pdf();
					
		$pageWidth = 280;
		$pageHeight = 420;//448
		$pageNew = $zendPDF->newPage($pageWidth.':'.$pageHeight.':');
		
		//$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD);
		$font = Zend_Pdf_Font::fontWithPath('fonts/times.ttf');
		$fontBold = Zend_Pdf_Font::fontWithPath('fonts/timesbd.ttf');
		$tahoma = Zend_Pdf_Font::fontWithPath('fonts/tahoma.ttf');
		$tahomaBold = Zend_Pdf_Font::fontWithPath('fonts/tahomabd.ttf');
		$arial = Zend_Pdf_Font::fontWithPath('fonts/arial.ttf');
		$arialBold = Zend_Pdf_Font::fontWithPath('fonts/arialbd.ttf');
		$pageNew->setFont($font, 12);
		
		$width = $pageWidth;
		$height = $pageHeight;
		$top = 0;
		$left = 5;
		$bottom = 0;
		$right = 5;
		
		//$polaTop = array(12,40,97,153,205,258,337,426,436,448);
		$polaTop = array(12,40,97,153,205,258,337,398,408,420);
		foreach($polaTop as $pt => $poleTop)
		$polaTop[$pt] = $pageHeight - $poleTop;
		
		$poleNr = 0;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr];
		
		$pageNew->setFont($fontBold, 14);
		//$pageNew->drawText($config['StacjaN'], $left, $top + 1, 'UTF-8');
		
		$opekLogo = 'OPEK Sp. z o.o.  Tel. (22)7327999';
		$pageNew->setFont($font, 10);
		$pageNew->drawText($opekLogo, 63, $top + 3, 'UTF-8');
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;						
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($font, 10);
		$pageNew->drawText('Nr listu:', $left, $top + 20, 'UTF-8');						
		$pageNew->setFont($fontBold, 14);
		$pageNew->drawText($nrListu, $left, $top + 4, 'UTF-8');
		
		if($nrListu > 0)
		{
			$barcodeFile = 'admin/barcodes/'.$nrListu.'.png';
			$this->common->drawBarcode128('CODE128_A', $nrListu, $barcodeFile);
			
			$barcodeLeft = 84;
			$barcodeTop = $top + 2;
			$barcodeWidth = 156;//org 115 x 62 skala 0.54
			$barcodeHeight = 24;
			//var_dump($barcodeFile);
			$barcodeImage = Zend_Pdf_Image::imageWithPath($barcodeFile);
			$pageNew->drawImage($barcodeImage, $barcodeLeft, $barcodeTop, $barcodeLeft + $barcodeWidth, $barcodeTop + $barcodeHeight);
		}
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Nadawca:', $left, $top + 50, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText($config['NazwaFirmyN'], $left, $top + 42, 'UTF-8');
		$pageNew->drawText($config['NazwiskoN'], $left, $top + 30, 'UTF-8');
		$pageNew->drawText($config['UlicaN'], $left, $top + 22, 'UTF-8');
		$KodPocztowyN = substr(str_replace('-', '', $config['KodPocztowyN']), 0, 5);
		$pageNew->drawText($KodPocztowyN.', '.$config['MiastoN'], $left, $top + 14, 'UTF-8');
		$pageNew->drawText('Tel: '.$config['TelefonN'], $left, $top + 2, 'UTF-8');
		
		$leftNow = $pageWidth - 160;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $top, $leftNow, $top - $height);
		
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Kod poczt. odb.:', $leftNow + 2, $top + 50, 'UTF-8');
		$KodPocztowyO = substr(str_replace('-', '', $user['KodPocztowyO']), 0, 5);
		if($KodPocztowyO > 0)
		{
			$barcodeFile = 'admin/barcodes/'.$KodPocztowyO.'.png';
			$this->common->drawBarcode128('CODE128_A', $KodPocztowyO, $barcodeFile);
			
			$barcodeLeft = $leftNow + 30;
			$barcodeTop = $top - 20;
			$barcodeWidth = 100;
			$barcodeHeight = 62;
			//var_dump($barcodeFile);
			$barcodeImage = Zend_Pdf_Image::imageWithPath($barcodeFile);
			$pageNew->drawImage($barcodeImage, $barcodeLeft, $barcodeTop, $barcodeLeft + $barcodeWidth, $barcodeTop + $barcodeHeight);
			
			$pageNew->setFont($fontBold, 12);
			$pageNew->drawText($KodPocztowyO, $barcodeLeft, $barcodeTop - 12, 'UTF-8');
		}
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth - 160, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Odbiorca:', $left, $top + 50, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText($user['NazwaFirmyO'], $left, $top + 42, 'UTF-8');
		$pageNew->drawText($user['UlicaO'], $left, $top + 22, 'UTF-8');
		$pageNew->drawText($user['NazwiskoO'], $left, $top + 30, 'UTF-8');
		$KodPocztowyO = substr(str_replace('-', '', $user['KodPocztowyO']), 0, 5);
		$pageNew->drawText($KodPocztowyO.', '.$user['MiastoO'], $left, $top + 14, 'UTF-8');
		$pageNew->drawText('Tel: '.$user['TelefonO'], $left, $top + 2, 'UTF-8');
		
		$leftNow = $pageWidth - 160;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $top, $leftNow, $top - $height);
		$leftNow++;
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Waga deklar.:', $leftNow + 30, $top + 14, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$waga = number_format(floatval($config['Waga']), 1, ',', ' ');
		$pageNew->drawText($waga.' kg', $leftNow + 30, $top + 5, 'UTF-8');
		$topNow = $top + 20;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow - 1, $topNow, $pageWidth, $topNow);
		$leftNow = $pageWidth - 60;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $top, $leftNow, $topNow);
		$leftNow++;
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Paleta:', $leftNow, $top + 14, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText('EU:--', $leftNow, $top + 5, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText('EP:--', $leftNow + 30, $top + 5, 'UTF-8');
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];		
		
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText($config['NrZewnetrzny'], $left, $top + 44, 'UTF-8');
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Opis:', $left, $top + 33, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$opisLines = $this->common->getPdfLines($config['opisZawartosci'], 40);
		$l = 1;
		if(count($opisLines) > 0)
		foreach($opisLines as $line)
		{
			if($l > 3) break;
			$pageNew->drawText($line, $left, $top + 33 - $l * 10, 'UTF-8');
			$l++;
		}
		
		$leftNow = 170;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $top, $leftNow, $top - $height);
		
		$topNow = $top + 26;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $topNow, $pageWidth - 83, $topNow);
		$leftNow++;
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Płatnik:', $leftNow, $topNow + 20, 'UTF-8');
		$pageNew->setFont($tahoma, 24);
		$pageNew->drawText($config['KtoPlaci'], $leftNow + 5, $topNow + 1, 'UTF-8');
		$topNow = $top + 0;
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Typ płat.:', $leftNow, $topNow + 20, 'UTF-8');
		$pageNew->setFont($tahoma, 24);
		$pageNew->drawText($config['RodzajPlatnosci'], $leftNow + 5, $topNow + 1, 'UTF-8');		
		
		$topNow = $top + 16;
		$leftNow = $pageWidth - 83;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $top, $leftNow, $top - $height);
		$leftNow++;
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Pobranie:', $leftNow, $topNow + 30, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$kwotaPobrania = number_format($config['kwotaPobrania'], 2, ',', ' ');
		$pageNew->drawText($kwotaPobrania.' zł', $leftNow, $topNow + 20, 'UTF-8');
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Ubezpieczenie:', $leftNow, $topNow + 12, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$kwotaUbezpieczenia = number_format($config['kwotaUbezpieczenia'], 2, ',', ' ');
		$pageNew->drawText($kwotaUbezpieczenia.' zł', $leftNow, $topNow + 2, 'UTF-8');	
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Ilość paczek:', $left, $top + 46, 'UTF-8');
		$pageNew->setFont($arial, 14);
		$pageNew->drawText('1 z 1', $left, $top + 27, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText('Data nadania:', $left, $top + 17, 'UTF-8');
		$pageNew->setFont($tahoma, 10);
		$pageNew->drawText(date('Y-m-d H:i', strtotime($config['DataN'])), $left, $top + 8, 'UTF-8');
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Kierunek:', $left + 60, $top + 46, 'UTF-8');
		$pageNew->setFont($tahoma, 16);
		$pageNew->drawText($config['StacjaN'].'->', $left + 59, $top + 24, 'UTF-8');
		$pageNew->setFont($tahoma, 40);
		$pageNew->drawText($config['StacjaO'], $left + 112, $top + 7, 'UTF-8');	
		
		$leftNow = $pageWidth - 66;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $top, $leftNow, $top - $height);
		$leftNow++;
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Strefa doręczenia:', $leftNow, $top + 46, 'UTF-8');
		$pageNew->setFont($arial, 42);
		//$pageNew->drawText('', $leftNow, $top + 7, 'UTF-8');
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Numer paczki:', $left, $top + 72, 'UTF-8');
		
		//$nrPaczki = $data[$buyer]['paczka'];
		if($nrPaczki > 0)
		{
			$barcodeFile = 'admin/barcodes/'.$nrPaczki.'.png';
			$this->common->drawBarcode128('CODE128_A', 'Z'.$nrPaczki, $barcodeFile);
			
			$barcodeLeft = 29;
			$barcodeTop = $top + 6;
			$barcodeWidth = 167;
			$barcodeHeight = 62;
			//var_dump($barcodeFile);
			$barcodeImage = Zend_Pdf_Image::imageWithPath($barcodeFile);
			$pageNew->drawImage($barcodeImage, $barcodeLeft, $barcodeTop, $barcodeLeft + $barcodeWidth, $barcodeTop + $barcodeHeight);
			
			$pageNew->setFont($fontBold, 12);
			$pageNew->drawText($nrPaczki, $barcodeLeft, $barcodeTop - 12, 'UTF-8');
		}
		
		//$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$topNow = $top;
		$leftNow = $pageWidth - 66;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $topNow, $leftNow, $topNow + 52);
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $topNow + 52, $pageWidth, $topNow + 52);
		$leftNow++;
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Grupa:', $leftNow, $top + 46, 'UTF-8');
		$pageNew->setFont($arial, 42);
		//$pageNew->drawText('G4', $leftNow, $top + 7, 'UTF-8');
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Druk: '.date('Y-m-d H:i:s'), $left, $top + 3, 'UTF-8');
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Oprogramowanie zewnętrzne dla v3.1.30', 121, $top + 3, 'UTF-8');
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Producent oprog.: BigCom     www: bigcom.pl', $left, $top + 5, 'UTF-8');

		//$zendPDF->pages[] = $pageNew;
		//return $zendPDF;
		return $pageNew;
	}
	
	function etykietaOpekWersjaOld($zendPDF, $user, $config, $nrListu, $nrPaczki)
	{
		require_once 'tcpdf/tcpdf.php';
		$zendPDF = new Zend_Pdf();
					
		$pageWidth = 280;
		$pageHeight = 420;//448
		$pageNew = $zendPDF->newPage($pageWidth.':'.$pageHeight.':');
		
		//$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD);
		$font = Zend_Pdf_Font::fontWithPath('fonts/times.ttf');
		$fontBold = Zend_Pdf_Font::fontWithPath('fonts/timesbd.ttf');
		$tahoma = Zend_Pdf_Font::fontWithPath('fonts/tahoma.ttf');
		$tahomaBold = Zend_Pdf_Font::fontWithPath('fonts/tahomabd.ttf');
		$arial = Zend_Pdf_Font::fontWithPath('fonts/arial.ttf');
		$arialBold = Zend_Pdf_Font::fontWithPath('fonts/arialbd.ttf');
		$pageNew->setFont($font, 12);
		
		$width = $pageWidth;
		$height = $pageHeight;
		$top = 0;
		$left = 5;
		$bottom = 0;
		$right = 5;
		
		//$polaTop = array(12,40,97,153,205,258,337,426,436,448);
		$polaTop = array(12,40,97,153,205,258,337,398,408,420);
		foreach($polaTop as $pt => $poleTop)
		$polaTop[$pt] = $pageHeight - $poleTop;
		
		$poleNr = 0;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr];
		
		$pageNew->setFont($fontBold, 14);
		$pageNew->drawText($config['StacjaN'], $left, $top + 1, 'UTF-8');
		
		$opekLogo = 'OPEK Sp. z o.o. (22) 732 79 99';
		$pageNew->setFont($font, 10);
		$pageNew->drawText($opekLogo, 63, $top + 3, 'UTF-8');
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;						
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($font, 10);
		$pageNew->drawText('Nr listu:', $left, $top + 20, 'UTF-8');						
		$pageNew->setFont($fontBold, 14);
		$pageNew->drawText($nrListu, $left, $top + 4, 'UTF-8');
		if($nrListu > 0)
		{
			$barcodeFile = 'admin/barcodes/'.$nrListu.'.png';
			$this->common->drawBarcode128('CODE128_A', $nrListu, $barcodeFile);
			
			$barcodeLeft = 84;
			$barcodeTop = $top + 2;
			$barcodeWidth = 156;//org 115 x 62 skala 0.54
			$barcodeHeight = 24;
			//var_dump($barcodeFile);
			$barcodeImage = Zend_Pdf_Image::imageWithPath($barcodeFile);
			$pageNew->drawImage($barcodeImage, $barcodeLeft, $barcodeTop, $barcodeLeft + $barcodeWidth, $barcodeTop + $barcodeHeight);
		}
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Nadawca:', $left, $top + 50, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText($config['NazwaFirmyN'], $left, $top + 42, 'UTF-8');
		$pageNew->drawText($config['NazwiskoN'], $left, $top + 30, 'UTF-8');
		$pageNew->drawText($config['UlicaN'], $left, $top + 22, 'UTF-8');
		$pageNew->drawText($config['KodPocztowyN'].', '.$config['MiastoN'], $left, $top + 14, 'UTF-8');
		$pageNew->drawText('Tel: '.$config['TelefonN'], $left, $top + 2, 'UTF-8');
		
		$leftNow = $pageWidth - 95;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $top, $leftNow, $top - $height);
		
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Kod poczt. odb.', $leftNow + 2, $top + 50, 'UTF-8');
		$KodPocztowyO = substr(str_replace('-', '', $user['KodPocztowyO']), 0, 5);
		if($KodPocztowyO > 0)
		{
			$barcodeFile = 'admin/barcodes/'.$KodPocztowyO.'.png';
			$this->common->drawBarcode128('CODE128_A', $KodPocztowyO, $barcodeFile, 1);
			
			$barcodeLeft = $leftNow + 13;
			$barcodeTop = $top + 8;
			$barcodeWidth = 65;
			$barcodeHeight = 32;
			//var_dump($barcodeFile);
			$barcodeImage = Zend_Pdf_Image::imageWithPath($barcodeFile);
			$pageNew->drawImage($barcodeImage, $barcodeLeft, $barcodeTop, $barcodeLeft + $barcodeWidth, $barcodeTop + $barcodeHeight);
		}
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Odbiorca:', $left, $top + 50, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText($user['NazwaFirmyO'], $left, $top + 42, 'UTF-8');
		$pageNew->drawText($user['UlicaO'], $left, $top + 22, 'UTF-8');
		$pageNew->drawText($user['NazwiskoO'], $left, $top + 30, 'UTF-8');	
		$pageNew->drawText($user['KodPocztowyO'].', '.$user['MiastoO'], $left, $top + 14, 'UTF-8');
		$pageNew->drawText('Tel: '.$user['TelefonO'], $left, $top + 2, 'UTF-8');
		
		$leftNow = $pageWidth - 95;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $top, $leftNow, $top - $height);
		$leftNow++;
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Waga dostar.:', $leftNow, $top + 12, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText($config['Waga'].' kg', $leftNow, $top + 3, 'UTF-8');
		$leftNow = $pageWidth - 95;
		$topNow = $top + 20;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $topNow, $pageWidth - 33, $topNow);
		$leftNow++;
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Pobranie:', $leftNow, $topNow + 30, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText($config['kwotaPobrania'].' zł', $leftNow, $topNow + 20, 'UTF-8');
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Ubezpieczenie:', $leftNow, $topNow + 12, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText($config['kwotaUbezpieczenia'].' zł', $leftNow, $topNow + 2, 'UTF-8');	
		
		$leftNow = $pageWidth - 33;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $top, $leftNow, $top - $height);
		$leftNow++;
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Płatnik:', $leftNow, $top + 22, 'UTF-8');
		$pageNew->setFont($tahoma, 24);
		$pageNew->drawText($config['KtoPlaci'], $leftNow + 8, $top + 1, 'UTF-8');
		$leftNow = $pageWidth - 33;
		$topNow = $top + 28;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $topNow, $pageWidth, $topNow);
		$leftNow++;
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Typ płat.:', $leftNow, $topNow + 22, 'UTF-8');
		$pageNew->setFont($tahoma, 24);
		$pageNew->drawText($config['RodzajPlatnosci'], $leftNow + 8, $topNow + 1, 'UTF-8');
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText($config['NrZewnetrzny'], $left, $top + 44, 'UTF-8');
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Opis:', $left, $top + 33, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$opisLines = $this->common->getPdfLines($config['opisZawartosci'], 40);
		$l = 1;
		if(count($opisLines) > 0)
		foreach($opisLines as $line)
		{
			if($l > 3) break;
			$pageNew->drawText($line, $left, $top + 33 - $l * 10, 'UTF-8');
			$l++;
		}
		
		$leftNow = 170;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $top, $leftNow, $top - $height);
		$leftNow++;
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Paleta:', $leftNow, $top + 45, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText('EU: 0', $leftNow, $top + 26, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText('EP: 0', $leftNow, $top + 9, 'UTF-8');
		
		$leftNow = $pageWidth - 83;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $top, $leftNow, $top - $height);
		$leftNow++;
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Strefa doręczenia:', $leftNow, $top + 45, 'UTF-8');
		$pageNew->setFont($arialBold, 44);
		//$pageNew->drawText('143', $leftNow + 3, $top + 6, 'UTF-8');
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Ilość paczek:', $left, $top + 46, 'UTF-8');
		$pageNew->setFont($arial, 14);
		$pageNew->drawText('1 z 1', $left, $top + 27, 'UTF-8');
		$pageNew->setFont($tahoma, 8);
		$pageNew->drawText('Data nadania:', $left, $top + 17, 'UTF-8');
		$pageNew->setFont($tahoma, 10);
		$pageNew->drawText($config['DataN'], $left, $top + 8, 'UTF-8');
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Kierunek:', $left + 60, $top + 46, 'UTF-8');
		$pageNew->setFont($tahoma, 16);
		$pageNew->drawText($config['StacjaN'].'->', $left + 59, $top + 24, 'UTF-8');
		$pageNew->setFont($tahoma, 40);
		$pageNew->drawText($config['StacjaO'], $left + 112, $top + 7, 'UTF-8');	
		
		$leftNow = $pageWidth - 66;
		$pageNew->setLineWidth(0.5)->drawLine($leftNow, $top, $leftNow, $top - $height);
		$leftNow++;
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Grupa:', $leftNow, $top + 46, 'UTF-8');
		$pageNew->setFont($arial, 42);
		//$pageNew->drawText('G4', $leftNow, $top + 7, 'UTF-8');
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Numer paczki:', $left, $top + 72, 'UTF-8');
		
		//$nrPaczki = $data[$buyer]['paczka'];
		if($nrPaczki > 0)
		{
			$barcodeFile = 'admin/barcodes/'.$nrPaczki.'.png';
			$this->common->drawBarcode128('CODE128_A', 'Z'.$nrPaczki, $barcodeFile, 1);
			
			$barcodeLeft = 19;
			$barcodeTop = $top + 6;
			$barcodeWidth = 167;
			$barcodeHeight = 62;
			//var_dump($barcodeFile);
			$barcodeImage = Zend_Pdf_Image::imageWithPath($barcodeFile);
			$pageNew->drawImage($barcodeImage, $barcodeLeft, $barcodeTop, $barcodeLeft + $barcodeWidth, $barcodeTop + $barcodeHeight);
		}
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Druk: '.date('Y-m-d H:i:s'), $left, $top + 3, 'UTF-8');
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Oprogramowanie zewnętrzne dla v3.1.30', 121, $top + 3, 'UTF-8');
		
		$pageNew->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $polaTop[$poleNr];
		$height = $polaTop[$poleNr] - $polaTop[$poleNr - 1];
		
		$pageNew->setFont($tahoma, 6);
		$pageNew->drawText('Producent oprog.: BigCom     www: bigcom.pl', $left, $top + 5, 'UTF-8');

		//$zendPDF->pages[] = $pageNew;
		//return $zendPDF;
		return $pageNew;
	}
	
	function listOpek($pageNewL, $user, $config, $nrListu, $nrPaczki, $listKrok, $listIleNaStrone)
	{
		require_once 'tcpdf/tcpdf.php';
		$zendPDFlist = new Zend_Pdf();
		
		$listWidth = 420;//532;
		$listHeight = 756;//280;//228;//756;//252 * 3
		//$listIleNaStrone = 1;
		$pageWidth = $listWidth;
		$pageHeight = floor($listHeight / $listIleNaStrone);
		
		//$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD);
		$font = Zend_Pdf_Font::fontWithPath('fonts/times.ttf');
		$fontBold = Zend_Pdf_Font::fontWithPath('fonts/timesbd.ttf');
		$tahoma = Zend_Pdf_Font::fontWithPath('fonts/tahoma.ttf');
		$tahomaBold = Zend_Pdf_Font::fontWithPath('fonts/tahomabd.ttf');
		$arial = Zend_Pdf_Font::fontWithPath('fonts/arial.ttf');
		$arialBold = Zend_Pdf_Font::fontWithPath('fonts/arialbd.ttf');
		$pageNewL->setFont($font, 12);
		
		//$user = $data[$buyer]['user'];
		
		$width = $pageWidth;
		$height = $pageHeight;
		$top = 0;
		$left = 5;
		$bottom = 0;
		$right = 5;
		
		$topAll = $listHeight - ($listKrok % $listIleNaStrone + 1) * $pageHeight;
		//$topAll -= 13;
		$topNow = $topAll;
		//var_dump($listHeight);
		//var_dump($listKrok);
		//var_dump($listIleNaStrone);
		//var_dump($topAll);
		
		$polaTops = array(22,114,240,252);
		foreach($polaTops as $pt => $poleTop)
		$polaTop[$pt] = $pageHeight - $poleTop;
		
		$poleNr = 0;
		$top = $topAll + $polaTop[$poleNr];
		$height = $polaTops[$poleNr];
		
		$leftNow = $pageWidth / 2 - 62;
		$pageNewL->setLineWidth(0.5)->drawLine($leftNow, $top + $height, $leftNow, $top);
		$pageNewL->setFont($fontBold, 22);
		$pageNewL->drawText($config['StacjaN'], $leftNow + 1, $top + 2, 'UTF-8');						
		$pageNewL->setLineWidth(0.5)->drawLine($pageWidth / 2, $top + $height, $pageWidth / 2, $top);
		$leftNow = $pageWidth / 2;
		$pageNewL->setFont($fontBold, 22);
		$pageNewL->drawText($config['StacjaO'], $leftNow + 1, $top + 2, 'UTF-8');
		$leftNow = $pageWidth / 2 + 62;
		$pageNewL->setLineWidth(0.5)->drawLine($leftNow, $top + $height, $leftNow, $top);
		$pageNewL->setLineWidth(0.5)->drawLine($pageWidth/2-62,$top+$height,$pageWidth/2+62,$top+$height);
		
		$KtoPlaci = '';
		if($config['KtoPlaci'] == 'N') $KtoPlaci = 'Koszt nadawcy';
		if($config['KtoPlaci'] == 'O') $KtoPlaci = 'Koszt odbiorcy';
		if($config['KtoPlaci'] == 'T') $KtoPlaci = 'Koszt trzeciego płatnika';
		$pageNewL->setFont($tahoma, 8);
		$pageNewL->drawText($KtoPlaci, $leftNow + 4, $top + 12, 'UTF-8');
		$RodzajPlatnosci = '';
		if($config['RodzajPlatnosci'] == 'P') $RodzajPlatnosci = 'Przelew';
		if($config['RodzajPlatnosci'] == 'G') $RodzajPlatnosci = 'Gotówka';
		$pageNewL->setFont($tahoma, 8);
		$pageNewL->drawText($RodzajPlatnosci, $leftNow + 4, $top + 3, 'UTF-8');
		
		$pageNewL->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $topAll + $polaTop[$poleNr];
		$height = $polaTops[$poleNr] - $polaTops[$poleNr - 1];
		
		$leftNow = 1;
		$pageNewL->setFont($tahoma, 8);
		$pageNewL->drawText('Firma:', $leftNow, $top + 78, 'UTF-8');
		$pageNewL->drawText('Nazwisko:', $leftNow, $top + 54, 'UTF-8');
		$pageNewL->drawText('Adres:', $leftNow, $top + 41, 'UTF-8');
		$pageNewL->drawText('Kod:', $leftNow, $top + 29, 'UTF-8');
		$pageNewL->drawText('MPK:', $leftNow, $top + 13, 'UTF-8');
		$leftNow = 39;
		$pageNewL->setFont($tahoma, 11);
		$NazwaFirmyN = $config['NazwaFirmyN'];
		if(!empty($config['NazwaFirmyN'])) $NazwaFirmyN .= ' ';
		$NazwaFirmyN.= '('.$config['NrKlientaN'].')';
		$NazwaFirmyN = $this->common->getPdfLines($NazwaFirmyN, 30);
		$l = 0;
		if(count($NazwaFirmyN) > 0)
		foreach($NazwaFirmyN as $NFN)
		{
			if($l > 2) break;
			$pageNewL->drawText($NFN, $leftNow, $top + 78 - $l++ * 12, 'UTF-8');
		}
		//$pageNewL->drawText($NazwaFirmyN, $leftNow, $top + 78, 'UTF-8');
		$pageNewL->drawText($config['NazwiskoN'], $leftNow, $top + 54, 'UTF-8');
		$pageNewL->drawText($config['UlicaN'], $leftNow, $top + 41, 'UTF-8');
		$pageNewL->drawText(str_replace('-','',$config['KodPocztowyN']),$leftNow,$top+29,'UTF-8');
		$pageNewL->drawText($config['NrZewnetrzny'], $leftNow, $top + 13, 'UTF-8');
		$pageNewL->setFont($tahoma, 8);
		$pageNewL->drawText('Miasto:', $leftNow + 37, $top + 29, 'UTF-8');
		$pageNewL->drawText('Telefon:', $leftNow + 82, $top + 17, 'UTF-8');
		$pageNewL->setFont($tahoma, 11);
		$pageNewL->drawText($config['MiastoN'], $leftNow + 64, $top + 29, 'UTF-8');
		$pageNewL->drawText($config['TelefonN'], $leftNow + 95, $top + 5, 'UTF-8');
		
		$pageNewL->setLineWidth(0.5)->drawLine($pageWidth / 2, $top + $height, $pageWidth / 2, $top);
		
		$leftNow = $pageWidth / 2 + 1;
		$pageNewL->setFont($tahoma, 8);
		$pageNewL->drawText('Firma:', $leftNow, $top + 78, 'UTF-8');
		$pageNewL->drawText('Nazwisko:', $leftNow, $top + 54, 'UTF-8');
		$pageNewL->drawText('Adres:', $leftNow, $top + 41, 'UTF-8');
		$pageNewL->drawText('Kod:', $leftNow, $top + 29, 'UTF-8');
		$leftNow = $pageWidth / 2 + 39;
		$pageNewL->setFont($tahoma, 11);
		//$pageNewL->drawText($user['NazwaFirmyO'], $leftNow, $top + 78, 'UTF-8');
		$NazwaFirmyO = $this->common->getPdfLines($user['NazwaFirmyO'], 30);
		$l = 0;
		if(count($NazwaFirmyO) > 0)
		foreach($NazwaFirmyO as $NFO)
		{
			if($l > 2) break;
			$pageNewL->drawText($NFO, $leftNow, $top + 78 - $l++ * 12, 'UTF-8');
		}
		$pageNewL->drawText($user['NazwiskoO'], $leftNow, $top + 54, 'UTF-8');
		$pageNewL->drawText($user['UlicaO'], $leftNow, $top + 41, 'UTF-8');
		$postcode = substr(str_replace('-', '', $user['KodPocztowyO']), 0, 5);
		$pageNewL->drawText($postcode, $leftNow, $top + 29, 'UTF-8');
		$pageNewL->drawText('', $leftNow, $top + 13, 'UTF-8');
		$pageNewL->setFont($tahoma, 8);
		$pageNewL->drawText('Miasto:', $leftNow + 37, $top + 29, 'UTF-8');
		$pageNewL->drawText('Telefon:', $leftNow + 82, $top + 17, 'UTF-8');
		$pageNewL->setFont($tahoma, 11);
		$pageNewL->drawText($user['MiastoO'], $leftNow + 64, $top + 29, 'UTF-8');
		$pageNewL->drawText($user['TelefonO'], $leftNow + 95, $top + 5, 'UTF-8');
		
		$pageNewL->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $topAll + $polaTop[$poleNr];
		$height = $polaTops[$poleNr] - $polaTops[$poleNr - 1];
		
		$leftNow = 0;
		$topNow = $top + 108;
		$pageNewL->setFont($tahoma, 8);
		$pageNewL->drawText('Cena netto:', $leftNow + 1, $topNow + 5, 'UTF-8');
		$pageNewL->drawText('0,00 zł', $leftNow + 47, $topNow + 5, 'UTF-8');
		$pageNewL->drawText('Cena brutto:', $leftNow + 89, $topNow + 5, 'UTF-8');
		$pageNewL->drawText('0,00 zł', $leftNow + 145, $topNow + 5, 'UTF-8');
		$pageNewL->setLineWidth(0.5)->drawLine($leftNow, $topNow, $leftNow + 181, $topNow);
		$topNow = $top + 68;
		$pageNewL->setFont($tahoma, 8);
		$pageNewL->drawText('Pobranie:', $leftNow + 1, $topNow + 30, 'UTF-8');
		$pageNewL->drawText('Ubezp.:', $leftNow + 1, $topNow + 21, 'UTF-8');
		$pageNewL->drawText('Bank:', $leftNow + 1, $topNow + 12, 'UTF-8');
		$pageNewL->drawText('Konto:', $leftNow + 1, $topNow + 3, 'UTF-8');
		$pageNewL->drawText($config['kwotaPobrania'].' zł', $leftNow + 38, $topNow + 30, 'UTF-8');
		$pageNewL->drawText($config['kwotaUbezpieczenia'].' zł', $leftNow + 38, $topNow + 21, 'UTF-8');
		$pageNewL->drawText($config['Konto'], $leftNow + 38, $topNow + 12, 'UTF-8');
		$pageNewL->drawText($config['NrKonta'], $leftNow + 38, $topNow + 3, 'UTF-8');
		$pageNewL->setLineWidth(0.5)->drawLine($leftNow, $topNow, $leftNow + 181, $topNow);
		$topNow = $top + 51;
		$pageNewL->setFont($tahoma, 8);
		$pageNewL->drawText('Ilość:', $leftNow + 1, $topNow + 5, 'UTF-8');
		$pageNewL->drawText('1', $leftNow + 27, $topNow + 5, 'UTF-8');
		$pageNewL->drawText('Waga:', $leftNow + 66, $topNow + 5, 'UTF-8');
		$pageNewL->drawText($config['Waga'].' kg', $leftNow + 95, $topNow + 5, 'UTF-8');
		$pageNewL->setLineWidth(0.5)->drawLine($leftNow, $topNow, $leftNow + 181, $topNow);
		$topNow = $top + 16;
		$pageNewL->setFont($tahoma, 6);
		$pageNewL->drawText('Opis:', $leftNow + 1, $topNow + 29, 'UTF-8');
		$pageNewL->setFont($tahoma, 8);
		$opisLines = $this->common->getPdfLines($config['opisZawartosci'], 40);
		$l = 0;
		if(count($opisLines) > 0)
		foreach($opisLines as $line)
		{
			if($l > 2) break;
			$pageNewL->drawText($line, $leftNow + 1, $topNow + 20 - $l * 9, 'UTF-8');
			$l++;
		}
		$pageNewL->setLineWidth(0.5)->drawLine($leftNow, $topNow, $leftNow + 181, $topNow);
		$topNow = $top + 0;
		$pageNewL->setFont($tahoma, 6);
		$pageNewL->drawText('Usługi specjalne:', $leftNow + 1, $topNow + 9, 'UTF-8');
		$UslugiSpecjalne = '';
		if((intval($config['UslugiSpecjalne']) & 1) == 1) $UslugiSpecjalne .= 'ZPD  ';
		if((intval($config['UslugiSpecjalne']) & 2) == 2) $UslugiSpecjalne .= 'ZPL  ';
		if((intval($config['UslugiSpecjalne']) & 4) == 4) $UslugiSpecjalne .= 'zwrot palet';
		$pageNewL->setFont($tahoma, 8);
		$pageNewL->drawText($UslugiSpecjalne, $leftNow + 50, $topNow + 4, 'UTF-8');
		
		$leftNow = 181;
		$pageNewL->setLineWidth(0.5)->drawLine($leftNow, $top + $height, $leftNow, $top);
		$pageNewL->setLineWidth(0.5)->drawLine($leftNow+134, $top+$height, $leftNow+134, $top);
		
		$topNow = $top + 51;
		if($nrListu > 0)
		{
			$barcodeOptions = array('text' => $nrListu, 'drawText' => true, 
				'font' => 'fonts/times.ttf', 'fontSize' => 13, 'factor' => 2);
			$rendererOptions  = array('imageType' => 'png');
			$renderer = Zend_Barcode::factory('Code39','image',$barcodeOptions,$rendererOptions);
			$barcodeFile = 'admin/barcodes/'.$nrListu.'.png';
			imagepng($renderer->draw(), $barcodeFile);
			//$renderer->render();
			
			$barcodeLeft = $leftNow + 12;
			$barcodeTop = $topNow + 18;
			$barcodeWidth = 94;
			$barcodeHeight = 43;
			//var_dump($barcodeFile);
			$barcodeImage = Zend_Pdf_Image::imageWithPath($barcodeFile);
			$pageNewL->drawImage($barcodeImage, $barcodeLeft, $barcodeTop, 
				$barcodeLeft + $barcodeWidth, $barcodeTop + $barcodeHeight);
		}
		$pageNewL->setLineWidth(0.5)->drawLine($leftNow, $topNow, $leftNow + 134, $topNow);
		$topNow = $top + 0;
		$pageNewL->setFont($tahoma, 6);
		$pageNewL->drawText('Uwagi:', $leftNow + 1, $topNow + 44, 'UTF-8');
		$pageNewL->setFont($tahoma, 8);
		$Uwagi = $config['Uwagi'];
		if(!empty($config['NumerFaktury'])) $Uwagi .= ', '.$config['NumerFaktury'];
		$uwagiLines = $this->common->getPdfLines($Uwagi, 30);
		$l = 0;
		if(count($uwagiLines) > 0)
		foreach($uwagiLines as $line)
		{
			if($l > 3) break;
			$pageNewL->drawText($line, $leftNow + 3, $topNow + 35 - $l * 9, 'UTF-8');
			$l++;
		}
		
		$leftNow = $pageWidth - 105;
		
		$topNow = $top + 105;
		$pageNewL->setFont($tahoma, 6);
		$pageNewL->drawText('Nr i podpis kuriera nadającego:', $leftNow + 1, $topNow + 14, 'UTF-8');
		$pageNewL->setFont($tahoma, 8);
		$pageNewL->drawText(substr($config['NrKurieraN'], 0, 6), $leftNow + 2, $topNow + 4, 'UTF-8');
		$pageNewL->drawText($config['PodpisKurieraN'], $leftNow + 40, $topNow + 4, 'UTF-8');
		$pageNewL->setLineWidth(0.5)->drawLine($leftNow, $topNow, $pageWidth, $topNow);
		$topNow = $top + 75;
		$pageNewL->setFont($tahoma, 6);
		$pageNewL->drawText('Data nadania i podpis nadawcy:', $leftNow + 1, $topNow + 24, 'UTF-8');
		$pageNewL->setFont($tahoma, 8);
		$pageNewL->drawText($config['DataN'], $leftNow + 2, $topNow + 14, 'UTF-8');
		$pageNewL->setLineWidth(0.5)->drawLine($leftNow, $topNow, $pageWidth, $topNow);
		$topNow = $top + 57;
		$pageNewL->setFont($tahoma, 6);
		$pageNewL->drawText('Deklarowana data doręczenia:', $leftNow + 1, $topNow + 11, 'UTF-8');
		$pageNewL->setFont($tahoma, 8);
		$dataO = date('Y-m-d', strtotime($config['DataN']) + 1 * 24 * 3600);
		$pageNewL->drawText($dataO, $leftNow + 2, $topNow + 2, 'UTF-8');
		$pageNewL->setLineWidth(0.5)->drawLine($leftNow, $topNow, $pageWidth, $topNow);
		$topNow = $top + 31;
		$pageNewL->setFont($tahoma, 6);
		$pageNewL->drawText('Nr i podpis kuriera doręczającego:', $leftNow + 1, $topNow + 19, 'UTF-8');
		$pageNewL->setLineWidth(0.5)->drawLine($leftNow, $topNow, $pageWidth, $topNow);
		$topNow = $top + 0;
		$pageNewL->setFont($tahoma, 6);
		$pageNewL->drawText('Data doręczenia i podpis odbiorcy:', $leftNow + 1, $topNow + 23, 'UTF-8');
		
		$pageNewL->setLineWidth(0.5)->drawLine(0, $top, $pageWidth, $top);
		
		$poleNr++;
		$top = $topAll + $polaTop[$poleNr];
		$height = $polaTops[$poleNr] - $polaTops[$poleNr - 1];
		$topNow = $top;
		$leftNow = 0;
		$pageNewL->setFont($tahoma, 8);
		$stopka = 'Producent oprogramowania: ';
		$stopka.= 'BigCom, ul. Boya - Żeleńskiego 12, 35-105 Rzeszów, www.bigcom.pl';
		$stopka.= '     Wersja: 130606';
		$pageNewL->drawText($stopka, $leftNow + 1, $topNow + 3, 'UTF-8');

		if(($listKrok + 1) % $listIleNaStrone != 0)
		$pageNewL->setLineWidth(0.5)->drawLine(0, $topAll, $pageWidth, $topAll);

		return $pageNewL;
	}
	
	function raportopek($buyers, $pomin, $data, $dataDo)
	{
		require_once 'tcpdf/tcpdf.php';
		$zendPDF = new Zend_Pdf();
		$zendPDFlist = new Zend_Pdf();
		$path = 'admin/kurierzy/opek';
		$file = $path.'/raport_dzienny_opek.pdf';
		@unlink($file);

		$iloscListow = 0;
		$iloscPaczek = 0;
		$lacznaWaga = 0;
		$sumaPobran = 0;
		$sumaUbezp = 0;
		foreach($buyers as $dealID => $deal)
		{
			if(isset($pomin[$deal['buyer_id']])) continue;
			$iloscListow++;
			$iloscPaczek++;
			$config = $deal['config'];
			$lacznaWaga += floatval($config['Waga']);
			$sumaPobran += floatval($config['kwotaPobrania']);
			$sumaUbezp += floatval($config['kwotaUbezpieczenia']);
			//$b[] = $dealID;
		}
		//var_dump($b);die();
		
		$strona = 0;
		$listKrok = 0;
		$listIle = $iloscListow;
		$listIleNaStrone = 5;
		$ileStron = ceil($listIle / $listIleNaStrone);
		//$listHeight = floor($listHeight / $listIleNaStrone);
		
		$dataWydruku = date('Y-m-d H:i:s');
		
		//var_dump($buyers);
		foreach($buyers as $nr => $deal)
		{
			if(isset($pomin[$deal['buyer_id']])) continue;
			try
			{
				$user = $deal['user'];
				$config = $deal['config'];
				$nrListu = $deal['list'];
				$nrPaczki = $deal['paczka'];
				//var_dump($deal);
				//continue;
				
				if(isset($addedToPDF[$nrListu])) continue;
				$addedToPDF[$nrListu] = true;

				if(true)
				{
					$listWidth = 840;
					$listHeight = 600;//448							
					
					$width = $listWidth;
					$height = $listHeight;
					$top = floor($listHeight * 0.95);
					$left = floor($listWidth * 0.03);
					$bottom = floor($listHeight * 0.06);
					$right = floor($listWidth * 0.94);
					
					$pageWidth = $width - ($width - $right) + $left;
					$pageHeight = floor($listHeight * 0.13);
					
					if($listKrok % $listIleNaStrone == 0) $strona++;
					
					if($listKrok % $listIleNaStrone == 0)
					$pageNew = $zendPDF->newPage($listWidth.':'.$listHeight.':');
					
					//$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD);
					$font = Zend_Pdf_Font::fontWithPath('fonts/times.ttf');
					$fontBold = Zend_Pdf_Font::fontWithPath('fonts/timesbd.ttf');
					$tahoma = Zend_Pdf_Font::fontWithPath('fonts/tahoma.ttf');
					$tahomaBold = Zend_Pdf_Font::fontWithPath('fonts/tahomabd.ttf');
					$arial = Zend_Pdf_Font::fontWithPath('fonts/arial.ttf');
					$arialBold = Zend_Pdf_Font::fontWithPath('fonts/arialbd.ttf');
					$pageNew->setFont($font, 12);
					
					if($listKrok % $listIleNaStrone == 0)
					{								
						//$pageNew->setLineWidth(0.5)->drawLine(0, $top, $listWidth, $top);
						
						$pageNew->setFont($fontBold, 18);
						$pageNew->drawText('Deliwer wysyłkowy', $left, $top - 15, 'UTF-8');
						$pageNew->drawText('OPEK Sp. z o.o.', $left, $top - 35, 'UTF-8');
						
						$line = 1;
						$size = 12;
						$leftNow = floor($listWidth * 0.30);
						$pageNew->setFont($fontBold, $size);
						$nadawca = 'Nadawca: '.$config['NrKlientaN'];
						$pageNew->drawText($nadawca, $leftNow, $top - $size * $line++, 'UTF-8');
						$pageNew->setFont($font, $size);
						$pageNew->drawText($config['NazwaFirmyN'], $leftNow, $top - $size * $line++, 'UTF-8');
						$pageNew->drawText($config['NazwiskoN'], $leftNow, $top - $size * $line++, 'UTF-8');
						$pageNew->drawText($config['UlicaN'], $leftNow, $top - $size * $line++, 'UTF-8');
						$kod = str_replace('-', '', $config['KodPocztowyN']);
						$miasto = mb_strtoupper($config['MiastoN'], 'UTF-8');
						$pageNew->drawText($kod.', '.$miasto, $leftNow, $top - $size * $line++, 'UTF-8');
						
						$line = 1;
						$size = 12;
						$leftNow = floor($listWidth * (1 - 0.5));
						$pageNew->setFont($fontBold, $size);
						$pageNew->drawText('Ilość listów:', $leftNow, $top - $size * $line++, 'UTF-8');
						$pageNew->drawText('Ilość paczek:', $leftNow, $top - $size * $line++, 'UTF-8');
						$pageNew->drawText('Łączna waga:', $leftNow, $top - $size * $line++, 'UTF-8');
						$pageNew->drawText('Suma pobrań:', $leftNow, $top - $size * $line++, 'UTF-8');
						$pageNew->drawText('Suma ubezp.:', $leftNow, $top - $size * $line++, 'UTF-8');
						
						$line = 1;
						$size = 12;
						$leftNow = floor($listWidth * (1 - 0.4));
						$pageNew->setFont($font, $size);
						$pageNew->drawText($iloscListow, $leftNow, $top - $size * $line++, 'UTF-8');
						$pageNew->drawText($iloscPaczek, $leftNow, $top - $size * $line++, 'UTF-8');
						$lacznaWagaNF = number_format($lacznaWaga, 2, ',', ' ');
						$pageNew->drawText($lacznaWagaNF.' kg', $leftNow, $top - $size * $line++, 'UTF-8');
						$sumaPobranNF = number_format($sumaPobran, 2, ',', ' ');
						$pageNew->drawText($sumaPobranNF.' zł', $leftNow, $top - $size * $line++, 'UTF-8');
						$sumaUbezpNF = number_format($sumaUbezp, 2, ',', ' ');
						$pageNew->drawText($sumaUbezpNF.' zł', $leftNow, $top - $size * $line++, 'UTF-8');
						
						$line = 1;
						$size = 12;
						$leftNow = floor($listWidth * (1 - 0.31));
						$pageNew->setFont($fontBold, $size);
						$pageNew->drawText('Data zestawienia:', $leftNow, $top - $size * $line++, 'UTF-8');
						$pageNew->drawText('Data druku:', $leftNow, $top - $size * $line++, 'UTF-8');
						$line += 2;
						$pageNew->drawText('Kurier nad.:', $leftNow, $top - $size * $line++, 'UTF-8');
						
						$line = 1;
						$size = 12;
						$leftNow = floor($listWidth * (1 - 0.2));
						$pageNew->setFont($font, $size);
						$pageNew->drawText($data.' - '.$dataDo, $leftNow, $top - $size * $line++, 'UTF-8');
						$pageNew->drawText($dataWydruku, $leftNow, $top - $size * $line++, 'UTF-8');
						$line += 2;
						$pageNew->drawText($config['PodpisKurieraN'], $leftNow, $top - $size * $line++, 'UTF-8');
						
						$topNow = floor($listHeight * (1 - 0.17));
						$pageNew->setLineWidth(1)->drawLine($left, $topNow, $right, $topNow);
						
						$topNow = floor($listHeight * 0.10);
						$pageNew->setLineWidth(1)->drawLine($left, $topNow, $right, $topNow);
						
						$size = 12;
						$line1 = 14;
						$line2 = 28;
						
						$leftNow = $left;
						$pageNew->setFont($font, $size);
						$pageNew->drawText('Producent oprogramowania:', $leftNow, $topNow - $line1, 'UTF-8');
						$pageNew->drawText('BigCom', $leftNow, $topNow - $line2, 'UTF-8');
						$pageNew->drawText('www.bigcom.pl', $leftNow + 100, $topNow - $line2, 'UTF-8');
						
						$leftNow = floor($listWidth * 0.27);
						$pageNew->setFont($fontBold, $size);
						$pageNew->drawText('Deliwer wysyłkowy', $leftNow, $topNow - $line1, 'UTF-8');
						$pageNew->drawText('OPEK Sp. z o.o.', $leftNow, $topNow - $line2, 'UTF-8');
						
						$leftNow = floor($listWidth * 0.43);
						$pageNew->setFont($font, $size);
						$pageNew->drawText('Wersja programu: 1306', $leftNow, $topNow - $line2, 'UTF-8');
						
						$leftNow = floor($listWidth * 0.60);
						$pageNew->setFont($font, $size);
						$pageNew->drawText('Data wydruku: '.$dataWydruku, $leftNow, $topNow - $line2, 'UTF-8');
						
						$leftNow = floor($listWidth * (1 - 0.14));
						$pageNew->setFont($fontBold, $size);
						$pageNew->drawText('Strona:', $leftNow, $topNow - $line1, 'UTF-8');
						
						$leftNow = floor($listWidth * (1 - 0.09));
						$pageNew->setFont($fontBold, $size);
						$pageNew->drawText($strona.' z '.$ileStron, $leftNow, $topNow - $line1, 'UTF-8');
					}
					
					$width = $pageWidth;
					$height = $pageHeight;
					
					$topAll = floor($listHeight * 0.17) + 2;
					$bottom = $listHeight - ($listKrok % $listIleNaStrone + 1) * $pageHeight - $topAll;
					$bottom -= ($listKrok % $listIleNaStrone) * 10;
					$top = $bottom + $pageHeight;							
					
					$pageNew->setLineWidth(0.5)->drawLine($left, $top, $right, $top);
					$pageNew->setLineWidth(0.5)->drawLine($left, $bottom, $right, $bottom);
					$pageNew->setLineWidth(0.5)->drawLine($left, $top, $left, $bottom);
					$pageNew->setLineWidth(0.5)->drawLine($right, $top, $right, $bottom);
					
					$bottomNow = $bottom + floor($height * 0.15);
					$pageNew->setLineWidth(0.5)->drawLine($left, $bottomNow, $right, $bottomNow);
					
					$topNow = $top - 5;
					$leftNow = $left + 2;
					$pageNew->setFont($font, 5);
					$pageNew->drawText('LP:', $leftNow, $topNow, 'UTF-8');
					$pageNew->setFont($fontBold, 8);
					$pageNew->drawText(($listKrok + 1), $leftNow + 1, $topNow - 8, 'UTF-8');
					$leftNow = $left + floor($width * 0.03);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Stacja N:', $leftNow, $topNow, 'UTF-8');
					$pageNew->setFont($fontBold, 8);
					$pageNew->drawText($config['StacjaN'], $leftNow, $topNow - 8, 'UTF-8');
					$leftNow = $left + floor($width * 0.08);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Kurier nadający:', $leftNow, $topNow, 'UTF-8');
					$pageNew->setFont($fontBold, 8);
					$pageNew->drawText($config['NrKurieraN'], $leftNow, $topNow - 8, 'UTF-8');
					
					$leftNow = $left;
					$rightNow = $leftNow + floor($width * 0.17);
					
					$topNow = $top - floor($height * 0.20);
					$pageNew->setLineWidth(0.5)->drawLine($leftNow, $topNow, $rightNow, $topNow);
					
					if($nrListu > 0)
					{
						$barcodeOptions = array('text' => $nrListu, 'drawText' => false, 'fontSize' => 18, 'stretchText' => false, 'factor' => 1, 'font' => 'fonts/times.ttf');
						$rendererOptions  = array('imageType' => 'png');
						$renderer = Zend_Barcode::factory('Code39', 'image', $barcodeOptions, $rendererOptions);
						$barcodeFile = 'admin/barcodes/'.$nrListu.'.png';
						imagepng($renderer->draw(), $barcodeFile);
						//$renderer->render();
						
						$barcodeTop = $topNow - 3;
						$barcodeWidth = 125;//org 115 x 62 skala 0.54
						$barcodeLeft = $leftNow + 5;
						$barcodeHeight = 15;
						//var_dump($barcodeFile);
						$barcodeImage = Zend_Pdf_Image::imageWithPath($barcodeFile);
						$pageNew->drawImage($barcodeImage, $barcodeLeft, $barcodeTop - $barcodeHeight, 
															$barcodeLeft + $barcodeWidth, $barcodeTop);
						
						$pageNew->setFont($fontBold, 12);
						$pageNew->drawText($nrListu, $barcodeLeft+40, $barcodeTop-$barcodeHeight-10, 'UTF-8');
					}
					
					$topNow = $top - floor($height * 0.60);
					$pageNew->setLineWidth(0.5)->drawLine($leftNow, $topNow, $rightNow, $topNow);
					
					$leftNow = $left + 2;
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Data nadania:', $leftNow, $topNow - 5, 'UTF-8');
					$pageNew->setFont($font, 8);
					$dataN = date('Y-m-d', strtotime($config['DataN']));
					$pageNew->drawText($dataN, $leftNow, $topNow - 13, 'UTF-8');
					$leftNow = $left + floor($width * 0.10);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('ID nadawcy:', $leftNow, $topNow - 5, 'UTF-8');
					$pageNew->setFont($font, 8);
					$pageNew->drawText($config['NrKlientaN'], $leftNow, $topNow - 13, 'UTF-8');
					
					$pageNew->setLineWidth(0.5)->drawLine($rightNow, $top, $rightNow, $bottomNow);
					
					$topNow = $top;
					$leftNow = $rightNow + 1;
					$rightNow = $leftNow + floor($width * 0.11);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Firma nadająca:', $leftNow, $topNow - 5, 'UTF-8');
					$line = 1;
					$size = 8;
					$topNow = $top - 6;
					$pageNew->setFont($font, $size++);					
					$nadawca = $config['NazwaFirmyN'].'<br>';
					$nadawca.= $config['NazwiskoN'].'<br>';
					$nadawca.= $config['UlicaN'].'<br>';
					$nadawca.= $kod = str_replace('-', '', $config['KodPocztowyN']).'     ';
					$nadawca.= $miasto = mb_strtoupper($config['MiastoN'], 'UTF-8');
					$nadawca = $this->common->getPdfLines($nadawca, 20);
					$line = 1;
					if(count($nadawca) > 0)
					foreach($nadawca as $n)
					{
						if($line > 6) break;
						$pageNew->drawText($n, $leftNow, $topNow - $size * $line++, 'UTF-8');
					}
					//$pageNew->drawText($config['NazwaFirmyN'], $leftNow, $topNow - $size * $line++, 'UTF-8');
					//$pageNew->drawText($config['NazwiskoN'], $leftNow, $topNow - $size * $line++, 'UTF-8');
					//$pageNew->drawText($config['UlicaN'], $leftNow, $topNow - $size * $line++, 'UTF-8');
					//$pageNew->drawText($kod, $leftNow, $topNow - $size * $line, 'UTF-8');
					//$pageNew->drawText($miasto, $leftNow + 30, $topNow - $size * $line, 'UTF-8');
					
					$pageNew->setLineWidth(0.5)->drawLine($rightNow, $top, $rightNow, $bottomNow);
					
					$topNow = $top;
					$leftNow = $rightNow;
					$rightNow = $leftNow + floor($width * 0.12);
					
					$leftTen = $leftNow;
					$topTen = $topNow;
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Ilość:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 8);
					$pageNew->drawText('1', $leftTen + 15, $topTen - 16, 'UTF-8');
					$leftTen = $leftNow + floor($width * 0.05);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Pobranie:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->drawText('[zł]', $rightNow - 8, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 8);
					$pobranieNF = number_format($config['kwotaPobrania'], 2, ',', ' ');
					$pageNew->drawText($pobranieNF, $leftTen + 10, $topTen - 16, 'UTF-8');
					
					$topNow = $top - floor($height * 0.27);
					$pageNew->setLineWidth(0.5)->drawLine($leftNow, $topNow, $rightNow, $topNow);
					$bottomTen = $top - floor($height * 0.54);
					$pageNew->setLineWidth(0.5)->drawLine($leftTen, $top, $leftTen, $bottomTen);
					
					$rightTen = $leftTen;
					$leftTen = $leftNow;
					$topTen = $topNow;
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Waga:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->drawText('[kg]', $rightTen - 10, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 8);
					$wagaNF = number_format($config['Waga'], 1, ',', ' ');
					$pageNew->drawText($wagaNF, $leftTen + 15, $topTen - 16, 'UTF-8');
					$leftTen = $leftNow + floor($width * 0.05);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Ubezpieczenie:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->drawText('[zł]', $rightNow - 8, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 8);
					$ubezpieczenieNF = number_format($config['kwotaUbezpieczenia'], 2, ',', ' ');
					$pageNew->drawText($ubezpieczenieNF, $leftTen + 10, $topTen - 16, 'UTF-8');
					
					$topNow = $bottomTen;
					$pageNew->setLineWidth(0.5)->drawLine($leftNow, $topNow, $rightNow, $topNow);
					
					$leftTen = $leftNow;
					$topTen = $topNow;
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Kto:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 8);
					$pageNew->drawText($config['KtoPlaci'], $leftTen + 5, $topTen - 16, 'UTF-8');
					$leftTen = $leftNow + floor($width * 0.022);
					$pageNew->setLineWidth(0.5)->drawLine($leftTen, $topNow, $leftTen, $bottomNow);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Typ pł.:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 8);
					$pageNew->drawText($config['RodzajPlatnosci'], $leftTen + 5, $topTen - 16, 'UTF-8');
					$leftTen = $leftTen + floor($width * 0.022);
					$pageNew->setLineWidth(0.5)->drawLine($leftTen, $topNow, $leftTen, $bottomNow);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Płatnik:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 8);
					$pageNew->drawText($config['NrKlientaP'], $leftTen + 30, $topTen - 16, 'UTF-8');
					
					$pageNew->setLineWidth(0.5)->drawLine($rightNow, $top, $rightNow, $bottomNow);
					
					$topNow = $top;
					$leftNow = $rightNow;
					$rightNow = $leftNow + floor($width * 0.07);
					
					$leftTen = $leftNow;
					$topTen = $topNow;
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Przedziały wagowe paczek:', $leftTen + 1, $topTen - 5, 'UTF-8');
					
					$pageNew->setLineWidth(0.5)->drawLine($rightNow, $top, $rightNow, $bottomNow);
					
					$topNow = $top;
					$leftNow = $rightNow;
					$rightNow = $leftNow + floor($width * 0.35);
					
					$leftTen = $leftNow;
					$topTen = $topNow;
					$rightTen = $leftNow + floor($width * 0.28);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Firma odbioru:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 8);
					$company = mb_strtoupper($user['NazwaFirmyO'], 'UTF-8');
					$pageNew->drawText($company, $leftTen + 1, $topTen - 13, 'UTF-8');
					$topTen = $topNow - floor($height * 0.22);
					$pageNew->setLineWidth(0.5)->drawLine($leftNow, $topTen, $rightTen, $topTen);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Nazwisko odbiorcy:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 8);
					$name = mb_strtoupper($user['NazwiskoO'], 'UTF-8');
					$pageNew->drawText($name, $leftTen + 1, $topTen - 13, 'UTF-8');
					$topTen = $topTen - floor($height * 0.22);
					$pageNew->setLineWidth(0.5)->drawLine($leftNow, $topTen, $rightTen, $topTen);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Adres odbioru:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 8);
					$street = mb_strtoupper($user['UlicaO'], 'UTF-8');
					$pageNew->drawText($street, $leftTen + 1, $topTen - 13, 'UTF-8');
					
					$topTen = $topTen - floor($height * 0.22);
					$bottomTen = $topTen;
					$pageNew->setLineWidth(0.5)->drawLine($rightTen, $top, $rightTen, $bottomTen);
					
					$topNow = $top;
					$leftTen = $rightTen;
					$topTen = $topNow;
					$rightTen = $rightNow;
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Kod poczt.:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 12);
					$postcode = str_replace('-', '', $user['KodPocztowyO']);
					$pageNew->drawText($postcode, $leftTen + 1, $topTen - 20, 'UTF-8');
					$topTen = $topNow - floor($height * 0.33);
					$pageNew->setLineWidth(0.5)->drawLine($leftTen, $topTen, $rightTen, $topTen);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Stacja O:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 12);
					$pageNew->drawText($config['StacjaO'], $leftTen + 1, $topTen - 20, 'UTF-8');
					
					$pageNew->setLineWidth(0.5)->drawLine($rightNow, $top, $rightNow, $bottomNow);
					
					$topNow = $bottomTen;
					$leftTen = $leftNow;
					$topTen = $topNow;							
					$pageNew->setLineWidth(0.5)->drawLine($leftTen, $topNow, $rightNow, $topNow);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Numer konta bankowego:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 12);
					//$pageNew->drawText($config['NrKonta'], $leftTen + 1, $topTen - 13, 'UTF-8');
					$leftTen = $leftNow + floor($width * 0.21);
					$pageNew->setLineWidth(0.5)->drawLine($leftTen, $topTen, $leftTen, $bottomNow);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Usługi:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 18);
					$pageNew->drawText('□', $leftTen + 1, $topTen - 15, 'UTF-8');
					$pageNew->setFont($font, 7);
					if((intval($config['UslugiSpecjalne']) & 1) == 1)
					$pageNew->drawText('X', $leftTen + 4, $topTen - 13, 'UTF-8');
					$pageNew->setFont($font, 8);
					$pageNew->drawText('ZPD', $leftTen + 11, $topTen - 13, 'UTF-8');
					$leftTen += 30;
					$pageNew->setFont($fontBold, 18);
					$pageNew->drawText('□', $leftTen + 1, $topTen - 15, 'UTF-8');
					$pageNew->setFont($font, 7);
					if((intval($config['UslugiSpecjalne']) & 2) == 2)
					$pageNew->drawText('X', $leftTen + 4, $topTen - 13, 'UTF-8');
					$pageNew->setFont($font, 8);
					$pageNew->drawText('ZPL', $leftTen + 11, $topTen - 13, 'UTF-8');
					$leftTen += 30;
					$pageNew->setFont($fontBold, 18);
					$pageNew->drawText('□', $leftTen + 1, $topTen - 15, 'UTF-8');
					$pageNew->setFont($font, 7);
					if((intval($config['UslugiSpecjalne']) & 4) == 4)
					$pageNew->drawText('X', $leftTen + 4, $topTen - 13, 'UTF-8');
					$pageNew->setFont($font, 8);
					$pageNew->drawText('Zwrot palet', $leftTen + 11, $topTen - 13, 'UTF-8');
					
					$topNow = $top;
					$leftTen = $rightTen;
					$topTen = $topNow;
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Miasto:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->setFont($fontBold, 7);
					$city = mb_strtoupper($user['MiastoO'], 'UTF-8');
					$pageNew->drawText($city, $leftTen + 1, $topTen - 12, 'UTF-8');
					$topTen = $topNow - floor($height * 0.19);
					$pageNew->setLineWidth(0.5)->drawLine($leftTen, $topTen, $right, $topTen);
					$pageNew->setFont($font, 5);
					$pageNew->drawText('Uwagi:', $leftTen + 1, $topTen - 5, 'UTF-8');
					$pageNew->setFont($font, 8);
					//$config['Uwagi'] = 'uwagi Numer kuriera nadającego przesyłkę (odbierającego od nadawcy) Numer konta w formacie IBAN (max 40 znaków) Nazwa banku na którego konto ma być przelane pobranie';
					$uwagi = $this->common->getPdfLines($config['opisZawartosci'], 20);
					$line = 1;
					if(count($uwagi) > 0)
					foreach($uwagi as $uwaga)
					{
						if($line > 5) break;
						$pageNew->drawText($uwaga, $leftTen + 1, $topTen - 5 - $line * 9, 'UTF-8');
						$line++;
					}
					
					if($listKrok % $listIleNaStrone == 0)
					$zendPDF->pages[] = $pageNew;
					$listKrok++;
					//continue;
				}						
			}
			catch(Zend_Pdf_Exception $error)
			{
				return 'Błąd: '.$error->getMessage();
				//echo $this->view->error;
			}
			catch(Exception $error)
			{
				return 'Błąd: '.$error;
				//echo $this->view->error;
			}
		}

		if(@count($zendPDF->pages) > 0)
		try
		{
			$zendPDF->save($file);
			return '<div class="k_ok"><a href="'.$this->obConfig->www.'/public/'.$file.'" target="_blank">Zobacz raport dzienny w PDF</a></div>';
		}
		catch(Zend_Pdf_Exception $error)
		{
			return '<div class="k_blad">Błąd: '.$error->getMessage().'</div>';
			//echo $this->view->error;
		}
	}
	
	function naklejka($pageNew, $produkt, $details, $prodKrok, $topNow)
	{
		//$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD);
		$fontBold = Zend_Pdf_Font::fontWithPath('fonts/timesbd.ttf');
		$font = Zend_Pdf_Font::fontWithPath('fonts/times.ttf');
		$pageNew->setFont($font, 10);
		
		$width = 280;
		$top = 420;
		$left = 5;
		$bottom = 5;
		$right = 5;
		
		if(true)
		{
			$pageNew->setFont($fontBold, 10);
			$LP = ($prodKrok + 1).'.';
			$lpLeft = $left;
			if($LP >= 10) $lpLeft = 0;
			$pageNew->drawText($LP, $lpLeft, $topNow - 10, 'UTF-8');							
			
			$pageNew->setFont($font, 9);
			$nazwa = $produkt['nazwa'];
			if(!empty($produkt['rozm'])) $nazwa .= ' / '.$produkt['rozm'];
			if(!empty($produkt['kolor'])) $nazwa .= ' / '.$produkt['kolor'];
			$opisLines = $this->common->getPdfLines($nazwa, 40);
			$l = 1;
			//if(false)
			if(count($opisLines) > 0)
			foreach($opisLines as $line)
			if($l <= 2)
			$pageNew->drawText($line, $left + 10, $topNow - $l++ * 10, 'UTF-8');
			$pageNew->setFont($font, 10);
		}
		
		if(isset($details['symbol']) && $details['symbol'] == 'on')
		if(!empty($produkt['oznaczenie']))
		{
			$symbol = $produkt['oznaczenie'];
			$pageNew->drawText($symbol, $left + 20, $topNow - 35 + 2, 'UTF-8');
		}
		
		if(isset($details['sztuk']) && $details['sztuk'] == 'on')
		{
			$pageNew->setFont($fontBold, 11);
			$sztuk = 'sztuk: '.intval($produkt['ilosc']);
			$pageNew->drawText($sztuk, $left + 20, $topNow - 50 + 2, 'UTF-8');
			$pageNew->setFont($font, 10);
		}
		
		if(isset($details['zdjecie']) && $details['zdjecie'] == 'on')
		{
			$zdjecieGlowne = !empty($produkt['img']) ? $produkt['img'] : 'brak.jpg';
			$pathImg = $this->obConfig->baseUrl.'/public/admin/zdjecia';
			$pathImgShort = str_replace($this->obConfig->baseUrl.'/public/', '', $pathImg);
			$pathImgShort2=str_replace($this->obConfig->baseUrl.'/public/','../public/',$pathImg);
			//var_dump($pathImgShort.'/'.$zdjecieGlowne);die();
			if(@file_exists($pathImgShort2.'/'.$zdjecieGlowne))
			{
				$image = Zend_Pdf_Image::imageWithPath($pathImgShort.'/'.$zdjecieGlowne);
				$imageSize = getimagesize($pathImgShort.'/'.$zdjecieGlowne);
				//var_dump($imagesize);
				$imageWidth = $imageSize[0];
				$imageHeight = $imageSize[1];
				$maxwidth = $width / 3;
				$maxheight = 45;
				$drawWidth = $maxwidth;
				$skalaWidth = $drawWidth / $imageWidth;
				$drawHeight = $imageHeight * $skalaWidth;
				if($drawHeight > $maxheight)
				{
					$drawHeight = $maxheight;
					$skalaHeight = $drawHeight / $imageHeight;
					$drawWidth = $imageWidth * $skalaHeight;
				}
				$drawLeft = $width - $maxwidth + ceil(($maxwidth - $drawWidth) / 2) - 2;
				$drawTop = 2 + ceil(($maxheight - $drawHeight) / 2);
				//$pageNew->drawImage($image, 200, 100, 400, 200);
				$pageNew->drawImage($image,$drawLeft,$topNow-$drawTop-$drawHeight,$drawLeft+$drawWidth,$topNow-$drawTop);
			}
		}
		
		$ean = @$produkt['ean'];
		if(isset($details['kod_ean']) && $details['kod_ean'] == 'on' && $ean > 0)
		{
			$barcodeOptions = array('text' => substr($ean, 0, 12), 'barHeight' => 200, 'barThickWidth' => 12, 'barThinWidth' => 4, 'font' => 'fonts/times.ttf', 'fontSize' => 40, 'factor' => 1, 'stretchText' => true, 'withChecksum' => true);
			$rendererOptions  = array('imageType' => 'png');
			$renderer = Zend_Barcode::factory('EAN13','image',$barcodeOptions,$rendererOptions);
			$barcodeFile = 'admin/barcodes/'.$ean.'.png';
			imagepng($renderer->draw(), $barcodeFile);
			//$renderer->render();								
			
			$barcodeWidth = 90;//org 115 x 62 skala 0.54
			$barcodeHeight = 47;
			$barcodeLeft = $width - $barcodeWidth;
			$barcodeTop = $topNow - 2;
			//var_dump($barcodeFile);
			$barcodeImage = Zend_Pdf_Image::imageWithPath($barcodeFile);
			$pageNew->drawImage($barcodeImage, $barcodeLeft, $barcodeTop - $barcodeHeight, $barcodeLeft + $barcodeWidth, $barcodeTop);
		}
		
		return $pageNew;
	}
}
?>