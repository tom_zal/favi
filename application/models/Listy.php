<?php
class Listy extends Zend_Db_Table
{
	public $id;
	function dodaj($dane, $id = null)
	{
		if(empty($id))
		{
			$where = 'deal_id ='. $dane['deal_id'];
		} else {
			$where = 'id ='. $id;
		}
        $result = $this->fetchRow($where);

        if(!empty($result))
		{
			$where = 'id = '.$result['id'];
			$r = $this->update($dane, $where);
			return $result['id'];
			
		} else {
			$this->insert($dane);
			$id = $this->getAdapter()->lastInsertId();
			return $id;
		}
           
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujZam($dane, $zam)
	{
		$where = 'deal_id = '.$zam;
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$result = $this->fetchAll();
		return $result;
	}
	function wypiszZam($id)
	{
		$result = $this->fetchRow('deal_id = '.$id);
		return $result;
	}
	function wypiszRaport($adminID, $data, $dataDo, $typprzesylki)
	{
		
		$where = 'data >= "'.$data.' 00:00:00" and data <= "'.$dataDo.' 23:59:59"';
		$where.= ' and typ = "'.$typprzesylki.'"';
		//echo $where;return null;
		$result = $this->fetchAll($where);
		
                return $result;
	}
	function edytujData($dane, $data, $adminID)
	{
		$where = 'admin_id = '.$adminID;
		$where.= ' and data >= "'.$data.' 00:00:00" and data <= "'.$data.' 23:59:59"';
		//$where.= ' and wyslano = 1';
		$this->update($dane, $where);
	}

	function wypiszJeden()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
        function usun($id){
            $result = $this->delete('id = "'.$id.'"');
            return true;
        }
		
	public function findByDealId($id)
	{
		$result = $this->fetchRow('deal_id = '.$id);
		if(!empty($result))
		{
			return $result->toArray();
		} else {
			return null;
		}
	}
}
?>