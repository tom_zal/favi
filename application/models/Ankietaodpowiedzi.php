<?php
class Ankietaodpowiedzi extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujGlosy()
	{
		$result = $this->fetchRow('id = '.$this->id);
		//$sql = 'UPDATE `Ankietaodpowiedzi` SET `glosy` = `glosy`+1 WHERE id = "'.$this->id.'"';
		$result = $this->update(array('glosy' => @intval($result['glosy']) + 1), 'id = '.$this->id);
	}
	function usunGlosy($dane)
	{
		$where = 'idan = '.$this->id;
		$this->update($dane, $where);
	}
	function usun()
	{
		$this->delete('id = "'.$this->id.'"');
	}
	function usunPrzypisane()
	{
		$this->delete('idan = "'.$this->id.'"');
	}
	function wypiszWszystko()
	{
		$where = 'idan="'.$this->id.'"';
		$order = 'pozycja ASC';
		$result = $this->fetchAll($where, $order);
		return $result;
	}
	function wypiszPojedynczy()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}

	public function changePosition($position, $id_cat)
	{
		$dane['pozycja'] = $position;
		$where = 'id= "'.$id_cat.'"';
		$this->update($dane, $where);
	}        
}
?>