<?php
class Online
{
    public $tabelaKontrahenci, $tabelaProdukty;
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);

        $this->platnosci = 'Onlineplatnosci';
        $this->zamowienia = 'Onlinezamowienia';
        $this->zamowieniaKontahent = 'Zamowieniakontrahenci';
        $this->online = 'Online';
        $this->dostawa = 'Sposobdostawy';
    }
	
    public function wypiszDanePos()
    {
        $where = 'id = 1';
        $select = $this->db->select()->from($this->online)->where($where);
        $result = $this->db->fetchAll($select);
        return $result[0];
    }
    public function edytujDanePos($dane)
    {
        $where = 'id = 1';
        $this->db->update($this->online, $dane, $where);
    }
    public function wypiszSposobyPlatnosci()
    {
        $select = $this->db->select()->from($this->platnosci);
        $result = $this->db->fetchAll($select);
        for($i=0;$i<count($result);$i++)
        {
            $nazwa = $this->selectdostawy($result[$i]['dostawa']);
            $result[$i]['dostawa'] = $nazwa['nazwa'];
        }
        return $result;
    }
    public function platnosc($id)
    {
        $where = 'id = '.$id;
        $select = $this->db->select()->from($this->platnosci)->where($where);
        $result = $this->db->fetchAll($select);
        $nazwadostawy = $this->selectdostawy($result[0]['dostawa']);
        $result[0]['dostawa']=$nazwadostawy['nazwa'];
        return $result[0];
    }
    public function wypiszZamowienia()
    {
        $select = $this->db->select()->from($this->zamowienia);
        $result = $this->db->fetchAll($select);
        return $result;
    }
    public function dodajZamowienie($dane)
    {
        $this->db->insert($this->zamowienia, $dane);
    }
    public function zmienStatusZamowienia($id,$dane,$typ)
    {
        $where = 'idzam = '.$id;
        $where2 = 'id = '.$id;
        $array = array('status'=> $dane['code']);
        $array2= array('online_typ' => $dane['message'], 'online' => $typ);
        $this->db->update($this->zamowienia, $array, $where);
        $this->db->update($this->zamowieniaKontahent, $array2, $where2);
		//wysylkastatusonlineuwagidatainfo
    }
    public function dodajPlatnosc($dane)
    {
        $this->db->insert($this->platnosci, $dane);
    }
    public function selectdostawy($id)
    {
        $where='id = '.$id;
        $select = $this->db->select()->from($this->dostawa)->where($where);
        $result = $this->db->fetchAll($select);
        return $result[0];
    }
    public function selectdostawyAll()
    {
        $select = $this->db->select()->from($this->dostawa);
        $result = $this->db->fetchAll($select);
        return $result;
    }
    public function deleteWybrany($id)
    {
        $where = 'id = '.$id;
        $this->db->delete($this->platnosci, $where);
    }
}
?>