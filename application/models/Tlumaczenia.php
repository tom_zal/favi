<?php
class Tlumaczenia extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;		
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	function dodaj($dane)
	{
        $this->insert($dane);
        return $this->getAdapter()->lastInsertId();
    }
	function updateData($array, $fraza = '')
	{
		$where = 'fraza = "'.$fraza.'"';
		$this->update($array, $where);
	}	
	function showData($sort = false)
	{
		$result = $this->fetchAll('1', $this->lang)->toArray();
		if($sort)
		{
			if(count($result) > 0)
			foreach($result as $row)
			{
				$results[$row['fraza']] = $row;
			}
			//var_dump($results);die();
			return @$results;
		}
		else return @$result;
	}
	function wypisz()
	{
		$this->common->getConfig();
		if(isset($_SESSION[$this->common->dbName]['tlumaczenia'][$this->lang])) return $_SESSION[$this->common->dbName]['tlumaczenia'][$this->lang];
		$sql = $this->select()->from(array('t' => 'Tlumaczenia'), array('fraza', $this->lang))->order('fraza asc');
		$results = $this->fetchAll($sql)->toArray();
		if(count($results) > 0)
		foreach($results as $result)
		{
			$array[$result['fraza']] = $result[$this->lang];
		}
		return $_SESSION[$this->common->dbName]['tlumaczenia'][$this->lang] = @$array;
	}
}
?>