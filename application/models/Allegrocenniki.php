<?php
class Allegrocenniki extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->module = $module;
		//$this->common = new Common(false, $module);
		//$this->obConfig = $this->common->getObConfig();
        //$this->db = $this->common->getDB($this->obConfig);
		//$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$sql = $this->select()->where('1')->order(array('nazwa asc'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function get($pole = 'id')
	{
		$sql = $this->select()->where('1')->order(array('nazwa asc'));
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i][$pole]] = $result[$i]->toArray();
		}
		return @$results;
	}
	function wypiszWhereNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa like "%'.$nazwa.'%"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa = "'.$nazwa.'"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	
	function usunID()
	{
		$result = $this->delete('id = '.$this->id);
	}
	function wypiszPojedynczego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}	
}
?>