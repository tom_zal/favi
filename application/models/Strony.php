<?php
class Strony extends Zend_Db_Table
{
    public $lang, $menu, $link;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		
		$this->menue = "";
    }

    function start()
	{
        $where = 'rodzic = 0';
        $order = 'pozycja';
        $result = $this->fetchAll($where, $order);
        //print_r($glowne);
        return $result;
    }

    function display_children_admin($parent, $level, $z=0)
	{
        $where = 'rodzic="'.$parent.'" AND lang = "'.$this->lang.'"';
        $order = ' pozycja ASC';

        $result = $this->fetchAll($where, $order);
        $row = $result->toArray();

        //  echo count($rowsetArray);

        for($i=0; $i<count($row);$i++)
		{
            if($row[$i]['rodzic']==0)
			{
                $this->menue.=''.str_repeat('',$level).'<dd>
							<a class="link" href="'.$this->link.'/admin/strony/edytuj/id/'.$row[$i]['id'].'">'.$row[$i]['nazwa'].'</a>
							<a href="'.$this->link.'/admin/strony/lista/del/'.$row[$i]['id'].'"> <img src="'.$this->link.'/public/images/admin/delete.png" /> </a>
							<a href="'.$this->link.'/admin/strony/edytuj/id/'.$row[$i]['id'].'">  <img src="'.$this->link.'/public/images/admin/edit.png" />   </a>
							<!--<a href="'.$this->link.'/admin/strony/dodaj/id/'.$row[$i]['id'].'">  <img src="'.$this->link.'/public/images/admin/add.png" /> </a>-->
								<form name="pozycjaaa" action="" method="post" style="float:right; margin-right:20px;">
									<input type="hidden" name="id" value="'.$row[$i]['id'].'" />
									<input type="text" name="pozycja" value="'.$row[$i]['pozycja'].'" style="width:30px; font-weight:bold;" />
									<input type="submit" name="pozycjonuj" value=" " class="pozycja" />
								</form>
							';
            }
			else
			{
                $this->menue.='<dd><a class="link" href="'.$this->link.'/admin/strony/edytuj/id/'.$row[$i]['id'].'">'.str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$level).' '.$row[$i]['nazwa'].'</a>
						   	<a href="'.$this->link.'/admin/strony/lista/del/'.$row[$i]['id'].'"><img src="'.$this->link.'/public/images/admin/delete.png" /></a>
							<a href="'.$this->link.'/admin/strony/edytuj/id/'.$row[$i]['id'].'"><img src="'.$this->link.'/public/images/admin/edit.png" /> </a>
							<!--<a href="'.$this->link.'/admin/strony/dodaj/id/'.$row[$i]['id'].'">  <img src="'.$this->link.'/public/images/admin/add.png" />    </a>-->
								<form name="pozycjaaa" action="" method="post" style="float:right; margin-right:20px;">
													<input type="hidden" name="id" value="'.$row[$i]['id'].'" />
													<input type="text" name="pozycja" value="'.$row[$i]['pozycja'].'" style="width:30px;" />
													<input type="submit" name="pozycjonuj" value=" " class="pozycja" />
								</form>
							</dd>';
            }

            $this->display_children_admin($row[$i]['id'], $level+1, $z++);
        }
    }

    function display_children_user($parent, $level, $style_main='link', $style_sub='link', $podswietl =0 , $z=0)
	{
        $where = 'rodzic="'.$parent.'" AND lang = "'.$this->lang.'"';
        $order = ' pozycja ASC';
        $result = $this->fetchAll($where, $order);
        $row = $result->toArray();
        for($i=0; $i<count($row);$i++)
		{
            if($row[$i]['rodzic']==0)
			{
                if($podswietl==$row[$i]['id']){
                    $this->menue.=''.str_repeat('',$level).'<dt class="'.$style_main.'"><a class="kat_gl_a" style="font-weight:bold;" href="'.$this->link.''.$this->linkZNazwy($row[$i]['nazwa']).'/id/'.$row[$i]['id'].'">'.$row[$i]['nazwa'].'</a></dt>';
                }
				else
				{
                    $this->menue.=''.str_repeat('',$level).'<dt class="'.$style_main.'"><a class="kat_gl_a" href="'.$this->link.''.$this->linkZNazwy($row[$i]['nazwa']).'/id/'.$row[$i]['id'].'">'.$row[$i]['nazwa'].'</a></dt>';
                }
            }
			else
			{
                if($podswietl==$row[$i]['id'])
				{
                    $this->menue.='<dd class="'.$style_sub.'"> <a class="podkat_gl_a" style="font-weight:bold;" href="'.$this->link.''.$this->linkZNazwy($row[$i]['nazwa']).'/id/'.$row[$i]['id'].'">'.str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$level).' '.$row[$i]['nazwa'].'</a></dd>';
                }
				else
				{
                    $this->menue.='<dd class="'.$style_sub.'"> <a class="podkat_gl_a" href="'.$this->link.''.$this->linkZNazwy($row[$i]['nazwa']).'/id/'.$row[$i]['id'].'">'.str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$level).' '.$row[$i]['nazwa'].'</a></dd>';
                }  
            }
            $this->display_children_user($row[$i]['id'], $level+1, $style_main, $style_sub,$podswietl, $z++);
        }
    }
    function display_footer($parent, $level, $style_main='link', $style_sub='link', $podswietl =0 , $z=0)
	{
        $where = 'rodzic="'.$parent.'" AND lang = "'.$this->lang.'"';
        $order = ' pozycja ASC';
        $result = $this->fetchAll($where, $order);
        $row = $result->toArray();
        for($i=0; $i<count($row);$i++)
		{
            $this->menue.='<a href="'.$this->link.'/id/'.$row[$i]['id'].'">'.$row[$i]['nazwa'].'</a><br>';
            $this->display_footer($row[$i]['id'], $level+1, $style_main, $style_sub,$podswietl, $z++);
        }
    }
    function linkZNazwy($nazwa)
    {
        $nazwa = str_replace(" ", "-", $nazwa);
        $nazwa = str_replace("ą", "a", $nazwa);
        $nazwa = str_replace("ę", "e", $nazwa);
        $nazwa = str_replace("ć", "c", $nazwa);
        $nazwa = str_replace("ś", "s", $nazwa);
        $nazwa = str_replace("ż", "z", $nazwa);
        $nazwa = str_replace("ź", "z", $nazwa);
        $nazwa = str_replace("ł", "l", $nazwa);
        $nazwa = str_replace("ó", "o", $nazwa);

        $nazwa = str_replace(" ", "_", $nazwa);
        $nazwa = str_replace("Ą", "a", $nazwa);
        $nazwa = str_replace("Ę", "e", $nazwa);
        $nazwa = str_replace("Ć", "c", $nazwa);
        $nazwa = str_replace("Ś", "s", $nazwa);
        $nazwa = str_replace("Ż", "z", $nazwa);
        $nazwa = str_replace("Ź", "z", $nazwa);
        $nazwa = str_replace("Ł", "l", $nazwa);
        $nazwa = str_replace("Ó", "o", $nazwa);
        
		//$nazwa = strtolower($nazwa);
        return $nazwa;
    }

    function changePozycja($id, $pozycja)
	{
        $update = array('pozycja' => $pozycja);
        $where = 'id = '.$id;
        $this->update($update, $where);
    }

    function kasuj($id
	{
        $where = 'id = '.$id;
        $this->delete($where);
        $s_where = 'rodzic='.$id;
        $result = $this->fetchAll($s_where);
        $rows = $result->toArray();

        for($i=0; $i < count($rows); $i++)
		{
            $id = $rows[$i]['id'];
            if($id)
			{
                $this->kasuj($id);
            }
        }
    }

    function dodaj_nowa($rodzic, $dane)
	{
        $where = 'rodzic = '.$rodzic;
        $order = 'pozycja';
        $limit = 1;
        $result = $this->fetchAll($where, $order, $limit);
        $arr = $result->toArray();
        $data = array('rodzic'=>$rodzic , 'nazwa' => $dane['nazwa'] , 'tekst' => $dane['tekst'], 'lang'=>"pl", 'pozycja' => ($arr[0]['pozycja']+1));
        print_r($data);
        $this->insert($data);
    }

    // wypisuje wybraną menu
    function showWybranaKategoria($id)
	{
        $where = 'id = '.$id;
        $result = $this->fetchRow($where);
        return $result;
    }

    public function edytuj($id, $dane, $img=null)
	{
        $where = 'id = '.$id;
        $this->update($dane, $where);
    }

    function get_path($id)
	{
        $where = 'id= '.$id ;
        $result = $this->fetchAll($where);
        //print_r($result);
        $path = array();
        if (@$result[0]->rodzic!='')
		{
            $path[] = array('id' => $result[0]->id, 'rodzic' => $result[0]->rodzic, 'nazwa' => $result[0]->nazwa);
            $path = array_merge($this->get_path($result[0]->rodzic), $path);
        }
        return $path;
    }
}
?>