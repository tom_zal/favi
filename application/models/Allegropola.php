<?php
class Allegropola extends Zend_Db_Table
{
    protected $_name = 'Allegropola';
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

    function dodaj($dane)
	{
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }
    function edytuj($dane)
	{
        $where = 'id = "'.$this->id.'"';
        $this->update($dane, $where);
    }
    function usun()
	{
        $where = 'id = "'.$this->id.'"';
        $this->delete($where);
    }
	
	function getOneByID()
	{
        $where = 'id = "'.$this->id.'"';
        $result = $this->fetchRow($where);
        return $result->toArray();
    }	
	function getOneByFormID($id)
	{
        $where = '`sell-form-id` = "'.$id.'"';
        $result = $this->fetchRow($where);
        return $result->toArray();
    }
	
	function getAll()
	{
		$sql = $this->select()->order('`sell-form-id`');
        $result = $this->fetchAll($sql);
        $res = $result->toArray();
        return $res;
    }	
	function getAllByKat($kat)
	{
		$where = '`sell-form-cat` = '.$kat.'';
		$sql = $this->select()->where($where)->order('sell-form-id');
        $result = $this->fetchAll($sql);
        $res = $result->toArray();
        return $res;
    }
	
	function getOneByName($title)
	{
		$where = '`sell-form-title` = "'.$title.'"';
        $result = $this->fetchRow($where);
        $res = $result->toArray();
        return $res;
    }
	
	function getPolaForKat($kategs)
	{
		$kats = explode(';', $kategs);
		for($i=count($kats)-1;$i>=0;$i--)
		{
			if($kats[$i] > 0)
			{
				$params = $this->getAllByKat($kats[$i]);
				if(count($params) > 0)
				$paramsAll[$kats[$i]] = $params;
			}
		}
		return isset($paramsAll) ? $paramsAll : null;
	}
	
	function getAllegroCount($webapi = null)
	{
		set_time_limit(0);
		try
		{
			if($webapi == null)
			{
				$konto = new Allegrokonta();
				$webapi = $konto->zaloguj();
				if($webapi == null) return $konto->error;
			}
			$opcje = array('offset' => 0, 'package-element' => 1);
			$pola = $webapi->objectToArray($webapi->GetSellFormFieldsExtLimit($opcje));
			return @intval($pola['form-fields-count']);
		}
		catch(SoapFault $error)
		{
			return 'B��d '.$error->faultcode.': '.$error->faultstring;
		}
	}
	function updateFromAllegro($webapi = null, $ile = 0, $start = 0, $limit = 0, $odrazu = false)
	{
		set_time_limit(0);
		try
		{
			if($webapi == null)
			{
				$konto = new Allegrokonta();
				$webapi = $konto->zaloguj();
				if($webapi == null) return $konto->error;
			}
			if($ile == 0)
			{
				$pola = $webapi->objectToArray($webapi->GetSellFormFieldsExt());
				if(count($pola) > 0)
				{
					$this->delete('1');
					if(@count($pola['sell-form-fields']) > 0)
					foreach($pola['sell-form-fields'] as $pole)
					{
						//var_dump($pole);
						$this->dodaj($pole);
					}
				}
			}
			else
			{
				$import = new Import();
				$opcje = array('offset' => $start / $ile, 'package-element' => $ile);
				if($limit == 0)	$limit = $this->getAllegroCount($webapi);
				if($limit > 0)
				{
					if($start == 0) $this->delete('1');
					for($od = $start; $od < $limit; $od += $ile)
					{
						//ob_clean();
						$ilosc = $import->getTabelaCount('allegropola');
						$pola = $webapi->objectToArray($webapi->GetSellFormFieldsExtLimit($opcje));
						//echo 'od = '.$od.' ile = '.$ile.' limit = '.$limit.' pola '.count($pola).'<br>';
						if(true && @count($pola) > 0)
						{
							$i = 0;
							if(@count($pola['sell-form-fields']) > 0)
							foreach($pola['sell-form-fields'] as $pole)
							{
								//var_dump($pole);
								$this->dodaj($pole);
								if(++$i % 20 == 0) @file_put_contents('../public/ajax/importpola.txt', $ilosc + $i);
							}
						}
						//var_dump($pola);
						$opcje['offset']++;
						if(!$odrazu) break;
					}
				}
			}
		}
		catch(SoapFault $error)
		{
			return 'B��d '.$error->faultcode.': '.$error->faultstring;
		}
	}
}
?>