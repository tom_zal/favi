<?php 
class Ileonline extends Zend_Db_Table
{
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
    function sprawdz($ip, $php_self)
	{
        $where = 'czas < '.(time() - 60 * 60 * 10);
        //$this->delete($where);
		$where = 'ip = "'.$ip.'"';
		$select = $this->select()->where($where);
		$row = $this->fetchAll($select);
		if(count($row) > 0)
		{
			$dane = array('czas' => time());
			$where = 'ip = "'.$ip.'"';
			$this->update($dane, $where);		
		}
		else
		{
			$dane = array('czas' => time(), 'ip' => $ip, 'plik' => $php_self);
			$this->insert($dane);
			$this->setOdwiedzin();
		}
		$where = 'czas > '.(time() - 60 * 10);
        $select = $this->select()->distinct()->from(array('p' => 'Ileonline'), 'ip')->where($where);
        $liczba = $this->fetchAll($select);		
        return count($liczba);
    }	
	function setOdwiedzin()
	{
		$settings = new Ustawienia();
		$ustawienia = $settings->wypisz();
		$odwiedzin = floatval($ustawienia['odwiedzin']) + 1;
		$settings->updateData(array('odwiedzin' => $odwiedzin));
		return $odwiedzin;
	}
	function messageNowy($ip)
	{
		$where = 'ip = "'.$ip.'" and czas < '.(time() - 10);
        $select = $this->select()->distinct()->from(array('p' => 'Ileonline'), 'ip')->where($where);
        $liczba = $this->fetchAll($select);//var_dump($liczba->toArray());echo $where;//die();
        if(count($liczba) == 0)
		{
			$main = new Podstrony();
			$main->link = 'Komunikat-dla-nowych-blok';
			$message = $main->getPodstrona();
			return stripslashes($message->tekst);
		}
    }
}
?>