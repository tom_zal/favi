<?php
class Subskrypcja extends Zend_Db_Table
{
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		$this->czasTypy = array(1 => 'dzień', 0 => 'tydzień', 2 => 'miesiąc', 3 => 'rok');		
		$this->typyAkcjiRozne = array('aktualnosci'=>'Aktualności','artykuly'=>'Artykuły','porady'=>'Porady','realizacje'=>'Realizacje');
		$this->typyAkcjiOznacz = array('nowosc'=>'Nowości','polecane'=>'Polecane','wyprzedaz'=>'Wyprzedaż');
    }
		
    function wypisz($lang)
	{
        $where = 'lang = "'.$lang.'"';
        $result = $this->fetchAll($where)->toArray();
        return $result;
    }
	function wypiszMaile($lang)
	{
        $where = 'lang = "'.$lang.'"';
        $select = $this->select()->where('email <> ""')->group('email')->order('email');
		$result = $this->fetchAll($select);
		//var_dump($result);die();
        for ($i = 0; $i < count($result); $i++)
		{
			$results[trim($result[$i]['email'])] = $result[$i]->toArray();
		}
		unset($result);
		//var_dump($results);die();
		return @$results;
    }
    function wypiszAktywne($lang)
	{
        $where = 'lang = "'.$lang.'" AND status = 1';
        $result = $this->fetchAll($where)->toArray();
        return $result;
    }
	function wypiszOsobyDoNewslettera($typ = 0, $status = "")
	{
		$pole = $typ ? 'komorka' : 'email';
		//array('CONCAT(komorka,",",email",imie,",",nazwisko,",",id)'))
        $select = $this->db->select()->from(array('s' => 'Subskrypcja'), array($pole))
			->where('lang = "'.$this->lang.'" and '.(strlen($status) == 1 ? 'status = "'.intval($status).'"' : "1"));
		//echo $select;
		$result = $this->db->fetchAll($select);
		if(count($result) > 0)
		foreach($result as $row)
		{
			if(@!empty($row[$pole]))
			$rows[] = $row[$pole];
		}
        return @$rows;
    }
	function sprMail($mail)
	{
        $where = 'email = "'.$mail.'"';
        $result = $this->fetchAll($where)->toArray();
        return $result;
    }
    function dodaj($email, $lang = "pl", $status = 1)
	{
        $dane['email'] = $email;
        $dane['lang'] = $lang;
        $dane['status'] = $status;
        return $this->insert($dane);
    }
	function dodajAll($dane)
	{
        $this->insert($dane);
    }

    function edytuj($id, $dane)
	{
        $where = 'id = '.$id;
        $this->update($dane, $where);
    }
	function usunMail($email)
	{
        $this->delete('email = "'.$email.'"');
    }
    function usun($id)
	{
        $this->delete('id = '.$id);
    }
	
	function getTresc($tryb = '', $rodzaj = '')
	{
		$status = $rodzaj ? "" : "";
		if(@in_array($tryb, array_keys($this->typyAkcjiRozne)))
		{
			$aktualnosciTable = ucfirst($tryb);
			$aktualnosci = new $aktualnosciTable();
			$aktualnosciNews = $aktualnosci->wypiszDoNewslettera($this->lang, $status);
			if(count($aktualnosciNews) == 0) return null;
			return $this->stworzTrescAktualnosci($aktualnosciNews, $tryb);
		}
		if(@in_array($tryb, array_keys($this->typyAkcjiOznacz)))
		{
			$nowosci = new Produkty();
			$nowosciNews = $nowosci->wypiszProduktyDoNewslettera($tryb, $this->lang, $status);
			if(count($nowosciNews) == 0) return null;
			$this->common = new Common(true, 'admin');
			$nowosciNews = $this->common->produktyGaleria($nowosciNews);
			return $this->stworzTrescProdukty($nowosciNews);
		}
	}

    function wyslijSubskrypcje($typ, $dane, $lang = 'pl')
	{
        $i = 0;
        foreach($dane as $key => $value) 
		{
            $data[$i]['id'] = $key;
            $i++;
        }
        if($typ == 'aktualnosci') $body = $this->stworzTrescAktualnosci($data, $typ);
        if($typ == 'artykuly') $body = $this->stworzTrescAktualnosci($data, $typ);
		if($typ == 'porady') $body = $this->stworzTrescAktualnosci($data, $typ);
		if(!isset($body)) $body = $this->stworzTrescProdukty($data);

        $emaile = $this->wypiszAktywne($lang);
        $wyslij = new Wiadomosci();
        $bledy = '';
		//echo $body;die();

        for($i=0;$i<count($emaile);$i++)
		{
            if(isset($maile[$emaile[$i]['email']])) continue;
            $bledy .= $wyslij->sendMsg($emaile[$i]['email'], $typ, $body);
			$maile[$emaile[$i]['email']] = $emaile[$i]['email'];
        }
		
		$kontrahent = new Kontrahenci();
		$zgoda = $kontrahent->wypiszKlienciNewsletter();
		for($i=0;$i<count($emaile);$i++)
		{
			if(isset($maile[$emaile[$i]['email']])) continue;
            $bledy .= $wyslij->sendMsg($zgoda[$i]['email'], $typ, $body);
			$maile[$emaile[$i]['email']] = $emaile[$i]['email'];
        }
		
        return $bledy;
    }

    function stworzTrescProdukty($dane)
	{
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'general');
		$this->ustawienia = $this->common->getUstawienia();
        $body = '<table border="0" width="650"><tr><td>';
        $produkty = new Produkty();
        for ($i = 0; $i < count($dane); $i++)
		{
            $produkty->id = $dane[$i]['id'];
            $produkt = $produkty->wypiszPojedyncza()->toArray();			
            $all = $this->Galeria($produkt);
			$skrot=$this->ustawienia['skrot']?$all['skrot']:mb_substr(strip_tags($all['tekst']),0,300,'UTF-8').'...';
			//var_dump($skrot);die();
            $body .='
            <div style="float: left; width: 650px; margin-top: 5px; _display: inline; padding-bottom: 10px; border-bottom: 1px solid silver;">
                <a style="float: left; margin: 5px; width: 163px; height: 129px; _display: inline; cursor: pointer; background: url('.$obConfig->www.'/public/images/strona/ramka.png) center no-repeat; text-align:center;" href="'.$obConfig->www.'/' . $all['link'] . '/">
                    <img style="float: none; max-width: 157px; max-height: 123px; margin: 3px; _display: inline;" src="'.$obConfig->www.'/public/admin/zdjecia/miniaturki/' . $all['img'] . '" alt="' . $all['nazwa'] . '" />
                </a>
                <a href="'.$obConfig->www.'/' . $all['link'] . '/" style="float: left; font-weight: bold; color: #191944; width: 465px; padding: 5px; margin-top: 5px; background-color: #999999; display: block; font-size: 14px;">' . $all['nazwa'] . '</a>
                <div style="float: left; width: 465px; margin: 5px; color: #3d5796;  _display: inline;">' . $skrot . '</div>
            </div>';
            $data = array('status' => 1);
            $produkty->id = $dane[$i]['id'];
            $produkty->edytujProdukty($data);
        }
        $body .= '</td></tr></table>';
        return $body;
    }

    function Galeria($prod)
	{
        $galmain = new Galeria;
        $img_main = $galmain->wyswietlGlowneProdukt($prod['id']);
        if(!isset($img_main['img'])) $img_main['img'] = 'brak.jpg';
        $produkty = array
		(
            'nazwa' => $prod['nazwa'],
            'skrot' => $prod['skrot'],
			'tekst' => $prod['tekst'],
            'link' => $prod['link'],
            'img' => $img_main['img']
        );
        return $produkty;
    }

    function stworzTrescAktualnosci($dane, $typ) 
	{
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'general');
        $body = '<table border="0" width="475"><tr><td>';
        if($typ == 'aktualnosci') $aktualnosci = new Aktualnosci();
		if($typ == 'artykuly') $aktualnosci = new Artykuly();
		if($typ == 'porady') $aktualnosci = new Porady();
		if(!isset($aktualnosci)) $aktualnosci = new Aktualnosci();
        
		for ($i = 0; $i < count($dane); $i++) 
		{
            $id = $dane[$i]['id'];
            $all = $aktualnosci->pojedyncza($id)->toArray();
			$skrot=$obConfig->aktualnosciSkrot?$all['skrot']:mb_substr(strip_tags($all['tekst']),0,300,'UTF-8').'...';
            $body .='
            <div style="float: left; width: 650px; margin-top: 5px; _display: inline; padding-bottom: 10px; border-bottom: 1px solid silver;">
                <a href="'.$obConfig->www.'/Aktualnosc/nr/' . $all['id'] . '/" style="float: left; font-weight: bold; color: #191944; width: 640px; padding: 5px; margin-top: 5px; background-color: #999999; display: block; font-size: 14px;">' . $all['temat'] . '</a>
                <div style="float: left; width: 640px; margin: 5px; color: #3d5796;  _display: inline;">' . $skrot . '</div>';
				if(true) $body .=
                '<a href="'.$obConfig->www.'/Aktualnosc/nr/' . $all['id'] . '/" style="float:right;">
                    Zobacz więcej...
                </a>';
            $body .=' </div>';
            $data = array('status' => 1);
            $id = $dane[$i]['id'];
            $aktualnosci->edytuj($data, $id);
        }
        $body .= '</td></tr></table>';
        return $body;
    }
	
	function wypiszOsoby($ilosc, $od, $ile, $sortowanie, $szukanie, $ids = null)
	{
		$od = $od * $ile;
		$limit = $ile;
		if($ilosc) { $od = 0; $limit = 9999; }
		$where = '1';
		if(!empty($ids) && is_array($ids) && count($ids) > 0) $where .= ' and id in ('.implode(',',$ids).')';
		
		if($szukanie !== null)
		{
			if($szukanie->nazwa != '')
			{
				$where .= ' and (imie like "%'.$szukanie->nazwa.'%"';
				$where .= ' or nazwisko like "%'.$szukanie->nazwa.'%"';
				$where .= ' or email like "%'.$szukanie->nazwa.'%"';
				$where .= ' or komorka like "%'.$szukanie->nazwa.'%"';
				$where .= ' or telefon like "%'.$szukanie->nazwa.'%")';
			}
			if($szukanie->data_rej != '')
			{
				$where .= ' and data_rej >= "'.$szukanie->dataOd.' 00:00:00"';
				$where .= ' and data_rej <= "'.$szukanie->dataDo.' 23:59:59"';
			}
			if(@$szukanie->status != '') $where .= ' and status = "'.$szukanie->status.'"';
		}
		
		if($sortowanie == null) 
		{
			$sortowanie = (object)array();
			$sortowanie->sort = 'data_rej';
			$sortowanie->order = 'desc';
		}
		
        $sql = $this->db->select()->from(array('s' => 'Subskrypcja'), array($ilosc?'count(distinct s.id)':'*'));		
		$sql->where($where);
		//if(!$ilosc) $sql->group('s.id');
		$sql->order($sortowanie->sort.' '.$sortowanie->order);
		if(!$ilosc) $sql->limit($limit, $od);
		
		//if(!$ilosc){echo $sql;return null;}
        $result = $this->db->fetchAll($sql);
        return $ilosc ? @intval($result[0]['count(distinct s.id)']) : $result;
    }
}
?>