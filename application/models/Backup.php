<?php
class Backup extends Zend_Db_Table
{ 
    function fileBackup($path, $baseUrl)
	{
        $filename = 'pliki_'.date("Y-m-d_H-i-s").'.zip';
		$this->path = str_replace('//','/',$_SERVER['DOCUMENT_ROOT'].$baseUrl);
        ob_start();
		ini_set('max_input_time', 0);		
        ini_set('max_execution_time', 0);
		$zip = 'zip -r '.$this->path.str_replace('../','/',$path).''.$filename.' '.$this->path.'/public/admin/zdjecia';
		//echo $zip; die();
        $system = system($zip);
		//var_dump($zip); die();
        ob_end_clean();
        return $system;
        
		$zip = new ZipArchive();
        $plik_arch = $path.$filename;
        if($zip->open($plik_arch, ZIPARCHIVE::CREATE) !== TRUE)
		{
           return ("nie mog� zrobi� pliku archiwum <$plik_arch>");
        }

        $zip->addFromString("info.txt", "Archiwum o nazwie $plik_arch zosta�o utworzone w dniu ".date("Y-m-d H:i:s"));
        
        foreach(new DirectoryIterator('../public/admin/zdjecia') as $file)
		{
            if(!$file->isDot())
			{
                $sciezka = "../public/admin/zdjecia/".$file->getFilename();       
                //echo $sciezka.'<br />';
                $zip->addFile($sciezka);
            }
        }
    
		if(false)
		{
			$dirs = glob('../public/admin/zdjecia/*', GLOB_ONLYDIR);
			foreach($dirs as $dir)
			{
				foreach(new DirectoryIterator($dir) as $file)
				{
					if(!$file->isDot())
					{
						$sciezka = $dir.'/'.$file->getFilename(); 
						//echo $sciezka.'<br />';
						$zip->addFile($sciezka);
					}
				}
			}
		}
		
		//echo "Ilo�� plik�w spakowanych: ".(($zip->numFiles)-1) . "\n";
		//dump($zip);
		$zip->close();		   
	}
	
	function baseBackup($config, $path, $baseUrl)
	{
		$dbHost = $config->host;
		$dbUsername = $config->username;
		$dbPassword = $config->password;
		$dbName = $config->dbname;  
		$filename = 'baza_danych_'.date("Y-m-d_H-i-s").".sql";
		$file = $path.$filename;
		
		//$file = '../data/cos11.sql';
		if(false)
		{
			$command = sprintf("mysqldump -u %s --password=%s -d %s --skip-no-data > %s",
				escapeshellcmd($dbUsername),
				escapeshellcmd($dbPassword),
				escapeshellcmd($dbName),
				escapeshellcmd($file)
			);
			//var_dump($command);//die();
			$exec = exec($command);
			//var_dump($exec);die();
		}
		else
		try
		{
			$dumpSettings = array('no-create-info'=>false);
			$backup = new MySQLDump();
			$backup->droptableifexists = false;
			$backup->connect($dbHost, $dbUsername, $dbPassword, $dbName); //Connect To Database
			if(!$backup->connected) { return ($backup->mysql_error); } //On Failed Connection, Show Error.
			else
			{
				$backup->list_tables(); //List Database Tables.
				$broj = count($backup->tables); //Count Database Tables.
				$dd = '';
				$import = new Import();
				for($i=0;$i<$broj;$i++)
				{
					$table_name = $backup->tables[$i]; //Get Table Names.
					if(@in_array($table_name, $import->notAllowed)) continue;
					$backup->dump_table($table_name); //Dump Data to the Output Buffer.
					$dd .= htmlspecialchars($backup->output); //Display Output.
				}
				$handle = fopen($file,'w+');
				fwrite($handle,$dd);
				fclose($handle);
				
				$zip = new ZipArchive();
				$filename2 = str_replace('.sql','.zip',$file);
				$plik_arch = $filename2;
				if($zip->open($plik_arch, ZIPARCHIVE::CREATE) !== TRUE)
				{
					return ("nie mog� zrobi� pliku archiwum <$plik_arch>");
				}
				$zip->addFile($file, $filename);
				$zip->close();
			}
		}
		catch(Exception $e)
		{
			return 'mysqldump-php error: ' . $e->getMessage();
		}
	}
	
	function pobierzLista()
	{
		$dane = array();
		$sciezka = "../public/admin/backup/"; 
		$i = 0;
		foreach(new DirectoryIterator($sciezka) as $file)
		{
		    if(!$file->isDot())
		    {
			    $dane[$i] = Array('nazwa' => $file->getFilename());
			    $i++;
		    }      
		}
		return $dane;
	}
	
	function kasuj($plik)
	{
		$sciezka = '../public/admin/backup/';
		unlink($sciezka.$plik);
	}        
}
?>