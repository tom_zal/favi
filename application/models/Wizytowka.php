<?php
class Wizytowka extends Zend_Db_Table
{
	public $link, $id;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodajWizytowke($dane)
	{
		$route = new Routers();
		$nowy = $route->dodaj($dane['link'], '', 'default', 'podstrony' ,'sprzedawca');
		if($nowy['id'] > 0) 
		{
			$dane['link'] = $nowy['link'];
			$this->insert($dane);
		}
		return $nowy;
	}
	
	function edytujWizytowke($dane)
	{
		$where = 'link = "'.$this->link.'"';
		$this->update($dane, $where);
	}
	
	function ustawDomyslny()
	{
		$where = 'domyslny = "T" and id != "'.$this->link.'"';
		$this->update(array('domyslny' => "N"), $where);
		$where = 'id = '.$this->link.'';
		$this->update(array('domyslny' => "T"), $where);
	}
	
	function wypiszWizytowki()
	{
		$result = $this->fetchAll();
		return $result;
	}
	
	function wypiszWizytowkiForProdukt($produkt)
	{
		$where = 'id = '.$produkt;		 
		$result = $this->fetchAll($where);
		return $result;			
	}
	
	function wypiszWizytowkiForProduktAll()
	{
		$result = $this->fetchAll();			
		for($i=0; $i<count($result); $i++)
		{
			$producent[$result[$i]->id] = $result[$i]->nazwa;		
		}
		return $producent;			
	}
	
	function usunWizytowke()
	{
		$where = 'link = "'.$this->link.'"';
		$this->delete($where);
		$route = new Routers();
		$route->usunPodstrony($this->link);			
	}
	function wypiszPojedynczego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function getWizytowka()
	{
		$result = $this->fetchRow('link = "'.$this->link.'"');
		return $result;
	}
	function getDomyslny()
	{
		$result = $this->fetchRow('domyslny = "T"');
		return $result;
	}
	
	function getWizytowkaForZamowioneProdukty($produkty)
	{
		$wizytowka = '';
		for($i=0;$i<count($produkty);$i++)
		{
			$katprod = new Katprod();
			$kats = $katprod->kategorieProduktu($produkty[$i]['id_prod']);
			for($j=count($kats)-1;$j>=0;$j--)
			{
				if($kats[$j]['sprzedawca'] > 0)
				{
					$wizytowki = new Wizytowka();
					$wizytowki->id = $kats[$j]['sprzedawca'];
					$sprzedawca = $wizytowki->wypiszPojedynczego();
					$wizyt = $sprzedawca['wizytowka'];
					if(!empty($wizytowka) && $wizytowka != $wizyt)
					{
						$default = true;
						break;
					}
					$wizytowka = $wizyt;
					break;
				}
			}
		}
		if(empty($wizytowka) || isset($default))
		{
			$wizytowki = new Wizytowka();
			$default = $wizytowki->getDomyslny();
			$wizytowka = $default['wizytowka'];
		}
		return $wizytowka;
	}
}
?>