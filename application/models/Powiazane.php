<?php

class Powiazane extends Zend_Db_Table {

    public $link, $id;

    public function __construct($module = 'admin') {
        parent::__construct();
        $this->_name = 'Produktypowiazania';
        $this->common = new Common(false, $module);
        $this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
        $this->lang = $this->common->getJezyk($module);
    }

    function add($dane) {
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }

    function check($dane) {
        $result = $this->fetchRow('id_prod = "' . $this->id . '" AND powiazany = "' . $dane['powiazany'] . '"');
        return $result;
    }

    function getRows() {
        $select = $this->select()->from(array('pp' => $this->_name), array('*'))->setIntegrityCheck(false)
                        ->joinLeft(array('p' => 'Produkty'), 'pp.powiazany = p.id', array('nazwa', 'link'))
                        ->where('pp.id_prod = "' . $this->id . '"')->order('p.nazwa');
//        echo $select;die;
        $result = $this->fetchAll($select)->toArray();
        return $result;
    }

    function deletePow($dane, $krzyz = true) {
        $where = '(0';
        if (true)
            $where .= ' or (id_prod = "' . $dane['id_prod'] . '" AND powiazany = "' . $dane['powiazany'] . '")';
        if ($krzyz)
            $where .= ' or (id_prod = "' . $dane['powiazany'] . '" AND powiazany = "' . $dane['id_prod'] . '")';
        $where .= ' )';
        return $this->delete($where);
    }

    function _delete() {
        $result = $this->delete('(id_prod = "' . $this->id . '") OR (powiazany = "' . $this->id . '")');
    }

    function _deleteIds($ids) {
        $result = $this->delete('id_prod in (' . $ids . ') or powiazany in (' . $ids . ')');
    }

    function getRowsFront($order = 'nazwa') {
        $where_search = "";
        if ($order == 'cena_brutto') {
            $order = 'cena';
            $where_search = ', (IF(`p`.`cena_promocji_b` = 0, `p`.cena_brutto, `p`.cena_promocji_b)) as cena';
        } elseif ($order == 'cena_brutto_hurt') {
            $order = 'cena';
            $where_search = ', (IF(`p`.`cena_promocji_b_hurt` = 0, `p`.cena_brutto_hurt, `p`.cena_promocji_b_hurt)) as cena';
        }

        $sql = 'SELECT DISTINCT p.* ' . $where_search . ', g.img, g.nazwa as img_opis
				FROM ' . $this->_name . ' AS pp
				LEFT JOIN Produkty AS p
				ON pp.powiazany = p.id
				LEFT JOIN Galeria AS g
				ON g.wlasciciel = p.id AND g.glowne = "T" and g.typ = "oferta" and g.lang = p.lang and g.wyswietl
				WHERE pp.id_prod = "' . $this->id . '" AND p.widoczny
				GROUP BY p.id
				ORDER BY ' . $order . ' ASC';

        $result = $this->getAdapter()->fetchAll($sql);
        return $result;
    }

    function getRowsCount() {
        $select = $this->select()
                ->distinct()
                ->from(array('pp' => $this->_name))
                ->setIntegrityCheck(false)
                ->joinLeft(array('p' => 'Produkty'), 'pp.powiazany = p.id', array('COUNT(*) AS num'))
                ->where('pp.id_prod = "' . $this->id . '" AND p.widoczny');

        $result = $this->fetchRow($select);
        return $result->num;
    }

}

?>