<?php

class Aktualnosci extends Zend_Db_Table {

    public $link, $id, $obConfig, $db;

    public function __construct($module = 'admin') {
        parent::__construct();
        $this->common = new Common(false, $module);
        $this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
        $this->lang = $this->common->getJezyk($module);
    }

    function wypiszGlowna($lang = "pl") {
        $where = 'glowna = "on" AND lang = "' . $lang . '"';
        $order = array('pozycja asc', 'data DESC');
        $result = $this->fetchAll($where, $order);
        return $result;
    }

    function wypiszDoNewslettera($lang = "pl") {
        $where = 'status = 0 AND lang = "' . $lang . '"';
        $order = array('pozycja asc', 'data DESC');
        $result = $this->fetchAll($where, $order);
        return $result;
    }

    function wypisz() {
        $result = $this->fetchAll();
        return $result;
    }

    function dodaj($dane, $lang = "pl") {
        $dane['lang'] = $lang;
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }

    function edytuj($dane, $id) {
        $where = 'id =' . $id;
        $this->update($dane, $where);
    }

    function pojedyncza($id) {
        $where = 'id = ' . $id;
        $print = $this->fetchRow($where);
        return $print;
    }

    function wypiszAktualnosciAdmin($lang = "pl", $typ = 1) {
        $where = 'typ = ' . $typ . ' AND lang = "' . $lang . '"';
        $order = array('pozycja asc', 'data DESC');
        $result = $this->fetchAll($where, $order);
        return $result;
    }

    function wypiszAktualnosci($lang = "pl", $typ = 1, $id = 0, $ile = 9999, $glowna = false) {
        $where = 'a.typ = ' . $typ . ' AND a.lang = "' . $this->lang . '" and a.wyswietl = "1"';
        if ($id > 0)
            $where .= ' and a.id <> ' . $id;
        if ($glowna)
            $where .= ' and a.glowna = "1"';
        $order = array('a.pozycja asc', 'a.data DESC');
        $sql = $this->db->select()->from(array('a' => 'Aktualnosci'), array('a.*'));
        if ($this->obConfig->podstronyGaleria) {
            $whereGal = 'a.id = g.wlasciciel and g.typ = "aktualnosci" and g.glowne = "T"';
            $whereGal.= ' and g.lang = "' . $this->lang . '" and g.wyswietl = "1"';
            $sql->joinleft(array('g' => 'Galeria'), $whereGal, array('img'));
        }
        $sql->where($where)->order($order)->limit($ile);
        //echo $sql;die();
        $result = $this->db->fetchAll($sql);
        //var_dump($result);die();
        return $result;
    }
    
    function newsAll($lang = "pl", $typ = 1){
        $sql = $this->db->select()->from(array('a' => 'Aktualnosci'), array('a.*'));
        $where = 'a.typ = ' . $typ . ' AND a.lang = "' . $this->lang . '" and a.wyswietl = "1"';
        $order = array('a.pozycja asc', 'a.data DESC');
        $sql->where($where)->order($order)->limit($ile);
        
        $result = $this->db->fetchAll($sql);
//        dump($result);die;
        return $result;
    }
    function wypiszAktualnosciGlowna($lang = 'pl', $typ = 1, $ile = 9999) {
        $where = 'typ = ' . $typ . ' AND lang = "' . $lang . '" AND glowna AND wyswietl = "1"';
        $order = array('pozycja asc', 'data DESC');
        $sql = $this->select()->where($where)->order($order)->limit($ile);
        $result = $this->fetchAll($sql);
        return $result;
    }

    function wypiszAktualnoscNajnowsza($lang = "pl", $typ = 1) {
        $where = 'typ = ' . $typ . ' AND lang = "' . $lang . '"';
        $order = array('pozycja asc', 'data DESC');
        $result = $this->fetchRow($where, $order, 1);
        return $result;
    }

}

?>