<?php
class Blad
{
    /** @var string ___FOR_ZEND_minOccurs=1 ___FOR_ZEND_maxOccurs=1 */
    public $blad;
}
class Tescik
{
    /** @var string */
    public $string;
	/** @var integer */
    public $integer;
	/** @var float */
    public $float;
	/** @var string */
    public $blad;
}
class Uzytkownik
{
    /** @var string */
    public $login;
	/** @var string */
    public $haslo;
}
class Ustawienia
{
    /** @var boolean */
    public $rabat_rodzaj;
	/** @var boolean */
    public $rabat_od;
	/** @var float */
    public $rabat_standard;	
	/** @var float */
    public $rabat_vip_mala;
	/** @var float */
    public $rabat_vip_duza;
	/** @var float */
    public $blokadakwota;
	/** @var integer */
    public $opcjailosczak;
	/** @var string */
    public $start;
	/** @var string */
    public $koniec;
	/** @var string */
    public $blad;
}
class KlientWymaganeDane
{
    /** @var string */
    public $nazwisko;
	/** @var string */
    public $imie;
	/** @var string */
    public $ulica;
	/** @var string */
    public $kod;
	/** @var string */
    public $miasto;
}
class KlientDaneDoEdycji
{
    /** @var string */
    public $nazwisko;
	/** @var string */
    public $imie;
	/** @var string */
	public $email;
	/** @var string */
    public $telefon;
	/** @var string */
    public $ulica;
	/** @var string */
	public $nr;
	/** @var string */
	public $mieszkanie;
	/** @var string */
    public $kod;
	/** @var string */
    public $miasto;
}
class KlientKarta
{
	/** @var string */
    public $karta;
	/** @var integer */
    public $kartavip;
	/** @var float */
    public $rabatstandard;
}
class Klient
{
    /** @var string */
    public $nazwisko;
	/** @var string */
    public $imie;
	/** @var string */
    public $email;
	/** @var string */
    public $telefon;
	/** @var string */
    public $adres;
	/** @var string */
    public $kod;
	/** @var string */
    public $miasto;
	/** @var string */
    public $data_rej;
	/** @var string */
    public $karta;
	/** @var integer */
    public $kartavip;
	/** @var float */
    public $konto;
	/** @var float */
    public $rabatstandard;
	/** @var float */
    public $suma;
	/** @var string */
    public $blad;
}
class Zakup
{
    /** @var string */
    public $id_ext;
	/** @var string */
    public $data;
	/** @var string */
    public $karta;
	/** @var float */
    public $kwota;
	/** @var float */
    public $kwotazaplata;
	/** @var float */
    public $rabat;
	/** @var string */
    public $rabatinfo;
	/** @var float */
    public $rabatotrz;
	/** @var float */
    public $rabatwyk;
	/** @var float */
    public $konto;
	/** @var string */
    public $blad;
}

class Lojalnosc
{
	public $login = 'yoursoft';
	public $haslo = '4a801fc3';
	public $common;
	
	public function __construct()
	{
		$this->common = new Common();
    }
	
	/**
	* Zwraca Tescik (do użytku wewnętrznego).
	* @return Tescik
	*/
	function Test()
	{
		$res = new Tescik();
		$res->string = 'string';
		$res->integer = 223;
		$res->float = 467.24;
		$res->blad = '';
		return $res;
	}
	
	/**
	* Zwraca błąd (do użytku wewnętrznego).
	* Wszystkie funkcje po napotkaniu błędu zwracają go za pomocą tej funkcji
	* @param Blad
	* @param string $blad
	* @return Blad
	*/	
	function ZwrocBlad($res = null, $blad = '')
	{
		if($res == null)
		$res = new Blad();
		$res->blad = $blad;
		return $this->common->convertTableTypes($this->table, $res);
		return $res;
		return array('blad' => $blad);
	}
	
	/**
	* Dokonuje autoryzacji użytkownika.
	* Wywoływana na początku każdej funkcji, nie ma potrzeby wywoływania jej oddzielnie
	* (choć można to zrobić w celu sprawdzenia poprawności loginu i hasła).
	* @param Uzytkownik
	* @return Blad
	*/
	function SprawdzLogin($uzytkownik)
	{
		$ok = true;
		$uzytkownik = (array)$uzytkownik;
		if(!is_array($uzytkownik)) $ok = false;
		if(!isset($uzytkownik['login']) || $uzytkownik['login'] != $this->login) $ok = false;
		if(!isset($uzytkownik['haslo']) || $uzytkownik['haslo'] != $this->haslo) $ok = false;
		return $ok ? $this->ZwrocBlad(null, ' ') : $this->ZwrocBlad(null, 'Brak albo niepoprawny login lub hasło!');
	}
	
	/**
	* Zwraca ustawienia systemu
	* @param Uzytkownik
	* @return Ustawienia
	*/
	function UstawieniaSystemu($uzytkownik = null)
	{
		$this->table = 'Lojalnoscustawienia';
		$res = new Ustawienia();
		$result = $this->SprawdzLogin($uzytkownik);
		$res->blad = @trim($result->blad);
		if(!empty($res->blad)) return $this->ZwrocBlad($res, $res->blad);
		//return $res;
		$settings = new Lojalnoscustawienia();
		$ustawienia = $settings->wypisz();
		foreach($res as $arr => $val)
		if($arr != 'blad') $res->$arr = $ustawienia[$arr];
		return $this->common->convertTableTypes($this->table, $res);
	}

	/**
	* Zwraca dane klienta dla podanego numeru karty
	* @param Uzytkownik
	* @param string $karta
	* @return Klient
	*/
	function KlientPoNumerzeKarty($uzytkownik = null, $karta = '')
	{
		$this->table = 'Kontrahenci';
		$res = new Klient();
		$result = $this->SprawdzLogin($uzytkownik);
		$res->blad = @trim($result->blad);
		if(!empty($res->blad)) return $this->ZwrocBlad($res, $res->blad);
		
		$karta = @strval($karta);
		if(empty($karta)) return $this->ZwrocBlad($res, 'Nie podano numeru karty');
		$kontrahenci = new Kontrahenci();
		$kontrahenci->karta = $karta;
		$kontrahent = $kontrahenci->getCard();
		if(empty($kontrahent)) return $this->ZwrocBlad($res, 'Nie znaleziono podanego numeru karty: '.$karta);
		else
		{
			$klient = $kontrahent->toArray();
			$klient['suma'] = $kontrahenci->getSum($klient['id']);
			$klient['adres'] = $kontrahenci->getAdres($klient);
			
			foreach($res as $arr => $val)
			if($arr != 'blad') $res->$arr = $klient[$arr];
			return $this->common->convertTableTypes('Kontrahenci', $res);
		}
	}
	
	/**
	* Zwraca dane klienta dla podanych danych osobowych
	* @param Uzytkownik
	* @param KlientWymaganeDane
	* @return Klient
	*/
	function KlientPoDanychOsobowych($uzytkownik = null, $dane = null)
	{
		$this->table = 'Kontrahenci';
		$res = new Klient();
		$result = $this->SprawdzLogin($uzytkownik);
		$res->blad = @trim($result->blad);
		if(!empty($res->blad)) return $this->ZwrocBlad($res, $res->blad);
		
		$dane = (array)$dane;
		if(empty($dane)) return $this->ZwrocBlad($res, 'Nie podano danych osobowych');
		$KlientWymaganeDane = new KlientWymaganeDane();
		foreach($KlientWymaganeDane as $pole => $dana)
		{
			if(@empty($dane[$pole])) return $this->ZwrocBlad($res, 'Nie podano wymaganego pola: '.$pole);
		}
		$kontrahenci = new Kontrahenci();
		$kontrahent = $kontrahenci->wypiszPoDanych($dane);
		if(empty($kontrahent)) return $this->ZwrocBlad($res, 'Nie znaleziono klienta dla podanych danych osobowych');
		else
		{
			$klient = $kontrahent->toArray();
			$klient['suma'] = $kontrahenci->getSum($klient['id']);
			$klient['adres'] = $kontrahenci->getAdres($klient);
			foreach($res as $arr => $val)
			if($arr != 'blad') $res->$arr = $klient[$arr];				
			return $this->common->convertTableTypes('Kontrahenci', $res);
		}
	}
	
	/**
	* Aktualizuje dane klienta dla podanego numeru karty
	* @param Uzytkownik
	* @param string $karta
	* @param KlientDaneDoEdycji
	* @return Klient
	*/
	function KlientAktualizujDaneOsobowe($uzytkownik = null, $karta = '', $dane = null)
	{
		$this->table = 'Kontrahenci';
		$res = new Klient();
		$result = $this->SprawdzLogin($uzytkownik);
		$res->blad = @trim($result->blad);
		if(!empty($res->blad)) return $this->ZwrocBlad($res, $res->blad);
		
		$karta = @strval($karta);
		if(empty($karta)) return $this->ZwrocBlad($res, 'Nie podano numeru karty');
		$dane = (array)$dane;
		$KlientDaneDoEdycji = new KlientDaneDoEdycji();
		foreach($KlientDaneDoEdycji as $pole => $dana)
		{
			if(isset($dane[$pole])) $data[$pole] = strval($dane[$pole]);
		}
		if(!isset($data)) return $this->ZwrocBlad($res, 'Nie podano danych osobowych');
		$kontrahenci = new Kontrahenci();
		$kontrahenci->karta = $karta;
		$kontrahent = $kontrahenci->getCard();
		if(empty($kontrahent)) return $this->ZwrocBlad($res, 'Nie znaleziono podanego numeru karty: '.$karta);
		else
		{
			$kontrahenci->edytujKarta($data, $karta);
			$kontrahent = $kontrahenci->getCard();
			$klient = $kontrahent->toArray();
			$klient['suma'] = $kontrahenci->getSum($klient['id']);
			$klient['adres'] = $kontrahenci->getAdres($klient);
			
			foreach($res as $arr => $val)
			if($arr != 'blad') $res->$arr = $klient[$arr];
			return $this->common->convertTableTypes('Kontrahenci', $res);
		}
	}
	
	/**
	* Dodaje klienta i zwraca jego dane
	* @param Uzytkownik
	* @param KlientKarta
	* @param KlientDaneDoEdycji
	* @return Klient
	*/
	function KlientDodaj($uzytkownik = null, $karta = null, $dane = null)
	{
		$this->table = 'Kontrahenci';
		$res = new Klient();
		$result = $this->SprawdzLogin($uzytkownik);
		$res->blad = @trim($result->blad);
		if(!empty($res->blad)) return $this->ZwrocBlad($res, $res->blad);
		
		$karta = (array)$karta;
		if(!isset($karta['karta'])) return $this->ZwrocBlad($res, 'Nie podano numeru karty');
		if(!isset($karta['kartavip'])) return $this->ZwrocBlad($res, 'Nie podano rodzaju karty');
		//if(!isset($karta['rabatstandard'])) return $this->ZwrocBlad($res, 'Nie podano rabatu');
		
		$data['karta'] = @preg_replace('/[^0-9]/', '', strval($karta['karta']));
		$data['kartavip'] = @abs(intval($karta['kartavip']));
		$data['rabatstandard'] = @abs(floatval($karta['rabatstandard']));
		
		if(strlen($data['karta']) != 5) return $this->ZwrocBlad($res, 'Zły numer karty, wymagane jest 5 cyfr');
		if(@!in_array($data['kartavip'],array(0,2,3))) return $this->ZwrocBlad($res,'Zły rodzaj karty,może być 0,2,3');
		if($data['rabatstandard'] > 100) return $this->ZwrocBlad($res, 'Zły rabat, nie może być większy niż 100.00');
		
		if($data['kartavip'] == 0 && ($data['karta'] < 10000 || $data['karta'] >= 40000))
		return $this->ZwrocBlad($res,'Zły numer karty, dla podanego rodzaju powinien być w przedziale >= 10000 < 40000');
		if($data['kartavip'] == 2 && ($data['karta'] < 40000 || $data['karta'] >= 70000))
		return $this->ZwrocBlad($res,'Zły numer karty, dla podanego rodzaju powinien być w przedziale >= 40000 < 70000');
		if($data['kartavip'] == 3 && ($data['karta'] < 70000 || $data['karta'] >= 99999))
		return $this->ZwrocBlad($res,'Zły numer karty, dla podanego rodzaju powinien być w przedziale >= 70000 < 99999');
		
		$dane = (array)$dane;
		$KlientWymaganeDane = new KlientWymaganeDane();
		$KlientWymaganeDane = @array_keys((array)$KlientWymaganeDane);
		$KlientDaneDoEdycji = new KlientDaneDoEdycji();
		foreach($KlientDaneDoEdycji as $pole => $dana)
		{
			if(isset($dane[$pole])) $data[$pole] = strval($dane[$pole]);
			if(@empty($dane[$pole]) && @in_array($pole, $KlientWymaganeDane))
			return $this->ZwrocBlad($res, 'Nie podano wymaganego pola: '.$pole);
		}
		//return $data;
		$kontrahenci = new Kontrahenci();		
		$kontrahent = $kontrahenci->getFromKarta($data['karta']);
		if(@$kontrahent['kartaaktywna'])return $this->ZwrocBlad($res,'Istnieje już podany numer karty: '.$data['karta']);
		else
		{
			$data['kartaaktywna'] = 1;
			$data['data_rej'] = date('Y-m-d H:i:s');
			if(empty($kontrahent)) $id = $kontrahenci->dodaj($data);
			else $kontrahenci->edytujKarta($data, $data['karta']);
			$kontrahenci->karta = $data['karta'];
			$kontrahent = $kontrahenci->getCard();
			$klient = $kontrahent->toArray();
			$klient['suma'] = $kontrahenci->getSum($klient['id']);
			$klient['adres'] = $kontrahenci->getAdres($klient);
			
			foreach($res as $arr => $val)
			if($arr != 'blad') $res->$arr = $klient[$arr];
			return $this->common->convertTableTypes('Kontrahenci', $res);
		}
	}
	
	/**
	* Dodaje zakup do systemu i zwraca informacje o nim
	* @param Uzytkownik
	* @param string $karta
	* @param float $kwota
	* @param float $kwotaPoRabacie
	* @param string $id_ext
	* @return Zakup
	*/
	function DodajZakup($uzytkownik = null, $karta = '', $kwota = 0, $kwotaPoRabacie = 0, $id_ext = '')
	{
		$this->table = 'Lojalnosczakupy';
		$res = new Zakup();
		$result = $this->SprawdzLogin($uzytkownik);
		$res->blad = @trim($result->blad);
		if(!empty($res->blad)) return $this->ZwrocBlad($res, $res->blad);
		
		$karta = @strval($karta);
		if(empty($karta)) return $this->ZwrocBlad($res, 'Nie podano numeru karty');
		if(floatval($kwota) == 0) return $this->ZwrocBlad($res, 'Nie podano kwoty zakupu przed rabatem');
		if(floatval($kwotaPoRabacie) == 0) return $this->ZwrocBlad($res, 'Nie podano kwoty zakupu po rabacie');
		if(floatval($kwotaPoRabacie) > floatval($kwota))
		return $this->ZwrocBlad($res, 'Kwota zakupu po rabacie nie może być większa niż kwota zakupu przed rabatem');
		$id_ext = @strval($id_ext);
		if(empty($id_ext)) return $this->ZwrocBlad($res, 'Nie podano numeru identyfikacyjnego zakupu');
		
		$kontrahenci = new Kontrahenci();
		$kontrahenci->karta = $karta;
		$kontrahent = $kontrahenci->getCard();
		if(empty($kontrahent)) return $this->ZwrocBlad($res, 'Nie znaleziono podanego numeru karty: '.$karta);
		
		$ob2 = new Query('Lojalnosczakupy');
		$ob2->column = 'id_ext';
		$ob2->value = $id_ext;
		$exist = $ob2->getRowValue();
		if(!empty($exist))
		return $this->ZwrocBlad($res, 'Istnieje już zakup o podanym numerze identyfikacyjnym: '.$id_ext);
		
		$settings = new Lojalnoscustawienia();
		$this->kartyVipRodzaje = $settings->kartyVipRodzaje;
		$this->kartyVipKeys = array_keys($this->kartyVipRodzaje);
		$this->kartyVipNazwy = array_values($this->kartyVipRodzaje);
		
		if(true)
		{
			$ob1 = new Query('Kontrahenci');					
			$ob = new Query('Lojalnoscustawienia');
			$ob->id = 1;
			$ust = $ob->getRow();
			
			if(true)
			{
				$ob2->order = 'id';
				$ob2->column = 'idkl';
				$ob2->value = intval($kontrahent['id']);
				$rows = $ob2->getRowsValue();
				$zakupow = count($rows);
			}
			
			//$karta = "20008";
			//$kwota = 400;
			$kwota = floatval($kwota);
			$kwotaPoRabacie = floatval($kwotaPoRabacie);
			$czyRabat = $kwotaPoRabacie < $kwota;
			//$czyRabat = true;
			//$ust['rabat_st_on'] = true;
			//$ust['rabat_od'] = false;
			//$ust['blokadakwota'] = 5;
			//$ust['opcjailosczak'] = 3;
			//$ust['rabat_rodzaj'] = false;
			
			$rabatInfo = '';
			if($ust['rabat_st_on'])
			{
				if($ust['rabat_od'] && $zakupow > 0)
				{
					$rabatInfo = 'Rabaty są przyznawane tylko od pierwszego zakupu';
					$rabatInfo.= ', na kartę '.$karta.' dokonano już '.$zakupow.' zakupów';
				}				
			}
			else $rabatInfo = 'Przyznawanie rabatów jest aktualnie wyłączone';
			
			if(empty($rabatInfo))
			{
				if(true)
				{
					$rabat = $ust[$this->kartyVipKeys[$kontrahent['kartavip']]];
					if(floatval($kontrahent['rabatstandard']) > 0) $rabat = $kontrahent['rabatstandard'];
					$rabat = floatval($rabat);
					$rabatOtrz = ($rabat * $kwota * 0.01);
				}
				if(true)
				{
					//$rabat = (1 - $kwotaPoRabacie / $kwota) * 100;
					$rabatWyk = max(0, $kwota - $kwotaPoRabacie);
				}
			}
			else
			{
				$rabat = 0;
				$rabatOtrz = 0;
				if($czyRabat)
				return $this->ZwrocBlad($res, $rabatInfo);
			}
			
			if(!$czyRabat)
			{
				$daneklient['konto'] = $kontrahent['konto'] + $rabatOtrz;
				$dane['kwotazaplata'] = $kwota;
				$dane['rabatwyk'] = 0.00;
				$rabatInfo = 'Nie wykorzystano rabatu, rabat przyznany od tego zakupu odłożono na konto';
			}
			else
			{
				if($ust['blokadakwotaon'] && $kontrahent['konto'] < $ust['blokadakwota'])
				return $this->ZwrocBlad($res, 'Rabat niemożliwy do przydzielenia, ponieważ klient nie osiągnął stanu konta min. '.$ust['blokadakwota'].' zł. Aktualny stan konta klienta to: '.$kontrahent['konto'].' zł');
				
				if($ust['opcjailosc'] && ($zakupow + 1 < $ust['opcjailosczak']))
				return $this->ZwrocBlad($res, 'Rabat niemożliwy do przydzielenia, ponieważ klient nie wykonał wymaganego limitu zakupów: '.($ust['opcjailosczak'] - 1).'. Wykonanych zakupów do tej pory: '.$zakupow);
				
				$rabatWykMax = !$ust['rabat_rodzaj'] ? $kontrahent['konto'] + $rabatOtrz : $kontrahent['konto'];
				$rabatWykMax = floatval($rabatWykMax);
				if($rabatWyk > $rabatWykMax)
				{
					if($ust['rabat_rodzaj']) return
					$this->ZwrocBlad($res, 'Rabat w wysokości '.$rabatWyk.' zł niemożliwy do wykorzystania, brak środków na koncie. Aktualny stan konta (bez rabatu przydzielonego od tego zakupu): '.$rabatWykMax.' zł');
					else return
					$this->ZwrocBlad($res, 'Rabat w wysokości '.$rabatWyk.' zł niemożliwy do wykorzystania, brak środków na koncie. Aktualny stan konta (z rabatem przydzielonym od tego zakupu): '.$rabatWykMax.' zł');
				}
				
				$dane['rabatwyk'] = floatval(($rabatWyk > $kwota) ? $kwota : $rabatWyk);
				$daneklient['konto'] = $kontrahent['konto'] + $rabatOtrz - $dane['rabatwyk'];
				$dane['kwotazaplata'] = ($kwota < $rabatWyk) ? 0.00 : ($kwota - $rabatWyk);
				
				if($ust['rabat_rodzaj'])
				$rabatInfo = 'Wykorzystano kwotę na koncie rabatowym, rabat przyznany od tego zakupu odłożono na konto';
				else
				$rabatInfo = 
					'Wykorzystano kwotę na koncie rabatowym i rabat przyznany od tego zakupu, resztę odłożono na konto';
			}
			
			$ob2->update(array('ostatnizakup' => 0), 'idkl = "'.intval($kontrahent['id']).'"');
	
			$dane['typdokumentu'] = 'paragon';
			$dane['numerdokumentu'] = '';//$dokument;
			$dane['ostatnizakup'] = 1;
			$dane['idkl'] = intval($kontrahent['id']);
			$data = date('Y-m-d H:i:s');
			$dane['data'] = $data;
			$dataex = explode(' ', $data);
			$datac = $dataex[0];
			$dataex  = explode('-', $datac);
			$dane['rok'] = $dataex[0];
			$dane['miesiac'] = $dataex[1];
			$dane['dzien'] = $dataex[2];
			$dane['autor'] = 'YourSoft';
			$dane['autorid'] = 0;
			$dane['kwota'] = floatval($kwota);
			$dane['dziennazwa'] = date("l", strtotime($datac)); 
			$dane['rabatotrz'] = $rabatOtrz;
			$dane['polecajacy'] = 0;
			$dane['rabatpolecajacy'] = 0;
			$dane['id_ext'] = $id_ext;
			
			$idzam = $ob2->_save($dane);
			
			if(isset($daneklient)) $ob1->update($daneklient, 'id = "'.intval($kontrahent['id']).'"');
			
			$dane['karta'] = $karta;
			$dane['rabat'] = $rabat;
			$dane['rabatinfo'] = $rabatInfo;
			$dane['konto'] = $daneklient['konto'];
			
			foreach($res as $arr => $val)
			if($arr != 'blad') $res->$arr = $dane[$arr];
			return $res;
		}
	}
}
?>