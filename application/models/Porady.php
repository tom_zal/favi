<?php
class Porady extends Zend_Db_Table
{
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
    function wypiszGlowna($lang = "pl")
	{
        $where = 'glowna = "on" AND lang = "'.$lang.'"';
        $order = 'id DESC';
        $result = $this->fetchAll($where, $order);
        return $result;
    }
    function wypiszDoNewslettera($lang = "pl")
	{
        $where = 'status = 0 AND lang = "'.$lang.'"';
        $order = 'id DESC';
        $result = $this->fetchAll($where, $order)->toArray();
        return $result;
    }
    function wypisz()
	{
        $result = $this->fetchAll();
        return $result;
    }

    function dodaj($dane, $lang = "pl")
	{
        $dane['lang'] = $lang;
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }

    function edytuj($dane, $id)
	{
        $where = 'id = '.$id;
        $this->update($dane, $where);
    }

    function pojedyncza($id)
	{
        $where = 'id = '.$id;
        $print = $this->fetchRow($where);
        return $print;
    }
    function wypiszAktualnosciAdmin($lang = "pl", $typ = 1)
	{
        $where = 'typ = '.$typ.' AND lang = "'.$lang.'"';
        $result = $this->fetchAll($where);
        return $result;
    }
    function wypiszAktualnosci($lang = "pl", $typ = 1)
	{
        $where = 'typ = '.$typ.' AND lang = "'.$lang.'"';
        $result = $this->fetchAll($where)->toArray();
        return $result;
    }
    function wypiszAktualnosciGlowna($lang = "pl", $typ = 1)
	{
        $where = 'typ = '.$typ.' AND lang = "'.$lang.'" AND glowna = "on"';
        $order = 'data DESC';
        $result = $this->fetchAll($where, $order)->toArray();
        return $result;
    }
}
?>