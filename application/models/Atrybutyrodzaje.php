<?php
class Atrybutyrodzaje extends Zend_Db_Table
{
	public $id, $_cache, $_cachename;	
	public $link, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

	function _save($data)
	{
		$this->insert($data);
		return $this->getAdapter()->lastInsertId();
	}
	function _update($data)
	{
		$this->update($data, 'id = '.$this->id);
	}
	function _delete()
	{
		$this->delete('id = '.$this->id);
	}
	function getRow()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function getRows()
	{
		$result = $this->fetchAll('1', 'nazwa asc');
		return $result;
	}
}
?>