<?php
class Produktypowiazania extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
	function getPowiazaniaForExport($ids, $rodzaj = 'produkt')
	{
		if(!$this->common->isArray($ids, true)) return null;
        $select = $this->db->select()->from(array('pp' => 'Produktypowiazania'), array('id_prod', 
			new Zend_Db_Expr('GROUP_CONCAT(DISTINCT powiazany ORDER BY powiazany ASC SEPARATOR "|") as powiaz')))
			->where('id_prod in('.implode(',',$ids).') and pp.rodzaj = "'.$rodzaj.'"')->group('id_prod')->order(array('id_prod asc'));
		//echo $select;die();
		$result = $this->db->fetchAll($select);
        for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i]['id_prod']] = $result[$i]['powiaz'];
		}
		return @$results;
    }
		
	function dodaj($dane)
	{
		//$dane['rodzaj'] = 'produkt';
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujProd($dane)
	{
		$where = 'id_prod = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujPowiaz($dane)
	{
		$where = 'powiazany = '.$this->id;
		$this->update($dane, $where);
	}
	function wypiszAll()
	{
		$result = $this->fetchAll();
		return $result;
	}
	function wypiszID()
	{
		$sql = $this->select()->where('id = '.$this->id);
		$result = $this->fetchRow($sql);
		return $result;
	}
	function wypiszProd($id, $rodzaj = 'produkt')
	{
		$sql = $this->select()->where('id_prod = '.$id.' and rodzaj = "'.$rodzaj.'"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszPowiaz($id, $rodzaj = 'produkt')
	{
		$sql = $this->select()->where('powiazany = '.$id.' and rodzaj = "'.$rodzaj.'"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszGratisy($ids = null, $implode = false)
	{
		if(!$this->common->isArray($ids, true)) return null;
		$select = $this->db->select()->from(array('p' => 'Partnerzy'), 
			$implode ? 'GROUP_CONCAT(distinct p.id SEPARATOR ",") as gratisy' : array('nazwa', 'logo', 'opis', 'link'))
			->join(array('pp' => 'Produktypowiazania'), 'p.id = pp.powiazany', $implode ? '' : array('p.id'))
			->where('pp.id_prod in ('.implode(',',$ids).') and pp.rodzaj = "gratisy" and p.typ = "gratisy"');
		if(!$implode) $select->group('p.id')->order('nazwa asc')->limit(9999);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszProdukt($id = 0, $rodzaj = 'produkt', $ids = null)
	{
		$where = 'pp.rodzaj = "'.$rodzaj.'"';
		if($this->common->isArray($ids, true)) $where .= ' and p.id in ('.$ids.')';
		else if($id >= 0) $where .= ' and pp.id_prod = '.$id;
		
		if($rodzaj == 'gratisy')
		$select = $this->db->select()->from(array('p' => 'Partnerzy'), array('nazwa', 'logo', 'opis', 'link'))
			->join(array('pp' => 'Produktypowiazania'), 'p.id = pp.powiazany', array('id', 'powiazany'))
			->where($where.' and p.typ = "gratisy"')->group('p.id')->order('nazwa asc')->limit(9999);
		else
		$select = $this->db->select()->from(array('p' => 'Produkty'), array('p.nazwa'))
			->join(array('pp' => 'Produktypowiazania'), 'p.id = pp.powiazany', array('id', 'powiazany'))
			->where($where)->group('p.id')->order('nazwa asc')->limit(9999);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszPowiazania($id = 0, $rodzaj = 'produkt')
	{
		$where = 'pp.rodzaj = "'.$rodzaj.'" and pp.powiazany = '.$id;
		$select = $this->db->select()->from(array('p' => 'Produkty'), array('p.nazwa'))
			->join(array('pp' => 'Produktypowiazania'), 'p.id = pp.id_prod', array('id', 'id_prod'))
			->where($where)->group('p.id')->order('nazwa asc')->limit(9999);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszProdukty($id, $ile = 3, $rodzaj = 'produkt')
	{
		$where = 'p.id > 0 and pp.rodzaj = "'.$rodzaj.'"';
		$where.= ' and (pp.id_prod = "'.$id.'")';// or pp.powiazany = "'.$id.'")';
		$whereGal = 'p.id = g.wlasciciel and g.typ = "oferta" and g.glowne = "T"';
		$whereGal.= ' and g.lang = "'.$this->lang.'" and g.wyswietl = "1"';
		$select = $this->db->select()->from(array('p' => 'Produkty'), array('p.*'))
			->joinleft(array('pp' => 'Produktypowiazania'), 'p.id = pp.powiazany', array(''))
			->joinleft(array('g' => 'Galeria'), $whereGal, array('img', 'img_opis' => 'nazwa'));
		//$select->joinleft(array('atp' => 'Atrybutypowiazania'), 'p.id = atp.id_og and atp.ceny and atp.cenyTryb', array('id_at'));
		//$select->joinleft(array('atc' => 'Atrybutyceny'), 'p.id = atc.id_prod and atc.cena_netto > 0', array('id_ext'));		
		$select->where($where)->group('p.id')->order('p.nazwa asc')->limit($ile);
		//echo $select;die();
		$result = $this->db->fetchAll($select);
		if(@intval($result[0]['id']) == 0) return null;
		return $result;
	}
	function usun()
	{
		$result = $this->delete('id = '.$this->id);
	}
	function usunProdukt($id, $rodzaj = 'produkt')
	{
		$result = $this->delete('id_prod = '.$id.' and rodzaj = "'.$rodzaj.'"');	
	}
	function usunPowiaz($id, $rodzaj = 'produkt')
	{
		$result = $this->delete('powiazany = '.$id.' and rodzaj = "'.$rodzaj.'"');	
	}
	function usunProduktyIds($ids, $rodzaj = 'produkt')
	{
		$result = $this->delete('id_prod in ('.$ids.') and rodzaj = "'.$rodzaj.'"');
	}
}
?>