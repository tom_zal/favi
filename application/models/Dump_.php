<?php
function dump($var, $zenddump = true)
{
	echo '<pre>';
	$zenddump ? Zend_Debug::dump($var) : print_r($var);
	echo '</pre>';
}
?>