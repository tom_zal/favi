<?php
class Zamowieniauwagi extends Zend_Db_Table
{
	public $link, $id;
	public function __construct($name = 'Zamowieniauwagi')
	{
		parent::__construct();
		$this->_name = $name;
    }
    function dodaj($dane)
    {
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }
    function edytuj($dane)
    {
        $where = 'id = '.$this->id;
        $this->update($dane, $where);
    }
    function wypisz()
    {
        $sql = $this->select()->order('id');
        $result = $this->fetchAll($sql);
        return $result;
    }

    function wypiszZamow($zamow = 0)
    {
        $sql = $this->select()->where('id_zam = "'.$zamow.'"')->order('data desc');
        $result = $this->fetchAll($sql);
        return $result;
    }		

    function usun()
    {
        $result = $this->delete('id = '.$this->id);			
    }
    function wypiszPojedynczego()
    {
        $result = $this->fetchRow('id = '.$this->id);
        return $result;
    }
}
?>