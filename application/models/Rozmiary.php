<?php
class Rozmiary extends Zend_Db_Table
{
	public $link, $id, $typ, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodajKolory($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytujKolory($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function wypiszKolory($typ = 1)
	{
		$sql = $this->select()->where($typ>0?'typ='.$typ:1)->order(array('nazwa','wkladka'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszJeden($nazwa, $typ = 1)
	{
		$sql = $this->select()->where('nazwa = "'.$nazwa.'" and typ = '.$typ)->order('wkladka asc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszKoloryDistinct($nazwa, $order = 'nazwa', $typ = 1)
	{
		if($nazwa == null) 
		$select = $this->select()->distinct()->from(array('p'=>'Rozmiary'),$order)
			->join(array('r' => 'Rozmiarowka'),'r.id = p.typ',array(''))->order('p.nazwa');
		else $select = $this->select()->where('nazwa = "'.$nazwa.'" and '.($typ>0?'typ='.$typ:1))->order($order);
		$result = $this->fetchAll($select);
		return $result;
	}		
	function wypiszKoloryDistinctKateg($id, $order = 'nazwa', $grupa = 0)
	{
		$where = 'p.widoczny = 1 and kp.id_kat = '.$id;
		if($grupa > 0)
		$where.= ' and (p.grupa = '.$grupa.' or p.grupa = -1)';
		$select = $this->db->select()->distinct()
			->from(array('kp' => 'Katprod'),array(''))
			->join(array('p' => 'Produkty'),'p.id = kp.id_prod',array(''))
			->join(array('rp' => 'Rozmiarproduktu'),'p.id = rp.id_produktu',array(''))
			->join(array('r' => 'Rozmiary'),'r.id = rp.id_koloru',array($order))
			->where($where)->group('r.id')->order($order);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}

	function usunKolory()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	
	function wypiszKolor()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	
	function znajdzKolor($kolor, $typ = 1)
	{
		$result = $this->fetchAll('nazwa LIKE "'.$kolor.'" and typ = '.$typ);
		return $result;
	}
	
	function szukajRozmiar($nazwa, $wkladka)
	{
		$select = $this->select()->where('nazwa = "'.$nazwa.'" and wkladka = "'.$wkladka.'"');
		$result = $this->fetchAll($select);
		//echo $select;
		return $result;
	}
}
?>