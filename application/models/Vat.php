<?php
class Vat extends Zend_Db_Table
{
	public $link, $id;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
	function dodajVat($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytujVat($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function wypiszVaty()
	{
		$sql = $this->select()->order('CAST(`nazwa` AS SIGNED) desc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function usunVat($id)
	{
		$result = $this->delete('id = '.$id);
	}
	function wypiszVat()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
			
	function znajdzVat($vat)
	{
		$result = $this->fetchAll('nazwa LIKE "'.$vat.'"');
		return $result;
	}
}
?>