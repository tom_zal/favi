<?php

//    function dump($dane) {
//        echo '<pre>';
//        print_r($dane);
//        echo '</pre>';
//    }
const MY_APPLICATION_ENV = 'DEVELOPMENT';
dump_on();

function dump_on() {
    $GLOBALS['_DUMP_OFF_'] = false;
}

function dump_off() {
    $GLOBALS['_DUMP_OFF_'] = true;
}

/**
 * Prints named information in pre tags
 * @param type $varible
 * @param type $name
 */
function dump($varible, $name = null) {
    
    if ((boolean) $name) {
        $name = '<b>' . $name . '</b> = ';
    }
    
    if (MY_APPLICATION_ENV == 'DEVELOPMENT' && !$GLOBALS['_DUMP_OFF_']) {
        
        echo '<pre>' . $name . print_r($varible, true) . '</pre>';
    }
}

/**
 * Prints named information in raw mode
 * @param type $varible
 * @param type $name
 */
function dumpt($varible, $name = null) {

    if ((boolean) $name) {
        $name = $name . ' = ';
    }

    if (MY_APPLICATION_ENV == 'DEVELOPMENT' && !$GLOBALS['_DUMP_OFF_']) {
        echo $name . print_r($varible, true) . "\n";
    }
}

/**
 * Prints named information in pre tags with htmlspecialchars function
 * @param type $varible
 * @param type $name
 */
function dumph($varible, $name = null) {

    if ((boolean) $name) {
        $name = '<b>' . $name . '</b> = ';
    }

    if (MY_APPLICATION_ENV == 'DEVELOPMENT' && !$GLOBALS['_DUMP_OFF_']) {
        echo '<pre>' . $name . htmlspecialchars(print_r($varible, true), ENT_QUOTES, 'UTF-8') . '</pre>';
    }
}

/**
 * Prints named var_dump in pre tags
 * @param mixed $varible
 * @param string $name
 */
function dumpv($varible, $name = 'q') {

    ob_start();
    var_dump($varible);
    $out = ob_get_clean();
    $out = preg_replace('/=>\s*/', ' =&gt; ', $out);
    $out = str_replace(array("<pre class='xdebug-var-dump' dir='ltr'>", '</pre>'), array('', ''), $out);

    if ((boolean) $name) {
        $name = '<b>' . $name . '</b> = ';
    }

    if (MY_APPLICATION_ENV == 'DEVELOPMENT' && !$GLOBALS['_DUMP_OFF_']) {
        echo '<pre>' . $name . $out . '</pre>';
    }
}

/**
 * Prints named dumpto
 * @param type $varible
 * @param type $name
 */
function dumpf($varible, $name = null) {

    global $config;

    if ((boolean) $name) {
        $name = $name . ' = ';
    }
    if (!$GLOBALS['_DUMP_OFF_']) {
        file_put_contents(
                $config['dump_file'], $name . print_r($varible, true) . PHP_EOL, FILE_APPEND
        );
    }
}

/**
 * Prints dBug from varible
 * @param mixed $varible
 * @param string $name
 */
function dumpd($varible, $bCollapsed = false, $forceType = "") {

    ob_start();
    new dBug($varible, $forceType, $bCollapsed);
    $out = ob_get_clean();

    if (MY_APPLICATION_ENV == 'DEVELOPMENT' && !$GLOBALS['_DUMP_OFF_']) {
        echo $out;
    }
}
function sub_string($str, $length = 20, $enc = 'utf-8') {
	$str = strip_tags($str);
	$out = null;
	if (mb_strlen($str, $enc) > $length) {
		$limit = mb_strrpos(mb_substr($str, 0, ($length - 3), $enc), ' ', 0, $enc);
		if (!$limit) {
			$limit = $length - 3;
		}
		if ($limit < 0)
			$limit = 0;
		$out = mb_substr($str, 0, $limit, $enc) . '...';
	} else {
		$out = $str;
	}
	return $out;
}

?>