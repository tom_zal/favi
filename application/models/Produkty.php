<?php

class Produkty extends Zend_Db_Table {

    public $link, $id, $obConfig, $db;

    public function __construct($module = 'admin') {
        parent::__construct();
        $this->module = $module;
        $this->common = new Common(false, $module);
        $this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
        $this->lang = $this->common->getJezyk($module);
        $this->ustawienia = $this->common->getUstawienia();
    }

    function dodajProdukty($dane, $lang = 'pl') {
        if (@empty($dane['lang']))
            $dane['lang'] = $this->lang;
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }

    function edytujProdukty($dane) {
        $where = 'id = ' . $this->id;
        //echo $where.' '.$this->id;
        return $this->update($dane, $where);
    }

    function edytujProduktyIds($dane, $ids, $msg = false) {
        if (empty($ids))
            return;
        $where = 'id in (' . $ids . ')';
        //echo $where.' '.$this->id;
        $this->common->cacheRemove(null, 'Produkty');
        $ok = $this->update($dane, $where);
        if ($ok) {
            $message = new Zend_Session_Namespace('message');
            if ($msg)
                $message->msg = '';
            $ile = @count(explode(",", $ids));
            if (count($dane) > 0)
                foreach ($dane as $co => $val) {
                    Events::log($_SESSION['loguser'], 'hurtzmienprodukty', "", 'lista', 'oferta', $co, $ids, $val);
                    if ($msg) {
                        $value = "";
                        $nazwa = str_replace("_", " ", $co);
                        if (isset($this->common->polaOnOff[$co]))
                            $value = $val ? "TAK" : "NIE";
                        if (isset($this->common->oznaczenia[$co]))
                            $value = $val ? "TAK" : "NIE";
                        if (isset($this->common->polaOpisy[$co]))
                            $nazwa = $this->common->polaOpisy[$co];
                        if (isset($this->common->oznaczenia[$co]))
                            $nazwa = $this->common->oznaczenia[$co]['nazwa'];
                        if (!empty($value))
                            $value = "na " . $value;
                        $message->msg .= '<div class="k_ok">Ustawiono "' . $nazwa . '" dla ' . $ile . ' produktów ' . $value . '</div>';
                    }
                }
        }
        return $ok;
    }

    function edytujProduktyAll($dane, $where = '1') {
        //$where = '1';
        $this->update($dane, $where);
    }

    function edytujProduktyImport($dane) {
        for ($i = 0; $i < count($dane); $i++) {
            $where = 'id = ' . $dane[$i]['id'];
            $this->update($dane[$i], $where);
        }
    }

    function wypiszProdukty($lang = 'pl') {
        $where = 'lang = "' . $lang . '"';
        $result = $this->fetchAll();
        return $result;
    }

    function wypiszProduktyAll($kategID = 0) {
        $where = 'p.nazwa <> "" and p.lang = "' . $this->lang . '" and p.zmiana is not NULL and p.widoczny = "1"';
        $sql = $this->db->select()->from(array('p' => 'Produkty'), array('p.id', 'p.nazwa', 'p.link'));
        if ($kategID > 0) {
            $where .= ' and k.id_kat = ' . $kategID;
            $sql->joinleft(array('k' => 'Katprod'), 'p.id = k.id_prod', array(''));
        }
        if (true || $this->obConfig->podstronyGaleria) {
            $whereGal = 'p.id = g.wlasciciel and g.typ = "oferta" and g.glowne = "T"';
            $whereGal.= ' and g.lang = "' . $this->lang . '" and g.wyswietl = "1"';
            $sql->joinleft(array('g' => 'Galeria'), $whereGal, array('img', 'img_opis' => 'nazwa'));
        }
        $sql->where($where)->group('p.id')->order('p.nazwa asc')->limit(999);
        //echo $sql;die();
        $result = $this->db->fetchAll($sql);
        //var_dump($result);die();
        return $result;
    }

    function wypiszProduktyEksport($sort = 'p.nazwa asc', $caching = true) {
        $where = 'p.nazwa <> ""';
        $sort = 'p.' . $sort . ' asc';
        if ($sort == 'producent')
            $sort = array('pp.nazwa asc', 'p.nazwa asc');
        if (true) {
            $session = new Zend_Session_Namespace();
            $cache = $session->cache;
            $cachename = 'eksport_produkty';
            if ($cache)
                $result = $cache->load('cache_' . $cachename);
            else
                $result = false;
            if ($result === false || !$caching) {
                $select = $this->db->select()->from(array('p' => 'Produkty'), array('*'))
                                ->joinleft(array('pp' => 'Producent'), 'pp.id = p.producent', array('nazwa as producent'))
                                ->where($where)->order($sort);
                //echo $select;
                $result = $select->query()->fetchAll();
                //$result = $this->db->fetchAll($select);
                if ($cache)
                    $cache->save($result, 'cache_' . $cachename);
            }
            return $result;
        }
        $result = $this->fetchAll($where, 'nazwa asc');
        return $result;
    }

    function wypiszProduktyImport($sort = 'nazwa', $caching = true) {
        $where = 'p.nazwa <> ""';
        if (true) {
            $session = new Zend_Session_Namespace();
            $cache = $session->cache;
            $cachename = 'import_produkty';
            if ($cache)
                $result = $cache->load('cache_' . $cachename);
            else
                $result = false;
            if ($result === false || !$caching) {
                $select = $this->db->select()
                        ->from(array('p' => 'Produkty'), array('*', $sort))
                        ->where($where); //->order('nazwa asc');
                //echo $select;
                $result = $select->query()->fetchAll();
                //$result = $this->db->fetchAll($select);
                if (count($result) > 0)
                    foreach ($result as $id => $result) {
                        $results[trim($result[$sort])] = $result;
                        //unset($result[$id]);
                    }
                $result = @$results;
                if ($cache)
                    $cache->save($result, 'cache_' . $cachename);
            }
            return $result;
        }
        $result = $this->fetchAll($where, 'nazwa asc');
        return $result;
    }

    function wypiszProduktyDoNewslettera($typ, $lang = 'pl') {
        $where = $typ . ' = 1 AND p.lang = "' . $lang . '" AND p.widoczny AND p.status = 0';
        $select = $this->db->select()->from(array('p' => 'Produkty'), array('*'));
        if (true) {
            $whereGal = 'p.id = g.wlasciciel and g.typ = "oferta" and g.glowne = "T"';
            $whereGal.= ' and g.lang = "' . $this->lang . '" and g.wyswietl = "1"';
            $select->joinleft(array('g' => 'Galeria'), $whereGal, array('img' => 'ifnull(g.img, "brak.jpg")', 'img_opis' => 'g.nazwa'));
        }
        $select->where($where);
        $result = $this->db->fetchAll($select);
        return $result;
    }

    function Odwiedziny($dane) {
        $where = 'id = ' . $this->id;
        $this->update($dane, $where);
    }

    function OdwiedzinyIDS($ids = null) {
        if (!$this->common->isArray($ids, true))
            return null;
        $where = 'id in (' . implode(',', $ids) . ')';
        return $this->update(array('odwiedziny' => 'odwiedziny + 1'), $where);
    }

    function raportOdwiedzin() {
        $sql = $this->select()->order('odwiedziny DESC')->limit(30, 0);
        $result = $this->fetchAll($sql)->toArray();
        return $result;
    }

    function usunProdukty($teraz = true, $ids = null) {
        $tryb = 'one';
        if ($this->common->isArray($ids, true)) {
            $ids = implode(',', $ids);
            if (!empty($ids))
                $tryb = 'all';
        }
        if ($tryb == 'one' && intval($this->id) == 0)
            return null;

        if ($teraz) {
            $routers = new Routers();
            if ($tryb == 'one')
                $routers->usunProdukt($this->id);
            if ($tryb == 'all')
                $routers->usunProduktyIds($ids);
            if (true) {
                $kategoria = new Katprod($this->id);
                if ($tryb == 'one')
                    $kategoria->deleteProdukt();
                if ($tryb == 'all')
                    $kategoria->deleteProduktyIds($ids);
            }
            if (true) {
                $galeria = new Galeria();
                if ($tryb == 'one')
                    $galeria->usunGalerie($this->id);
                if ($tryb == 'all')
                    $galeria->usunGalerieIds($ids);
            }
            if ($this->obConfig->tryb == 'rozmiary') {
                $rozmiar = new Rozmiarproduktu();
                if ($tryb == 'one')
                    $rozmiar->usunProdukt($this->id);
                if ($tryb == 'all')
                    $rozmiar->usunProduktyIds($ids);
            }
            if ($this->obConfig->kolory) {
                $kolor = new Kolorproduktu();
                if ($tryb == 'one')
                    $kolor->usunProdukt($this->id);
                if ($tryb == 'all')
                    $kolor->usunProduktyIds($ids);
            }
            if ($this->obConfig->atrybuty) {
                $atrybuty = new Atrybutypowiazania();
                if ($tryb == 'one')
                    $atrybuty->usunProdukt($this->id);
                if ($tryb == 'all')
                    $atrybuty->usunProduktyIds($ids);
            }
            if ($this->obConfig->atrybutyCenyRozne) {
                $atrybuty = new Atrybutyceny();
                if ($tryb == 'one')
                    $atrybuty->usunProdukt($this->id);
                if ($tryb == 'all')
                    $atrybuty->usunProduktyIds($ids);
            }
            if ($this->obConfig->produktyPowiazania) {
                $powiazania = new Produktypowiazania();
                if ($tryb == 'one')
                    $powiazania->usunProdukt($this->id);
                if ($tryb == 'all')
                    $powiazania->usunProduktyIds($ids);
            }
            if ($this->obConfig->platnosciProdukt) {
                $platnosci = new PlatnosciProdukty();
                if ($tryb == 'one')
                    $platnosci->usunProdukt($this->id);
                if ($tryb == 'all')
                    $platnosci->usunProduktyIds($ids);
            }
            if ($this->obConfig->opinieProdukt) {
                $opinie = new Opinie();
                if ($tryb == 'one')
                    $opinie->usunProdukt($this->id);
                if ($tryb == 'all')
                    $opinie->usunProduktyIds($ids);
            }
            if ($tryb == 'one')
                return $this->delete('id = ' . $this->id);
            if ($tryb == 'all')
                return $this->delete('id in (' . $ids . ')');
        }
        else {
            if ($tryb == 'one')
                return $this->edytujProdukty(array('zmiana' => NULL));
            if ($tryb == 'all')
                return $this->edytujProduktyIds(array('zmiana' => NULL), $ids);
        }
    }

    function wypiszPojedyncza() {
        $result = $this->fetchRow('id = ' . $this->id);
        return $result;
    }

    function wypiszPojedynczaAll() {
        $select = $this->db->select()->from(array('p' => 'Produkty'), array('*'));
        if (true) {
            $whereGal = 'p.id = g.wlasciciel and g.typ = "oferta" and g.glowne = "T"';
            $whereGal.= ' and g.lang = "' . $this->lang . '" and g.wyswietl = "1"';
            $select->joinleft(array('g' => 'Galeria'), $whereGal, array('img' => 'ifnull(g.img, "brak.jpg")', 'img_opis' => 'g.nazwa'));
        }
        $select->where('p.id = ' . $this->id);
        $result = $this->db->fetchRow($select);
        return $result;
    }

    function wypiszTest($id = 0) {
        $result = $this->fetchRow('lang = "' . $this->lang . '" and nazwa = "" and widoczny = 0 and status = ' . ( - intval($id)));
        return $result;
    }

    function wypiszProduktyPodzielone($od = 0, $po = 30, $sort = 'nazwa', $order = 'asc', $lang = 'pl') {
        $where = 'lang = "' . $lang . '"';
        $sql = $this->select()->where($where)->order($sort . ' ' . $order)->limit($po, $od);
        $result = $this->fetchAll($sql);
        return $result;
    }

    function iloscProduktow($lang = 'pl') {
        $where = 'widoczny = 1 AND lang = "' . $lang . '"';
        $result = $this->fetchAll($where);
        return count($result);
    }

    function iloscProduktowAll($widoczny = false) {
        $where = $widoczny ? 'widoczny' : 1;
        $select = $this->db->select()->from(array('p' => 'Produkty'), array('count(*)'))->where($where);
        //echo $select;
        $result = $this->db->fetchAll($select);
        return @intval($result[0]['count(*)']);
    }

    function wypiszProduktyPodzieloneStrona($ile, $strona = 0, $ilosc = 24, $sortowanie = null, $szukanie = null, $spec = null, $ids = null, $test = false, $links = false) {
//        foreach ($szukanie as $k => $v){
//            echo $k.' ' . $v . ' - ' . '<br/>';
//        }
//        dump($szukanie);die;
        $od = $strona * $ilosc;
        $limit = $ilosc;
        if ($ile) {
            $od = 0;
            $limit = 9999;
        }
        $where = 'p.nazwa <> "" and p.lang = "' . $this->lang . '" and p.zmiana is not NULL';
        if ($this->common->isArray($ids, true))
            $where .= ' and p.id in (' . implode(',', $ids) . ')';
        $select = 'p.id, p.link';
        $having = '1';

        if ($sortowanie == null) {
            $sortowanie = (object) array();
            $sortowanie->sort = 'p.nazwa';
            $sortowanie->order = 'asc';
            $sortowanie->specjalne = '';
        }

        $allegroDoKonca = 'time(if(a.wystawiono is not NULL, TIMEDIFF(DATE_ADD(a.wystawiono, INTERVAL a.na_ile DAY), NOW()), ' . ($sortowanie->order == 'asc' ? '"9999:00:00"' : '"0000:00:00"') . '))';

        $producent = $this->obConfig->producent;
        $prodGrupy = $this->obConfig->produktyGrupy;
        $rozmiar = (isset($sortowanie->sort) && ($sortowanie->sort == 'rozmiar') || $sortowanie->sort == 'wkladka');
        $rozmiar = $rozmiar || (isset($szukanie->rozmiar) && $szukanie->rozmiar != 'all');
        $rozmiar = $rozmiar || (isset($szukanie->wkladka) && $szukanie->wkladka != 'all');
        $rozmiar = $rozmiar || ($this->obConfig->trybCena == 'rozmiary');
        $kolor = (isset($sortowanie->sort) && $sortowanie->sort == 'kolor');
        $kolor = $kolor || (isset($szukanie->kolor) && $szukanie->kolor != -1);
        $dostepnosc = (isset($sortowanie->sort) && $sortowanie->sort == 'dostepnosc');
        $rozmiar = $rozmiar || ($dostepnosc && $this->obConfig->tryb == 'rozmiary');
        $kolor = $kolor && !$dostepnosc;
        $allegro = (isset($sortowanie->sort) && $sortowanie->sort == 'allegro_do_konca');
        $allegro = $allegro || (isset($szukanie->aukcje) && $szukanie->aukcje == 'all');
        $odwiedziny = (isset($szukanie->odwiedziny) && $szukanie->odwiedziny != '');
        $odwiedziny = $odwiedziny || (isset($szukanie->dataOdwiedzinOd) && $szukanie->dataOdwiedzinOd != '');
        $sprzedaz = (isset($szukanie->sprzedaz) && $szukanie->sprzedaz != '');
        $sprzedaz = $sprzedaz || (isset($szukanie->dataSprzedazyOd) && $szukanie->dataSprzedazyOd != '');
        $data = (isset($szukanie->dataOd) && $szukanie->dataOd != '');
        $atrybuty = $this->obConfig->atrybuty && ($this->obConfig->atrybutyCeny || (@intval($sortowanie->sort) > 0));

        $atrybCeny = $this->obConfig->atrybuty && ($this->obConfig->atrybutyCenyRozne);
        if ($odwiedziny || $sprzedaz) {
            $atrybuty = false;
            $atrybCeny2 = false;
        }
        $atrybut = ' = -1';
        $rabaty = $this->obConfig->rabaty && !$this->obConfig->rabatyKoszyk;
        $hurt = @$szukanie->typ_klienta || $this->obConfig->tylkoHurt;
        $netto = isset($sortowanie->sort) && $sortowanie->sort == 'cena_netto';
        $netto = $netto || ($hurt && $this->obConfig->cenyNettoDomyslneHurt);
        $netto = $netto || (!$hurt && $this->obConfig->cenyNettoDomyslneDetal);

        if ($rabaty) {
            $rabatKat = '(SELECT rabat FROM Kategorie WHERE id = (select max(id_kat) from Katprod kpr join Kategorie kr on kpr.id_kat = kr.id where kpr.id_prod = p.id and kr.rabat > 0))';
            $user = new Zend_Session_Namespace('kontrahent');
            $userID = @intval($user->id);
            $rabatUser = $userID > 0 ? @floatval($user->dane['rabat']) : 0;
            $rabatUserKat = '(SELECT wartosc_rab FROM Rabaty WHERE kategoria_rab = (select max(id_kat) from Katprod kpr join Rabaty r on kpr.id_kat = r.kategoria_rab where kpr.id_prod = p.id and r.wartosc_rab > 0 and r.id_user = ' . $userID . '))';
            $rabat = 'if(p.promocja, 0, GREATEST(0,';
            if ($this->obConfig->rabatyProdukty)
                $rabat .= 'ifnull(p.rabat, 0),';
            if ($this->obConfig->rabatyProducenci)
                $rabat .= 'ifnull(pp.rabat, 0),';
            if ($this->obConfig->rabatyKategorie)
                $rabat .= 'ifnull(' . $rabatKat . ', 0),';
            if ($userID > 0) {
                if ($this->obConfig->rabatyKlienci)
                    $rabat .= 'ifnull(' . $rabatUser . ', 0),';
                if ($this->obConfig->rabatyKlienciKategorie)
                    $rabat .= 'ifnull(' . $rabatUserKat . ', 0),';
            }
            $rabat .= '0))';
        }

        if ($rozmiar) {
            if ($hurt) {
                $cenaMin = 'if(p.trybCena="rozmiary",';
                $cenaMin.= 'if(p.promocja,';
                $cenaMin.= 'min(if(r.typ>0,rp.cena_promocji_b_hurt,null)),';
                $cenaMin.= 'min(if(r.typ>0,rp.cena_brutto_hurt,null))';
                $cenaMin.= '),';
                $cenaMin.= 'if(p.promocja,p.cena_promocji_b_hurt,p.cena_brutto_hurt)';
                $cenaMin.= ')';
            } else {
                $cenaMin = 'if(p.trybCena="rozmiary",';
                $cenaMin.= 'if(p.promocja,';
                $cenaMin.= 'min(if(r.typ>0,rp.cena_promocji_b,null)),';
                $cenaMin.= 'min(if(r.typ>0,rp.cena_brutto,null))';
                $cenaMin.= '),';
                $cenaMin.= 'if(p.promocja,p.cena_promocji_b,p.cena_brutto)';
                $cenaMin.= ')';
            }
            $cenaSearchClause = 'where';
        } else {
            if ($atrybuty && $this->obConfig->atrybutyCeny) {
                $cenaPromocja = $hurt ? 'cena_promocji_b_hurt' : 'cena_promocji_b';
                $cenaBezPromo = $hurt ? 'cena_brutto_hurt' : 'cena_brutto';
                $atrTyp = '(atp.typatrybut = "lista" or atp.typatrybut = "multi")';
                $promo = 'p.promocja'; // and p.promocjaDo >= "'.date('Y-m-d').'"';
                $cena = 'if(' . $promo . ', p.' . $cenaPromocja . ', p.' . $cenaBezPromo . ')';
                $if11 = $atrTyp . ' and atp.ceny = "1" and atp.cenyTryb = "1"';
                $atr1 = 'if(' . $promo . ',
				min(distinct if(' . $if11 . ',atp.' . $cenaPromocja . ',null)),
				min(distinct if(' . $if11 . ',atp.' . $cenaBezPromo . ',null)))';
                $atr1if = 'sum(if(' . $if11 . ',1,0))>0';
                $if10 = $atrTyp . ' and atp.ceny = "1" and atp.cenyTryb = "0"';
                $atr0 = 'if(' . $promo . ',
				sum(distinct if(' . $if10 . ',atp.' . $cenaPromocja . ',0)),
				sum(distinct if(' . $if10 . ',atp.' . $cenaBezPromo . ',0)))';
                $atr0if = 'sum(if(' . $if10 . ',1,0))>0';
                $cenaMin = 'if(' . $atr1if . ',' . $atr1 . ',if(' . $atr0if . ',' . $cena . '+' . $atr0 . ',' . $cena . '))';
                //$cenaMin = $cena;
                $cenaSearchClause = 'having';
            } else {
                if ($hurt) {
                    if ($this->obConfig->cenyNettoDomyslneHurt)
                        $cenaMin = 'if(p.promocja = 1, p.cena_promocji_n_hurt, p.cena_netto_hurt)';
                    else
                        $cenaMin = 'if(p.promocja = 1, p.cena_promocji_b_hurt, p.cena_brutto_hurt)';
                }
                else {
                    if ($this->obConfig->cenyNettoDomyslneDetal)
                        $cenaMin = 'if(p.promocja = 1, p.cena_promocji_n, p.cena_netto)';
                    else
                        $cenaMin = 'if(p.promocja = 1, p.cena_promocji_b, p.cena_brutto)';
                }
                if (true && $this->obConfig->atrybutyCenyRozne) {
                    $cenaMin = 'if(p.cena_atryb, min(' . str_replace('p.cena', 'atc.cena', $cenaMin) . '), ' . $cenaMin . ')';
                }
                $cenaSearchClause = 'where';
            }
        }
        if ($rabaty)
            $cenaMin = '(' . $cenaMin . ' * (1 - ifnull(' . $rabat . ', 0) / 100))';
        if ($netto)
            $cenaMin = str_replace('_brutto', '_netto', $cenaMin);
        if ($netto)
            $cenaMin = str_replace('_b', '_n', $cenaMin);
        $cenaMinSearch = str_replace('min', '', $cenaMin);
        $cenaMinSearch = ($cenaSearchClause == 'where') ? $cenaMinSearch : 'cena_min';

        if ($ile)
            $select = 'count(distinct p.id)'; //$select .= ', '.$cenaMin.' as cena_min';
        if (!$ile && !$links) {
            $select = 'p.*';
            $select .= ', ' . $cenaMin . ' as cena_min';
            if (false && $rabaty)
                $select .= ', ' . $rabat . ' as rabat_obl';
            if ($rozmiar)
                $select .= ', r.nazwa as rozmiar';
            if ($kolor)
                $select .= ', k.nazwa as kolor';
            if (true)
                $select .= ', ifnull(g.img, "brak.jpg") as img, g.nazwa as img_opis';
            if ($dostepnosc && $rozmiar)
                $select .= ', sum(rp.dostepnosc) as dostepnosc';
            if ($allegro)
                $select .= ', a.wystawiono, a.na_ile, a.nr, ' . $allegroDoKonca;
            if ($allegro)
                $select .= ', a.sztuk_wystawiono, a.sztuk_pozostalo'; //, count(nr) as ile_aukcji';
            if ($odwiedziny)
                $select .= ', sum(o.odwiedziny) as odwiedziny_suma';
            if ($sprzedaz)
                $select .= ', sum(ap.ilosc) as sprzedaz, sum(ap.wartosc) as sprzedaz_wartosc';
            if ($atrybuty)
                $select .= ', atg.nazwa as atrybut,if(at.nazwa is not null,at.nazwa,atp.wartosc) as atrybval';
        }

        $specjalne = '';
        $sort = 'p.nazwa';
        $rozmiaryJoin = 'LEFT';
        if (!$ile)
            if ($sortowanie != null) {
                if ($sortowanie->sort == 'nazwa' && @!empty($szukanie->nazwa))
                    $sort = 'if(p.nazwa like "' . $szukanie->nazwa . '%",1,0) desc, p.nazwa';
                if ($sortowanie->sort == 'oznaczenie')
                    $sort = 'p.oznaczenie';
                if ($sortowanie->sort == 'cena_netto')
                    $sort = 'cena_min';
                if ($sortowanie->sort == 'cena_brutto')
                    $sort = 'cena_min';
                if ($sortowanie->sort == 'data')
                    $sort = 'p.data';
                if ($sortowanie->sort == 'opak')
                    $sort = 'p.jedn_miary_ile';
                if ($sortowanie->sort == 'pozycja')
                    $sort = 'p.pozycja';
                if ($sortowanie->sort == 'rozmiar')
                    $sort = 'r.nazwa';
                if ($sortowanie->sort == 'wkladka')
                    $sort = 'r.wkladka';
                if ($sortowanie->sort == 'kolor')
                    $sort = 'k.nazwa';
                if ($sortowanie->sort == 'allegro_do_konca')
                    $sort = $allegroDoKonca; //'do_konca';
                if ($sortowanie->sort == 'wystawiono')
                    $sort = 'a.wystawiono';
                if ($sortowanie->sort == 'na_ile')
                    $sort = 'a.na_ile';
                if ($sortowanie->sort == 'sztuk_wystawiono')
                    $sort = 'a.sztuk_wystawiono';
                if ($sortowanie->sort == 'sztuk_pozostalo')
                    $sort = 'a.sztuk_pozostalo';
                if ($sortowanie->sort == 'odwiedziny')
                    $sort = 'sum(o.odwiedziny)';
                if ($sortowanie->sort == 'sprzedaz')
                    $sort = 'sum(ap.ilosc)';
                if ($sortowanie->sort == 'sprzedaz_ilosc')
                    $sort = 'sum(ap.ilosc)';
                if ($sortowanie->sort == 'sprzedaz_wartosc')
                    $sort = 'sum(ap.wartosc)';
                if ($sortowanie->sort == 'dostepnosc') {
                    if ($this->obConfig->tryb == 'produkt')
                        $sort = 'p.dostepnosc';
                    else {
                        $sort = 'sum(rp.dostepnosc)';
                        $rozmiaryJoin = 'INNER';
                    }
                }
                if ($sortowanie->specjalne != '' && $sortowanie->specjalne != 'all')
                    $specjalne = $sortowanie->specjalne . ' desc, ';
                if (intval($sortowanie->sort) > 0) {
                    $atrybut = intval($sortowanie->sort);
                    $atrybutygrupy = new Atrybutygrupy();
                    $atrybutygrupy->id = $atrybut;
                    $atryb = $atrybutygrupy->getRow();
                    //var_dump($atryb);
                    if ($atryb['typatrybut'] == 'tekst')
                        $atrybpole = 'atp.wartosc';
                    else
                        $atrybpole = 'at.nazwa';
                    if ($atryb['jedn_miary_typ'] == 'tekst')
                        $sort = $atrybpole;
                    if ($atryb['jedn_miary_typ'] == 'int')
                        $sort = 'CAST(' . $atrybpole . ' AS SIGNED)';
                    if ($atryb['jedn_miary_typ'] == 'float')
                        $sort = 'CAST(' . $atrybpole . ' AS DECIMAL)';
                    $sort = 'if(atp.id_gr = ' . $atrybut . ', ' . $sort . ', "")';
                    $atrybut = ' = ' . $atrybut;
                }
            }

        if ($szukanie != null) {
            //if($szukanie->nazwa != '') $where .= ' and p.nazwa like "%'.$szukanie->nazwa.'%"';
            if ($szukanie->nazwa != '')
                $where .= $this->common->szukajFraz($szukanie->nazwa, 'p.nazwa', 1, 0, 1, $this->module == 'default', 0);
            //if(@!empty($szukanie->nazwa)) $where .= ' and (p.nazwa like "'.$szukanie->nazwa.'%" or 
            //p.nazwa like "% '.$szukanie->nazwa.'%" or p.nazwa like "%-'.$szukanie->nazwa.'%")';
            if ($szukanie->oznaczenie != '')
                $where .= ' and p.oznaczenie like "%' . $szukanie->oznaczenie . '%"';
            //if(@!empty($szukanie->oznaczenie)) $where .= ' and (p.oznaczenie like "'.$szukanie->oznaczenie.'%" or 
            //p.oznaczenie like "% '.$szukanie->oznaczenie.'%" or p.oznaczenie like "%-'.$szukanie->oznaczenie.'%")';
            if (@!empty($szukanie->marka))
                $where .= ' and (pp.nazwa like "%' . $szukanie->marka . '%")';
            if (isset($szukanie->grupa) && @$szukanie->grupa != 'all')
                if ($this->module == 'admin')
                    $where.=' and (p.grupa=' . $szukanie->grupa . ')';
                else
                    $where .= ' and (p.grupa = "' . $szukanie->grupa . '" or p.grupa = -1)';
            if (isset($szukanie->rozmiar) && $szukanie->rozmiar != 'all')
                $where .= ' and r.nazwa = "' . $szukanie->rozmiar . '"';
            if (isset($szukanie->wkladka) && $szukanie->wkladka != 'all')
                $where .= ' and r.wkladka = "' . $szukanie->wkladka . '"';
            if (@$szukanie->kolor != -1 && $kolor)
                $where .= ' and k.id = ' . $szukanie->kolor;
            if (@$szukanie->cena_netto > 0)
                $$cenaSearchClause .= ' and ' . $cenaMinSearch . ' = ' . str_replace(",", ".", $szukanie->cena_netto);
            if (@$szukanie->cena_brutto > 0)
                $$cenaSearchClause .= ' and ' . $cenaMinSearch . ' = ' . str_replace(",", ".", $szukanie->cena_brutto);
            if (@$szukanie->cena_od > 0)
                $$cenaSearchClause .= ' and ' . $cenaMinSearch . ' >= ' . str_replace(",", ".", $szukanie->cena_od);
            if (@$szukanie->cena_do > 0)
                $$cenaSearchClause .= ' and ' . $cenaMinSearch . ' <= ' . str_replace(",", ".", $szukanie->cena_do);
            if (@$szukanie->producent > 0)
                $where .= ' and p.producent = ' . $szukanie->producent . '';
            if (isset($szukanie->nowosc) && $szukanie->nowosc)
                $where .= ' and p.nowosc = "1"';
            if (isset($szukanie->promocja) && $szukanie->promocja)
                $where .= ' and p.promocja = "1"';
            if (isset($szukanie->promocjaNet) && $szukanie->promocjaNet)
                $where .= ' and p.promocja_net = "1"';
            if (isset($szukanie->wyprzedaz) && $szukanie->wyprzedaz)
                $where .= ' and p.wyprzedaz = "1"';
            //if(isset($szukanie->bestseller) && $szukanie->bestseller) $where .= ' and p.bestseller = "1"';
            if (isset($szukanie->polecane) && $szukanie->polecane)
                $where .= ' and p.polecane = "1"';
            if (isset($szukanie->specjalne) && $szukanie->specjalne)
                $where .= ' and p.specjalne = "1"';
            if (isset($szukanie->produktDnia) && $szukanie->produktDnia)
                $where .= ' and p.produkt_dnia = "1"';
            if (isset($szukanie->glowna) && $szukanie->glowna)
                $where .= ' and p.glowna = "1"';
            if (isset($szukanie->widoczny) && $szukanie->widoczny)
                $where .= ' and !p.widoczny';
            else if ($this->module != 'admin')
                $where .= ' and p.widoczny';

            if (isset($szukanie->aukcje) && $szukanie->aukcje == 'all')
                $where .= ' and a.wystawiono is not null and (a.na_ile = 0 or (DATE_ADD(a.wystawiono,INTERVAL a.na_ile DAY)-NOW()) > 0)';

            if (isset($szukanie->odwiedziny) && $szukanie->odwiedziny != '') {
                $where .= ' and o.data is not null';
                if ($szukanie->odwiedziny != 'all') {
                    $where .= ' and o.data >= "' . $szukanie->odwiedziny . '-01-01"';
                    $where .= ' and o.data <= "' . $szukanie->odwiedziny . '-12-31"';
                }
            }
            if (isset($szukanie->dataOdwiedzinOd) && $szukanie->dataOdwiedzinOd != '')
                if (isset($szukanie->dataOdwiedzinDo) && $szukanie->dataOdwiedzinDo != '') {
                    $where .= ' and o.data is not null';
                    if ($szukanie->dataOdwiedzinOd != 'all')
                        if ($szukanie->dataOdwiedzinDo != 'all') {
                            $where .= ' and o.data >= "' . $szukanie->dataOdwiedzinOd . '"';
                            $where .= ' and o.data <= "' . $szukanie->dataOdwiedzinDo . '"';
                        }
                }
            if (isset($szukanie->sprzedaz) && $szukanie->sprzedaz != '') {
                $where .= ' and ak.data is not null';
                if ($szukanie->sprzedaz != 'all') {
                    $where .= ' and ak.data >= "' . $szukanie->sprzedaz . '-01-01 00:00:00"';
                    $where .= ' and ak.data <= "' . $szukanie->sprzedaz . '-12-31 23:59:59"';
                }
            }
            if (isset($szukanie->dataSprzedazyOd) && $szukanie->dataSprzedazyOd != '')
                if (isset($szukanie->dataSprzedazyDo) && $szukanie->dataSprzedazyDo != '') {
                    $where .= ' and ak.data is not null';
                    if ($szukanie->dataSprzedazyOd != 'all')
                        if ($szukanie->dataSprzedazyDo != 'all') {
                            $where .= ' and ak.data >= "' . $szukanie->dataSprzedazyOd . ' 00:00:00"';
                            $where .= ' and ak.data <= "' . $szukanie->dataSprzedazyDo . ' 23:59:59"';
                        }
                }
            if (isset($szukanie->dataOd) && $szukanie->dataOd != '')
                if (isset($szukanie->dataDo) && $szukanie->dataDo != '') {
                    $where .= ' and p.data is not null';
                    if ($szukanie->dataOd != 'all')
                        if ($szukanie->dataDo != 'all') {
                            $where .= ' and p.data >= "' . $szukanie->dataOd . ' 00:00:00"';
                            $where .= ' and p.data <= "' . $szukanie->dataDo . ' 23:59:59"';
                        }
                }
            if (@count($szukanie->atrybuty) > 0) {
                $atrybutypowiazania = new Atrybutypowiazania();
                $prods = $atrybutypowiazania->getProdukty($szukanie->atrybuty);
//                dumpv($prods);die;
                if (is_array($prods))
                    $where .= ' and p.id in (' . implode(',', $prods) . ')';
                //$atrybut = -1;//' in ('.implode(',', array_keys($szukanie->atrybuty)).')';
            }
        }
        else if ($this->module != 'admin')
            $where .= ' and p.widoczny = "1"';
        if ($this->module != 'admin' && $prodGrupy)
            $where .= ' and (pg.id is null or pg.wyswietl = "1")';
        if ($this->module != 'admin' && $this->ustawienia['produkty_bez_zdjec'])
            $where .= ' and g.img is not null';

        if ($spec != null)
            $where .= ' and p.' . $spec . ' = 1 ';
        $prod = 0; // @$szukanie->producent < 0 && $this->module != 'admin' ? -1 : 0;
        if ($prod < 0)
            $where .= ' AND (kp2.id_kat is null or (k2.rodzic = 0 and !k2.modele))';

        if ($szukanie->producenci) {
            if (count($szukanie->producenci) > 1) {
                $i = 1;
                foreach ($szukanie->producenci as $prod) {
                    if ($i < count($szukanie->producenci)){
                        $str = ' or ';
                    } else {
                        $str = '';
                    }
                    $w .= ' pp.id = "' . $prod . '"'.  $str ;
                    $i++;
                }
                $where = ' (' . $w . ')';
                
            } else {
                $where .= ' and pp.id = "' . (array_values($szukanie->producenci)[0]) . '"';
            }
        }

        $sql = 'SELECT ' . $select . ' FROM Produkty p ';
        if ($prodGrupy)
            $sql .= 'LEFT JOIN Produktygrupy pg ON(p.grupa = pg.id) ';
        if ($rozmiar)
            $sql .= 'LEFT JOIN Rozmiarproduktu rp ON(p.id = rp.id_produktu) ';
        if ($rozmiar)
            $sql .= 'LEFT JOIN Rozmiary r ON(r.id = rp.id_koloru and r.typ = p.rozmiarowka) ';
        if (false)
            $sql .= 'LEFT JOIN Katprod kat ON(p.id = kat.id_prod and kat.typ = "kategorie") ';
        if ($prod < 0)
            $sql .= 'LEFT JOIN Katprod kp2 ON(p.id = kp2.id_prod) ';
        if ($prod < 0)
            $sql .= 'LEFT JOIN Kategorie k2 ON(k2.id = kp2.id_kat) ';
        if ($kolor)
            $sql .= 'LEFT JOIN Kolorproduktu kp ON(p.id = kp.id_produktu) ';
        if ($kolor)
            $sql .= 'LEFT JOIN Kolory k ON(k.id = kp.id_koloru) ';
        if ($producent)
            $sql .= 'LEFT JOIN Producent pp ON(pp.id = p.producent) ';
        if (true)
            $sql .= 'LEFT JOIN Galeria g ON(p.id = g.wlasciciel and g.typ = "oferta" and g.glowne = "T") ';
        if ($allegro)
            $sql .= 'LEFT JOIN Allegroaukcje a ON(a.id_prod = p.id) ';
        if ($odwiedziny)
            $sql .= 'LEFT JOIN Odwiedziny o ON(o.id_prod = p.id and o.typ = "produkty") ';
        if ($sprzedaz)
            $sql .= 'LEFT JOIN Archiwumprodukty ap ON(ap.id_prod = p.id) ';
        if ($sprzedaz)
            $sql .= 'LEFT JOIN Archiwumkontrahenci ak ON(ap.id_zam = ak.id) ';
        if ($atrybuty)
            $sql .= 'LEFT JOIN Atrybutypowiazania atp ON(p.id = atp.id_og) '; //and atp.id_gr '.$atrybut.') ';
        if ($atrybuty)
            $sql .= 'LEFT JOIN Atrybuty at ON(atp.id_at = at.id) ';
        if ($atrybuty)
            $sql .= 'LEFT JOIN Atrybutygrupy atg ON(if(atp.id_gr>0,atp.id_gr,at.id_gr) = atg.id) ';
        if ($atrybCeny)
            $sql .= 'LEFT JOIN Atrybutyceny atc ON(p.id = atc.id_prod) ';
        $sql .= 'WHERE ' . $where . ' ';
        $group = $rozmiar || $allegro || $atrybCeny || strlen($having) > 1;
        $group = $group || (strpos($select, 'sum(') !== false) || (strpos($select, 'count(') !== false);
        $group = $group || (strpos($sort, 'sum(') !== false) || (strpos($sort, 'count(') !== false);
        $groupBy = false && $allegro ? 'a.nr' : 'p.id';
        if (!$ile && $group)
            $sql .= 'GROUP BY ' . $groupBy . ' HAVING ' . $having . ' ';
        if (!$ile)
            $sql .= 'ORDER BY ' . $specjalne . ' ' . $sort . ' ' . $sortowanie->order . ' ';
        $sql .= 'LIMIT ' . $od . ', ' . $limit . ' ';
        $sql = str_replace('=null', '= "null"', $sql);
        $sql = str_replace('= null', '="null"', $sql);
        if (!$ile && $test)
            echo $sql;
//dump($sql);die;
        $result = $this->db->fetchAll($sql);
        return $ile ? @intval($result[0]['count(distinct p.id)']) : $result;
    }

    function getCenaMin($hurt = false, $promo = null) {
        $cenaPromocja = $hurt ? 'cena_promocji_b_hurt' : 'cena_promocji_b';
        $cenaBezPromo = $hurt ? 'cena_brutto_hurt' : 'cena_brutto';
        $promo = $promo !== null ? intval($promo) : 'p.promocja';
        $cena = 'if(' . $promo . ', p.' . $cenaPromocja . ', p.' . $cenaBezPromo . ')';
        $if11 = '(atp.typatrybut = "lista" or atp.typatrybut = "multi") and atp.ceny = "1" and atp.cenyTryb = "1"';
        $atr1 = 'if(' . $promo . ',';
        $atr1.= 'min(if(' . $if11 . ',atp.' . $cenaPromocja . ',null)),';
        $atr1.= 'min(if(' . $if11 . ',atp.' . $cenaBezPromo . ',null)))';
        $atr1if = 'sum(if(' . $if11 . ',1,0))>0';
        $if10 = '(atp.typatrybut = "lista" or atp.typatrybut = "multi") and atp.ceny = "1" and atp.cenyTryb = "0"';
        $atr0 = 'if(' . $promo . ',';
        $atr0.= 'sum(if(' . $if10 . ',atp.' . $cenaPromocja . ',0)),';
        $atr0.= 'sum(if(' . $if10 . ',atp.' . $cenaBezPromo . ',0)))';
        $atr0if = 'sum(if(' . $if10 . ',1,0))>0';
        $cenaMin = 'if(' . $atr1if . ',' . $atr1 . ',if(' . $atr0if . ',' . $cena . '+' . $atr0 . ',' . $cena . '))';
        //$cenaMin = $cena;
        return $cenaMin;
    }

    function getCena($id = 0, $ids = null, $hurt = false, $kat = 0) {
        $cenaMin = $this->getCenaMin($hurt, null);
        $select = $cenaMin . ' as cena_min'; //, (cena_min / (1 + p.vat / 100)) as cena_min_n';
        if (true) {
            $cenaHurtPromocja = $this->getCenaMin(true, 1);
            $select.=',' . $cenaHurtPromocja . ' as cena_promocji_b_hurt';
            $cenaHurtBezPromo = $this->getCenaMin(true, 0);
            $select.=',' . $cenaHurtBezPromo . ' as cena_brutto_hurt';
            $cenaDetalPromocja = $this->getCenaMin(false, 1);
            $select.=',' . $cenaDetalPromocja . ' as cena_promocji_b';
            $cenaDetalBezPromo = $this->getCenaMin(false, 0);
            $select.=',' . $cenaDetalBezPromo . ' as cena_brutto';
        }
        if (false) {
            $cenaHurt = $this->getCenaMin(true, null);
            $select.=',' . $cenaHurt . ' as cena_hurt';
            $cenaDetal = $this->getCenaMin(false, null);
            $select.=',' . $cenaDetal . ' as cena_detal';
            $cenaPromocja = $this->getCenaMin($hurt, 1);
            $select.=',' . $cenaPromocja . ' as cena_promocja';
            $cenaBezPromo = $this->getCenaMin($hurt, 0);
            $select.=',' . $cenaBezPromo . ' as cena_bezpromo';
        }
        $where = 'p.widoczny';
        if ($id > 0)
            $where .= ' and p.id = ' . $id;
        if ($this->common->isArray($ids, true))
            $where .= ' and p.id in(' . implode(',', $ids) . ')';
        if ($kat > 0)
            $where .= ' and k.id_kat = ' . $kat;
        $sql = 'SELECT p.id, ' . $select . ' FROM Produkty p ';
        if ($kat > 0)
            $sql .= 'LEFT JOIN Katprod k ON(p.id = k.id_prod) ';
        $sql .= 'LEFT JOIN Atrybutypowiazania atp ON(p.id = atp.id_og) ';
        $sql .= 'LEFT JOIN Atrybuty at ON(atp.id_at = at.id) ';
        $sql .= 'LEFT JOIN Atrybutygrupy atg ON(if(atp.id_gr>0,atp.id_gr,at.id_gr) = atg.id) ';
        $sql .= 'WHERE ' . $where . ' GROUP BY p.id ORDER BY cena_min DESC'; // LIMIT 1 ';
        //echo($sql);
        $result = $this->db->fetchAll($sql);
        for ($i = 0; $i < count($result); $i++) {
            $prodID = $result[$i]['id'];
            unset($result[$i]['id']);
            $results[$prodID] = $result[$i];
        }
        return @$results; //@floatval($result[0]['cena_min']);
    }

    function wyszukaj($co, $w, $prod = 0) {
        if ($w != 'cena_brutto' && $prod == 0) {
            $result = $this->fetchAll($w . ' LIKE "%' . $co . '%"');
        } else if ($prod > 0) {
            $result = $this->fetchAll($w . ' LIKE "%' . $co . '%" AND producent =' . $prod);
        } else {
            $result = $this->fetchAll($w . ' = ' . $co);
        }
        return $result;
    }

    function wyszukajStronicowanie($co, $w, $prod = 0, $od = 0, $po = 20, $sort = 'nazwa', $order = 'asc') {
        if ($w != 'cena_brutto' && $prod == 0) {
            $where = $w . ' LIKE "%' . $co . '%"';
        } else if ($prod > 0) {
            $where = $w . ' LIKE "%' . $co . '%" AND producent =' . $prod;
        } else {
            $where = $w . ' = ' . $co;
        }
        $sql = $this->select()->where($where)->order($order)->limit($po, $od);
        $result = $this->fetchAll($sql)->toArray();
        return $result;
    }

    function szukajKodNazwa($co) {
        $result = $this->fetchAll('widoczny = 1 AND nazwa LIKE "%' . $co . '%" OR oznaczenie LIKE "%' . $co . '%"');
        return $result;
    }

    function wypiszSzukajKodNazwa($od = 0, $po = 30, $co) {
        $where = 'widoczny = 1 AND nazwa LIKE "%' . $co . '%" OR oznaczenie LIKE "%' . $co . '%"';
        $sql = $this->select()->where($where)->limit($po, $od);
        $result = $this->fetchAll($sql)->toArray();
        return $result;
    }

    function szukajOpis($co) {
        $result = $this->fetchAll('widoczny = 1 AND skrot LIKE "%' . $co . '%" OR tekst LIKE "%' . $co . '%"');
        return $result;
    }

    function wypiszSzukajOpis($od = 0, $po = 30, $co) {
        $where = 'widoczny = 1 AND skrot LIKE "%' . $co . '%" OR tekst LIKE "%' . $co . '%"';
        $sql = $this->select()->where($where)->limit($po, $od);
        $result = $this->fetchAll($sql)->toArray();
        return $result;
    }

    function szukajZamiennik($co, $gdzie) {
        if ($gdzie != 0)
            $result = $this->fetchAll('widoczny = 1 AND ' . $gdzie . ' LIKE "%' . $co . '%"');
        else
            $result = $this->fetchAll('widoczny = 1 AND mann LIKE "%' . $co . '%" OR filtron LIKE "%' . $co . '%"');
        return $result;
    }

    function wypiszSzukajZamiennik($od = 0, $po = 30, $co, $gdzie) {
        $where = 'widoczny = 1 AND ' . $gdzie . ' LIKE "%' . $co . '%"';
        if ($gdzie != 0)
            $where = 'widoczny = 1 AND ' . $gdzie . ' LIKE "%' . $co . '%"';
        else
            $where = 'widoczny = 1 AND mann LIKE "%' . $co . '%" OR filtron LIKE "%' . $co . '%"';

        $sql = $this->select()->where($where)->limit($po, $od);
        $result = $this->fetchAll($sql)->toArray();
        return $result;
    }

    function szukajCena($od, $do) {
        $result = $this->fetchAll('widoczny = 1 AND cena_brutto >= ' . $od . ' AND cena_brutto < ' . $do);
        return $result;
    }

    function wypiszSzukajCena($od = 0, $po = 30, $od_ceny, $do) {
        $where = 'widoczny = 1 AND cena_brutto >= ' . $od_ceny . ' AND cena_brutto < ' . $do;
        $sql = $this->select()->where($where)->limit($po, $od);
        $result = $this->fetchAll($sql)->toArray();
        return $result;
    }

    function wyszukajslowo($co) {
        $result = $this->fetchAll('widoczny = 1 AND nazwa LIKE "%' . $co . '%" OR skrot LIKE "%' . $co . '%"');
        return $result;
    }

    function wyszukajslowo2($co) {
        $where = 'widoczny = 1 AND (nazwa LIKE "%' . $co . '%")'; // or nazwa like "% '.$co.'%" or nazwa like "%-'.$co.'%")';
        $sql = $this->select()->where($where)->group('nazwa')->order('nazwa');
        $result = $this->fetchAll($sql);
        return $result;
    }

    function wyszukajsymbol2($co = "", $prod = 0, $kateg = 0) {
        $where = 'p.widoczny AND p.zmiana is not null and (p.oznaczenie LIKE "%' . $co . '%" or p.oznaczenie LIKE "%' . str_replace(" ", "", $co) . '%")';
        if ($prod > 0)
            $where .= ' AND p.producent = "' . $prod . '"';
        if ($prod < 0)
            $where .= ' AND (kp2.id_kat is null or (k2.rodzic = 0 and !k2.modele))';
        if ($kateg > 0)
            $where .= ' AND kp.id_kat = "' . $kateg . '"';
        //$sql = $this->select()->where($where)->group('oznaczenie')->order('oznaczenie');
        $select = $this->db->select()->from(array('p' => 'Produkty'), array('id', 'nazwa', 'oznaczenie', 'link'));
        if ($prod > 0)
            $select->join(array('pp' => 'Producent'), 'p.producent = pp.id', array(''));
        if ($kateg > 0)
            $select->join(array('kp' => 'Katprod'), 'kp.id_prod = p.id', array(''));
        if ($prod < 0)
            $select->joinleft(array('kp2' => 'Katprod'), 'kp2.id_prod = p.id', array(''));
        if ($prod < 0)
            $select->joinleft(array('k2' => 'Kategorie'), 'kp2.id_kat = k2.id', array(''));
        $select->where($where)->group('p.oznaczenie')->order('p.oznaczenie asc');
        //$result = $this->fetchAll($sql);
        $result = $this->db->fetchAll($select); //echo $select;
        return $result;
    }

    function wyszukajslowo2producent($co) {
        $where = 'p.widoczny = 1 AND (p.nazwa LIKE "' . $co . '%" or p.nazwa like "% ' . $co . '%" or p.nazwa like "%-' . $co . '%")';
        $select = $this->db->select()->from(array('p' => 'Produkty'), array(''))
                        ->join(array('pp' => 'Producent'), 'p.producent = pp.id', array('id', 'nazwa'))
                        ->where($where . ' AND p.producent > 0')->group('p.producent')->order('pp.nazwa asc');
        //echo $select;
        $result = $this->db->fetchAll($select);
        return $result;
    }

    function wypiszWynikiPodzielone($od = 0, $po = 30, $co) {
        $where = 'widoczny = 1 AND nazwa LIKE "%' . $co . '%" OR skrot LIKE "%' . $co . '%"';
        $sql = $this->select()->where($where)->limit($po, $od);
        $result = $this->fetchAll($sql);
        return $result;
    }

    function wyszukajPoKodzie($kod) {
        $result = $this->fetchAll('oznaczenie = "' . $kod . '"');
        return $result[0];
    }

    function iloscProduktowSpecjalnych($specjalne, $lang = 'pl', $glowna = 0, $ids = null) {
        $where = $specjalne . ' = 1 AND widoczny = 1 AND lang = "' . $lang . '"';
        if ($glowna != 0)
            $where .= ' AND glowna = 1';
        if ($this->common->isArray($ids, true))
            $where .= ' AND id in (' . implode(',', $ids) . ')';
        //$result = $this->fetchAll($where);
        $select = $this->db->select()->from(array('p' => 'Produkty'), array('count(*)'))->where($where);
        //echo $select;
        $result = $this->db->fetchRow($select);
        return @intval($result['count(*)']);
        //return count($result);
    }

    function wypiszNowosciStrona() {
        $where = 'nowosc = 1 AND glowna_strona = 1 AND widoczny = 1 ';
        $sql = $this->select()->where($where)->limit('1', '0');
        $result = $this->fetchAll($sql);
        return $result;
    }

    function wypiszPromocjaStrona() {
        $where = 'promocja = 1 AND glowna_strona = 1 AND widoczny = 1 ';
        $sql = $this->select()->where($where)->limit('1', '0');
        $result = $this->fetchAll($sql);
        return $result;
    }

    function wypiszSpecjalne($specjalne, $lang = 'pl', $glowna = 0, $limit = null, $ile = 20, $order = 'nazwa') {
        $where = $specjalne . ' = 1 AND widoczny = 1 AND lang = "' . $lang . '"';
        if ($glowna != 0)
            $where.=' AND glowna = 1';
        $sql = $this->select()->where($where)->order($order)->limit($ile, $limit);
        $result = $this->fetchAll($sql);
        return $result;
    }

    function wypiszSpecjalneStrona($specjalne, $lang = 'pl', $glowna = 0, $limit = null, $ile = '20') {
        $where = $specjalne . ' = 1 AND widoczny = 1 AND lang = "' . $lang . '"';
        if ($glowna != 0)
            $where.='AND glowna_strona = 1';
        $sql = $this->select()->where($where)->limit($ile, $limit);
        $result = $this->fetchAll($sql);
        return $result;
    }

    function inneKolory($nazwa) {
        $where = 'widoczny = 1 and id != ' . $this->id . ' and nazwa = "' . $nazwa . '"';
        $sql = $this->select()->where($where);
        $result = $this->fetchAll($sql);
        return $result;
    }

    function szukaj($id) {
        $result = $this->fetchAll('id = ' . $id);
        return $result;
    }

    function szukajNazwa($nazwa) {
        //$result = $this->fetchAll('nazwa = "'.$nazwa.'"');
        $select = $this->select()->where('nazwa = "' . $nazwa . '"');
        $result = $this->fetchAll($select); //die();
        return $result;
    }

    function wypiszJeden() {
        $select = $this->select()->where('id = ' . $this->id);
        $result = $this->fetchAll($select);
        return $result;
    }

    function zmien($dane) {
        $where = 'id = ' . $this->id;
        $this->update($dane, $where);
    }

    function znajdzKod($kod) {
        $select = $this->select()->where('id_ext = "' . $kod . '"');
        $result = $this->fetchRow($select);
        return $result;
    }

    function znajdzKody($kod) {
        $select = $this->select()->distinct()
                ->from(array('p' => 'Produkty'), 'id_ext')
                ->where('id_ext like "%' . $kod . '%"')
                ->order('id_ext asc');
        $result = $this->fetchAll($select);
        return $result;
    }

    function wypiszAukcje() {
        $select = $this->db->select()->from(array('p' => 'Produkty'), array('id as rp.id', 'dostepnosc', 'dostepny'))
                        ->joinleft(array('a' => 'Allegroaukcje'), 'a.id_prod = p.id', array('id as a.id', 'nr', 'sztuk_pozostalo', 'na_ile', 'sztuk_wystawiono', 'wystawiono'))
                        ->where('p.id = ' . $this->id)->order('');
        //echo $select;
        $result = $this->db->fetchAll($select);
        return $result;
    }

    function changePositionAjax($position, $id) {
        $dane['pozycja'] = $position;
        $where = 'id= "' . $id . '"';
        $this->update($dane, $where);
    }

    function wypiszMaxPozycja() {
        $select = $this->db->select()->from(array('p' => 'Produkty'), array('max(pozycja)'))->where(1);
        //echo $select;
        $result = $this->db->fetchAll($select);
        return isset($result[0]['max(pozycja)']) ? intval($result[0]['max(pozycja)']) : 0;
    }

    function cenaMax($kateg = 0, $hurt = false, $atryb = false) {
        if ($hurt) {
            if ($this->obConfig->cenyNettoDomyslneHurt)
                $cena = 'if(p.promocja, p.cena_promocji_n_hurt, p.cena_netto_hurt)';
            else
                $cena = 'if(p.promocja, p.cena_promocji_b_hurt, p.cena_brutto_hurt)';
        }
        else {
            if ($this->obConfig->cenyNettoDomyslneDetal)
                $cena = 'if(p.promocja, p.cena_promocji_n, p.cena_netto)';
            else
                $cena = 'if(p.promocja, p.cena_promocji_b, p.cena_brutto)';
        }
        if ($atryb && $this->obConfig->atrybutyCeny)
            $cena = 'max(' . str_replace('p.cena', 'atp.cena', $cena) . ')';
        if ($atryb && $this->obConfig->atrybutyCenyRozne)
            $cena = 'if(p.cena_atryb, max(' . str_replace('p.cena', 'atc.cena', $cena) . '), ' . $cena . ')';

        $where = 'widoczny = 1';
        if ($kateg > 0)
            $where .= ' and k.id_kat = ' . $kateg;
        $sql = $this->select()->from(array('p' => 'Produkty'), $cena . ' as cena');
        if ($kateg > 0)
            $sql->joinleft(array('k' => 'Katprod'), 'p.id = k.id_prod', array(''));
        if ($atryb && $this->obConfig->atrybutyCeny)
            $sql->joinleft(array('atp' => 'Atrybutypowiazania'), 'p.id = atp.id_og and atp.ceny and atp.cenyTryb', array(''));
        if ($atryb && $this->obConfig->atrybutyCenyRozne)
            $sql->joinleft(array('atc' => 'Atrybutyceny'), 'p.id = atc.id_prod and atc.cena_netto > 0', array(''));
        $sql->where($where)->group('p.id')->order('cena desc')->limit(1);
        //echo $sql;
        $result = $this->fetchAll($sql);
        return count($result) > 0 ? @floatval($result[0]['cena']) : 0;
    }

    function wyszukajAdminRabaty($co, $w, $kat = 0, $prod = 0) {
        if ($w != 'cena_brutto' && $kat == 0 && $prod == 0) {
            $where = 'p.' . $w . ' LIKE "%' . $co . '%"';
        } else if ($w == 'cena_brutto' && $prod == 0) {
            $wher = null;
            if ($prod > 0)
                $wher .= ' AND p.producent =' . $prod;

            if ($kat > 0)
                $wher .= ' AND kp.id_kat =' . $kat;

            $where = 'p.' . $w . ' LIKE "' . $co . '%" ' . $wher;
        }
        else if ($prod > 0 && $kat > 0) {
            $where = 'p.' . $w . ' LIKE "%' . $co . '%" AND p.producent =' . $prod . ' AND kp.id_kat =' . $kat . '';
        } else if ($kat > 0) {
            $where = 'p.' . $w . ' LIKE "%' . $co . '%" AND kp.id_kat =' . $kat . '';
        } else if ($prod > 0) {
            $where = 'p.' . $w . ' LIKE "%' . $co . '%" AND p.producent =' . $prod . '';
        } else {
            $where = '' . $w . ' = "' . $co . '"';
        }

        $sql = $this->select()
                        ->from(array('p' => 'Produkty'))->setIntegrityCheck(false)
                        ->join(array('kp' => 'Katprod'), 'p.id = kp.id_prod')
                        ->where($where)->group('p.id')->order('p.id DESC');

        $result = $this->fetchAll($sql);
        return $result;
    }

    function cenaDetalFromHurtMarzaForKateg($kateg = 0, $marza = 0) {
        $sql = 'UPDATE Produkty SET ';
        $sql.= 'cena_netto = if(cena_netto_hurt > 0, cena_netto_hurt * (1 + ' . $marza . ' / 100), cena_netto)';
        $sql.= ', ';
        $sql.= 'cena_brutto = if(cena_brutto_hurt > 0, cena_brutto_hurt * (1 + ' . $marza . ' / 100), cena_brutto)';
        $sql.= ', ';
        $sql.= 'cena_promocji_n=if(cena_promocji_n_hurt>0,cena_promocji_n_hurt*(1+' . $marza . '/100),cena_promocji_n)';
        $sql.= ', ';
        $sql.= 'cena_promocji_b=if(cena_promocji_b_hurt>0,cena_promocji_b_hurt*(1+' . $marza . '/100),cena_promocji_b)';
        $sql.= ' WHERE trybCena = "produkt" ';
        //$sql.= ' and id in (SELECT id_prod from Katprod where id_kat = '.$kateg.')';
        $sql.= ' and id in (SELECT id_prod from Katprod group by id_prod having max(id_kat) = ' . $kateg . ')';
        //echo $sql;die();
        $this->db->query($sql);
        if ($this->obConfig->caching)
            $this->common->cacheRemove($sql, 'Produkty');
    }

    function wypiszProduktySchowek($ids = null, $ile = 99999, $order = 'nazwa asc') {
        if ($this->common->isArray($ids, true)) {
            $where = 'p.id in (' . implode(',', $ids) . ') and p.widoczny';
            $select = $this->db->select()->from(array('p' => 'Produkty'), array('*'));
            $select->joinleft(array('g' => 'Galeria'), 'g.wlasciciel=p.id and g.glowne="T" and g.typ="oferta"', array('img'));
            $select->where($where)->order($order)->limit($ile);
            $result = $this->db->fetchAll($select);
            return $result;
        }
    }

    public function linkiSitemap() {
        $select = $this->db->select()->from(array('p' => 'Produkty'), array('id', 'link', 'nazwa'));
        $select->joinleft(array('g' => 'Galeria'), 'g.wlasciciel=p.id and g.glowne="T" and g.typ="oferta"', array('img', 'img_opis' => 'g.nazwa'));
        $select->where('p.nazwa <> "" and p.widoczny and p.zmiana is not null and p.lang = "pl"')->order('p.nazwa')->limit(99999);
        $result = $this->db->fetchAll($select);
        return $result;
    }

}

?>