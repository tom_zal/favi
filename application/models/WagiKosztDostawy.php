<?php
class WagiKosztDostawy extends Zend_Db_Table
{
	public $link, $id, $typ;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujWagaDostawa($dostawa = 0, $waga = 0, $dane)
	{
		$where = 'id_dostawa = '.$dostawa.' and id_waga = '.$waga;
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$sql = $this->select()->where('1')->order(array('koszt'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszJeden($koszt)
	{
		$sql = $this->select()->where('koszt = "'.$koszt.'"')->order('koszt');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function usun()
	{
		$result = $this->delete('id = '.$this->id);			
	}		
	function wypiszID()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function wypiszWagaDostawa($dostawa = 0, $waga = 0)
	{
		$where = '1';
		if($waga > 0) $where .= ' and id_waga = '.$waga;
		if($dostawa > 0) $where .= ' and id_dostawa = '.$dostawa;
		$sql = $this->select()->where($where);
		//echo $sql;die();
		if($waga > 0 && $dostawa > 0)
		return $this->fetchRow($sql);
		else $result = $this->fetchAll($sql);
		if(@count($result) > 0)
		foreach($result as $row)
		{
			if($waga > 0) $results[$row['id_dostawa']] = $row->toArray();
			if($dostawa > 0) $results[$row['id_waga']] = $row->toArray();
		}
		return @$results;
	}
}
?>