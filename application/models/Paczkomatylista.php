<?php
class Paczkomatylista extends Zend_Db_Table
{
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

	function updateData($array)
	{
		$where = 'id = "'.$this->id.'"';
		$this->update($array, $where);
	}	
	function showData()
	{
		$result = $this->fetchAll();		
		return $result;
	}
	
	function wypiszID()
	{
		$result = $this->select()->where('id = "'.$this->id.'"');
		return $this->fetchRow($result)->toArray();
	}
	function wypiszName($name = "")
	{
		if(empty($name)) return null;
		$result = $this->select()->where('name = "'.$name.'"');
		return $this->fetchRow($result)->toArray();
	}
	
	function inpost_get_machine_list($town = "", $paymentavailable = "", $postcode = "")
	{
		$where = '1';
		if(!empty($town)) $where .= 'and town = "'.$town.'"';
		if(!empty($postcode)) $where .= 'and postcode = "'.$postcode.'"';
		//if(!empty($paymentavailable)) $where .= 'and paymentavailable = "'.$paymentavailable.'"';
		if($paymentavailable == "t") $where .= 'and paymentavailable = 1';
		if($paymentavailable == "f") $where .= 'and paymentavailable = 0';
		$result = $this->select()->where($where);
		return $this->fetchAll($result);
	}
}
?>