<?php
class Login extends Zend_Db_Table
{
	public $id;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
	function zaloguj($dane)
	{
		$result = $this->fetchAll('login = "'.$dane['login'].'"');
		if(count($result) > 0)
		foreach($result as $user)
		{
			if($user['haslo']==$dane['pass'])
			{
				return $user;
			}
			else
			{
				return null;
			}		
		}
		return null;
	}
	
	function ZmienHaslo($new_pass_1, $new_pass_2, $old_pass, $login = 'admin')
	{
		$result = $this->fetchAll('login = "'.$login.'"');
		if(count($result) > 0)
		foreach($result as $user)
		{
			if($user['haslo'] != md5($old_pass))
			{
				return 0;
			}
			if($old_pass == $new_pass_1)
			{
				return -1;
			}
			if($new_pass_1 == 'admin')
			{
				return -2;
			}
			else
			{
				$haslo = md5($new_pass_1);
				$dane = array('haslo' => $haslo);
				$where = 'id = '.$user['id'];
            	$this->update($dane, $where);
				return 1;
			}			
		}
	}
	
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	
	function wypisz()
	{
		$result = $this->fetchAll();
		return $result;
	}
	
	function wypiszPojedynczego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	
	function usun()
	{
		$where = 'id = '.$this->id.'';
		$this->delete($where);
	}
}
?>