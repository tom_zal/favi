<?php
class Druki extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
	public function wypisz($id)
	{
		$where = 'id =' . $id;
		return $this->fetchRow($where);
	}
	
	public function zapisz($id, $data)
	{
		$where = 'id =' . $id;
        $this->update($data, $where);
	}
	
	public function wypiszByMode($mode)
	{
		$where = 'mode = "' . $mode.'"';
		return $this->fetchRow($where);
	}
	
	public function zapiszByMode($mode, $data)
	{
		$where = 'mode = "' . $mode.'"';
        $this->update($data, $where);
	}

}
?>