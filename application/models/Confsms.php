<?php
class Confsms extends Zend_Db_Table
{
	public $id;	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	function edytuj($dane)
	{
		$where = 'id = "'.$this->id.'"';
		$this->update($dane, $where);
	}
	function wyswietl()
	{
		$result = $this->fetchRow('id = 1');
		return $result;
	}
	function wyslij($telefon, $tekst, $typ = null, $kto = null)
	{
		$data = $this->wyswietl();
		if($typ === null) $typ = $data['typ_wiadomosci'];
		if($kto === null) $kto = $data['pole_nadawca'];
		//$daneSMS['nadawca'] = $data['pole_nadawca'];
		//$daneSMS['typ'] = $data['typ_wiadomosci'];
		$soap = new SmsClientSoap();
		return $soap->send($tekst, $telefon, $typ, $kto);
	}
}
?>