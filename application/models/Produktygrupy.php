<?php
class Produktygrupy extends Zend_Db_Table
{
	public $link, $id;
	
	public function __construct()
	{
		parent::__construct();
		$common = new Common();
		$this->obConfig = $common->getObConfig();
        $this->db = $common->getDB($this->obConfig);
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$this->update($dane, 'id = '.$this->id);
	}
	function wypisz($wyswietl = false)
	{
		$where = $wyswietl ? 'wyswietl' : 1;
		$sql = $this->select()->where($where)->order(array('pozycja', 'nazwa'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszIle()
	{
		$select = $this->db->select()->from(array('pg' => 'Produktygrupy'), array('*'))
			->joinleft(array('p' => 'Produkty'), 'pg.id = p.grupa', array('count(p.id) as ile'))
			->where('1')->group('pg.id')->order(array('pg.nazwa'));
		//echo $select;die();
        $stmt = $select->query();
		$result = $stmt->fetchAll();
        return $result;
	}
	
	function wypiszNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa = "'.$nazwa.'"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszWhereNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa like "%'.$nazwa.'%"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	
	function wypiszForAll($pole = 'id')
	{
		$result = $this->fetchAll();
		for($i = 0; $i < count($result); $i++)
		{
			$grupy[$result[$i][$pole]] = $result[$i][$pole == 'id' ? 'nazwa' : 'id'];
		}		
		return @$grupy;
	}
	function getGrupy($pole = 'nazwa')
	{
		$sql = $this->select()->order('nazwa');
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i][$pole]] = $result[$i]->toArray();
		}
		return @$results;
	}
	
	function usun()
	{
		$result = $this->delete('id = '.$this->id);
	}
	function wypiszPojedynczego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
}
?>