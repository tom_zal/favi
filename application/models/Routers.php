<?php
class Routers extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
    function wczytaj()
	{
        $result = $this->fetchAll()->toArray();
        for($i=0;$i<count($result);$i++)
		{
            $trasy[$result[$i]['id']] =  array
			(
				'route' => $result[$i]['route'].'/*',
				'defaults' => array
				(
					'module' => $result[$i]['module'],
					'controller' => $result[$i]['controller'],
					'action' => $result[$i]['action'],
					'id' => $result[$i]['params']
				)
            );
        }
        $trasy_rutingu = array('routes' => $trasy);
        return $trasy_rutingu;
    }
	
	function wczytajPojedynczyAction($action = "", $controller = "oferta")
	{
		$where = 'action = "'.$action.'" and controller = "'.$controller.'"';
	    //echo $where;
        $result = $this->fetchRow($where);
	    return $result;
	}
	
	function wczytajPojedynczy($path)
	{
	   if(isset($path[2]))
	   {
           $path[0] = $path[2];
       }
	   
       if(isset($path[1]))
	   {
           $path[0] = $path[1];
       }
	   else 
	   {
           $path[1] = '';
       }
	   
       $where = 'route LIKE "'.$path[0].'"';
	   //echo $where;
       $result = $this->fetchRow($where);

       if(count($result) > 0) 
	   {
           $result = $result->toArray();
           $trasy[$result['id']] =  array 
		   (
               'route' => $result['route'].'/*',
               'defaults' => array
			   (
                   'module' => $result['module'],
                   'controller' => $result['controller'],
                   'action' => $result['action'],
                   'id' => $result['params'],
                   'paramurl' => $path[1]
               ),
           );
       } 
	   else
	   {
           $trasy[0] =  array 
		   (
               'route' => ':controller/:action/*',
               'defaults' => array
			   (
                   'module' => 'default',
                   'controller' => 'index',
                   'action' => 'index'
               )
           );
           $trasy[1] =  array 
		   (
               'route' => 'admin/:controller/:action/*',
               'defaults' => array
			   (
                   'module' => 'admin',
                   'controller' => 'index',
                   'action' => 'index',
               )
           );
           $trasy[2] =  array 
		   (
                   'route' => 'ajax/:controller/:action/*',
                   'defaults' => array
				   (
                       'module' => 'ajax',
                       'controller' => 'index',
                       'action' => 'index',
                   )
           );
           $trasy[3] =  array 
		   (
                   'route' => 'platnosci/:controller/:action/*',
                   'defaults' => array
				   (
                       'module' => 'platnosci',
                       'controller' => 'index',
                       'action' => 'index',
                   )
           );
           $trasy[4] =  array 
		   (
                   'route' => 'szablon/:controller/:action/*',
                   'defaults' => array
				   (
                       'module' => 'szablon',
                       'controller' => 'index',
                       'action' => 'index',
                   )
           );
       }
	   
       $trasy_rutingu = array('routes' => $trasy);

       return $trasy_rutingu;
   }
   
    function linkZNazwy($nazwa)
	{
        return $this->common->makeLink($nazwa, true);
    }
	
    function sprawdzCzyIstnieje($nazwa)
	{
		if(empty($nazwa)) return 0;
		if($nazwa == 'Nowy-produkt') return 1;
        $where = 'route LIKE "'.$nazwa.'"';
        $result = $this->fetchAll($where);
        if(count($result) > 0) return 0;
        else return 1;
    }
	
    function dodaj($nazwa, $parametry = NULL, $modul = 'default', $kontroler = 'produkt', $akcja = 'index')
	{
        $nazwa = trim($nazwa);
        $nazwa = $this->linkZNazwy($nazwa);
		if($parametry == '') $parametry = $nazwa;
        //echo $nazwa;
        //$nazwa = $nazwa.'/*';
		$zawieraID = (substr($nazwa, - (strlen($parametry) + 1)) == '-'.$parametry);
        $sprawdz = $zawieraID || $this->sprawdzCzyIstnieje($nazwa);
        if($sprawdz)
		{
            $data['route'] = $nazwa;
            $data['module'] = $modul;
            $data['controller'] = $kontroler;
            $data['action'] = $akcja;
            $data['params'] = $parametry;

            $this->insert($data);
            $id = $this->getAdapter()->lastInsertId();
            $ret['id'] = $id;
            $ret['link'] = $data['route'];
            return $ret;
        }
		else
		{
            $ret['id'] = 0;
            $ret['link'] = $nazwa;
            //print_r($ret);
            return $ret;
        }
    }
    function edytuj($nazwa, $id = 0, $link = '', $params = '', $changeParams = false)
	{
        $nazwa = trim($nazwa);
        $nazwa = $this->linkZNazwy($nazwa);
        //$nazwa = $nazwa.'/*';
        $sprawdz = $this->sprawdzCzyIstnieje($nazwa);
        if($sprawdz)
		{
            $dane['route'] = $nazwa;
			$where = '1';
			if(false || $id > 0) $where .= ' and id = '.$id;
			if(!empty($link)) $where .= ' and route = "'.$link.'"';
			if(!empty($params)) $where .= ' and params = "'.$params.'"';
			if($changeParams) $dane['params'] = $nazwa;
			if($where != '1')
            $this->update($dane, $where);
            $ret['id'] = $id > 0 ? $id : $link;
            $ret['link'] = $nazwa;
            return $ret;
        }
		else
		{
            $ret['id'] = 0;
            $ret['link'] = $nazwa;
            return $ret;
        }
    }
	function edytujPodstrona($linkOld, $linkNew)
	{
		if(empty($linkOld)) return false;
		if(empty($linkNew)) return false;
		
		$dane['route'] = $linkNew;
		$where = 'route = "'.$linkOld.'" and (controller = "oferta" and params is null)';
		//var_dump($where);die();
		$this->update($dane, $where);
		
		$dane['params'] = $linkNew;
		$where = 'route = "'.$linkOld.'" and (controller = "podstrony" and (action = "show" or action = "strona"))';
		//var_dump($where);die();
		$this->update($dane, $where);
		
		return true;
    }
	
	function setLink($dane, $link, $lang = null)
	{
		if(empty($link)) return;
		$where = 'route = "'.$link.'"';
		$ok = $this->update($dane, $where);
		if(empty($lang)) return $ok;
		$dane['route'] .= '-'.$lang;
		$where = 'route = "'.$link.'-'.$lang.'"';
		//var_dump($where); die();
		return $this->update($dane, $where);
	}
	
    function usun($id)
	{
        $this->delete('id = '.$id);
    }
	function usunProdukt($id = 0)
	{
        $this->delete('module = "default" and controller = "produkt" and action = "index" and params = "'.$id.'"');
    }
	function usunProduktyIds($ids)
	{
        $this->delete('module = "default" and controller = "produkt" and action = "index" and params in ('.$ids.')');
    }
	function usunProdukty() 
	{
        $this->delete('module = "default" and controller = "produkt" and action = "index"');
    }
	function usunKategoria($id = 0)
	{
        $this->delete('module = "default" and controller = "oferta" and action = "kategorie" and params = "'.$id.'"');
    }
	function usunKategorie() 
	{
        $this->delete('module = "default" and controller = "oferta" and action = "kategorie"');
    }	
	function usunPodstrony($params)
	{
        $this->delete('controller = "podstrony" and params = "'.$params.'"');
    }
}
?>