<?php
class Partnerzy extends Zend_Db_Table
{
	public $link, $id;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

	function dodaj($dane)
	{
		$dane['lang'] = $this->lang;
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function wypiszAll($typ = 'partnerzy')
	{
		$where = 'typ = "'.$typ.'"';
		$where .= ' and lang = "'.$this->lang.'"';
		$sql = $this->select()->where($where)->order(array('pozycja', 'nazwa'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function usun()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	function wypisz()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function znajdz($nazwa)
	{
		$result = $this->fetchAll('nazwa LIKE "'.$nazwa.'"');
		return $result;
	}
}
?>