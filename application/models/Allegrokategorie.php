<?php
class Allegrokategorie extends Zend_Db_Table
{
    protected $_name = 'Allegrokategorie';
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

    function dodaj($dane)
	{
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }

    function edytuj($dane)
	{
        $where = 'id = "'.$this->id.'"';
        $this->update($dane, $where);
    }
	function edytujID($dane)
	{
        $where = '`cat-id` = "'.$this->id.'"';
        $this->update($dane, $where);
    }
    function usun()
	{
        $where = 'id = "'.$this->id.'"';
        $this->delete($where);
    }
	
	function getOneByID()
	{
        $where = 'id = "'.$this->id.'"';
        $result = $this->fetchRow($where);
        return $result->toArray();
    }	
	function getOneByCatID($id)
	{
        $where = '`cat-id` = "'.$id.'"';
        $result = $this->fetchRow($where);
        return $result->toArray();
    }
	
	function getAll()
	{
		$sql = $this->select()->order('cat-parent', 'cat-position');
        $result = $this->fetchAll($sql);
        $res = $result->toArray();
        return $res;
    }
	function getAllEmptyPaths()
	{
		$sql = $this->select()->order('cat-parent', 'cat-position')->where('path = "" and '.time());
        $result = $this->fetchAll($sql);
        return $result;
    }
	function getAllByName($name)
	{
		$where = '`cat-name` = "'.$name.'"';
        $result = $this->fetchAll($where);
        $res = $result->toArray();
        return $res;
    }
	function getAllByCatID()
	{
		$sql = $this->select()->order('cat-parent', 'cat-position')->where(time());
        $result = $this->fetchAll($sql);
        for($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i]['cat-id']] = $result[$i]->toArray();
		}
		return @$results;
    }
	
	function getChildren($id)
	{
		$sql = $this->select()->where('`cat-parent` = '.$id)->order('cat-position');
        $result = $this->fetchAll($sql);
        $res = $result->toArray();
        return $res;
    }
	function getChildrenOfChild($id = 0)
	{
		//$where = '`cat-parent` in (select `cat-id` from Allegrokategorie where `cat-parent` = '.$id.')';
		$sql = $this->select()->from(array('ak' => 'Allegrokategorie'), array(''))
			->joinleft(array('akk' => 'Allegrokategorie'), 'ak.`cat-id` = akk.`cat-parent`', array('*'))
			->where('ak.`cat-parent` = '.$id)->order(array('cat-parent asc', 'cat-position asc'));
		//echo $sql;
        $result = $this->fetchAll($sql);
        $res = $result->toArray();
        return $res;
    }
	
	function getPath($id, $all = null)
	{
		$path = $id;
		$ile = 1;
		if(@count($all) > 0) $result = @$all[$id];
		else
		{
			$sql = $this->select()->where('`cat-id` = '.$id);
			$result = $this->fetchRow($sql);
		}
        while(@$result['cat-parent'] > 0)
		{
			if(@count($all) > 0) $result = @$all[$result['cat-parent']];
			else
			{
				$sql = $this->select()->where('`cat-id` = '.$result['cat-parent']);
				$result = $this->fetchRow($sql);
			}
			if(count($result) == 0) break;
			$path = @$result['cat-id'].';'.$path;
			$ile++;
		}
		while($ile++ < 6) $path .= ';-1';
		//var_dump($path);
        return $path;
    }
	function getPathNames($id)
	{
		$sql = $this->select()->where('`cat-id` = '.$id);
        $result = $this->fetchRow($sql);
		$names[] = $result['cat-name'];		
        while($result['cat-parent'] > 0)
		{
			$sql = $this->select()->where('`cat-id` = '.$result['cat-parent']);
			$result = $this->fetchRow($sql);
			if(count($result) == 0) break;
			$names[] = $result['cat-name'];
		}
        return $names;
    }
	function getName($path)
	{
		$kats = explode(';', $path);
		for($i = count($kats) - 1; $i >= 0; $i--)
		{
			if(intval($kats[$i]) > 0)
			{
				$kateg = $this->getOneByCatID($kats[$i]);
				return $kateg['cat-name'];
			}
		}
		return null;
    }
	function getKateg($path)
	{
		$kats = explode(';', $path);
		for($i = count($kats) - 1; $i >= 0; $i--)
		{
			if(intval($kats[$i]) > 0)
			{
				return intval($kats[$i]);
			}
		}
		return 0;
    }
	function getPathInfo($path)
	{
		if(empty($path)) return null;
		$where = '`cat-id` in ('.str_replace(';', ',', str_replace(';-1', '', $path)).')';
        $sql = $this->select()->where($where)->order('LENGTH(path)');		
		$result = $this->fetchAll($sql);
        $res = $result->toArray();
        return $res;
	}
	function getKatsInfo($kats)
	{
		if(empty($kats)) return null;
		$where = '`cat-id` in ('.$kats.')';
        $sql = $this->select()->where($where)->order('LENGTH(path)');
		$result = $this->fetchAll($sql);
        $res = $result->toArray();
        return $res;
	}
	
	function getAllegroCount($webapi = null)
	{
		set_time_limit(0);
		try
		{
			if($webapi == null)
			{
				$konto = new Allegrokonta();
				$webapi = $konto->zaloguj();
				if($webapi == null) return $konto->error;
			}
			$count = $webapi->objectToArray($webapi->GetCatsDataCount());
			return @intval($count['cats-count']);
		}
		catch(SoapFault $error)
		{
			return 'B��d '.$error->faultcode.': '.$error->faultstring;
		}
	}

	function updateFromAllegro($webapi = null, $ile = 0, $start = 0, $limit = 0, $odrazu = false)
	{
		set_time_limit(0);
		try
		{
			if($webapi == null)
			{
				$konto = new Allegrokonta();
				$webapi = $konto->zaloguj();
				if($webapi == null) return $konto->error;
			}
			if($ile == 0)
			{
				$kategs = $webapi->objectToArray($webapi->GetCatsData());
				if(count($kategs) > 0)
				{
					$this->delete('1');
					if(@count($kategs['cats-list']) > 0)
					foreach($kategs['cats-list'] as $kateg)
					{
						//var_dump($kateg);
						$this->dodaj($kateg);
					}
					//$this->setPaths();
				}
			}
			else
			{
				$import = new Import();
				$opcje = array('offset' => $start / $ile, 'package-element' => $ile);
				if($limit == 0)	$limit = $this->getAllegroCount($webapi);
				if($limit > 0)
				{
					if($start == 0) $this->delete('1');
					for($od = $start; $od < $limit; $od += $ile)
					{
						$all = $this->getAllByCatID();
						$ilosc = $import->getTabelaCount('allegrokategorie');
						$kategs = $webapi->objectToArray($webapi->GetCatsDataLimit($opcje));
						if(true && @count($kategs) > 0)
						{
							$i = 0;
							if(@count($kategs['cats-list']) > 0)
							foreach($kategs['cats-list'] as $kateg)
							{
								//var_dump($kateg);
								//$all[$kateg['cat-id']] = $kateg;
								//$kateg['path'] = $this->getPath($kateg['cat-id'], $all);
								$this->dodaj($kateg);
								if(++$i % 20 == 0) @file_put_contents('../public/ajax/importkategorie.txt', $ilosc + $i);
							}
							//$this->setPaths();
						}
						//var_dump($kategs);
						$opcje['offset']++;
						if(!$odrazu) break;
					}
				}
			}
		}
		catch(SoapFault $error)
		{
			return 'B��d '.$error->faultcode.': '.$error->faultstring;
		}
	}
	
	function setPaths()
	{
		set_time_limit(0);
		$all = $this->getAllByCatID();
		$katsAll = $this->getAllEmptyPaths();
		if(count($katsAll) > 0)
		foreach($katsAll as $kat)
		{
			if(!empty($kat['path'])) continue;
			$path = $this->getPath($kat['cat-id'], $all);
			$this->id = $kat['cat-id'];
			$this->edytujID(array('path' => $path));
		}
	}
}
?>