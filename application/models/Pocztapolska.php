<?php

class Pocztapolska extends Zend_Db_Table
{
    public $dane, 
           $adres, 
           $nazwa, 
           $nazwa2, 
           $mobile, 
           $paczka, 
           $fileName,
           $nip = NULL,
           $arBuffor = array(),
           $arEnvelope = array(), 
           $typPaczka, $pocztaApi, $error, $konto, $urzadNadania, $globalList = array(), $typy = array();
  
    public function __construct($module = 'admin', $paczkaTable = '')
	{
        parent::__construct();		
        $this->pocztaApi = new ElektronicznyNadawca();        
        $pocztaPolskaKonto = new Pocztapolskakonto();
        $this->konto = $pocztaPolskaKonto->pojedyncza(1);
        $this->urzadNadania = $this->konto['urzad'];
		$this->paczkaTable = !empty($paczkaTable) ? $paczkaTable : Paczka::$table;
		
		$this->typy['paczka'] = 'Paczka pocztowa';
		$this->typy['pobraniowa'] = 'Przesyłka pobraniowa';
		$this->typy['polecona'] = 'Przesyłka polecona';
		$this->typy['nierejestrowana'] = 'Przesyłka nierejestrowana';		
		$this->typy['firmowa_polecona'] = 'Przesyłka firmowa polecona';
		$this->typy['firmowa_nierejestrowana'] = 'Przesyłka firmowa nierejestrowana';
		$this->typy['biznesowa'] = 'Przesyłka biznesowa (Pocztex Kurier48)';
		$this->typy['deklaracja'] = 'Przesyłka listowa z zadeklarowaną wartością';
		$this->typy['pocztex'] = 'Pocztex w obrocie krajowym (Usługa kurierska)';
		//$this->typy['usluga_kurierska'] = 'Usługa kurierska';
    }
	function getTyp($typDef = "")
	{
		$typ = $typDef;
		if($typDef == "firmowa_polecona") $typ = "firmowapolecona";
		if($typDef == "firmowa_nierejestrowana") $typ = "firmowanierejestrowana";
		if($typDef == "biznesowa") $typ = "przesylkakurierska48";
		if($typDef == "pocztex") $typ = "uslugakurierska";
		return $typ;
	}
	function getTypDef($typ = "")
	{
		$typDef = $typ;
		if($typ == "firmowapolecona") $typDef = "firmowa_polecona";
		if($typ == "firmowanierejestrowana") $typDef = "firmowa_nierejestrowana";
		if($typ == "przesylkakurierska48") $typDef = "biznesowa";
		if($typ == "uslugakurierska") $typDef = "pocztex";
		return $typDef;
	}
    
    function updateData($array)
    {
		$where = 'id = 1';
		$this->update($array, $where);
    }	
    function showData()
    {
		$result = $this->fetchAll();		
		return $result;
    }	
    function dodaj($dane)
    {
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
    }
    function edytuj($dane)
    {
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
    }
    function edytujID($id, $dane)
    {
		$where = 'id = "'.$id.'"';
		$this->update($dane, $where);
    }
    function wypiszJeden()
    {
		$result = $this->fetchRow('id = "'.$this->id.'"');
		return $result;
    }
    function wypiszOne($id = 0)
    {
		$sql = $this->select()->where('id = "'.$id.'"');
		$result = $this->fetchRow($sql);
		if($result == null)
		{
			$dane['id'] = $id;
			$this->dodaj($dane);
			$result = $this->fetchRow($sql);
		}
		return $result->toArray();
    }
	
    function getDane($id, $odbiorca, $dane)
	{
        $this->setAddress($odbiorca);
        $typPrzesylka = isset($dane['typ'][$id]) ? $dane['typ'][$id] : null;
        $danetyp = isset($dane[$typPrzesylka][$id])? $dane[$typPrzesylka][$id] : '';
        
        switch($typPrzesylka)
		{
            case 'paczka':
                $this->setDataPaczka($danetyp,$typPrzesylka);
            break;  
            case 'pobraniowa':
                $this->setDataPobraniowa($danetyp,$typPrzesylka);
            break; 
            case 'polecona':
                $this->setDataPolecona($danetyp,$typPrzesylka);
            break; 
            case 'firmowapolecona':
                $this->setDataFirmowaPolecona($danetyp);
            break; 
            case 'firmowanierejestrowana':
                $this->setDataFirmowaNierejestrowana($danetyp);
            break; 
            case 'nierejestrowana':
                $this->setDataNierejestrowana($danetyp, $typPrzesylka);
            break; 
            case 'deklaracja':
                $this->setDataDeklaracja($danetyp, $typPrzesylka);
            break;
            case 'uslugakurierska':
                $this->setDataUslugaKurierska($danetyp, $typPrzesylka);
            break; 
            case 'przesylkakurierska48':
                $this->setPrzesylkaKurierska48($danetyp);
            break; 
        }
		//var_dump($this->dane);die();
        return $this->dane;
    }
    
    function setAddress($dane) {
        $adres = new adresType();
        $this->checkIsCompany($dane);
        //$this->checkIsMobile($dane);
        $adres->nazwa = $this->nazwa;
        $adres->nazwa2 = $this->nazwa2;
        $adres->nip = $this->nip;
        $adres->ulica = $dane['ulica'];
        $adres->numerDomu = $dane['nrDom'];
        $adres->numerLokalu = $dane['nrLokal'];
        $adres->miejscowosc = $dane['miasto'];
        $adres->kodPocztowy = str_replace('-', '', $dane['kod']);
        $adres->kraj = 'Polska';
        $adres->telefon = str_replace(' ', '', $dane['telKontakt']);
        $adres->email = $dane['emailKontakt'];
        //$adres->mobile = $this->mobile;
		$adres->mobile = str_replace(' ', '', $dane['komKontakt']);
		//$this->checkIsMobile($dane);
        $this->adres = $adres;
        $this->dane['adres'] = $this->adres;
    }
    
    function setDataPaczka($dane, $typPrzesylka = 'paczka') {
        $typ = new paczkaPocztowaType();
        if($dane != null) {
            $typ->posteRestante = 0;
            $typ->iloscPotwierdzenOdbioru = isset($dane['dpotwierdzenie']) ? $dane['potwierdzenie'] : 0;
            $typ->kategoria = $dane['typ'];//$typPrzesylka;
            $typ->gabaryt = $dane['gabaryt']; 
            $typ->masa = round(floatval(str_replace(',', '.', $dane['masa'])) * 1000);
            $typ->wartosc = isset($dane['dwartosc']) ? round(floatval(str_replace(',', '.', $dane['wartosc'])) * 100) : '';
            $typ->zwrotDoslanie = isset($dane['zwrot'])? 1:0;
            $typ->egzemplarzBiblioteczny = isset($dane['biblioteka'])? 1:0;
            $typ->dlaOciemnialych = isset($dane['ociemniali'])? 1:0;
            
            if(!empty($dane['opis'])) $typ->opis = $dane['opis'];
        }
        $this->typ = $typ;
        return $this->dane['typ'] = $this->typ;
    }
    
    function setDataPolecona($dane, $typPrzesylka = 'polecona') {
        $typ = new przesylkaPoleconaKrajowaType();
        if($dane != null) {
            $typ->posteRestante = 0;
            $typ->epo = new EPOSimpleType(); // EPOType
            $typ->iloscPotwierdzenOdbioru = isset($dane['dpotwierdzenie']) ? $dane['potwierdzenie'] : 0;
            $typ->kategoria = $dane['typ'];//$typPrzesylka;
            $typ->gabaryt = $dane['gabaryt']; 
            $typ->masa = round(floatval(str_replace(',', '.', $dane['masa'])) * 1000);
            $typ->wartosc = isset($dane['dwartosc']) ? round(floatval(str_replace(',', '.', $dane['wartosc'])) * 100) : '';
            $typ->zwrotDoslanie = isset($dane['zwrot'])? 1:0;
            $typ->egzemplarzBiblioteczny = isset($dane['biblioteka'])? 1:0;
            $typ->dlaOciemnialych = isset($dane['ociemniali'])? 1:0;
            
            if(!empty($dane['opis'])) $typ->opis = $dane['opis'];
        }
        $this->typ = $typ;
        return $this->dane['typ'] = $this->typ;
    }
    
    function setDataPobraniowa($dane, $typPrzesylka = 'pobraniowa') {
        $typ = new przesylkaPobraniowaType();
        $typ1 = new pobranieType();
        if($dane != null) {
            $typ1->sposobPobrania = $dane['sposob'];
            $typ1->kwotaPobrania = round(floatval(str_replace(',', '.', $dane['kwota'])) * 100);
            $typ1->nrb = $dane['rachunek']; // anonymous51
            $typ1->tytulem = $dane['tytul']; // anonymous52
            $typ1->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = isset($dane['sprawdz']) ? 1: 0;
            
            $typ->posteRestante = 0;
            $typ->pobranie = $typ1;
            $typ->iloscPotwierdzenOdbioru = isset($dane['dpotwierdzenie']) ? $dane['potwierdzenie'] : 0;
            $typ->ostroznie = isset($dane['ostroznie']) ? 1 : 0;
            $typ->kategoria = $dane['typ'];//$typPrzesylka;
            $typ->gabaryt = $dane['gabaryt'];
            $typ->wartosc = isset($dane['dwartosc']) ? round(floatval(str_replace(',', '.', $dane['wartosc'])) * 100) : '';
            $typ->masa = round(floatval(str_replace(',', '.', $dane['masa'])) * 1000);
            
            if(!empty($dane['opis'])) $typ->opis = $dane['opis'];

        }
        $this->typ = $typ;
        return $this->dane['typ'] = $this->typ;
    }
    
    function setDataFirmowaPolecona($dane) {
        $typ = new przesylkaFirmowaPoleconaType();
        if($dane != null) {
            $typ->posteRestante = 0;
            $typ->miejscowa  = isset($dane['miejscowa']) ? $dane['miejscowa'] : 1;
            $typ->iloscPotwierdzenOdbioru = isset($dane['dpotwierdzenie']) ? $dane['potwierdzenie'] : 0;
            $typ->masa = round(floatval(str_replace(',', '.', $dane['masa'])) * 1000);
            $typ->egzemplarzBiblioteczny = isset($dane['biblioteka'])? 1:0;
            $typ->dlaOciemnialych = isset($dane['ociemniali'])? 1:0;
            if(!empty($dane['opis'])) $typ->opis = $dane['opis'];

        }
        $this->typ = $typ;
        return $this->dane['typ'] = $this->typ;
    }
    
    function setDataFirmowaNierejestrowana($dane) {
        $typ = new listZwyklyType();
        if($dane != null) {
            $typ->posteRestante = 0;
            $typ->ilosc = 1;
            $typ->masa = round(floatval(str_replace(',', '.', $dane['masa'])) * 1000);
            if(!empty($dane['opis'])) $typ->opis = $dane['opis'];
        }
        $this->typ = $typ;
        return $this->dane['typ'] = $this->typ;
    }
    
    function setDataNierejestrowana($dane, $typPrzesylka='nierejestrowana') {
        $typ = new listZwyklyType();
        if($dane != null) {
            $typ->posteRestante = 0;
            $typ->ilosc = 1;
            $typ->kategoria = $dane['typ'];//$typPrzesylka;
            $typ->gabaryt = $dane['gabaryt'];
            $typ->masa = round(floatval(str_replace(',', '.', $dane['masa'])) * 1000);
            if(!empty($dane['opis'])) $typ->opis = $dane['opis'];
        }
        $this->typ = $typ;
        return $this->dane['typ'] = $this->typ;
    }
    
    function setDataDeklaracja($dane, $typPrzesylka='nierejestrowana') {
        $typ = new przesylkaListowaZadeklarowanaWartoscType();
        if($dane != null) {
            $typ->posteRestante = 0;
            $typ->wartosc = round(floatval(str_replace(',', '.', $dane['wartosc'])) * 100);
            $typ->iloscPotwierdzenOdbioru = isset($dane['dpotwierdzenie']) ? $dane['potwierdzenie'] : 0;
            $typ->kategoria = $dane['typ'];//$typPrzesylka;
            $typ->gabaryt = $dane['gabaryt'];
            $typ->masa = round(floatval(str_replace(',', '.', $dane['masa'])) * 1000);
            $typ->zwrotDoslanie = isset($dane['zwrot'])? 1:0;
            $typ->egzemplarzBiblioteczny = isset($dane['biblioteka'])? 1:0;
            $typ->dlaOciemnialych = isset($dane['ociemniali'])? 1:0;
            if(!empty($dane['opis'])) $typ->opis = $dane['opis'];
        }
        $this->typ = $typ;
        return $this->dane['typ'] = $this->typ;
    }
    
    function setDataUslugaKurierska($dane) {
        $typ = new uslugaKurierskaType();
        if($dane != null) {
            $typ->doreczenie = new doreczenieUslugaKurierskaType();
            $typ->termin = $dane['termin'];
            
            if(!empty($dane['masa'])){
                $typ->masa = new masaType();
                $typ->masa = round(floatval(str_replace(',', '.', $dane['masa'])) * 1000); // masaType
            }
            
            if(!empty($dane['uiszczaOplate'])) {
                switch($dane['uiszczaOplate']){
                    case 'A': $typ->uiszczaOplate = uiszczaOplateType::ADRESAT;
                    break;
                    case 'N': $typ->uiszczaOplate = uiszczaOplateType::NADAWCA;
                    break;
                }
            }
            
            if(!empty($dane['godzdoreczenia'])) $typ->doreczenie->oczekiwanaGodzinaDoreczenia = $dane['godzinaDoreczenia'];

            $typ->zawartosc = $dane['zawartosc'];
            if(!empty($dane['pobranie'])) {
                $typ->pobranie = new pobranieType();
                if(!empty($dane['sposob'])) {
                    $typ->pobranie->sposobPobrania = $dane['sposob'];
                    $liczba = floatval(str_replace(',', '.', $dane['kwota']));
                    $typ->pobranie->kwotaPobrania = round($liczba * 100); 

                    $typ->pobranie->nrb = $dane['rachunek'];
                    $typ->pobranie->tytulem = $dane['tytul'];
                }
            }
            
            if(!empty($dane['wartosc'])) $typ->wartosc = round(floatval(str_replace(',', '.', $dane['wartosc'])) * 100);

            if(!empty($dane['potwierdzenieOdbioru'])) {
                $typ->potwierdzenieOdbioru = new potwierdzenieOdbioruKurierskaType();
                $typ->potwierdzenieOdbioru->sposob = $dane['potwierdzenieOdbioru'];
                $typ->potwierdzenieOdbioru->ilosc = $dane['potwierdzenieOdbioruIlosc'];
            }
            if(!empty($dane['potwierdzenieDoreczenia'])) {
                $typ->potwierdzenieDoreczenia = new potwierdzenieDoreczeniaType();
                $typ->potwierdzenieDoreczenia->sposob = $dane['potwierdzenieDoreczenia'];
                $typ->potwierdzenieDoreczenia->kontakt = $dane['potwierdzenieDoreczeniaKontakt'];
            }
            
            $typ->ostroznie = !empty($dane['ostroznie'])? 1 : 0;
            $typ->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = !empty($dane['sprZawrPrzezOdbiorce'])? 1 : 0;
            
            if(!empty($dane['doreczenieWeWskaznymDniu'])) $typ->doreczenie->oczekiwanyTerminDoreczenia = $dane['doreczenieWeWskaznymDniuData'];
            
            $typ->doRakWlasnych = !empty($dane['doRakWlasnych'])? 1 : 0;
            if(!empty($dane['wSobote'])) {
                $typ->doreczenie->wSobote = '1';
                $typ->doreczenie->oczekiwanaGodzinaDoreczenia = null;
            }
            
            if(!empty($dane['odbiorPrzesylkiOdNadawcyType'])) {
                $typ->odbiorPrzesylkiOdNadawcy = new odbiorPrzesylkiOdNadawcyType();
                $typ->odbiorPrzesylkiOdNadawcy->wSobote = '1';
            }
            
            if(!empty($dane['ubezpieczenie'])) {
                $typ->ubezpieczenie = new ubezpieczenieType();
                $typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
                $typ->ubezpieczenie->kwota = $dane['ubezpieczenieKwota'];
            }
            
            $typ = $this->setZwrotDokumentowKurier($dane, $typ);
            
            if(!empty($dane['numerPrzesylkiKlienta'])) $typ->numerPrzesylkiKlienta = $dane['numerPrzesylkiKlienta'];
            if(!empty($dane['opis'])) $typ->opis = $dane['opis'];
        }
        $this->typ = $typ;
        return $this->dane['typ'] = $this->typ;
    }
    
    function setPrzesylkaKurierska48($dane) {
        $typ = new przesylkaBiznesowaType();
        if($dane != null) {
            if(!empty($dane['pobranie'])) {
                $typ->pobranie = new pobranieType();
                if(!empty($dane['sposob'])) {
                    $typ->pobranie->sposobPobrania = $dane['sposob'];
                    $kwota = floatval(str_replace(',', '.', $dane['kwota']));
                    $typ->pobranie->kwotaPobrania = round($kwota * 100); // wartoscType

                    $typ->pobranie->nrb = $dane['rachunek'];
                    $typ->pobranie->tytulem = $dane['tytul'];
                }
            }
            if(!empty($dane['gabaryt'])) {
                switch($dane['gabaryt']) {
                    case 'XXL': $typ->gabaryt = gabarytBiznesowaType::XXL;
                    break;
                }
            }
            if(!empty($dane['wartosc']) && !empty($dane['masa'])) {
                $wartosc = floatval(str_replace(',', '.', $dane['wartosc']));
                $typ->wartosc = round($wartosc * 100); // wartoscType
                $typ->masa = round(floatval(str_replace(',', '.', $dane['masa'])) * 1000); // masaType
            }
            $typ->ostroznie = !empty($dane['ostroznie']) ? 1 : 0;
            if(!empty($dane['ubezpieczenie'])) {
                $typ->ubezpieczenie = new ubezpieczenieType();
                $typ->ubezpieczenie->rodzaj = rodzajUbezpieczeniaType::STANDARD;
                $typ->ubezpieczenie->kwota = $dane['ubezpieczenieKwota'];
            }
            if(!empty($dane['opis'])) {
                $typ->opis = $dane['opis'];
            }
			
			$urzadWydaniaEPrzesylki = @intval($dane['urzadWydaniaEPrzesylki']);
			if($urzadWydaniaEPrzesylki > 0)
			{
				$typ->urzadWydaniaEPrzesylki = new urzadWydaniaEPrzesylkiType();
				$typ->urzadWydaniaEPrzesylki->id = $urzadWydaniaEPrzesylki; 
				unset($typ->subPrzesylka);
			}
			//var_dump($typ);die();
        }
        $this->typ = $typ;
        return $this->dane['typ'] = $this->typ;
    }
    
    function setZwrotDokumentowKurier($dane, $typ){
        if(!empty($dane['dokZwrotDiv'])) {
            $typ->zwrotDokumentow = new zwrotDokumentowKurierskaType();
            $typ->zwrotDokumentow->rodzajList = new rodzajListType();
            switch($dane['zwrotDokumentow']) {
                case 'LZP':
                    $typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
                break;
                case 'LZE':
                    $typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
                break;
                case 'LPP':
                    $typ->zwrotDokumentow->rodzajList->polecony = '1';
                    $typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::PRIORYTETOWA;
                break;
                case 'LPE':
                    $typ->zwrotDokumentow->rodzajList->polecony = '1';
                    $typ->zwrotDokumentow->rodzajList->kategoria = kategoriaType::EKONOMICZNA;
                break;
                case 'P24':
                    $typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_24;
                break;
                case 'P48':
                    $typ->zwrotDokumentow->rodzajPaczka = terminZwrotDokumentowPaczkowaType::PACZKA_48;
                break;
                case 'KE24':
                    $typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::EKSPRES24;
                break;
                case 'KMD3GD5KM':
                    $typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_5KM;
                break;
                case 'KMD3GD10KM':
                    $typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_10KM;
                break;
                case 'KMD3GD15KM':
                    $typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_DO_15KM;
                break;
                case 'KMD3GP15KM':
                    $typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::MIEJSKI_DO_3H_POWYZEJ_15KM;
                break;
                case 'KBD20KG':
                    $typ->zwrotDokumentow->rodzajPocztex = terminZwrotDokumentowKurierskaType::BEZPOSREDNI_DO_20KG;
                break;
            }
        }
        return $typ;
    }
    
    function addShipment($buffor) {
        try {
            $guid = new getGuid();
            $guid->ilosc = 1;
            $gui = $this->pocztaApi->getGuid($guid);
            $this->typ->guid = $gui['guid'];
            $this->typ->adres = $this->adres;
            $add = new addShipment();
            $add->przesylki[] = $this->typ;
            $add->idBufor = $buffor;
            $soap = $this->pocztaApi->addShipment($add);
            
            return $this->getDataFromSoap($soap);
        } catch (SoapFault $error) {
            $this->paczka->error[$this->dostawca] = 'Nie udało się dodać przesyłek do elektronicznego nadawcy poczty.';
        }
    }
    
    function getDataFromSoap($dane) {
        $error['error'] = NULL;
        if(empty($dane['retval']['error'])) {
            return array(
             'guid'=>$dane['retval']['guid'],
             'numerPrzesylka' => $dane['retval']['numerNadania'],
             'error' => NULL
            );
        }
        else {
            if (isset($dane['retval']['error']['0'])) {
                for ($i = 0; $i < count($dane['retval']['error']); $i++) {
                    $error['error'] .= '<br />Błąd numer: ' . $dane['retval']['error'][$i]['errorNumber'] . ': ' . $dane['retval']['error'][$i]['errorDesc'];
                }
                return $error;
            } else {
                $error['error'] .= '<br />Błąd numer: ' . $dane['retval']['error']['errorNumber'] . ': ' . $dane['retval']['error']['errorDesc'];
                return $error;
            }
        }
    }
    
    function getDataFromSoapBuffor($dane) {
        $error['error'] = NULL;
        
        if(!isset($dane['error']) || empty($dane['error'])) {
            return $dane;
        }
        else {
            if (isset($dane['error']['0'])) {
                $this->paczka->error[$this->dostawca] = $error['error'] .='Błąd danych buffora. Przesyłka nie została zatwierdzona.';
                for ($i = 0; $i < count($dane['retval']['error']); $i++) {
                    $this->paczka->error[$this->dostawca] = $error['error'] .= '<br />Błąd numer: ' . $dane['error'][$i]['errorNumber'] . ': ' . $dane['error'][$i]['errorDesc'];
                }
                return $error;
            } else {
                $this->paczka->error[$this->dostawca] = $error['error'] .= '<br />Błąd numer: ' . $dane['error']['errorNumber'] . ': ' . $dane['error']['errorDesc'];
                return $error;
            }
        }
    }
    
    function createBuffor() {
     //   $urzadNadania = $this->getUrzedyNadania();
        if($this->urzadNadania !=0) {
            try {
                $buforType = new buforType();
                $buforType->dataNadania = date('Y-m-d');
                $buforType->active = '1';
                $buforType->urzadNadania = $this->urzadNadania;
                $createBufor = new createEnvelopeBufor();
                $createBufor->bufor = $buforType;
                $newBufor = $this->pocztaApi->createEnvelopeBufor($createBufor);
				if(@count($newBufor['error']) > 0)
				{
					$this->paczka->error[$this->dostawca] = 'Błąd bufora:';
					foreach($newBufor['error'] as $error)
					$this->paczka->error[$this->dostawca].= '<br/>'.$error['errorNumber'].': '.$error['errorDesc'].'';
				}
				else
				{
					$zmiany['data_nowego_zbioru'] = $newBufor['createdBufor']['dataNadania'];
					$zmiany['buffor'] = $newBufor['createdBufor']['idBufor'];
				}
				//var_dump($this->urzadNadania);die();
                
                return $zmiany;
                
            } catch(SoapFault $e) {
                $this->paczka->error[$this->dostawca] = 'Nie udało się stworzyć buffora przesyłek.<br />Sprawdź dane logowania do poczty.';
            }
        } 
        else {
            $this->paczka->error[$this->dostawca] = 'Nie udało się dodać przesyłek do elektronicznego nadawcy poczty. Nie wybrano urzędu nadania. <br />Sprawdź ustawienia.';
        }
    }
    
    function sendBuffor($buffor = 0) {
        try {
            $soap = new sendEnvelope();
            $soap->urzadNadania = $this->urzadNadania;
            $soap->idBufor = $buffor['buffor'];
            $response = $this->pocztaApi->sendEnvelope($soap);
            $data = $this->getDataFromSoapBuffor($response);
            $this->paczka->id = $this->paczka->przesylkiDodane[$this->paczka->dostawca];
            $this->paczka->_update($data);
        }
        catch(SoapFault $e) {
            $this->paczka->error[$this->dostawca] = 'Nie udało się dodać przesyłek do elektronicznego nadawcy poczty. Błąd pobrania urzędu nadania. <br />Sprawdź dane logowania.';
        }
    }
    
    function getUrzedyNadania() {
        $getUrzedyNadania = new getUrzedyNadania();
        $UrzedyNadania = $this->pocztaApi->getUrzedyNadania($getUrzedyNadania);
        return isset($UrzedyNadania['urzedyNadania']['urzadNadania']) ? $UrzedyNadania['urzedyNadania']['urzadNadania'] : null;
    }
            
        
    function checkIsCompany($dane) {
        if(isset($dane['czyFirma']) && $dane['czyFirma'] == 'on') {
            $this->nazwa = $dane['nazwa'];
            $this->nazwa2 = $dane['imie'].' '.$dane['nazwisko'];
            $this->nip = $dane['nip'];
        }
        else {
            $this->nazwa = $dane['imie'].' '.$dane['nazwisko'];
            $this->nazwa2 = '';
        }
    }
    
    function sortEnvelope($envelope) {
        $ar = array();
        if(!empty($envelope)) {
            foreach($envelope as $e) {
                $ar[] = $e;
            }
        }
        return $envelope;
    }
    
    function getDataSoapPrint($dane) {
        if(empty($dane['error'])) {
            return $dane;
        }
        else {
            if (!empty($dane['error'])) {
                $this->error .= $dane['error'].'<br />';
            }
        }
    }
    
    function createPdf($dane, $typ) {
        $paczka = new Paczka($this->paczkaTable);
		//var_dump($dane['content']);die();
        if(isset($dane['content'][0])) {
            $it = 0;
            foreach($dane['content'] as $d) {
                $content = $d['pdfContent'];
                $paczka->id =  $this->ar2[$d['guid']];
                $this->symbol = $d['guid'];//$paczka->id;
                $dane = $this->savePdf($content, $typ);
                $this->globalList[] = $this->fileName;
                $paczka->edytuj($dane);
                $it++;
            }
        }
        elseif($dane['content']) {
            $content = $dane['content']['pdfContent'];
            $paczka->id =  $this->ar2[$dane['content']['guid']];
            $this->symbol = $dane['content']['guid'];//nrNadania;
            $dane = $this->savePdf($content, $typ);
            $paczka->edytuj($dane);
            $this->globalList[] = $this->fileName;
        }
    }
    
    function savePdf($content, $typ) {
        $fileName = $typ.'_pocztapolska_'.$this->symbol.'.pdf';
        $file = fopen('./admin/pdf/'.$fileName, 'w');
        fwrite($file, $content);
        fclose($file);
        
        $dokument['wydruk_'.$typ] = $this->fileName = $fileName;
        return $dokument;
    }
        
    
    function getDokument($nrGuid, $typ='etykieta') {
        $envelope = $this->sortEnvelope($this->arEnvelope);
        if(is_array($envelope)) $envelope = array_unique($envelope);
        
        foreach($envelope as $envelope) {
             $type = new getAddressLabel();
             $type->idEnvelope = $envelope;
			 try
			 {
				$soap = $this->pocztaApi->getAddressLabel($type);
			 }
			 catch (SoapFault $error)
			 {
				 $soap = array('error' => $error->getMessage());
			 }
             $response = $this->getDataSoapPrint($soap);
             $this->createPdf($response, $typ);
        }
    }
    
    function checkIsMobile($dane) {
        $mobile = str_replace(' ','',$dane['telKontakt']);
        if(strlen($mobile) == 9) {
            $this->mobile = $mobile;
        }
        else {
            $this->mobile = '';
        }
    }
}
?>