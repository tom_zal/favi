<?php
class Producent extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = 'pl';//$this->common->getJezyk($module);
    }
		
	function dodajProducenta($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytujProducenta($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function wypiszProducentow()
	{
		$sql = $this->select()->where('lang = "'.$this->lang.'"')->order('nazwa');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function getProducenci($pole = 'id')
	{
		$sql = $this->select()->where('lang = "'.$this->lang.'"')->order('nazwa');
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i][$pole]] = $result[$i]->toArray();
		}
		return @$results;
	}
	function wypiszProducentowWhereNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa like "%'.$nazwa.'%"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszProducentaNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa = "'.$nazwa.'"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	
	function wypiszProducentowForProdukt($produkt)
	{
		$where = 'id = '.$produkt;	 
		$result = $this->fetchAll($where);
		return $result;		
	}
	
	function wypiszProducentowForProduktAll()
	{
		$result = $this->fetchAll();		
		for($i=0; $i<count($result); $i++)
		{
			$producent[$result[$i]->id] = $result[$i]->nazwa;		
		}		
		return $producent;		
	}
	
	function usunProducenta()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	function wypiszPojedynczego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	
	function wypiszProducentowDlaKategorii($kateg = 0)
	{
		$select = $this->db->select()->from(array('p' => 'Produkty'), array(''))
			->join(array('pp' => 'Producent'), 'p.producent = pp.id', array('*'))
			->joinleft(array('kp' => 'Katprod'), 'p.id = kp.id_prod', array(''))
			->where('p.widoczny = 1 and kp.id_kat = '.$kateg)->group('pp.id')->order('pp.nazwa asc');
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszProduktyDlaKategorii($kateg = 0)
	{
		$select = $this->db->select()->from(array('p' => 'Produkty'), 
			array('group_concat(distinct p.id separator ",") as ids'))
			->join(array('pp' => 'Producent'), 'p.producent = pp.id', array(''))
			->joinleft(array('kp' => 'Katprod'), 'p.id = kp.id_prod', array(''))
			->where('kp.id_kat = '.$kateg)->order('p.nazwa asc');
		//echo $select;
		$result = $this->db->fetchRow($select);
		return @strval($result['ids']);
	}
        function wypiszProducentowForProduktCount(){
            $sql = 'SELECT r.id, r.nazwa, COUNT(p.producent) AS ile FROM Produkty AS p LEFT JOIN Producent AS r ON p.producent = r.id GROUP BY p.producent';
            $result = $this->db->fetchAll($sql);
//            dump($result);die;
            return $result;
        }
}
?>