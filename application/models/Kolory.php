<?php
class Kolory extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodajKolory($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytujKolory($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function wypiszKolory()
	{
		$sql = $this->select()->where('lang = "'.$this->lang.'"')->order(array('pozycja','nazwa'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function usunKolory()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	function wypiszKolor()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function znajdzKolor($kolor)
	{
		$result = $this->fetchAll('nazwa LIKE "'.$kolor.'"');
		return $result;
	}
}
?>