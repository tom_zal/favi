<?php
class Kontrahencilogin extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{
		if(@empty($dane['typ'])) $dane['typ'] = 'kontrahenci';
		//var_dump($dane);die();
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	
	function wypisz()
	{
		$result = $this->fetchAll();
		return $result;
	}
	
	function odswiez($user = 0, $typ = 'kontrahenci')
	{
		$logon = $this->wypiszForLastLogon($user, $typ);
		//var_dump($logon);//die();
		$obecny = $this->wypiszForLastObecni($user, @strtotime($logon[0]['zalogowano']), $typ);
		//var_dump($obecny);die();
		if(count($obecny) > 0)
		{
			$this->update(array('odswiezono' => date('Y-m-d H:i:s')), 'id = '.@intval($obecny[0]['id_kl']));
		}
		else
		{
			$this->dodaj(array('typ' => $typ, 'id_kontr' => $user, 
				'zalogowano' => '0000-00-00 00:00:00', 'odswiezono' => date('Y-m-d H:i:s')));
		}
	}
	
	function wypiszForKontrahent($kontr = 0, $ile = 9999, $typ = 'kontrahenci')
	{
		$where = 'id_kontr = '.$kontr.' and typ = "'.$typ.'" and zalogowano <> "0000-00-00 00:00:00"';
		$sql = $this->select()->where($where)->order('zalogowano desc')->limit($ile);
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForLastLogon($user = 0, $typ = 'kontrahenci')
	{
		$where = 'zalogowano != "0000-00-00 00:00:00"';
		if($user > 0) $where .= ' and k.id = '.$user;
		$sql = $this->db->select()
			->from(array('l' => 'Kontrahencilogin'), array('*', 'id as id_kl', 'max(zalogowano) as zalogowano'))
			->join(array('k' => ucfirst($typ)), 'k.id = l.id_kontr', array('*'))
			->where($where)->group('k.id')->order('max(zalogowano) desc');
		//echo $sql;
		$result = $this->db->fetchAll($sql);
		return $result;
	}
	function wypiszForLastObecni($user = 0, $time = 0, $typ = 'kontrahenci')
	{
		$where = '1';
		if($time == 0) $time = time() - 10 * 60;
		if($user > 0) $where .= ' and k.id = '.$user;
		$where .= ' and odswiezono >= "'.date('Y-m-d H:i:s', $time).'"';
		$having = '1';
		//$having = 'max(odswiezono) >= "'.date('Y-m-d H:i:s', $time).'"';
		$sql = $this->db->select()
			->from(array('l' => 'Kontrahencilogin'), array('*', 'id as id_kl', 'max(odswiezono) as odswiezono'))
			->join(array('k' => ucfirst($typ)), 'k.id = l.id_kontr', array('*'))
			->where($where)->group('k.id')->having($having)->order('max(odswiezono) desc');
		//echo $sql;//die();
		$result = $this->db->fetchAll($sql);
		return $result;
	}
	
	function usun()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	
	function wypiszPojedynczego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
}
?>