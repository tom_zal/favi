<?php
class Realizacje extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
    function wypiszGlowna($lang="pl")
	{
        $where = 'glowna = "on" AND lang = "'.$lang.'"';
        $order = array('pozycja asc', 'data DESC');
        $result = $this->fetchAll($where, $order);
        return $result;
    }
    function wypiszDoNewslettera($lang="pl")
	{
        $where = 'status = 0 AND lang = "'.$lang.'"';
        $order = array('pozycja asc', 'data DESC');
        $result = $this->fetchAll($where, $order);
        return $result;
    }
    function wypisz()
	{
        $result = $this->fetchAll();
        return $result;
    }
    function dodaj($dane, $lang="pl")
	{
        $dane['lang'] = $lang;
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }
    function edytuj($dane, $id)
	{
        $where = 'id = '.$id;
        $this->update($dane, $where);
    }
    function pojedyncza($id)
	{
        $where = 'id = '.$id;
        $print = $this->fetchRow($where);
        return $print;
    }
    function wypiszAktualnosciAdmin($lang="pl",$typ=1)
	{
        $where = 'typ = '.$typ.' AND lang = "'.$lang.'"';
		$order = array('pozycja asc', 'data DESC');
        $result = $this->fetchAll($where, $order);
        return $result;
    }
    function wypiszAktualnosci($lang="pl",$typ=1,$id=0,$ile=9999,$glowna=false)
	{
        $where = 'r.typ = '.$typ.' AND r.lang = "'.$this->lang.'" and r.wyswietl = "1"';
		if($id > 0) $where .= ' and r.id <> '.$id;
		if($glowna) $where .= ' and r.glowna = "1"';
		$order = array('r.pozycja asc', 'r.data DESC');
		$sql = $this->db->select()->from(array('r' => 'Realizacje'), array('r.*'));
		if($this->obConfig->podstronyGaleria)
		{
			$whereGal = 'r.id = g.wlasciciel and g.typ = "realizacje" and g.glowne = "T"';
			$whereGal.= ' and g.lang = "'.$this->lang.'" and g.wyswietl = "1"';
			$sql->joinleft(array('g' => 'Galeria'), $whereGal ,array('img'));
		}
		$sql->where($where)->order($order)->limit($ile);
		//echo $sql;die();
		$result = $this->db->fetchAll($sql);
		//var_dump($result);die();
        return $result;
    }
    function wypiszAktualnosciGlowna($lang='pl',$typ=1,$ile=9999)
	{
        $where = 'typ = '.$typ.' AND lang = "'.$lang.'" AND glowna AND wyswietl = "1"';
        $order = array('pozycja asc', 'data DESC');
        $sql = $this->select()->where($where)->order($order)->limit($ile);
		$result = $this->fetchAll($sql);
        return $result;
    }	
	function wypiszAktualnosciNajnowsza($lang="pl",$typ=1)
	{
        $where = 'typ = '.$typ.' AND lang = "'.$lang.'"';
		$order = array('pozycja asc', 'data DESC');
        $result = $this->fetchRow($where, $order, 1);
        return $result;
    }
}
?>