<?php
class Wagi extends Zend_Db_Table
{
	public $link, $id, $typ;
		
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$sql = $this->select()->where('1')->order(array('od', 'do'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszWaga($waga = 0)
	{
		$sql = $this->select()->where('od <= '.$waga.' and do >= '.$waga)->order(array('od', 'do'));
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszJeden($nazwa)
	{
		$sql = $this->select()->where('nazwa = "'.$nazwa.'"')->order('od');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszWagiDlaDostawy($dostawa = 0)
	{
		if($this->obConfig->dostawyOdWagi)
		$select = $this->db->select()->from(array('w' => 'Wagi'), array('*'))
		->join(array('wd' => 'WagiDostawy'), 'w.id = wd.id_waga and wd.id_dostawa = '.$dostawa, array(''))
		->joinleft(array('wkd'=>'Wagikosztdostawy'),'w.id=wkd.id_waga and wkd.id_dostawa='.$dostawa,array('koszt'))
		->where('1');
		else
		$select = $this->db->select()->from(array('w' => 'Wagi'), array('*'))
		->joinleft(array('wkd'=>'Wagikosztdostawy'),'w.id=wkd.id_waga and wkd.id_dostawa='.$dostawa,array('koszt'))
		->where('1');
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result; 
	}
	function WypiszDostawyDlaWagi($waga = 0)
	{
		$select = $this->db->select()->from(array('wd' => 'WagiDostawy'), array(''))
		->join(array('sd'=>'Sposobdostawy'),'wd.id_dostawa=sd.id',array('id','nazwa'))
		->where('wd.id_waga = '.$waga);
		//echo $select;
		$result = $this->db->fetchAll($select);
		if(count($result) > 0)
		foreach($result as $row)
		{
			$results[$row['id']] = $row;
		}
		return @$results; 
	} 
	function usun()
	{
		$result = $this->delete('id = '.$this->id);			
	}		
	function wypiszID()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function wypiszMaxWaga()
	{
		$select = $this->db->select()->from(array('w' => 'Wagi'), array('max(do)'))->where(1);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return @floatval($result[0]['max(do)']); 
	}
}
?>