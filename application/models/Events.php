<?php
class Events extends Zend_Db_Table
{
	public $id, $column, $keywords, $rowset = 99999, $page = 0, $_name = 'Zdarzenia';

	function addAdmin($data)
	{
		$this->insert($data);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function editAdmin($data)
	{
		$this->update($data, 'id="'.$this->id.'"');
	}
	function deleteAdmin()
	{
		$this->delete('id = "'.$this->id.'"');
	}	
	function _delete()
	{
		$this->delete('id = "'.$this->id.'"');
	}
	function rowAdmin()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	
	function wypiszAutorow() 
	{
		$sql = $this->select()->distinct()->from(array('z' => 'Zdarzenia'), 'uzytkownik')->where('uzytkownik <> ""')->order('uzytkownik asc');
		$result = $this->fetchAll($sql);
		return $result;
	}

	function rowsAdmin($szukanie = null)
	{
		$where = 'id <> 0 and '.time();
		if(@$szukanie->prodID > 0)
		{
			$prodID = $szukanie->prodID;
			$where .= ' and (0';
			$where .= ' or ((action = "oferta" or controller = "index") and idk = "'.$prodID.'")';
			$where .= ' or (controller = "oferta" and ';
			$where .= '(gdzie = "'.$prodID.'" or gdzie like "'.$prodID.',%" or gdzie like "%,'.$prodID.'" or gdzie like "%,'.$prodID.',%"';
			$where .= ' or gdzie like "'.$prodID.' %" or gdzie like "% '.$prodID.'" or gdzie like "% '.$prodID.' %"))';
			$where .= ' or 0)';
		}
		if(@!empty($szukanie->jezyk)) $where .= ' and lang = "'.$szukanie->jezyk.'"';
		//if(@!empty($szukanie->domena)) $where .= ' and domena = "'.$szukanie->domena.'"';
		if(@!empty($szukanie->dataOd)) $where .= ' and data >= "'.$szukanie->dataOd.' 00:00:00"';
		if(@!empty($szukanie->dataDo)) $where .= ' and data <= "'.$szukanie->dataDo.' 23:59:59"';
		if(@!empty($szukanie->autor)) $where .= ' and uzytkownik = "'.$szukanie->autor.'"';
		if(@!empty($szukanie->adres_ip)) $where .= ' and adres_ip like "%'.$szukanie->adres_ip.'%"';
		if(@!empty($szukanie->adres_mac)) $where .= ' and adres_mac like "%'.$szukanie->adres_mac.'%"';
		if(@!empty($szukanie->user_agent)) $where .= ' and user_agent like "%'.$szukanie->user_agent.'%"';
		
		$select = $this->select()
					   ->where($where)
					   ->limit($this->rowset, $this->page)
					   ->order('data DESC');
		//echo $select;
		$result = $this->fetchAll($select)->toArray();
		return $result;
	}

	function pagerAdmin($szukanie = null)
	{
		$where = 'id <> 0 and '.time();
		if(@$szukanie->prodID > 0)
		{
			$prodID = $szukanie->prodID;
			$where .= ' and (0';
			$where .= ' or ((action = "oferta" or controller = "index") and idk = "'.$prodID.'")';
			$where .= ' or (controller = "oferta" and ';
			$where .= '(gdzie = "'.$prodID.'" or gdzie like "'.$prodID.',%" or gdzie like "%,'.$prodID.'" or gdzie like "%,'.$prodID.',%"))';
			$where .= ' or 0)';
		}
		if(@!empty($szukanie->jezyk)) $where .= ' and lang = "'.$szukanie->jezyk.'"';
		//if(@!empty($szukanie->domena)) $where .= ' and domena = "'.$szukanie->domena.'"';
		if(@!empty($szukanie->dataOd)) $where .= ' and data >= "'.$szukanie->dataOd.' 00:00:00"';
		if(@!empty($szukanie->dataDo)) $where .= ' and data <= "'.$szukanie->dataDo.' 23:59:59"';
		if(@!empty($szukanie->autor)) $where .= ' and uzytkownik = "'.$szukanie->autor.'"';
		if(@!empty($szukanie->adres_ip)) $where .= ' and adres_ip like "%'.$szukanie->adres_ip.'%"';
		if(@!empty($szukanie->adres_mac)) $where .= ' and adres_mac like "%'.$szukanie->adres_mac.'%"';
		if(@!empty($szukanie->user_agent)) $where .= ' and user_agent like "%'.$szukanie->user_agent.'%"';
		
		$sql = $this->select()
					->where($where)
					->from($this->_name, array('num' => 'COUNT(id)'))
					->order('data DESC');
		$result = $this->fetchRow($sql);

		$rows = ceil($result['num'] / $this->rowset);
		return $rows;
	}
	
	static function log($uzytkownik='', $status, $idk='', $akcja='', $controller='', $param='', $nazwa='', $val='', $module='admin', $dane='')
	{
		$ob = new Events();
		
		$common = new Common(false, $module);
		$obConfig = $common->getObConfig();
		if(@!$obConfig->zdarzeniauser) return null;
		
		$stat = Events::statusyZdarzen($status);
		
		switch($status)
		{
			case 'hurtzmienkategorie': $stat .= ' na id#'.$val; break;
			case 'hurtpowiel': $stat .= ' - język: '.$val; break;
			case 'usunzdjecieprod': $stat .= ' '.$nazwa.' - zdjęcie id#'.$val; break;
		}
		
		$data['co'] = $stat;
		$data['idk'] = $idk;
		
		if(!empty($nazwa))
		{
			$data['co'] = sprintf($stat, $nazwa);
		}
		else
		{
			if($idk != '' && is_numeric($idk))
			{
				$data['co'] = sprintf($stat, $idk);
			}
			elseif($idk != null)
			{
				$data['idk'] = $idk;
			}
		}
		
		if($status == 'hurtzmienprodukty')
		{
			$idk = $val;
			$jak = $val ? 'włączenie' : 'wyłączenie';				
			switch($param)
			{
				case 'widoczny': $co = $jak.' widoczności';	break;
				case 'jedn_miary_zb_czy': $co = $jak.' opakowań zbiorczych';	break;
				case 'jedn_miary_zb_typ': $co = $jak.' typu opakowań zbiorczych'; break;
				case 'negocjuj_cene': $co = $jak.' pola "negocjuj cene"'; break;
				case 'producent': $co = 'zmiana producenta na id#'.$val; break;
				case 'grupa': $co = 'zmiana grupy na id#'.$val; break;
				case 'rabat': $co = 'zmiana rabatu na '.$val; break;
				case 'ceny': $co = 'zmiana ceny na '.$val; break;
				case 'tekst': $co = 'zmiana opisu na: '.mb_substr(strip_tags($val), 0, 100, "UTF-8").'...'; $idk = ""; break;
			}
			if(!isset($co))
			foreach($common->oznaczenia as $oznacz => $oznaczenie)
			if($param == $oznacz.$suffix)
			{
				$co = $jak.' '.$oznaczenie['nazwa'];
				break;
			}
			$data['idk'] = $idk;
			if(isset($co)) $data['co'] .= ' - '.$co;
		}
		
		//$data['domena'] = $_SESSION['domena'];
		$data['lang'] = $common->lang;
		$data['data'] = date('Y-m-d H:i:s');
		$data['action'] = $akcja;
		$data['controller'] = $controller;
		$data['param'] = $param;
		$data['gdzie'] = $nazwa;
		$data['uzytkownik'] = $uzytkownik;
		$data['dane'] = $dane;
		$data['adres_ip'] = $_SERVER["REMOTE_ADDR"];
		$data['user_agent'] = $_SERVER["HTTP_USER_AGENT"];
		$data['adres_mac'] = MacAddress::getCurrentMacAddress(PHP_OS);
		//var_dump($data);die();			
		$ob->addAdmin($data);
		
		if(false)
		if($data['action'] == "zmianawidoczny" || $data['action'] == "zmianasztuk" || $data['action'] == "zamowienieok" 
		|| strpos($data['gdzie'], "iloś") > 0 || strpos($data['gdzie'], "widocznoś") > 0)
		{
			$wiadomosci = new Wiadomosci();
			$wiadomosci->powiadomZdarzenie($data);
		}
	}
	
	static function statusyZdarzen($ar)
	{
		$array = array
		(
			'statuszamowienia'=> 'Zmieniono status zamówienia <b>id #%s</b>',
			'usunzamowienia'=> 'Usunięto zamówienie <b>id#%s</b>',
			'podstronaedytuj'=> 'Edytowano podstronę <b>%s</b>',
			'banerusun'=> 'Usunięto slajd <b>id#%s</b>',
			'baneredytuj'=> 'Edytowano slajd <b>id#%s</b>',
			'banerdodaj'=> 'Dodano slajd',
			'newsletterwyslano' => 'Wysłano newsletter',
			'newsletterusunieto' => 'Usunięto adres z newslettera',
			'ustawieniaedytuj' => 'Ustawienia panelu zostały zmienione',
			'haslozmien' => 'Użytkownik zmienił hasło',
			'emailustaw' => 'Konto e-mail zostało zmienione.',
			'pinterest' => 'Pinterest został ustawiony',
			'facebook' => 'Facebook został ustawiony',
			'portalespolecznosciowe' => 'Portale społecznościowe zostały ustawione',
			'wiadomosci' => 'Wiadomości zostały zmienione',
			'klientdodany' => 'Klient został dodany',
			'klientedytuj' => 'Edytowano klienta',
			'klientusun' => 'Usunięto klienta <b>id#%s</b>',
			'dodanoprodukt' => 'Dodano produkt <b>id#%s</b>',
			'edytujprodukt' => 'Edytowano produkt <b>%s</b>',
			'edytujproduktdostwid' => 'Zmiana ilości / widoczności z edycji produktu',
			'usunprodukt' => 'Usunięto produkt <b>id#%s</b>',
			'hurtusunprodukty' => 'Usunięto produkty z operacji hurtowych',
			'kontoallegro' => 'Edytowano konto allegro',
			'ustawieniaallegro' => 'Edytowano ustawienia allegro',
			'cennikiallegro' => 'Dodano cenniki dostawy allegro',
			'cennikiallegroedytuj' => 'Edytowano cennik dostawy allegro',
			'cennikiallegrousun' => 'Usunięto cennik dostawy allegro',
			'dodanoproducent' => 'Dodano producenta',
			'edytujproducent' => 'Edytowano producenta',
			'usunproducent' => 'Usunięto producenta #id%s', 
			'dodajrozmiar' => 'Dodano rozmiarówkę',
			'edytujrozmiar' => 'Edytowano rozmiarówkę',
			'usunrozmiar' => 'Usunięto rozmiarówkę #id%s',
			'dodanokolor' => 'Dodano kolor',
			'edytujkolor' => 'Edytowano kolor',
			'usunkolor' => 'Usunięto kolor #id%s',
			'dodanokategoria' => 'Dodano kategorię',
			'edytujkategoria' => 'Edytowano kategorię',
			'usunkategoria' => 'Usunięto kategorię #id%s',
			'dodanozytkownik' => 'Dodano użytkownika',
			'usunuzytkownik' => 'Usunięto użytkownika',
			'edytujuzytkownik' => 'Edytowano użytkownika',
			'userzalogowany' => 'Użytkownik zalogował się do panelu',
			'inpostedytuj' => 'Inpost zmieniono ustawienia',
			'dpdedytuj' => 'DPD zmieniono ustawienia',
			'opekedytuj' => 'OPEK zmieniono ustawienia',
			'vatdodaj' => 'Stawka VAT została dodana',
			'vatedytuj' => 'Stawka VAT została edytowana',
			'vatusun' => 'Stawka VAT została usunięta',
			'dostep_potw_zam' => 'Potwierdzenie zamówienia',
			'zmianawidocznyajax' => 'Zmiana widoczności z listy produktów',
			'zmianasztukajax' => 'Zmiana ilości z listy produktów',
			'zmianacenyajax' => 'Zmiana ceny z listy produktów',
			'zmiananazwyajax' => 'Zmiana nazwy z listy produktów',
			'usunatrybuty' => 'Usunięcie atrybutów z edycji produktu',
			'zmienatrybuty' => 'Zmiana atrybutów z edycji produktu',
			'hurtzmienceny' => 'Zmiana ceny z operacji hurtowych',
			'hurtzmienatrybuty' => 'Zmiana atrybutów z operacji hurtowych',
			'hurtzmienkategorie' => 'Zmiana kategorii z operacji hurtowych',
			'hurtzmienprodukty' => 'Zmiana produktów z operacji hurtowych',
			'hurtkopia' => 'Kopiowanie produktów z operacji hurtowych',
			'hurtpowiel' => 'Powielanie produktów z operacji hurtowych',
			'usunzdjecieprod' => 'Usunięto zdjęcie produktu',
			'zamowpotwzmienilosc' => 'Zmiana ilości po potwierdzeniu zamówienia',
			'hurtdodajpowiaz' => 'Dodanie powiązania z operacji hurtowych',
			'hurtdodajpowiazmulti' => 'Dodanie powiązań z operacji hurtowych',
			'hurtusunpowiazmulti' => 'Usunięcie powiązań z operacji hurtowych',
			'usunpowiaz' => 'Usunięcie powiązania z edycji produktu',
			'zamowpotwzmienwidok' => 'Ukrycie widoczności po potwierdzeniu zamówienia'
		);
		return $array[$ar];
	}
	
	static function getRaport($lang = "pl", $data = "")
	{
		if(empty($data)) $data = date("Y-m-d");
		$szukanie = new Zend_Session_Namespace('szukanieAdminEventsRaport');
		$szukanie->prodID = 0;
		$szukanie->jezyk = $lang;
		//$szukanie->domena = '';
		$szukanie->dataOd = $data;
		$szukanie->dataDo = $data;
		$szukanie->autor = '';
		$szukanie->adres_ip = '';
		$szukanie->adres_mac = '';
		$szukanie->user_agent = '';
		
		$Object = new Events;
		$Object->rowset = 99999;
		$Object->page = 0;
		$events = $Object->rowsAdmin($szukanie);
		if(count($events) > 0)
		{
			//$filename = 'admin/zip/raport_'.date('Y-m-d').'.xls';
			
			/*
			$workbook = new Spreadsheet_Excel_Writer($filename);
			$workbook->setVersion(8);
			$workbook->setCountry(48);

			$format_bold = $workbook->addFormat();
			$format_bold->setBold();
			$format_bold->setAlign('center');
			$format_bold->setHAlign('center');
			$format_bold->setTextWrap();
			$format_center = $workbook->addFormat();
			$format_center->setAlign('center');
			$format_left = $workbook->addFormat();
			$format_left->setAlign('left');
			$format_right = $workbook->addFormat();
			$format_right->setAlign('right');

			$worksheet = $workbook->addWorksheet();
			$worksheet->setInputEncoding('UTF-8');
			
			$worksheet->setColumn(0, 0, 5);
			$worksheet->setColumn(1, 1, 10);
			$worksheet->setColumn(2, 2, 6);
			$worksheet->setColumn(3, 3, 20);
			$worksheet->setColumn(4, 4, 15);
			$worksheet->setColumn(5, 5, 15);
			$worksheet->setColumn(6, 6, 150);
			
			$worksheet->write(0, $i++, 'LP', $format_bold);
			$worksheet->write(0, $i++, 'Domena', $format_bold);
			$worksheet->write(0, $i++, 'Język', $format_bold);
			$worksheet->write(0, $i++, 'Data', $format_bold);
			$worksheet->write(0, $i++, 'Adres IP', $format_bold);				
			$worksheet->write(0, $i++, 'Użytkownik', $format_bold);
			$worksheet->write(0, $i++, 'Zdarzenie', $format_bold);
			*/
			
			$html = '<table style="width:550px; font-size:8px;" cellspacing="0" cellpadding="0">';
			if(true)
			{
				$html .= '<tr style="width:550px;">';
				$html .= '<th style="width:20px;text-align:center;">LP</th>';
				//$html .= '<th style="width:50px;text-align:center;">Domena</th>';
				$html .= '<th style="width:20px;text-align:center;">Język</th>';
				$html .= '<th style="width:60px;text-align:center;">Data</th>';
				//$html .= '<th style="width:50px;text-align:center;">Adres IP</th>';
				$html .= '<th style="width:50px;text-align:center;">Użytkownik</th>';
				$html .= '<th style="width:350px;text-align:center;">Zdarzenie</th>';
				$html .= '</tr>';
			}
			$html .= '<tr style="width:550px;"><th colspan="6">&nbsp;</th</tr>';

			$i = 0;
			$lp = 0;
			//var_dump($events);die();
			foreach($events as $event)
			{
				$j = 0;
				if($event['action'] == "zmianawidoczny" || $event['action'] == "zmianasztuk" || $event['action'] == "zamowienieok" 
				|| strpos($event['gdzie'], "iloś") > 0 || strpos($event['gdzie'], "widocznoś") > 0)
				{
					/*
					$worksheet->write($i + 1, $j++, $i + 1, $format_center);
					$worksheet->write($i + 1, $j++, $event['domena'], $format_left);
					$worksheet->write($i + 1, $j++, $event['lang'], $format_left);
					$worksheet->write($i + 1, $j++, $event['data'], $format_left);
					$worksheet->write($i + 1, $j++, $event['adres_ip'], $format_left);
					$worksheet->write($i + 1, $j++, $event['uzytkownik'], $format_left);
					*/
					
					$zdarz = $event['co'];
					if(@strpos($event['co'], $event['gdzie']) === false)
					$zdarz .= " ".str_replace(",", ",", $event['gdzie']);
					
					//$worksheet->write($i + 1, $j++, $zdarz, $format_left);
					$border = 'border-bottom:1px solid #c9c9c9;';
					$html .= '<tr style="'.$border.'width:550px; border-bottom:1px solid black;">';
					$html .= '<td style="'.$border.'width:20px;text-align:center;">'.($i + 1).'</td>';
					//$domena = @$_SESSION['domeny'][$event['domena']]['www'];
					//$domena = str_replace("http://www.", "", $domena);
					//$html .= '<td style="'.$border.'width:50px;text-align:center;">'.$domena.'</td>';
					$html .= '<td style="'.$border.'width:20px;text-align:center;">'.$event['lang'].'</td>';
					$html .= '<td style="'.$border.'width:60px;text-align:center;">'.date("Y-m-d H:i",strtotime($event['data'])).'</td>';
					//$html .= '<td style="'.$border.'width:50px;text-align:center;">'.$event['adres_ip'].'</td>';
					$html .= '<td style="'.$border.'width:50px;text-align:center;">'.$event['uzytkownik'].'</td>';
					$html .= '<td style="'.$border.'width:350px;text-align:left;">'.$zdarz.'</td>';
					$html .= '</tr>';
						
					$i++;
					//if($i > 10) break;
				}
			}
			$html.= '</table>';
			
			try
			{
				//require_once 'Zend/Loader.php';
				require_once("dompdf_config.inc.php");
				//require_once("include/autoload.inc.php");
				//Zend_Loader::registerAutoload();
				//spl_autoload_register('DOMPDF_autoload');
				$load = Zend_Loader_Autoloader::getInstance();
				$load->pushAutoloader('DOMPDF_autoload','');
				//require_once("include/dompdf_exception.cls.php");
				//require_once("../application/models/dompdf6/dompdf_config.inc.php");
				$dompdf = new DOMPDF();
				//$dompdf->set_base_path(APPLICATION_PATH."/../");
				$css = '<head>'.PHP_EOL.'<style type="text/css">'.PHP_EOL.@$css.PHP_EOL.'</style>'.PHP_EOL.'</head>';
				$html = '<body>'.PHP_EOL.$html.PHP_EOL.'</body>';	
				//echo $html; die();
				$html = iconv('UTF-8','ISO-8859-2//IGNORE', $html);
				$body = '<html>'.PHP_EOL.$css.$html.PHP_EOL.'</html>';
				$dompdf->load_html($body);
				$dompdf->render();
				$pdf = $dompdf->output();
				//var_dump($pdf);
				$filename = 'admin/zip/raport_'.$data.'.pdf';
				//$dompdf->stream($filename, array("Attachment" => false));die();
				if(file_put_contents($filename, $pdf) !== false)
				{
					return $filename;
					//$href = '<a href="'.$this->baseUrl.'/public/'.$filename.'" target="_blank">'.$filename.'</a>';
					//$this->view->blad_edycji = '<div class="k_ok">'.$href.'</div>';
				}
				//else $this->view->message = '<div class="k_blad">Błąd zapisywania raportu PDF</div>';
			}
			catch(DOMPDF_Exception $e)
			{
				//$this->view->message = '<div class="k_blad">'.$e->getMessage().'</div>';
			}
			catch(Exception $e)
			{
				//$this->view->message = '<div class="k_blad">'.$e->getMessage().'</div>';
			}
			return "";
			//$workbook->close();
			//$this->view->raport = $filename;
		}
	}
}
?>