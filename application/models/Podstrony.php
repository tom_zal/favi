<?php
class Podstrony extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
        }
	
        function getPodstronyAll(){
            $sql = $this->db->select()->from(array('p' => 'Podstrony'), array('*'))->order('lp');
            $results = $this->db->fetchAll($sql);
            
            return $results;
        }
    
	function getPodstrony($strip = true, $zdj = false)
	{
		$where = 'p.lang = "'.$this->lang.'" and (p.domena is null or p.domena = "'.$this->domena.'")';
		$sql = $this->db->select()->from(array('p' => 'Podstrony'), array('*'));
		if($zdj && $this->obConfig->podstronyGaleria)
		{
			$whereGal = 'p.id = g.wlasciciel and g.typ = "podstrony" and g.glowne = "T"';
			$whereGal.= ' and g.lang = "'.$this->lang.'" and g.wyswietl = "1"';
			$sql->joinleft(array('g' => 'Galeria'), $whereGal, array('img', 'img_opis' => 'nazwa'));
		}
		$sql->where($where);
		$results = $this->db->fetchAll($sql);
		foreach($results as $result)
		{
			if(@!empty($result['tekst'])) $result['tekst'] = stripslashes($result['tekst']);//$this->common->fixTekst($result['tekst'], $strip);
			if(@!empty($result['short'])) $result['short'] = stripslashes($result['short']);//$this->common->fixTekst($result['short'], $strip);
			$rows['link'][$result['link']] = $result;
			$rows['tytul'][$result['tytul']] = $result;
			$rows['temat'][$result['temat']] = $result;
		}
		$this->podstrony = @$rows;
		//var_dump($rows['tytul']);
	}
	function getPodstrona($strip = true, $zdj = false)
	{
		//if(@!empty($this->tytul)) echo $this->tytul.' '.intval(isset($this->podstrony['tytul'][$this->tytul])).'<br>';
		if(@!empty($this->link) && isset($this->podstrony['link'][$this->link])) return $this->podstrony['link'][$this->link];
		if(@!empty($this->tytul) && isset($this->podstrony['tytul'][$this->tytul])) return $this->podstrony['tytul'][$this->tytul];
		if(@!empty($this->temat) && isset($this->podstrony['temat'][$this->temat])) return $this->podstrony['temat'][$this->temat];
		$where = 'p.lang = "'.$this->lang.'"';
		if(@intval($this->id)>0) $where .= ' and p.id = "'.$this->id.'"';
		if(@!empty($this->temat)) $where .= ' and p.temat = "'.$this->temat.'"';
		if(@!empty($this->tytul)) $where .= ' and p.tytul = "'.$this->tytul.'"';
		if(@!empty($this->link)) $where .= ' and p.link = "'.$this->link.'"';
		$sql = $this->db->select()->from(array('p' => 'Podstrony'), array('*'));
		if($zdj && $this->obConfig->podstronyGaleria)
		{
			$whereGal = 'p.id = g.wlasciciel and g.typ = "podstrony" and g.glowne = "T"';
			$whereGal.= ' and g.lang = "'.$this->lang.'" and g.wyswietl = "1"';
			$sql->joinleft(array('g' => 'Galeria'), $whereGal, array('img', 'img_opis' => 'nazwa'));
		}
		$sql->where($where);
		$result = $this->db->fetchRow($sql);
		if(@!empty($result['tekst'])) $result['tekst'] = $this->common->fixTekst($result['tekst'], $strip);
		if(@!empty($result['short'])) $result['short'] = $this->common->fixTekst($result['short'], $strip);
		return $result;
	}
	function setPodstrona($dane)
	{
		$where = 'id = '.$this->id;
		$ret['id'] = $this->id;
		$ret['link'] = $dane['link'];		
		if(true)
		{
			//$this->link = $dane['link'];
			$podstrona = $this->getPodstrona();
			//var_dump($podstrona);die();
			$link = $podstrona['link'];
			if($dane['temat'] != $podstrona['temat']) $link = $dane['temat'];
			if($dane['link'] != $podstrona['link']) $link = $dane['link'];
			if($podstrona['position'] == 'menu' && $link != $podstrona['link'])
			{
				$route = new Routers();
				$link = $route->linkZNazwy($link);
				$sprawdz = $this->sprawdzCzyIstnieje($link);
				if($sprawdz)
				{
					$ok = $route->edytujPodstrona($podstrona['link'], $link);
					if(@$ok)
					{
						$dane['link'] = $link;
						$ret['link'] = $link;
					}					
				}
				else $ret['id'] = 0;
			}
			//var_dump($dane);die();
		}
		if($ret['id'] > 0)
		$this->update($dane, $where);
		return $ret;
	}
	function setTytul($dane, $tytul, $lang = 'pl')
	{
		if(empty($tytul)) return;
		$where = 'tytul = "'.$tytul.'" and lang = "'.$lang.'"';
		$this->update($dane, $where);
	}
	function setLink($dane, $link, $lang = null)
	{
		if(empty($link)) return;
		$where = 'link = "'.$link.'"';
		$this->update($dane, $where);
		if(empty($lang)) return;
		$dane['link'] .= '-'.$lang;
		$where = 'link = "'.$link.'-'.$lang.'"';
		//var_dump($where); die();
		$this->update($dane, $where);
	}
	function insertPodstrona($dane)
	{
		$dane['lp'] = $this->wypiszMaxPozycja($dane['lang']) + 1;
		if(!isset($dane['polozenie']))
		$dane['polozenie'] = 'topbottom';
		$route = new Routers();
		$nowy = $route->dodaj($dane['link'], '', 'default', 'podstrony' ,'show');
		if($nowy['id'] > 0) 
		{
			$dane['link'] = $nowy['link'];
			$this->insert($dane);
		}
		else
		{
			$dane['link'] .= '-'.$this->lang;
			$nowy = $route->dodaj($dane['link'], '', 'default', 'podstrony' ,'show');
			if($nowy['id'] > 0) 
			{
				$dane['link'] = $nowy['link'];
				$this->insert($dane);
			}
		}
		return $nowy;
	}
	function wypiszMaxPozycja($lang = 'pl')
	{
		$select = $this->db->select()->from(array('p'=>'Podstrony'),array('max(lp)'))->where('lang="'.$lang.'"');
		//echo $select;
		$result = $this->db->fetchRow($select);
		return isset($result['max(lp)']) ? intval($result['max(lp)']) : 0;
    }
	function deletePodstrona()
	{
		$where = 'link = "'.$this->link.'"';
		$this->delete($where);
		$route = new Routers();
		$route->usunPodstrony($this->link);
	}
	function insertBlok($dane)
	{
		//$dane['link'] .= '-'.$this->lang;
		$nazwa = trim($dane['link']).'-'.$dane['position'];
		$route = new Routers();
		$nazwa = $route->linkZNazwy($nazwa);
		$sprawdz = $this->sprawdzCzyIstnieje($nazwa);
		$ret['id'] = $sprawdz;
		if($sprawdz)
		{
			$dane['link'] = $nazwa;
			$this->insert($dane);
			$ret['id'] = $this->getAdapter()->lastInsertId();
			$ret['link'] = $nazwa;
		}
		return $ret;
	}
	function deleteBlok()
	{
		$where = 'link = "'.$this->link.'"';
		$this->delete($where);
	}
	function sprawdzCzyIstnieje($nazwa) 
	{
		if(empty($nazwa)) return 0;
		$where = 'link = "'.$nazwa.'" and lang = "'.$this->lang.'"';
		$result = $this->fetchAll($where);
		if(count($result)>0) return 0;
		else return 1;
	}
	
	public function linkiSitemap()
	{
        $select = $this->select('id,link')->where('temat <> "" and position = "menu" and lp > 0 and lang = "pl"')->order('temat');
        $result = $this->fetchAll($select);
        return $result;
    }
}
?>