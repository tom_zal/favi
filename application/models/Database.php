<?php
    class Database extends Zend_Db_Table  {
        public $obConfig, 
               $profiling = false, 
               $typ = 'general', 
               $table = false, 
               $colId = 'id', 
               $colOrder = 'id', 
               $eleShow = 'ASC';
        public function __construct() {
            parent::__construct();
            $this->_name = !$this->table ? $this->_name : $this->table;
            $this->obConfig = new Zend_Config_Ini('../application/config.ini', $this->typ);
            $this->view = Zend_Layout::getMvcInstance()->getView();
            $this->request = Zend_Controller_Front::getInstance()->getRequest();
            $this->response = Zend_Controller_Front::getInstance()->getResponse();
            $this->redirect = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector'); 
            $this->params = $this->request->getParams();
        }
        function _insert($data) {
            try {
                $this->insert($data);
                return $this->getAdapter()->lastInsertId();
            } catch (Zend_Exception $e) {
                if($this->profiling) echo 'Błąd wstawienia wiersza do bazy danych (nazwa tabeli '.$this->_name.'): <br />'.$e;
            }
        }
        function _update($data) {
            try {
                $this->update($data, $this->colId.' = "'.$this->id.'"');
            } catch(Zend_Exception $e) {
                if($this->profiling) echo 'Błąd edycji wiersza w bazie danych (nazwa tabeli '.$this->_name.'): <br />'.$e;
            }
        }
        function _delete(){
            try {
                $this->delete($this->colId.' = "'.$this->id.'"');
            } catch(Zend_Exception $e) {
                if($this->profiling) echo 'Błąd usunięcia wiersza z bazy danych (nazwa tabeli '.$this->_name.'): <br />'.$e;
            }
        }
        function _get() {
            try {
                return $this->fetchRow($this->colId.' = "'.$this->id.'"');
            } catch(Zend_Exception $e) {
                if($this->profiling) echo 'Błąd pobrania wiersza z bazy danych (nazwa tabeli '.$this->_name.'): <br />'.$e;
            }
        }
        function _getAll() {
            try {
                return $this->fetchAll($this->colId.' = "'.$this->id.'"', $this->colOrder.' '.$this->eleShow)->toArray();
            } catch(Zend_Exception $e) {
                if($this->profiling) echo 'Błąd pobrania wierszy z bazy danych (nazwa tabeli '.$this->_name.'): <br />'.$e;
            }
        }
        function _redirect($redrect = '') {
            $this->redirect->gotoUrl($redrect);
        }
        function _sortArray($ar = array()) {
            $a = array();
            if(!empty($ar)) {
                foreach($ar as $r) $a[$r[$this->colId]] = $r;
            }
            return $a;
        }
    }
?>