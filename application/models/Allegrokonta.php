<?php
class Allegrokonta extends Zend_Db_Table
{
    public $ID, $login, $choose, $error;
    protected $_name = 'Allegro_konta';
	public $link, $obConfig, $db;
	
	public function __construct($module = 'admin', $ID = 1)
	{
		parent::__construct();
		$this->ID = $ID;
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

    function dodaj($dane)
	{
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }
    function edytuj($dane)
	{
        $where = 'id = "'.$this->ID.'"';
        $this->update($dane, $where);
    }
	function edytujDefault($dane)
	{
        $where = 'zal = "1"';
        $this->update($dane, $where);
    }
    function usun()
	{
        $where = 'id = "'.$this->ID.'"';
        $this->delete($where);
    }

    function klient()
	{
        $where = 'id = "'.$this->ID.'"';
        $result = $this->fetchRow($where);
        $res = $result->toArray();
        return $res;
    }
	function klientZal()
	{
        $where = 'zal = "1"';
        $result = $this->fetchRow($where);
        $res = $result;
        return $res;
    }
    function zobaczklient()
	{
        $where = 'id = "'.$this->ID.'"';
        $result = $this->fetchRow($where);
        $res = $result;
        return $res;
    }
    
    public function zal($id)
	{
        $where = 'id = "'.$id.'"';
        $dane['zal'] = '1';
        $this->update($dane, $where);
        
        $nzal = $this->listaKlient();
        for($i=0;$i<count($nzal);$i++)
		{
			if($nzal[$i]['id'] != $id)
			{
				$where = 'id = "'.$nzal[$i]['id'].'"';
				$dane['zal'] = '0';
				$this->update($dane, $where);
			}
        }
    }
	
    function listaKlient()
	{
        $result = $this->fetchAll();
        $res = $result->toArray();
        return $res;
    }
	function listapobierzKlient()
	{
        $where = 'auto = "1"';
        $result = $this->fetchAll($where);
        $res = $result->toArray();
        return $res;
    }
	function getLogin($login = "")
	{
        $where = 'login = "'.$login.'"';
        $result = $this->fetchRow($where);
        return $result;
    }

    function checkLogin()
	{
        if($this->choose == 0)
		{
            $where = 'login = "'.$this->login.'"';
        } 
		else
		{
            $where = 'login = "'.$this->login.'" AND id <> "'.$this->ID.'"';
		}

        $result = $this->fetchAll($where);
        $res = $result->toArray();
        return $res;
    }
	
	public function zaloguj($ID = 1) 
	{
		$this->ID = $ID;
		$config = $this->klient();
		
		//define('ALLEGRO_ID', 'id');
		@define('ALLEGRO_LOGIN', $config['login']);
		@define('ALLEGRO_PASSWORD', $config['haslo']);
		@define('ALLEGRO_KEY', $config['klucz_webapi']);
		@define('ALLEGRO_COUNTRY', 1);
		
		try
		{
			$webapi = new AllegroWebAPI();
			$webapi->Login();
			@define('ALLEGRO_ID', $webapi->GetUserID($config['login']));
			$webapi = new AllegroWebAPI();
			$webapi->Login();
			$allegro = new AllegroClientSoap($config, null);
			$this->error = '';
			//$this->view->blad_edycji = 'Stan konta Allegro: '.$this->webapi->MyBilling().'<br/>';
		}
		catch(SoapFault $error)
		{
			$this->error = 'Błąd '.$error->faultcode.': '.$error->faultstring."";
			return null;
		}
		catch(Exception $error)
		{
			$this->error = 'Błąd '.$error->faultcode.': '.$error->faultstring."";
			return null;
		}
		return $webapi;
    }
	
	public function findByCronStart()
	{
		$where = 'cron_start = 1';
		$r = $this->fetchRow($where);
		if(!empty($r)) $r = $r->toArray();
		return $r;
	}
	
	public function findCronStartNext($current_id = 0)
	{
		$sql = 'select min(id) as id from `Allegro_konta` where id > "'.$current_id.'"';
		$select = $this->db->query($sql);
		$r = $select->fetchAll();
		$r = $r[0];
		if(!empty($r['id']))
		{
			$dane['cron_start'] = 0;
			$where = '1';
			$u = $this->update($dane, $where);
			if(!empty($u))
			{
				$dane['cron_start'] = 1;
				$where = 'id = "'.$r['id'].'"';
				$this->update($dane, $where);
			}			
		}
		else
		{
			$sql = 'select min(id) as id from `Allegro_konta` where 1';
			$select = $this->db->query($sql);
			$r2 = $select->fetchAll();
			$r2 = $r2[0];
			if(!empty($r2['id']))
			{
				$dane['cron_start'] = 0;
				$where = '1';
				$u = $this->update($dane, $where);
				if(!empty($u))
				{
					$dane['cron_start'] = 1;
					$where = 'id = "'.$r2['id'].'"';
					$this->update($dane, $where);
				}
			}
		}
	}
}
?>