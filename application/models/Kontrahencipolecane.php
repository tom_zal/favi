<?php
class Kontrahencipolecane extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
        $where = 'id = '.$this->id;
        return $this->update($dane, $where);
    }
	function wypiszAll()
	{
		$result = $this->fetchAll();
		return $result;
	}
	function wypiszID()
	{
		$sql = $this->select()->where('id = '.$this->id);
		$result = $this->fetchRow($sql);
		return $result;
	}
	function wypiszKlient($id)
	{
		$sql = $this->select()->where('id_kontr = '.$id);
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszKlientTyp($id, $typ = 'polecane')
	{
		$sql = $this->select()->where('id_kontr = '.$id.' and typ = "'.$typ.'"');
		$result = $this->fetchRow($sql);
		return $result;
	}
	function wypiszProd($id)
	{
		$sql = $this->select()->where('id_prod = '.$id);
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszProdTyp($id, $typ = 'polecane')
	{
		$sql = $this->select()->where('id_prod = '.$id.' and typ = "'.$typ.'"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function obsluzObserwowane($id = 0)
	{
		$sql = $this->select()->where('id_prod like "%,'.$id.',%" and typ = "obserwowane"');
		$result = $this->fetchAll($sql);
		$kontrahenci = new Kontrahenci();
		$wiadomosci = new Wiadomosci();
		$produkty = new Produkty();
		if(count($result) > 0)
		foreach($result as $row)
		{
			$kontrahenci->id = $row['id_kontr'];
			$user = $kontrahenci->wypiszPojedynczy();
			if(@!empty($user['email']))
			{
				$produkty->id = $id;
				$produkt = $produkty->wypiszPojedyncza();
				$link = '<a href="'.$this->obConfig->www.'/'.$produkt['link'].'">';
				$link.= $produkt['nazwa'].'</a>';
				$wiadomosci->obserwowane($user['email'], $link, $user['lang']);
			}
			$this->id = $row['id'];
			$this->edytuj(array('id_prod' => str_replace(','.$id.',', ',', $row['id_prod'])));
		}
		return $result;
	}
	function dodajProdukt($id = 0, $prod = 0, $typ = 'polecane')
	{
		$row = $this->wypiszKlientTyp($id, $typ);
		if(count($row) == 0)
		{
			$dane['id_kontr'] = $id;
			$dane['id_prod'] = ','.$prod.',';
			$dane['typ'] = $typ;
			$this->insert($dane);
			return true;
		}
		$ids = @explode(',', $row['id_prod']);
		if(empty($row['id_prod']) || !$this->common->isArray($ids, true))
		{
			$this->id = $row['id'];
			$this->edytuj(array('id_prod' => ','.$prod.','));
			return true;
		}
		else if(!in_array($prod, $ids))
		{
			if(count($ids) > 0)
			foreach($ids as $i => $id)
			{
				if(intval($id) == 0)
				unset($ids[$i]);
			}
			$ids[] = $prod;
			$this->id = $row['id'];
			$this->edytuj(array('id_prod' => ','.implode(',', $ids).','));
			return true;
		}
		else return false;
	}
	function wypiszKontrahent($id, $typ = 'polecane')
	{
		$select = $this->db->select()->from(array('p' => 'Produkty'), array('nazwa'))
			->join(array('kp' => 'Kontrahencipolecane'), 'p.id = kp.id_prod', array('id'))
			->where('kp.id_kontr = '.$id.' and typ = "'.$typ.'"')->order('nazwa asc')->limit(9999);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszProdukty($id, $typ = 'polecane', $ile = 99999, $order = 'nazwa asc')
	{
		$row = $this->wypiszKlientTyp($id, $typ);
		$ids = @unserialize($row['id_prod']);
		if(!$this->common->isArray($ids, true)) return null;
		$select = $this->db->select()->from(array('p' => 'Produkty'), array('*'))
			->where('p.id in ('.implode(',', $ids).') and p.widoczny')->order($order)->limit($ile);
		//echo $select; return null;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszProdukty2($id, $typ = 'polecane', $ile = 99999, $order = 'nazwa asc', $wykluczBestsellery = false)
	{
		$row = $this->wypiszKlientTyp($id, $typ);
		$ids = @explode(',', $row['id_prod']);
		if(count($ids) > 0)
		foreach($ids as $i => $id)
		{
			if(intval($id) == 0)
			unset($ids[$i]);
		}
		if(!$this->common->isArray($ids, true)) return null;
		
		$where = 'p.id in ('.implode(',', $ids).') and p.widoczny';
		if($typ == 'bestseller' && $wykluczBestsellery)
		$where.= ' and p.id not in (select id_prod from Archiwumprodukty)';
		$select = $this->db->select()->from(array('p' => 'Produkty'), array('*'));
		$select->joinleft(array('g'=>'Galeria'),'g.wlasciciel=p.id and g.glowne="T" and g.typ="oferta"',array('img'));
		$select->where($where)->order($order)->limit($ile);
		//echo $select; return null;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszProduktyOld($id, $typ = 'polecane', $ile = 3, $order = 'nazwa asc')
	{
		$select = $this->db->select()->from(array('p' => 'Produkty'), array('*'))
			->join(array('kp' => 'Kontrahencipolecane'), 'p.id = kp.id_prod', array(''))
			->where('kp.id_kontr = '.$id.' and typ = "'.$typ.'"')->order($order)->limit($ile);
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function usun()
	{
		$result = $this->delete('id = '.$this->id);
	}
	function usunKlient($id)
	{
		$result = $this->delete('id_kontr = '.$id);
	}
	function usunKlientTyp($id, $typ)
	{
		$result = $this->delete('id_kontr = '.$id.' and typ = "'.$typ.'"');
	}
	function usunProdukt($id)
	{
		$result = $this->delete('id_prod = '.$id);
	}
	function usunProduktTyp($id, $typ)
	{
		$result = $this->delete('id_prod = '.$id.' and typ = "'.$typ.'"');
	}
}
?>