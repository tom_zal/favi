<?php
class Atrybutygrupy extends Zend_Db_Table
{
	public $id, $_cache, $_cachename;	
	public $link, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		
		$session = new Zend_Session_Namespace();
		$this->_cache = $session->cache;
		$this->_cachename = $this->_name;
		$this->caching = false;
    }

	function _save($data)
	{
		$this->insert($data);
		//$this->_cache->remove('_cache_'.$this->_cachename.'_'.$this->lang);
		return $this->getAdapter()->lastInsertId();
	}

	function _update($data)
	{
		$this->update($data, 'id = '.$this->id);
		//$this->_cache->remove('_cache_'.$this->_cachename.'_'.$this->lang);
	}

	function _delete()
	{
		$this->delete('id = '.$this->id);
		//$this->_cache->remove('_cache_'.$this->_cachename.'_'.$this->lang);
	}
	
	function _updateRodzaj($rodzajOld = 0, $rodzajNew = 0)
	{
		$this->update(array('id_rodzaj' => $rodzajNew), 'id_rodzaj = '.$rodzajOld);
		//$this->cache->remove('cache_'.$this->cachename.'_'.$this->lang);
	}
	
	function usunKategorie($id = 0, $id_kat = 0)
	{
		$this->id = $id;
		$row = $this->getRow();
		if(@!empty($row['id_kat']))
		{
			$kats = str_replace(";".$id_kat.";", ";", ";".$row['id_kat'].";");
			$this->_update(array('id_kat' => trim(str_replace(";;", ";", $kats), ";")));
		}
	}
	function dodajKategorie($id = 0, $id_kat = 0)
	{
		$this->id = $id;
		$row = $this->getRow();
		if(@!empty($row['id_kat']))
		{
			$kats = str_replace(";".$id_kat.";", ";", ";".$row['id_kat'].";");
			$this->_update(array('id_kat' => trim(str_replace(";;", ";", $kats.";".$id_kat), ";")));
		}
		else $this->_update(array('id_kat' => $id_kat));
	}
	function getRow()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function getRows($id = 0, $sort = "poz ASC")
	{
		$where = 'lang = "'.$this->lang.'" and (id_prod = 0 or id_prod = '.$id.')';
        $result = $this->fetchAll($where, $sort);
		return $result;
	}
	function getAtrybutyForKateg($id_kat = 0)
	{
		$where = 'lang = "'.$this->lang.'"';
		if($id_kat == 0) $where .= ' and (id_kat is null or REPLACE(id_kat, ";", "") = "")';
		else if($id_kat > 0) $where .= ' and CONCAT(";", id_kat, ";") like "%;'.$id_kat.';%"';
		else $where .= ' and (LENGTH(REPLACE(id_kat, ";", "")) > 0 and CONCAT(";", id_kat, ";") not like "%;'.abs($id_kat).';%")';
		$result = $this->fetchAll($where, 'poz ASC');//echo $where.'<br>';
		return $result->toArray();
	}
    function getRowsa($id = 0, $kategorie = null)
	{
		$where = 'lang = "'.$this->lang.'" and (id_prod = 0 or id_prod = '.$id.')';
        $result = $this->fetchAll($where, 'poz ASC');
        for($i=0;$i<count($result);$i++)
		{
            $ile = array_intersect(explode(';', $result[$i]['id_kat']), $kategorie);
            //print_r($ile);
            if(count($ile) > 0)
			{
                $result1[] = $result[$i];
            }
            elseif($result[$i]['id_prod'] > 0 && $result[$i]['id_prod'] == $id)
			{
                $result1[] = $result[$i];
            }
            elseif($result[$i]['id_prod'] == 0 && empty($result[$i]['id_kat']))
			{
                $result1[] = $result[$i];
            }           
        }        
		return @$result1;
	}

	function getRowsSearch($id = 0)
	{
		$where = 'lang = "'.$this->lang.'" and (id_prod = 0 or id_prod = '.$id.')';
        $result = $this->fetchAll($where.' AND wyszukiwarka = "1" AND status = "1"', 'nazwa ASC');
		return $result;
	}
	
	function getAtrybuty($ids = null)
	{
		//if(!$ids) return null;
		$where = '1';
		if($this->common->isArray($ids, true)) $where = 'id in ('.implode(',',$ids).')';
		$result = $this->fetchAll($where, 'poz ASC');
		if(count($result) > 0)
		foreach($result as $row)
		{
			$results[$row['id']] = $row;
		}
		return @$results;
	}
	function getAtrybutySort()
	{
		$result = $this->fetchAll('wyszukiwarka = "1" AND status = "1" and typatrybut <> "multi"', 'nazwa ASC');
		return $result;
	}
	function getAtrybutyTekst($jedn = '')
	{
		$where = 1;
		if(!empty($jedn)) $where = 'jedn_miary_typ = "'.$jedn.'"';
		$result = $this->fetchAll('status = "1" and typatrybut = "tekst" and '.$where, 'nazwa ASC');
		return $result;
	}

	function getRowsUniqueFront($_cache = false, $caching = false, $_cachename = 'atrybutyfront')
	{            
		if($_cache) $this->result = $_cache->load('_cache_'.$_cachename.'_'.$this->lang);else $this->result = false;

		if($this->result === false || !$caching)
		{
			$Object = new Atrybutygrupy();
			$Object->lang = $this->lang;
			$grupy = $Object->getRowsSearch()->toArray();
			$Object = new Atrybuty();
			$Object->lang = $this->lang;
			$atrybuty = $Object->getRowsSearch()->toArray();

			$array = array();
			$g = 0;
			$z = 0;
			$flaga = 0;
			if(!empty($grupy))
			foreach($grupy as $grupa)
			{
				$a = 0;
				if(!empty($atrybuty))
				foreach($atrybuty as $atrybut)
				{
					if($grupa['id'] == $atrybut['id_gr'])
					{
						if($grupa['glowny'] == 1)
						{
							$array['glowne'][$g]['grupa'] = $grupa['nazwa'];
							$array['glowne'][$g]['glowne']['atrybut'][$a] = $atrybut;
							$flaga = 0;
						}
						else
						{
							$array['zwykle'][$z]['grupa'] = $grupa['nazwa'];
							$array['zwykle'][$z]['atrybut'][$a] = $atrybut;
							$flaga = 1;
						}
						$a++;
					}
				}
				if($flaga == 0) $g++;
				if($flaga == 1) $z++;
			}
			
			$this->result = $array;
			if($_cache) $_cache->save($this->result, '_cache_'.$_cachename.'_'.$this->lang);
		}
		return $this->result;
	}
	
	function getAtrybutyGrupy($pole = 'symbol')
	{
		$sql = $this->select()->where($pole.' <> ""')->order($pole);
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i][$pole]] = $result[$i]->toArray();
		}
		return @$results;
	}
}    
?>