<?php

class AuthDataV1
{
	public $login;
	public $masterFID;
	public $password;
	
	public function __construct($module = 'admin')
	{
		$this->module = $module;
		$dpdDane = new Dpddane($module);
		$this->dpdDane = $dpdDane->wypisz();
		$this->login = $this->dpdDane['login'];
		$this->masterFID = $this->dpdDane['FID'];
		$this->password = $this->dpdDane['haslo'];
    }
};

class Dpd extends Zend_Db_Table
{
	public $id;
	
	public $url1 = 'https://dpdservices.dpd.com.pl/DPDPackageXmlServicesService/DPDPackageXmlServices?wsdl';
	public $url2 = 'https://dpdservices.dpd.com.pl/DPDPackageObjServicesService/DPDPackageXmlServices?wsdl';
	public $url_test1 = 'https://dpdservicesdemo.dpd.com.pl/DPDPackageXmlServicesService/DPDPackageXmlServices?wsdl';
	public $url_test2 = 'https://dpdservicesdemo.dpd.com.pl/DPDPackageObjServicesService/DPDPackageXmlServices?wsdl';
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->module = $module;
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		$this->daneKlient = array('address','city','company','countryCode','email','FID','name','phone','postalCode');
    }
	
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujRef($dane, $ref)
	{
		$where = 'reference = "'.$ref.'"';
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$result = $this->fetchAll();
		return $result;
	}
	function wypiszZam($id)
	{
		$result = $this->fetchAll('id_zam = '.$id);
		return $result;
	}
	function wypiszRaport($data)
	{
		$result = $this->fetchAll('data >= "'.$data.' 00:00:00" and data <= "'.$data.' 23:59:59"');
		return $result;
	}

	function wypiszJeden()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function usunJeden()
	{
		$result = $this->delete('id = '.$this->id);
	}
	
	function dane($array, $nazwa = '')
	{
		$dane = '';
		$nazwa = ucfirst($nazwa);
		$params = @count($array['params']);
		$shortTag = $array === null || (count($array) == 1 && $params);
		if($params)
		{
			$tagParam = ''.$nazwa.'';
			foreach($array['params'] as $par => $param)
			$tagParam .= ' '.$par.'="'.$param.'"';
			$tagParam .= $shortTag ? ' /' : '';
		}
		if(!empty($nazwa) && !$params) $dane .= '<'.$nazwa.'>';
		if(!empty($nazwa) && $params) $dane .= '<'.$tagParam.'>';

		if(is_array($array))
		{
			foreach($array as $id => $value)
			{
				if($id == 'params') continue;
				if(is_array($value))
				{
					$dane .= $this->dane($value, $id);
				}
				else
				{
					if($value === null) $dane .= '<'.ucfirst($id).'/>';
					else $dane .= '<'.ucfirst($id).'>'.$value.'</'.ucfirst($id).'>';
				}
			}
		}
		if(!empty($nazwa) && !$shortTag) $dane .= '</'.$nazwa.'>';
		return $dane;
	}
    function funkcja($nazwa, $config = null, $odbiorca = null)
	{
		$dpdDane = new Dpddane();
		$daneKurier = $dpdDane->wypisz();
		
		if($nazwa == 'generatePackagesNumbersV1')
		{
			$paczka['content'] = $config['Content'];
			$paczka['customerData1'] = $config['CustomerData1'];
			$paczka['customerData2'] = $config['CustomerData2'];
			$paczka['customerData3'] = $config['CustomerData3'];
			$paczka['sizeX'] = $config['SizeX'];
			$paczka['sizeY'] = $config['SizeY'];
			$paczka['sizeZ'] = $config['SizeZ'];
			$paczka['weight'] = $config['Weight'];
			$przesylka['Parcels'] = array('Parcel' => $paczka);
			
			$przesylka['Reference'] = $this->id.'_'.$odbiorca['FID'];
			$przesylka['ref1'] = $this->id;
			$przesylka['ref2'] = $odbiorca['FID'];
			$przesylka['ref3'] = '';			
			$przesylka['payerType'] = $config['PayerType'];
			
			$odbiorca['PostalCode'] = str_replace('-', '', $odbiorca['PostalCode']);
			//unset($odbiorca['FID']);
			$przesylka['receiver'] = $odbiorca;
			
			$sender = array();
			foreach($this->daneKlient as $pole)
			$sender[$pole] = $config[ucfirst($pole)];
			$sender['postalCode'] = str_replace('-', '', $sender['postalCode']);
			$przesylka['sender'] = $sender;
			
			$services = array();
			if($config['DeclaredValue'] > 5000)
			{
			$services['DeclaredValue']['Amount'] = $config['DeclaredValue'];
			$services['DeclaredValue']['Currency'] = 'PLN';
			}
			$services['Guarantee']['params']['type'] = $config['Guarantee'];
			$services['Guarantee']['Attr1'] = $config['Guarantee_Attr1'];
			if(@$config['CUD'] && @!$config['ROD']) $services['CUD'] = null;
			if(@$config['DOX'] && @$paczka['weight'] <= 0.5) $services['DOX'] = null;
			if(@$config['ROD'] && @!$config['CUD']) $services['ROD'] = null;
			if($config['COD'] > 0)
			{
			$services['COD']['Amount'] = $config['COD'];
			$services['COD']['Currency'] = 'PLN';
			}
			if(@$config['InPers']) $services['InPers'] = null;
			if(@!empty($config['SelfCol']))
			$services['SelfCol']['params']['receiver'] = $config['SelfCol'];
			if(@$config['PrivPers']) $services['PrivPers'] = null;
			if(@$config['CarryIn']) $services['CarryIn'] = null;
			//Us�uga 'Wniesienie' nie jest dozwolona dla wybranego numeru p�atnika
			if(@$config['Duty']) $services['Duty'] = null;
			if(@$config['Pallet']) $services['Pallet'] = null;
			$przesylka['services'] = $services;
			
			$package = array('Package' => $przesylka);
			if(true) $dane['Packages'] = $package;
			else $dane['openUMLV1'] = array('Packages' => $package);
		}
		else if($nazwa == 'generateSpedLabelsXV1')
		{
			$dpdServiceParam['Policy'] = 'STOP_ON_FIRST_ERROR';
			$session['SessionType'] = 'DOMESTIC';
			if(isset($config['SessionId']))
			$session['SessionId'] = $config['SessionId'];
			if(isset($config['packageId']))
			$session['Packages']['Package']['PackageId'] = $config['packageId'];
			if(isset($config['reference']))
			$session['Packages']['Package']['Reference'] = $config['reference'];
			if(isset($config['parcelId']))
			$session['Packages']['Package']['Parcels']['Parcel']['ParcelId'] = $config['parcelId'];
			if(isset($config['waybill']))
			$session['Packages']['Package']['Parcels']['Parcel']['Waybill'] = $config['waybill'];
			$dpdServiceParam['Session'] = $session;
			$dane['DPDServicesParamsV1'] = $dpdServiceParam;
		}
		else if($nazwa == 'generateProtocolXV1')
		{
			$dpdServiceParam['Policy'] = 'STOP_ON_FIRST_ERROR';
			$dpdServiceParam['PickupAddress']['FID'] = $daneKurier['FID'];
			$session['SessionType'] = 'DOMESTIC';
			if(isset($config['SessionId']))
			$session['SessionId'] = $config['SessionId'];
			if(isset($config['packageId']))
			$session['Packages']['Package']['PackageId'] = $config['packageId'];
			if(isset($config['reference']))
			$session['Packages']['Package']['Reference'] = $config['reference'];
			if(isset($config['parcelId']))
			$session['Packages']['Package']['Parcels']['Parcel']['ParcelId'] = $config['parcelId'];
			if(isset($config['waybill']))
			$session['Packages']['Package']['Parcels']['Parcel']['Waybill'] = $config['waybill'];
			$dpdServiceParam['Session'] = $session;
			$dane['DPDServicesParamsV1'] = $dpdServiceParam;
		}
		else $dane = $config;
		
		if(false)
		{
			$authDataV1 = array();
			$authDataV1['login'] = $daneKurier['login'];
			$authDataV1['masterFID'] = $daneKurier['FID'];
			$authDataV1['password'] = $daneKurier['haslo'];
			if(false) $dane['authDataV1'] = $authDataV1;
		}
		if(false) $dane['pkgNumsGenerationPolicyV1'] = 'ALL_OR_NOTHING';
		
		$xml = '';
		if(false)
		{
			$xml.= '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">';
			$xml.= '<S:Header/>';
			$xml.= '<S:Body>';
			$xml.= '<ns2:'.$nazwa.' xmlns:ns2="http://dpdservices.dpd.com.pl/">';
		}
		$xml.= $this->dane($dane);
		if(false)
		{
			$xml.= '</ns2:'.$nazwa.'>';
			$xml.= '</S:Body>';
			$xml.= '</S:Envelope>';
		}
		return $xml;
    }
}
?>