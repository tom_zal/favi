<?php
class DHL24webapiclient extends SoapClient
{
    const WSDL = 'https://dhl24.com.pl/webapi2'; 
	const WSDL_TEST = 'http://sandbox.dhl24.com.pl/webapi2'; 
    public function __construct($wsdl = "")
    {
        parent::__construct( !empty($wsdl) ? $wsdl : self::WSDL );
    }
}
class Dhldane extends Zend_Db_Table
{	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->module = $module;
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

	function updateData($array)
	{
		$where = 'id = 1';
		$this->update($array, $where);
	}	
	function showData()
	{
		$result = $this->fetchAll();		
		return $result;
	}	
	function wypisz()
	{
		$result = $this->select()->where('id = 1');
		return $this->fetchRow($result)->toArray();
	}
}
?>