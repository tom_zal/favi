<?php
class uploadJpg
{       
	public $lokalizacja, $sciezka, $sciezkaSmall, $sciezkaThumb, $sciezkaTiny;
	private $fullWidth, $fullHeight, $smallWidth, $smallHeight;
	private $thumbWidth, $thumbHeight, $tinyWidth, $tinyHeight;
	private $mime, $polishBig, $polishSmall, $maxfilesize, $dostepne_typy;
	public $nazwa, $typ, $size, $tmp, $error;
	
	function __construct($module = 'admin')
	{
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig('general');
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		$this->obConfig = $this->common->getObConfig('image');
		$this->sciezka = $this->obConfig->ImageDir;
		$this->sciezkaSmall = $this->obConfig->ImageDir.$this->obConfig->SmallImageDir;
		$this->sciezkaThumb = $this->obConfig->ImageDir.$this->obConfig->Thumbs;
		$this->sciezkaTiny = $this->obConfig->ImageDir.$this->obConfig->Tiny;
		$this->maxfilesize = 4;
		$this->fullWidth =  $this->obConfig->szerokosc;
		$this->fullHeight =  $this->obConfig->wysokosc;
		$this->smallWidth = $this->obConfig->min_x_1;
		$this->smallHeight = $this->obConfig->min_y_1;
		$this->thumbWidth = $this->obConfig->min_x_2;
		$this->thumbHeight = $this->obConfig->min_y_2;
		$this->tinyWidth = $this->obConfig->min_x_3;
		$this->tinyHeight = $this->obConfig->min_y_3;
		$this->polishBig = 
		array('Ę'=>'e','Ó'=>'o','Ą'=>'a','Ś'=>'s','Ł'=>'l','Ż'=>'z','Ź'=>'z','Ć'=>'c','Ń'=>'n');
		$this->polishSmall = 
		array('ę'=>'e','ó'=>'o','ą'=>'a','ś'=>'s','ł'=>'l','ż'=>'z','ź'=>'z','ć'=>'c','ń'=>'n');
		$ver = explode('.', phpversion());
		$this->mime = ($ver[0] >= 5 && $ver[1] >= 3) && function_exists('finfo');
		$this->dostepne_typy = array("image/jpeg", "image/jpg", "image/pjpg", "image/pjpeg", "image/png", "image/gif");
	}


	function checkPhoto($znakWodny = false)
	{
		$error = "";

		if(!$this->nazwa) return 'Nie wprowadzono pliku';

		if($this->size > ($this->maxfilesize * 1024 * 1024))
		{
			return 'Plik jest zbyt duży - max '.$this->maxfilesize.' MB';
		}
		
		if($this->mime)
		{
			$file_info = new finfo(FILEINFO_MIME);
			$typ = explode(';', $file_info->buffer(file_get_contents($this->tmp)));
			$this->typ = $typ[0];
		}
		if(!in_array($this->typ, $this->dostepne_typy))
		{
			return 'Niepoprawny typ danych ['.$this->typ.'] - wybierz poprawny plik JPG / GIF / PNG';
		}
		
		if(true && $this->typ != 'image/png' && $this->obConfig->uploadSimple && !$znakWodny)
		{
			if($this->uploadPhoto())
			{
				return 'Błąd tworzenia zdjęcia na serwerze.';
			}
		}
		else return $this->uploadPhotoPHPUpload($znakWodny);
		
		return $error;
	}
	
	private function uploadPhotoPHPUpload($znakWodny = false)
	{
		$array = array
		(
			'name' => $this->nazwa, 
			'type' => $this->typ,
			'tmp_name' => $this->tmp,
			'error' => $this->error,
			'size' => $this->size
		);
		$test = false;
		$info = pathinfo($this->tmp);
		$typ = explode('/', $this->typ);
		$nazwa = md5(uniqid(mt_rand(), true));
		if(!empty($this->link)) $nazwa = $this->link;
		if($test) $nazwa = '0000000000000000';
		//if($typ[1] == 'jpeg') $typ[1] = 'jpg';
		//$this->nazwa = $nazwa.'.'.$typ[1];
		//$znakWodny = $this->obConfig->uploadWatermark;
		if($znakWodny)
		{
			$tmp = '../public/admin/zdjecia/_'.$this->nazwa;
			copy($this->tmp, $tmp);
			$array['tmp_name'] = $this->tmp = $tmp;
		}
		//$znakWodny = $znakWodny && !empty($this->poz);
		$znakWodny = $znakWodny && @intval($this->opac) > 0;
		$this->nazwa = $nazwa.'.'.strtolower($info['extension']);
		//var_dump($this->nazwa);die();
		
		$watermark = '../public/admin/watermark/watermark.png';
		if($znakWodny && @intval($this->opac) < 10)
		{
			$znak_imagesize = @getimagesize($watermark);
			//var_dump($znak_imagesize);die();
			$znak_width = $znak_imagesize[0];
			$znak_height = $znak_imagesize[1];
			switch($znak_imagesize['mime'])
			{
				case "image/png": $znak_org = imagecreatefrompng($watermark); break;
				case "image/gif": $znak_org = imagecreatefromgif($watermark); break;
				case "image/jpeg":
				case "image/jpg":
				case "image/pjpg":
				case "image/pjpeg": default: $znak_org = imagecreatefromjpeg($watermark);
			}
			//var_dump($znak_org);die();
			$znak_new = imagecreatetruecolor($znak_width, $znak_height);
			imagealphablending($znak_new, false);
			imagesavealpha($znak_new, true);
			$transparent = imagecolorallocatealpha($znak_new, 255, 255, 255, 127);
			imagefilledrectangle($znak_new, 0, 0, $znak_width, $znak_height, $transparent);
			//var_dump($znak_new);die();
			$upload = new upload($watermark);
			$upload->imagecopymergealpha($znak_new, $znak_org, 0, 0, 0, 0, $znak_width, $znak_height, $this->opac * 10);
			$watermark .= '_'.$this->opac.'.png';
			imagepng($znak_new, $watermark, 1);
			//echo '<img src="/'.$watermark.'">';die();
		}
		
		$handle = @new upload($array);
		$handle->no_upload_check = true;
		if($handle->uploaded)
		{
			if($test) $handle->file_overwrite = true;			
			$handle->file_new_name_body = $nazwa;
			$handle->image_resize = true;
			$handle->image_ratio = true;
			$handle->image_x = $this->obConfig->szerokosc;
			$handle->image_y = $this->obConfig->wysokosc;
			$handle->image_ratio_no_zoom_in = true;
			if($znakWodny)
			{
				$handle->image_watermark = $watermark;
				//$handle->image_watermark_x = 5;
				//$handle->image_watermark_y = 5;
				$handle->image_watermark_position = $this->poz;//TBLR
				$handle->image_watermark_no_zoom_in = true;
				$handle->image_watermark_no_zoom_out = false;
			}
			$handle->process($this->obConfig->ImageDir);
			if($handle->processed)
			{
				$this->nazwa = $handle->file_dst_name;
				//var_dump($this->nazwa);die();
				$name = $handle->file_dst_name_body;
				if($test) echo '<img src="http://www.local.d/panel/public/admin/zdjecia/'.$name.'.jpg">';
				if($znakWodny) $handle->clean();
				if(true && $znakWodny)
				$handle = @new upload($handle->file_dst_path.$this->nazwa);
				$handle->no_upload_check = true;
				$handle->file_overwrite = true;
				$handle->file_new_name = $this->nazwa;
				$handle->file_new_name_body = $name;
				$handle->image_resize = true;
				$handle->image_ratio = true;
				$handle->image_x = $this->obConfig->min_x_1;
				$handle->image_y = $this->obConfig->min_y_1;
				$handle->image_ratio_no_zoom_in = true;
				$handle->process($this->obConfig->ImageDir.''.$this->obConfig->SmallImageDir);				
				//var_dump($array);
				if($handle->processed)
				{					
					$name = $handle->file_dst_name_body;
					if($test) echo '<img src="http://www.local.d/panel/public/admin/zdjecia/miniaturki/'.$name.'.jpg">';
					if(false && $znakWodny)
					$handle = @new upload($handle->file_dst_path.$this->nazwa);
					$handle->no_upload_check = true;
					$handle->file_overwrite = true;
					$handle->file_new_name = $this->nazwa;
					$handle->file_new_name_body = $name;
					$handle->image_resize = true;
					$handle->image_ratio = true;
					$handle->image_x = $this->obConfig->min_x_2;
					$handle->image_y = $this->obConfig->min_y_2;
					$handle->image_ratio_no_zoom_in = true;
					$handle->process($this->obConfig->ImageDir.''.$this->obConfig->Thumbs);
					if($handle->processed)
					{
						$name = $handle->file_dst_name_body;
						if($test) echo '<img src="http://www.local.d/panel/public/admin/zdjecia/thumbs/'.$name.'.jpg">';
						if(false && $znakWodny)
						$handle = @new upload($handle->file_dst_path.$this->nazwa);
						$handle->no_upload_check = true;
						$handle->file_overwrite = true;
						$handle->file_new_name = $this->nazwa;
						$handle->file_new_name_body = $name;
						$handle->image_resize = true;
						$handle->image_ratio = true;
						$handle->image_x = $this->obConfig->min_x_3;
						$handle->image_y = $this->obConfig->min_y_3;
						$handle->image_ratio_no_zoom_in = true;
						$handle->process($this->obConfig->ImageDir.''.$this->obConfig->Tiny);
						if($handle->processed)
						{
							$name = $handle->file_dst_name_body;
							if($test) echo '<img src="http://www.local.d/panel/public/admin/zdjecia/tiny/'.$name.'.jpg">';
							if(!$znakWodny) $handle->clean();
						}
						else return '<div class="k_blad">Błąd tworzenia miniaturki #3.<br/>'.$handle->error.'</div>';
					}
					else return '<div class="k_blad">Błąd tworzenia miniaturki #2.<br/>'.$handle->error.'</div>';
				}
				else return '<div class="k_blad">Błąd tworzenia miniaturki #1.<br/>'.$handle->error.'</div>';
			}
			else return '<div class="k_blad">Błąd tworzenia zdjęcia na serwerze.<br/>'.$handle->error.'</div>';
		}
		//die();
		return null;
	}

	private function uploadPhoto()
	{
		//$this->nazwa = strtr($this->nazwa, $this->polishBig);
		//$this->nazwa = strtr($this->nazwa, $this->polishSmall);
		//$this->nazwa = strtolower(str_replace(' ', '_', $this->nazwa));
		//while(strlen($rand = mt_rand()) < strlen(mt_getrandmax())) $rand = '0'.$rand;
		$typ = explode('/', $this->typ);
		$nazwa = md5(uniqid(mt_rand(), true));
		if(!empty($this->link)) $nazwa = $this->link;
		$this->nazwa = $nazwa.'.'.$typ[1];
		
		$this->lokalizacja = $this->sciezka.'/'.$this->nazwa;

		if(!move_uploaded_file($this->tmp, $this->lokalizacja))
		{
			if(!rename($this->tmp, $this->lokalizacja)) return 1;
		}
		
		$this->miniPhoto('full', 'width');
		$this->miniPhoto('small', 'both');
		$this->miniPhoto('thumb', 'both');
		$this->miniPhoto('tiny', 'both');
		return 0;		
	}

	public function miniPhoto($size = 'full', $aspect = 'both', $forceAspect = true)
	{
		list($width, $height) = getimagesize($this->lokalizacja);

		switch($size)
		{
			case 'full': 
				$nowaLokalizacja = $this->sciezka.'/'.$this->nazwa;
				$sizeWidth = $this->fullWidth;
				$sizeHeight = $this->fullHeight;
				break;
			case 'small': 
				$nowaLokalizacja = $this->sciezkaSmall.'/'.$this->nazwa;
				$sizeWidth = $this->smallWidth;
				$sizeHeight = $this->smallHeight;
				break;
			case 'thumb': 
				$nowaLokalizacja = $this->sciezkaThumb.'/'.$this->nazwa;
				$sizeWidth = $this->thumbWidth;
				$sizeHeight = $this->thumbHeight;
				break;
			case 'tiny': 
				$nowaLokalizacja = $this->sciezkaTiny.'/'.$this->nazwa;
				$sizeWidth = $this->tinyWidth;
				$sizeHeight = $this->tinyHeight;
				break;
		}
		
		if($width <= $sizeWidth && $height <= $sizeHeight) 
		{
			if($size != 'full') copy($this->lokalizacja, $nowaLokalizacja);
			return;
		}
		
		switch($aspect)
		{
			case 'both':
				$procentWidth = ($sizeWidth / $width) * 100;
				$procentHeight = ($sizeHeight / $height) * 100;
				$procent = min($procentWidth, $procentHeight);
				$newwidth = ($width * $procent) / 100;
				$newheight = ($height * $procent) / 100;
			break;
			case 'width':
				$newwidth = min($sizeWidth, $width);
				$procent = ($newwidth / $width) * 100;
				if($forceAspect) $newheight = ($height * $procent) / 100;
				else $newheight = min($sizeHeight, ($height * $procent) / 100);
			break;
			case 'height':
				$newheight = min($sizeHeight, $height);
				$procent = ($newheight / $height) * 100;
				if($forceAspect) $newwidth = ($width * $procent) / 100;
				else $newwidth = min($sizeWidth, ($width * $procent) / 100);
			break;
			case 'none':
				$newwidth = $sizeWidth;
				$newheight = $sizeHeight;
			break;
		}

		$thumb = imagecreatetruecolor($newwidth, $newheight);
		switch($this->typ)
		{
			case "image/png":
				$source = imagecreatefrompng($this->lokalizacja); break;
			case "image/gif":
				$source = imagecreatefromgif($this->lokalizacja); break;
			case "image/jpeg":
			case "image/jpg":
			case "image/pjpg":
			case "image/pjpeg":
			default: $source = imagecreatefromjpeg($this->lokalizacja);
		}

		imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		
		switch($this->typ)
		{
			case "image/png": imagepng($thumb, $nowaLokalizacja, 1); break;
			case "image/gif": imagegif($thumb, $nowaLokalizacja); break;
			case "image/jpeg":
			case "image/jpg":
			case "image/pjpg":
			case "image/pjpeg":
			default: imagejpeg($thumb, $nowaLokalizacja, 95);
		}	
	}
	
	public function resizeWatermark($path, $login)
	{
		//var_dump($path);die();		
		$zdj_info = pathinfo($path);
		$zdj = basename($path); //$zdj_info['basename'];
		$zdjNew = $zdj.'_'.$login.'.'.$zdj_info['extension'];
		if(@file_exists($zdjNew)) return array('file' => $zdjNew);
		//var_dump($zdj_info);die();
		
		if($login != "aletuczysto_pl") return array('file' => $zdj);
		
		if(@!file_exists($path)) return array('error' => 'Brak zdjęcia');
		$zdj_imagesize = @getimagesize($path);
		//var_dump($zdj_imagesize);die();
		$file_width = $zdj_imagesize[0];
		$file_height = $zdj_imagesize[1];
		
		$jm = "px";
		$www = 726; $hhh = 545;
		$top = 0; $bottom = 0; $left = 0; $right = 0;
		switch($login)
		{
			//case "james890": $top = 20; $bottom = 15; $left = 15; $right = 0; break;
			//case "profisys": $top = 5; $bottom = 20; $left = 5; $right = 5; break;
			//case "profi-system": $top = 20; $bottom = 20; $left = 5; $right = 5; break;
			//case "aletuczysto_pl": $top = 20; $bottom = 15; $left = 15; $right = 0; break;
			//case "higienaserwis24": $top = 5; $bottom = 20; $left = 5; $right = 5; break;
			case "james890": $top = 90; $bottom = 100; $left = 25; $right = 25; break;
			case "profisys": $top = 15; $bottom = 100; $left = 15; $right = 15; break;
			case "profi-system": $top = 90; $bottom = 100; $left = 25; $right = 25; break;
			case "aletuczysto_pl": $top = 120; $bottom = 90; $left = 120; $right = 0; break;
			case "higienaserwis24": $top = 15; $bottom = 100; $left = 15; $right = 15; break;
			default: $top = 0; $bottom = 0; $left = 0; $right = 0; break;
		}
		
		if(empty($login)) return array('error' => 'Brak loginu Allegro');
		$watermark = 'allegro_miniatury_'.$login.'.png';		
		$watermarkPath = 'admin/watermark/'.$watermark;
		if(@!file_exists($watermarkPath)) return array('file' => $zdj);
		
		$znak_imagesize = @getimagesize($watermarkPath);
		//var_dump($znak_imagesize);die();		
		$znak_width = $znak_imagesize[0];
		$znak_height = $znak_imagesize[1];
		$this->typ = $znak_imagesize['mime'];
		$mime = explode("/", $this->typ);
		$ext = @$mime[1];
		
		$top += 20;
		$bottom += 20;
		$ratioX = $znak_width / $file_width;
		if($ratioX < 1) $ratioX = 1;
		//echo('ratioX='.$ratioX.'<br>');
		$left *= $ratioX;
		$right *= $ratioX;
		//echo('left='.$left.'<br>');
		//echo('right='.$right.'<br>');
		$ratioY = $znak_height / $file_height;
		if($ratioY < 1) $ratioY = 1;
		//echo('ratioY='.$ratioY.'<br>');
		$top *= $ratioY;
		$bottom *= $ratioY;
		//echo('top='.$top.'<br>');
		//echo('bottom='.$bottom.'<br>');
		
		//echo('file_width='.$file_width.'<br>');
		//echo('file_height='.$file_height.'<br>');
		$file_width_new = $file_width;
		$file_height_new = $file_height;
		$file_width_new += $left + $right;
		$file_height_new += $top + $bottom;
		//$file_width_new = ceil($file_width * (1 + ($left + $right) / 100));
		//echo('file_width_new='.$file_width_new.'<br>');
		//$file_height_new = ceil($file_height * (1 + ($top + $bottom) / 100));
		//echo('file_height_new='.$file_height_new.'<br>');
		$ratio = $file_height_new / $file_width_new;
		//echo('ratio='.$ratio.'<br>');
		if(true && $ratio > 0.75)
		{
			$width_ratio = (int)($file_height_new * 1.33);
			//echo('width_ratio='.$width_ratio.'<br>');
			$width_added = max(0, $width_ratio - $file_width_new);
			//echo('width_added='.$width_added.'<br>');
			$left += ($width_added / 2);
			$right += ($width_added / 2);
			$file_width_new += $width_added;
			//$file_width_new = ceil($file_width * (1 + ($left + $right) / 100));
			//echo('file_width_new='.$file_width_new.'<br>');
		}
		
		$this->lokalizacja = $watermarkPath;
		$watermarkNew = $watermark.'_'.$file_width_new.'x'.$file_height_new;
		$nowaLokalizacja = 'admin/watermark/'.$watermarkNew.'.'.$ext;
		//var_dump($nowaLokalizacja);die();
		if(false || @!file_exists($nowaLokalizacja))
		{
			$array = array
			(
				'name' => $watermark, 
				'type' => $this->typ,
				'tmp_name' => $this->lokalizacja,
				'error' => 0,
				'size' => filesize($this->lokalizacja)
			);
			//var_dump($array);die();
			
			$handle = @new upload($array);
			$handle->no_upload_check = true;
			$handle->file_overwrite = true;
			$handle->file_new_name_body = $watermarkNew;
			$handle->image_resize = true;
			$handle->image_x = $file_width_new;
			$handle->image_y = $file_height_new;
			$handle->image_ratio = false;
			//$handle->image_border = $top.'% '.$right.'% '.$bottom.'% '.$left.'%';
			//$handle->image_ratio_no_zoom_in = true;
			$handle->process('../public/admin/watermark');
			if(!$handle->processed)
			return array('error' => $handle->error);
			
			echo '<img src="/public/'.$nowaLokalizacja.'" style="background:white;border:0px solid black;">';
			//die();
		}
		
		$image_watermark = $nowaLokalizacja;
		//$image_watermark_position = "TBLR";
		$image_watermark_x = 0;
		$image_watermark_y = 0;
		//var_dump($image_watermark);die();
		
		$this->sciezka = $zdj_info['dirname'];		
		$this->typ = $zdj_imagesize['mime'];
		$mime = explode("/", $this->typ);
		$ext = @$mime[1];
		
		$this->lokalizacja = $this->sciezka.'/'.$zdj;
		$nowaLokalizacja = $this->sciezka.'/'.$zdjNew;
		//$nowaLokalizacjaName = $zdj_info['filename'];
		//$nowaLokalizacjaName = str_replace('.'.$ext, '', $zdjNew);
		
		$array = array
		(
			'name' => $zdj, 
			'type' => $this->typ,
			'tmp_name' => $this->lokalizacja,
			'error' => 0,
			'size' => filesize($this->lokalizacja)
		);
		//var_dump($array);die();
		
		$handle = @new upload($array);
		$handle->no_upload_check = true;
		$handle->file_overwrite = true;
		$handle->file_new_name_body = $zdjNew;
		$handle->file_new_name_ext = "";
		$handle->file_force_extension = false;
		$handle->image_resize = false;
		$handle->image_x = $file_width;
		$handle->image_y = $file_height;
		$handle->image_ratio = true;
		//$handle->image_ratio_no_zoom_in = true;
		$handle->image_border = $top.$jm.' '.$right.$jm.' '.$bottom.$jm.' '.$left.$jm.'';
		$handle->image_watermark = $image_watermark;
		$handle->image_watermark_x = $image_watermark_x;
		$handle->image_watermark_y = $image_watermark_y;
		$handle->image_watermark_no_zoom_in = false;
		$handle->image_watermark_no_zoom_out = false;
		//$handle->image_watermark_position = $image_watermark_position;
		$handle->process($this->sciezka);	
		//var_dump($handle);die();
		if($handle->processed)
		{
			return array('file' => $zdjNew);
		}
		else return array('error' => $handle->error);
	}
}
?>