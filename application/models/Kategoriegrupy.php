<?php
class Kategoriegrupy extends Zend_Db_Table
{
	public $link, $id;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{
		$dane['lang'] = $this->lang;
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$this->update($dane, 'id = '.$this->id);
	}
	function wypisz()
	{
		$sql = $this->select()->where('1')->order('nazwa');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszIle()
	{
		$select = $this->db->select()->from(array('kg' => 'Kategoriegrupy'), array('*'))
			->joinleft(array('k' => 'Kategorie'), 'kg.id = k.grupa', array('count(k.id) as ile'))
			->where('1')->group('kg.id')->order(array('kg.nazwa'));
		//echo $select;die();
        $stmt = $select->query();
		$result = $stmt->fetchAll();
        return $result;
	}
	function wypiszKats()
	{
		$select = $this->db->select()->from(array('kg' => 'Kategoriegrupy'), array(''))
			->joinleft(array('k' => 'Kategorie'), 'kg.id = k.grupa', array('GROUP_CONCAT(k.id SEPARATOR ",") as kats'))
			->where('kg.id = '.$this->id);
		//echo $select;die();
        $stmt = $select->query();
		$result = $stmt->fetchAll();
        return @$result[0]['kats'];
	}
	function wypiszNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa = "'.$nazwa.'"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszWhereNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa like "%'.$nazwa.'%"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	
	function wypiszForAll($pole = 'id')
	{
		$result = $this->fetchAll();
		for($i = 0; $i < count($result); $i++)
		{
			$grupy[$result[$i][$pole]] = $result[$i][$pole == 'id' ? 'nazwa' : 'id'];
		}		
		return @$grupy;
	}
	function getGrupy($pole = 'nazwa')
	{
		$sql = $this->select()->order('nazwa');
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i][$pole]] = $result[$i]->toArray();
		}
		return @$results;
	}
	
	function usun()
	{
		$result = $this->delete('id = '.$this->id);
	}
	function wypiszPojedynczego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
}
?>