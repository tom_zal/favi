<?php
class Pocztapolskadane extends Zend_Db_Table
{
	public $id;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->module = $module;
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		try
		{
            $this->view = Zend_Layout::getMvcInstance()->getView();
        }
		catch(Zend_Exception $e)
		{
            die('Problem z pobraniem widoku');
        }
    }
	
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujZam($dane, $zam)
	{
		$where = 'id_zam = '.$zam;
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$zamow = new Zamowienia($this->module);
		$sql = $this->db->select()->from($this->_name, array('*'));
		$sql->joinleft(array('zk' => 'Zamowieniakontrahenci'), 'zk.id = id_zam', array('numer' => $zamow->nr));
		$sql->where('id_zam >= 0');
		$result = $this->db->fetchAll($sql);
		return $result;
	}	
	function wypiszNumer($nrNad = "")
	{
		$zamow = new Zamowienia($this->module);
		$sql = $this->db->select()->from($this->_name, array('*'));
		$sql->joinleft(array('zk' => 'Zamowieniakontrahenci'), 'zk.id = id_zam', array('numer' => $zamow->nr));
		$sql->where('nr_nad = "'.$nrNad.'"');
		$result = $this->db->fetchRow($sql);//echo $sql;
		return $result;
	}
	function wypiszSprStatus()
	{
		$zamow = new Zamowienia($this->module);
		$sql = $this->db->select()->from($this->_name, array('*'));
		$sql->joinleft(array('zk' => 'Zamowieniakontrahenci'), 'zk.id = id_zam', array('email','numer' => $zamow->nr));
		$sql->where('bufor > 0 and data_wysylka <> "0000-00-00 00:00:00" and status_konczace = 0');
		$result = $this->db->fetchAll($sql);
		return $result;
	}
	function wypiszZam($id, $multi = true)
	{
		if($multi) $result = $this->fetchAll('id_zam = '.$id);
		else $result = $this->fetchRow('id_zam = '.$id);
		return $result;
	}
	function wypiszJeden()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function usunJeden()
	{
		$result = $this->delete('id = '.$this->id);
	}
	function usunGUID($guid = "")
	{
		$result = $this->delete('guid = "'.$guid.'"');
	}
	
	function zapisz($dane)
	{
		$id = @intval($dane['id_zam']);
		$exist = $this->wypiszZam($id, false);
		if(count($exist) > 0)
		return $this->edytujZam($dane, $id);
		else return $this->dodaj($dane);
	}
	
	function urzadWydaniaEPrzesylkiPobierz()
	{
		if(!isset($_SESSION['urzadWydaniaEPrzesylki']))
		{
			try
			{
				$en = new ElektronicznyNadawca();
				$ur = new getUrzedyWydajaceEPrzesylki();
				$urzedy = $en->getUrzedyWydajaceEPrzesylki($ur);
				//$pocztapolskadane = new Pocztapolskadane();
				$urzadWydaniaEPrzesylki = $urzedy['urzadWydaniaEPrzesylki'];
				//$urzadWydaniaEPrzesylki = $this->urzadWydaniaEPrzesylkiSort($urzadWydaniaEPrzesylki);
				$_SESSION['urzadWydaniaEPrzesylki'] = $urzadWydaniaEPrzesylki;
				//var_dump(count($_SESSION['urzadWydaniaEPrzesylki']));die();
			}
			catch(SoapFault $error)
			{
				//4270
				/*
					'id' => int 346935
					'wojewodztwo' => string 'mazowieckie' (length=11)
					'powiat' => string 'Warszawa' (length=8)
					'miejsce' => string 'śródmieście' (length=14)
					'kodPocztowy' => string '00001' (length=5)
					'miejscowosc' => string 'Warszawa' (length=8)
					'ulica' => string 'Mikołaja Kopernika' (length=19)
					'numerDomu' => string '36/40' (length=5)
				*/
				return '<div class="k_blad">Nie można pobrać punktów odbioru przesyłki<br/>'.$error->getMessage().'</div>';
			}
		}
	}
	function urzadWydaniaEPrzesylkiSzukaj($urzadWydaniaEPrzesylkiInput = "", $inputs = null, $or = true)
	{
		if(!isset($_SESSION['urzadWydaniaEPrzesylki'])) $this->urzadWydaniaEPrzesylkiPobierz();
		
		if(@mb_strlen($urzadWydaniaEPrzesylkiInput, "UTF-8") >= 3)
		{
			$urzadWydaniaEPrzesylki = null;
			$input = $urzadWydaniaEPrzesylkiInput;
			if(@count($_SESSION['urzadWydaniaEPrzesylki']) > 0)
			foreach($_SESSION['urzadWydaniaEPrzesylki'] as $urzad)
			{
				if(mb_stripos($urzad['miejscowosc'], $input, 0, "UTF-8") !== false
				//|| mb_stripos($urzad['wojewodztwo'], $input, 0, "UTF-8") !== false
				//|| mb_stripos($urzad['powiat'], $input, 0, "UTF-8") !== false
				//|| mb_stripos($urzad['miejsce'], $input, 0, "UTF-8") !== false
				|| mb_stripos($urzad['kodPocztowy'], $input, 0, "UTF-8") !== false)
				//|| @mb_stripos($urzad['ulica'], $input, 0, "UTF-8") !== false
				//|| @mb_stripos($urzad['numerDomu'], $input, 0, "UTF-8") !== false
				$urzadWydaniaEPrzesylki[$urzad['id']] = $urzad;
			}
			//$pocztapolskadane = new Pocztapolskadane();
			return $this->urzadWydaniaEPrzesylkiSort($urzadWydaniaEPrzesylki);
			//$this->view->urzadWydaniaEPrzesylki = $urzadWydaniaEPrzesylki;
		}
		if(count($inputs) > 0)
		{
			$urzadWydaniaEPrzesylki = null;
			if(@count($_SESSION['urzadWydaniaEPrzesylki']) > 0)
			foreach($_SESSION['urzadWydaniaEPrzesylki'] as $urzad)
			{
				$ok = !$or;
				foreach($inputs as $co => $input)
				{
					if(!empty($input) && !empty($urzad[$co]))
					{
						$string = $urzad[$co]; $szukaj = $input;
						if($co == "ulica") { $string = $input; $szukaj = $urzad[$co]; }
						if($or) $ok = $ok || @mb_stripos($string, $szukaj, 0, "UTF-8") !== false;
						else $ok = $ok && @mb_stripos($string, $szukaj, 0, "UTF-8") !== false;
					}
				}
				if($ok) $urzadWydaniaEPrzesylki[$urzad['id']] = $urzad;
			}
			return $this->urzadWydaniaEPrzesylkiSort($urzadWydaniaEPrzesylki);
		}
    }
	function urzadWydaniaEPrzesylkiSort($result = null, $asoc = true)
	{
		$this->sortFunc = '';
		$this->sorter = 'miejscowosc';
		$this->sorter2 = 'ulica';
		$this->sortOrder = 'asc';
        if(is_array($result) && !empty($result) && count($result) > 0)
		{
			if($asoc) uasort($result, array($this, 'urzadWydaniaEPrzesylkiCmp'));
			else usort($result, array($this, 'urzadWydaniaEPrzesylkiCmp'));
		}
		return $result;
    }
	function urzadWydaniaEPrzesylkiMiasta()
	{
		if(!isset($_SESSION['urzadWydaniaEPrzesylkiMiasta']))
		{
			if(!isset($_SESSION['urzadWydaniaEPrzesylki'])) $this->urzadWydaniaEPrzesylkiPobierz();
			if(@count($_SESSION['urzadWydaniaEPrzesylki']) > 0)
			foreach($_SESSION['urzadWydaniaEPrzesylki'] as $urzad)
			{
				$miasto = $urzad['miejscowosc'];
				$miasta[$miasto] = $miasto;
			}
			//setlocale(LC_ALL, 'pl_PL.UTF-8');
			//@ksort($miasta, SORT_LOCALE_STRING);
			//@uksort($miasta, 'strcoll');
			@uksort($miasta, array($this, 'cmpPL'));
			//var_dump($miasta);die();
			return $_SESSION['urzadWydaniaEPrzesylkiMiasta'] = @$miasta;
		}
		else return $_SESSION['urzadWydaniaEPrzesylkiMiasta'];
    }
	function cmpPL($a, $b)
	{
		//echo $this->common->usunPolskieZnaki($a).' '.$this->common->usunPolskieZnaki($b).'<br>';
		return strcasecmp($this->common->usunPolskieZnaki($a, true), $this->common->usunPolskieZnaki($b, true));
	}
	function urzadWydaniaEPrzesylkiCmp($a, $b)
	{
        $cmp1 = @$a[$this->sorter];
		$cmp2 = @$b[$this->sorter];
		if(isset($this->sortFunc) && !empty($this->sortFunc) && function_exists($this->sortFunc))
		{
			$sortFunc = $this->sortFunc;
			$cmp1 = $sortFunc($cmp1);
			$cmp2 = $sortFunc($cmp2);
		}
		if($cmp1 == $cmp2)
		{
			if(@!empty($this->sorter2))
			{
				$cmp1 = @$a[$this->sorter2];
				$cmp2 = @$b[$this->sorter2];
				if($cmp1 == $cmp2) return 0;
			}
			else return 0;
		}
		if($this->sortOrder == 'asc') return ($cmp1 < $cmp2) ? -1 : 1;
		if($this->sortOrder == 'desc') return ($cmp1 > $cmp2) ? -1 : 1;
    }
	function setDataPocztaPolska()
	{
		$pocztapolska = new Zend_Session_Namespace('pocztapolska');
		if(!isset($pocztapolska->miasto)) $pocztapolska->miasto = '';
		if(!isset($pocztapolska->urzadWydania)) $pocztapolska->urzadWydania = '';
		if(isset($this->_get['miasto'])) $pocztapolska->miasto = $this->_get['miasto'];
		if(isset($this->_get['urzadWydania'])) $pocztapolska->urzadWydania = $this->_get['urzadWydania'];
		$this->view->urzadWydania = $pocztapolska->urzadWydania;
		$this->view->urzadWydaniaMiasto = $pocztapolska->miasto;
		$this->view->urzedyWydania = ($pocztapolska->miasto != '') ? $this->urzadWydaniaEPrzesylkiSzukaj($pocztapolska->miasto) : '';
	}
	function getToCart()
	{
		if($this->view->obConfig->pocztapolska)
		{
			$this->view->urzedyWydaniaMiasta = $this->urzadWydaniaEPrzesylkiMiasta();
			$this->setDataPocztaPolska();
		}
	}
}
?>