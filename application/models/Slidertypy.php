<?php
class Slidertypy extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
    function wypiszAll()
	{
        $result = $this->fetchAll();
        return $result;
    }
    function dodaj($dane, $lang="pl")
	{
        $dane['lang'] = $this->lang;
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }
    function edytuj($dane, $id)
	{
        $where = 'id =' . $id;
        $this->update($dane, $where);
    }
    function pojedyncza($id = 0)
	{
        $where = 'id = '.$id;
        $print = $this->fetchRow($where);
        return $print;
    }
    function wypisz($lang = "pl")
	{
        $where = 'lang = "'.$this->lang.'"';
		$order = array('data DESC');
        $result = $this->fetchAll($where, $order);
        return $result;
    }
}
?>