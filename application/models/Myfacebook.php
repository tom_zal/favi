<?php
    class Myfacebook extends Facebook
    {
        private $fcbid, $fcbdata,$profile = null;

        public function __construct($config)
        {
            parent::__construct($config);
        }
        public function connectFcb()
        {
            $user = $this->getUser();
            if($user !=0)
            {
                try
                {
                   $this->profile = $this->api('/me');
                }
                catch (FacebookApiException $e) {
                   $this->profile = null;
                }
            }
            return $this->profile;
        }
    }
?>