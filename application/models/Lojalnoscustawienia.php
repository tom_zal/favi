<?php
class Lojalnoscustawienia extends Zend_Db_Table
{
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
	function updateData($array)
	{
		$where = 'id = 1';
		$this->update($array, $where);
	}
	
	function showData()
	{
		$result = $this->fetchAll();
		return $result;
	}
	
	function wypisz()
	{
		$result = $this->select()->where('id = 1');
		return $this->fetchRow($result);
	}
	function changePodstrona($tekst, $klient = null)
	{
		$ustawienia = $this->wypisz();
		$tekst = str_replace('TERMIN', $ustawienia['rej_do_zam'], $tekst);
		$tekst = str_replace('PROCENT_STANDARD', $ustawienia['rabat_standard'], $tekst);
		$tekst = str_replace('PROCENT_POLECANY', $ustawienia['rabat_polecony'], $tekst);
		$tekst = str_replace('PROCENT_POLECAJACY', $ustawienia['rabat_polecajacy'], $tekst);
		if(false)
		{
			$rabaty = new Lojalnoscrabaty();
			$minRabat = $rabaty->wypiszMinWartosc();
			$tekst = str_replace('MIN_RABAT', $minRabat[0]['wartosc'], $tekst);
		}
		$tekst = str_replace('MIN_RABAT', $ustawienia['min_rabat'], $tekst);
		$tekst = str_replace('MIN_KWOTA', $ustawienia['min_kwota'], $tekst);
		if(count($klient) > 0)
		{
			$tekst = str_replace('NAZWISKO_KLIENTA', $klient['imie'].' '.$klient['nazwisko'], $tekst);
			$tekst = str_replace('NUMER_KARTY_REJESTR', $klient['karta'], $tekst);
			
			if($klient['polecajacy'] > 0)
			$tekst = str_replace('PROCENT_ZALOGOWANY', $ustawienia['rabat_polecony'], $tekst);
			else
			$tekst = str_replace('PROCENT_ZALOGOWANY', $ustawienia['rabat_standard'], $tekst);			
			
			if(true)
			{
			$zamowienia = new Zamowienia();
			$warunek = $zamowienia->getStanKontaWarunek($klient, $ustawienia);
			$warunekPolecane = $zamowienia->getStanKontaWarunekPolecane($klient, $ustawienia);
			$warunekRazem = floatval($warunek['wartosc'] + $warunekPolecane['wartosc']);
			$tekst = str_replace('STAN_KONTA_RABAT_DOSTĘPNE', $klient['konto'], $tekst);
			$tekst = str_replace('STAN_KONTA_RABAT_WARUNEK', $warunekRazem, $tekst);
			//$tekst=str_replace('STAN_KONTA_RABAT_WARUNEK',$warunek['wartosc'].'+'.$warunekPolecane['wartosc'],$tekst);
			$tekst = str_replace('STAN_KONTA_RABAT_RAZEM', $klient['konto'] + $warunekRazem, $tekst);
			}
		}
		return $tekst;
		
		//SELECT `zk`.`data_rej`, `zk`.`data`, DATE_ADD(zk.data_rej, INTERVAL 2 DAY) AS `termin`, sum(ceil(wartosc_zamowienia * (3.00 / 100))) AS `wartosc`, `lp`.`wykorzystano` FROM `Zamowieniakontrahenci` AS `zk` INNER JOIN `Lojalnoscpolecenia` AS `lp` ON zk.email = lp.polecany and lp.polecajacy = 14 and zk.data < termin WHERE (email in (Select email from Kontrahenci where polecajacy = 14) and potwierdzenie = 1 and status <> "anulowane" and online_typ <> "odrzucona" and online_typ <> "anulowana" and online_typ <> "zakończona" and wartosc_zamowienia >= 0.00 and zk.data < termin)
	}
}
?>