<?php
class PlatnosciProdukty
{
    public $tabelaKontrahenci, $tabelaProdukty;
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);

        $this->dostawa = 'Sposobdostawy';
        $this->platnosc = 'Sposobplatnosci';
		$this->platnosciDostawy = 'Platnosci';
        $this->wartosc = 'Platnosciprodukty';
        $this->online = 'Online';
        $this->zamowieniaKontahent = 'Zamowieniakontrahenci';
		$this->platnosciProdukty = 'Platnosciprodukty';
    }
	
	function getPlatnosciForExport($ids)
	{
		if(!$this->common->isArray($ids, true)) return null;
        $select = $this->db->select()->from(array('pp' => 'Platnosciprodukty'), array('id_produkt', 
			new Zend_Db_Expr('GROUP_CONCAT(DISTINCT id_platnosci ORDER BY id_platnosci ASC SEPARATOR "|") as plat')))
			->where('id_produkt in('.implode(',',$ids).')')->group('id_produkt')->order(array('id_produkt asc'));
		//echo $select;die();
		$result = $this->db->fetchAll($select);
        for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i]['id_produkt']] = $result[$i]['plat'];
		}
		return @$results;
    }

    public function deleteWybrany($id)
	{
        $where = 'id_platnosci = '.$id;
        $this->db->delete($this->wartosc, $where);
    }
	function usunProdukt($id)
	{
		$this->db->delete($this->wartosc, 'id_produkt = '.$id);
	}
	function usunProduktyIds($ids)
	{
		$result = $this->delete('id_produkt in ('.$ids.')');
	}
	function usunProduktPlatnosc($produktID = 0, $platnoscID = 0)
	{
		$this->db->delete($this->wartosc, 'id_produkt = '.$produktID.' and id_platnosci = '.$platnoscID);
	}

    public function addWartosc($dane)
	{
        $this->db->insert($this->wartosc, $dane);
    }

    public function selectdostawy($id)
	{
        $where = 'id = '.$id;
        $select = $this->db->select()->from($this->dostawa)->where($where);
        $result = $this->db->fetchAll($select);
        return $result[0];
    }

    public function selectdostawyAll()
	{
        $select = $this->db->select()->from($this->dostawa);
        $result = $this->db->fetchAll($select);
        return $result;
    }

    public function selectplatnosci($id)
	{
        $where = 'id = '.$id;//.' AND widoczny = 1';
        //echo $where;
        $select = $this->db->select()->from($this->platnosc)->where($where);
        $result = $this->db->fetchAll($select);
        return $result[0];
    }

    public function selectplatnosciAll()
	{
        $select = $this->db->select()->from($this->platnosc);//->where('widoczny = 1');
        $result = $this->db->fetchAll($select);
        return $result;
    }
	
	public function selectPlatnosciDostawy($id)
	{
        $select = $this->db->select()->from($this->platnosciDostawy)->where(' id = ' . $id);
        $result = $this->db->fetchAll($select);
        return $result;
    }

    public function selectwartosci($id)
	{
        $select = $this->db->select()
			->from(array('pp' => 'Platnosciprodukty'),array('*'))
			->join(array('p' => 'Platnosci'),'p.id = pp.id_platnosci',array('*'))
			->where('pp.id_produkt = ' . $id);
        $result = $this->db->fetchAll($select);
        return $result;
    }

    public function WypiszDostawyDlaPlatnosci($platnosc)
	{
        $where = 'platnosc = '.$platnosc;
        $select = $this->db->select()->from($this->wartosc)->where($where);
        $result = $this->db->fetchAll($select);
        for($i=0; $i<count($result); $i++)
		{
            $where = 'id = ' . $result[$i]['dostawa'];
            $select = $this->db->select()->from($this->dostawa)->where($where);
            $sposobydostawy = $this->db->fetchAll($select);
            $dostawy[] = $sposobydostawy[0];
        }
        return $dostawy;
    }
    public function WypiszWartoscDostawy($dostawa, $platnosc)
	{
        $where = 'platnosc = '.$platnosc.' AND dostawa = '.$dostawa;
        $select = $this->db->select()->from($this->wartosc)->where($where);
        $result = $this->db->fetchAll($select);
        return $result[0];
    }
    public function platnosc($id)
	{
        $where = 'id = '.$id;
        $select = $this->db->select()->from($this->wartosc)->where($where);
        $result = $this->db->fetchAll($select);
        $nazwaplatnosci = $this->selectplatnosci($result[0]['platnosc']);
        $result[0]['platnosc'] = $nazwaplatnosci['nazwa'];
        $result[0]['id_platnosc'] = $nazwaplatnosci['id'];
        $nazwadostawy = $this->selectdostawy($result[0]['dostawa']);
        $result[0]['dostawa'] = $nazwadostawy['nazwa'];
        $result[0]['id_dostawa'] = $nazwadostawy['id'];
		$result[0]['apikurier'] = @$nazwadostawy['apikurier'];
        return $result[0];
    }

    public function wypiszWszystkie($id) 
	{
        $lista = $this->selectwartosci($id);
        for ($i = 0; $i < count($lista); $i++) 
		{
			$idPlatnosci = $lista[$i]['id_platnosci'];
			$platnoscDostawa =  $this->selectPlatnosciDostawy($idPlatnosci);
			if(count($platnoscDostawa) == 0) continue;
			//var_dump($platnoscDostawa);
            $nazwaplatnosci = $this->selectplatnosci($platnoscDostawa[0]['platnosc']);
			$lista[$i]['id_platnosc'] = $lista[$i]['platnosc'];
            $lista[$i]['platnosc'] = $nazwaplatnosci['nazwa'];
            $nazwadostawy = $this->selectdostawy($platnoscDostawa[0]['dostawa']);
			$lista[$i]['id_dostawa'] = $lista[$i]['dostawa'];
            $lista[$i]['dostawa'] = $nazwadostawy['nazwa'];
			$lista[$i]['apikurier'] = @$nazwadostawy['apikurier'];
			$platnosci[$idPlatnosci] = $lista[$i];
        }
        return @$platnosci;
    }
}
?>