<?php
class Lojalnoscpolecenia extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db, $sumajednkonto, $sumajedn, $suma, $sumakonto;
	public $drzewo, $polecenia, $dzieci, $klient, $klienci, $aktualne, $archiwum, $admin, $settings;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		$this->admin = $module == 'admin';
		$this->settings = $this->common->getUstawienia();
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujPolecany($dane, $mail)
	{
		$where = 'polecany = "'.$mail.'"';
		$this->update($dane, $where);
	}
	function wypiszAll()
	{
		$result = $this->fetchAll();
		return $result;
	}
	function wypiszID()
	{
		$sql = $this->select()->where('id = '.$this->id);
		$result = $this->fetchRow($sql);
		return $result;
	}
	function wypiszPolecajacy($id)
	{
		$sql = $this->select()->where('polecajacy = '.$id);
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszPolecajacyAktywne($id)
	{
		$where = 'termin >= "'.date('Y-m-d').'" and wykorzystano = "0000-00-00"';
		//$sql = $this->select()->where('polecajacy = '.$id.' and '.$where);
		$select = $this->db->select()
			->from(array('lp' => 'Lojalnoscpolecenia'), array('termin','data','wykorzystano'))
			->join(array('k' => 'Kontrahenci'), 'k.email = lp.polecany', array('*'))
			->where('lp.polecajacy = '.$id.' and '.$where)->order('wykorzystano desc');
		//echo $select;//die();
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszPolecajacyWykorzystane($id)
	{
		$where = 'wykorzystano <> "0000-00-00"';
		$select = $this->db->select()
			->from(array('lp' => 'Lojalnoscpolecenia'), array('termin','data','wykorzystano'))
			->join(array('k' => 'Kontrahenci'), 'k.email = lp.polecany', array('*'))
			->where('lp.polecajacy = '.$id.' and '.$where)->order('wykorzystano desc');
		//echo $select;//die();
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszIluPolecajacych()
	{
		$where = 'wykorzystano <> "0000-00-00"';
		$select = $this->db->select()
			->from(array('lp' => 'Lojalnoscpolecenia'), array(''))
			->join(array('k' => 'Kontrahenci'), 'k.id = lp.polecajacy', array('count(distinct k.id) as ilosc'))
			->where('1 and '.$where);
		//echo $select;//die();
		$result = $this->db->fetchRow($select);
		return @intval($result['ilosc']);
	}
	function wypiszPolecajacych($szukanie)
	{
		$od = isset($szukanie->od) ? $szukanie->od : 0;
		$ile = isset($szukanie->ile) ? $szukanie->ile : 10;		
		$where = 'wykorzystano <> "0000-00-00"';
		$select = $this->db->select()
			->from(array('lp' => 'Lojalnoscpolecenia'), array('polecany','termin','data','wykorzystano'))
			->join(array('k' => 'Kontrahenci'), 'k.id = lp.polecajacy', array('*'))
			->where('1 and '.$where)->group('k.id')->order('k.nazwisko asc')->limit($ile, $od * $ile);
		//echo $select;//die();
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszPolecany($mail)
	{
		$sql = $this->select()->where('polecany = "'.$mail.'"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszPolecanyAktywny($mail)
	{
		$where = 'termin >= "'.date('Y-m-d').'" and wykorzystano = "0000-00-00"';
		$sql = $this->select()->where('polecany = "'.$mail.'" and '.$where);
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszPolecenie($id, $mail)
	{
		$sql = $this->select()->where('polecajacy = '.$id.' and polecany = "'.$mail.'"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function usun()
	{
		$result = $this->delete('id = '.$this->id);
	}
	function usunPolecajacy($id = 0)
	{
		$result = $this->delete('polecajacy = '.$id);	
	}
	function usunPolecany($mail = '')
	{
		$result = $this->delete('polecany = "'.$mail.'"');
	}
	function usunPolecenie($id = 0, $mail = '')
	{
		$result = $this->delete('polecajacy = '.$id.' and polecany = "'.$mail.'"');
	}
	
	public function getAllWithDetails($polecajacy = 0)
	{
		$where = $polecajacy < 0 ? 'polecajacy = '.$polecajacy : 1;
		if(false) $where .= ' and wykorzystano <> "0000-00-00"';
		$sql = 'SELECT *, (SELECT count(*) FROM Kontrahenci where polecajacy = lp.polecajacy) as dzieci FROM Lojalnoscpolecenia as lp where '.$where.' order by wykorzystano desc';
		//echo $sql;
		//return;
		$kontrahent = new Kontrahenci();
		$this->klienci = $kontrahent->getKontrahenci('email');
		$zamowienia = new Zamowienia();
		$archiwum = new Archiwum();
		$this->aktualne = $zamowienia->getSumyKlientow('email', false);
		$this->archiwum = $archiwum->getSumyKlientow('email', true);
        $result = $this->db->fetchAll($sql);
		for($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i]['id']] = $result[$i];
			$klient = @$this->klienci[$result[$i]['polecany']];
			unset($result[$i]['dzieci']);
			$dzieci[$result[$i]['polecajacy']][$klient['id']]['polecenie'] = $result[$i];
			$dzieci[$result[$i]['polecajacy']][$klient['id']]['klient'] = $klient;
		}
		$dzieci[0] = $kontrahent->getKontrahenci('id', 'polecajacy = 0');
		if(count($dzieci[0]) > 0)
		foreach($dzieci[0] as $id => $rodzic)
		{
			unset($dzieci[0][$id]);
			$dzieci[0][$id]['klient'] = $rodzic;
			$dzieci[0][$id]['polecenie'] = null;
		}
		$this->polecenia = @$results;
		$this->dzieci = @$dzieci;		
    }
	
	function getWartosciZamowien($parent = 0, $polecenia = null)
	{
		$rows = @$this->dzieci[$parent];
		if(count($rows) > 0)
		foreach($rows as $id => $row)
		{
			$klient = $row['klient'];
			//$polecenie = $row['polecenie'];
			$this->suma[$id] = @floatval($this->aktualne[$klient['email']]);
			$this->suma[$id] += @floatval($this->archiwum[$klient['email']]);
			$this->sumakonto[$id] = $klient['konto'];
			
			$kl = !empty($klient['polecajacy']) ? $klient['polecajacy'] : null;
			if(false && $kl != null)
			{
				if(!isset($this->sumajedn[$kl])) $this->sumajedn[$kl] = 0;
				$this->sumajedn[$kl] += $this->suma[$id];
 
				if(!isset($this->sumajednkonto[$kl])) $this->sumajednkonto[$kl] = 0;
				$this->sumajednkonto[$kl] += $this->sumakonto[$id];
			}
			
			if(count($polecenia) > 0)
			foreach($polecenia as $polecony)
			{
				//if(!in_array($polecony, array_keys($rows)))
				$this->suma[$polecony] += $this->suma[$id];
				$this->sumakonto[$polecony] += $this->sumakonto[$id];
			}
			if(@count($this->dzieci[$id]) > 0)// && ($this->admin || $parent != $this->klient))
			{
				$polecenia[$id] = $id;
				$this->getWartosciZamowien($id, $polecenia);
				unset($polecenia[$id]);
			}
		}
	}
	
	function display_children_admin($parent = 0, $level = 0)
	{
		$baseUrl = str_replace('//', '/', '/'.$this->obConfig->baseUrl.'/');
		$rows = @$this->dzieci[$parent];
		if(count($rows) > 0)
		foreach($rows as $id => $row)
		{
			if($parent==0 && @count($this->dzieci[$id])==0 && $this->suma[$id]==0 && $id!=$this->klient) continue;
			//if(@count($this->dzieci[$id]) == 0 && $this->suma[$id] == 0 && $id != $this->klient) continue;
			$klient = $row['klient'];			
			//$rodzic = $this->klienci[$parent['email']];
			//$polecenie = $row['polecenie'];
			//$polecajacy = $this->klienci[$rodzic['email']];
			$aktualne = @floatval($this->aktualne[$klient['email']]);
			$archiwum = @floatval($this->archiwum[$klient['email']]);
			
			$this->drzewo .= '<li id="klient'.$id.'">';
			$hasPeople = (@count($this->dzieci[$id]) > 0 ? 'hasPeople': 'himself');
			$this->drzewo .= '<a class="link '.$hasPeople.'" href="javascript:void(0);">';
			$this->drzewo .= '<b>'.$klient['imie'].' '.$klient['nazwisko'].':</b>';
			$this->drzewo .= '</a>';
			
			//$this->drzewo .= '<span>&nbsp;'.$this->suma[$id].'&nbsp;zł</span>';
			$this->drzewo .= '<span class="row-cash">';
			if($this->admin)
			{
				$this->drzewo .= '<span><strong>Stan konta:</strong>&nbsp;';
				$this->drzewo .= number_format($klient['konto'],2,'.','').'&nbsp;zł</span><b>|</b>';
			}
			if(true)
			{
				$this->drzewo .= '<span><strong>Zakupy:</strong>&nbsp;';
				$this->drzewo .= ($aktualne + $archiwum).'&nbsp;zł</span><b>|</b>';
			}			
			if($this->admin)
			if(@count($this->dzieci[$id]) > 0)
			{
				$this->drzewo .= '<span><strong>Stan konta znajomych</strong>&nbsp;';
				//$this->drzewo .= number_format($this->sumajednkonto[$id],2,'.','').'&nbsp;zł</span><b>|</b>';
				$znajomiKonto = $this->sumakonto[$id] - floatval($klient['konto']);
				$this->drzewo .= number_format($znajomiKonto,2,'.','').'&nbsp;zł</span><b>|</b>';
				$this->drzewo .= '<span><strong>Znajomi kupili</strong>&nbsp;';
				//$this->drzewo .= number_format($this->sumajedn[$id],2,'.','').'&nbsp;zł</span><b>|</b>';
				$znajomiKupili = $this->suma[$id] - ($aktualne + $archiwum);
				$this->drzewo .= number_format($znajomiKupili,2,'.','').'&nbsp;zł</span><b>|</b>';
			}
			if(!$this->admin)
			if(@count($this->dzieci[$id]) > 0)
			{
				$this->drzewo .= '<span><strong>Grupa kupiła</strong>&nbsp;';
				$this->drzewo .= $this->suma[$id].'&nbsp;zł</span><b>|</b>';
			}			
            if(false && @count($this->dzieci[$id]) > 0)
			{
                $this->drzewo .='<b>|</b>
					<strong>Sortuj grupę wg: &nbsp;</strong><span>
                    <select name="sortuj">
                        <option>wybierz</option>
                        <option>nazwiska</option>
                        <option>zamówień</option>
                    </select></span>';
            }
            $this->drzewo .= '</span>';
			
			if($this->admin)// && ($aktualne + $archiwum) > 0)
			{
				$this->drzewo .= '<a href="'.$baseUrl.'admin/lojalnosc/pokaz/id/'.$klient['id'].'" ';
				$this->drzewo .= 'class="zobacz" onclick="window.location=this.href;">analiza</a>';
			}
			if($this->admin)
			{
				$this->drzewo .= '<a href="'.$baseUrl.'admin/kontrahent/mail/mailid/'.$klient['id'].'" ';
				$this->drzewo .= 'class="zobacz" onclick="window.location=this.href;">e-mail</a>';
			}
			if($this->admin && $this->obConfig->sms)
			{
				$this->drzewo .= '<a href="'.$baseUrl.'admin/kontrahent/sms/smsid/'.$klient['id'].'" ';
				$this->drzewo .= 'class="zobacz" onclick="window.location=this.href;">sms</a>';
			}
			if($parent == $this->klient && !$this->admin && $this->settings['usun_polecany'])
			{
				$warunek = 'Czy na pewno chcesz usunąć tą poleconą przez Ciebie osobę ('.$klient['imie'].' '.$klient['nazwisko'].') ? \nOd tej pory nie będziesz już otrzymywać prowizji od jej zakupów z tytułu polecenia ! \nOsoba ta nie będzie już otrzymywać prowizji od swoich zakupów z tytułu bycia poleconym przez Ciebie!';
				$warunek = "window.confirm('".$warunek."')";
				$this->drzewo .= '<a href="'.$baseUrl.'kontrahent/lojalnosc/polecany/usun/id/'.$klient['id'].'" ';
				$this->drzewo .= 'class="zobacz" onclick="if('.$warunek.')window.location=this.href;">usuń</a>';
			}
			if(@isset($this->dzieci[$id][$this->klient]) && !$this->admin && $this->settings['usun_polecajacy'])
			{
				$warunek = 'Czy na pewno chcesz usunąć osobę która Cię poleciła ('.$klient['imie'].' '.$klient['nazwisko'].') ? \nOd tej pory nie będziesz już otrzymywać prowizji od Twoich zakupów z tytułu bycia poleconym ! \nOsoba ta nie będzie od tej pory otrzymywać prowizji od Twoich zakupów z tytułu polecenia Ciebie !';
				$warunek = "window.confirm('".$warunek."')";
				$this->drzewo .= '<a href="'.$baseUrl.'kontrahent/lojalnosc/polecajacy/usun/id/'.$klient['id'].'" ';
				$this->drzewo .= 'class="zobacz" onclick="if('.$warunek.')window.location=this.href;">usuń</a>';
			}
			if(@count($this->dzieci[$id]) > 0 && ($this->admin || $parent != $this->klient))
			{
				$this->drzewo .= '<ul>';
				$this->display_children_admin($id, $level + 1);
				$this->drzewo .= '</ul>';
			}
			
			$this->drzewo .= '</li>';
		}
	}
}
?>