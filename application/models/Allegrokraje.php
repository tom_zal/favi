<?php
class Allegrokraje extends Zend_Db_Table
{
    protected $_name = 'Allegrokraje';
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

    function dodaj($dane)
	{
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }

    function edytuj($dane)
	{
        $where = 'id = "'.$this->id.'"';
        $this->update($dane, $where);
    }

    function usun()
	{
        $where = 'id = "'.$this->id.'"';
        $this->delete($where);
    }
	
	function getOneByID()
	{
        $where = 'id = "'.$this->id.'"';
        $result = $this->fetchRow($where);
        return $result->toArray();
    }	
	
	function getAll()
	{
		$sql = $this->select()->order('country-name', 'state-name');
        $result = $this->fetchAll($sql);
        $res = $result->toArray();
        return $res;
    }
	
	function getCountries()
	{
		$sql = $this->select()->group('country-name')->order('country-name');
        $result = $this->fetchAll($sql);
        $res = $result->toArray();
        return $res;
    }	
	function getCountriesDrzewo($cache = false, $caching = false)
	{
		if(false) $result = $cache->load($this->_name); else $result = false;
		if($result === false || !$caching) 
		{
			$sql = $this->select()->group('country-id')->order('country-name');
			$result = $this->fetchAll($sql)->toArray();
			if(false) $cache->save($result, $this->_name, array($this->_name));
		}		
		return $result;
	}
	function getRegions($name, $sort = true)
	{
		$sql = $this->select()->where('`country-name` = "'.$name.'" and `state-name` <> ""')->order('state-name');
        $result = $this->fetchAll($sql);
		if($sort)
		{
			for($i=0; $i<count($result); $i++)
			{
				$results[$result[$i]['state-id']] = $result[$i]['state-name'];		
			}
			return @$results;
		}
        else return $result->toArray();
    }
	
	function getChildren($id)
	{
		$sql = $this->select()->where('`country-id` = '.$id)->order('state-name');
        $result = $this->fetchAll($sql);
        $res = $result->toArray();
        return $res;
    }
	function getChildrenDrzewo($id = 0, $cache = false, $caching = false)
	{
		if(false) $result = $cache->load($this->_name.$id); else $result = false;
		if($result === false || !$caching) 
		{
			$sql = $this->select()->where('`country-id` = '.$id)->order('state-name');
			$result = $this->fetchAll($sql)->toArray();
			if(false) $cache->save($result, $this->_name.'_'.$id, array($this->_name));
		}		
		return $result;
	}
	
	function updateFromAllegro($webapi = null, $ile = 0, $start = 0, $limit = 0, $odrazu = false)
	{
		set_time_limit(0);
		try
		{
			if($webapi == null)
			{
				$konto = new Allegrokonta();
				$webapi = $konto->zaloguj();
				if($webapi == null) return $konto->error;
			}
			$kraje = $webapi->objectToArray($webapi->GetCountries());
			if(count($kraje) > 0)
			{
				$this->delete('1');
				foreach($kraje as $k)
				{
					//$this->dodaj($k);
					$wojew = $webapi->objectToArray($webapi->GetStatesInfo($k['country-id']));
					//var_dump($wojew);
					//if($k['country-id'] > 2) break;
					foreach($wojew as $w)
					{
						$w['country-id'] = $k['country-id'];
						$w['country-name'] = $k['country-name'];
						if($k['country-id'] > 1 && $w['state-name'] == 'dolnośląskie')
						{
							$w['state-id'] = 0;
							$w['state-name'] = '';
							$this->dodaj($w);
							break;
						}
						$this->dodaj($w);
					}
				}
			}
		}
		catch(SoapFault $error)
		{
			return 'Błąd '.$error->faultcode.': '.$error->faultstring;
		}
	}
}
?>