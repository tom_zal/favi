<?php
class Rozmiarowka extends Zend_Db_Table
{
	public $link, $id;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodajRozmiar($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}	
	function edytujRozmiar($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}	
	function wypiszRozmiar()
	{
		$sql = $this->select()->order('nazwa');
		$result = $this->fetchAll($sql);
		return $result;
	}	
	function usunRozmiar()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	function wypiszPojedynczego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function wypiszAll()
	{
		$sql = $this->select();
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i]['id']] = $result[$i]->toArray();
		}
		return @$results;
	}
}
?>