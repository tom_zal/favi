<?php
class Siodemka extends Zend_Db_Table
{
	public $id, $test = 0, $error='', $counter =1, $errorPaczka, $counterPaczka =1, $counterBlad = 0, $errorObsluga, $config;
	
	public $url_test = 'http://testws.siodemka.com/mm7ws/SiodemkaServiceSoapHttpPort';
	public $url = 'http://webmobile7.siodemka.com/mm7ws/SiodemkaServiceSoapHttpPort';
	
	public function __construct($module = 'admin', $paczkaTable = '')
	{
		parent::__construct();
		$this->module = $module;
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		$this->paczkaTable = !empty($paczkaTable) ? $paczkaTable : Paczka::$table;
    }
	
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujNR($dane, $nr)
	{
		$where = 'nr = '.$nr;
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$result = $this->fetchAll();
		return $result;
	}
	function wypiszZam($id)
	{
		$result = $this->fetchAll('id_zam = '.$id);
		return $result;
	}
	function wypiszRaport($data)
	{
		$result = $this->fetchAll('data >= "'.$data.' 00:00:00" and data <= "'.$data.' 23:59:59"');
		return $result;
	}

	function wypiszJeden()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function usunJeden()
	{
		$result = $this->delete('id = '.$this->id);
	}
	
	function dane($array, $nazwa)
	{
		$dane = '';
		if(strpos($nazwa, 'paczkaID') === false) $dane .= '<ns1:'.$nazwa.'>';
		if(is_array($array))
		{
			foreach($array as $id => $value)
			if(is_array($value)) $dane .= $this->dane($value, $id);
			else $dane .= '<ns1:'.$id.'>'.$value.'</ns1:'.$id.'>';
		}
		if(strpos($nazwa, 'paczkaID') === false) $dane .= '</ns1:'.$nazwa.'>';
		return $dane;
	}
    function funkcja($nazwa, $input = null, $separator = null, $input2 = null) 
	{
		$siodemkadane = new Siodemkadane();
		$daneKurier = $siodemkadane->wypisz();
		
		if($nazwa == 'listNadanieElement')
		{
			$przesylka['nrPrzesylki'] = '0';
			$przesylka['nrExt'] = $this->id;
			$przesylka['mpk'] = '0';//miejsce powstawania koszt�w, pole wlasne
			$przesylka['rodzajPrzesylki'] = $input['rodzajPrzesylki'];
			$przesylka['placi'] = $input['placi'];
			$przesylka['formaPlatnosci'] = $input['formaPlatnosci'];
			//$przesylka['nadawca'] = new Array();
			$przesylka['nadawca']['numer'] = $input['numer'];
			$przesylka['nadawca']['nazwisko'] = $input['nazwisko'];
			$przesylka['nadawca']['imie'] = $input['imie'];
			$przesylka['nadawca']['telKontakt'] = $input['telKontakt'];
			$przesylka['nadawca']['emailKontakt'] = $input['emailKontakt'];
			//$przesylka['odbiorca'] = new Array();
			$przesylka['odbiorca']['numer'] = '0';
			$przesylka['odbiorca']['nrExt'] = $this->id;
			$przesylka['odbiorca']['czyFirma'] = intval(isset($input2['czyFirma']) && $input2['czyFirma'] === 'on');
			$przesylka['odbiorca']['nazwa'] = $input2['nazwa'];
			$przesylka['odbiorca']['nip'] = @strval($input2['nip']);
			$przesylka['odbiorca']['nazwisko'] = $input2['nazwisko'];
			$przesylka['odbiorca']['imie'] = $input2['imie'];
			$przesylka['odbiorca']['kodKraju'] = 'PL';
			$przesylka['odbiorca']['kod'] = $input2['kod'];
			$przesylka['odbiorca']['miasto'] = $input2['miasto'];
			$przesylka['odbiorca']['ulica'] = $input2['ulica'];
			$przesylka['odbiorca']['nrDom'] = $input2['nrDom'];
			$przesylka['odbiorca']['nrLokal'] = $input2['nrLokal'];
			$przesylka['odbiorca']['telKontakt'] = $input2['telKontakt'];
			$przesylka['odbiorca']['emailKontakt'] = $input2['emailKontakt'];
			//$przesylka['platnik'] = new Array();
			$przesylka['platnik']['numer'] = '0';
			$przesylka['platnik']['telKontakt'] = '0';
			//$przesylka['uslugi'] = new Array();
			$przesylka['uslugi']['nrBezpiecznejKoperty'] = $input['nrBezpiecznejKoperty'];
			$przesylka['uslugi']['zkld'] = intval(isset($input['zkld']) && $input['zkld'] === 'on');
			$przesylka['uslugi']['zd'] = intval(isset($input['zd']) && $input['zd'] === 'on');
			//$przesylka['uslugi']['ubezpieczenie'] = new Array();
			$przesylka['uslugi']['ubezpieczenie']['kwotaUbezpieczenia'] = $input['kwotaUbezpieczenia'];
			$przesylka['uslugi']['ubezpieczenie']['opisZawartosci'] = $input['opisZawartosci'];
			//$przesylka['uslugi']['pobranie'] = new Array();
			$przesylka['uslugi']['pobranie']['kwotaPobrania'] = $input['kwotaPobrania'];
			$przesylka['uslugi']['pobranie']['formaPobrania'] = $input['formaPobrania'];
			$przesylka['uslugi']['pobranie']['nrKonta'] = $input['nrKonta'];
			$przesylka['uslugi']['awizacjaTelefoniczna'] = 
				intval(isset($input['awizacjaTelefoniczna']) && $input['awizacjaTelefoniczna'] === 'on');
			$przesylka['uslugi']['potwNadEmail'] = 
				intval(isset($input['potwNadEmail']) && $input['potwNadEmail'] === 'on');
			$przesylka['uslugi']['potwDostEmail'] = 
				intval(isset($input['potwDostEmail']) && $input['potwDostEmail'] === 'on');
			$przesylka['uslugi']['potwDostSMS'] = 
				intval(isset($input['potwDostSMS']) && $input['potwDostSMS'] === 'on');
			$przesylka['uslugi']['skladowanie'] = $input['skladowanie'];
			$przesylka['uslugi']['nadOdbPKP'] = 
				intval(isset($input['nadOdbPKP']) && $input['nadOdbPKP'] === 'on');
			$przesylka['uslugi']['odbNadgodziny'] = 
				intval(isset($input['odbNadgodziny']) && $input['odbNadgodziny'] === 'on');
			$przesylka['uslugi']['odbWlas'] = 
				intval(isset($input['odbWlas']) && $input['odbWlas'] === 'on');
			$przesylka['uslugi']['palNextDay'] = 
				intval(isset($input['palNextDay']) && $input['palNextDay'] === 'on');
			$przesylka['uslugi']['osobaFiz'] = 
				intval(isset($input['osobaFiz']) && $input['osobaFiz'] === 'on');
			$przesylka['uslugi']['market'] = 
				intval(isset($input['market']) && $input['market'] === 'on');
			$przesylka['uslugi']['zastrzDorNaGodz'] = $input['zastrzDorNaGodz'];
			$przesylka['uslugi']['zastrzDorNaDzien'] = $input['zastrzDorNaDzien'];
			//$przesylka['paczki'] = new Array();
			//$przesylka['paczki']['paczka'] = new Array();
			$ilePaczek = 1;
			for($i=0;$i<$ilePaczek;$i++)
			{
				$przesylka['paczki']['paczkaID'.$i]['paczka']['nrpp'] = '0';//nr paczki z Siodemki
				$przesylka['paczki']['paczkaID'.$i]['paczka']['typ'] = $input['typ'];
				$przesylka['paczki']['paczkaID'.$i]['paczka']['waga'] = $input['waga'];
				$przesylka['paczki']['paczkaID'.$i]['paczka']['gab1'] = $input['gab1'];
				$przesylka['paczki']['paczkaID'.$i]['paczka']['gab2'] = $input['gab2'];
				$przesylka['paczki']['paczkaID'.$i]['paczka']['gab3'] = $input['gab3'];
				$przesylka['paczki']['paczkaID'.$i]['paczka']['ksztalt'] = 
					intval(isset($input['ksztalt']) && $input['ksztalt'] === 'on');
				$przesylka['paczki']['paczkaID'.$i]['paczka']['wagaGabaryt'] = '';
			}
			//$przesylka['potwierdzenieNadania'] = new Array();
			$przesylka['potwierdzenieNadania']['dataNadania'] = $input['dataNadania'];
			$przesylka['potwierdzenieNadania']['numerKuriera'] = $input['numerKuriera'];
			$przesylka['potwierdzenieNadania']['podpisNadawcy'] = $input['podpisNadawcy'];
			$przesylka['uwagi'] = '';
		
			$dane['przesylka'] = $przesylka;
		}
		else $dane = $input;
		
		$dane['klucz'] = $daneKurier['klucz'];
		if($separator !== null) $dane['separator'] = $separator;
		
		$xml = '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
		$xml.= '<soap:Body xmlns:ns1="http://app.siodemka.com/mm7ws/type/">';
		$xml.= $this->dane($dane, $nazwa);
		$xml.= '</soap:Body></soap:Envelope>';
		return $xml;
    }
	
	function dokumenty($vals, $typ = 'list')
	{
        if(isset($vals) && !empty($vals) && is_array($vals))
		{
            $blad['error'] = '';
            
            foreach($vals as $val)
            {
                switch($val['tag'])
				{
                    case 'NS0:RESULT': 
                        $fileContent = base64_decode($val['value'], true);
                        $fileName = $typ.'_siodemka_'.$this->symbol.'.pdf';
                        $file = fopen('./admin/pdf/'.$fileName, 'w');
                        fwrite($file, $fileContent);
                        fclose($file);
                        $dokument['wydruk_'.$typ] = $fileName;
                    break;
                    case 'NS0:OPIS': $blad['error'] .= $val['value'].'<br />';
                    break;    
                    case 'FAULTSTRING': $blad['error'] .= $val['value'].'<br />';
                    break;   
                }
            }     
            
            if(strlen($blad['error']) > 1)
			{
                return $blad;
            }
            elseif(isset($dokument))
			{
                return $dokument;
            }
        }
        else
		{
            $blad['error'] = 'API Si�demki nie zwr�ci�o �adnych danych.';
            return $blad;
        }
    }
    
    function obslugaBledu($tablica, $pole)
	{
        $keys = array_keys($tablica);
        switch($keys[0])
		{
            case 'error':
                $przesylka = isset($this->symbol)? 'Nr przesy�ka:  '.$this->symbol.' - ' : '';
                $this->errorObsluga = $przesylka.$tablica['error'].'<br />';
            break; 
            case $pole: $tablica = $tablica; 
            break;    
        }
        if(isset($tablica['error'])) unset($tablica['error']);
        return $tablica;
    }
    
    
    
    function getDokument($nrPrzesylek, $funkcja = 'wydrukListPdfElement', $poleApi='numer', $typdokument = 'list')
	{
        $siodemka = new Siodemka('admin', $this->paczkaTable);
        $paczka = new Paczka($this->paczkaTable);
        $errorObsluga = '';
        $globalList = array();
        
        foreach($nrPrzesylek as $numer)
		{
            $siodemka->errorObsluga = '';
            $xml = $siodemka->funkcja($funkcja, array($poleApi => $numer), '');
            $api = $siodemka->api($xml);
            $siodemka->symbol = $numer;
            $list = $siodemka->dokumenty($api, $typdokument);
            
            $dane = $siodemka->obslugaBledu($list, 'wydruk_'.$typdokument);
            
            if($siodemka->errorObsluga == '')
			{
                $paczka->id = $this->ar2[$numer];
                $paczka->edytuj($dane);
                $globalList[] = $dane['wydruk_'.$typdokument];
            }
            else $errorObsluga .= $siodemka->errorObsluga;
        }
        return array($errorObsluga, $globalList);
    }
    
    function joinFilePdf($files , $filejoined, $page = 'all')
	{
        $files = array_unique($files);
        if(count($files) > 0)
		{
            $pdfMerger = @new PDFMerger();   
            foreach($files as $file)
			{
                $pdfMerger->addPDF('./admin/pdf/'.$file, $page);
            }
            $pdfMerger->merge('file', $_SERVER['DOCUMENT_ROOT'].'/public/admin/pdf/'.$filejoined);  
            return $filejoined;
        }
        return '';
    }
    
    function api($xml)
	{
        if(isset($xml) && !empty($xml))
		{
            $url = $this->test ? $this->url_test : $this->url;

            $ch = curl_init();
            @curl_setopt($ch, CURLOPT_URL, $url);
            @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            @curl_setopt($ch, CURLOPT_CRLF, false);
            @curl_setopt($ch, CURLOPT_FRESH_CONNECT, false);
            @curl_setopt($ch, CURLOPT_HEADER, false);
            @curl_setopt($ch, CURLOPT_HEADER, false);
            @curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
            @curl_setopt($ch, CURLOPT_HTTPGET, false);
            @curl_setopt($ch, CURLOPT_POST, true);
            @curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            @curl_setopt($ch, CURLOPT_TIMEOUT, 30);

            $result = @curl_exec($ch);
            if(curl_errno($ch)) $error = 'ERROR -> '.curl_errno($ch).' : '.curl_error($ch);
            else
            {
				$xmlParser = xml_parser_create();
				xml_parse_into_struct($xmlParser, $result, $vals, $index);
				xml_parser_free($xmlParser);
            }
            curl_close($ch);
            
            if(isset($vals))
			{
                return $vals;
            }    
            return $error;
        }
    }
    
    function paczkaDodawanie($vals)
	{
        if(isset($vals) && !empty($vals) && is_array($vals))
		{
            $blad['error'] = '';
            foreach($vals as $val)
            {
                switch($val['tag'])
				{
                    case 'NS0:NRPRZESYLKI': $przesylka['numerPrzesylka'] = $val['value'];
                    break;
                    case 'NS0:NRPP': $nrpp[] = $val['value'];
                    break;
                    case 'NS0:OPIS': $blad['error'] .= $val['value'].'<br />';
                    break;   
                    case 'FAULTSTRING': $blad['error'] .= $val['value'].'<br />';
                    break;  
                }
            }     
            
            if(strlen($blad['error']) > 1)
			{
                return $blad;
            }
            elseif(isset($przesylka))
			{
                $przesylka['nrp'] = implode(';', $nrpp);
                return $przesylka;
            }
        }
        else
		{
            $blad['error'] = 'API Si�demki nie zwr�ci�o �adnych danych.';
            return $blad;
        }
    }
    
    function walidacja($domyslne, $pozostale)
	{
        $this->counterPaczka = 1;
        $this->errorPaczka = '';
        $this->walidacjaOdbiorca($pozostale);
        $this->walidacjaPaczka($domyslne);
        $this->walidacjaUsluga($domyslne);
        return $this->errorPaczka;
    }
    
    function walidacjaDomyslne($dane)
	{
        $walid = new WalidacjaKontrahenta();
        if(empty($dane['numer'])) {
            $this->error .= $this->counter++.'. Pole numer nadawcy jest wymagane.<br />'; 
            $this->counterBlad++;
        }
        
        if(empty($dane['nazwisko'])) {
            $this->error .= $this->counter++.'. Pole nazwisko osoby nadaj�cej jest wymagane.<br />'; 
            $this->counterBlad++;
        }
        
        if(empty($dane['imie'])) {
            $this->error .= $this->counter++.'. Pole imi� osoby nadaj�cej jest wymagane.<br />'; 
            $this->counterBlad++;
        }
        
        if(empty($dane['telKontakt'])) {
            $this->error .= $this->counter++.'. Pole telefon osoby nadaj�cej jest wymagane.<br />'; 
            $this->counterBlad++;
        }
        
        if(empty($dane['emailKontakt'])) {
            $this->error .= $this->counter++.'. Pole email osoby nadaj�cej jest wymagane.<br />'; 
            $this->counterBlad++;
        }
        
        if(!empty($dane['emailKontakt']) && !$walid->sprawdzEmail($dane['emailKontakt'])) {
            $this->error .= $this->counter++.'. Podany email osoby nadaj�cej jest niepoprawny.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['dataNadania'])) {
            $this->error .= $this->counter++.'. Pole data nadania jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['podpisNadawcy'])) {
            $this->error .= $this->counter++.'. Pole podpis nadawcy jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['nrKonta'])) {
            $this->error .= $this->counter++.'. Pole numer konta jest wymagany.<br />';
            $this->counterBlad++;
        }
        
        if(!empty($dane['nrKonta']) && !$walid->sprawdzIban($dane['nrKonta'])) {
            $this->error .= $this->counter++.'. Pole numer konta jest niepoprawny.<br />';
            $this->counterBlad++;
        }
        if(strlen($this->error) > 1) $this->paczka->error[$this->paczka->dostawca] = $this->error;
        return $this->error;
    }
    
    function walidacjaOdbiorca($dane)
	{
        $walid = new WalidacjaKontrahenta();
        
        if(empty($dane['nazwa'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole nazwa odbiorcy jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(isset($dane['czyFirma']) && $dane['czyFirma'] == 'on') {
            if(empty($dane['nip'])) {
                $this->errorPaczka .= $this->counterPaczka++.'. Pole nip jest wymagane.<br />';
                $this->counterBlad++;
            }
            if(!empty($dane['nip']) && $walid->sprawdzNIP($dane['nip'])) {
                //$this->errorPaczka .= $this->counterPaczka++.'. Pole nip jest niepoprawne.<br />';
                //$this->counterBlad++;
            }
        }
        
        if(empty($dane['nazwisko'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole nazwisko jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['imie'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole imi� jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['kod'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole kod pocztowy jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['kod']) &&  !$walid->sprawdzKod($dane['kod'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole kod pocztowy jest niepoprawny (00-000).<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['miasto'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole miasto jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['ulica'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole ulica jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['nrDom'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole numer domu jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['telKontakt'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole telefon jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['emailKontakt'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole email jest wymagane.<br />'; 
            $this->counterBlad++;
        }
        
        if(!empty($dane['emailKontakt']) && !$walid->sprawdzEmail($dane['emailKontakt'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Podany email jest niepoprawny.<br />';
            $this->counterBlad++;
        }
    }
    
    function walidacjaPaczka($dane)
	{
        if(empty($dane['typ'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole rodzaj wysy�ki jest wymagane.<br />';
            $this->counterBlad++;
        }
        if(empty($dane['waga'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole waga jest wymagane.<br />';
            $this->counterBlad++;
        }
        if(empty($dane['gab1'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole d�ugo�� jest wymagane.<br />';
            $this->counterBlad++;
        }
        if(empty($dane['gab2'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole szeroko�� jest wymagane.<br />';
            $this->counterBlad++;
        }
        if(empty($dane['gab3'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole wysoko�� jest wymagane.<br />';
            $this->counterBlad++;
        }
    }
    
    function walidacjaUsluga($dane)
	{
        if(isset($dane['czyPobranie']) && $dane['czyPobranie'] == 'on') {
            if(empty($dane['kwotaPobrania'])) {
                $this->errorPaczka .= $this->counterPaczka++.'. Pole kwota pobrania jest wymagane.<br />';
                $this->counterBlad++;
            }
            if(empty($dane['formaPobrania'])) {
                $this->errorPaczka .= $this->counterPaczka++.'. Pole forma pobrania jest wymagane.<br />';
                $this->counterBlad++;
            }
        }
        
        if(isset($dane['czyUbezpieczenie']) && $dane['czyUbezpieczenie'] == 'on') {
            if(empty($dane['kwotaUbezpieczenia'])) {
                $this->errorPaczka .= $this->counterPaczka++.'. Pole kwota ubezpieczenia jest wymagane.<br />';
                $this->counterBlad++;
            }            
        }
        
        if(empty($dane['opisZawartosci'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole zawarto�� przesy�ki jest wymagane.<br />';
            $this->counterBlad++;
        }
    }
    
    function setDefaultData($cfg)
	{
        $this->config = $cfg['domyslne'];
    }
    
    function waliduj($przesylki, $odbiorca)
	{
        $errorDomyslne = $this->walidacjaDomyslne($this->config);
        
        unset($przesylki['domyslne']);
        foreach($przesylki as $key => $dane)
		{
            $error = $this->walidacja(array_merge($dane, $this->config),$odbiorca[$key]); 
            if(strlen($error) >0)
			{
                $this->paczka->checkErrors(array('error'=>$error), $key);
            }
        }
    }
    
    function addToApi($id, $dane, $odbiorca)
	{
        $this->paczka->id = $id;
        $przSprawdz = $this->paczka->sprawdzZamowienieNumer();
        if(empty($przSprawdz['numeryPrzesylek']))
		{
            $this->id = $id;
            $xml = $this->funkcja('listNadanieElement', $dane, null,$odbiorca);
            $curl = $this->api($xml);
            $response = $this->paczkaDodawanie($curl);
            return $response;
        }
        else
		{
            return array('error'=>'Zam�wienie posiada ju� numery przesy�ki: '.$przSprawdz['numeryPrzesylek'].'<br />');
        }
    }
}
?>