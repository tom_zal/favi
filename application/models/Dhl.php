<?php
class Dhl extends Zend_Db_Table
{
	public $id;
	public $blpPieceIdMin = 90000000000;
	public $blpPieceIdMax = 99999999999;
	
	public function __construct($module = 'admin', $paczkaTable = '')
	{
		parent::__construct();
		$this->module = $module;		
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		$this->paczkaTable = !empty($paczkaTable) ? $paczkaTable : Paczka::$table;
    }
	
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujNR($dane, $nr)
	{
		$where = 'nr = '.$nr;
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$result = $this->fetchAll();
		return $result;
	}
	function wypiszZam($id)
	{
		$result = $this->fetchAll('id_zam = '.$id);
		return $result;
	}
	function wypiszAll($id_zam = 0)
	{
		$zamow = new Zamowienia($this->module);
		$sql = $this->db->select()->from($this->_name, array('*'));
		$sql->joinleft(array('zk' => 'Zamowieniakontrahenci'), 'zk.id = id_zam', array('numer' => $zamow->nr));
		$sql->where($id_zam > 0 ? 'id_zam = "'.$id_zam.'"' : 1);
		$result = $this->db->fetchAll($sql);
		return $result;
	}
	function wypiszNR($shipmentId)
	{
		$result = $this->fetchRow('shipmentId = "'.$shipmentId.'"');
		return $result;
	}
	function wypiszRaport($data)
	{
		$result = $this->fetchAll('data >= "'.$data.' 00:00:00" and data <= "'.$data.' 23:59:59"');
		return $result;
	}
	function wypiszJeden()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function usunJeden()
	{
		$result = $this->delete('id = '.$this->id);
	}
	function usunNR($shipmentId = 0)
	{
		$result = $this->delete('shipmentId = "'.$shipmentId.'"');
	}
	function getNrPaczki()
	{
		$sql = $this->select()->from(array('d' => "Dhl"), array('max(blpPieceId)'))->where('blpPieceId > '.$this->blpPieceIdMin);
		$result = $this->fetchRow($sql);
		$nr = intval($result['max(blpPieceId)']);
		if($nr == 0) return strval($this->blpPieceIdMin);
		return "" + strval($nr + 1);
	}
	function getNowyNrPaczki()
	{
		$sql = $this->db->select()->from(array('d' => "Dhl"), array('blpPieceId'))->where('blpPieceId > 0');
        $result = $this->db->fetchAll($sql);
		if(count($result) > 0)
		foreach($result as $row)
		{
			$blpPieceIds[$row['blpPieceId']] = $row;
		}
		do $blpPieceId = rand(1, 9).mt_rand(1000000000, 2147483647);
		while(isset($blpPieceIds[$blpPieceId]));
        return $blpPieceId;
    }
	
	function dokumenty($value, $typ = 'list')
	{
        if(isset($value) && !empty($value) && is_object($value))
		{
			if(@!empty($value->error))
			{
				$blad['error'] = $value->error;
				return $blad;
			}
            $item = $value->item;
			$labelData = $item->labelData;			
			$fileContent = base64_decode($labelData, true);
			//if(@$_GET['test']) var_dump($value);
			$fileName = $typ.'_dhl_'.$this->symbol.'.pdf';
			$file = fopen('./admin/pdf/'.$fileName, 'w');
			fwrite($file, $fileContent); fclose($file);
			$dokument['wydruk_'.$typ] = $fileName;    
            return $dokument;
        }
        else
		{
            $blad['error'] = 'API DHL nie zwróciło żadnych danych.';
            return $blad;
        }
    }
    
    function obslugaBledu($tablica, $pole)
	{
        $keys = array_keys($tablica);
        switch($keys[0])
		{
            case 'error':
                $przesylka = isset($this->symbol)? 'Nr przesyłka:  '.$this->symbol.' - ' : '';
                $this->errorObsluga = $przesylka.$tablica['error'].'<br />';
            break; 
            case $pole: $tablica = $tablica; 
            break;    
        }
        if(isset($tablica['error'])) unset($tablica['error']);
        return $tablica;
    }
    
    
    
    function getDokument($nrPrzesylek, $funkcja = 'getLabels', $typ = 'LP', $typdokument = 'list')
	{
        $dhl = new Dhl('admin', $this->paczkaTable);
        $paczka = new Paczka($this->paczkaTable);
        $errorObsluga = '';
        $globalList = array();
		
		$dhlDane = new Dhldane();
		$config = $dhlDane->wypisz();
        
        foreach($nrPrzesylek as $numer)
		{
            $dhl->errorObsluga = '';
            $xml = $dhl->funkcja($funkcja, $config, array('labelType' => $typ, 'shipmentId' => $numer));
			//if(@$_GET['test']) var_dump($xml->itemsToPrint);//die();
            $api = $dhl->api($funkcja, $config, $xml);
			//var_dump($api);die();
            $dhl->symbol = $numer;
            $list = $dhl->dokumenty($api, $typdokument);
            $dane = $dhl->obslugaBledu($list, 'wydruk_'.$typdokument);
            //var_dump($dane);die();
            if($dhl->errorObsluga == '')
			{
                $paczka->id = $this->ar2[$numer];
                $paczka->edytuj($dane);
                $globalList[] = $dane['wydruk_'.$typdokument];
            }
            else $errorObsluga .= $dhl->errorObsluga;
        }
        return array($errorObsluga, $globalList);
    }
	
	function getKurier($nrPrzesylek, $kurier = null)
	{
        $dhl = new Dhl('admin', $this->paczkaTable);
        $paczka = new Paczka($this->paczkaTable);
        $errorObsluga = '';
        $globalList = array();
		
		$dhlDane = new Dhldane();
		$config = $dhlDane->wypisz();
		$funkcja = 'bookCourier';
		
		$dhl->errorObsluga = '';
		$kurier['shipmentIdList'] = $nrPrzesylek;
		$xml = $dhl->funkcja($funkcja, $config, $kurier);
		//var_dump($xml);die();
		$api = $dhl->api($funkcja, $config, $xml);
		//var_dump($api);die();
		if(@empty($api->error))
        foreach($nrPrzesylek as $numer)
		{
			$dane['kurier'] = $api->item;
			//var_dump($api);die();
			$dhl->symbol = $numer;
			$paczka->id = $this->ar2[$numer];
			$paczka->edytuj($dane);
			$globalList = $dane['kurier'];
        }
		else $errorObsluga .= $api->error;
		
        return array($errorObsluga, $globalList);
    }
	
	function delKurier($nrPrzesylek, $kurier = null)
	{
        $dhl = new Dhl('admin', $this->paczkaTable);
        $paczka = new Paczka($this->paczkaTable);
        $errorObsluga = '';
        $globalList = array();
		
		$dhlDane = new Dhldane();
		$config = $dhlDane->wypisz();
		$funkcja = 'cancelCourierBooking';
		
		$dhl->errorObsluga = '';
		//$kurier['orderId'] = $nrPrzesylek;
		$xml = $dhl->funkcja($funkcja, $config, $kurier);
		//var_dump($xml);//die();
		$api = $dhl->api($funkcja, $config, $xml);
		//var_dump($api);die();
		$item = isset($api->item) ? $api->item : $api;
		if(@empty($item->error))
        foreach($nrPrzesylek as $numer)
		{
			$dane['kurier'] = "";
			//var_dump($api);die();
			$dhl->symbol = $numer;
			$paczka->id = $this->ar2[$numer];
			if(@$item->result)
			$paczka->edytuj($dane);
			$globalList = $item->id;
        }
		else $errorObsluga .= $item->error;
		
        return array($errorObsluga, $globalList);
    }
    
    function joinFilePdf($files , $filejoined, $page = 'all')
	{
        $files = array_unique($files);
        if(count($files) > 0)
		{
			$merge = false;
			//var_dump($files);
            $pdfMerger = @new PDFMerger();   
            foreach($files as $file)
			{
				$plik = './admin/pdf/'.$file;
				if(true && file_exists($plik) && filesize($plik) > 0)
				{
					$merge = true;
					$pdfMerger->addPDF($plik, $page);
				}
            }
			if(!$merge) return '';
            $pdfMerger->merge('file', $_SERVER['DOCUMENT_ROOT'].'/public/admin/pdf/'.$filejoined);
            //var_dump($_SERVER['DOCUMENT_ROOT'].'/public/admin/pdf/'.$filejoined);die();
			return $filejoined;
        }		
        return '';
    }
    
    function api($funkcja, $dane, $xml)
	{
        if(isset($xml) && !empty($xml))
		{
			$url = $dane['www']; //die();
            //$url = $this->test ? $this->url_test : $this->url;			
            try
			{
				$client = new DHL24webapiclient($url);
				//var_dump($xml->shipments->item[0]);die();
				//var_dump($xml);//die();
				$result = $client->{$funkcja}($xml);
				//var_dump($result);die();
				//@var_dump($result->{$funkcja.'Result'});//die();
				return $result->{$funkcja.'Result'};
			}
			catch(SoapFault $e)
			{
				//var_dump($e);die();
				$result = (object)array();
				$result->error = $e->getMessage();
				return $result;
			}
        }
    }
    
    function paczkaDodawanie($value)
	{
        if(isset($value) && !empty($value) && is_object($value))
		{
			if(!isset($value->error) || empty($value->error))
			{
				$item = $value->item;
				//var_dump($item);die();
				$przesylka['numerPrzesylka'] = $item->shipmentId;
				$przesylka['dataPrzesylka'] = @strval($item->created);//$shipmentItem->shipmentDate;
				//$przesylka['orderStatus'] = @strval($item->orderStatus);
				//$przesylka['blpPieceId'] = $pieceListItem->blpPieceId;
				return $przesylka;
			}
			else
			{
				$blad['error'] = $value->error;
				return $blad;
			}
        }
        else
		{
            $blad['error'] = 'API DHL nie zwróciło żadnych danych.';
            return $blad;
        }
    }
    
    function walidacja($domyslne, $pozostale)
	{
		$this->counter = 1;
        $this->counterPaczka = 1;
        $this->errorPaczka = '';
        $this->walidacjaOdbiorca($pozostale);
        $this->walidacjaPaczka($domyslne);
        $this->walidacjaUsluga($domyslne);
        return $this->errorPaczka;
    }
    
    function walidacjaDomyslne($dane)
	{
		$this->error = '';
		
        $walid = new WalidacjaKontrahenta();
        if(false && empty($dane['numer'])) {
            $this->error .= $this->counter++.'. Pole numer nadawcy jest wymagane.<br />'; 
            $this->counterBlad++;
        }
        
        if(empty($dane['name'])) {
            $this->error .= $this->counter++.'. Pole nazwisko osoby nadającej jest wymagane.<br />'; 
            $this->counterBlad++;
        }
        
        if(empty($dane['contactPhone'])) {
            $this->error .= $this->counter++.'. Pole telefon osoby nadającej jest wymagane.<br />'; 
            $this->counterBlad++;
        }
        
        if(empty($dane['contactEmail'])) {
            $this->error .= $this->counter++.'. Pole email osoby nadającej jest wymagane.<br />'; 
            $this->counterBlad++;
        }
        
        if(!empty($dane['contactEmail']) && !$walid->sprawdzEmail($dane['contactEmail'])) {
            $this->error .= $this->counter++.'. Podany email osoby nadającej jest niepoprawny.<br />';
            $this->counterBlad++;
        }
        
        if(false && empty($dane['dataNadania'])) {
            $this->error .= $this->counter++.'. Pole data nadania jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(false && empty($dane['podpisNadawcy'])) {
            $this->error .= $this->counter++.'. Pole podpis nadawcy jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(false && empty($dane['nrKonta'])) {
            $this->error .= $this->counter++.'. Pole numer konta jest wymagany.<br />';
            $this->counterBlad++;
        }
        
        if(false && !empty($dane['nrKonta']) && !$walid->sprawdzIban($dane['nrKonta'])) {
            $this->error .= $this->counter++.'. Pole numer konta jest niepoprawny.<br />';
            $this->counterBlad++;
        }
        if(strlen($this->error) > 1) $this->paczka->error[$this->paczka->dostawca] = $this->error;
        return $this->error;
    }
    
    function walidacjaOdbiorca($dane)
	{
        $walid = new WalidacjaKontrahenta();
        
        if(empty($dane['name'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole nazwa odbiorcy jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['postalCode'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole kod pocztowy jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['postalCode']) &&  !$walid->sprawdzKod($dane['postalCode'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole kod pocztowy jest niepoprawny (00000).<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['city'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole miasto jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['street'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole ulica jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['houseNumber'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole numer domu jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['contactPhone'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole telefon jest wymagane.<br />';
            $this->counterBlad++;
        }
        
        if(empty($dane['contactEmail'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole email jest wymagane.<br />'; 
            $this->counterBlad++;
        }
        
        if(!empty($dane['contactEmail']) && !$walid->sprawdzEmail($dane['contactEmail'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Podany email jest niepoprawny.<br />';
            $this->counterBlad++;
        }
    }
    
    function walidacjaPaczka($dane)
	{
        if(empty($dane['product'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole Usługa jest wymagane.<br />';
            $this->counterBlad++;
        }
        if(empty($dane['weight'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole waga jest wymagane.<br />';
            $this->counterBlad++;
        }
        if(empty($dane['width'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole długość jest wymagane.<br />';
            $this->counterBlad++;
        }
        if(empty($dane['height'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole szerokość jest wymagane.<br />';
            $this->counterBlad++;
        }
        if(empty($dane['length'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole wysokość jest wymagane.<br />';
            $this->counterBlad++;
        }
    }
    
    function walidacjaUsluga($dane)
	{
        if(isset($dane['collectOnDelivery']) && $dane['collectOnDelivery'] == '1') {
            if(empty($dane['collectOnDeliveryValue'])) {
                $this->errorPaczka .= $this->counterPaczka++.'. Pole kwota pobrania jest wymagane.<br />';
                $this->counterBlad++;
            }
            if(empty($dane['collectOnDeliveryForm'])) {
                $this->errorPaczka .= $this->counterPaczka++.'. Pole forma pobrania jest wymagane.<br />';
                $this->counterBlad++;
            }
        }
        
        if(isset($dane['insurance']) && $dane['insurance'] == '1') {
            if(empty($dane['insuranceValue'])) {
                $this->errorPaczka .= $this->counterPaczka++.'. Pole kwota ubezpieczenia jest wymagane.<br />';
                $this->counterBlad++;
            }            
        }
        
        if(empty($dane['content'])) {
            $this->errorPaczka .= $this->counterPaczka++.'. Pole zawartość przesyłki jest wymagane.<br />';
            $this->counterBlad++;
        }
    }
    
    function setDefaultData($cfg)
	{
        $this->config = $cfg;//['domyslne'];
    }
    
    function waliduj($przesylki, $odbiorca)
	{
        $errorDomyslne = $this->walidacjaDomyslne($this->config);
        
        unset($przesylki['domyslne']);
        foreach($przesylki as $key => $dane)
		{
            $error = $this->walidacja(array_merge($dane, $this->config), $odbiorca[$key]); 
            if(strlen($error) > 0)
			{
                $this->paczka->checkErrors(array('error' => $error), $key);
            }
        }
    }
    
    function addToApi($id, $dane, $odbiorca)
	{
        $this->paczka->id = $id;
        $przSprawdz = $this->paczka->sprawdzZamowienieNumer();
        if(empty($przSprawdz['numerPrzesylka']))
		{
			//var_dump($dane);die();
            $this->id = $id;
            $xml = $this->funkcja('createShipments', $dane, $odbiorca);
			//var_dump($xml->shipments->item[0]);die();
            $curl = $this->api('createShipments', $dane, $xml);
			//var_dump($curl);die();
            $response = $this->paczkaDodawanie($curl);
			//var_dump($response);die();
			if(@empty($response['error']))
			{
				//var_dump($response);die();
				if(@empty($response['dataPrzesylka']))
				$response['dataPrzesylka'] = $xml->shipments->item[0]->shipmentDate;			
				$response['numerPaczka'] = $xml->shipments->item[0]->pieceList->item[0]->blpPieceId;
			}
			//else $response['error'] .= '<br/>'.$xml->__toString;
			//else if(1||@$_GET['xxx']){var_dump($xml);die();}
            return $response;
        }
        else
		{
            return array('error'=>'Zamówienie posiada już numer przesyłki: '.$przSprawdz['numerPrzesylka'].'<br />');
        }
    }
	
	function funkcja($funkcja, $config, $odbiorca)
	{
		$authData = (object)array();
		$authData->username = $config['username'];
        $authData->password = $config['password'];
		
		$xml = (object)array();
		$xml->authData = $authData;
		//var_dump($odbiorca);die();
		
		switch($funkcja)
		{
			case 'createShipments':
			
			$shipments = (object)array(); //ShipmentFullData
			$shipmentItem = (object)array();
			
			$shipper = (object)array();//addressData
			$shipper->name = $config['name'];
			$shipper->postalCode = str_replace("-","",$config['postalCode']);
			$shipper->city = $config['city'];
			$shipper->street = $config['street'];
			$shipper->houseNumber = $config['houseNumber'];
			$shipper->apartmentNumber = $config['apartmentNumber'];
			$shipper->contactPerson = $config['contactPerson'];
			$shipper->contactPhone = $config['contactPhone'];
			$shipper->contactEmail = $config['contactEmail'];
			$shipmentItem->shipper = $shipper;
			
			$receiver = (object)array();//addressData
			$receiver->country = "PL"; //de
			$receiver->isPackstation = false;
			$receiver->isPostfiliale = false;
			$receiver->postnummer = "";
			$receiver->name = $odbiorca['name'];
			$receiver->postalCode = str_replace("-","",$odbiorca['postalCode']);
			$receiver->city = $odbiorca['city'];
			$receiver->street = $odbiorca['street'];
			$receiver->houseNumber = $odbiorca['houseNumber'];
			$receiver->apartmentNumber = $odbiorca['apartmentNumber'];
			$receiver->contactPerson = $odbiorca['contactPerson'];
			$receiver->contactPhone = $odbiorca['contactPhone'];
			$receiver->contactEmail = $odbiorca['contactEmail'];
			$shipmentItem->receiver = $receiver;
			
			$payment = (object)array();//paymentData
			$payment->paymentMethod = $config['paymentMethod'];
			$payment->payerType = $config['payerType'];
			$payment->accountNumber = $config['accountNumber'];//receiverSap
			$payment->costsCenter = $this->id > 0 ? $this->id : 0;
			$shipmentItem->payment = $payment;
			
			$pieceList = (object)array();
			$pieceListItem = (object)array();//pieceDefinition
			$pieceListItem->type = $config['type'];
			$pieceListItem->width = intval($config['width']);
			$pieceListItem->height = intval($config['height']);
			$pieceListItem->length = intval($config['length']);
			$pieceListItem->weight = intval($config['weight']);
			$pieceListItem->quantity = 1;//intval($config['quantity']);
			$pieceListItem->nonStandard = @(bool)$config['nonStandard'];
			$pieceListItem->euroReturn = @(bool)$config['euroReturn'];
			$pieceListItem->blpPieceId = $this->getNowyNrPaczki();
			$pieceList->item[] = $pieceListItem;
			$shipmentItem->pieceList = $pieceList;			
			
			$service = (object)array();//ServiceDefinition
			$service->product = $config['product'];
			$service->deliveryEvening = @(bool)$config['deliveryEvening'];
			$service->deliverySaturday = @(bool)$config['deliverySaturday'];
			$service->collectOnDelivery = @(bool)$config['collectOnDelivery'];
			$service->collectOnDeliveryValue = floatval(str_replace(",",".",$config['collectOnDeliveryValue']));
			$service->collectOnDeliveryForm = $config['collectOnDeliveryForm'];
			$service->collectOnDeliveryReference = $config['collectOnDeliveryReference'];
			$service->insurance = @(bool)$config['insurance'];
			$service->insuranceValue = floatval(str_replace(",",".",$config['insuranceValue']));
			$service->returnOnDelivery = @(bool)$config['returnOnDelivery'];
			$service->returnOnDeliveryReference = $config['returnOnDeliveryReference'];
			$service->proofOfDelivery = @(bool)$config['proofOfDelivery'];
			$service->selfCollect = @(bool)$config['selfCollect'];
			$service->deliveryToNeighbour = @(bool)$config['deliveryToNeighbour'];
			$service->predeliveryInformation = @(bool)$config['predeliveryInformation'];
			$service->preaviso = @(bool)$config['preaviso'];
			$shipmentItem->service = $service;
			
			$shipmentItem->shipmentDate = date("Y-m-d", strtotime($config['shipmentDate']));
			$shipmentItem->skipRestrictionCheck = false;//@(bool)$config['skipRestrictionCheck'];
			$shipmentItem->comment = $config['comment'];
			$shipmentItem->content = $config['content'];
			$shipmentItem->reference = $this->id > 0 ? $this->id : "";
			//$shipmentItem->labelType = "BLP";
			
			$shipments->item[] = $shipmentItem;
			$xml->shipments = $shipments;
			
			break;
			
			case 'getLabels':
			
			$itemToPrint = (object)array();
			$itemToPrint->labelType = $odbiorca['labelType'];
			$itemToPrint->shipmentId = $odbiorca['shipmentId'];
			$itemsToPrint = (object)array();
			$itemsToPrint->item[] = $itemToPrint;
			$xml->itemsToPrint = $itemsToPrint;
			
			break;
			
			case 'bookCourier':
			
			//$bookCourier = (object)array();
			$xml->pickupDate = $odbiorca['pickupDate'];
			$xml->pickupTimeFrom = $odbiorca['pickupTimeFromHour'].':'.$odbiorca['pickupTimeFromMin'];
			$xml->pickupTimeTo = $odbiorca['pickupTimeToHour'].':'.$odbiorca['pickupTimeToMin'];
			$xml->additionalInfo = stripslashes($odbiorca['additionalInfo']);
			$shipmentIdList = (object)array();
			$shipmentIdList->item = $odbiorca['shipmentIdList'];
			$xml->shipmentIdList = $shipmentIdList;
			//$xml->bookCourier = $bookCourier;
			
			break;
			
			case 'cancelCourierBooking':
			
			//$cancelCourierBooking = (object)array();
			$orders = (object)array();
			$orders->item = $odbiorca['orderId'];
			$xml->orders = $orders;
			//$xml->cancelCourierBooking = $cancelCourierBooking;
			
			break;
		}
		
		return $xml;
	}
}
?>