<?php
class Kategorie extends Zend_Db_Table
{
    public $lang, $menu, $link, $mial, $db, $allegro, $eBay, $kats, $dzieci, $podstrona;
	public $arrowPath, $arrowThis, $arrowMain, $arrowLvls, $arrowMainPath, $arrowLvlsPath;

    public function __construct($module = 'admin', $typ = 'kategorie')
	{
        parent::__construct();
        $this->menu = "";
		$this->katAll = null;
		$this->module = $module;
		$this->grupa = 0;
		$this->active = false;
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->link = $this->obConfig->baseUrl;
		$this->lang = $this->common->getJezyk($module);
		$this->typ = $typ;
		
		if(false && $this->obConfig->allegro)
		{
			$this->konto = new Allegrokonta();
			$this->konto->ID = 1;
			$this->config = $this->konto->klient();
			$this->uid = $this->config['uid'];
		}
		
		$link = str_replace('//','/','/'.$this->link.'/');
		$this->arrowPath = '';//'<img src="'.$link.'public/images/strona/arrow_menu.png" alt="&gt;">';
		$this->arrowThis = '';//'<img src="'.$link.'public/images/strona/arrow_menu.png" alt="&gt;">';
		$this->arrowMain = '';//'<img src="'.$link.'public/images/strona/arrow_menu.png" alt="&gt;">';
		$this->arrowLvls = '';//<span class="arr">&gt;</span>';
		$this->arrowMainPath = '';//'<img src="'.$link.'public/images/strona/arrow_menu_active.png" alt="&gt;">';
		$this->arrowLvlsPath = '';//'<img src="'.$link.'public/images/strona/arrow_menu_active.png" alt="&gt;">';
		//$this->arrowPath = '<label>'.$this->arrowPath.'</label>';
		//$this->arrowThis = '<label>'.$this->arrowThis.'</label>';
		//$this->arrowMain = '<label>'.$this->arrowMain.'</label>';
		//$this->arrowLvls = '<label>'.$this->arrowLvls.'</label>';
		//$this->arrowMainPath = '<label>'.$this->arrowMainPath.'</label>';
		//$this->arrowLvlsPath = '<label>'.$this->arrowLvlsPath.'</label>';
    }

	function wypiszKategorie()
	{
		$this->_name = 'Kategorie';
		$result = $this->fetchAll();
		return $result;
	}
	function dodajKategorie($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function usunKategorie()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	
	public function getAllWithDetails($rodzic = -1, $numeruj = true)
	{
		$sql = 'SELECT *, (SELECT count(*) FROM Kategorie where rodzic = k.id) as dzieci, (SELECT count(*) FROM Katprod kp JOIN Produkty p on p.id = kp.id_prod where p.widoczny = 1 and p.zmiana is not NULL and kp.id_kat = k.id and '.($this->grupa > 0 ? '(p.grupa = '.$this->grupa.' or p.grupa = -1)' : 1).') as produkty, (SELECT count(*) FROM Galeria g JOIN Katprod kp on g.wlasciciel = kp.id_prod where kp.id_kat = k.id and g.glowne = "N" and g.typ = "galeria") as galeria FROM Kategorie as k where typ = "'.$this->typ.'" and lang = "'.$this->lang.'" and '.($rodzic >= 0 ? 'k.rodzic = '.$rodzic : 1).' and '.($this->module != 'admin' ? 'k.widoczny' : 1).' order by k.pozycja asc';
		//echo $sql;
		//return;
        $result = $this->db->fetchAll($sql);
		if(!$numeruj) $results = $result;
		for ($i = 0; $i < count($result); $i++)
		{
			if($this->grupa > 0 && $result[$i]['produkty'] == 0) continue;
			if($numeruj) $results[$result[$i]['id']] = $result[$i];
			if($this->podstrona != 'galeria' || $this->obConfig->galeriaPusteKategorie || @$result[$i]['galeria'] > 0)
			$dzieci[$result[$i]['rodzic']][] = $result[$i];			
		}
		$this->kats = @$results;
		$this->dzieci = @$dzieci;
    }
		
    function start()
	{
        $where = 'rodzic = 0 AND typ = "'.$this->typ.'" AND lang = "'.$this->lang.'"';
        $order = 'pozycja';
		$this->_name = 'Kategorie';
        $result = $this->fetchAll($where, $order);		
        //var_dump($result);
        return $result;
    }
	
    function znajdzIdKat($idkat)
    {
        $where = 'id = '.$idkat;
		$result = $this->fetchAll($where);
		return $result;
    }
	
	function showAll($parent = 0, $lvl = 0, $pole = '') 
	{
		$this->_name = 'Kategorie';
		$where = 'rodzic = '.$parent.' AND typ = "'.$this->typ.'" AND lang = "'.$this->lang.'"';
        $order = ' pozycja ASC';
        $sql = $this->select()->where($where)->order($order);
        $row = $this->fetchAll($sql);
		for ($i = 0; $i < count($row); $i++)
		{
			$kat = $row[$i]->toArray();
			$kat['lvl'] = $lvl;
			//$kat['path'] = $this->getPathString($id, $offer['kateg'], 'kategorie', '/', '/', '');
			if(empty($pole)) $this->katAll[] = $kat;
			else $this->katAll[$kat[$pole]] = $kat;
			$this->showAll($row[$i]['id'], $lvl + 1, $pole);
		}
    }
	function showAllWithChild($parent = 0) 
	{
		$this->_name = 'Kategorie';
		$where = 'rodzic = '.$parent.' AND typ = "'.$this->typ.'" AND lang = "'.$this->lang.'"';
        $order = ' pozycja ASC';
        $result = $this->fetchAll($where, $order);
        $row = $result->toArray();
		for ($i = 0; $i < count($row); $i++)
		{
			$this->katAll[$row[$i]['id']] = $row[$i];
			$ile = $this->showAllWithChild($row[$i]['id']);
			if($ile == 0) unset($this->katAll[$row[$i]['id']]);
		}
		return count($row);
    }
	
	function display_children_admin($parent, $level, $z = 0)
	{
		$row = @$this->dzieci[$parent];
		//die();
		
        for ($i = 0; $i < count($row); $i++)
		{
			if ($row[$i]['rodzic'] == 0)
			{
                $this->menu.='<tr id="rowPos_'.$row[$i]['id'].'" name="rodzic_0" class="white bord">';
				
				if($this->obConfig->pokazKategorieID)
				$this->menu.='<td><input type="hidden" name="lp" value="'.$this->z++.'"><input type="hidden" name="path" value="0">'.$row[$i]['id'].'</td>';
				
				if(true && $this->obConfig->produkty)
				$this->menu.='<td><a class="link" title="Edytuj" href="'.$this->link.'/admin/oferta/lista/kat/'.$row[$i]['id'].'"><b class="cat_main">'.$row[$i]['nazwa'].'</b></a></td>';
				else 
				$this->menu.='<td><a class="link" title="Edytuj" href="'.$this->link.'/admin/kategorie/edytuj/id/'.$row[$i]['id'].'"><b class="cat_main">'.$row[$i]['nazwa'].'</b></a></td>';
				
				$this->menu.='<td align="center"><span class="ico_drag"></span></td>';				
				if(false)
				{
					$this->menu.='<td align="center">';
					$this->menu.='<form name="pozycjaaa" action="" method="post" >
								<input type="hidden" name="id" value="'.$row[$i]['id'].'" />
								<input class="position_text" type="text" name="pozycja" value="'.$row[$i]['pozycja'].'"/>
								<div class="button2"><input title="Zapisz pozycje" type="submit" name="pozycjonuj" value="Zapisz" class="position" /></div>
								</form>';
					$this->menu.='</td>';
				}
				else if(false)
				{
					$this->menu.='<td align="center">';
					$this->menu.='<input class="position_text" type="text" id="pozycja'.$row[$i]['id'].'" name="pozycja" value="'.$row[$i]['pozycja'].'"/>';
					$this->menu.='<a title="Zmień pozycję" href="javascript:void(0);" onclick="window.location=\''.$this->link.'/admin/kategorie/lista/pozid/'.$row[$i]['id'].'/pozycja/\'+$(\'#pozycja'.$row[$i]['id'].'\').val();"><img src="'.$this->link.'/public/images/admin/change.png" /></a>';
					$this->menu.='</td>';
				}
				
				if(true)
				$this->menu.='<td align="center"><a title="Zmień widoczność" href="'.$this->link.'/admin/kategorie/lista/pokazid/'.$row[$i]['id'].'/widoczny/'.(@intval(!$row[$i]['widoczny'])).'">
				<img src="'.$this->link.'/public/images/admin/ikony/'.(@intval($row[$i]['widoczny']) ? 'ikona_ok' : 'ikona_usun').'.png" /></a></td>';
				else
				$this->menu.='<td align="center">&nbsp;</td>';
				
				$this->menu.='<td align="center"><a title="Zobacz" target="_blank" class="see_ico white" href="'.$this->link.'/'.$row[$i]['link'].'">Zobacz</a></td>';
                                
                if($level < 4)
                    $this->menu.= '<td align="center"><a title="Dodaj" class="add_ico white" href="'.$this->link.'/admin/kategorie/dodaj/id/'.$row[$i]['id'].'">Dodaj</a></td>';
                else
                    $this->menu.= '<td align="center"><span class="ok_label">-</span></td>';
                
				$this->menu.='<td align="center"><a title="Edytuj" class="edit_ico white" href="'.$this->link.'/admin/kategorie/edytuj/id/'.$row[$i]['id'].'">Edytuj</a></td>';
				
				if($this->edit === false)
					$this->menu.='<td align="center"><a title="Usuń" onclick="return confirm(\'Czy napewno chcesz usunąć wybraną pozycję ?\')" class="del_ico white" href="'.$this->link.'/admin/kategorie/lista/del/'.$row[$i]['id'].'">Usuń</a></td>';

				if($this->edit === false)
				{
					if($this->allegro)
					$this->menu.='<td align="center"><input type="checkbox" name="checkbox['.$row[$i]['id'].']" value="'.$row[$i]['nazwa'].'" /><a title="Wystaw" style="margin-left:10px;" href="'.$this->link.'/admin/allegroprodukt/wystawprod/katID/'.$row[$i]['id'].'"><img src="'.$this->link.'/public/images/admin/ikony/ikona_zmien.png" /></a></td>';
					if($this->eBay)
					$this->menu.='<td align="center"><input type="checkbox" name="checkbox['.$row[$i]['id'].']" value="'.$row[$i]['nazwa'].'" /><a title="Wystaw" style="margin-left:10px;" href="'.$this->link.'/admin/ebayprodukt/wystawprod/katID/'.$row[$i]['id'].'"><img src="'.$this->link.'/public/images/admin/ikony/ikona_zmien.png" /></a></td>';
				}
				$this->menu.='</tr>';
            }
			else
			{
                $this->menu.='<tr id="rowPos_'.$row[$i]['id'].'" name="rodzic_'.$row[$i]['rodzic'].'" class="white bord">';
				
				if($this->obConfig->pokazKategorieID)
				$this->menu.='<td><input type="hidden" name="lp" value="'.$this->z++.'"><input type="hidden" name="path" value="'.$z.'">'.$row[$i]['id'].'</td>';
                
				if(true && $this->obConfig->produkty)
				$this->menu.='<td><a class="link level'.$level.'" title="Edytuj" href="'.$this->link.'/admin/oferta/lista/kat/'.$row[$i]['id'].'"><b>'.$row[$i]['nazwa'].'</b></a></td>';
				else 
				$this->menu.='<td><a class="link level'.$level.'" title="Edytuj" href="'.$this->link.'/admin/kategorie/edytuj/id/'.$row[$i]['id'].'"><b>'.$row[$i]['nazwa'].'</b></a></td>';
				
                $this->menu.='<td align="center"><span class="ico_drag"></span></td>';
                if(false)
				{
					$this->menu.='<td align="center">';
					$this->menu.='<form name="pozycjaaa" action="" method="post" >
								<input type="hidden" name="id" value="'.$row[$i]['id'].'" />
								<input class="position_text" type="text" name="pozycja" value="'.$row[$i]['pozycja'].'"/>
								<div class="button2"><input title="Zapisz pozycje" type="submit" name="pozycjonuj" value="Zapisz" class="position" /></div>
								</form>';
					$this->menu.='</td>';
				}
				else if(false)
				{
					$this->menu.='<td align="center">';
					$this->menu.='<input class="position_text" type="text" id="pozycja'.$row[$i]['id'].'" name="pozycja" value="'.$row[$i]['pozycja'].'"/>';
					$this->menu.='<a title="Zmień pozycję" href="javascript:void(0);" onclick="window.location=\''.$this->link.'/admin/kategorie/lista/pozid/'.$row[$i]['id'].'/pozycja/\'+$(\'#pozycja'.$row[$i]['id'].'\').val();"><img src="'.$this->link.'/public/images/admin/change.png" /></a>';
					$this->menu.='</td>';
				}
				
				if(true)
				$this->menu.='<td align="center"><a title="Zmień widoczność" href="'.$this->link.'/admin/kategorie/lista/pokazid/'.$row[$i]['id'].'/widoczny/'.(@intval(!$row[$i]['widoczny'])).'">
				<img src="'.$this->link.'/public/images/admin/ikony/'.(@intval($row[$i]['widoczny']) ? 'ikona_ok' : 'ikona_usun').'.png" /></a></td>';
				else
				$this->menu.='<td align="center">&nbsp;</td>';
               
			    $this->menu.='<td align="center"><a title="Zobacz" target="_blank" class="see_ico white" href="'.$this->link.'/'.$row[$i]['link'].'">Zobacz</a></td>';
                
				if($level < 4)
                    $this->menu.= '<td align="center"><a title="Dodaj" class="add_ico white" href="'.$this->link.'/admin/kategorie/dodaj/id/'.$row[$i]['id'].'">Dodaj</a></td>';
                else
                    $this->menu.= '<td align="center"><span class="ok_label">-</span></td>';

                $this->menu.='<td align="center"><a title="Edytuj" class="edit_ico white" href="'.$this->link.'/admin/kategorie/edytuj/id/'.$row[$i]['id'].'">Edytuj</a></td>';
                
                if($this->edit === false)
                    $this->menu.='<td align="center"><a title="Usuń" onclick="return confirm(\'Czy napewno chcesz usunąć wybraną pozycję ?\')" class="del_ico white" href="'.$this->link.'/admin/kategorie/lista/del/'.$row[$i]['id'].'">Usuń</a></td>';
				
				if($this->edit === false)
				{
                    if($this->allegro)
    				$this->menu.='<td align="center"><input type="checkbox" name="checkbox['.$row[$i]['id'].']" value="'.$row[$i]['nazwa'].'" /><a title="Wystaw" style="margin-left:10px;" href="'.$this->link.'/admin/allegroprodukt/wystawprod/katID/'.$row[$i]['id'].'"><img src="'.$this->link.'/public/images/admin/ikony/ikona_zmien.png" /></a></td>';
    				if($this->eBay)
    				$this->menu.='<td align="center"><input type="checkbox" name="checkbox['.$row[$i]['id'].']" value="'.$row[$i]['nazwa'].'" /><a title="Wystaw" style="margin-left:10px;" href="'.$this->link.'/admin/ebayprodukt/wystawprod/katID/'.$row[$i]['id'].'"><img src="'.$this->link.'/public/images/admin/ikony/ikona_zmien.png" /></a></td>';
                }            
				$this->menu.='</tr>';
            }
		
            $this->display_children_admin($row[$i]['id'], $level + 1, $z++);
        }
    }
	
	function display_children_admin_select($parent, $level, $z = 0, $aaa = 0)
	{
		//if(!isset($this->dzieci)) $this->getAllWithDetails();
        $row = @$this->dzieci[$parent];
        //$row = $this->db->fetchAll('typ = "'.$this->typ.'" and rodzic = "$"');
        //var_dump($this->dzieci);die();
        $tablica = explode(';', $aaa);
        //print_r($row);
        for($i = 0; $i < count($row); $i++)
		{
			$rodzic = @intval($row[$i]['rodzic']);
			$dzieci = @count($this->dzieci[$row[$i]['id']]);
			$this->input .= '<label class="rodzic'.$rodzic.'" data-value-rodzic="'.$rodzic.'" style="margin-left:'.($level*15).'px;"><input type="checkbox" ';
			if(in_array($row[$i]['id'], $tablica)) $this->input .= 'checked="checked"';
			$this->input .= ' name="kategorie[]" value="'.$row[$i]['id'].'"/>'.$row[$i]['nazwa'].'</label>';
			if($dzieci > 0) $this->input .= '<span class="rodzic'.$rodzic.'" onclick="$(\'.rodzic'.$row[$i]['id'].'\').toggle();">&#8595</span>';
			//$this->input .= '<br/>';
            //$this->getAllWithDetails($row[$i]['id']);
            $this->display_children_admin_select($row[$i]['id'], $level + 1, $z++, $aaa);
        }
    }

    function display_children_user($parent, $poziom, $ids, $first = null, $szablon = false)
	{
        //if ($poziom != 0) 
		$this->menu .= '<div id="menu0">';
		
        if ($poziom != 0) $row = array($this->kats[$parent]);
        if ($poziom == 0) $row = $this->dzieci[0];

		$firstExist = false;
        for ($i = 0; $i < count($row); $i++) 
		{
			//var_dump($row);
			//if($row[$i]['produkty'] == 0) continue;
			
			if($poziom == 0)// || $first == null)
			{
				if($i == 0 || $firstExist) $first = ' lvl'.$poziom.'_first'; else $first = '';
				if($i == count($row) - 1) $first = ' lvl'.$poziom.'_last';
				$firstExist = false;
			}
			
			if($ids != null && is_array($ids) && array_search($row[$i]['id'], $ids) !== false)
			{
				$first .= ' lvl'.$poziom.'_active lvl_active';
				$this->active = true;
			}
			if($ids != null && is_array($ids) && $row[$i]['id'] == $ids[count($ids)-1])
			$first .= ' lvl_this_active';
			//if($ids != null && is_array($ids) && $ids[0] != $row[$i]['id'] && $poziom == 0) continue;
			
			$allegroID = 0;
			if($szablon)
			{
				$kats = explode(';', $row[$i]['allegro']);
				if(count($kats)>0)
				for($k=count($kats)-1;$k>=0;$k--)
				{
					if($kats[$k]>0)
					{
						$allegroID = $kats[$k];
						break;
					}
				}
				if(false)
				{
					$allegro = new Allegroaukcje();
					$allAll = $allegro->ileAukcjiDlaKategorii($row[$i]['id']);
				}
			}

            if ($poziom == 0 && $parent == 0)
			{
				$this->menu.='<div class="lvl'.$poziom.''.$first.'"';
				if($this->obConfig->rozwijajKategorie && $row[$i]['dzieci'] >= 0)
				$this->menu.=' onmouseover="$(\'.dd_lvl'.($poziom+1).'\').hide(); $(\'.dd_id'.$row[$i]['id'].'\').show();  //$(\'.lvl0_active\').removeClass(\'lvl0_active\'); $(this).addClass(\'lvl0_active\');" onmouseout="//$(\'.dd_id'.$row[$i]['id'].'\').hide();" ';
				$this->menu.='>';
				
				if(!$szablon)
				{
					$this->menu .= '<a href="'.$this->link.''.$row[$i]['link'].'';
					if($this->obConfig->ofertaGrupy) $this->menu .= '/grupa/'.$this->grupa;
					if($this->obConfig->ofertaGaleria) $this->menu .= '/podstrona/'.$this->podstrona;
					$this->menu .= '"';
					if($this->obConfig->rozwijajKategorie && $row[$i]['dzieci'] < 0)
					$this->menu .= ' onclick="return false;" ';
				}
				if($szablon) $this->menu .= 
					'<a href="http://allegro.pl/listing.php/user?us_id='.@$this->uid.'&category='.$allegroID.'" ';
	
				if(!$szablon)
				$this->menu .= 'onmouseover="$(this).children(\'span.ile\').css(\'visibility\',\'visible\');"  onmouseout="$(this).children(\'span.ile\').css(\'visibility\',\'hidden\');"';
				
				$this->menu .= '>';
				if($this->arrowMainPath && $ids != null && is_array($ids) && array_search($row[$i]['id'], $ids) !== false)
				$this->menu .= $this->arrowMainPath;
				else 
				if($this->arrowThis && $ids != null && is_array($ids) && $row[$i]['id'] == $ids[count($ids)-1])
				$this->menu .= $this->arrowThis;
				else 
				if($this->arrowPath && $ids != null && is_array($ids) && array_search($row[$i]['id'], $ids) !== false)
				$this->menu .= $this->arrowPath;
				else
				$this->menu .= $this->arrowMain;
				$this->menu .= '<span>'.$row[$i]['nazwa'].'</span>';
				$this->menu .= '<span class="ile" style="visibility:hidden;">&nbsp;('.$row[$i]['produkty'].')</span>';
				$this->menu .= '</a>';
			}
            else
			{
				$this->menu.='<div class="lvl'.$poziom.''.$first.'"';
				if($this->obConfig->rozwijajKategorie && $row[$i]['dzieci'] >= 0)
				$this->menu.=' onmouseover="$(\'.dd_lvl'.($poziom+1).'\').hide(); $(\'.dd_id'.$row[$i]['id'].'\').show(); //$(\'.lvl'.$poziom.'_active\').removeClass(\'lvl'.$poziom.'_active\'); $(this).addClass(\'lvl'.$poziom.'_active\');"';
				$this->menu.='>';
				
				if(!$szablon)
				{
					$this->menu .= '<a href="'.$this->link.''.$row[$i]['link'].'';
					if($this->obConfig->ofertaGrupy) $this->menu .= '/grupa/'.$this->grupa;
					if($this->obConfig->ofertaGaleria) $this->menu .= '/podstrona/'.$this->podstrona;
					$this->menu .= '"';
					if($this->obConfig->rozwijajKategorie && $row[$i]['dzieci'] < 0)
					$this->menu .= ' onclick="return false;" ';
				}
				if($szablon) $this->menu .= 
					'<a href="http://allegro.pl/listing.php/user?us_id='.@$this->uid.'&category='.$allegroID.'" ';
	
				if(!$szablon)
				$this->menu .= 'onmouseover="$(this).children(\'span.ile\').css(\'visibility\',\'visible\');"  onmouseout="$(this).children(\'span.ile\').css(\'visibility\',\'hidden\');"';
				
				$this->menu .= '>';
				//$this->menu .= '&gt; ';
				
				if($this->arrowLvlsPath && $ids != null && is_array($ids) && array_search($row[$i]['id'], $ids) !== false)
				$this->menu .= $this->arrowLvlsPath;
				else 
				if($this->arrowThis && $ids != null && is_array($ids) && $row[$i]['id'] == $ids[count($ids)-1])
				$this->menu .= $this->arrowThis;
				else 				
				if($this->arrowPath && $ids != null && is_array($ids) && array_search($row[$i]['id'], $ids) !== false)
				$this->menu .= $this->arrowPath;
				else
				$this->menu .= $this->arrowLvls;
				$this->menu .= '<span>'.$row[$i]['nazwa'].'</span>';
				
				$this->menu .= '<span class="ile" style="visibility:hidden;">&nbsp;('.$row[$i]['produkty'].') </span> ';
				$this->menu .= '</a>';
			}
			
			//if($ids == null || ($ids[0] != $row[$i]['id'])) continue;
			$hide = ($ids == null || ($ids[0] != $row[$i]['id']));
			$hide = ($ids == null || (is_array($ids) && array_search($row[$i]['id'], $ids) === false));

            $res = @$this->dzieci[$row[$i]['id']];
			
			$firstExist = false;
			$active = ' lvl'.($poziom+1).'_active lvl_active';
			
			if(count($res) > 0)
			{
				$this->menu.='<div class="dd_lvl'.($poziom+1).' dd_id'.$row[$i]['id'].'"';
				if($hide) $this->menu.=' style="display:none;"';
				//if(empty($active)) $this->menu.=' style="display:none;"';
				$this->menu.='>';
			}
			
			//if(!$hide)
            for ($j = 0; $j < count($res); $j++)
			{
				if(true || $res[$j]['produkty'] > 0) //continue;
				{
				
				//if(isset($first) || $first == null)
				if($j == 0 || $firstExist) $first = ' lvl'.($poziom+1).'_first'; else $first = '';
				if($j == count($res) - 1) $first = ' lvl'.($poziom+1).'_last';
				//if($j == count($res) - 1){var_dump($j);var_dump(count($res));var_dump($res[$j]);die();}
				if($ids != null && is_array($ids) && array_search($res[$j]['id'], $ids) !== false)
				{
					$first .= ' lvl'.($poziom+1).'_active lvl_active';
					$this->active = true;
					$active = '';
				}
				if($ids != null && is_array($ids) && $res[$j]['id'] == $ids[count($ids)-1])
				$first .= ' lvl_this_active';
				$firstExist = false;
 	
				if(false)
				{
					$this->menu.='<div class="dd_lvl'.($poziom+1).' dd_id'.$row[$i]['id'].'"';
					if($hide) $this->menu.=' style="display:none;"';
					//if(empty($active)) $this->menu.=' style="display:none;"';
					$this->menu.='>';
				}
				
				$allegroID = 0;
				if($szablon)
				{
					$kats = explode(';', $res[$j]['allegro']);
					if(count($kats)>0)
					for($k=count($kats)-1;$k>=0;$k--)
					{
						if($kats[$k]>0)
						{
							$allegroID = $kats[$k];
							break;
						}
					}
					if(false)
					{
						$allegro = new Allegroaukcje();
						$all = $allegro->ileAukcjiDlaKategorii($res[$j]['id']);
					}
				}
				
				if($res[$j]['dzieci'] == 0)
				{
					$this->menu .='<div class="dl_lvl'.($poziom+1).'">';
					$this->menu .='<div class="lvl'.($poziom+1).''.$first.'"';
					if($this->obConfig->rozwijajKategorie && $row[$i]['dzieci'] >= 0)
					$this->menu .=' onmouseover="$(\'.dd_lvl'.($poziom+2).'\').hide(); $(\'.dd_id'.$row[$i]['id'].'\').show(); //$(\'.lvl'.($poziom+1).'_active\').removeClass(\'lvl'.($poziom+1).'_active\'); $(this).addClass(\'lvl'.($poziom+1).'_active\');"';
					$this->menu .='>';
					
					if(!$szablon)
					{
						$this->menu .= '<a href="'.$this->link.''.$res[$j]['link'].'';
						if($this->obConfig->ofertaGrupy) $this->menu .= '/grupa/'.$this->grupa;
						if($this->obConfig->ofertaGaleria) $this->menu .= '/podstrona/'.$this->podstrona;
						$this->menu .= '"';
					}
					if($szablon) 
					{
						$this->menu .= '<a href="http://allegro.pl/listing.php/user?us_id='.@$this->uid.'&category='.$allegroID.'" ';
					}
					
					if(!$szablon)
					$this->menu .= 'onmouseover="$(this).children(\'span.ile\').css(\'visibility\',\'visible\');"  onmouseout="$(this).children(\'span.ile\').css(\'visibility\',\'hidden\');"';					
					
					$this->menu .= '>';
					//$this->menu .= '&gt; ';
					
					//if($poziom <= 1)
					if($this->arrowLvlsPath && $ids!=null && is_array($ids) && array_search($res[$j]['id'],$ids)!==false)
					$this->menu .= $this->arrowLvlsPath;
					else 
					if($this->arrowThis && $ids != null && is_array($ids) && $res[$j]['id'] == $ids[count($ids)-1])
					$this->menu .= $this->arrowThis;
					else
					if($this->arrowPath && $ids != null && is_array($ids) && array_search($res[$j]['id'], $ids) !== false)
					$this->menu .= $this->arrowPath;
					else
					$this->menu .= $this->arrowLvls;
					
					$this->menu .= '<span>'.$res[$j]['nazwa'].'</span>';
					$this->menu .= '<span class="ile"';
					if(!$szablon) $this->menu .= ' style="visibility:hidden;"';
					$this->menu .= '>&nbsp;('.$res[$j]['produkty'].') </span> ';
					$this->menu .= '</a></div></div>';
				}
				else
				{
                    $this->display_children_user($res[$j]['id'], $poziom + 1, $ids, null);
                }
				
				//$this->menu.='</dd>';
				}
				
				if(false)
				if(!$szablon && $j == count($res) - 1)
				{
					$this->menu.='<div class="dd_lvl'.($poziom+1).' dd_id'.$i.'"';
					if($hide) $this->menu.=' style="visibility:hidden;"';
					$this->menu.='>';
					
					$this->menu .= '<div class="dl_lvl'.($poziom+1).'"><div class="lvl'.($poziom+1).' lvl'.($poziom+1).'_all'.$active.'">';
					
					$this->menu .= '<a href="'.$this->link.$row[$i]['link'].'" ';
					
					if(!$szablon)
					$this->menu .= 'onmouseover="$(this).children(\'span.ile\').css(\'visibility\',\'visible\');"  onmouseout="$(this).children(\'span.ile\').css(\'visibility\',\'hidden\');"';
					
					$this->menu .= '>Wszystkie';
					$this->menu .= '<span class="ile"';
					if(!$szablon) $this->menu .= ' style="visibility:hidden;"';
					$this->menu .= '>&nbsp;('.$row[$i]['produkty'].') </span> ';
					$this->menu .= '</a></div></div>';
					$this->menu .= '</div>';
				}
            }
			if(count($res) > 0) $this->menu.='</div>';
			$this->menu .= '</div>';
        }
        //if ($poziom != 0) 
		$this->menu.='</div>';
    }
    function display_children_user_link($parent, $poziom, $style_main='link', $style_sub='link')
	{
        if ($poziom != 0)
            $this->menu.='<dl>';
        if ($poziom != 0)
            $where = 'id="'.$parent.'" AND lang = "'.$this->lang.'"';
        if ($poziom == 0)
            $where = 'rodzic="'.$parent.'" AND lang = "'.$this->lang.'"';
        $order = ' pozycja ASC';
        $result = $this->fetchAll($where, $order);
        $row = $result->toArray();

        for ($i = 0; $i < count($row); $i++)
		{
            if ($poziom == 0 && $parent != 0)
                $this->menu.='<dd><a class="'.$style_main.'" href="'.$this->link.''.$row[$i]['id'].'">'.str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $poziom).' '.$row[$i]['nazwa'].'</a></dd>';
            else
                $this->menu.='<dt><a class="'.$style_main.'" href="'.$this->link.''.$row[$i]['id'].'">'.str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $poziom).' '.$row[$i]['nazwa'].'</a></dt>';

            $where = 'rodzic="'.$row[$i]['id'].'" AND lang = "'.$this->lang.'"';
            $order = ' pozycja ASC';
            $result = $this->fetchAll($where, $order);
            $res = $result->toArray();
            for ($j = 0; $j < count($res); $j++) // kol = $res[$j]
			{
                $where = 'rodzic="'.$res[$j]['id'].'" AND lang = "'.$this->lang.'"';
                $order = ' pozycja ASC';
                $result = $this->fetchAll($where, $order);
                $pot = $result->toArray();
                $this->menu.='<dd>';
                if (count($pot) > 0)
                    $this->display_children_user_link($res[$j]['id'], $poziom + 1, $style_sub, $style_sub);
                else
                    $this->menu.='<a class="'.$style_sub.'" href="'.$this->link.''.$res[$j]['id'].'">'.str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $poziom + 1).' '.$res[$j]['nazwa'].'</a>';
                $this->menu.='</dd>';
            }
        }
        if ($poziom != 0)
            $this->menu.='</dl>';
    }    
    
    function changePositionAjax($position, $id_cat)
	{
        $dane['pozycja'] = $position;
        $where = 'id = '.$id_cat;
        $this->update($dane, $where);
    }

    function changePozycja($id, $pozycja)
	{
        $update = array('pozycja' => $pozycja);
        $where = 'id = '.$id;
        $this->update($update, $where);
    }	
	
	public function renderuj_menu_glowne()
	{
		$this->_name = 'Podstrony';
		$where = 'position = "menu" AND (polozenie = "top" OR polozenie = "topbottom") AND lang = "'.$this->lang.'"';
		$where.= ' AND lp > 0 AND rodzic = 0';
		$order = 'lp ASC';
        $result = $this->fetchAll($where, $order)->toArray();
		return $result;
	}
	function showWybranyRodzicPodstrony($id = 0)
	{
		$this->_name = 'Podstrony';
        return $this->fetchAll('rodzic = '.$id.' AND lang = "'.$this->lang.'"', 'pozycja');
    }
	function showWybraniRodzicePodstrony()
	{
		$this->_name = 'Podstrony';
        $result = $this->fetchAll('parent <> "" AND lang = "'.$this->lang.'"', 'temat');
		return $this->common->sortByPole($result, 'parent', 'link', 'rodzic');
    }
	public function renderuj_menu_dolne()
	{
		$this->_name = 'Podstrony';
		$where = 'position = "menu" AND (polozenie = "bottom" OR polozenie = "topbottom")';
		$where.= ' AND p.lang = "'.$this->lang.'" AND lp is not null';
        //$result = $this->fetchAll($where, $order)->toArray();
		$sql = $this->db->select()->from(array('p' => 'Podstrony'), array('*'));
		if($this->obConfig->podstronyGaleria)
		{
			$whereGal = 'p.id = g.wlasciciel and g.typ = "podstrony" and g.glowne = "T"';
			$whereGal.= ' and g.lang = "'.$this->lang.'" and g.wyswietl = "1"';
			$sql->joinleft(array('g' => 'Galeria'), $whereGal, array('img', 'img_opis' => 'nazwa'));
		}
		$sql->where($where)->order('lp ASC');
		$result = $this->db->fetchAll($sql);
        return $result;
	}

    function kasuj($id)
	{
        $route = new Routers();
        $k_where = 'id = '.$id;
        $wynik = $this->fetchRow($k_where);

		if(@intval($wynik['route_id']) > 0)
        $route->usun($wynik['route_id']);

        $kateg_prod = new Katprod();
        $kateg_prod ->id = $id;
        $kateg_prod ->deleteKategoria();
 
        $where = 'id = '.$id;
        $this->delete($where);
        $s_where = 'rodzic = '.$id;
        $result = $this->fetchAll($s_where);
        $rows = $result->toArray();

        for($i = 0; $i < count($rows); $i++)
		{
            $id = $rows[$i]['id'];
            if($id)
			{
                $kateg_prod ->id = $id;
                $kateg_prod ->deleteKategoria();
                $this->kasuj($id);
            }
        }
    }
    function wypiszDzieci($rodzic)
	{
        $where = 'k.rodzic = '.$rodzic.' and k.typ = "'.$this->typ.'" and k.lang = "'.$this->lang.'"';
		if($this->module != 'admin') $where .= ' and k.widoczny = "1"';
        $order = 'k.pozycja';
        //$result = $this->fetchAll($where, $order)->toArray();
		$sql = $this->db->select()->from(array('k' => 'Kategorie'), array('*'));
		if($this->obConfig->podstronyGaleria)
		{
			$whereGal = 'k.id = g.wlasciciel and g.typ = "kategorie" and g.glowne = "T"';
			$whereGal.= ' and g.lang = "'.$this->lang.'" and g.wyswietl = "1"';
			$sql->joinleft(array('g' => 'Galeria'), $whereGal, array('img', 'img_opis' => 'nazwa'));
		}
		$sql->where($where)->order($order);
		$result = $this->db->fetchAll($sql);
        return $result;
    }
	function wypiszDzieciProducent($rodzic = 0, $producent = 0)
	{
        $where = 'rodzic = '.$rodzic.' and producent = '.$producent;
        $order = 'pozycja';
        $select = $this->db->select()->from(array('k' => 'Kategorie'), array('*'))
			->joinleft(array('kp' => 'Katprod'), 'k.id = kp.id_kat', array(''))
			->joinleft(array('p' => 'Produkty'), 'p.id = kp.id_prod', array(''))
			->where($where)->group('k.id')->order($order);
		//echo $select;die();
		$result = $this->db->fetchAll($select);
        return $result;
    }

    function dodaj_nowa($rodzic, $nazwa, $typ = 'kategorie', $img = '')
	{
        $where = 'rodzic = '.$rodzic;
        $order = 'pozycja DESC';
        $limit = 1;
        $result = $this->fetchAll($where, $order, $limit);
		//var_dump($result);
        $arr = $result->toArray();
        $poz = $rodzic == 0 ? @$arr[0]['pozycja'] + 10 : @$arr[0]['pozycja'] + 1;
		$array = array('rodzic'=>$rodzic,'nazwa'=>$nazwa,'typ'=>$typ,'img'=>$img,'lang'=>$this->lang,'pozycja'=>$poz);
        $this->insert($array);
        return $this->getAdapter()->lastInsertId();
    }
	
	function zapiszDoBazy($id, $nazwa, $wlasciciel, $nazwa_obrazka)
	{
        if($id == 1) $dane = array('img' => $nazwa, 'img_opis' => $nazwa_obrazka);
		if($id == 2) $dane = array('img1' => $nazwa, 'img1_opis' => $nazwa_obrazka);
		if($id == 3) $dane = array('img2' => $nazwa, 'img2_opis' => $nazwa_obrazka);
        //print_r($dane);
        $this->update($dane, 'id = '.$wlasciciel);
    }
	
	function zmienNazwe($nazwa,$id,$imgID)
	{
        if($imgID == 1) $this->update(array('img_opis' => $nazwa), 'id = '.$id);
		if($imgID == 2) $this->update(array('img1_opis' => $nazwa), 'id = '.$id);
		if($imgID == 3) $this->update(array('img2_opis' => $nazwa), 'id = '.$id);
    }
	
	function usunZdjecie($imgID,$id)
	{
		if($imgID == 1) $this->update(array('img' => '', 'img_opis' => ''), 'id = '.$id);
		if($imgID == 2) $this->update(array('img1' => '', 'img1_opis' => ''), 'id = '.$id);
		if($imgID == 3) $this->update(array('img2' => '', 'img2_opis' => ''), 'id = '.$id);
    }

    function showWybranaKategoria($id)
	{
		$where = 'id = '.$id;
        $result = $this->fetchRow($where);
        return $result;
    }
    function showWybranyRodzic($id)
	{
        $where = 'rodzic = '.$id;
        $order = 'pozycja';
        $result = $this->fetchAll($where,$order)->toArray();
        return $result;
    }
    public function edytujNazwa($id, $nazwa, $img = null)
	{
        $where = 'id = '.$id;
        $array = array('nazwa' => $nazwa);
        if($img)
		{
            $array = array('nazwa' => $nazwa, 'img' => $img);
        }
        $this->update($array, $where);
    }

    public function edytuj($id, $data)
	{
        $where = 'id = '.$id;
        $this->update($data, $where);
    }
	function setLink($dane, $link, $lang = null)
	{
		if(empty($link)) return;
		$where = 'link = "'.$link.'"';
		$this->update($dane, $where);
		if(empty($lang)) return;
		$dane['link'] .= '-'.$lang;
		$where = 'link = "'.$link.'-'.$lang.'"';
		//var_dump($where); die();
		$this->update($dane, $where);
	}

    function get_path($id)
	{
        $where = 'id = '.$id;
        $res = $this->fetchAll($where);
        //var_dump($where);
        $path = array();
        if(@$res[0]->rodzic != '')
		{
            $path[] = array('id'=>$res[0]->id,'rodzic'=>$res[0]->rodzic,'nazwa'=>$res[0]->nazwa,'link'=>$res[0]->link);
            $path = array_merge($this->get_path($res[0]->rodzic), $path);
        }
        return $path;
    }
	function getPath($id)
	{
        $res = @$this->katAll[$id];
        //print_r($res);
        $path = array($res);
        if(@intval($res['rodzic']) > 0)
		{
            $path = array_merge($this->getPath($res['rodzic']), $path);
        }
        return $path;
    }
	function getPaths($typ = 'kategorie', $sep = '/', $last = '>', $spa = ' ')
	{
        if(@intval($this->katAll) > 0)
		foreach($this->katAll as $id => $kateg)
		{
            $this->paths[$id] = $this->getPath($kateg['id']);
			$this->katAll[$id]['kats'] = $this->paths[$id];
			$this->katAll[$id]['path'] = $this->getPathString($kateg['id'], $this->paths[$id], $sep, $last, $spa);
        }
    }
	function getPathString($id, $nawigacja = false, $sep = '/', $last = '>', $spa = ' ')
	{
		if($nawigacja === false) $nawigacja = $this->get_path($id);
		$result = '';
        for($i = 0; $i < count($nawigacja); $i++)
		{
			if(@$nawigacja[$i]['typ'] != $this->typ) continue;
			$result .= $nawigacja[$i]['nazwa'];
			if($i < count($nawigacja) - 2) $result .= $spa.$sep.$spa;
			if($i == count($nawigacja) - 2) $result .= $spa.$last.$spa;
		}
        return $result;
    }
	
	public function podstronyAdmin($sort = 'id')
	{
		$this->_name = 'Podstrony';
		$where = 'position = "menu" and lp is not null';// and (short is not null or tekst is not null)';
		//if(@!$this->obConfig->regulamin) $where .= ' and tytul <> "'.$this->common->lang('Regulamin').'"';
		if($this->obConfig->podstronyMenu)
		{
			$where = 'p.position = "menu" and p.lp is not null';
			$temat = 'if(LENGTH(pp.temat) > 0, CONCAT(p.temat, " - ", pp.temat), p.temat)';
			$temat = 'if(LENGTH(k.nazwa) > 0, CONCAT(p.temat, " - ", k.nazwa), '.$temat.')';
			$temat = 'if(LENGTH(kk.nazwa) > 0, CONCAT(p.temat, " - ", k.nazwa, " - ", kk.nazwa), '.$temat.')';
			$sql = $this->db->select()->from(array('p' => 'Podstrony'), array('*', 'temat' => $temat));
			$sql->joinleft(array('pp' => 'Podstrony'), 'pp.id=p.rodzic and p.parent="podstrony"', array(''));
			$sql->joinleft(array('k' => 'Kategorie'), 'k.id=p.rodzic and p.parent="kategorie"',	array(''));
			$sql->joinleft(array('kk' => 'Kategorie'), 'kk.id=k.rodzic and k.typ="kategorie"', array(''));
			$sql->where($where.' and p.lang = "'.$this->lang.'"')->order($sort);
			$result = $this->db->fetchAll($sql);
		}
		else $result = $this->fetchAll($where.' and lang = "'.$this->lang.'"', str_replace('p.','',$sort));
		//echo $sql;
		return $result;
	}
	public function podstronyNotMenuAdmin($sort = 'id')
	{
		$this->_name = 'Podstrony';
		$where = 'position = "menu" and (short is not null or tekst is not null) and lp is null and polozenie is null';
        $result = $this->fetchAll($where.' and lang = "'.$this->lang.'"', $sort);
		return $result;
	}
	
	public function blokAdmin()
	{
		$this->_name = 'Podstrony';
		$where = 'position = "blok" and (short is not null or tekst is not null) and lp is null and polozenie is null';
        $result = $this->fetchAll($where.' and lang = "'.$this->lang.'"', 'tytul asc');
		return $result;
	}	
	public function blokAllegroAdmin()
	{
		$this->_name = 'Podstrony';
		$where = 'position = "allegro" and (short is not null or tekst is not null) and lp is null and polozenie is null';
        $result = $this->fetchAll($where.' and lang = "'.$this->lang.'"', 'tytul asc');
		return $result;
	}
	public function blokLojalnoscAdmin()
	{
		$this->_name = 'Podstrony';
		$where = 'position = "lojalnosc" and (short is not null or tekst is not null) and lp is null and polozenie is null';
        $result = $this->fetchAll($where.' and lang = "'.$this->lang.'"', 'tytul asc');
		return $result;
	}
	
	function szukajNazwaRodzic($nazwa = '', $rodzic = 0)
	{
        $where = 'nazwa = "'.$nazwa.'" and rodzic = '.$rodzic;
        $result = $this->fetchAll($where);
        return $result;
    }
	
	function getKategorie($pole = 'id')
	{
		$sql = $this->select()->order('nazwa');
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i][$pole]] = $result[$i]->toArray();
		}
		return @$results;
	}
}
?>