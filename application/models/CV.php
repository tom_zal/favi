<?php
class CV extends Zend_Db_Table 
{
    public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

    function dodaj($dane) 
	{
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }
    function edytuj($dane) 
	{
        $where = 'id = '.$this->id;
        $this->update($dane, $where);
    }
    function wypisz($szukanie, $sortowanie) 
	{
		$where = 'id > 1';
		if($szukanie->nazwa != '') 
		$where .= ' and imie like "%'.$szukanie->nazwa.'%" or nazwisko like "%'.$szukanie->nazwa.'%"';
		if($szukanie->woj != 'all') $where .= ' and woj = "'.$szukanie->woj.'"';
		if($szukanie->miasto != 'all') $where .= ' and miasto = "'.$szukanie->miasto.'"';
		if($szukanie->plec != 'all') $where .= ' and plec = "'.$szukanie->plec.'"';
		if($szukanie->ulubione != '') $where .= ' and ulubione = "1"';
		
        $sql = $this->select()->where($where)->order($sortowanie->sort.' '.$sortowanie->order);
        $result = $this->fetchAll($sql);
        return $result;
    }
	function wypiszMiasta() 
	{
		$where = 'id > 1';		
        $sql = $this->select()->distinct()->from(array('m' => 'CV'), 'miasto')->where($where)->order('miasto asc');
        $result = $this->fetchAll($sql);
        return $result;
    }
    function usun() 
	{
        $result = $this->delete('id = '.$this->id);
    }
    function wypiszPojedynczy() 
	{
        $result = $this->fetchRow('id = '.$this->id);
        return $result;
    }
    function wypiszPojedynczyArray() 
	{		
        $result = $this->fetchRow('id = '.$this->id);
        $array = $result->toArray();
        return $array;
    }
}
?>