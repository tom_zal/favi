<?php
class Atrybuty extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		
		$this->typatr = array
		(
			array('nazwa' => 'pole tekstowe', 'typatrybut' => 'tekst'),
			//array('nazwa' => 'pole wyboru TAK / NIE', 'typatrybut' => 'wybor'),
			array('nazwa' => 'pole jednokrotnego wyboru', 'typatrybut' => 'lista'),
			array('nazwa' => 'pola wielokrotnego wyboru', 'typatrybut' => 'multi')
		);
		$this->typpola = array('tekst' => 'Tekst', 'int' => 'Liczba całkowita', 'float' => 'Liczba rzeczywista');
		$this->opcjeWybor = array(1 => 'NIE', 2 => 'TAK');
    }

	function _save($data)
	{
		$this->insert($data);
		//$this->cache->remove('cache_'.$this->cachename.'_'.$this->lang);
		return $this->getAdapter()->lastInsertId();
	}

	function _update($data)
	{
		$this->update($data, 'id = '.$this->id);
		//$this->cache->remove('cache_'.$this->cachename.'_'.$this->lang);
	}

	function _delete()
	{
		$this->delete('id = '.$this->id);
		//$this->cache->remove('cache_'.$this->cachename.'_'.$this->lang);
	}
	function _deleteGroup()
	{
		$this->delete('id_gr = '.$this->id);
		//$this->cache->remove('cache_'.$this->cachename.'_'.$this->lang);
	}

	function getRow()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function getRows($id = 0)
	{
		$where = 'lang = "'.$this->lang.'" and (id_prod = 0 or id_prod = '.$id.')';
        $result = $this->fetchAll($where, 'nazwa ASC');
		return $result;
	}
	function getRowsSearch($id = 0)
	{
		$where = 'lang = "'.$this->lang.'" and (id_prod = 0 or id_prod = '.$id.')';
		$result = $this->fetchAll($where.' AND wyszukiwarka = "1" AND status = "1"', 'nazwa ASC');
		return $result;
	}
	function getRowsGroup($id = 0)
	{
		$where = 'lang = "'.$this->lang.'" and (id_prod = 0 or id_prod = '.$id.')';
		$result = $this->fetchAll($where.' AND id_gr = "'.$this->id.'"', array('kolejnosc asc', 'nazwa ASC'));
		return $result;
	}
	function getRowsGroupFr($id = 0)
	{
		$where = 'lang = "'.$this->lang.'" and (id_prod = 0 or id_prod = '.$id.')';
		$result = $this->fetchAll($where, 'nazwa ASC');
		return $result;
	}	
	
	function getAtrybutyOld($ids = null)
	{
		if(!$this->common->isArray($ids, true)) return null;
		$where = 'at.id in ('.implode(',',$ids).')';
		$select = $this->db->select()->from(array('at' => 'Atrybuty'), array('id', 'id_gr', 'nazwa as wartosc'))
			->joinleft(array('ag' => 'Atrybutygrupy'), 'at.id_gr = ag.id', array('nazwa','typatrybut','jedn_miary'))
			->where($where)->order(array('ag.nazwa asc', 'at.nazwa asc'));
		//echo $select;return null;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function getAtrybutyOld2($ids = null, $sort = '', $multi = null, $prodID = null, $grupaID = null, $keySort = false)
	{
		if($prodID === null && !$this->common->isArray($ids, true)) return null;
		$where = 'ag.lang = "'.$this->lang.'"';
		if(is_array($ids)) $where .= ' and at.id in ('.implode(',',$ids).')';
		if($prodID !== null) $where .= ' and (at.id_prod = '.$prodID.' or at.id_prod = 0)';
		if($grupaID !== null) $where .= ' and at.id_gr = '.$grupaID; // or at.id_gr = 0)';
		$select = $this->db->select()->from(array('at' => 'Atrybuty'), array('id', 'id_gr', 'nazwa as wartosc'))
			->joinleft(array('ag' => 'Atrybutygrupy'), 'at.id_gr = ag.id', array('nazwa','typatrybut','jedn_miary'));
		if($grupaID > 0) $select->join(array('ap' => 'Atrybutypowiazania'), 'ap.id_at = at.id', array(''));
		if($grupaID > 0) $where .= ' and at.id_gr = ap.id_gr and ap.id_og = '.$prodID;
		$select->where($where)->order(array('ag.poz asc', 'ag.nazwa asc', 'at.nazwa asc'));
		//echo $select;die();//return null;
		$result = $this->db->fetchAll($select);
		if(!empty($sort)) $result = $this->common->sortByPole($result, $sort, $multi);
		if(!empty($sort) || $keySort) ksort($result);
		return $result;
	}
	function getAtrybuty($ids = 1, $sort = '', $multi = null, $prodID = null, $grupaID = null, $keySort = false, $kats = null)
	{
            
//		if($prodID === null && !$this->common->isArray($ids, true)) return null;
		$where = 'ag.lang = "'.$this->lang.'"';
		if(is_array($ids)) $where .= ' and at.id in ('.implode(',',$ids).')';
		if($prodID !== null) $where .= ' and (at.id_prod = '.$prodID.' or at.id_prod = 0)';
		if($grupaID !== null) $where .= ' and at.id_gr = '.$grupaID; // or at.id_gr = 0)';
                
		$select = $this->db->select()->from(array('at' => 'Atrybuty'), array('id', 'id_gr', 'nazwa as wartosc', 'kolejnosc'))
			->joinleft(array('ag' => 'Atrybutygrupy'), 'at.id_gr = ag.id', array('nazwa','typatrybut','jedn_miary','jedn_miary_typ','poz'));
		if($grupaID > 0) $select->join(array('ap' => 'Atrybutypowiazania'), 'ap.id_at = at.id', array(''));
		if($grupaID > 0) $where .= ' and at.id_gr = ap.id_gr and ap.id_og = '.$prodID;
		
		if($this->obConfig->atrybutyKategorie)
		{
			if(is_array($kats) && count($kats) > 0)
			{
				$where .= ' and (ag.id_kat is null or REPLACE(ag.id_kat, ";", "") = ""';
				foreach($kats as $kat)
				if(@intval($kat['id'] > 0))
				$where .= ' or CONCAT(";", ag.id_kat, ";") like "%;'.$kat['id'].';%"';
				$where .= ' or 0)';
			}
			else if($kats) $where .= ' and (ag.id_kat is null or REPLACE(ag.id_kat, ";", "") = "")';
		}
		$sortuj = 'if(ag.jedn_miary_typ <> "tekst", CAST(at.nazwa as DECIMAL), at.nazwa)';
		$select->where($where)->order(array('ag.poz asc', 'ag.nazwa asc', $sortuj.' asc'));
//		echo $select;die;//return null;
		$result = $this->db->fetchAll($select);
		//var_dump($result);die();
		if(@count($result) > 0) usort($result, array($this, 'cmpAll'));
		//var_dump($result);die();
		if(!empty($sort)) $result = $this->common->sortByPole($result, $sort, $multi);
		if(!empty($sort) || $keySort) ksort($result);
		//if(@count($ids)>3) { var_dump($result); die(); }
		return $result;
	}
	
	function getAtrybutyAllAdmin($id = 0, $kategorieMain = null)
	{
		$ObjectA = new Atrybuty();
		$ObjectA->cache = false;
		$ObjectA->cachename = 'atrybuty_atrybuty';
		$ObjectA->lang = $this->lang;
		$ObjectG = new Atrybutygrupy();
		$ObjectG->lang = $this->lang;
		$ObjectAp = new Atrybutypowiazania();
		$ObjectAp->id = $id;
			
		for($t=0;$t<count($kategorieMain);$t++)
		$kategorie_id[] = $kategorieMain[$t]['id_kat'];

		if(isset($kategorie_id) && !empty($kategorie_id))
		$this->grupy = $ObjectG->getRowsa($id, $kategorie_id);
		else $this->grupy = $ObjectG->getRows($id);
					
		$this->atrybuty = $ObjectA->getRows($id)->toArray();
		$this->atrybutyuniq = $ObjectAp->getRowsUnique();
		
		$array = array();
		$array2 = array();
		$array3 = array();
		$array4 = array();
		
		if(!empty($this->atrybutyuniq))
		foreach($this->atrybutyuniq as $one)
		{
			$array[] = $one->id_at;
			if(@intval($one['id_gr']) > 0) $array2[$one['id_gr']] = $one['wartosc'];
			$atrybutyuniq[$one['id_gr']][$one['id_at']] = $one->toArray();
			if(@intval($one['id_at']) == 0 || $one['typatrybut'] == 'lista') $array3[$one['id_gr']] = $one['ceny'];
			if(@intval($one['id_at']) == 0 || $one['typatrybut'] == 'lista') $array4[$one['id_gr']] = $one['pozycja'];
		}
		$this->atrybutyuniq = $array;
		$this->atrybutyuniq2 = $array2;
		$this->atrybutyuniq3 = $array3;
		$this->atrybutyuniq4 = $array4;
  
		$array = array();
		$g = 0;

		if(!empty($this->grupy))
		foreach($this->grupy as $grupa)
		{
			$a = 0;
			$array[$g]['grupa'] = $grupa['nazwa'];
			$array[$g]['grupa_id'] = $grupa['id'];
			$array[$g]['id_rodzaj'] = $grupa['id_rodzaj'];
			$array[$g]['id_prod'] = $grupa['id_prod'];
			$array[$g]['id_kat'] = $grupa['id_kat'];
			$array[$g]['typatrybut'] = $grupa['typatrybut'];
			$array[$g]['jedn_miary'] = $grupa['jedn_miary'];
			$array[$g]['jedn_miary_typ'] = $grupa['jedn_miary_typ'];
			//var_dump($this->atrybuty);
			if(!empty($this->atrybuty))
			{
				foreach($this->atrybuty as $atrybut)
				{
					if($grupa['id'] == $atrybut['id_gr'])
					{
						$a = $atrybut['id'];
						$array[$g]['atrybut'][$a] = $atrybut;
						$array[$g]['atrybut'][$a]['checked'] = @in_array($atrybut['id'], $this->atrybutyuniq);
						$array[$g]['atrybut'][$a]['img'] = @$atrybutyuniq[$atrybut['id_gr']][$atrybut['id']]['img'];
						if($grupa['typatrybut'] == 'lista' && $array[$g]['atrybut'][$a]['checked'])
						$array[$g]['checked'] = $atrybut['id'];
						if($grupa['typatrybut'] == 'multi' && $array[$g]['atrybut'][$a]['checked'])
						$array[$g]['checked'] = true;
						//if($grupa['typatrybut'] == 'lista') $a = 0;
						$array[$g]['atrybut'][$a]['dane'] = @$atrybutyuniq[$atrybut['id_gr']][$atrybut['id']];
						$a++;//die();
					}
				}
			}
			if(@count($array[$g]['atrybut']) > 0)
			{
				//var_dump($array[$g]);die();
				$this->sorter = 'nazwa';
				$this->sortFunc = '';
				if($array[$g]['jedn_miary_typ'] == "int") $this->sortFunc = 'intval';
				if($array[$g]['jedn_miary_typ'] == "float") $this->sortFunc = 'floatval';
				$this->sortOrder = 'asc';
				if(true && !empty($this->sortFunc))
				uasort($array[$g]['atrybut'], array($this, 'cmpAll'));
			}
			if(!strcmp("wybor", $grupa['typatrybut']))
			{
				$array[$g]['wartosc'] = @$this->atrybutyuniq2[$grupa['id']];
			}
			if(!strcmp("tekst", $grupa['typatrybut']))
			{
				$array[$g]['wartosc'] = @$this->atrybutyuniq2[$grupa['id']];
			}
			if(true && $this->obConfig->atrybutyCenyRozne)
			{
				$array[$g]['ceny'] = @$this->atrybutyuniq3[$grupa['id']];
			}
			if(true && $this->obConfig->atrybutyProdPozycja)
			{
				$array[$g]['pozycja'] = @$this->atrybutyuniq4[$grupa['id']];
			}
			$g++;
		}
		
		if(true && $this->obConfig->atrybutyProdPozycja)
		{
			$this->sorter = 'pozycja';
			$this->sortOrder = 'asc';
			usort($array, array($this, 'cmpAll'));
		}
		
		//var_dump($array);die();
		return $array;
	}
	
	function getAtrybutyAll($id = 0, $produkt = null, $typ_klienta = 0, $sort = '')
	{
		$ObjectA = new Atrybuty();
		$ObjectA->lang = $this->lang;
		$ObjectG = new Atrybutygrupy();
		$ObjectG->lang = $this->lang;
		$ObjectAp = new Atrybutypowiazania();
		$ObjectAp->id = $id;
		$this->grupy = $ObjectG->getRows($id, array('if(typatrybut="multi",1,0) desc', 'nazwa asc'))->toArray();
		$this->atrybuty = $ObjectA->getRows($id)->toArray();
		$this->atrybutyuniq = $ObjectAp->getRowsUnique();
		//var_dump($this->grupy);die();
		$array = array();
		$array2 = array();
		$array3 = array();
		$array4 = array();

		if(!empty($this->atrybutyuniq))
		foreach($this->atrybutyuniq as $one)
		{
			$array[] = $one->id_at;
			if(isset($one['id_gr']) && $one['id_gr'] > 0)
			{
				$array2[$one['id_gr']]['wartosc'] = $one['wartosc'];				
				$array2[$one['id_gr']]['wybor'] = isset($one['wybor']) ? $one['wybor'] : null;
			}
			$atrybutyuniq[$one['id_gr']][$one['id_at']] = $one->toArray();
			if(@intval($one['id_at']) == 0 || $one['typatrybut'] == 'lista') $array3[$one['id_gr']] = $one['ceny'];
			if(@intval($one['id_at']) == 0 || $one['typatrybut'] == 'lista') $array4[$one['id_gr']] = $one['pozycja'];
		}
		$this->atrybutyuniq = $array;
		$this->atrybutyuniq2 = $array2;
		$this->atrybutyuniq3 = $array3;
		$this->atrybutyuniq4 = $array4;

		$array = array();
		$arrat = array();
		$g = 0;
		//var_dump($this->atrybuty);

		if(!empty($this->grupy))
		foreach($this->grupy as $grupa) 
		{
			$a = 0;
			//var_dump($grupa);
			$array[$g]['id'] = $grupa['id'];
			$array[$g]['grupa'] = $grupa['nazwa'];
			$array[$g]['typatrybut'] = $grupa['typatrybut'];
			$array[$g]['jedn_miary'] = $grupa['jedn_miary'];
			$array[$g]['jedn_miary_typ'] = $grupa['jedn_miary_typ'];
			if(!empty($this->atrybuty)) 
			{
				$atrybuty = null;
				foreach($this->atrybuty as $atrybut) 
				{
					if($grupa['id'] == $atrybut['id_gr']) 
					{								
						if(is_array($this->atrybutyuniq) && in_array($atrybut['id'], $this->atrybutyuniq)) 
						{
							$atrybut['dane'] = @$atrybutyuniq[$atrybut['id_gr']][$atrybut['id']];
							if(@$produkt['promocja'])
							{
								if($typ_klienta)
								{
									$brutto = $atrybut['dane']['cena_promocji_b_hurt'];
									$netto = $atrybut['dane']['cena_promocji_n_hurt'];
								}
								else
								{
									$brutto = $atrybut['dane']['cena_promocji_b'];
									$netto = $atrybut['dane']['cena_promocji_n'];
								}
							}
							else
							{
								if($typ_klienta)
								{
									$brutto = $atrybut['dane']['cena_brutto_hurt'];
									$netto = $atrybut['dane']['cena_netto_hurt'];
								}
								else
								{
									$brutto = $atrybut['dane']['cena_brutto'];
									$netto = $atrybut['dane']['cena_netto'];
								}
							}
							$atrybut['dane']['brutto'] = $brutto;
							$atrybut['dane']['netto'] = $netto;
							$atrybuty[] = $atrybut;
							//var_dump($array[$g]);
						}								
					}
				}
				if(!strcmp("lista", $grupa['typatrybut']))
				$array[$g]['atrybut'] = @$atrybuty[0];
				if(!strcmp("multi", $grupa['typatrybut']))
				$array[$g]['atrybut'] = $atrybuty;
			}
			if(@count($array[$g]['atrybut']) > 0)
			{
				$typ = @$array[$g]['jedn_miary_typ'];
				$array[$g]['atrybut'] = $this->common->compare($array[$g]['atrybut'], true, 'nazwa', $typ, 'asc');
			}
			if(!strcmp("tekst", $grupa['typatrybut'])) 
			{
				if(isset($this->atrybutyuniq2[$grupa['id']]) && !empty($this->atrybutyuniq2[$grupa['id']]))
				{
					$array[$g]['wartosc'] = $this->atrybutyuniq2[$grupa['id']]['wartosc'];
				}
			}
			if(!strcmp("wybor", $grupa['typatrybut'])) 
			{
				if(isset($this->atrybutyuniq2[$grupa['id']]) && !empty($this->atrybutyuniq2[$grupa['id']]))
				{
					$array[$g]['wybor'] = $this->atrybutyuniq2[$grupa['id']]['wybor'];
					$array[$g]['wartosc'] = $this->atrybutyuniq2[$grupa['id']]['wartosc'];
				}
			}
			if(true && $this->obConfig->atrybutyCenyRozne)
			{
				$array[$g]['ceny'] = @$this->atrybutyuniq3[$grupa['id']];
			}
			if(true && $this->obConfig->atrybutyProdPozycja)
			{
				$array[$g]['pozycja'] = @$this->atrybutyuniq4[$grupa['id']];
			}
			//var_dump($array[$g]);
			$g++;
		}
		usort($array, array($this, 'cmpAtr'));
		//var_dump($array);die();
		if(!empty($sort)) return $this->common->sortByPole($array, $sort);
		return $array;
	}
	function cmpAtr($a, $b)
	{
		//var_dump($a);die();
		$nazwaA = isset($a['grupa']) ? $a['grupa'] : @$a['atrybut'];
		$nazwaB = isset($b['grupa']) ? $b['grupa'] : @$b['atrybut'];
		$pozycjaA = @intval($a['pozycja']);
		$pozycjaB = @intval($b['pozycja']);
		$cenyTrybA = @is_array($a['atrybut']) ? @intval($a['atrybut'][0]['dane']['cenyTryb']) : 0;
		$cenyTrybB = @is_array($b['atrybut']) ? @intval($b['atrybut'][0]['dane']['cenyTryb']) : 0;
		$typAtrybutA = @strval($a['typatrybut']);
		$typAtrybutB = @strval($b['typatrybut']);
		$typAtrybutA = $typAtrybutA=="multi" ? ($cenyTrybA?5:4) : ($typAtrybutA=="tekst" ? 3 : $typAtrybutA=="lista" ? 2 : 1);
		$typAtrybutB = $typAtrybutB=="multi" ? ($cenyTrybB?5:4) : ($typAtrybutB=="tekst" ? 3 : $typAtrybutB=="lista" ? 2 : 1);
		$cmp1 = $pozycjaA != $pozycjaB ? $pozycjaA : ($cenyTrybA != $cenyTrybB ? $cenyTrybA : ($typAtrybutA != $typAtrybutB ? $typAtrybutA : $nazwaA));
		$cmp2 = $pozycjaA != $pozycjaB ? $pozycjaB : ($cenyTrybA != $cenyTrybB ? $cenyTrybB : ($typAtrybutA != $typAtrybutB ? $typAtrybutB : $nazwaB));
		//echo $a['grupa'].' '.$cenyTrybA.' '.$b['grupa'].' '.$cenyTrybB.' '.$cmp1.' '.$cmp2.'<br>';
		if($cmp1 == $cmp2) return 0;
		if($cenyTrybA == $cenyTrybB) return ($cmp1 < $cmp2) ? -1 : 1;
		if($cenyTrybA != $cenyTrybB) return ($cmp1 > $cmp2) ? -1 : 1;
	}
	function cmpAll($a, $b)
	{
		//var_dump($a);die();
		$pozA = @$a['poz'];
		$pozB = @$b['poz'];
		if($pozA != $pozB) return ($pozA < $pozB) ? -1 : 1;
		$nazwaA = @$a['nazwa'];
		$nazwaB = @$b['nazwa'];
		if($nazwaA != $nazwaB) return strcmp($nazwaA, $nazwaB);
		$kolejA = @$a['kolejnosc'];
		$kolejB = @$b['kolejnosc'];
		if($kolejA != $kolejB) return ($kolejA < $kolejB) ? -1 : 1;
		$wartoscA = @$a['wartosc'];
		$wartoscB = @$b['wartosc'];
		$typAtrybutA = @strval($a['jedn_miary_typ']);
		$typAtrybutB = @strval($b['jedn_miary_typ']);
		if($typAtrybutA == "int") return intval($wartoscA) < intval($wartoscB) ? -1 : 1;
		if($typAtrybutA == "float") return floatval($wartoscA) < floatval($wartoscB) ? -1 : 1;
		return strcmp($wartoscA, $wartoscB);
	}
	
	function getAtrybutyAllForIds($ids = null)
	{
		if(!$this->common->isArray($ids, true)) return null;
		$where = 'p.id in ('.implode(',', $ids).') and atp.id > 0';// and atp.ceny';
		$select = $this->db->select()->distinct()
				->from(array('p' => 'Produkty'), array('id as id_prod'))
				->joinleft(array('atp' => 'Atrybutypowiazania'), 'p.id = atp.id_og',
				array('id as id_atp','wartosc','ceny','cenyTryb','cena_netto','cena_brutto'))
				->joinleft(array('at' => 'Atrybuty'), 'atp.id_at = at.id', array('id as id_at', 'nazwa as opcja'))
				->joinleft(array('atg' => 'Atrybutygrupy'),	'if(atp.id_gr>0,atp.id_gr,at.id_gr) = atg.id',
				array('id as id_gr', 'nazwa as atrybut', 'typatrybut', 'jedn_miary', 'jedn_miary_typ'))
				->where($where)->order(array('atg.poz asc', 'atg.nazwa asc', 'at.nazwa asc'));
		//echo $select;
		$result = $this->db->fetchAll($select);
		//var_dump($result);die();
		for ($i = 0; $i < count($result); $i++)
		{
			$res = $result[$i];
			if($res['typatrybut'] == 'multi') // || !empty($res['id_at']))
			{
				if(!isset($results[$res['id_prod']][$res['id_gr']]))
				{
					$resAll = $res;
					unset($resAll['id_at']);
					unset($resAll['opcja']);
					$results[$res['id_prod']][$res['id_gr']] = $resAll;
				}
				$resOne['id_at'] = $res['id_at'];
				$resOne['opcja'] = $res['opcja'];
				$resOne['ceny'] = $res['ceny'];
				$resOne['cenyTryb'] = $res['cenyTryb'];
				$resOne['netto'] = $res['cena_netto'];
				$resOne['brutto'] = $res['cena_brutto'];
				$results[$res['id_prod']][$res['id_gr']]['opcje'][$res['id_at']] = $resOne;
			}
			else $results[$res['id_prod']][$res['id_gr']] = $res;
		}
		if(@count($results) > 0)
        foreach($results as $id => $rows)
		{
			if(@count($rows) > 0)
			{
				uasort($rows, array($this, 'cmpAtr'));
				$results[$id] = array_reverse($rows);
			}
		}
		//var_dump($results);die();
		return @$results;
	}
	
	function getAtrybutOpcje($id = 0, $kat = 0, $sort = '')
	{
		$where = 'atg.id = '.$id.' and at.id_gr = '.$id.' and atp.id_gr = '.$id;
		if($id > 0) $where .= ' and id_kat = '.$kat;
		$select = $this->db->select()->distinct()
				->from(array('at' => 'Atrybuty'), array('id as id_atr', 'nazwa as opcja'))
				->join(array('atg' => 'Atrybutygrupy'),	'at.id_gr = atg.id',
				array('id as id_gr', 'nazwa as atrybut', 'typatrybut', 'jedn_miary', 'jedn_miary_typ'))
				->join(array('atp' => 'Atrybutypowiazania'), 'at.id = atp.id_at and at.id_gr = atp.id_gr', 
				array('id as id_atp','wartosc'))
				->join(array('p' => 'Produkty'), 'atp.id_og = p.id', array('id as id_prod', 'nazwa as produkt'))
				->joinleft(array('kp' => 'Katprod'), 'kp.id_prod = p.id and kp.typ = "kategorie"', array('id_kat_prod'))
				->where($where)->group('at.id')->order(array('at.nazwa asc'));
		//echo $select;
		$result = $this->db->fetchAll($select);
		if(!empty($sort)) return $this->common->sortByPole($result, $sort);
		else return $result;
	}
	
	function getAtrybutyImport($pole = 'id_gr', $pole1 = 'id_gr', $pole2 = 'nazwa')
	{
		$sql = $this->select()->where($pole.' <> 0')->order($pole);
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i][$pole1]][$result[$i][$pole2]] = $result[$i]->toArray();
		}
        unset($result);
		return @$results;
	}
    function getAtrybutyValuesForProdukt($atrybuty)
	{
		if(@count($atrybuty) > 0)
        foreach($atrybuty as $at => $atryb)
        {
            $value = '';
            if($atryb['typatrybut'] == 'lista') $value = $atryb['opcja'];
            if($atryb['typatrybut'] == 'tekst') $value = $atryb['wartosc'];
            if($atryb['typatrybut'] == 'multi')
            {
                $values = null;
                if(@count($atryb) > 0)
                foreach($atryb as $co => $val)
                {
                    if($co > 0 && is_array($val))
                    if(@!empty($val['opcja']))
                    $values[] = $val['opcja'];
                }
                $value = @implode(', ', $values);
            }
            $name = !empty($atryb['symbol']) ? $atryb['symbol'] : $atryb['atrybut'];
            $atrVals[$name] = $atrVals[$at] = $value;
        }
        return @$atrVals;
	}
    function getAtrybutyNazwaForProdukt($produkt, $atrcena = null)
	{
        $nazwa = $produkt['nazwa'];
        if($atrcena == null) $atrcena = $produkt;
		if(@count($produkt['atrybuty']) > 0)
        foreach($produkt['atrybuty'] as $at => $atryb)
        {
            //echo $at.' '.@intval($atrcena['atr'.$at]).'<br>';
            if($atryb['typatrybut'] != 'multi' || @intval($atrcena['atr'.$at]) > 0)
            if(@!empty($produkt['atrVals'][$at]) && strpos($nazwa, $produkt['atrVals'][$at]) === false)
            {
                $nazwa .= ' '.$produkt['atrVals'][$at];
                if(!empty($atryb['jedn_miary'])) $nazwa .= ''.$atryb['jedn_miary'];
            }
        }
        return $nazwa;
	}
}
?>