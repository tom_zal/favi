<?php
class Slider extends Zend_Db_Table
{
	public $id, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function wypisz($typ = 'slider')
	{
		$where = '(typ = "" or typ = "'.$typ.'") and lang = "'.$this->lang.'"';
		$sql = $this->select()->where($where)->order('kolejnosc');
        $result = $this->fetchAll($sql);
        return $result;
	}
	function wypiszAktywne($typ = 'slider')
	{
		$where = 'aktywny = 1 and (typ = "" or typ = "'.$typ.'") and lang = "'.$this->lang.'"';
        $sql = $this->select()->where($where)->order('kolejnosc');
        $result = $this->fetchAll($sql);
        return $result;
	}
	function wypiszJeden()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function usun()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	function maxKolejnosc($typ = 'slider')
	{
		$select = 'SELECT max(kolejnosc) FROM Slider where typ = "'.$typ.'" and lang = "'.$this->lang.'"';
		$result = $this->db->fetchAll($select);
		//echo $select;
		return isset($result[0]['max(kolejnosc)']) ? $result[0]['max(kolejnosc)'] : 0;
	}
	function ustawGlowne($id, $typ = 'slider')
	{
        $dane = array('glowny' => 'N');
        $where = 'typ = "'.$typ.'" and lang = "'.$this->lang.'"';
        $this->update($dane, $where);

        $dane = array('glowny' => 'T');
        $where = 'id = '.$id;
        $this->update($dane, $where);
    }
	function wypiszGlowne($typ = 'slider')
	{
		$result = $this->fetchRow('glowny = "T" and typ = "'.$typ.'" and lang = "'.$this->lang.'"');
		return $result;
	}
}
?>