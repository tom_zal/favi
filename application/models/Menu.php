<?php
class Menu extends Zend_Db_Table 
{
    public $lang, $menu, $link, $kats;
	public $arrowPath, $arrowThis, $arrowMain, $arrowLvls, $arrowMainPath, $arrowLvlsPath;

    public function __construct($module = 'admin')
	{
        parent::__construct();
		$this->menu = "";
        $this->menue = "";
		$this->module = $module;
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->link = $this->obConfig->baseUrl;
		$this->lang = $this->common->getJezyk($module);		
		
		$link = str_replace('//','/','/'.$this->link.'/');
		$this->arrowMenu = '<img src="'.$link.'public/images/strona/wiecej.gif" alt="&gt;">';
		$this->arrowPath = 0;//'<img src="'.$link.'public/images/strona/li.png" alt="&gt;">';
		$this->arrowThis = 0;//'<img src="'.$link.'public/images/strona/li.png" alt="&gt;">';
		$this->arrowMain = '<img src="'.$link.'public/images/strona/li.png" alt="&gt;">';
		$this->arrowLvls = '<img src="'.$link.'public/images/strona/li.png" alt="&gt;">';
		$this->arrowMainPath = '<img src="'.$link.'public/images/strona/li.png" alt="&gt;">';
		$this->arrowLvlsPath = '<img src="'.$link.'public/images/strona/li.png" alt="&gt;">';
		//$this->arrowPath = '<label>'.$this->arrowPath.'</label>';
		//$this->arrowThis = '<label>'.$this->arrowThis.'</label>';
		//$this->arrowMain = '<label>'.$this->arrowMain.'</label>';
		//$this->arrowLvls = '<label>'.$this->arrowLvls.'</label>';
		//$this->arrowMainPath = '<label>'.$this->arrowMainPath.'</label>';
		//$this->arrowLvlsPath = '<label>'.$this->arrowLvlsPath.'</label>';
    }

    function start() 
	{
        $where = 'rodzic = 0';
        $order = 'pozycja';
        $result = $this->fetchAll($where, $order);
        //print_r($glowne);
        return $result;
    }
	
	function wypiszMenu($id = 0, $ile = 9999, $numeruj = false)
	{
        $where = 'm.lang = "'.$this->lang.'" and m.wyswietl = "1"';
		if($id > 0) $where .= ' and m.rodzic = '.$id;
		$order = array('m.pozycja asc', 'm.data DESC');
		$sql = $this->db->select()->from(array('m' => 'Menu'), array('m.*', 'temat' => 'nazwa'));
		if($this->obConfig->podstronyGaleria)
		{
			$whereGal = 'm.id = g.wlasciciel and g.typ = "menu" and g.glowne = "T"';
			$whereGal.= ' and g.lang = "'.$this->lang.'" and g.wyswietl = "1"';
			$sql->joinleft(array('g' => 'Galeria'), $whereGal ,array('img'));
		}
		$sql->where($where)->order($order)->limit($ile);
		//echo $sql;die();
		$result = $this->db->fetchAll($sql);
		//var_dump($result);die();
		if(!$numeruj) return $result;
		for($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i]['id']] = $result[$i];
		}
        return @$results;
    }
	
	public function getAllWithDetails($rodzic = -1, $numeruj = true)
	{
		$sql = 'SELECT *, (SELECT count(*) FROM Menu where rodzic = m.id) as dzieci FROM Menu as m where m.wyswietl and '.($rodzic >= 0 ? 'm.rodzic = '.$rodzic : 1).' and m.lang = "'.$this->lang.'" and '.($this->module != 'admin' ? 'm.wyswietl' : 1).' order by m.pozycja asc';
		//echo $sql;
		//return;
        $result = $this->db->fetchAll($sql);
		if(!$numeruj) $results = $result;
		for ($i = 0; $i < count($result); $i++)
		{
			if($numeruj)
			$results[$result[$i]['id']] = $result[$i];
			$dzieci[$result[$i]['rodzic']][] = $result[$i];
		}
		$this->kats = @$results;
		$this->dzieci = @$dzieci;
    }

    function display_children_admin($parent, $level, $z=0) 
	{
        $where = 'rodzic="'.$parent.'" AND lang = "'.$this->lang.'"';
        $order = ' pozycja ASC';

        $result = $this->fetchAll($where, $order);
        $row = $result->toArray();

		if(!isset($this->z)) $this->z = 0;
        for($i=0; $i<count($row);$i++) 
		{
            if($row[$i]['rodzic'] == 0) 
			{
                //$this->menue.=''.str_repeat('',$level);
				$this->menue.='<tr id="rowPos_'.$row[$i]['id'].'" name="rodzic_0" class="white bord">';
				
				if($this->obConfig->pokazKategorieID)
				$this->menue.='<span style="display:none;"><input type="hidden" name="lp" value="'.$this->z++.'"><input type="hidden" name="path" value="0">'.$row[$i]['id'].'</span>';
				
				$this->menue.='<td><a class="link" href="'.$this->link.'/admin/menu/edytuj/id/'.$row[$i]['id'].'">
				<b class="cat_main">'.$row[$i]['nazwa'].'</b></a></td>';
				
				$this->menue.='<td align="center"><span class="ico_drag" style="display:inline;float:left;"></span></td>';
				if(false)
				{
					$this->menue.='<td align="center">';
					$this->menue.='<form name="pozycjaaa" action="" method="post" >
								<input type="hidden" name="id" value="'.$row[$i]['id'].'" />
								<input class="position_text" type="text" name="pozycja" value="'.$row[$i]['pozycja'].'"/>
								<div class="button2"><input title="Zapisz pozycje" type="submit" name="pozycjonuj" value="Zapisz" class="position" /></div>
								</form>';
					$this->menue.='</td>';
				}
				else
				{
					$this->menue.='<td align="center">';
					$this->menue.='<input class="position_text" type="text" id="pozycja'.$row[$i]['id'].'" name="pozycja" value="'.$row[$i]['pozycja'].'"/>';
					$this->menue.='<a title="Zmień pozycję" href="javascript:void(0);" onclick="window.location=\''.$this->link.'/admin/menu/index/pozid/'.$row[$i]['id'].'/pozycja/\'+$(\'#pozycja'.$row[$i]['id'].'\').val();"><img src="'.$this->link.'/public/images/admin/change.png" /></a>';
					$this->menue.='</td>';
				}
						
				if(true)
				$this->menue.='<td align="center"><a title="Zmień widoczność" href="'.$this->link.'/admin/menu/index/pokazid/'.$row[$i]['id'].'/widoczny/'.(@intval(!$row[$i]['wyswietl'])).'">
				<img src="'.$this->link.'/public/images/admin/ikony/'.(@intval($row[$i]['wyswietl']) ? 'ikona_ok' : 'ikona_usun').'.png" /></a></td>';
				else
				$this->menue.='<td align="center">&nbsp;</td>';
				
				$this->menue.='<td align="center"><a title="Zobacz" target="_blank" class="see_ico white" href="'.$this->link.'/'.$row[$i]['link'].'">Zobacz</a></td>';

				$this->menue.='<td align="center"><a href="'.$this->link.'/admin/menu/dodaj/id/'.$row[$i]['id'].'">
				<img src="'.$this->link.'/public/images/admin/ikony/ikona_dodaj.png" /> </a></td>';				
				
				$this->menue.='<td align="center"><a href="'.$this->link.'/admin/menu/edytuj/id/'.$row[$i]['id'].'">
				<img src="'.$this->link.'/public/images/admin/ikony/ikona_edytuj.png" /> </a></td>';
				
				if(true)
				$this->menue.='<td align="center"><a href="'.$this->link.'/admin/menu/index/del/'.$row[$i]['id'].'" 
				onclick="return confirm(\'Czy napewno chcesz usunąć wybraną pozycję ?\')">
				<img src="'.$this->link.'/public/images/admin/ikony/ikona_usun.png" /> </a></td>';
				else
				$this->menue.='<td align="center">&nbsp;</td>';
				
				$this->menue.='</tr>';
            }
			else 
			{
                $this->menue.='<tr id="rowPos_'.$row[$i]['id'].'" name="rodzic_'.$row[$i]['rodzic'].'" class="white bord">';
				
				if($this->obConfig->pokazKategorieID)
				$this->menue.='<span style="display:none;><input type="hidden" name="lp" value="'.$this->z++.'"><input type="hidden" name="path" value="'.$z.'">'.$row[$i]['id'].'</span>';
				
				$this->menue.='<td><a class="link level'.$level.'" href="'.$this->link.'/admin/menu/edytuj/id/'.$row[$i]['id'].'"><b>'.$row[$i]['nazwa'].'</b></a></td>';
				
				$this->menue.='<td align="center"><span class="ico_drag" style="display:inline;float:left;"></span></td>';
				if(false)
				{
					$this->menue.='<td align="center">';
					$this->menue.='<form name="pozycjaaa" action="" method="post" >
								<input type="hidden" name="id" value="'.$row[$i]['id'].'" />
								<input class="position_text" type="text" name="pozycja" value="'.$row[$i]['pozycja'].'"/>
								<div class="button2"><input title="Zapisz pozycje" type="submit" name="pozycjonuj" value="Zapisz" class="position" /></div>
								</form>';
					$this->menue.='</td>';
				}
				else
				{
					$this->menue.='<td align="center">';
					$this->menue.='<input class="position_text" type="text" id="pozycja'.$row[$i]['id'].'" name="pozycja" value="'.$row[$i]['pozycja'].'"/>';
					$this->menue.='<a title="Zmień pozycję" href="javascript:void(0);" onclick="window.location=\''.$this->link.'/admin/menu/index/pozid/'.$row[$i]['id'].'/pozycja/\'+$(\'#pozycja'.$row[$i]['id'].'\').val();"><img src="'.$this->link.'/public/images/admin/change.png" /></a>';
					$this->menue.='</td>';
				}
				
				if(true)
				$this->menue.='<td align="center"><a title="Zmień widoczność" href="'.$this->link.'/admin/menu/index/pokazid/'.$row[$i]['id'].'/widoczny/'.(@intval(!$row[$i]['wyswietl'])).'">
				<img src="'.$this->link.'/public/images/admin/ikony/'.(@intval($row[$i]['wyswietl']) ? 'ikona_ok' : 'ikona_usun').'.png" /></a></td>';
				else
				$this->menue.='<td align="center">&nbsp;</td>';

				$this->menue.='<td align="center"><a title="Zobacz" target="_blank" class="see_ico white" href="'.$this->link.'/'.$row[$i]['link'].'">Zobacz</a></td>';
				
				$this->menue.='<td align="center"><a href="'.$this->link.'/admin/menu/dodaj/id/'.$row[$i]['id'].'">
				<img src="'.$this->link.'/public/images/admin/ikony/ikona_dodaj.png" /> </a></td>';
				
				$this->menue.='<td align="center"><a href="'.$this->link.'/admin/menu/edytuj/id/'.$row[$i]['id'].'">
				<img src="'.$this->link.'/public/images/admin/ikony/ikona_edytuj.png" /> </a></td>';
				
				if(true)
				$this->menue.='<td align="center"><a href="'.$this->link.'/admin/menu/index/del/'.$row[$i]['id'].'" onclick="return confirm(\'Czy napewno chcesz usunąć wybraną pozycję ?\')">
				<img src="'.$this->link.'/public/images/admin/ikony/ikona_usun.png" /></a></td>';
				else
				$this->menue.='<td align="center">&nbsp;</td>';
				
				$this->menue.='</tr>';
            }
            $this->display_children_admin($row[$i]['id'], $level+1, $z++);
        }
    }
	
	function display_children_user_menu($parent, $level, $podswietl = 0, $z = 0, $it = false, $rodzic = 0, $lp = 0)
	{
		if(false)
		{
			$where = 'rodzic = "'.$parent.'" AND lang = "'.$this->lang.'" and wyswietl = "1"';
			$order = ' pozycja ASC';
			if($parent == 0) $order = ' pozycja DESC';
			$result = $this->fetchAll($where, $order);
			$row = $result->toArray();
			//echo $where;var_dump($row);
		}
		
        $row = @$this->dzieci[$parent];
		if(false && $parent == 0)
		$row = array_reverse($row);
		//var_dump($row);die();
		
		if($this->obConfig->menuOfertaMainEdycja)
		{
			$this->ustawienia = $this->common->getUstawienia();
			$this->ustaw = @unserialize($this->ustawienia['menu']);
		
			$this->IE6 = strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6') !== FALSE;
			$this->IE6height = false;
			$this->ieH = 0;
			$this->ieMH = 0;
			$this->ieLH = 0;
			
			$style = '';
			$styleActive = '';
			$styleAll = '';
			$hover = '';
			$out = '';
			$style .= "font-family:'".$this->ustaw['font']."';";
			$style .= "color:#".$this->ustaw['kolor'].";";
			$styleActive .= "color:#".$this->ustaw['kolor_hover'].";";
			$hover .= "$(this).css('color', '#".$this->ustaw['kolor_hover']."');";
			$out .= "$(this).css('color', '#".$this->ustaw['kolor']."');";
			if($parent > 0)
			{
				$styleAll .= 'min-height:'.$this->ustaw['podmenu_height'].'px;';
				$styleAll .= 'line-height:'.$this->ustaw['podmenu_height'].'px;';
			}
			$style .= $styleAll;
			$styleActive .= $styleAll;
			
			$styleUL = '';
			$styleULActive = '';
			$styleULAll = '';
			$hoverUL = '';
			$outUL = '';
			if($parent > 0)
			{
				$styleULAll .= 'background-color:#'.$this->ustaw['kolor_podmenu_tlo'].';';
				$styleULAll .= 'border-style:solid;';
				$styleULAll .= 'border-color:#'.$this->ustaw['podmenu_ramka_kolor'].';';
				$styleULAll .= 'border-width:'.$this->ustaw['podmenu_ramka_size'].'px;';
				if($level > 1 && $lp >= 0)
				$styleULAll .= 'margin-top:-'.$this->ustaw['podmenu_ramka_size'].'px;';
				if($level > 1 && $lp >= 0 && $this->IE6)
				$styleULAll .= 'margin-left:'.($this->ustaw['podmenu_ramka_size']*2).'px;';
			}
			$styleUL .= $styleULAll;
			$styleULActive .= $styleULAll;
			
			$styleLI = '';
			$styleLIActive = '';
			$styleLIAll = '';
			$hoverLI = '';
			$outLI = '';
			if($parent == 0)
			{
				//$styleLI .= 'background-color:#'.$this->ustaw['kolor_menu_tlo'].';';
				$styleLIActive .= 'background-color:#'.$this->ustaw['kolor_menu_tlo_hover'].';';
				$hoverLI .= "$(this).css('backgroundColor', '#".$this->ustaw['kolor_menu_tlo_hover']."');";
				$outLI .= "$(this).css('backgroundColor', 'transparent');";
			}
			else
			{
				if(!$this->IE6)
				{
					$styleLIAll .= 'min-height:'.$this->ustaw['podmenu_height'].'px;';
					$styleLIAll .= 'line-height:'.$this->ustaw['podmenu_height'].'px;';
				}
				if(true)
				{
					$styleLI .= 'background-color:#'.$this->ustaw['kolor_podmenu_tlo'].';';
					$styleLIActive .= 'background-color:#'.$this->ustaw['kolor_podmenu_tlo_hover'].';';
					$hoverLI .= "$(this).css('backgroundColor', '#".$this->ustaw['kolor_podmenu_tlo_hover']."');";
					$outLI .= "$(this).css('backgroundColor', '#".$this->ustaw['kolor_podmenu_tlo']."');";
				}
			}
			$styleLI .= $styleLIAll;
			$styleLIActive .= $styleLIAll;
		}
		
		if($parent > 0 && count($row) > 0)
		{
			$this->menue .= '<ul class="menuLinkiSubMenu ul'.$level.'"';
			if($this->obConfig->menuOfertaMainEdycja)
			$this->menue .= ' style="'.$styleUL.'" onmouseover="'.$hoverUL.'" onmouseout="'.$outUL.'"';
			$this->menue .= '>';
		}

        for($i = 0; $i < count($row); $i++)
		{
			$dzieci = ($parent > 0 && @count($this->dzieci[$row[$i]['id']]) > 0);			
			
			$link = @$row[$i]['link'];
			$linkPL = str_replace('-'.$this->lang, '', $link);
			$site = str_replace('-'.$this->lang, '', $this->site);
			$site = @$this->common->makeLink($this->common->lang(trim($site)));
			$active = $link == $site || $linkPL == $site;
			//if($link=='link'){var_dump($site);die();}
			$active = $active || (@is_array($this->ids) && array_search($row[$i]['id'], $this->ids) !== false);
			
			if($this->obConfig->menuOfertaMainEdycja)
			{
				$stylLI = $active ? $styleLIActive : $styleLI;
				$overLI = !$active ? $hoverLI : '';
				$autLI = !$active ? $outLI : '';
			}
			
			if(!$dzieci)
			{
				$this->menue .= '<li class="li'.$level.'"';
				if($this->obConfig->menuOfertaMainEdycja)
				$this->menue .= ' style="'.$stylLI.'" onmouseover="'.$overLI.'" onmouseout="'.$autLI.'"';
				$this->menue .= '>';
			}
			else
			{
				$this->menue .= '<li class="expand li'.$level.'"';
				if($this->obConfig->menuOfertaMainEdycja)
				$this->menue .= ' style="'.$stylLI.'" onmouseover="'.$overLI.'" onmouseout="'.$autLI.'"';
				$this->menue .= '>';
			}
			
			if($this->obConfig->menuOfertaMainEdycja)
			{
				$styl = $active ? $styleActive : $style;
				$over = !$active ? $hover : '';
				$aut = !$active ? $out : '';
			}
			
            if($row[$i]['rodzic'] == 0)
			{
				//$this->menue .= str_repeat('', $level);
				$it = ($podswietl == $row[$i]['id'] || $row[$i]['id'] == $rodzic);
                //if($it)
				{
					$this->menue .= '<a href="'.$this->link.''.$row[$i]['link'].'"';
					$this->menue .= ($active?' id="menuActive"':'').' class="'.($dzieci?'expand':'').'"';
					if($this->obConfig->menuOfertaMainEdycja)
					$this->menue .= ' style="'.$styl.'" onmouseover="'.$over.'" onmouseout="'.$aut.'"';
					$this->menue .= '>';
					$this->menue .= str_replace(' ', '&nbsp;', $row[$i]['nazwa']);
					//if($dzieci)
					$this->menue .= $this->arrowMenu;
					$this->menue .= '</a>';
                }
            }
			else
			{
                //if($podswietl == $row[$i]['id'])
				{
					$this->menue .= '<a href="'.$this->link.''.$row[$i]['link'].'"';
					$this->menue .= ($active?' id="menuActive"':'').' class="'.($dzieci?'expand':'').'"';
					if($this->obConfig->menuOfertaMainEdycja)
					$this->menue .= ' style="'.$styl.'" onmouseover="'.$over.'" onmouseout="'.$aut.'"';
					$this->menue .= '>';
					$this->menue .= str_replace(' ', '&nbsp;', $row[$i]['nazwa']);
					//if($dzieci)
					$this->menue .= $this->arrowMenu;
					$this->menue .= '</a>';
                }  
            }
            $this->display_children_user_menu($row[$i]['id'], $level+1, $podswietl, $z++, $it, $rodzic, $i);
			$this->menue .= '</li>';
        }
		if($parent > 0 && count($row) > 0) $this->menue .= '</ul>';
    }

	function display_children_user($parent, $poziom, $ids, $first = null, $szablon = false)
	{
        //if ($poziom != 0)
		$this->menu .= '<dl id="menu0">';
		
        if ($poziom != 0) $row = array($this->kats[$parent]);
        if ($poziom == 0) $row = $this->dzieci[$parent];

		$firstExist = true;
        for ($i = 0; $i < count($row); $i++) 
		{
			//var_dump($row);
			
			if($poziom == 0 || $first == null)
			{
				if($i == 0 || $firstExist) $first = ' lvl'.$poziom.'_first'; else $first = '';
				if($i == count($row) - 1) $first = ' lvl'.$poziom.'_last';
				$firstExist = false;
			}
			
			if($ids != null && is_array($ids) && array_search($row[$i]['id'], $ids) !== false)
			$first .= ' lvl'.$poziom.'_active lvl_active';
			if($ids != null && is_array($ids) && $row[$i]['id'] == $ids[count($ids)-1])
			$first .= ' lvl_this_active';
			//if($ids != null && is_array($ids) && $ids[0] != $row[$i]['id'] && $poziom == 0) continue;
			if($row[$i]['link']==$this->site || str_replace('-'.$this->lang,'',$row[$i]['link'])==$this->site)
			$first .= ' lvl_this_active';			
			//if($row[$i]['link']=='link'){var_dump($this->site);die();}
			
			$allegroID = 0;

            if($poziom == 0 && $parent == 0)
			{
				$this->menu.='<dt class="lvl'.$poziom.''.$first.'"';
				if($this->obConfig->rozwijajKategorie && $row[$i]['dzieci'] > 0)
				$this->menu.=' onclick="$(\'.dd_id'.$row[$i]['id'].'\').toggle(); $(\'.lvl0_active\').removeClass(\'lvl0_active\'); $(this).addClass(\'lvl0_active\');"';
				$this->menu.='>';
				
				$this->menu .= '<a href="'.$this->link.''.$row[$i]['link'].'';
				//if($this->obConfig->ofertaGaleria) $this->menu .= '/podstrona/'.$this->podstrona;
				$this->menu .= '"';
				if($this->obConfig->rozwijajKategorie && $row[$i]['dzieci'] > 0)
				$this->menu .= ' onclick="return false;" ';
	
				//$this->menu .= 'onmouseover="$(this).children(\'span\').css(\'visibility\',\'visible\');"  onmouseout="$(this).children(\'span\').css(\'visibility\',\'hidden\');"';
				
				$this->menu .= '>';
				if($this->arrowMainPath && $ids != null && is_array($ids) && array_search($row[$i]['id'], $ids) !== false)
				$this->menu .= $this->arrowMainPath;
				else 
				if($this->arrowThis && $ids != null && is_array($ids) && $row[$i]['id'] == $ids[count($ids)-1])
				$this->menu .= $this->arrowThis;
				else 
				if($this->arrowPath && $ids != null && is_array($ids) && array_search($row[$i]['id'], $ids) !== false)
				$this->menu .= $this->arrowPath;
				else
				$this->menu .= $this->arrowMain;
				$this->menu .= $row[$i]['nazwa'];
				//$this->menu .= '<span class="ile" style="visibility:hidden;">&nbsp;('.$row[$i]['produkty'].')</span>';
				$this->menu .= '</a></dt>';
			}
            else
			{
				$this->menu.='<dt class="lvl'.$poziom.''.$first.'"';
				if($this->obConfig->rozwijajKategorie && $row[$i]['dzieci'] > 0)
				$this->menu.=' onclick="$(\'.dd_id'.$row[$i]['id'].'\').toggle(); $(\'.lvl'.$poziom.'_active\').removeClass(\'lvl'.$poziom.'_active\'); $(this).addClass(\'lvl'.$poziom.'_active\');"';
				$this->menu.='>';
				
				$this->menu .= '<a href="'.$this->link.''.$row[$i]['link'].'';
				//if($this->obConfig->ofertaGaleria) $this->menu .= '/podstrona/'.$this->podstrona;
				$this->menu .= '"';
				if($this->obConfig->rozwijajKategorie && $row[$i]['dzieci'] > 0)
				$this->menu .= ' onclick="return false;" ';
	
				//$this->menu .= 'onmouseover="$(this).children(\'span\').css(\'visibility\',\'visible\');"  onmouseout="$(this).children(\'span\').css(\'visibility\',\'hidden\');"';
				
				$this->menu .= '>';
				//$this->menu .= '&gt; ';
				
				if($this->arrowLvlsPath && $ids != null && is_array($ids) && array_search($row[$i]['id'], $ids) !== false)
				$this->menu .= $this->arrowLvlsPath;
				else 
				if($this->arrowThis && $ids != null && is_array($ids) && $row[$i]['id'] == $ids[count($ids)-1])
				$this->menu .= $this->arrowThis;
				else 				
				if($this->arrowPath && $ids != null && is_array($ids) && array_search($row[$i]['id'], $ids) !== false)
				$this->menu .= $this->arrowPath;
				else
				$this->menu .= $this->arrowLvls;
				$this->menu .= $row[$i]['nazwa'];
				
				//$this->menu .= '<span class="ile" style="visibility:hidden;">&nbsp;('.$row[$i]['produkty'].') </span> ';
				$this->menu .= '</a></dt>';
			}
			
			//if($ids == null || ($ids[0] != $row[$i]['id'])) continue;
			$hide = ($ids == null || ($ids[0] != $row[$i]['id']));
			$hide = ($ids == null || (is_array($ids) && array_search($row[$i]['id'], $ids) === false));

            $res = @$this->dzieci[$row[$i]['id']];
			
			$firstExist = true;
			$active = ' lvl'.($poziom+1).'_active lvl_active';
			//if(!$hide)
            for ($j = 0; $j < count($res); $j++)
			{
				//if(isset($first) || $first == null)
				if($j == 0 || $firstExist) $first = ' lvl'.($poziom+1).'_first'; else $first = '';
				if($j == count($res) - 1) $first = ' lvl'.($poziom+1).'_last';
				if($ids != null && is_array($ids) && array_search($res[$j]['id'], $ids) !== false)
				{
					$first .= ' lvl'.($poziom+1).'_active lvl_active';
					$active = '';
				}
				if($ids != null && is_array($ids) && $res[$j]['id'] == $ids[count($ids)-1])
				$first .= ' lvl_this_active';
				$firstExist = false;
 	
                $this->menu.='<dd class="dd_lvl'.($poziom+1).' dd_id'.$row[$i]['id'].'"';
				if($hide) $this->menu.=' style="display:none;"';
				//if(empty($active)) $this->menu.=' style="display:none;"';
				$this->menu.='>';
				
				$allegroID = 0;
				
				if($res[$j]['dzieci'] == 0)
				{
					$this->menu .= '<dl class="dl_lvl'.($poziom+1).'"><dt class="lvl'.($poziom+1).''.$first.'">';
					
					$this->menu .= '<a href="'.$this->link.''.$res[$j]['link'].'';
					//if($this->obConfig->ofertaGaleria) $this->menu .= '/podstrona/'.$this->podstrona;
					$this->menu .= '"';
					
					//$this->menu .= 'onmouseover="$(this).children(\'span\').css(\'visibility\',\'visible\');"  onmouseout="$(this).children(\'span\').css(\'visibility\',\'hidden\');"';					
					
					$this->menu .= '>';
					//$this->menu .= '&gt; ';
					
					//if($poziom <= 1)
					if($this->arrowLvlsPath && $ids!=null && is_array($ids) && array_search($res[$j]['id'],$ids)!==false)
					$this->menu .= $this->arrowLvlsPath;
					else 
					if($this->arrowThis && $ids != null && is_array($ids) && $res[$j]['id'] == $ids[count($ids)-1])
					$this->menu .= $this->arrowThis;
					else
					if($this->arrowPath && $ids != null && is_array($ids) && array_search($res[$j]['id'], $ids) !== false)
					$this->menu .= $this->arrowPath;
					else
					$this->menu .= $this->arrowLvls;
					
					$this->menu .= $res[$j]['nazwa'];
					//$this->menu .= '<span class="ile"';
					//if(!$szablon) $this->menu .= ' style="visibility:hidden;"';
					//$this->menu .= '>&nbsp;('.$res[$j]['produkty'].') </span> ';
					$this->menu .= '</a></dt></dl>';
				}
				else
				{
                    $this->display_children_user($res[$j]['id'], $poziom + 1, $ids, $first);
                }
				
				$this->menu.='</dd>';
            }
        }
        //if ($poziom != 0) 
		$this->menu.='</dl>';
    }
	
	function changePositionAjax($position, $id_cat)
	{
        $dane['pozycja'] = $position;
        $where = 'id = '.$id_cat;
        $this->update($dane, $where);
    }
	
    function changePozycja($id, $pozycja) 
	{
        $update = array('pozycja' => $pozycja);
        $where = 'id = '.$id;
        $this->update($update, $where);
    }

    function kasuj($id) 
	{
        $where = 'id = '.$id;
		$kasuj = $this->showWybranaKategoria($id);
		$route = new Routers();
		$route->usunPodstrony($kasuj->link);
        $this->delete($where);
        $s_where = 'rodzic='.$id;
        $result = $this->fetchAll($s_where);
        $rows = $result->toArray();

        for($i=0; $i < count($rows); $i++) 
		{
            $id = $rows[$i]['id'];
            if($id)
			{
				$kasuj = $this->showWybranaKategoria($id);
                $this->kasuj($id);
				$route = new Routers();
				$route->usunPodstrony($kasuj->link);
            }
        }
    }

    function dodaj_nowa($rodzic, $dane) 
	{
        $where = 'rodzic = '.$rodzic;
        $order = 'pozycja desc';
        $limit = 1;
        $result = $this->fetchAll($where, $order, $limit);
        $arr = $result->toArray();
        $data = array('rodzic' => $rodzic, 'nazwa' => $dane['nazwa'], 'tekst' => $dane['tekst'],
									'lang' => $this->lang, 'pozycja' => (@$arr[0]['pozycja']+1));
		$route = new Routers();
		$nowy = $route->dodaj($data['nazwa'], '', 'default', 'podstrony' ,'menu');
		if($nowy['id'] > 0) 
		{
			$data['link'] = $nowy['link'];
			$this->insert($data);
		}
		else if($this->obConfig->jezyki)
		{
			$nowy = $route->dodaj($data['nazwa'].'-'.$this->lang, '', 'default', 'podstrony' ,'menu');
			if($nowy['id'] > 0) 
			{
				$data['link'] = $nowy['link'];
				$this->insert($data);
			}
		}
		return $nowy;
    }
	
    function showWybranaKategoria($id) 
	{
        $where = 'id = '.$id;
        $result = $this->fetchRow($where);
		if(@!empty($result->tekst))	$result->tekst = $this->common->fixTekst($result->tekst);
		if(@!empty($result->short))	$result->short = $this->common->fixTekst($result->short);
        return $result;
    }
	function showWybranaKategoriaLink($link) 
	{
        $where = 'link = "'.$link.'"';
        $result = $this->fetchRow($where);
		if(@!empty($result->tekst))	$result->tekst = $this->common->fixTekst($result->tekst);
		if(@!empty($result->short))	$result->short = $this->common->fixTekst($result->short);
        return $result;
    }
	function showMainPageKategoriaLink() 
	{
        $where = 'disable = "1" and lang = "'.$this->lang.'"';
        $result = $this->fetchRow($where);
        return $result;
    }

    public function edytuj($id, $dane, $img=null) 
	{
        $where = 'id = '.$id;
        $this->update($dane, $where);
    }
	function setLink($dane, $link, $lang = null)
	{
		if(empty($link)) return;
		$where = 'link = "'.$link.'"';
		$this->update($dane, $where);
		if(empty($lang)) return;
		$dane['link'] .= '-'.$lang;
		$where = 'link = "'.$link.'-'.$lang.'"';
		//var_dump($where); die();
		$this->update($dane, $where);
	}

    function get_path($id) 
	{
        $where = 'id = '.$id;
        $result = $this->fetchAll($where);
        //var_dump($where);
        $path = array();
        if(@$result[0]->rodzic!='') 
		{
            $path[] = array('id' => $result[0]->id, 'rodzic' => $result[0]->rodzic, 'nazwa' => $result[0]->nazwa);
            $path = array_merge($this->get_path($result[0]->rodzic), $path);
        }
        return $path;
    }
}
?>