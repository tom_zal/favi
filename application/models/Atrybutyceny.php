<?php
class Atrybutyceny extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

	function _save($data)
	{
		$this->insert($data);
		return $this->getAdapter()->lastInsertId();
	}
	function _update($data)
	{
		$this->update($data, 'id = '.$this->id);
	}
    function zmien($data)
	{
		$this->update($data, 'id = '.$this->id);
	}
	function addAttr($atr = 0)
	{
		//IF NOT EXISTS((SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE() AND COLUMN_NAME='atr0' AND TABLE_NAME='Atrybutyceny'))
		$table = new Zend_Db_Table($this->_name);
		$info = $table->info();
		if(@!in_array($atr, $info['cols']))
		try
		{
			$sql = 'ALTER TABLE `Atrybutyceny` ADD `'.$atr.'` INT(11) NOT NULL';
			$this->db->query($sql);
			$this->common->cacheRemove(null, $this->_name);
			//$table = new Zend_Db_Table($this->_name);
			//$info = $table->info();
			//var_dump($info);die();
		}
		catch(Exception $e){}
	}
	
	function _delete()
	{
		$this->delete('id = '.$this->id);
	}
	function _deleteGroup()
	{
		$this->delete('id_prod = '.$this->id);
	}
	function _deleteAttr($atr)
	{
		$this->delete('atr'.$atr.' = '.$this->id);
	}
	
	function usunProdukt($id)
	{
		$this->delete('id_prod = '.$id);
	}
	function usunProduktyIds($ids)
	{
		$result = $this->delete('id_prod in ('.$ids.')');
	}
    function wypiszJeden()
	{
		$select = $this->select()->where('id = '.$this->id);
		$result = $this->fetchAll($select);
		return $result;
	}
	function getRow()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}	
	function getRows()
	{
		$result = $this->fetchAll('1', 'id_prod ASC');
		return $result;
	}
	function getRowsUnique()
	{
		$result = $this->fetchAll('id_prod = '.$this->id, 'id ASC');
		return $result;
	}
	function getRowsWhere($where = '1', $atr = 'atr0', $ord = 'atr0')
	{
		$select = $this->db->select()->distinct()->from(array('ac' => $this->_name), array($atr, 'dostepnosc'));
		if($ord == 'nazwa') $select->joinleft(array('at' => 'Atrybuty'), 'ac.'.$atr.' = at.id', array(''));
		$select->where($where)->order($ord);
		$result = $this->db->fetchAll($select);
		//echo $select; //$result = $this->common->sortByPole($result, $atr);
		return $result;
	}
	function getAtrybutyCeny($pole = 'id_ext', $ids = null)
	{
		//if(empty($ids) || !is_array($ids) || count($ids) == 0) return null;
		$sql = $this->select()->where($pole.' <> ""')->order($pole);//and id_prod in ('.implode(',',$ids).')
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i][$pole]] = $result[$i]->toArray();
		}
        unset($result);
		return @$results;
	}
    function getAtrybutyCenyForExport($kat = 0, $atr = 0)
	{
		$select = $this->db->select()->from(array('ac'=>$this->_name),
                array('id_prod','id_ext','dostepnosc','cena_netto_hurt','cena_promocji_n_hurt','atr'.$atr));
		$select->join(array('p' => 'Produkty'), 'ac.id_prod = p.id', array('nazwa','oznaczenie','promocja','tekst'));
        $select->join(array('kp' => 'Katprod'), 'kp.id_prod = p.id', array(''));
		$select->where('kp.id_kat = '.$kat.' and ac.id_ext <> "" && ac.cena_netto_hurt > 0 and atr'.$atr.' > 0')->order('p.nazwa asc');
		$result = $this->db->fetchAll($select);
		//echo $select; //$result = $this->common->sortByPole($result, $atr);
		return $result;
	}
    function getAtrybutyCenyByProdukt($pole = 'id')
	{
		$sql = $this->select()->where($pole.' <> ""')->order($pole);
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i]['id_prod']][$result[$i][$pole]] = $result[$i]->toArray();
		}
        unset($result);
		return @$results;
	}
    function wypiszAukcje($null = false)
	{
		$where = '1';
		if($null)
		$where = 'a.wystawiono is not null and (DATE_ADD(a.wystawiono,INTERVAL a.na_ile DAY)-NOW()) > 0';
		$select = $this->db->select()->from(array('ac' => 'Atrybutyceny'), 
			array('id as rp.id', 'id_prod', 'dostepnosc'))
			->joinleft(array('a' => 'Allegroaukcje'), 'a.id_atr = ac.id', 
					array('id as a.id', 'nr', 'sztuk_pozostalo', 'na_ile', 'sztuk_wystawiono', 'wystawiono'))
			->where('ac.id_prod = '.$this->id.' and '.$where)->order(array('ac.id_ext'));
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
    function getAtrybutyValuesForAtrCena($atrCena, $prodAtrs, $atrybutyAll = null)
	{
        $atrybuty = new Atrybuty();
        if($atrybutyAll == null)
        $atrybutyAll = $atrybuty->getAtrybutyImport('id_gr', 'id_gr', 'id');
        $atrybutyCenyTable = new Zend_Db_Table('Atrybutyceny');
        $atrybutyCenyTableInfo = $atrybutyCenyTable->info();
        if(@count($atrybutyCenyTableInfo['cols']) > 0)
        foreach($atrybutyCenyTableInfo['cols'] as $co)
        {
            if(strpos($co, 'atr') === 0)
            {
                $atr = intval(str_replace('atr', '', $co));
                if($atr > 0)
                {
                    $val = @intval($atrCena[$co]);
                    $atryb = @$prodAtrs[$atr];
                    //$grName = @$atrybutyGrupyAll[$atr]['nazwa'];
                    $atrName = @$atrybutyAll[$atr][$val]['nazwa'];
                    if(!empty($atrName))
                    {
                        $name = !empty($atryb['symbol']) ? $atryb['symbol'] : $atryb['atrybut'];
                        $atrVals[$name] = $atrVals[$atr] = $atrName;
                    }
                }
            }
        }
        return @$atrVals;
	}
    function getAtrybutyNazwaForAtrCena($atrCena, $atrybutyAll = null)
	{
        $nazwa = '';
        $atrybuty = new Atrybuty();
        if($atrybutyAll == null)
        $atrybutyAll = $atrybuty->getAtrybutyImport('id_gr', 'id_gr', 'id');
        $atrybutyCenyTable = new Zend_Db_Table('Atrybutyceny');
        $atrybutyCenyTableInfo = $atrybutyCenyTable->info();
        if(@count($atrybutyCenyTableInfo['cols']) > 0)
        foreach($atrybutyCenyTableInfo['cols'] as $co)
        {
            if(strpos($co, 'atr') === 0)
            {
                $atr = intval(str_replace('atr', '', $co));
                if($atr > 0)
                {
                    $val = @intval($atrCena[$co]);
                    $atrName = @$atrybutyAll[$atr][$val]['nazwa'];
                    if(!empty($atrName))
                    {
                        $nazwa .= ' '.$atrName;
                        //if(!empty($atryb['jedn_miary'])) $nazwa .= ''.$atryb['jedn_miary'];
                    }
                }
            }
        }
        return $nazwa;
	}
}    
?>