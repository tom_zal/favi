<?php
class Ebaykategorie extends Zend_Db_Table
{
    protected $_name = 'ebat_cat';
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

    function dodaj($dane)
	{
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }

    function edytuj($dane)
	{
        $where = 'id = "'.$this->id.'"';
        $this->update($dane, $where);
    }

    function usun()
	{
        $where = 'id = "'.$this->id.'"';
        $this->delete($where);
    }
	
	function getOneByID()
	{
        $where = 'id = "'.$this->id.'"';
        $result = $this->fetchRow($where);
        return $result->toArray();
    }
	
	function getOneByCatID($id)
	{
        $where = '`cat_id` = "'.$id.'"';
        $result = $this->fetchRow($where);
        return $result->toArray();
    }
	
	function getAll()
	{
		$sql = $this->select()->order('parent_cat_id', 'cat_name');
        $result = $this->fetchAll($sql);
        $res = $result->toArray();
        return $res;
    }
	
	function getAllByName($name)
	{
		$where = '`cat_name` = "'.$name.'"';
        $result = $this->fetchAll($where);
        $res = $result->toArray();
        return $res;
    }
	
	function getChildren($id)
	{
		$sql = $this->select()->where('`parent_cat_id` = '.$id)->order('cat_name');
        $result = $this->fetchAll($sql);
        $res = $result->toArray();
        return $res;
    }
}
?>