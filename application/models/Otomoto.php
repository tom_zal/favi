<?php

@define('OTOMOTO_BLAD_NIEOKRESLONY', -1);
@define('OTOMOTO_BLAD_TYPU', -2);
@define('OTOMOTO_LIMIT_ZDJEC', -3);
@define('MAKS_LICZBA_ZDJ_OTOMOTO', 10);

class Otomoto extends Zend_Db_Table
{
    public $ID, $login, $choose;
    protected $_name = 'Otomoto';
	public $link, $obConfig, $db;
	
	public function __construct($logon = true, $module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		
		$this->ID = 1;
		$this->otoMotoDane = $this->klient();
		$this->webapi = $this->otoMotoDane['klucz'];
		$this->parametry = array
		(
			"dealer-login" => $this->otoMotoDane['login'],
			"dealer-password" => $this->otoMotoDane['haslo'],
			"country-code" => "1",
			"webapi-key" => $this->webapi
		);
		$this->adres_otomoto = 'http://beta.otomoto.pl/webapi/server.php?wsdl';
		//$this->adres_otomoto = 'http://otomoto.pl/webapi/server.php?wsdl';
		$this->klientOtomoto = new SoapClient($this->adres_otomoto);
		if($logon)
		try
		{
			$this->kontoOtomoto = $this->klientOtomoto->__soapCall('doDealerLogin', $this->parametry);
			$_SESSION['sidotomoto'] = $this->kontoOtomoto['session-id'];
			$this->serwisNiedostepny = false;		
		}
		catch(SoapFault $e)
		{
			$this->serwisNiedostepny = true;
		}
		else $this->serwisNiedostepny = true;
		$_SESSION['adres_otomoto'] = $this->adres_otomoto;
    }
	
	function getOtoMotoPola()
	{
        $otoMotoPola[] = 'pojazd_id_ogloszenia';
		$otoMotoPola[] = 'pojazd_typ';
		$otoMotoPola[] = 'pojazd_marka_id';
		$otoMotoPola[] = 'pojazd_marka_name';
		$otoMotoPola[] = 'pojazd_marka_wlasne';
		$otoMotoPola[] = 'pojazd_model_id';
		$otoMotoPola[] = 'pojazd_model_name';
		$otoMotoPola[] = 'pojazd_model_nazwa_wl';
		$otoMotoPola[] = 'pojazd_dod_opis_model';
		$otoMotoPola[] = 'pojazd_wersja_id';
		$otoMotoPola[] = 'pojazd_wersja_name';
		$otoMotoPola[] = 'pojazd_wersja_wlasne';
		$otoMotoPola[] = 'pojazd_typ_nadwozia_key';
		$otoMotoPola[] = 'pojazd_typ_nadwozia_name';
		$otoMotoPola[] = 'pojazd_kategoria_allegro_id';
		$otoMotoPola[] = 'pojazd_kategoria_allegro_name';
		$otoMotoPola[] = 'pojazd_kategoria_allegro_II_id';
		$otoMotoPola[] = 'pojazd_kategoria_allegro_II_name';
		$otoMotoPola[] = 'pojazd_kategoria_allegro_III_id';
		$otoMotoPola[] = 'pojazd_kategoria_allegro_III_name';
		$otoMotoPola[] = 'pojazd_kategoria_allegro_IV_id';
		$otoMotoPola[] = 'pojazd_kategoria_allegro_IV_name';
		$otoMotoPola[] = 'pojazd_num_wybr_kat_allegro';
		$otoMotoPola[] = 'pojazd_cena';
		$otoMotoPola[] = 'pojazd_waluta';
		$otoMotoPola[] = 'pojazd_typ_ceny';
		$otoMotoPola[] = 'pojazd_do_negocjacji';
		$otoMotoPola[] = 'pojazd_wystawiam_f_vat';
		$otoMotoPola[] = 'pojazd_w_leasingu';
		$otoMotoPola[] = 'pojazd_kwota_odstepnego';
		$otoMotoPola[] = 'pojazd_waluta_odstepnego';
		$otoMotoPola[] = 'pojazd_wysokosc_raty';
		$otoMotoPola[] = 'pojazd_ilosc_pozos_rat';
		$otoMotoPola[] = 'pojazd_wartosc_wykupu';
		$otoMotoPola[] = 'pojazd_mozliwosc_kredytowania';
		$otoMotoPola[] = 'pojazd_rok_produkcji';
		$otoMotoPola[] = 'pojazd_przeglad_rok';
		$otoMotoPola[] = 'pojazd_przeglad_miesiac';
		$otoMotoPola[] = 'pojazd_ubezp_oc_rok';
		$otoMotoPola[] = 'pojazd_ubezp_oc_miesiac';
		$otoMotoPola[] = 'pojazd_pierw_rej_rok';
		$otoMotoPola[] = 'pojazd_pierw_rej_miesiac';
		$otoMotoPola[] = 'pojazd_przebieg';
		$otoMotoPola[] = 'pojazd_moc';
		$otoMotoPola[] = 'pojazd_jedn_mocy';
		$otoMotoPola[] = 'pojazd_jedn_mocy_name';
		$otoMotoPola[] = 'pojazd_rodzaj_paliwa_key';
		$otoMotoPola[] = 'pojazd_rodzaj_paliwa_name';
		$otoMotoPola[] = 'pojazd_skrzynia_biegow_key';
		$otoMotoPola[] = 'pojazd_skrzynia_biegow_name';
		$otoMotoPola[] = 'pojazd_kolor_key';
		$otoMotoPola[] = 'pojazd_kolor_name';
		$otoMotoPola[] = 'pojazd_metalik';
		$otoMotoPola[] = 'pojazd_liczba_drzwi';
		$otoMotoPola[] = 'pojazd_poj_skokowa';
		$otoMotoPola[] = 'pojazd_uszkodzony';
		$otoMotoPola[] = 'pojazd_stan_techniczny';
		$otoMotoPola[] = 'pojazd_stan_techniczny_name';
		$otoMotoPola[] = 'pojazd_email_wyst';
		$otoMotoPola[] = 'pojazd_stat_poj_sprow_id';
		$otoMotoPola[] = 'pojazd_stat_poj_sprow_name';
		$otoMotoPola[] = 'pojazd_publ_w_serw_zagr';
		$otoMotoPola[] = 'pojazd_publ_w_tablica_pl';
		$otoMotoPola[] = 'pojazd_kraj_pochodzenia_id';
		$otoMotoPola[] = 'pojazd_kraj_pochodzenia_name';
		$otoMotoPola[] = 'pojazd_kraj_akt_rej_id';
		$otoMotoPola[] = 'pojazd_kraj_akt_rej_name';
		$otoMotoPola[] = 'pojazd_vin';
		$otoMotoPola[] = 'pojazd_dod_info_wlasne';
		$otoMotoPola[] = 'pojazd_dod_wyposaz_wlasne';
		$otoMotoPola[] = 'pojazd_ladowalnosc';
		$otoMotoPola[] = 'pojazd_liczba_osi';
		$otoMotoPola[] = 'pojazd_rodzaj_napedu_key';
		$otoMotoPola[] = 'pojazd_rodzaj_napedu_name';
        return $otoMotoPola;
    }

    function dodaj($dane)
	{
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }
    function edytuj($dane)
	{
        $where = 'id = "'.$this->ID.'"';
        $this->update($dane, $where);
    }
    function usun()
	{
        $where = 'id = "'.$this->ID.'"';
        $this->delete($where);
    }

    function klient()
	{
        $where = 'id = "'.$this->ID.'"';
        $result = $this->fetchRow($where);
        //$res = $result->toArray();
        return $result;
    }
    function listaKlient()
	{
        $result = $this->fetchAll();
        $res = $result->toArray();
        return $res;
    }
	
	public function sortujTabObiektow($dane_z_otomoto_temp)
	{
		$tab_temp = array();
		$liczba_pozycji = count($dane_z_otomoto_temp);
		for($i=0; $i<$liczba_pozycji; $i++)
		{
			$tab_temp[$dane_z_otomoto_temp[$i]->id] = $dane_z_otomoto_temp[$i]->name;
		}
		asort($tab_temp, SORT_STRING);
		unset($dane_z_otomoto_temp);
		$i=0;
		foreach($tab_temp as $klucz => $wartosc)
		{
			$key_name = new KeyName();
			$key_name->id = $klucz;
			$key_name->name = $wartosc;
			$dane_z_otomoto_temp[$i] = $key_name;
			$i++;
		}
		return $dane_z_otomoto_temp;
	}
	
	public function unprepare_message($message)
	{
	   $unhtml_specialchars_match = array('#&(?!(\#[0-9]+;))#', '#<#', '#>#', '#"#');
	   $unhtml_specialchars_replace = array('&amp;', '&lt;', '&gt;', '&quot;');
	
	   return stripslashes(preg_replace($unhtml_specialchars_match, $unhtml_specialchars_replace, $message));
	}

    public function formatuj_bledy_otomoto($odpowiedz)
	{
		$tab_bledy = @$odpowiedz['error-list'];
		$bledy = '';
		$i = 1;
		$liczba_bledow = @count($tab_bledy);
		if($liczba_bledow > 0)
		foreach($tab_bledy as $blad)
		{
			if($blad->desc != null)
			{
				$bledy .= $i.'. '.$blad->desc.'<br />';
			}
			else
			{
				$bledy .= $i.'. '.$blad->key.'<br />';
			}
			$i++;
		}
		return $bledy;
	}
	
	public function przygotuj_listy_wyboru(&$data, $bez_mod_i_wer = false)
	{
		$typ_pojazdu = strtoupper($data['pojazd_typ']);

		if(@$_SESSION['marki_lista_'.$typ_pojazdu] == null)
		{
			$parametry = array
			(
				'type' => $typ_pojazdu,
				'webapi-key' => $this->webapi
			);
			$dane_z_otomoto_temp = $this->klientOtomoto->__soapCall('getMakes', $parametry);
			
			$temp = '';
			$liczba_pozycji = count($dane_z_otomoto_temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp .= $dane_z_otomoto_temp[$i]->id.';'.$dane_z_otomoto_temp[$i]->name.'|';
			}
			$temp = substr($temp, 0, strlen($temp)-1);
			$_SESSION['marki_lista_'.$typ_pojazdu] = $temp;
		}
		if(@$_SESSION['marki_lista_'.$typ_pojazdu] != null)
		{
			$temp = explode('|',$_SESSION['marki_lista_'.$typ_pojazdu]);
			$liczba_pozycji = count($temp);	
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp_pozycja = explode(';', $temp[$i]);
				$key_name = new KeyName();
				$key_name->id = $temp_pozycja[0];
				$key_name->name = $temp_pozycja[1];
				$dane_z_otomoto['marki'][$i] = $key_name;
			}
		}

		// gdy chcemy pobrac rowniez modele i wersje (tak dla edycji ogloszenia, nie dla dodawania ogloszenia)
		if($bez_mod_i_wer == false)
		{
			if($typ_pojazdu != 'TRUCK')
			{
				if(($data['pojazd_marka_id'] != '-1' /*&& $_SESSION['pojazd_marka_id_pop'] != $data['pojazd_marka_id']*/))
				{
					$parametry = array
					(
						'make-id' => $data['pojazd_marka_id'],
						'webapi-key' => $this->webapi,
						'country-code' => '1',
						'type' => $typ_pojazdu
					);
					$dane_z_otomoto_temp = $this->klientOtomoto->__soapCall('getModels', $parametry);
					$dane_z_otomoto_temp = $this->sortujTabObiektow($dane_z_otomoto_temp);

					$temp = '';
					$liczba_pozycji = count($dane_z_otomoto_temp);
					for($i=0; $i<$liczba_pozycji; $i++)
					{
						$temp .= $dane_z_otomoto_temp[$i]->id.';'.$dane_z_otomoto_temp[$i]->name.'|';
					}
					$temp = substr($temp, 0, strlen($temp)-1);
					$_SESSION['modele_lista'] = $temp;

					$_SESSION['pojazd_marka_id_pop'] = $data['pojazd_marka_id'];
				}
				if($_SESSION['modele_lista'] != null)
				{
					$temp = explode('|',$_SESSION['modele_lista']);
					$liczba_pozycji = count($temp);
					for($i=0; $i<$liczba_pozycji; $i++)
					{
						$temp_pozycja = explode(';', $temp[$i]);
						$key_name = new KeyName();
						$key_name->id = $temp_pozycja[0];
						$key_name->name = $temp_pozycja[1];
						$dane_z_otomoto['modele'][$i] = $key_name;
					}
				}
				$_SESSION['pojazd_marka_id_pop'] = $data['pojazd_marka_id']; // poprzednia marka
			}

			if($typ_pojazdu == 'CAR')
			{
				if(($data['pojazd_model_id'] != '-1' && @$_SESSION['pojazd_model_id_pop'] != $data['pojazd_model_id']))
				{
					$parametry = array
					(
						'make-id' => $data['pojazd_marka_id'],
						'model-id' => $data['pojazd_model_id'],
						'webapi-key' => $this->webapi,
						'country-code' => '1',
						'type' => $typ_pojazdu
					);
					$dane_z_otomoto_temp = $this->klientOtomoto->__soapCall('getVersions', $parametry);
		
					$temp = '';
					$liczba_pozycji = count($dane_z_otomoto_temp);
					for($i=0; $i<$liczba_pozycji; $i++)
					{
						$temp .= $dane_z_otomoto_temp[$i]->id.';'.$dane_z_otomoto_temp[$i]->name.'|';
					}
					$temp = substr($temp, 0, strlen($temp)-1);
					$_SESSION['wersje_lista_'.$typ_pojazdu] = $temp;				
					$_SESSION['pojazd_model_id_pop'] = $data['pojazd_model_id'];
				}
				if($_SESSION['wersje_lista_'.$typ_pojazdu] != null)
				{
					$temp = explode('|',$_SESSION['wersje_lista_'.$typ_pojazdu]);
					$liczba_pozycji = count($temp);
					for($i=0; $i<$liczba_pozycji; $i++)
					{
						$temp_pozycja = explode(';', $temp[$i]);
						$key_name = new KeyName();
						$key_name->id = $temp_pozycja[0];
						$key_name->name = $temp_pozycja[1];
						$dane_z_otomoto['wersje'][$i] = $key_name;
					}
				}
				$_SESSION['pojazd_model_id_pop'] = $data['pojazd_model_id']; // poprzedni model
			}
		}

		if(@$_SESSION['typ_nadwozia_lista_'.$typ_pojazdu] == null)
		{
			$parametry = array
			(
				'type' => $typ_pojazdu,
				'webapi-key' => $this->webapi
			);
			$dane_z_otomoto_temp = $this->klientOtomoto->__soapCall('getVehicleCategories', $parametry);
			
			$temp = '';
			$liczba_pozycji = count($dane_z_otomoto_temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp .= $dane_z_otomoto_temp[$i]->key.';'.$dane_z_otomoto_temp[$i]->name.'|';
			}
			$temp = substr($temp, 0, strlen($temp)-1);
			$_SESSION['typ_nadwozia_lista_'.$typ_pojazdu] = $temp;
		}
		if(@$_SESSION['typ_nadwozia_lista_'.$typ_pojazdu] != null)
		{
			$temp = explode('|',$_SESSION['typ_nadwozia_lista_'.$typ_pojazdu]);
			$liczba_pozycji = count($temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp_pozycja = explode(';', $temp[$i]);
				$key_name = new KeyName();
				$key_name->key = $temp_pozycja[0];
				$key_name->name = $temp_pozycja[1];
				$dane_z_otomoto['typ_nadwozia'][$i] = $key_name;
			}
		}

		if(@$_SESSION['dod_info_lista_'.$typ_pojazdu] == null)
		{
			$parametry = array
			(
				'type' => $typ_pojazdu,
				'webapi-key' => $this->webapi
			);
			$dane_z_otomoto_temp = $this->klientOtomoto->__soapCall('getVehicleFeatures', $parametry);

			$temp = '';
			$liczba_pozycji = count($dane_z_otomoto_temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp .= $dane_z_otomoto_temp[$i]->key.';'.$dane_z_otomoto_temp[$i]->name.'|';
			}
			$temp = substr($temp, 0, strlen($temp)-1);
			$_SESSION['dod_info_lista_'.$typ_pojazdu] = $temp;
		}
		if(@$_SESSION['dod_info_lista_'.$typ_pojazdu] != null)
		{
			$temp = explode('|',$_SESSION['dod_info_lista_'.$typ_pojazdu]);
			$liczba_pozycji = count($temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp_pozycja = explode(';', $temp[$i]);
				$key_name = new KeyName();
				$key_name->key = $temp_pozycja[0];
				$key_name->name = $temp_pozycja[1];
				$dane_z_otomoto['dod_info'][$i] = $key_name;
			}
		}

		if(@$_SESSION['dod_wyposaz_lista_'.$typ_pojazdu] == null)
		{
			$parametry = array
			(
				'type' => $typ_pojazdu,
				'webapi-key' => $this->webapi
			);
			$dane_z_otomoto_temp = $this->klientOtomoto->__soapCall('getVehicleExtras', $parametry);;
			
			$temp = '';
			$liczba_pozycji = count($dane_z_otomoto_temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp .= $dane_z_otomoto_temp[$i]->key.';'.$dane_z_otomoto_temp[$i]->name.'|';
			}
			$temp = substr($temp, 0, strlen($temp)-1);
			$_SESSION['dod_wyposaz_lista_'.$typ_pojazdu] = $temp;
		}
		if(@$_SESSION['dod_wyposaz_lista_'.$typ_pojazdu] != null)
		{
			$temp = explode('|',$_SESSION['dod_wyposaz_lista_'.$typ_pojazdu]);
			$liczba_pozycji = count($temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp_pozycja = explode(';', $temp[$i]);
				$key_name = new KeyName();
				$key_name->key = $temp_pozycja[0];
				$key_name->name = $temp_pozycja[1];
				$dane_z_otomoto['dod_wyposaz'][$i] = $key_name;
			}
		}

		if(strtoupper($typ_pojazdu) == 'MOTORBIKE')
		{
			$poszukiwany_typ_pojazdu = 'MOTOCYKLE';
		}
		else
		{
			$poszukiwany_typ_pojazdu = 'SAMOCHODY';
		}

		if(@$_SESSION['kategorie_allegro_lista_'.$poszukiwany_typ_pojazdu] == null 
		|| @$_SESSION['kategorie_allegro_II_lista_'.$poszukiwany_typ_pojazdu] == null)
		{
			$parametry = array
			(
				'webapi-key' => $this->webapi
			);
			$kategorie_allegro_cala_lista = $this->klientOtomoto->__soapCall('getAllegroCategories', $parametry);
			if(!empty($kategorie_allegro_cala_lista))
			{
				foreach($kategorie_allegro_cala_lista as $kategoria_I)
				{
					foreach($kategoria_I->children as $kategoria_II)
					{
						if(strtoupper($kategoria_II->name) == $poszukiwany_typ_pojazdu) //samochody lub motocykle
						{
							$dane_z_otomoto_temp = $kategoria_II->children; 
							//$kategoria_II->children to np: osobowe, ciezarowe, dostawcze
						}
						foreach($kategoria_II->children as $kategoria_III)
						{
							foreach($kategoria_III->children as $kategoria_IV)
							{
								if($kategoria_IV->id == $data['pojazd_kategoria_allegro_II_id'])
								{
									$dane_z_otomoto_temp_II = $kategoria_III->children; 
									//$kategoria_III->children to np: opel
								}
								foreach($kategoria_IV->children as $kategoria_V)
								{
									if($kategoria_V->id == $data['pojazd_kategoria_allegro_III_id'])
									{
										$dane_z_otomoto_temp_III = $kategoria_IV->children; 
										//$kategoria_IV->children to np: astra
									}
									foreach($kategoria_V->children as $kategoria_VI)
									{
										if($kategoria_VI->id == $data['pojazd_kategoria_allegro_IV_id'])
										{
											$dane_z_otomoto_temp_IV = $kategoria_V->children;  
											//$kategoria_IV->children to np: II
										}
									}
								}
							}
						}
					}
				}
			}

			$temp = '';
			$liczba_pozycji = @count($dane_z_otomoto_temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp .= $dane_z_otomoto_temp[$i]->id.';'.$dane_z_otomoto_temp[$i]->name.'|';
			}
			$temp = substr($temp, 0, strlen($temp)-1);
			$_SESSION['kategorie_allegro_lista_'.$poszukiwany_typ_pojazdu] = $temp;

			$temp = '';
			$liczba_pozycji = @count($dane_z_otomoto_temp_II);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp .= $dane_z_otomoto_temp_II[$i]->id.';'.$dane_z_otomoto_temp_II[$i]->name.'|';
			}
			$temp = substr($temp, 0, strlen($temp)-1);
			$_SESSION['kategorie_allegro_II_lista_'.$poszukiwany_typ_pojazdu] = $temp;

			$temp = '';
			$liczba_pozycji = @count($dane_z_otomoto_temp_III);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp .= $dane_z_otomoto_temp_III[$i]->id.';'.$dane_z_otomoto_temp_III[$i]->name.'|';
			}
			$temp = substr($temp, 0, strlen($temp)-1);
			$_SESSION['kategorie_allegro_III_lista_'.$poszukiwany_typ_pojazdu] = $temp;

			$temp = '';
			$liczba_pozycji = @count($dane_z_otomoto_temp_IV);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp .= $dane_z_otomoto_temp_IV[$i]->id.';'.$dane_z_otomoto_temp_IV[$i]->name.'|';
			}
			$temp = substr($temp, 0, strlen($temp)-1);
			$_SESSION['kategorie_allegro_IV_lista_'.$poszukiwany_typ_pojazdu] = $temp;

			//$_SESSION['poszukiwany_typ_pojazdu_pop'] = $poszukiwany_typ_pojazdu;
		}

		if(@$_SESSION['kategorie_allegro_lista_'.$poszukiwany_typ_pojazdu] != null)
		{
			$temp = explode('|',$_SESSION['kategorie_allegro_lista_'.$poszukiwany_typ_pojazdu]);
			$liczba_pozycji = count($temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp_pozycja = explode(';', $temp[$i]);
				$key_name = new KeyName();
				$key_name->id = $temp_pozycja[0];
				$key_name->name = $temp_pozycja[1];
				$dane_z_otomoto['kategorie_allegro'][$i] = $key_name;
			}
		}
		if(@$_SESSION['kategorie_allegro_II_lista_'.$poszukiwany_typ_pojazdu] != null)
		{
			$temp = explode('|',$_SESSION['kategorie_allegro_II_lista_'.$poszukiwany_typ_pojazdu]);
			$liczba_pozycji = count($temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp_pozycja = explode(';', $temp[$i]);
				$key_name = new KeyName();
				$key_name->id = $temp_pozycja[0];
				$key_name->name = $temp_pozycja[1];
				$dane_z_otomoto['kategorie_allegro_II'][$i] = $key_name;
			}
		}
		if(@$_SESSION['kategorie_allegro_III_lista_'.$poszukiwany_typ_pojazdu] != null)
		{
			$temp = explode('|',$_SESSION['kategorie_allegro_III_lista_'.$poszukiwany_typ_pojazdu]);
			$liczba_pozycji = count($temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp_pozycja = explode(';', $temp[$i]);
				$key_name = new KeyName();
				$key_name->id = $temp_pozycja[0];
				$key_name->name = $temp_pozycja[1];
				$dane_z_otomoto['kategorie_allegro_III'][$i] = $key_name;
			}
		}
		if(@$_SESSION['kategorie_allegro_IV_lista_'.$poszukiwany_typ_pojazdu] != null)
		{
			$temp = explode('|',$_SESSION['kategorie_allegro_IV_lista_'.$poszukiwany_typ_pojazdu]);
			$liczba_pozycji = count($temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp_pozycja = explode(';', $temp[$i]);
				$key_name = new KeyName();
				$key_name->id = $temp_pozycja[0];
				$key_name->name = $temp_pozycja[1];
				$dane_z_otomoto['kategorie_allegro_IV'][$i] = $key_name;
			}
		}

		if(@$_SESSION['waluty_lista'] == null)
		{
			$parametry = array
			(
				'webapi-key' => $this->webapi
			);
			$dane_z_otomoto_temp = $this->klientOtomoto->__soapCall('getAllowedCurrencies', $parametry);
			$temp = '';
			$liczba_pozycji = count($dane_z_otomoto_temp);

			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$pozycja_temp = get_object_vars($dane_z_otomoto_temp[$i]);
				$temp .= $pozycja_temp['currency-id'].'|';
			}
			$temp = substr($temp, 0, strlen($temp)-1);
			$_SESSION['waluty_lista'] = $temp;
		}
		if(@$_SESSION['waluty_lista'] != null)
		{
			$temp = explode('|',$_SESSION['waluty_lista']);
			$liczba_pozycji = count($temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$dane_z_otomoto['waluty'][$i] = $temp[$i];
			}
		}

		if(@$_SESSION['rodzaj_paliwa_lista'] == null)
		{
			$parametry = array
			(
				'webapi-key' => $this->webapi,
				'type' => $typ_pojazdu
			);
			$dane_z_otomoto_temp = $this->klientOtomoto->__soapCall('getFuelTypes', $parametry);
			
			$temp = '';
			$liczba_pozycji = count($dane_z_otomoto_temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp .= $dane_z_otomoto_temp[$i]->key.';'.$dane_z_otomoto_temp[$i]->name.'|';
			}
			$temp = substr($temp, 0, strlen($temp)-1);
			$_SESSION['rodzaj_paliwa_lista'] = $temp;
		}
		if(@$_SESSION['rodzaj_paliwa_lista'] != null)
		{
			$temp = explode('|',$_SESSION['rodzaj_paliwa_lista']);
			$liczba_pozycji = count($temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp_pozycja = explode(';', $temp[$i]);
				$key_name = new KeyName();
				$key_name->key = $temp_pozycja[0];
				$key_name->name = $temp_pozycja[1];
				$dane_z_otomoto['rodzaj_paliwa'][$i] = $key_name;
			}
		}

		if(@$_SESSION['skrzynia_biegow_lista'] == null)
		{
			$parametry = array
			(
				'webapi-key' => $this->webapi
			);
			$dane_z_otomoto_temp = $this->klientOtomoto->__soapCall('getGearBoxTypes', $parametry);
			
			$temp = '';
			$liczba_pozycji = count($dane_z_otomoto_temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp .= $dane_z_otomoto_temp[$i]->key.';'.$dane_z_otomoto_temp[$i]->name.'|';
			}
			$temp = substr($temp, 0, strlen($temp)-1);
			$_SESSION['skrzynia_biegow_lista'] = $temp;
		}
		if(@$_SESSION['skrzynia_biegow_lista'] != null)
		{
			$temp = explode('|',$_SESSION['skrzynia_biegow_lista']);
			$liczba_pozycji = count($temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp_pozycja = explode(';', $temp[$i]);
				$key_name = new KeyName();
				$key_name->key = $temp_pozycja[0];
				$key_name->name = $temp_pozycja[1];
				$dane_z_otomoto['skrzynia_biegow'][$i] = $key_name;
			}
		}

		if(@$_SESSION['kolor_lista'] == null)
		{
			$parametry = array
			(
				'webapi-key' => $this->webapi
			);
			$dane_z_otomoto_temp = $this->klientOtomoto->__soapCall('getColours', $parametry);
			
			$temp = '';
			$liczba_pozycji = count($dane_z_otomoto_temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp .= $dane_z_otomoto_temp[$i]->key.';'.$dane_z_otomoto_temp[$i]->name.'|';
			}
			$temp = substr($temp, 0, strlen($temp)-1);
			$_SESSION['kolor_lista'] = $temp;
		}
		if(@$_SESSION['kolor_lista'] != null)
		{
			$temp = explode('|',$_SESSION['kolor_lista']);
			$liczba_pozycji = count($temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp_pozycja = explode(';', $temp[$i]);
				$key_name = new KeyName();
				$key_name->key = $temp_pozycja[0];
				$key_name->name = $temp_pozycja[1];
				$dane_z_otomoto['kolor'][$i] = $key_name;
			}
		}

		if(@$_SESSION['kraje_lista'] == null)
		{
			$parametry = array
			(
				'webapi-key' => $this->webapi
			);
			$dane_z_otomoto_temp = $this->klientOtomoto->__soapCall('getCountries', $parametry);
			$temp = '';
			$liczba_pozycji = count($dane_z_otomoto_temp);
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp .= $dane_z_otomoto_temp[$i]->id.';'.$dane_z_otomoto_temp[$i]->name.'|';
			}
			$temp = substr($temp, 0, strlen($temp)-1);
			$_SESSION['kraje_lista'] = $temp;
		}
		if(@$_SESSION['kraje_lista'] != null)
		{
			$temp = explode('|',$_SESSION['kraje_lista']);
			$liczba_pozycji = count($temp);
			$dane_z_otomoto['kraje'][$i] = $key_name;
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp_pozycja = explode(';', $temp[$i]);
				$key_name = new KeyName();
				$key_name->id = $temp_pozycja[0];
				$key_name->name = $temp_pozycja[1];
				$dane_z_otomoto['kraje'][$i] = $key_name;
			}

			$lista_czestych_krajow = array('PL', 'D', 'CZ', 'GB', 'HU');
			$liczba_czestych_krajow = count($lista_czestych_krajow);
			$k=0;
			for($i=0; $i<$liczba_pozycji; $i++)
			{
				$temp_pozycja = explode(';', $temp[$i]);
				for($j=0;$j<$liczba_czestych_krajow; $j++)//te kraje beda zdublowane i umieszczone na samym poczatku listy
				{
					if(strtoupper($temp_pozycja[0]) == $lista_czestych_krajow[$j])
					{
						$key_name = new KeyName();
						$key_name->id = $temp_pozycja[0];
						$key_name->name = $temp_pozycja[1];
						$dane_z_otomoto['czeste_kraje'][$k] = $key_name;
						$k++;						
					}
				}
			}
		}

		if($typ_pojazdu == 'MOTORBIKE')
		{
			if(@$_SESSION['rodzaj_napedu_lista'] == null)
			{
				$parametry = array
				(
					'type' => $typ_pojazdu,
					'webapi-key' => $this->webapi
				);
				$dane_z_otomoto_temp = $this->klientOtomoto->__soapCall('getTransmissionTypes', $parametry);
				$temp = '';
				$liczba_pozycji = count($dane_z_otomoto_temp);
				for($i=0; $i<$liczba_pozycji; $i++)
				{
					$temp .= $dane_z_otomoto_temp[$i]->key.';'.$dane_z_otomoto_temp[$i]->name.'|';
				}
				$temp = substr($temp, 0, strlen($temp)-1);
				$_SESSION['rodzaj_napedu_lista'] = $temp;
			}
			if(@$_SESSION['rodzaj_napedu_lista'] != null)
			{
				$temp = explode('|',$_SESSION['rodzaj_napedu_lista']);
				$liczba_pozycji = count($temp);
				for($i=0; $i<$liczba_pozycji; $i++)
				{
					$temp_pozycja = explode(';', $temp[$i]);
					$key_name = new KeyName();
					$key_name->key = $temp_pozycja[0];
					$key_name->name = $temp_pozycja[1];
					$dane_z_otomoto['rodzaj_napedu'][$i] = $key_name;
				}
			}
		}

		return @$dane_z_otomoto;
	}
	
	public function walidacjaFormularza($data, $wystawOtoMoto = 0)
	{
		$data['widoczny_otomoto'] = @intval($wystawOtoMoto);
		
        $error = '';
        $counter = 0;
		$typ_pojazdu = strtoupper($data['pojazd_typ']);

        if(false && empty($data['nazwa']))
		{
            $counter++;
            $error .= $counter.'. Pole "Nazwa ogłoszenia" jest wymagane.<br />';
        }

        if(false && !empty($data['link_r']))
		{
            if(!preg_match('^[a-zA-Z]+://+[A-Za-z0-9 \-_]+[A-Za-z0-9\.\/%&=\?\-_\,\#\+]+$^', $data['link_r']))
			{
                $counter++;
                $error .= $counter.'. Niepoprawny format pola "Link". Przykładowa postać linku: http://www.bigcom.pl/partnerzy/.<br />';
            }
        }
        
        if(@!empty($data['link_r']) && empty($data['link_nazwa']))
		{
            $counter++;
            $error .= $counter.'. W przypadku uzupełnienia pola link, pole "Nazwa linku" jest wymagane.<br />';
        }
		
        if((empty($data['pojazd_marka_id']) || $data['pojazd_marka_id'] == '-1') && $data['widoczny_otomoto'] == 1)
		{
            $counter++;
            $error .= $counter.'. Pole "Marka" jest wymagane, gdy ogłoszenie ma być opublikowane w otomoto.<br />';
        }

        if(@strlen($data['pojazd_marka_wlasne']) > 40)
		{
            $counter++;
            $error .= $counter.'. Pole "Własna marka" zawiera więcej niż 40 znaków.<br />';
        }

		if($typ_pojazdu != 'TRUCK')
		{
		    if((empty($data['pojazd_model_id']) || $data['pojazd_model_id'] == '-1') && $data['widoczny_otomoto'] == 1)
			{
		        $counter++;
		        $error .= $counter.'. Pole "Model" jest wymagane, gdy ogłoszenie ma być opublikowane w otomoto.<br />';
		    }
		}

        if((empty($data['pojazd_model_id']) || $data['pojazd_model_id'] == '-1') && (empty($data['pojazd_model_nazwa_wl']) || $data['pojazd_model_nazwa_wl'] == '-1') && $data['widoczny_otomoto'] == 189)
		{
            $counter++;
            $error .= $counter.'. Pole "Nazwa własna modelu" jest wymagane gdy nie podano modelu z rozwijalnej listy.<br />';
        }

        if(@strlen($data['pojazd_model_nazwa_wl']) > 40)
		{
            $counter++;
            $error .= $counter.'. Pole "Nazwa własna modelu" posiada więcej niż 40 znaków.<br />';
        }
		
        if(@strlen($data['pojazd_dod_opis_model']) > 100)
		{
            $counter++;
            $error .= $counter.'. Pole "Dodatkowy opis modelu" zawiera więcej niż 100 znaków.<br />';
        }
		
        /*
		if((empty($data['pojazd_wersja_id']) || $data['pojazd_wersja_id'] == '-1') && $data['widoczny_otomoto'] == 1)
		{
            $counter++;
            $error .= $counter.'. Pole "Wersja" jest wymagane, gdy ogłoszenie ma być opublikowane w otomoto.<br />';
        }
		*/
		
		if((empty($data['pojazd_typ_nadwozia_key']) || $data['pojazd_typ_nadwozia_key'] == '-1') && $data['widoczny_otomoto'] == 1)
		{
            $counter++;
            $error .= $counter.'. Pole "Typ" jest wymagane, gdy ogłoszenie ma być opublikowane w otomoto.<br />';
        }

        if(@strlen($data['pojazd_wersja_wlasne']) > 40)
		{
            $counter++;
            $error .= $counter.'. Pole "Własna wersja" zawiera więcej niż 40 znaków.<br />';
        }

        if(@strlen($data['pojazd_dod_info_wlasne']) > 255)
		{
            $counter++;
            $error .= $counter.'. Pole "Własne dodatkowe informacje" zawiera więcej niż 255 znaków.<br />';
        }
		
        if(@strlen($data['pojazd_dod_wyposaz_wlasne']) > 255)
		{
            $counter++;
            $error .= $counter.'. Pole "Własne dodatkowe wyposażenie" zawiera więcej niż 255 znaków.<br />';
        }

        if(@$data['pojazd_w_leasingu'] == 'n' && (empty($data['pojazd_cena']) || $data['pojazd_cena'] == '-1') && $data['widoczny_otomoto'] == 1)
		{
            $counter++;
            $error .= $counter.'. Pole "Cena" jest wymagane, gdy ogłoszenie ma być opublikowane w otomoto.<br />';
        }
		
		if(!empty($data['pojazd_cena']) && preg_match('/^[0-9]+(\.[0-9]+)?$/', $data['pojazd_cena']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Cena" ma nieprawidłowy format. Oczekiwano liczby dziesiętnej. Precyzja do dwóch miejsc po przecinku np: 22000.50<br />';
        }
		else if(!empty($data['pojazd_cena']) && strlen($data['pojazd_cena']) > 11)
		{
            $counter++;
            $error .= $counter.'. Pole "Cena" posiada za dużo znaków. Maksymalnie 11.<br />';
        }
		else
		{
			$data['pojazd_cena'] = @round(floatval($data['pojazd_cena']), 2); //czy może być tutaj zaokrąglenie?
			if($data['pojazd_cena'] == 0)
			{
				$data['pojazd_cena'] = '';
			}
			/* else
			{
				$data['pojazd_cena'] = number_format($data['pojazd_cena'], 2, '.', ' ');
			}
			*/
		}

        /*
		if((empty($data['pojazd_cena']) || $data['pojazd_cena'] == '-1') && (empty($data['cena_pln']) || $data['cena_pln'] == '-1'))
		{
            $counter++;
            $error .= $counter.'. Pole "Cena PLN" jest wymagane.<br />';
        }
		else
		{
			if(!empty($data['cena_pln']) && preg_match('/^[0-9]+(\.[0-9]+)?$/', $data['cena_pln']) == 0)
			{
		        $counter++;
		        $error .= $counter.'. Pole "Cena PLN" ma nieprawidłowy format. Oczekiwano liczby dziesiętnej. Precyzja do dwóch miejsc po przecinku np: 22000.50<br />';
		    }
			else if(!empty($data['cena_pln']) && strlen($data['cena_pln']) > 11)
			{
		        $counter++;
		        $error .= $counter.'. Pole "Cena PLN" posiada za dużo znaków. Maksymalnie 11.<br />';
		    }
		}
		*/
		
        if((empty($data['pojazd_typ_ceny']) || $data['pojazd_typ_ceny'] == '-1') && $data['widoczny_otomoto'] == 1)
		{
            $counter++;
            $error .= $counter.'. Typ ceny jest wymagany, gdy ogłoszenie ma być opublikowane w otomoto.<br />';
        }
		
		if(!empty($data['pojazd_wystawiam_f_vat']) && preg_match('/^(n)|(y)?$/', $data['pojazd_wystawiam_f_vat']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Wystawiam fakturę VAT" posiada nieprawidłową wartość.<br />';
        }

        /*if(!empty($data['pojazd_do_negocjacji']) && preg_match('/^[nt]|(-1)?$/', $data['pojazd_do_negocjacji']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Do negocjacji" posiada nieprawidłowy format.<br />';
        }
		*/
		
		if(!empty($data['pojazd_do_negocjacji']) && preg_match('/^(n)|(y)?$/', $data['pojazd_do_negocjacji']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Do negocjacji" posiada nieprawidłową wartość.<br />';
        }
		
        /*if(!empty($data['pojazd_w_leasingu']) && preg_match('/^[nt]|(-1)?$/', $data['pojazd_w_leasingu']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "W leasingu" posiada nieprawidłowy format.<br />';
        }
		*/
		if(!empty($data['pojazd_w_leasingu']) && preg_match('/^(n)|(y)?$/', $data['pojazd_w_leasingu']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Leasing" posiada nieprawidłową wartość.<br />';
        }

		if(!empty($data['pojazd_kwota_odstepnego']) && preg_match('/^[0-9]+$/', $data['pojazd_kwota_odstepnego']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Kwota odstępnego" ma nieprawidłowy format. Oczekiwano liczby całkowitej większej od zera.<br />';
        }
		else if(!empty($data['pojazd_kwota_odstepnego']) && strlen($data['pojazd_kwota_odstepnego']) > 10)
		{
            $counter++;
            $error .= $counter.'. Pole "Kwota odstępnego" posiada za dużo znaków. Maksymalnie 10.<br />';
        }
		/* else
		{
			$data['pojazd_kwota_odstepnego']= number_format(str_replace(' ', '', $data['pojazd_kwota_odstepnego']), 0, '.', ' ');
		}
		*/

        if(@$data['pojazd_w_leasingu'] == 'y' && (empty($data['pojazd_wysokosc_raty']) || $data['pojazd_wysokosc_raty'] == '-1'))
		{
            $counter++;
            $error .= $counter.'. Pole "Wysokosc raty" jest wymagane, gdy pojazd jest w leasingu.<br />';
        }
		
		if(!empty($data['pojazd_wysokosc_raty']) && preg_match('/^[0-9]+$/', $data['pojazd_wysokosc_raty']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Wysokosc raty" ma nieprawidłowy format. Oczekiwano liczby całkowitej większej od zera.<br />';
        }
		else if(!empty($data['pojazd_wysokosc_raty']) && strlen($data['pojazd_wysokosc_raty']) > 10)
		{
            $counter++;
            $error .= $counter.'. Pole "Wysokosc raty" posiada za dużo znaków. Maksymalnie 10.<br />';
        }
		/* else
		{
			$data['pojazd_wysokosc_raty']= number_format(str_replace(' ', '', $data['pojazd_wysokosc_raty']), 0, '.', ' ');
		}
		*/

        if(@$data['pojazd_w_leasingu'] == 'y' && (empty($data['pojazd_ilosc_pozos_rat']) || $data['pojazd_ilosc_pozos_rat'] == '-1'))
		{
            $counter++;
            $error .= $counter.'. Pole "Ilość pozostałych rat" jest wymagane, gdy pojazd jest w leasingu.<br />';
        }
		
		if(!empty($data['pojazd_ilosc_pozos_rat']) && preg_match('/^[0-9]+$/', $data['pojazd_ilosc_pozos_rat']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Ilość pozostałych rat" ma nieprawidłowy format. Oczekiwano liczby całkowitej większej od zera.<br />';
        }
		else if(!empty($data['pojazd_ilosc_pozos_rat']) && strlen($data['pojazd_ilosc_pozos_rat']) > 4)
		{
            $counter++;
            $error .= $counter.'. Pole "Ilosc pozostałych rat" posiada za dużo znaków. Maksymalnie 4.<br />';
        }
		/* 
		else
		{
			$data['pojazd_ilosc_pozos_rat']= number_format(str_replace(' ', '', $data['pojazd_ilosc_pozos_rat']), 0, '.', ' ');
		}
		*/

		if(!empty($data['pojazd_wartosc_wykupu']) && preg_match('/^[0-9]+$/', $data['pojazd_wartosc_wykupu']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Wartość wykupu" ma nieprawidłowy format. Oczekiwano liczby całkowitej większej od zera.<br />';
        }
		else if(!empty($data['pojazd_wartosc_wykupu']) && strlen($data['pojazd_wartosc_wykupu']) > 10)
		{
            $counter++;
            $error .= $counter.'. Pole "Wartość wykupu" posiada za dużo znaków. Maksymalnie 10.<br />';
        }
		/*
		else
		{
			$data['pojazd_wartosc_wykupu']= number_format(str_replace(' ', '', $data['pojazd_wartosc_wykupu']), 0, '.', ' ');
		}
		*/

		if(!empty($data['pojazd_mozliwosc_kredytowania']) && preg_match('/^(n)|(y)?$/', $data['pojazd_mozliwosc_kredytowania']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Możliwość kredytowania" posiada nieprawidłową wartość.<br />';
        }

        if((empty($data['pojazd_rok_produkcji']) || $data['pojazd_rok_produkcji'] == '-1') && $data['widoczny_otomoto'] == 1)
		{
            $counter++;
            $error .= $counter.'. Pole "Rok produkcji" jest wymagane, gdy ogłoszenie ma być opublikowane w otomoto.<br />';
        }
		else if($data['pojazd_rok_produkcji'] != '-1' && preg_match('/^[0-9]{4}$/', $data['pojazd_rok_produkcji']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Rok produkcji" ma nieprawidłowy format. Oczekiwano czterocyfrowej liczby całkowitej większej od zera.<br />';
        }

		if(@$data['pojazd_stan_techniczny'] != 'new' && (empty($data['pojazd_przeglad_rok']) && !empty($data['pojazd_przeglad_miesiac'])))
		{
            $counter++;
            $error .= $counter.'. Pole "Przegląd" ma nieprawidłowy format. Wymagany jest rok przeglądu, gdy podany jest miesiąc przeglądu.<br />';
        }
		if(@$data['pojazd_stan_techniczny'] != 'new' && (!empty($data['pojazd_przeglad_rok']) && $data['pojazd_przeglad_rok'] != '-1' && preg_match('/^[0-9]{4}$/', $data['pojazd_przeglad_rok']) == 0))
		{
            $counter++;
            $error .= $counter.'. Pole "Przegląd" ma nieprawidłowy format. Rok to czterocyfrowa liczba całkowita większa od zera.<br />';
        }
		if(@$data['pojazd_stan_techniczny'] != 'new' && (!empty($data['pojazd_przeglad_miesiac']) && $data['pojazd_przeglad_miesiac'] != '-1' && preg_match('/^[0-9]{1,2}$/', $data['pojazd_przeglad_miesiac']) == 0))
		{
            $counter++;
            $error .= $counter.'. Pole "Przegląd" ma nieprawidłowy format. Miesiąc to dwucyfrowa liczba całkowita większa od zera.<br />';
        }
		if(@$data['pojazd_stan_techniczny'] != 'new' && ((empty($data['pojazd_przeglad_rok']) || $data['pojazd_przeglad_rok'] == '-1') && $data['pojazd_przeglad_miesiac'] != '-1' && $data['widoczny_otomoto'] == 1))
		{
            $counter++;
            $error .= $counter.'. Pole "Przegląd" ma nieprawidłowy format. Jeżeli podany jest miesiąc przeglądu, należy podać również rok, gdy ogłoszenie ma być opublikowane w otomoto.<br />';
        }

		if(@$data['pojazd_stan_techniczny'] != 'new' && (empty($data['pojazd_ubezp_oc_rok']) && !empty($data['pojazd_ubezp_oc_miesiac'])))
		{
            $counter++;
            $error .= $counter.'. Pole "Ubezpieczenie OC" ma nieprawidłowy format. Wymagany jest rok ubezpieczenia, gdy podany jest miesiąc ubezpieczenia.<br />';
        }
		if(@$data['pojazd_stan_techniczny'] != 'new' && (!empty($data['pojazd_ubezp_oc_rok']) && $data['pojazd_ubezp_oc_rok'] != '-1'  && preg_match('/^[0-9]{4}$/', $data['pojazd_ubezp_oc_rok']) == 0))
		{
            $counter++;
            $error .= $counter.'. Pole "Ubezpieczenie OC" ma nieprawidłowy format. Rok to czterocyfrowa liczba całkowita większa od zera.<br />';
        }
		if(@$data['pojazd_stan_techniczny'] != 'new' && (!empty($data['pojazd_ubezp_oc_miesiac']) && $data['pojazd_ubezp_oc_miesiac'] != '-1' && preg_match('/^[0-9]{1,2}$/', $data['pojazd_ubezp_oc_miesiac']) == 0))
		{
            $counter++;
            $error .= $counter.'. Pole "Ubezpieczenie OC" ma nieprawidłowy format. Miesiąc to dwucyfrowa liczba całkowita większa od zera.<br />';
        }
		if(@$data['pojazd_stan_techniczny'] != 'new' && ((empty($data['pojazd_ubezp_oc_rok']) || $data['pojazd_ubezp_oc_rok'] == '-1') && $data['pojazd_ubezp_oc_miesiac'] != '-1'))
		{
            $counter++;
            $error .= $counter.'. Pole "Ubezpieczenie OC" ma nieprawidłowy format. Jeżeli podany jest miesiąc ubezpieczenia, należy podać również rok.<br />';
        }

		if(@$data['pojazd_stan_techniczny'] != 'new' && (empty($data['pojazd_pierw_rej_rok']) && !empty($data['pojazd_pierw_rej_miesiac'])))
		{
            $counter++;
            $error .= $counter.'. Pole "Pierwsza rejestracja" ma nieprawidłowy format. Wymagany jest rok rejestracji, gdy podany jest miesiąc rejestracji.<br />';
        }
		if(@$data['pojazd_stan_techniczny'] != 'new' && (!empty($data['pojazd_pierw_rej_rok']) && $data['pojazd_pierw_rej_rok'] != '-1' && preg_match('/^[0-9]{4}$/', $data['pojazd_pierw_rej_rok']) == 0))
		{
            $counter++;
            $error .= $counter.'. Pole "Pierwsza rejestracja" ma nieprawidłowy format. Rok to czterocyfrowa liczba całkowita większa od zera.<br />';
        }
		if(@$data['pojazd_stan_techniczny'] != 'new' && (!empty($data['pojazd_pierw_rej_miesiac']) && $data['pojazd_pierw_rej_miesiac'] != '-1' && preg_match('/^[0-9]{1,2}$/', $data['pojazd_pierw_rej_miesiac']) == 0))
		{
            $counter++;
            $error .= $counter.'. Pole "Pierwsza rejestracja" ma nieprawidłowy format. Miesiąc to dwucyfrowa liczba całkowita większa od zera.<br />';
        }
		if(@$data['pojazd_stan_techniczny'] != 'new' && ((empty($data['pojazd_pierw_rej_rok']) || $data['pojazd_pierw_rej_rok'] == '-1') && $data['pojazd_pierw_rej_miesiac'] != '-1' && $data['widoczny_otomoto'] == 1))
		{
            $counter++;
            $error .= $counter.'. Pole "Pierwsza rejestracja" ma nieprawidłowy format. Jeżeli podany jest miesiąc rejestracji, należy podać również rok, gdy ogłoszenie ma być opublikowane w otomoto.<br />';
        }

		if(!empty($data['pojazd_przebieg']) && preg_match('/^[0-9]+$/', $data['pojazd_przebieg']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Przebieg" ma nieprawidłowy format. Oczekiwano liczby całkowitej większej od zera.<br />';
        }
		else if(!empty($data['pojazd_przebieg']) && strlen($data['pojazd_przebieg']) > 10)
		{
            $counter++;
            $error .= $counter.'. Pole "Przebieg" posiada za dużo znaków. Maksymalnie 10.<br />';
        }

		if(!empty($data['pojazd_moc']) && preg_match('/^[0-9]+$/', $data['pojazd_moc']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Moc" ma nieprawidłowy format. Oczekiwano liczby całkowitej większej od zera.<br />';
        }
		else if(!empty($data['pojazd_przebieg']) && strlen($data['pojazd_przebieg']) > 10)
		{
            $counter++;
            $error .= $counter.'. Pole "Moc" posiada za dużo znaków. Maksymalnie 10.<br />';
        }

        if(!empty($data['pojazd_jedn_mocy']) && preg_match('/^(-1)|(kW)|(PS)?$/', $data['pojazd_jedn_mocy']) == 0)
		{
            $counter++;
            $error .= $counter.'. Jednostka mocy posiada nieprawidłowy format.<br />';
        }

       	/*
		if(!empty($data['pojazd_metalik']) && preg_match('/^(-1)|(n)|(t)?$/', $data['pojazd_metalik']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Metalik" posiada nieprawidłowy format.<br />';
        }
		*/
		if(!empty($data['pojazd_metalik']) && preg_match('/^(n)|(y)?$/', $data['pojazd_metalik']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Metalik" posiada nieprawidłową wartość.<br />';
        }

        if(!empty($data['pojazd_liczba_drzwi']) && preg_match('/^(-1)|(2\/3)|(4\/5)?$/', $data['pojazd_liczba_drzwi']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Liczba drzwi" posiada nieprawidłowy format.<br />';
        }

		if(!empty($data['pojazd_poj_skokowa']) && preg_match('/^[0-9]+$/', $data['pojazd_poj_skokowa']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Pojemność skokowa" ma nieprawidłowy format. Oczekiwano liczby całkowitej większej od zera.<br />';
        }
		else if(!empty($data['pojazd_poj_skokowa']) && strlen($data['pojazd_poj_skokowa']) > 8)
		{
            $counter++;
            $error .= $counter.'. Pole "Pojemność skokowa" posiada za dużo znaków. Maksymalnie 8.<br />';
        }

		if(@$data['widoczny_otomoto'] == 1 && (empty($data['pojazd_stan_techniczny']) || $data['pojazd_stan_techniczny'] == '-1') && $typ_pojazdu == 'CAR')
		{
            $counter++;
            $error .= $counter.'. Pole "Stan techniczny" jest wymagane, gdy ogłoszenie ma być opublikowane w otomoto.<br />';
		}

		if(!empty($data['pojazd_stan_techniczny']) && $data['pojazd_stan_techniczny'] != '-1' && $typ_pojazdu == 'CAR')
		{
            if($data['pojazd_stan_techniczny'] == 'undamaged')
			{
				$data['pojazd_uszkodzony'] = 'n';
			}
			else if($data['pojazd_stan_techniczny'] == 'damaged')
			{
				$data['pojazd_uszkodzony'] = 'y';
			}
        }

        if(!empty($data['pojazd_uszkodzony']) && preg_match('/^(-1)|(n)|(t)?$/', $data['pojazd_uszkodzony']) == 0 && $typ_pojazdu != 'CAR')
		{
            $counter++;
            $error .= $counter.'. Pole "Pojazd uszkodzony" posiada nieprawidłowy format.<br />';
        }

        if(!empty($data['pojazd_email_wyst']) && strlen($data['pojazd_email_wyst']) > 64)
		{
            $counter++;
            $error .= $counter.'. Pole "E-mail wystawiającego" posiada za dużo znaków. Maksymalnie 64.<br />';
        }

        if(!empty($data['pojazd_ladowalnosc']) && preg_match('/^[0-9]+$/', $data['pojazd_ladowalnosc']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Ładowność" ma nieprawidłowy format. Oczekiwano liczby całkowitej większej od zera.<br />';
        }
		else if(!empty($data['pojazd_ladowalnosc']) && strlen($data['pojazd_ladowalnosc']) > 10)
		{
            $counter++;
            $error .= $counter.'. Pole "Ładowność" posiada za dużo znaków. Maksymalnie 10.<br />';
        }

        if(!empty($data['pojazd_liczba_osi']) && $data['pojazd_liczba_osi'] != '-1' && preg_match('/^[0-9]*$/', $data['pojazd_liczba_osi']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Liczba osi" ma nieprawidłowy format. Oczekiwano liczby całkowitej większej od zera.<br />';
        }
		else if(!empty($data['pojazd_liczba_osi']) && strlen($data['pojazd_liczba_osi']) > 10)
		{
            $counter++;
            $error .= $counter.'. Pole "Liczba osi" posiada za dużo znaków. Maksymalnie 10.<br />';
        }

		if(!empty($data['pojazd_vin']) && strlen($data['pojazd_vin']) != 17)
		{
            $counter++;
            $error .= $counter.'. Pole "VIN - numer ident. pojazdu" posiada złą ilość znaków. Oczekiwano 17 znaków.<br />';
        }
		
		if(!empty($data['pojazd_publ_w_serw_zagr']) && preg_match('/^(n)|(y)?$/', $data['pojazd_publ_w_serw_zagr']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Zgoda na publikacje w serwisach zagranicznych" posiada nieprawidłową wartość.<br />';
        }

		if(!empty($data['pojazd_publ_w_tablica_pl']) && preg_match('/^(n)|(y)?$/', $data['pojazd_publ_w_tablica_pl']) == 0)
		{
            $counter++;
            $error .= $counter.'. Pole "Chcę również dodać darmowe ogłoszenie w serwisie tablica.pl" posiada nieprawidłową wartość.<br />';
        }

		if(!empty($data['pojazd_num_wybr_kat_allegro']) && preg_match('/^[0-9]+$/', $data['pojazd_num_wybr_kat_allegro']) == 0)
		{
            $counter++;
            $error .= $counter.'. Numer wybranej kategorii Allegro ma nieprawidłowy format. Oczekiwano liczby całkowitej większej od zera.<br />';
        }

        return $error;
    }
	
	public function dod_edyt_ogl_otomoto($id = 0, $data, $tab_dod_info_oto, $tab_dod_wyposaz_oto, $pole_tekst)
	{
		$edytuj = new Produkty();
		$edytuj->id = $id;
		$pojRealizacja = $edytuj->wypiszPojedyncza();
		$pojRealizacjaDane = @unserialize($pojRealizacja['otoMoto']);
		//var_dump($data);
		$typ_pojazdu = strtoupper($data['pojazd_typ']);

		$insertion = array
		(
			'type' => $data['pojazd_typ'],
			'make-id' => $data['pojazd_marka_id'],
			'vehicle-category-key' => $data['pojazd_typ_nadwozia_key'],
			'price' => @$data['pojazd_cena'],
			'price-type-key' => $data['pojazd_typ_ceny'],
			'vehicle-features-list' => $tab_dod_info_oto,
			'vehicle-extras-list' => $tab_dod_wyposaz_oto
		);

		if($typ_pojazdu == 'CAR')
		{
			$insertion['tech-condition'] = $data['pojazd_stan_techniczny'];
		}
		else
		{
			$insertion['damaged'] = @$data['pojazd_uszkodzony'];
		}
		if($typ_pojazdu == 'TRUCK')
		{
			$insertion['model-name'] = $data['pojazd_model_nazwa_wl'];
		}
		else
		if(trim($data['pojazd_model_id']) != null && trim($data['pojazd_model_id']) != '-1')
		{
			$insertion['model-id'] = $data['pojazd_model_id']; // dla CAR i MOTORBIKE
		}
		else
		{
			$insertion['model-name'] = $data['pojazd_model_nazwa_wl']; // dla CAR i MOTORBIKE
		}
		if($typ_pojazdu == 'CAR')
		{
			$insertion['version-id'] = $data['pojazd_wersja_id'];
		}
		if($typ_pojazdu != 'TRUCK')
		{
			if(trim($data['pojazd_dod_opis_model']) != null)
			{
				$insertion['model-name-extension'] = $data['pojazd_dod_opis_model'];
			}
		}

		/*
		if($data['pojazd_kategoria_allegro_IV_id'] != null && intval($data['pojazd_kategoria_allegro_IV_id']) > 0)
		{
			$insertion['allegro-cat-id'] = $data['pojazd_kategoria_allegro_IV_id'];
		}
		else if($data['pojazd_kategoria_allegro_III_id'] != null && intval($data['pojazd_kategoria_allegro_III_id']) > 0)
		{
			$insertion['allegro-cat-id'] = $data['pojazd_kategoria_allegro_III_id'];
		}
		else if($data['pojazd_kategoria_allegro_II_id'] != null && intval($data['pojazd_kategoria_allegro_II_id']) > 0)
		{
			$insertion['allegro-cat-id'] = $data['pojazd_kategoria_allegro_II_id'];
		}
		else if($data['pojazd_kategoria_allegro_id'] != null && intval($data['pojazd_kategoria_allegro_id']) > 0)
		{
			$insertion['allegro-cat-id'] = $data['pojazd_kategoria_allegro_id'];
		}
		*/

		if($data['pojazd_num_wybr_kat_allegro'] != null && intval($data['pojazd_num_wybr_kat_allegro']) > 0)
		{
			$insertion['allegro-cat-id'] = $data['pojazd_num_wybr_kat_allegro'];
		}

		if(@trim($data['pojazd_do_negocjacji']) != null && $data['pojazd_do_negocjacji'] != '-1')
		{
			$insertion['price-negotiable'] = $data['pojazd_do_negocjacji'];
		}
		if(@trim($data['pojazd_wystawiam_f_vat']) != null && $data['pojazd_wystawiam_f_vat'] != '-1')
		{
			$insertion['invoice-available'] = $data['pojazd_wystawiam_f_vat'];
		}
		if(@trim($data['pojazd_w_leasingu']) == 'y')
		{
			$insertion['leasing'] = $data['pojazd_w_leasingu'];
			$insertion['price-currency'] = $data['pojazd_waluta_odstepnego'];
		}
		else
		{
			$insertion['price-currency'] = $data['pojazd_waluta'];
		}
		if(@trim($data['pojazd_kwota_odstepnego']) != null)
		{
			$insertion['leasing_compensation_amount'] = $data['pojazd_kwota_odstepnego'];
		}
		if(@trim($data['pojazd_wysokosc_raty']) != null)
		{
			$insertion['leasing_instalment_amount'] = $data['pojazd_wysokosc_raty'];
		}
		if(@trim($data['pojazd_ilosc_pozos_rat']) != null)
		{
			$insertion['leasing_instalment_number'] = $data['pojazd_ilosc_pozos_rat'];
		}
		if(@trim($data['pojazd_wartosc_wykupu']) != null)
		{
			$insertion['leasing_residual_value'] = $data['pojazd_wartosc_wykupu'];
		}
		if(@trim($data['pojazd_mozliwosc_kredytowania']) != null && $data['pojazd_mozliwosc_kredytowania'] != '-1')
		{
			$insertion['credit-ability'] = $data['pojazd_mozliwosc_kredytowania'];
		}
		if(@trim($data['pojazd_rok_produkcji']) != null && $data['pojazd_rok_produkcji'] != '-1')
		{
			$insertion['build-year'] = $data['pojazd_rok_produkcji'];
		}
		if(@trim($data['pojazd_przeglad_rok']) != null && $data['pojazd_przeglad_rok'] != '-1')
		{
			$insertion['tech-check-year'] = $data['pojazd_przeglad_rok'];
		}
		else // np: gdy stan techniczny = new
		{
			$insertion['tech-check-year'] = '';
		}
		if(@trim($data['pojazd_przeglad_miesiac']) != null && $data['pojazd_przeglad_miesiac'] != '-1')
		{
			$insertion['tech-check-month'] = $data['pojazd_przeglad_miesiac'];
		}
		else // np: gdy stan techniczny = new
		{
			$insertion['tech-check-month'] = '';
		}
		if(@trim($data['pojazd_ubezp_oc_rok']) != null && $data['pojazd_ubezp_oc_rok'] != '-1')
		{
			$insertion['insurance-year'] = $data['pojazd_ubezp_oc_rok'];
		}
		else // np: gdy stan techniczny = new
		{
			$insertion['insurance-year'] = '';
		}
		if(@trim($data['pojazd_ubezp_oc_miesiac']) != null && $data['pojazd_ubezp_oc_miesiac'] != '-1')
		{
			$insertion['insurance-month'] = $data['pojazd_ubezp_oc_miesiac'];
		}
		else // np: gdy stan techniczny = new
		{
			$insertion['insurance-month'] = '';
		}
		if(@trim($data['pojazd_pierw_rej_rok']) != null && $data['pojazd_pierw_rej_rok'] != '-1')
		{
			$insertion['first-registration-year'] = $data['pojazd_pierw_rej_rok'];
		}
		else
		{ // np: gdy stan techniczny = new
			$insertion['first-registration-year'] = '';
		}
		if(@trim($data['pojazd_pierw_rej_miesiac']) != null && $data['pojazd_pierw_rej_miesiac'] != '-1')
		{
			$insertion['first-registration-month'] = $data['pojazd_pierw_rej_miesiac'];
		}
		else
		{ // np: gdy stan techniczny = new
			$insertion['first-registration-month'] = '';
		}
		if(@trim($data['pojazd_przebieg']) != null)
		{
			$insertion['odometer'] = $data['pojazd_przebieg'];
		}
		else
		{ // np: gdy stan techniczny = new
			$insertion['odometer'] = '';
		}
		if(@trim($data['pojazd_moc']) != null)
		{
			$insertion['power'] = $data['pojazd_moc'];
		}
		if(@trim($data['pojazd_jedn_mocy']) != null && $data['pojazd_jedn_mocy'] != '-1')
		{
			$insertion['power-unit'] = $data['pojazd_jedn_mocy'];
		}
		if(@trim($data['pojazd_rodzaj_paliwa_key']) != null && $data['pojazd_rodzaj_paliwa_key'] != '-1')
		{
			$insertion['fuel-type-key'] = $data['pojazd_rodzaj_paliwa_key'];
		}
		if(@trim($data['pojazd_skrzynia_biegow_key']) != null && $data['pojazd_skrzynia_biegow_key'] != '-1')
		{
			$insertion['gearbox-type-key'] = $data['pojazd_skrzynia_biegow_key'];
		}
		if(@trim($data['pojazd_kolor_key']) != null && $data['pojazd_kolor_key'] != '-1')
		{
			$insertion['colour-key'] = $data['pojazd_kolor_key'];
		}
		if(@trim($data['pojazd_metalik']) != null && $data['pojazd_metalik'] != '-1')
		{
			$insertion['colour-metallic'] = $data['pojazd_metalik'];
		}
		if($typ_pojazdu == 'CAR')
		{
			if(@trim($data['pojazd_liczba_drzwi']) != null && $data['pojazd_liczba_drzwi'] != '-1')
			{
				$insertion['doors-key'] = $data['pojazd_liczba_drzwi'];
			}
		}
		if($typ_pojazdu != 'TRUCK')
		{
			if(@trim($data['pojazd_poj_skokowa']) != null && $data['pojazd_poj_skokowa'] != '-1')
			{
				$insertion['cubic-capacity'] = $data['pojazd_poj_skokowa'];
			}
		}
		if(@trim($data['pojazd_email_wyst']) != null)
		{
			$insertion['seller-email'] = $data['pojazd_email_wyst'];
		}
		if(@trim($data['pojazd_stat_poj_sprow_id']) != null && $data['pojazd_stat_poj_sprow_id'] != '-1')
		{
			$insertion['origin-status'] = $data['pojazd_stat_poj_sprow_id'];
		}
		else // np: gdy stan techniczny = new
		{
			$insertion['origin-status'] = '';
		}
		if(@trim($data['pojazd_kraj_pochodzenia_id']) != null && $data['pojazd_kraj_pochodzenia_id'] != '-1')
		{
			$insertion['origin-country'] = $data['pojazd_kraj_pochodzenia_id'];
		}
		else // np: gdy stan techniczny = new
		{
			$insertion['origin-country'] = '';
		}
		if(@trim($data['pojazd_kraj_akt_rej_id']) != null && $data['pojazd_kraj_akt_rej_id'] != '-1')
		{
			$insertion['registration-country'] = $data['pojazd_kraj_akt_rej_id'];
		}
		else // np: gdy stan techniczny = new
		{
			$insertion['registration-country'] = '';
		}
		if($typ_pojazdu == 'TRUCK')
		{
			if(@trim($data['pojazd_ladowalnosc']) != null && $data['pojazd_ladowalnosc'] != '-1')
			{									
				$insertion['load-capacity'] = $data['pojazd_ladowalnosc'];
			}
			if(@trim($data['pojazd_liczba_osi']) != null && $data['pojazd_liczba_osi'] != '-1')
			{									
				$insertion['axis-num'] = $data['pojazd_liczba_osi'];
			}
		}
		if($typ_pojazdu == 'MOTORBIKE')
		{
			if(@trim($data['pojazd_rodzaj_napedu']) != null && $data['pojazd_rodzaj_napedu'] != '-1')
			{									
				$insertion['transmission-key'] = $data['pojazd_rodzaj_napedu'];
			}
		}
		if(@trim($data['pojazd_vin']) != null)
		{
			$insertion['vin'] = $data['pojazd_vin'];
		}
		if(@trim($data['pojazd_publ_w_serw_zagr']) != null && $data['pojazd_publ_w_serw_zagr'] != '-1')
		{
			$insertion['any-country'] = $data['pojazd_publ_w_serw_zagr'];
		}
		if(@trim($data['pojazd_publ_w_tablica_pl']) != null && $data['pojazd_publ_w_tablica_pl'] != '-1')
		{
			$insertion['tablica-province'] = $data['pojazd_publ_w_tablica_pl'];
		}
		if(@trim($pole_tekst) != null)
		{
			$insertion['description'] = $pole_tekst;
		}

		if(@!empty($pojRealizacjaDane['data']['pojazd_id_ogloszenia'])) // gdy edytujemy ogłoszenie w otomoto
		{
			$insertion['id'] = $pojRealizacjaDane['data']['pojazd_id_ogloszenia'];
		}
		$parameters = array
		(
			"insertion" => $insertion,
			"session-id" => $_SESSION['sidotomoto'],
			"webapi-key" => $this->webapi
		);
		//var_dump($pojRealizacjaDane);die();

		try
		{
			$response = $this->klientOtomoto->__soapCall('doInsertionEdit', $parameters); //dodawanie ogloszenia
		}
		catch(Exception $e)
		{
			if($e->getMessage() == 'insertion not found') // ponowna próba dodania ogłoszenia
			{
				$parameters['insertion']['id'] = '';
				$response = $this->klientOtomoto->__soapCall('doInsertionEdit', $parameters); 
			}
		}
		if(@$response['status'] == 'OK')
		{
			//$temp_data['pojazd_id_ogloszenia'] = $response['id'];
			$pojRealizacjaDane['data']['pojazd_id_ogloszenia'] = $response['insertion']->id;
			$temp_data['otoMoto'] = @serialize($pojRealizacjaDane);
			$edytuj->edytujProdukty($temp_data);
		}
		//$numer_edyt_ogloszenia = $response['insertion']->id;
		return @$response;
	}
	
	public function usun_ogloszenie($id, $insertion)
	{
		$parameters = array
		(
			"insertion" => $insertion,
			"session-id" => $_SESSION['sidotomoto'],
			"webapi-key" => $this->webapi
		);
		try
		{
			$response = $this->klientOtomoto->__soapCall('doInsertionDelete', $parameters);
			if($response == 'OK')
			{
				$edytuj = new Produkty();
				$edytuj->id = $id;
				$pojRealizacja = $edytuj->wypiszPojedyncza();
				$pojRealizacjaDane = @unserialize($pojRealizacja['otoMoto']);
				$pojRealizacjaDane['data']['pojazd_id_ogloszenia'] = '';
				$temp_data['otoMoto'] = @serialize($pojRealizacjaDane);
				$edytuj->edytujProdukty($temp_data);
			}
			return $response;
		}
		catch(SoapFault $e)
		{
			if($e->getMessage() == 'insertion not found')
			{
				$edytuj = new Produkty();
				$edytuj->id = $id;
				$pojRealizacja = $edytuj->wypiszPojedyncza();
				$pojRealizacjaDane = @unserialize($pojRealizacja['otoMoto']);
				$pojRealizacjaDane['data']['pojazd_id_ogloszenia'] = '';
				$temp_data['otoMoto'] = @serialize($pojRealizacjaDane);
				$edytuj->edytujProdukty($temp_data);
			}
			return $e->getMessage();
		}
		catch(Exception $e)
		{
			return $e;
		}
		return null;
	}

    public function dodaj_pola_name(&$data/*, $typ_pojazdu = 'CAR'*/)
	{
		if($this->serwisNiedostepny === false)
		{
			$parametry = array(
				'type' => $data['pojazd_typ'],
				'webapi-key' => $this->webapi
			);
			$marki = $this->klientOtomoto->__soapCall('getMakes', $parametry);
			if(!empty($marki))
			{
				foreach($marki as $marka)
				{
					if($data['pojazd_marka_id'] == $marka->id)
					{
						$data['pojazd_marka_name'] = $marka->name;
					}
				}
			}

			if(isset($data['pojazd_model_id'])) // tylko dla CAR i MOTORBIKE
			{
				if($data['pojazd_model_id'] != '-1')
				{
					$parametry = array
					(
						'make-id' => $data['pojazd_marka_id'],
						'webapi-key' => $this->webapi,
						'country-code' => '1',
						'type' => $data['pojazd_typ']
					);

					$modele = $this->klientOtomoto->__soapCall('getModels', $parametry);
					if(!empty($modele))
					{
						foreach($modele as $model)
						{
							if($data['pojazd_model_id'] == $model->id)
							{
								$data['pojazd_model_name'] = $model->name;
							}
						}
					}
				}
			}

			if($data['pojazd_wersja_id'] != '-1' && $data['pojazd_typ'] == 'CAR')
			{
				$parametry = array
				(
					'make-id' => $data['pojazd_marka_id'],
					'model-id' => $data['pojazd_model_id'],
					'webapi-key' => $this->webapi,
					'country-code' => '1',
					'type' => $data['pojazd_typ']
				);
				$wersje = $this->klientOtomoto->__soapCall('getVersions', $parametry);
				if(!empty($wersje))
				{
					foreach($wersje as $wersja)
					{
						if($data['pojazd_wersja_id'] == $wersja->id)
						{
							$data['pojazd_wersja_name'] = $wersja->name;
						}
					}
				}
			}

			$parametry = array
			(
				'type' => $data['pojazd_typ'],
				'webapi-key' => $this->webapi
			);
			$typy_nadwozi = $this->klientOtomoto->__soapCall('getVehicleCategories', $parametry);
			if(!empty($typy_nadwozi))
			{
				foreach($typy_nadwozi as $typ_nadwozia)
				{
					if($data['pojazd_typ_nadwozia_key'] == $typ_nadwozia->key) 
					{
						$data['pojazd_typ_nadwozia_name'] = $typ_nadwozia->name;
					}
				}
			}

			/*
			if($typ_pojazdu == 'MOTORBIKE')
			{
				$poszukiwany_typ_pojazdu = 'MOTOCYKLE';
			}
			else
			{
				$poszukiwany_typ_pojazdu = 'SAMOCHODY';
			}
			*/
			
			$parametry = array
			(
				'webapi-key' => $this->webapi
			);
			$kategorie_allegro = $this->klientOtomoto->__soapCall('getAllegroCategories', $parametry);
			if(!empty($kategorie_allegro))
			{
				foreach($kategorie_allegro as $kategoria_I)
				{
					foreach($kategoria_I->children as $kategoria_II)
					{
						/*
						if(strtoupper($kategoria_II->name) == $poszukiwany_typ_pojazdu)
						{
							$data['pojazd_kategoria_allegro_name'] = $kategoria_I->name;
						}
						*/
						foreach($kategoria_II->children as $kategoria_III)
						{
							if($kategoria_III->id == $data['pojazd_kategoria_allegro_id'])
							{
								$data['pojazd_kategoria_allegro_name'] = $kategoria_III->name;
								//np: osobowe, ciężarowe, dostawcze
							}
							foreach($kategoria_III->children as $kategoria_IV)
							{
								if($kategoria_IV->id == $data['pojazd_kategoria_allegro_II_id'])
								{
									$data['pojazd_kategoria_allegro_II_name'] = $kategoria_IV->name; 
									//$kategoria_III->children to np: opel
								}
								foreach($kategoria_IV->children as $kategoria_V)
								{
									if($kategoria_V->id == $data['pojazd_kategoria_allegro_III_id'])
									{
										$data['pojazd_kategoria_allegro_III_name'] = $kategoria_V->name; 
										//$kategoria_IV->children to np: astra
									}
									foreach($kategoria_V->children as $kategoria_VI) {
										if($kategoria_VI->id == $data['pojazd_kategoria_allegro_IV_id'])
										{
											$data['pojazd_kategoria_allegro_IV_name'] = $kategoria_VI->name;  
											//$kategoria_IV->children to np: II
										}
									}
								}
							}
						}
					}
				}
			}


			if($data['pojazd_jedn_mocy'] == 'PS')
			{
				$data['pojazd_jedn_mocy_name'] = 'KM';
			}
			else if($data['pojazd_jedn_mocy'] == 'kW')
			{
				$data['pojazd_jedn_mocy_name'] = 'KW';
			}

			$parametry = array
			(
				'webapi-key' => $this->webapi,
				'type' => $data['pojazd_typ']
			);
			$rodzaje_paliw = $this->klientOtomoto->__soapCall('getFuelTypes', $parametry);
			if(!empty($rodzaje_paliw)) {
				foreach($rodzaje_paliw as $rodzaj_paliwa)
				{
					if($data['pojazd_rodzaj_paliwa_key'] == $rodzaj_paliwa->key)
					{
						$data['pojazd_rodzaj_paliwa_name'] = $rodzaj_paliwa->name;
					}
				}
			}

			$parametry = array
			(
				'webapi-key' => $this->webapi
			);
			$skrzynie_biegow = $this->klientOtomoto->__soapCall('getGearBoxTypes', $parametry);
			if(!empty($skrzynie_biegow))
			{
				foreach($skrzynie_biegow as $skrzynia_biegow)
				{
					if($data['pojazd_skrzynia_biegow_key'] == $skrzynia_biegow->key)
					{
						$data['pojazd_skrzynia_biegow_name'] = $skrzynia_biegow->name;
					}
				}
			}

			$kolory = $this->klientOtomoto->__soapCall('getColours', $parametry);
			if(!empty($kolory))
			{
				foreach($kolory as $kolor)
				{
					if($data['pojazd_kolor_key'] == $kolor->key)
					{
						$data['pojazd_kolor_name'] = $kolor->name;
					}
				}
			}

			if($data['pojazd_stan_techniczny'] != null && $data['pojazd_stan_techniczny'] != '-1')
			{
				if($data['pojazd_stan_techniczny'] == 'new')
				{
					$data['pojazd_stan_techniczny_name'] = 'nowy';
				}
				else if($data['pojazd_stan_techniczny'] == 'undamaged')
				{
					$data['pojazd_stan_techniczny_name'] = 'niezniszczony';
				}
				else if($data['pojazd_stan_techniczny'] == 'damaged') 
				{
					$data['pojazd_stan_techniczny_name'] = 'zniszczony';
				}
			}

			$stat_poj_sprow = $data['pojazd_stat_poj_sprow_id'];
			if($stat_poj_sprow == '-1') $data['pojazd_stat_poj_sprow_name'] = '-1';
			else if($stat_poj_sprow == '1') $data['pojazd_stat_poj_sprow_name'] = 'do sprowadzenia';
			else if($stat_poj_sprow == '2') $data['pojazd_stat_poj_sprow_name'] = 'sprowadzony/nieopłacony';
			else if($stat_poj_sprow == '3') $data['pojazd_stat_poj_sprow_name'] = 'przygotowany do rejestracji/opłacony';
			else if($stat_poj_sprow == '4') $data['pojazd_stat_poj_sprow_name'] = 'sprowadzony zarejestrowany';

			$kraje_pochodzenia = $this->klientOtomoto->__soapCall('getCountries', $parametry);
			if(!empty($kraje_pochodzenia))
			{
				foreach($kraje_pochodzenia as $kraj_pochodzenia)
				{
					if($data['pojazd_kraj_pochodzenia_id'] == $kraj_pochodzenia->id)
					{
						$data['pojazd_kraj_pochodzenia_name'] = $kraj_pochodzenia->name;
					}
				}
			}

			//lista krajów pochodzenia jest ta sama co krajów aktualnej rejestracji
			if(!empty($kraje_pochodzenia))
			{
				foreach($kraje_pochodzenia as $kraj_akt_rej)
				{
					if($data['pojazd_kraj_akt_rej_id'] == $kraj_akt_rej->id)
					{
						$data['pojazd_kraj_akt_rej_name'] = $kraj_akt_rej->name;
					}
				}
			}

			if(strtoupper($data['pojazd_typ']) == 'MOTORBIKE')
			{
				$parametry = array
				(
					'type' => $data['pojazd_typ'],
					'webapi-key' => $this->webapi
				);
				$rodzaje_napedow = $this->klientOtomoto->__soapCall('getTransmissionTypes', $parametry);
				if(!empty($rodzaje_napedow))
				{
					foreach($rodzaje_napedow as $rodzaj_napedu)
					{
						if($data['pojazd_rodzaj_napedu_key'] == $rodzaj_napedu->key)
						{
							$data['pojazd_rodzaj_napedu_name'] = $rodzaj_napedu->name;
						}
					}
				}
			}
		}
		else // gdy serwis jest niedostępny
		{
			//gdy serwis jest niedostępny i użytkownik wybrał pozycje "-- wybierz --", 
			//w innym przypadku zostawiamy starą wartość
			if($data['pojazd_marka_id'] == '-1') $data['pojazd_marka_name'] = null;
			if($data['pojazd_model_id'] == '-1') $data['pojazd_model_name'] = null;
			if($data['pojazd_wersja_id'] == '-1') $data['pojazd_wersja_name'] = null;
			if($data['pojazd_typ_nadwozia_key'] == '-1') $data['pojazd_typ_nadwozia_name'] = null;
			if($data['pojazd_kategoria_allegro_id'] == '-1') $data['pojazd_kategoria_allegro_name'] = null;
			if($data['pojazd_kategoria_allegro_II_id'] == '-1') $data['pojazd_kategoria_allegro_II_name'] = null;
			if($data['pojazd_kategoria_allegro_III_id'] == '-1') $data['pojazd_kategoria_allegro_III_name'] = null;
			if($data['pojazd_kategoria_allegro_IV_id'] == '-1') $data['pojazd_kategoria_allegro_IV_name'] = null;
			if($data['pojazd_jedn_mocy'] == '-1') $data['pojazd_jedn_mocy_name'] = null;
			if($data['pojazd_rodzaj_paliwa_key'] == '-1') $data['pojazd_rodzaj_paliwa_name'] = null;
			if($data['pojazd_skrzynia_biegow_key'] == '-1') $data['pojazd_skrzynia_biegow_name'] = null;
			if($data['pojazd_kolor_key'] == '-1') $data['pojazd_kolor_name'] = null;
			if($data['pojazd_stan_techniczny'] == '-1') $data['pojazd_stan_techniczny_name'] = null;
			if($data['pojazd_stat_poj_sprow_id'] == '-1') $data['pojazd_stat_poj_sprow_name'] = null;
			if($data['pojazd_kraj_pochodzenia_id'] == '-1') $data['pojazd_kraj_pochodzenia_name'] = null;
			if($data['pojazd_kraj_akt_rej_id'] == '-1') $data['pojazd_kraj_akt_rej_name'] = null;
			if($data['pojazd_rodzaj_napedu_key'] == '-1') $data['pojazd_rodzaj_napedu_name'] = null;
		}
	}

    public function zapisz_dod_info($id, &$data, &$data_dod_info)
	{
		// zapisywanie listy dodatkowych informacji - początek
		if($this->serwisNiedostepny === false)
		{
			$parametry = array
			(
				'type' => $data['pojazd_typ'],
				'webapi-key' => $this->webapi
			);
			$dodatkowe_info = $this->klientOtomoto->__soapCall('getVehicleFeatures', $parametry);
			$liczba_dod_info_wsz = count($dodatkowe_info);

			$dodInfoRealizacji = new RealizacjeDodInfo();
			$dodInfoRealizacji->id = $id;
			$dodInfoRealizacji->usunDodInfo();

			$liczba_dod_info = $this->liczba_wsz_dod_info;
			$temp_tab_dod_info = null;
			$tab_dod_info_oto = null;
			for($i=1; $i<=$liczba_dod_info; $i++)
			{
				for($j=0; $j<$liczba_dod_info_wsz; $j++)
				{
					if(($data_dod_info['pojazd_dod_info_key'.$i] != '-1') && ($data_dod_info['pojazd_dod_info_key'.$i] ==  $dodatkowe_info[$j]->key))
					{
						$temp_tab_dod_info['realizacje_id'] = $id;  //id realizacji
						$temp_tab_dod_info['pojazd_dod_info_key'] = $dodatkowe_info[$j]->key;
						$temp_tab_dod_info['pojazd_dod_info_name'] = $dodatkowe_info[$j]->name;
						$tab_dod_info_oto[$i]['key'] = $dodatkowe_info[$j]->key;
						$tab_dod_info_oto[$i]['name'] = $dodatkowe_info[$j]->name;
						$dodInfoRealizacji->dodajDodInfo($temp_tab_dod_info);
					}
				}
			}
			// zapisywanie listy dodatkowych informacji - koniec
			return $tab_dod_info_oto; //zwracamy listę dodatkwych informacji dla ogłoszenia otomoto
		}
		else
		{
			return false;
		}
}

	public function przygotuj_dod_info_oto(&$data, &$data_dod_info)
	{
		if($this->serwisNiedostepny === false)
		{
			$parametry = array
			(
				'type' => $data['pojazd_typ'],
				'webapi-key' => $this->webapi
			);
			$dodatkowe_info = $this->klientOtomoto->__soapCall('getVehicleFeatures', $parametry);
			$liczba_dod_info_wsz = count($dodatkowe_info);

			$liczba_dod_info = $this->liczba_wsz_dod_info;
			$tab_dod_info_oto = null;
			for($i=1; $i<=$liczba_dod_info; $i++)
			{
				for($j=0; $j<$liczba_dod_info_wsz; $j++)
				{
					if(@$data_dod_info[$i] == $dodatkowe_info[$j]->key)
					{
						$tab_dod_info_oto[$i]['key'] = $dodatkowe_info[$j]->key;
						$tab_dod_info_oto[$i]['name'] = $dodatkowe_info[$j]->name;
					}
				}
			}
			return $tab_dod_info_oto; //zwracamy listę dodatkowych informacji dla ogłoszenia otomoto
		}
		else
		{
			return false;
		}
	}

    public function zapisz_dod_wyposaz($id, &$data, &$data_dod_wyposaz)
	{
		// zapisywanie listy dodatkowego wyposażenia - początek
		if($this->serwisNiedostepny === false)
		{
			$parametry = array
			(
				'type' => $data['pojazd_typ'],
				'webapi-key' => $this->webapi
			);
			$dodatkowe_wyposaz = $this->klientOtomoto->__soapCall('getVehicleExtras', $parametry);
			$liczba_dod_wyposaz_wsz = count($dodatkowe_wyposaz);

			$dodWyposazRealizacji = new RealizacjeDodWyposaz();
			$dodWyposazRealizacji->id = $id;
			$dodWyposazRealizacji->usunDodWyposaz();

			$liczba_dod_wyposaz = $this->liczba_wsz_dod_wyposaz;
			$temp_tab_dod_wyposaz = null;
			$tab_dod_wyposaz_oto = null;
			for($i=1; $i<=$liczba_dod_wyposaz; $i++)
			{
				for($j=0; $j<$liczba_dod_wyposaz_wsz; $j++)
				{
					if(($data_dod_wyposaz['pojazd_dod_wyposaz_key'.$i] != '-1') && ($data_dod_wyposaz['pojazd_dod_wyposaz_key'.$i] ==  $dodatkowe_wyposaz[$j]->key))
					{
						$temp_tab_dod_wyposaz['realizacje_id'] = $id;  //id realizacji
						$temp_tab_dod_wyposaz['pojazd_dod_wyposaz_key'] = $dodatkowe_wyposaz[$j]->key;
						$temp_tab_dod_wyposaz['pojazd_dod_wyposaz_name'] = $dodatkowe_wyposaz[$j]->name;
						$tab_dod_wyposaz_oto[$i]['key'] = $dodatkowe_wyposaz[$j]->key;
						$tab_dod_wyposaz_oto[$i]['name'] = $dodatkowe_wyposaz[$j]->name;
						$dodWyposazRealizacji->dodajDodWyposaz($temp_tab_dod_wyposaz);
					}
				}
			}
			// zapisywanie listy dodatkowego wyposażenia - koniec
			return $tab_dod_wyposaz_oto; //zwracamy listę dodatkwego wyposażenia dla ogłoszenia otomoto
		}
		else
		{
			return false;
		}
	}

    public function przygotuj_dod_wyposaz_oto(&$data, &$data_dod_wyposaz)
	{
		if($this->serwisNiedostepny === false)
		{
			$parametry = array
			(
				'type' => $data['pojazd_typ'],
				'webapi-key' => $this->webapi
			);
			$dodatkowe_wyposaz = $this->klientOtomoto->__soapCall('getVehicleExtras', $parametry);
			$liczba_dod_wyposaz_wsz = count($dodatkowe_wyposaz);

			$liczba_dod_wyposaz = $this->liczba_wsz_dod_wyposaz;
			$tab_dod_wyposaz_oto = null;
			for($i=1; $i<=$liczba_dod_wyposaz; $i++)
			{
				for($j=0; $j<$liczba_dod_wyposaz_wsz; $j++)
				{
					if(@$data_dod_wyposaz[$i] == $dodatkowe_wyposaz[$j]->key)
					{
						$tab_dod_wyposaz_oto[$i]['key'] = $dodatkowe_wyposaz[$j]->key;
						$tab_dod_wyposaz_oto[$i]['name'] = $dodatkowe_wyposaz[$j]->name;
					}
				}
			}
			return $tab_dod_wyposaz_oto; //zwracamy listę dodatkwego wyposażenia dla ogłoszenia otomoto
		}
		else
		{
			return false;
		}
	}
	
	public function dodajDoOtoMoto($id)
	{
		$this->ImageDir = new Zend_Config_Ini('../application/config.ini', 'image');
		$galeria = new Galeria();
        $where = 'id = '.$id;
        $pojZdjecie = $galeria->fetchAll($where);
		if(count($pojZdjecie) > 0)
		{
			$typ_pliku = strchr($pojZdjecie[0]['img'], '.');
			$typ_pliku = substr($typ_pliku, 1);
			$sciezka = $this->ImageDir->ImageDir.$pojZdjecie[0]['img'];

			$plik = fopen($sciezka, 'r');
			$zdjecie = fread($plik, filesize($sciezka));
			fclose($plik);

			try
			{
				$parametry = array
				(
					'insertion' => array('id' => $this->id_ogloszenia_otomoto),
					'session-id' => $_SESSION['sidotomoto'],
					'webapi-key' => $this->webapi
				);
				$ogloszenie = $this->klientOtomoto->__soapCall('getInsertion', $parametry);
				$ogloszenie = get_object_vars($ogloszenie);
				$liczba_zdj_ogloszenia = $ogloszenie['photo-num'];
				$photo = array
				(
					'number' => $liczba_zdj_ogloszenia + 1,
					'body' => new SoapVar($zdjecie, XSD_BASE64BINARY),
					'link' => '',
					'body-content-type' => ''
				);
				$parametry = array
				(
					'insertion-id' => $this->id_ogloszenia_otomoto,
					'photo' => $photo,
					'session-id' => $_SESSION['sidotomoto'],
					'webapi-key' => $this->webapi
				);
				//var_dump($parametry);die();
				$re = $this->klientOtomoto->__soapCall('doPhotoEdit', $parametry);

				return true;
			}
			catch(Exception $e)
			{
				if(preg_match('/(internal error)/i', $e->getMessage()) == 1 
				&& strtolower($typ_pliku) != 'jpg' && strtolower($typ_pliku) != 'jpeg')
				{
					return OTOMOTO_BLAD_TYPU;
				}
				return false;
			}
		}
    }
}
?>