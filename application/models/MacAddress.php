<?php
class MacAddress {
    public $OS;
    private static $valid_mac = "([0-9A-F]{2}[:-]){5}([0-9A-F]{2})";

    private static $mac_address_vals = array(
        "0", "1", "2", "3", "4", "5", "6", "7",
        "8", "9", "A", "B", "C", "D", "E", "F"
     );

    public static function validateMacAddress($mac) {
        return (bool) preg_match("/^" . self::$valid_mac . "$/i", $mac);
    }
    
    public static function findMacAddress($ipconfig){
        preg_match("/" . self::$valid_mac . "/i", $ipconfig, $out);
        return isset($out[0]) ? $out[0] : false;
    }

    protected static function runCommand($command) {
        return shell_exec($command);
    }
    
    public static function checkOs($OS) {
        switch(strtolower($OS)) {
            case "linux" :
                return self::runCommand("netstat -ie 2>&1");//ifconfig -a
            break;
            case "solaris" :
            break;
            case "unix" :
            break;
            case "aix" :
            break;
            case "android":
            break;    
            default:
                return self::runCommand("ipconfig /all");
            break;
        }
        return false;
    }

    public static function getCurrentMacAddress($OS) {
        $ifconfig = self::checkOs($OS);
        $ifconfig = self::findMacAddress($ifconfig);
        if(strlen($ifconfig) > 0) {
           return trim(strtoupper($ifconfig));
        }
        return "";
    }
}
?>