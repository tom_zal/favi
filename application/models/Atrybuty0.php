<?php
class Atrybuty extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		
		$this->typatr = array
		(
			array('nazwa' => 'pole tekstowe', 'typatrybut' => 'tekst'),
			array('nazwa' => 'pole wyboru TAK / NIE', 'typatrybut' => 'wybor'),
			array('nazwa' => 'pole jednokrotnego wyboru', 'typatrybut' => 'lista'),
			array('nazwa' => 'pola wielokrotnego wyboru', 'typatrybut' => 'multi')
		);
		$this->typpola = array('tekst' => 'Tekst', 'int' => 'Liczba całkowita', 'float' => 'Liczba rzeczywista');
    }

	function _save($data)
	{
		$this->insert($data);
		$this->cache->remove('cache_'.$this->cachename.'_'.$this->lang);
		return $this->getAdapter()->lastInsertId();
	}

	function _update($data)
	{
		$this->update($data, 'id = '.$this->id);
		$this->cache->remove('cache_'.$this->cachename.'_'.$this->lang);
	}

	function _delete()
	{
		$this->delete('id = '.$this->id);
		$this->cache->remove('cache_'.$this->cachename.'_'.$this->lang);
	}
	function _deleteGroup()
	{
		$this->delete('id_gr = '.$this->id);
		$this->cache->remove('cache_'.$this->cachename.'_'.$this->lang);
	}

	function getRow()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function getRows($id = 0)
	{
		$where = 'lang = "'.$this->lang.'" and (id_prod = 0 or id_prod = '.$id.')';
        $result = $this->fetchAll($where, 'nazwa ASC');
		return $result;
	}
	function getRowsSearch($id = 0)
	{
		$where = 'lang = "'.$this->lang.'" and (id_prod = 0 or id_prod = '.$id.')';
		$result = $this->fetchAll($where.' AND wyszukiwarka = "1" AND status = "1"', 'nazwa ASC');
		return $result;
	}
	function getRowsGroup($id = 0)
	{
		$where = 'lang = "'.$this->lang.'" and (id_prod = 0 or id_prod = '.$id.')';
		$result = $this->fetchAll($where.' AND id_gr = "'.$this->id.'"', 'nazwa ASC');
		return $result;
	}
	function getRowsGroupFr($id = 0)
	{
		$where = 'lang = "'.$this->lang.'" and (id_prod = 0 or id_prod = '.$id.')';
		$result = $this->fetchAll($where, 'nazwa ASC');
		return $result;
	}
	
	function getAtrybuty($ids = 1)
	{
		if(!$ids) return null;
		$where = '1';
		if(is_array($ids)) $where = 'at.id in ('.implode(',',$ids).')';
		$select = $this->db->select()->from(array('at' => 'Atrybuty'), array('id', 'id_gr', 'nazwa as wartosc'))
			->joinleft(array('ag' => 'Atrybutygrupy'), 'at.id_gr = ag.id', array('nazwa','typatrybut','jedn_miary'))
			->where($where)->order(array('ag.nazwa asc', 'at.nazwa asc'));
		//echo $select;return null;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	
	function getAtrybutyAll($id = 0, $produkt, $typ_klienta)
	{
		$ObjectA = new Atrybuty();
		$ObjectA->lang = $this->lang;
		$ObjectG = new Atrybutygrupy();
		$ObjectG->lang = $this->lang;
		$ObjectAp = new Atrybutypowiazania();
		$ObjectAp->id = $id;
		$this->grupy = $ObjectG->getRows($id)->toArray();
		$this->atrybuty = $ObjectA->getRows($id)->toArray();
		$this->atrybutyuniq = $ObjectAp->getRowsUnique();

		$array = array();
		$array2 = array();

		if(!empty($this->atrybutyuniq))
		foreach($this->atrybutyuniq as $one)
		{
			$array[] = $one->id_at;
			if(isset($one['id_gr']) && $one['id_gr'] > 0) $array2[$one['id_gr']] = $one['wartosc'];
			$atrybutyuniq[$one['id_gr']][$one['id_at']] = $one->toArray();
		}
		$this->atrybutyuniq = $array;
		$this->atrybutyuniq2 = $array2;

		$array = array();
		$arrat = array();
		$g = 0;
		//var_dump($this->atrybuty);

		if(!empty($this->grupy))
		foreach($this->grupy as $grupa) 
		{
			$a = 0;
			//var_dump($grupa);
			$array[$g]['id'] = $grupa['id'];
			$array[$g]['grupa'] = $grupa['nazwa'];
			$array[$g]['typatrybut'] = $grupa['typatrybut'];
			$array[$g]['jedn_miary'] = $grupa['jedn_miary'];
			$array[$g]['jedn_miary_typ'] = $grupa['jedn_miary_typ'];
			if(!empty($this->atrybuty)) 
			{
				$atrybuty = null;
				foreach($this->atrybuty as $atrybut) 
				{
					if($grupa['id'] == $atrybut['id_gr']) 
					{								
						if(is_array($this->atrybutyuniq) && in_array($atrybut['id'], $this->atrybutyuniq)) 
						{
							$atrybut['dane'] = @$atrybutyuniq[$atrybut['id_gr']][$atrybut['id']];
							if(@$produkt['promocja'])
							{
								if($typ_klienta)
								{
									$brutto = $atrybut['dane']['cena_promocji_b_hurt'];
									$netto = $atrybut['dane']['cena_promocji_n_hurt'];
								}
								else
								{
									$brutto = $atrybut['dane']['cena_promocji_b'];
									$netto = $atrybut['dane']['cena_promocji_n'];
								}
							}
							else
							{
								if($typ_klienta)
								{
									$brutto = $atrybut['dane']['cena_brutto_hurt'];
									$netto = $atrybut['dane']['cena_netto_hurt'];
								}
								else
								{
									$brutto = $atrybut['dane']['cena_brutto'];
									$netto = $atrybut['dane']['cena_netto'];
								}
							}
							$atrybut['dane']['brutto'] = $brutto;
							$atrybut['dane']['netto'] = $netto;
							$atrybuty[] = $atrybut;
							//var_dump($array[$g]);
						}								
					}
				}
				if(!strcmp("lista", $grupa['typatrybut']))
				$array[$g]['atrybut'] = @$atrybuty[0];
				if(!strcmp("multi", $grupa['typatrybut']))
				$array[$g]['atrybut'] = $atrybuty;
			}
			if(!strcmp("tekst", $grupa['typatrybut'])) 
			{
				if(isset($this->atrybutyuniq2[$grupa['id']]) && !empty($this->atrybutyuniq2[$grupa['id']]))
				{
					$array[$g]['wartosc'] = $this->atrybutyuniq2[$grupa['id']];
				}
			}
			//var_dump($array[$g]);
			$g++;
		}
		$arrat = is_array($arrat) ? array_merge($arrat) : null;
		//var_dump($array);die();
		return $array;
	}
	
	function getAtrybutyAllForIds($ids = null)
	{
		if(!is_array($ids)) return null;
		$where = 'p.id in ('.implode(',', $ids).') and atp.id > 0';
		$select = $this->db->select()->distinct()
				->from(array('p' => 'Produkty'), array('id as id_prod'))
				->joinleft(array('atp' => 'Atrybutypowiazania'), 'p.id = atp.id_og', array('id as id_atp','wartosc'))
				->joinleft(array('at' => 'Atrybuty'), 'atp.id_at = at.id', array('id as id_at', 'nazwa as opcja'))
				->joinleft(array('atg' => 'Atrybutygrupy'),	'if(atp.id_gr>0,atp.id_gr,at.id_gr) = atg.id',
				array('id as id_gr', 'nazwa as atrybut', 'typatrybut', 'jedn_miary', 'jedn_miary_typ'))
				->where($where)->order(array('atg.poz asc', 'at.nazwa asc'));
		//echo $select;
		$result = $this->db->fetchAll($select);
		for ($i = 0; $i < count($result); $i++)
		{
			$res = $result[$i];
			if($res['typatrybut'] == 'multi')// || !empty($res['id_at']))
			{
				if(!isset($results[$res['id_prod']][$res['id_gr']]))
				{
					$resAll = $res;
					unset($resAll['id_at']);
					unset($resAll['opcja']);
					$results[$res['id_prod']][$res['id_gr']] = $resAll;
				}
				$resOne['id_at'] = $res['id_at'];
				$resOne['opcja'] = $res['opcja'];
				$results[$res['id_prod']][$res['id_gr']][$res['id_at']] = $resOne;
			}
			else $results[$res['id_prod']][$res['id_gr']] = $res;
		}
		return @$results;
	}
	
	function getAtrybutOpcje($id = 0, $kat = 0, $sort = '')
	{
		$where = 'atg.id = '.$id.' and at.id_gr = '.$id.' and atp.id_gr = '.$id;
		if($id > 0) $where .= ' and id_kat = '.$kat;
		$select = $this->db->select()->distinct()
				->from(array('at' => 'Atrybuty'), array('id as id_atr', 'nazwa as opcja'))
				->join(array('atg' => 'Atrybutygrupy'),	'at.id_gr = atg.id',
				array('id as id_gr', 'nazwa as atrybut', 'typatrybut', 'jedn_miary', 'jedn_miary_typ'))
				->join(array('atp' => 'Atrybutypowiazania'), 'at.id = atp.id_at and at.id_gr = atp.id_gr', 
				array('id as id_atp','wartosc'))
				->join(array('p' => 'Produkty'), 'atp.id_og = p.id', array('id as id_prod', 'nazwa as produkt'))
				->joinleft(array('kp' => 'Katprod'), 'kp.id_prod = p.id and kp.typ = "kategorie"', array('id_kat_prod'))
				->where($where)->group('at.id')->order(array('at.nazwa asc'));
		//echo $select;
		$result = $this->db->fetchAll($select);
		if(!empty($sort)) return $this->common->sortByPole($result, $sort);
		else return $result;
	}
}    
?>