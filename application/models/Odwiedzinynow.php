<?php
class Odwiedzinynow extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		
		$this->params = $this->common->getParams();
    }
	
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	
	function dodajOdwiedziny($dane)
	{
		if(!isset($dane['typ'])) $dane['typ'] = 'produkty';
		$where = 'id_prod = '.$dane['id_prod'].' and data = "'.$dane['data'].'" and typ = "'.$dane['typ'].'"';
		$sql = $this->select()->where($where);
		$result = $this->fetchAll($sql);
		if($result == null || count($result) == 0)
		{
			$dane['odwiedziny'] = 1;
			$this->insert($dane);
			$id = $this->getAdapter()->lastInsertId();
			return $id;
		}
		else
		{
			$dane['odwiedziny'] = $result[0]['odwiedziny'] + 1;
			$this->id = $result[0]['id'];
			$this->edytuj($dane);
			return 0;
		}
	}
	function dodajOdwiedzinyIds($ids, $data, $typ)
	{
		if(!$this->common->isArray($ids, true)) return null;
		$ids = implode(',', $ids);
		
		$sql = 'update Odwiedziny set odwiedziny = odwiedziny + 1 where ';
		$sql.= 'id_prod in ('.$ids.') and typ = "'.$typ.'" and data = "'.$data.'"';
		//echo $sql;die();
        $this->db->query($sql);
		
		$sql = 'insert into Odwiedziny (id_prod, data, odwiedziny, typ) ';
		$sql.= 'Select p.id, "'.$data.'", 1, "'.$typ.'" ';
		$sql.= 'from Produkty p where p.id in ('.$ids.') and p.id not in ';
		$sql.= '(Select o.id_prod from Odwiedziny o where ';
		$sql.= 'id_prod in ('.$ids.') and typ = "'.$typ.'" and data = "'.$data.'")';
		//echo $sql;die();
        $this->db->query($sql);
		
		if($this->obConfig->caching) $this->common->cacheRemove($sql, 'Odwiedziny');
	}
	
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	
	function wypisz()
	{
		$result = $this->fetchAll();
		return $result;
	}
	
	function wypiszAktualnie($id = 0, $typ = 'produkty')
	{
		$where = 'id_prod = '.$id.' and typ = "'.$typ.'" and data >= "'.date('Y-m-d H:i:s', time() - 60 * 10).'"';
		$sql = $this->select()->from(array('o' => 'Odwiedzinynow'), array('count(*)'))->where($where);
		$result = $this->fetchAll($sql);
		return @intval($result[0]['count(*)']);
	}
	
	function wypiszForProdukt($id = 0, $typ = 'produkty')
	{
		$where = 'id_prod = '.$id.' and typ = "'.$typ.'"';
		$sql = $this->select()->where($where)->order('data desc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForProduktMiesiace($id, $rok, $typ = 'produkty')
	{
		$where = 'id_prod = '.$id.' and data >= "'.$rok.'-01-01" and data <= "'.$rok.'-12-31" and typ = "'.$typ.'"';
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), array('odwiedziny', 'EXTRACT(MONTH FROM data) as czas'))
			->where($where)->order('data asc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForProduktLata($id, $typ = 'produkty')
	{
		$where = 'id_prod = '.$id.' and typ = "'.$typ.'"';
		$sql = $this->select()->from(array('o' => 'Odwiedziny'), 
			'EXTRACT(YEAR FROM data) as czas, sum(odwiedziny) as odwiedziny')
			->where($where)->group('czas')->order('data asc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszLataForProdukt($id, $typ = 'produkty')
	{
		$where = 'id_prod = '.$id.' and typ = "'.$typ.'"';
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), 'EXTRACT(YEAR FROM data) as czas')
			->where($where)->group('czas')->order('data asc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	
	function wypiszForKategoria($id = 0)
	{
		$where = 'id_kat = '.$id.' and typ = "produkty"';
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), '')
			->join(array('kp' => 'Katprod'), 'o.id_prod = kp.id_prod', array(''))
			->where($where)->order('data desc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForKategoriaMiesiace($id, $rok)
	{
		$where = 'id_kat = '.$id.' and data >= "'.$rok.'-01-01" and data <= "'.$rok.'-12-31" and typ = "produkty"';
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), 'odwiedziny, EXTRACT(MONTH FROM data) as czas')
			->join(array('kp' => 'Katprod'), 'o.id_prod = kp.id_prod', array(''))
			->where($where)->group('czas')->order('data asc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForProducentMiesiace($id, $rok)
	{
		$where = 'pp.id = '.$id.' and o.data >= "'.$rok.'-01-01" and o.data <= "'.$rok.'-12-31" and o.typ = "produkty"';
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), 'o.odwiedziny, EXTRACT(MONTH FROM o.data) as czas')
			->join(array('p' => 'Produkty'), 'o.id_prod = p.id', array(''))
			->join(array('pp' => 'Producent'), 'pp.id = p.producent', array(''))
			->where($where)->group('czas')->order('o.data asc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForKategoriaMiesiace2($id, $rok)
	{
		$where = 'id_prod = '.$id.' and data >= "'.$rok.'-01-01" and data <= "'.$rok.'-12-31" and typ = "kategorie"';
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), 'odwiedziny, EXTRACT(MONTH FROM data) as czas')
			->where($where)->group('czas')->order('data asc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForProducentMiesiace2($id, $rok)
	{
		$where = 'id_prod = '.$id.' and data >= "'.$rok.'-01-01" and data <= "'.$rok.'-12-31" and typ = "producenci"';
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), 'odwiedziny, EXTRACT(MONTH FROM data) as czas')
			->where($where)->group('czas')->order('data asc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForKategoriaLata($id)
	{
		$where = 'id_kat = '.$id.' and typ = "produkty"';
		$sql = $this->select()->from(array('o' => 'Odwiedziny'), 
			'EXTRACT(YEAR FROM data) as czas, sum(odwiedziny) as odwiedziny')
			->join(array('kp' => 'Katprod'), 'o.id_prod = kp.id_prod', array(''))
			->where($where)->group('czas')->order('data asc');
		//echo $sql;
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForProducentLata($id)
	{
		$where = 'pp.id = '.$id.' and o.typ = "producenci"';
		$sql = $this->select()->from(array('o' => 'Odwiedziny'), 
			'EXTRACT(YEAR FROM o.data) as czas, sum(o.odwiedziny) as odwiedziny')
			->join(array('p' => 'Produkty'), 'o.id_prod = p.id', array(''))
			->join(array('pp' => 'Producent'), 'pp.id = p.producent', array(''))
			->where($where)->group('czas')->order('o.data asc');
		//echo $sql;
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForKategoriaLata2($id)
	{
		$where = 'id_prod = '.$id.' and typ = "kategorie"';
		$sql = $this->select()->from(array('o' => 'Odwiedziny'), 
			'EXTRACT(YEAR FROM data) as czas, sum(odwiedziny) as odwiedziny')
			->where($where)->group('czas')->order('data asc');
		//echo $sql;
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForProducentLata2($id)
	{
		$where = 'id_prod = '.$id.' and typ = "producenci"';
		$sql = $this->select()->from(array('o' => 'Odwiedziny'), 
			'EXTRACT(YEAR FROM data) as czas, sum(odwiedziny) as odwiedziny')
			->where($where)->group('czas')->order('data asc');
		//echo $sql;
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszLataForKategoria($id)
	{
		$where = '1 and typ = "produkty"'; if($id != 'all') $where .= ' and id_kat = '.$id;
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), 'EXTRACT(YEAR FROM data) as czas')
			->join(array('kp' => 'Katprod'), 'o.id_prod = kp.id_prod', array(''))
			->where($where)->group('czas')->order('data asc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszLataForKategoria2($id)
	{
		$where = '1 and typ = "kategorie"'; if($id != 'all') $where .= ' and id_prod = '.$id;
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), 'EXTRACT(YEAR FROM data) as czas')
			->where($where)->group('czas')->order('data asc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszLataForRodzic($id)
	{
		$where = '1 and typ = "produkty"'; if($id != 'all') $where .= ' and rodzic = '.$id;
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), 'EXTRACT(YEAR FROM data) as czas')
			->join(array('kp' => 'Katprod'), 'o.id_prod = kp.id_prod', array(''))
			->join(array('k' => 'Kategorie'), 'kp.id_kat = k.id', array(''))
			->where($where)->group('czas')->order('data asc');
		//echo $sql;return;
		$result = $this->fetchAll($sql);
		return $result;
	}
	
	function wypiszForAll($id = 0)
	{
		$where = '1 and typ = "produkty"';
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), '')->where($where)->order('data desc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForAllMiesiace($rok, $typ = 'produkty')
	{
		$where = '1 and data >= "'.$rok.'-01-01" and data <= "'.$rok.'-12-31" and typ = "'.$typ.'"';
		$sql = $this->select()->from(array('o' => 'Odwiedziny'), 
			'EXTRACT(MONTH FROM data) as czas, sum(odwiedziny) as odwiedziny')
			->where($where)->group('czas')->order('data asc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForAllLata($typ = 'produkty')
	{
		$where = '1 and typ = "'.$typ.'"';
		$sql = $this->select()->from(array('o' => 'Odwiedziny'), 
			'EXTRACT(YEAR FROM data) as czas, sum(odwiedziny) as odwiedziny')
			->where($where)->group('czas')->order('data asc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszLataForAll($typ = 'produkty')
	{
		$where = '1';// and typ = "'.$typ.'"';
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), 'EXTRACT(YEAR FROM data) as czas')
			->where($where)->group('czas')->order('data asc');
		$result = $this->fetchAll($sql);
		return $result;
	}
	
	function wypiszForKategoriaSumaMiesiace($id, $dataOd, $dataDo)
	{
		$where = 'data >= "'.$dataOd.'" and data <= "'.$dataDo.'" and typ = "produkty"';
		if($id != 'all') $where .= ' and rodzic = '.$id;
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), '*, sum(odwiedziny) as odwiedziny')
			->join(array('kp' => 'Katprod'), 'o.id_prod = kp.id_prod', array(''))
			->join(array('k' => 'Kategorie'), 'kp.id_kat = k.id', array(''))
			->where($where)->group('id_kat')->order(array('odwiedziny desc', 'rodzic asc'));
		//echo $sql;return;
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForKategoriaSumaMiesiace2($id, $dataOd, $dataDo)
	{
		$where = 'data >= "'.$dataOd.'" and data <= "'.$dataDo.'" and typ = "kategorie"';
		if($id != 'all') $where .= ' and rodzic = '.$id;
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), '*, sum(odwiedziny) as odwiedziny')
			->join(array('k' => 'Kategorie'), 'k.id = o.id_prod', array(''))
			->where($where)->group('id_prod')->order(array('odwiedziny desc', 'rodzic asc'));
		//echo $sql;return;
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForProducentSumaMiesiace($id, $dataOd, $dataDo)
	{
		$where = 'o.data >= "'.$dataOd.'" and o.data <= "'.$dataDo.'" and o.typ = "produkty"';
		if($id > 0) $where .= ' and pp.id = '.$id;
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), '*, sum(o.odwiedziny) as odwiedziny')
			->join(array('p' => 'Produkty'), 'p.id = o.id_prod', array(''))
			->join(array('pp' => 'Producent'), 'pp.id = p.producent', array(''))
			->where($where)->group('producent')->order(array('o.odwiedziny desc', 'pp.id asc'));
		//echo $sql;//return;
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForProducentSumaMiesiace2($id, $dataOd, $dataDo)
	{
		$where = 'o.data >= "'.$dataOd.'" and o.data <= "'.$dataDo.'" and o.typ = "producenci"';
		if($id > 0) $where .= ' and pp.id = '.$id;
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), '*, sum(o.odwiedziny) as odwiedziny')
			->join(array('pp' => 'Producent'), 'pp.id = o.id_prod', array(''))
			->where($where)->group('id_prod')->order(array('o.odwiedziny desc', 'pp.id asc'));
		//echo $sql;//return;
		$result = $this->fetchAll($sql);
		return $result;
	}
	
	function wypiszForKategoriaSumaLata($id)
	{
		$where = '1 and typ = "produkty"'; if($id != 'all') $where .= ' and rodzic = '.$id;
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), '*, sum(odwiedziny) as odwiedziny')
			->join(array('kp' => 'Katprod'), 'o.id_prod = kp.id_prod', array(''))
			->join(array('k' => 'Kategorie'), 'kp.id_kat = k.id', array(''))
			->where($where)->group('id_kat')->order(array('odwiedziny desc', 'rodzic asc'));
		//echo $sql;
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForKategoriaSumaLata2($id)
	{
		$where = '1 and typ = "kategorie"'; if($id != 'all') $where .= ' and rodzic = '.$id;
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), '*, sum(odwiedziny) as odwiedziny')
			->join(array('k' => 'Kategorie'), 'k.id = o.id_prod', array(''))
			->where($where)->group('id_prod')->order(array('odwiedziny desc', 'rodzic asc'));
		//echo $sql;
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForProducentSumaLata($id)
	{
		$where = '1 and o.typ = "produkty"'; if($id > 0) $where .= ' and producent = '.$id;
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), '*, sum(o.odwiedziny) as odwiedziny')
			->join(array('p' => 'Produkty'), 'p.id = o.id_prod', array(''))
			->join(array('pp' => 'Producent'), 'pp.id = p.producent', array(''))
			->where($where)->group('producent')->order(array('o.odwiedziny desc', 'pp.id asc'));
		//echo $sql;
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszForProducentSumaLata2($id)
	{
		$where = '1 and o.typ = "producenci"'; if($id > 0) $where .= ' and producent = '.$id;
		$sql = $this->select()
			->from(array('o' => 'Odwiedziny'), '*, sum(o.odwiedziny) as odwiedziny')
			->join(array('pp' => 'Producent'), 'pp.id = o.id_prod', array(''))
			->where($where)->group('id_prod')->order(array('o.odwiedziny desc', 'pp.id asc'));
		//echo $sql;
		$result = $this->fetchAll($sql);
		return $result;
	}
	
	function usun()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	
	function wypiszJeden()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
}
?>