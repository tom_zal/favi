<?php
class Query extends Zend_Db_Table
{
	public $id, $name;
	public $hashcode = array();
	public $normalcode = array();

	public function __construct($name, $module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		
		$this->_name = $name;
		$this->hashcode = array('a','$','u','k','o','L','%','r','t',')','*');
		$this->normalcode = array(0,1,2,3,4,5,6,7,8,9);
	}
	function _save($data)
	{
		$this->insert($data);
		return $this->getAdapter()->lastInsertId();
	}
	function _update($data)
	{
		$this->update($data, 'id = '.$this->id);
	}
	function _updateValue($data)
	{
		$this->update($data, $this->column.' = "'.$this->value.'"');
	}
	function _updateValueRange($data)
	{
		$this->update($data, $this->column.' > "'.$this->value.'"');
	}
	function getRowsRange()
	{
		$where = $this->column.'>="'.$this->value.'"'.( $this->valuemax ? ' AND '.$this->column .'<="'.$this->valuemax.'"': '').''.( isset($this->id) ? ' AND '.$this->column2.'="'.$this->id.'"': '').''.( isset($this->column3) ? ' AND '.$this->column3.'="'.$this->value3.'"': '');
		return $this->fetchAll($where, isset($this->order) ? $this->order : 'id')->toArray();
	}
	function getRowsRangeIn()
	{
		$sql = $this->select()->from($this->_name, '*')
				->where($this->column.'>="'.$this->value.'"'.( $this->valuemax ? ' AND '.$this->column .'<="'.$this->valuemax.'"': '').''.( isset($this->id) ? ' AND '.$this->column2.'="'.$this->id.'"': '').''.( isset($this->column3) ? ' AND '.$this->column3.'="'.$this->value3.'"': ''))
				->setIntegrityCheck(false)
				->where($this->columnin.' IN (?)', $this->in)
				->order($this->order.' DESC');

		if(isset($this->group)) $sql->group($this->group);
		
		$result = $this->fetchAll($sql)->toArray();
		return $result;
	}
	function getRowsRangeInSum()
	{
		$result = $this->fetchRow(
			$this->select()->from($this->_name, 'SUM('.$this->columnsum.') AS sum')
				->where($this->column.'>="'.$this->value.'"'.( $this->valuemax ? ' AND '.$this->column .'<="'.$this->valuemax.'"': '').''.( isset($this->id) ? ' AND '.$this->column2.'="'.$this->id.'"': '').''.( isset($this->column3) ? ' AND '.$this->column3.'="'.$this->value3.'"': ''))
				->setIntegrityCheck(false)
				->where($this->columnin.' IN (?)', $this->in)
				->order($this->order.' DESC')
		)->toArray();
		return $result;
	}
	function _delete()
	{
		$this->delete('id = '.$this->id);
	}
	function _deleteUnion($column)
	{
		$this->delete($column.' = '.$this->id);
	}
	function getRow()
	{
		$result = $this->fetchRow('id = "'.$this->id.'"');
		return $result;
	}
	function getRowValue()
	{
		$result = $this->fetchRow($this->column.' = "'.$this->value.'"');
		return $result;
	}
	function getRowsUnion($column, $where = null)
	{
		return $this->fetchAll($column.' = '.$this->id.''.$where);
	}
	function getRows($order='id', $sequence='DESC')
	{
		return $this->fetchAll(null, $order.' '.$sequence);
	}
	function getRowsUnique()
	{
		$result = $this->fetchAll($this->column.'="'.$this->value.'"')->toArray();
		return $result;
	}
	function getRowsFieldCount()
	{
		$result['num'] = 0;
		$result = $this->fetchRow(
			$this->select()->from($this->_name,'COUNT(*) AS num')->where($this->where)
		)->toArray();
		return $result['num'];
	}
	function getRowsFieldSum()
	{
		$result['sum'] = 0.00;
		$result = $this->fetchRow(
			$this->select()->from($this->_name,'SUM('.$this->column.') AS sum')->where($this->where)
		)->toArray();
		return $result['sum'] == null ? 0.00 : $result['sum'];
	}
	function getRowsField()
	{
		$result = $this->fetchAll(
			$this->select()->from($this->_name, '*')->where($this->where)->order($this->order.' DESC')->limitPage($this->page, $this->rowcount)
		)->toArray();
		return $result;
	}
	function getRowsFieldUnion()
	{
		$sql1 = $this->select()->setIntegrityCheck(false)->from(array('t1' => $this->_name1))->where($this->where1);
		$sql2 = $this->select()->setIntegrityCheck(false)->from(array('t2' => $this->_name2))->where($this->where2);
		$sql = $this->select()->union(array($sql1, $sql2))->order($this->order.' DESC')
							->limitPage($this->page, $this->rowcount);
		//echo $sql;
		$result = $sql->query()->fetchAll();
		return $result;
	}
	function getRowsFieldCountJoin()
	{
		$result['num'] = 0;
		$result = $this->fetchRow(
			$this->select()->from(array('a'=>$this->_name),'COUNT(*) AS num')->where($this->where)
		)->toArray();
		return $result['num'];
	}
	function getRowsFieldJoin()
	{
		$result = $this->fetchAll(
			$this->select()->from(array('a'=>$this->_name), '*')
				->setIntegrityCheck(false)
				->join(array('b'=>$this->jointable), 'b.'.$this->joinfieldb.'= a.'.$this->joinfielda,$this->joinel)
				->where($this->where)->order($this->order.' DESC')->limitPage($this->page, $this->rowcount)
		)->toArray();
		return $result;
	}
	function getRowsValue()
	{
		$result = $this->fetchAll($this->column.'="'.$this->value.'"',$this->order)->toArray();
		return $result;
	}
	function getRowValueMulti()
	{
		$where = '1';
		if(is_array($this->column))
		{
			foreach($this->column as $col=>$val)
			{
				$where .= ' AND '.$col.'="'.$val.'"';
			}
		}
		$result = $this->fetchRow($where,$this->order);
		return $result;
	}
	function getRowsValueMulti()
	{
		$where = '1';
		if(is_array($this->column))
		{
			foreach($this->column as $col=>$val)
			{
				$where .= ' AND '.$col.'="'.$val.'"';
			}
		}
		$result = $this->fetchAll($where,$this->order)->toArray();
		return $result;
	}
	function getRowsPager($count, $offset, $order = 't.id', $sequence = 'ASC', $where = true)
	{
		return $this->getAdapter()->fetchAll
		(
			'SELECT `t`.*, 
			(
				SELECT DISTINCT COUNT(`tc`.`id`)
				FROM '.$this->_name.' AS `tc`
				WHERE '.$where.'
				ORDER BY `tc`.`id`
				LIMIT 1
			) as `pager`
			FROM '.$this->_name.' AS `t`
			WHERE '.$where.'
			ORDER BY '.$order.' '.$sequence.'
			LIMIT '.$offset .', '.$count
		);
	}
	
	static function setIds($rows, $key = 'id')
	{
		$array = array();
		if(!empty($rows))
		{
			foreach($rows as $r)
			{
				$array[] = $r[$key];
			}
		}
		return $array;
	}
        
	static function sortById($rows, $key = 'id')
	{
		$array = array();
		if(!empty($rows))
		{
			foreach($rows as $r)
			{
				$array[$r[$key]] = $r;
			}
		}
		return $array;
	}
        
	public static function dump($var)
	{
		echo '<pre>';
		echo print_r($var);
		echo '</pre>';
	}
	
	static function getRandChar($size)
	{
		$acceptedChars = 'azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN0123456789';
		$max = strlen($acceptedChars)-1;
		$rand = null;
		for($i=0; $i < $size; $i++) $rand .= $acceptedChars{mt_rand(0, $max)};
		return $rand;
	}
	
	static function checkRandChar($size = 8)
	{
		$ob = new Kodyrabatowe();
		$codes = $ob->getRowsCode();
		$code = null;
		do
		{
			$code = self::getRandChar($size);
		}
		while(isset($codes[$code]));
		return $code;
	}

	function _upload($file, $path, $type='img', $width = null, $height = null, $resize = true, $ratio = true)
	{
		$img = null;
		$handle = new upload($file);
		$IMG = md5(date('h-i-s, j-m-y').''.$file['size']);

		if($handle->uploaded)
		{
			$handle->file_new_name_body = $IMG;
			$handle->file_overwrite = true;
			$handle->auto_create_dir = true;
			$handle->dir_chmod = 0777;
			if($type == 'img' && !empty($width) && !empty($height))
			{
				$handle->resize = $resize;
				$handle->ratio = $ratio;
				$handle->image_resize = $resize;
				$handle->image_ratio = $ratio;
				$handle->image_y = $width;
				$handle->image_x = $height; 
			}
			$handle->process($path);
			if($handle->processed)
			{
				$handle->clean();
				$this->image = $handle->file_dst_name;
			}
		}
		return $this->image;
	}

	public function _sort($pos, $id)
	{
		$this->update(array('pozycja'=>$pos), 'id="'.$id.'"');
	}
	
	public static function doPdf($dane, $pozx = 100, $pozy = 100, $pozxjump = 100, $pozyjump = 100, $page = 1, $elementwiersz = 1, $path='admin/test/', $name='test.pdf', $file = 'admin/test/test.png', $width = 420, $height = 756, $twoline = false)
	{
		$save = true;
		$ob = new Zend_Pdf();

		$listWidth = $width;
		$listHeight = $height;

		$font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_TIMES_BOLD);
		//$font = Zend_Pdf_Font::fontWithPath('fonts/times.ttf');

		$barcodeLeft = 0;
		$barcodeTop = 0;
		$barcodeWidth = $listWidth;
		$barcodeHeight = $listHeight;
		$barcodeImage = Zend_Pdf_Image::imageWithPath($file);

		$il = 1;
		for($x=0;$x<intval($page);$x++)
		{
			$pageNew = $ob->newPage($listWidth.':'.$listHeight.':');
			$pageNew->drawImage($barcodeImage, $barcodeLeft, $barcodeTop, $barcodeLeft + $barcodeWidth, $barcodeTop + $barcodeHeight);
			$pageNew->setFont($font, 12);
			$row = 0;
			$el = 0;
			
			foreach($dane[$il] as $key=>$kod)
			{
				$tempel = $el;
				$temprow = $row;
				if($el == ($elementwiersz-1))
				{
					$row++;
					$el = 0;
				}
				else
				{
					$el++;
				}
				
				$wy = ($temprow * $pozyjump) + ($pozy);
				$wx = ($tempel * $pozxjump) + ($pozx);
				$pageNew->drawText($key, $wx, $wy, 'UTF-8');
				if($twoline) $pageNew->drawText($kod, $wx, $wy-$twoline, 'UTF-8');
			}
			$il++;
			$ob->pages[] = $pageNew;
		}

		if(count($ob->pages) > 0)
		{
			$save = $ob->save($path.''.$name);
		}
		return $save;
	}
	function weekOfMonth($date) {
		$date_parts = explode('-', $date);
		$date_parts[2] = '01';
		$first_of_month = implode('-', $date_parts);
		$day_of_first = date('N', strtotime($first_of_month));
		$day_of_month = date('j', strtotime($date));
		return ceil(($day_of_first + $day_of_month - 1) / 7);
	}
	function charDrawWeek($array, $arraysort, $plusowac = false)
	{
		$arraydaty = array();
		if(!empty($array))
		{
			array_multisort($arraysort, SORT_ASC,$array);
			foreach($array as $data=>$ar)
			{
				$ex = explode('-', $data);
				$tydzien = date('W', strtotime($data));
				date('j', strtotime($data));
				$miesiac = date('F', strtotime($data));
				$tydzienmiesiac = Query::weekOfMonth($data);
				$romnumber = Common::getRomNum();
				$tydzienmiesiac = $romnumber[$tydzienmiesiac];
				$arrmiesiac = Common::getMceString();
				$miesiac = $arrmiesiac[$miesiac];

				if(!isset($arraydaty[$ex[1]]))
				{
					if(!isset($arraydaty[$ex[1]][$tydzien]))
					{
						$arraydaty[$ex[1]][$tydzien][0] = $miesiac;
						$arraydaty[$ex[1]][$tydzien][1] = $tydzienmiesiac;
						$arg = 2;
						foreach($ar as $row)
						{
							$arraydaty[$ex[1]][$tydzien][$arg] = $row;
							$arg++;
						}
					}
					else
					{
						$arraydaty[$ex[1]][$tydzien][0] = $miesiac;
						$arraydaty[$ex[1]][$tydzien][1] = $tydzienmiesiac;
						$arg = 2;
						foreach($ar as $row)
						{
							if(!$plusowac) $arraydaty[$ex[1]][$tydzien][$arg] += $row;
							else $arraydaty[$ex[1]][$tydzien][$arg] = $row;
							$arg++;
						}
					}

				}
				else
				{
					if(!isset($arraydaty[$ex[1]][$tydzien]))
					{
						$arraydaty[$ex[1]][$tydzien][0] = $miesiac;
						$arraydaty[$ex[1]][$tydzien][1] = $tydzienmiesiac;
						$arg = 2;
						foreach($ar as $row)
						{
							$arraydaty[$ex[1]][$tydzien][$arg] = $row;
							$arg++;
						}
					}
					else
					{
						$arraydaty[$ex[1]][$tydzien][0] = $miesiac;
						$arraydaty[$ex[1]][$tydzien][1] = $tydzienmiesiac;
						$arg = 2;
						foreach($ar as $row)
						{
							if(!$plusowac) $arraydaty[$ex[1]][$tydzien][$arg] += $row;
							else $arraydaty[$ex[1]][$tydzien][$arg] = $row;
							$arg++;
						}
					}
				}
			}
		}
		return $arraydaty;
	}

	function charDrawDays($array, $arraysort, $plusowac = false)
	{
		$arraydaty = array();
		if(!empty($array))
		{
			array_multisort($arraysort, SORT_ASC,$array);
			foreach($array as $data=>$ar)
			{
				$ex = explode('-', $data);
				$tydzien = date('W', strtotime($data));
				date('j', strtotime($data));
				$miesiac = date('F', strtotime($data));
				$tydzienmiesiac = Query::weekOfMonth($data);
				$romnumber = Common::getRomNum();
				$tydzienmiesiac = $romnumber[$tydzienmiesiac];
				$arrmiesiac = Common::getMceString();
				$miesiac = $arrmiesiac[$miesiac];

				//dump($ex);die;
				if(!isset($arraydaty[$ex[1]]))
				{
					if(!isset($arraydaty[$ex[1]][$tydzien]))
					{
						$arraydaty[$ex[1]][$tydzien][0] = $miesiac;
						$arraydaty[$ex[1]][$tydzien][1] = $tydzienmiesiac;
						$arg = 2;
						foreach($ar as $row)
						{
							$arraydaty[$ex[1]][$tydzien][$arg] = $row;
							$arg++;
						}
					}
					else
					{
						$arraydaty[$ex[1]][$tydzien][0] = $miesiac;
						$arraydaty[$ex[1]][$tydzien][1] = $tydzienmiesiac;
						$arg = 2;
						foreach($ar as $row)
						{
							if(!$plusowac) $arraydaty[$ex[1]][$tydzien][$arg] += $row;
							else $arraydaty[$ex[1]][$tydzien][$arg] = $row;
							$arg++;
						}
					}

				}
				else
				{
					if(!isset($arraydaty[$ex[1]][$tydzien]))
					{
						$arraydaty[$ex[1]][$tydzien][0] = $miesiac;
						$arraydaty[$ex[1]][$tydzien][1] = $tydzienmiesiac;
						$arg = 2;
						foreach($ar as $row)
						{
							$arraydaty[$ex[1]][$tydzien][$arg] = $row;
							$arg++;
						}
					}
					else
					{
						$arraydaty[$ex[1]][$tydzien][0] = $miesiac;
						$arraydaty[$ex[1]][$tydzien][1] = $tydzienmiesiac;
						$arg = 2;
						foreach($ar as $row)
						{
							if(!$plusowac) $arraydaty[$ex[1]][$tydzien][$arg] += $row;
							else $arraydaty[$ex[1]][$tydzien][$arg] = $row;
							$arg++;
						}
					}
				}
			}
		}
		return $arraydaty;
	}

	function getRandNumber($min=1, $max=10, $size = 4)
	{
		$rand = mt_rand($min,$max);
		$length = strlen($rand);
		if($length > $size) $rand = substr($rand, 0, $size);
		if($length < $size)
		{
		   for($i=0; $i<=$size; $i++)
		   {
			   if($length<$size)
			   {
				   $_rand = mt_rand(1,9);
				   $rand .= $_rand;
				   $length = strlen($rand);
			   }
			   elseif($rand==$size) break;
		   }
		}
		return $rand;
	}
	
	public function encodePassword($pass)
	{
		return str_replace($this->normalcode,$this->hashcode,$pass);
	}

	public function decodePassword($pass)
	{
		return str_replace($this->hashcode,$this->normalcode,$pass);
	}
}
?>