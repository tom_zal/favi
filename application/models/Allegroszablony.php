<?php
class Allegroszablony extends Zend_Db_Table
{	
	public $id;
	protected $_name = 'Allegroszablony';
	private $db, $obConfig;

    public function __construct($module = 'admin')
	{
		parent::__construct();
        $this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		return $this->getAdapter()->lastInsertId();
	}
	
	function edytuj($dane)
	{
		$where = 'id = '.$this->id.'';
		$this->update($dane, $where);
	}
	
	function ustawDomyslny()
	{
		$where = 'domyslny = "T" and id != "'.$this->id.'"';
		$this->update(array('domyslny' => "N"), $where);
		$where = 'id = '.$this->id.'';
		$this->update(array('domyslny' => "T"), $where);
	}
	
	function wypisz()
	{
		$result = $this->fetchAll('nazwa <> ""', 'nazwa asc');
		return $result;
	}

	function szukajNowy()
	{
		$result = $this->fetchAll('nazwa = ""');
		return $result;
	}
	
	function usun()
	{
		$where = 'id = '.$this->id;
		$this->delete($where);
	}
	function wypiszPojedynczego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function getDomyslny()
	{
		$result = $this->fetchRow('domyslny = "T"');
		return $result;
	}
	
	public function getSzablonFromExcelWebPage($szablonName)
	{
		//$szablonName = 'Szablony';
		$path = 'http://'.$_SERVER['SERVER_NAME'].'/'.$this->obConfig->baseUrl.'/public/admin/szablony';
		
		$szablonName = str_replace(' ', '%20', $szablonName);
		$szablon = @file_get_contents($path.'/'.$szablonName);
		if($szablon === false) return 'Brak pliku szablonu '.$path.'/'.$szablonName;
		$szablonName = str_replace('.html', '', $szablonName);
		$szablonName = str_replace('.htm', '', $szablonName);
		
		$bodyStart = strpos($szablon, '<body>');
		$bodyEnd = strpos($szablon, '</body>');
		if(false && $bodyStart !== false && $bodyEnd !== false)
		{
			$szablon = substr($szablon, strpos($szablon, '<body>') + 6);
			$szablon = substr($szablon, 0, strpos($szablon, '</body>'));
		}
		
		$ifgte = '<!--';//'<!--[if gte vml 1]>';
		$endif = '-->';//'<![endif]--><![if !vml]>';
		$start = strpos($szablon, $ifgte);
		if(false)
		while($start !== false)
		{
			$end = strpos(substr($szablon, $start), $endif);
			//if($end === false || $end < $start) break;
			$vml = substr($szablon, $start, $end + strlen($endif));
			//echo htmlspecialchars($vml);
			$szablon = str_replace($vml, '', $szablon);
			$start = strpos($szablon, $ifgte);					
		}
		//$szablon = str_replace('<![if !vml]>', '', $szablon);
		//$szablon = str_replace('<![if supportMisalignedColumns]>', '', $szablon);
		//$szablon = str_replace('<![endif]>', '', $szablon);
		
		$polishNO = $this->common->getZnaki('pl', 'no');
		$polishOK = $this->common->getZnaki('pl', 'ok');
		$szablon = str_replace($polishNO, $polishOK, $szablon);
		
		$polishBigNO = $this->common->getZnaki('PL', 'no');
		$polishBigOK = $this->common->getZnaki('PL', 'ok');
		$szablon = str_replace($polishBigNO, $polishBigOK, $szablon);

		//$szablon = preg_replace('/(if gte vml 1)*(endif)/', '', $szablon,-1,$ile);
		//$ile = preg_match_all('/<!--.*-->/s', $szablon, $matches);
		//var_dump(strlen($szablon));
		$szablon = str_replace($szablonName.'_pliki/', $path.'/'.$szablonName.'_pliki/', $szablon);		
		//echo htmlspecialchars($szablon);die();
		return $szablon;
	}	
	
	public function getSzablonAllegro($id, $rozm, $produkt, $szablon, $kontoID)
	{
		$konto = new Allegrokonta();
		$konto->ID = $kontoID > 0 ? intval($kontoID) : 1;
		$config = $konto->klient();

		if($id >= 0)
		{
			$szablony = new Allegroszablony();
			$szablony->id = $szablon;
			$szablon = $szablony->wypiszPojedynczego();
				
			$tekst = $szablon['tekst'];//$this->getSzablonFromExcelWebPage($szablon['szablon']);
			$charset = '<meta content="text/html; charset=utf-8" http-equiv="Content-Type">';
			//$tekst = str_replace('<head>'.PHP_EOL.'</head>', '<head>'.$charset.'</head>', $tekst);
			if(strpos($tekst, '<html') === false && strpos($tekst, '<body') === false)
			$tekst = '<html><head>'.$charset.'</head><body>'.$tekst.'</body></html>';
			
			$szablonName = $szablon['szablon'];
			//$szablonName = str_replace(' ', '%20', $szablonName);
			$szablonName = str_replace('.html', '', $szablonName);
			$szablonName = str_replace('.htm', '', $szablonName);
			
			$sciezka = 'admin/szablony/'.$szablonName.'_pliki/';
			$glob = glob($sciezka.'*.css');
			if($glob && count($glob) > 0)
			foreach($glob as $f => $file)
			{
				$css = @file_get_contents($file);
				if(!empty($css))
				{
					$styl = '<style type="text/css">'.$css.'</style>';
					$tekst = str_replace('<head>', '<head>'.$styl, $tekst);
				}
			}

			$SKU1 = '<div style="display:none;">PROD='.$id.'=PROD</div>';
			$SKU2 = '<div style="display:none;">ROZM='.$rozm.'=ROZM</div>';
			$tekst = str_ireplace('</body>', $SKU1.$SKU2.'</body>', $tekst);
			$tekst = str_replace('ALLEGRO_UID', $config['uid'], $tekst);
			$nazwa = $this->obConfig->allegroNazwa && @!empty($produkt['nazwa_allegro']) ? $produkt['nazwa_allegro'] : $produkt['nazwa'];
			$tekst = str_replace('NAZWA_PRODUKTU', $nazwa, $tekst);
			$tekst = str_replace('CENA_PRODUKTU', $produkt['brutto'], $tekst);
			$opis = $this->obConfig->allegroOpis && @!empty($produkt['tekst_allegro']) ? $produkt['tekst_allegro'] : $produkt['tekst'];
			$tekst = str_replace('OPIS_PRODUKTU', $opis, $tekst);
			$tekst = $this->common->fixTekst($tekst, true, true);
			
			$path = $this->obConfig->www.'/public/admin/zdjecia';
			
			$imgMain = '';
			if(@!empty($produkt['zdj']['img']))
			{
				$opis = @$produkt['zdj']['nazwa'];
				$src = $path.'/'.$produkt['zdj']['img'];
				$imgMain = '<img title="'.$opis.'" alt="'.$opis.'" src="'.$src.'" id="imgMain" />';
			}
			$tekst = str_replace('ZDJECIE_1', $imgMain, $tekst);
			
			$galeria = '';			
			for($i=0;$i<@count($produkt['galeria']);$i++)
			{
				$opis = $produkt['galeria'][$i]['nazwa'];
				$src = $path.'/'.$produkt['galeria'][$i]['img'];
				$img = '<img src="'.$src.'" title="'.$opis.'" alt="'.$opis.'" />';
				$tekst = str_replace('ZDJECIE_'.($i+1).'_OPIS', $opis, $tekst);
				if($i >= 0) $galeria .= $img; //.'<br/>'.$opis;
				//if($i == 0) $img = '<img title="'.$opis.'" alt="'.$opis.'" src="'.$src.'" id="imgMain" />';
				$tekst = str_replace('ZDJECIE_'.($i+1), $img, $tekst);
			}
			//var_dump($produkt['galeria']); echo $tekst; die();
			$tekst = str_replace('GALERIA_PRODUKTU', $galeria, $tekst);
			
			$params = "";
			if(true && @!empty($produkt['producent']['nazwa']))
			$params .= '<label>'.$this->common->lang('producent').': </label><span>'.$produkt['producent']['nazwa'].'</span><br/>';
			if(true && @!empty($produkt['marka']['nazwa']))
			$params .= '<label>'.$this->common->lang('Marka').': </label><span>'.$produkt['marka']['nazwa'].'</span><br/>';
			if(true && @!empty($produkt['kod']))
			$params .= '<label>'.$this->common->lang('Model').': </label><span>'.$produkt['kod'].'</span><br/>';
			$tekst = str_replace('PARAMETRY_PRODUKTU', $params, $tekst);
			
			$kategs = "";
			$menu = new Kategorie();
			$kategorie = $menu->showWybranyRodzic(0);
			if(count($kategorie) > 0)
			foreach($kategorie as $kateg)
			{
				if(empty($kateg['allegro'])) continue;
				$allegro = str_replace(";-1", "", $kateg['allegro']);
				if(empty($allegro) || $allegro == "-1") continue;
				$allegro = explode(";", $allegro);
				if(empty($allegro) || count($allegro) == 0) continue;
				$allegro = end($allegro);
				if(empty($allegro) || intval($allegro) <= 0) continue;
				$kategs .= '<a href="http://allegro.pl/listing/user/listing.php?id='.$allegro.'&us_id='.$config['uid'].'&country=1">';
				$kategs .= '<span>'.$kateg['nazwa'].'</span></a>';
			}
			$tekst = str_replace('KATEGORIE_ALLEGRO', $kategs, $tekst);
			
			$marki = "";
			$producent = new Producent();
			$producenci = $producent->wypiszProducentow();
			if(count($producenci) > 0)
			foreach($producenci as $marka)
			{
				if(empty($marka['logo'])) continue;
				$marki .= '<a href="http://allegro.pl/listing/user/listing.php?us_id='.$config['uid'].'&description=1&string='.urlencode($marka['nazwa']).'&order=m&search_scope=userItems-'.$config['uid'].'">';
				$marki .= '<img src="'.$this->obConfig->www.'/public/admin/producenci/'.$marka['logo'].'" title="'.$marka['nazwa'].'" />';
				$marki .= '</a>';
			}
			$tekst = str_replace('PRODUCENCI_ALLEGRO', $marki, $tekst);
			
			$menu = new Kategorie();
			$blokAllegro = $menu->blokAllegroAdmin();
			if(count($blokAllegro) > 0)
			foreach($blokAllegro as $blok)
			{
				$nazwa = trim($blok['link']);
				//$nazwa = strtolower($nazwa);
				//$nazwa = str_replace('-','',$nazwa);
				//$nazwa = str_replace('allegro','',$nazwa);
				$blok['tekst'] = $this->common->fixTekst($blok['tekst'], true, true);
				$tekst = str_replace('BLOK_'.$nazwa, $blok['tekst'], $tekst);
			}
			
			return $tekst;
		}
		else
		{
			return 'Produkt nie istnieje.';
		}
	}
}
?>