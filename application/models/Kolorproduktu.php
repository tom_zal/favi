<?php
class Kolorproduktu extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
	function getKoloryForExport($ids)
	{
		if(!$this->common->isArray($ids, true)) return null;
        $select = $this->db->select()->from(array('kp' => 'Kolorproduktu'), array('id_produktu', 
			new Zend_Db_Expr('GROUP_CONCAT(DISTINCT id_koloru ORDER BY id_koloru ASC SEPARATOR "|") as kolor')))
			->where('id_produktu in('.implode(',',$ids).')')->group('id_produktu')->order(array('id_produktu asc'));
		//echo $select;die();
		$result = $this->db->fetchAll($select);
        for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i]['id_produktu']] = $result[$i]['kolor'];
		}
		return @$results;
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function wypiszAll()
	{
		$result = $this->fetchAll();
		return $result;
	}
	function wypisz()
	{
		$select = $this->db->select()->from(array('kp' => 'Kolorproduktu'), array('id','id_produktu','id_koloru'))
			->joinleft(array('k' => 'Kolory'), 'k.id = kp.id_koloru', array('nazwa', 'img', 'color'))
			->where('kp.id_produktu = '.$this->id)->group('k.id')->order(array('k.nazwa'));
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszForIds($ids = null)
	{
		if(!$this->common->isArray($ids, true)) return null;
		$select = $this->db->select()
			->from(array('kp' => 'Kolorproduktu'), array('id', 'id_produktu', 'id_koloru'))
			->join(array('k' => 'Kolory'), 'k.id = kp.id_koloru', array('nazwa', 'img', 'color'))
			->where('kp.id_produktu in ('.implode(',', $ids).')')->order(array('k.nazwa'));
		//echo $select;
		$result = $this->db->fetchAll($select);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i]['id_produktu']][] = $result[$i];
		}
		return @$results;
	}
	function usun()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	function usunProdukt($id)
	{
		$result = $this->delete('id_produktu = '.$id);
	}
	function usunProduktyIds($ids)
	{
		$result = $this->delete('id_produktu in ('.$ids.')');
	}
	function znajdzKolor($id_prod,$id_kol)
	{
		$result = $this->fetchAll('id_produktu = '.$id_prod.' AND id_koloru = '.$id_kol)->toArray();
		return $result;
	}
	
	function getKoloryString($id, $kolory = false, $sep = ',', $spa = ' ')
	{
		if($kolory === false)
		{
			$kolory->id = $id;
			$kolory = $this->wypisz();
		}
		$result = '';
        for($i = 0; $i < count($kolory); $i++)
		{
			$result .= $kolory[$i]['nazwa'];
			if($i < count($kolory) - 1) $result .= $sep.$spa;
		}
        return $result;
    }
}
?>