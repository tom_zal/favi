<?php
/**
 * smsAPI_HTTPClient
 *
 * Copyright (c) 2010, ComVision
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation and/or
 *    other materials provided with the distribution.
 *  - Neither the name of the smsAPI.pl nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @author ComVision <info@smsapi.pl>
 * @copyright 2010 ComVision
 * @license BSD-3
 * @package smsapi
 * @subpackage client\html
 * @version 1.0 14.10.2010
 */

/**
 * Client smsAPI przez HTTP/HTTPS
 *
 * Przyklad:
 * <code>
 * require_once 'smsapi.php';
 * //Instrukcja odpowiedziala za automatyczne wczytywanie class
 * spl_autoload_register(array('smsAPI','__autoload'));
 *
 * try {
 *
 *	$smsapi = new smsAPI_HTTPClient('login', md5('password'));
 *
 *	$msg = $smsapi->new_sms();
 *	$msg->add_to( 'xxxxxxxxx' );
 *	$msg->message	= 'test';
 *	$msg->eco		= TRUE;
 *	$msg->flash		= FALSE;
 *	$msg->test		= TRUE;
 *
 * 	$result = $msg->send();
 * 	var_dump($result);
 *
 * 	$smsapi->delete_sms( $result[0]->id );
 * }
 * catch ( smsAPI_Exception $e )
 * {
 * 	echo $e->getMessage();
 * }
 * </code>
 *
 * @see http://www.smsapi.pl/spec/smsAPI.pdf
 */
class smsAPI_HTTPClient
{
	/**
	 * Url do smsAPI
	 *
	 * @var string
	 */
	protected $url = null;
	/**
	 * Sposob przesylania danych
	 *
	 * @see smsAPI_HTTPClient::METHOD_GET
	 * @see smsAPI_HTTPClient::METHOD_POST
	 * @var int
	 */
	protected $method = null;

	/**
	 * Dane klienta
	 *
	 * @var array
	 */
	protected $client;

	/**
	 * Przesylania danych metodą GET do smsAPI
	 */
	const METHOD_GET	= 0;
	/**
	 * Przesylania danych metodą POST do smsAPI
	 */
	const METHOD_POST	= 1;

	/**
	 * Timeout
	 *
	 * @ignore
	 * @var float
	 */
	protected $timeout		= 0;

	/**
	 * Tablica opisu błędów
	 *
	 * @var array
	 */
	public static $errors = array(
		0	=> 'Nieznany błąd',
		8	=> 'Błąd w odwołaniu (Prosimy zgłośić)',
		11	=> 'Zbyt długa lub brak wiadomości lub ustawiono parametr nounicode i pojawiły się znaki specjalne w wiadomości. Dla wysyłki VMS błąd oznacza brak pliku WAV lub treści TTS',
		12	=> 'Wiadomość zawiera ponad 160 znaków (gdy użyty parametr &single=1)',
		13	=> 'Nieprawidłowy numer odbiorcy',
		14	=> 'Nieprawidłowe pole nadawcy',
		17	=> 'Nie można wysłać FLASH ze znakami specjalnymi',
		18	=> 'Nieprawidłowa liczba parametrów',
		19	=> 'Za dużo wiadomości w jednym odwołaniu',
		20	=> 'Nieprawidłowa liczba parametrów IDX',
		21	=> 'Wiadomość MMS ma za duży rozmiar (maksymalnie 300kB)',
		22	=> 'Błędny format SMIL',
		23	=> 'Błąd pobierania pliku dla wiadomości MMS lub VMS',
		24	=> 'Błędny format pobieranego pliku ',
		30	=> 'Brak parametru UDH jak podany jest datacoding=bin',
		31	=> 'Błąd konwersji TTS',
		32	=> 'Nie można wysyłać wiadomości Eco, MMS i VMS na zagraniczne numery.',
		33	=> 'Brak poprawnych numerów',
		40	=> 'Brak grupy o podanej nazwie',
		41	=> 'Wybrana grupa jest pusta (brak kontaktów w grupie)',
		50	=> 'Nie można zaplanować wysyłki na więcej niż 3 miesiące w przyszłość',
		51	=> 'Ustawiono błędną godzinę wysyłki, wiadomość VMS mogą być wysyłane tylko pomiędzy godzinami 8 a 22',
		52	=> 'Za dużo prób wysyłki wiadomości do jednego numeru (maksymalnie 10 prób w przeciągu 60sek do jednego numeru)',
		101	=> 'Niepoprawne lub brak danych autoryzacji. UWAGA! Hasło do API może różnić się od hasła do Panelu Klienta',
		102	=> 'Nieprawidłowy login lub hasło',
		103	=> 'Brak punktów dla tego użytkownika',
		104	=> 'Brak szablonu',
		105	=> 'Błędny adres IP (włączony filtr IP dla interfejsu API)',
		200	=> 'Nieudana próba wysłania wiadomości',
		201	=> 'Wewnętrzny błąd systemu (prosimy zgłosić)',
		202	=> 'Zbyt duża ilość jednoczesnych odwołań do serwisu, wiadomość nie została wysłana (prosimy odwołać się ponownie)',
		300	=> 'Nieprawidłowa wartość pola points (przy użyciu pola points jest wymagana wartość 1)',
		301	=> 'ID wiadomości nie istnieje',
		400	=> 'Nieprawidłowy ID statusu wiadomości',
		999	=> 'Wewnętrzny błąd systemu (prosimy zgłosić)'
	);

	/**
	 * Inicializacjia
	 *
	 * Przyklad:
	 * <code>
	 * try {
	 *
	 *	$smsapi = new smsAPI_HTTPClient('login', md5('password'));
	 *
	 * }
	 * catch ( smsAPI_Exception $e )
	 * {
	 * 	echo $e->getMessage();
	 * }
	 * </code>
	 *
	 * @uses smsAPI_HTTPClient::METHOD_GET
	 * @uses smsAPI_HTTPClient::METHOD_POST
	 * @param string	$username	Nazwa użytkownika w serwisie smsAPI
	 * @param string	$password	Hasło do Twojego konta w serwisie smsAPI zaszyfrowane w MD5
	 * @param int		$timeout	Timeout polączenia
	 * @param bool		$ssl		http lub https
	 * @param int		$method		Sposób przesyania danych do smsAPI
	 */
	public function  __construct($username, $password, $timeout = 0,$ssl = true, $method = self::METHOD_POST)
	{
		$this->url = $ssl == TRUE ? 'https://ssl.smsapi.pl/send.do' : 'http://api.smsapi.pl/send.do';
		$this->method = $method;

		if( !isset( $username ) OR empty ( $username ) )
			throw new smsAPI_Exception('Argument "username" is invalid');

		if( !isset( $password ) OR strlen( $password ) != 32 )
			throw new smsAPI_Exception('Argument "password" is invalid');

		$this->timeout	= (int)$timeout;

		$this->client = array(
			
			'username'	=> $username,
			'password'	=> $password
		);
	}

	/**
	 * Utwórz nowa wiadomość
	 *
	 * Przyklad:
	 * <code>
	 * try {
	 *
	 *	$smsapi = new smsAPI_HTTPClient('login', md5('password'));
	 *
	 *	//Tworzenie wiadomości
	 *	$msg = $smsapi->new_sms();
	 *	$msg->add_to( 'xxxxxxxxx', 'xxxxxxxxx' );
	 *	$msg->message	= 'test message';
	 *	$msg->eco		= TRUE;
	 *	$msg->flash		= FALSE;
	 *
	 *	//Wysłanie wiadomości
	 *	$result = $msg->send();
	 *
	 * 	var_dump($result);
	 *	//array
	 *	//  0 =>
	 *	//    object(stdClass)[3]
	 *	//      public 'id' => string '1010131304559538088' (length=19)
	 *	//      public 'points' => string '0.07' (length=1)
	 * }
	 * catch ( smsAPI_Exception $e )
	 * {
	 * 	echo $e->getMessage();
	 * }
	 * </code>
	 *
	 * @param array $params
	 * @return smsAPI_HTTP_SMS
	 */
	public function new_sms($params = null)
	{
		return new smsAPI_HTTP_SMS($params, $this);
	}

	/**
	 * Wysyłanie pojedynczego SMS'a
	 *
	 * Przyklad:
	 * <code>
	 * //Tworzenie wiadomości
	 * $msg = new smsAPI_HTTP_SMS();
	 * $msg->add_to( 'xxxxxxxxx', 'xxxxxxxxx' );
	 * $msg->message	= 'test message';
	 * $msg->eco		= TRUE;
	 * $msg->flash		= FALSE;
	 *
	 * try {
	 *
	 *	$smsapi = new smsAPI_HTTPClient('login', md5('password'));
	 *
	 *	//Wysłanie wiadomości
	 *	$result = $smsapi->send_sms($msg);
	 * 
	 * 	var_dump($result);
	 *	//array
	 *	//  0 =>
	 *	//    object(stdClass)[3]
	 *	//      public 'id' => string '1010131304559538088' (length=19)
	 *	//      public 'points' => string '0.07' (length=1)
	 * }
	 * catch ( smsAPI_Exception $e )
	 * {
	 * 	echo $e->getMessage();
	 * }
	 * </code>
	 *
	 * @throws smsAPI_Exception
	 * @param smsAPI_HTTP_SMS $sms
	 * @return stdClass(id,points)
	 */
	public function send_sms($sms)
	{
		if( !($sms instanceof smsAPI_HTTP_SMS) )
			throw new smsAPI_Exception('Invalid function argument', -18);

		$response = $this->server_call( $sms->_get_params() );

		$result = array();

		foreach ( $response as $r )
		{
			$r = trim($r);
			if( empty ($r) )			 continue;

			$r = explode(':', $r);

			if( !strcasecmp($r[0], 'OK') )
				$tmp = (object)array( 'id' => $r[1], 'points'	=> $r[2] );
			else
				$tmp = (object)array( 'error' => (int)@$r[1] );

			if( isset( $r[3] ) ) $tmp->phone = $r[3];

			$result[] = $tmp;
		}

		return $result;
	}

	/**
	 * Usuwanie zaplanowanej wiadomości
	 *
	 * Przyklad:
	 * <code>
	 * try {
	 *
	 *	$smsapi = new smsAPI_HTTPClient('login', md5('password'), true);
	 *
	 *	$smsapi->delete_sms('1010131304559538088');
	 * }
	 * catch ( smsAPI_Exception $e )
	 * {
	 * 	echo $e->getMessage();
	 * }
	 * </code>
	 *
	 * @throws smsAPI_Exception
	 * @param string $id ID wiadomości w systemie smsAPI
	 * @return null
	 */
	public function delete_sms($id)
	{
		$result = $this->server_call( array( 'sch_del'	=> (string)$id ) );

		$result = explode(':', $result[0]);

		if( !strcasecmp($result[0], 'ERROR') )
		{
			$code = (int)$result[1];
			$msg = isset( $code ) ? self::$errors[$code] : self::$errors[0];
			throw new smsAPI_Exception($msg, $code );
		}

		if( strcasecmp($result[0], 'OK') )
			throw new smsAPI_Exception ('Unknown error', 0);
	}

	/**
	 * Sprawdzenie ilości punktów na koncie
	 *
	 * Przyklad
	 * <code>
	 * try {
	 *
	 *	$smsapi = new smsAPI_HTTPClient('login', md5('password'), true);
	 *
	 *	$result = $smsapi->get_points(true);
	 * }
	 * catch ( smsAPI_Exception $e )
	 * {
	 * 	echo $e->getMessage();
	 * }
	 * </code>
	 *
	 * @param bool $details
	 * @return stdClass(points,pro,eco)
	 */
	public function get_points($details = false)
	{
		$response = $this->server_call(array(
			'points'	=> 1,
			'details'	=> (bool)$details
		));

		$result = new stdClass;
		$result->points = 0;

		$response[0] = explode(':', $response[0]);
		
		if( !strcasecmp($response[0][0], 'ERROR') )
		{
			$result->error = trim(@$response[0][1]);
		}
		else
		{
			$result->points = trim(@$response[0][1]);
		
			$count = count($response);
			if( $count >= 2 ) $result->pro = trim(@$response[1]);
			if( $count >= 3 ) $result->eco = trim(@$response[2]);
		}
		
		return $result;
	}

	const CALL_AUTO		= 0;
	const CALL_FOPEN	= 1;
	const CALL_CURL		= 2;
	
	public static $call = self::CALL_AUTO;

	protected function get_call()
	{
		if( in_array(self::$call, array( self::CALL_CURL, self::CALL_FOPEN )) ) return self::$call;

		if( ini_get('allow_url_fopen') == 1 ) return self::CALL_FOPEN;
		if( function_exists('curl_init') ) return self::CALL_CURL;

		throw new smsAPI_Exception('allow_url_fopen is disabled and curl_init doesn\'t exists');
	}

	/**
	 * Wywolanie zapytania do strony smsAPI.pl
	 *
	 * @uses smsAPI_HTTPClient::METHOD_GET
	 * @uses smsAPI_HTTPClient::METHOD_POST
	 * @throws smsAPI_Exception
	 * @param string $params
	 * @return string
	 */
	protected function server_call($params)
	{
		$opts = array(
			'http'		=> array(
				'method'	=> ($this->method == self::METHOD_POST ? 'POST' : 'GET'),
				'header'	=> array(
					'user_agent'	=>	'SMSApi_HTTP_Client'
				)
			)
		);

		$url = $this->url;
		$data = http_build_query( array_merge( (array)$params, $this->client ), null, '&' );

		switch( $this->method )
		{
			case self::METHOD_GET:

				$opts['http']['method']	= 'GET';
				$url .= '?' . $data;
				break;

			case self::METHOD_POST:
			default:

				$opts['http']['method']		= 'POST';
				$opts['http']['header']		= 'Content-type: application/x-www-form-urlencoded';
				$opts['http']['content']	= $data;
				break;
		}

		if( $this->timeout > 0 )
		{
			$opts['http']['timeout'] = $this->timeout;
		}

		smsAPI::call_listners('server_call args', array( 'opts' => $opts, 'url' => $url ));

		switch( $this->get_call() )
		{
			case self::CALL_FOPEN:
				$context = stream_context_create($opts);
				$result = file_get_contents($url, false, $context);
				break;

			case self::CALL_CURL:
				$ch = curl_init();
				@curl_setopt($ch, CURLOPT_URL, $url);
				@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
				@curl_setopt($ch, CURLOPT_CRLF, false);
				@curl_setopt($ch, CURLOPT_FRESH_CONNECT, false);
				@curl_setopt($ch, CURLOPT_HEADER, false);
				@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

				if( $this->method == self::METHOD_GET )
				{
					@curl_setopt($ch, CURLOPT_POST, false);
					@curl_setopt($ch, CURLOPT_HTTPGET, true);
				}
				else
				{
					@curl_setopt($ch, CURLOPT_HTTPGET, false);
					@curl_setopt($ch, CURLOPT_POST, true);
					@curl_setopt($ch, CURLOPT_POSTFIELDS, $opts['http']['content']);
				}

				@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				if( isset( $opts['http']['timeout'] ) )
				{
					@curl_setopt($ch, CURLOPT_TIMEOUT, $opts['http']['timeout']);
				}

				$result = @curl_exec($ch);

				curl_close($ch);
				break;
			default:
				throw new smsAPI_Exception('allow_url_fopen is disabled and curl_init doesn\'t exists');
		}

		smsAPI::call_listners('server_call result', array( 'result' => $result ));

		if( $result === false OR empty($result) )
			throw new smsAPI_Exception('HTTP request failed', -200);

		$result = trim($result, " ;\n\t\r");
		
		$result = explode(';', $result);

		if( empty($result) )
			throw new smsAPI_Exception ('Empty result', 0);

		return $result;
	}
}
