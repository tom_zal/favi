<?php
class Common
{
	public $obConfig, $db, $sorter;
	
	public function __construct($ten = false, $module = 'admin')
	{
		$this->zl = 'zł';
		$this->module = $module;
		$this->localhosts = array('localhost', 'www.local.d', 'local.d');
		$this->local = in_array($_SERVER['HTTP_HOST'], $this->localhosts);
		$this->lang = $this->getJezyk($module);
		$this->langs = $this->getJezyki();
		$this->langID = @intval(array_search($this->lang, array_keys($this->langs)));
		
		$this->position = array('blok' => 'bloki', 'menu' => 'podstrony', 'allegro' => 'allegro', 'lojalnosc' => 'lojalnosc');
		$this->polozenie = array('top' => 'Menu', 'topbottom' => 'Menu i Stopka', 'bottom' => 'Stopka', '' => 'Inne');
		$this->znakiArrows = array(8598=>"TL",8593=>"T",8599=>"TR",8592=>"L",8709=>"",8594=>"R",8601=>"BL",8595=>"B",8600=>"BR");
		
		$this->oznaczenia = array('nowosc' => 'Nowość', 'promocja' => 'Promocja', 'promocja_net' => 'Promocja Internet', 'polecane' => 'Polecane', 'wyprzedaz' => 'Wyprzedaż', 'specjalne' => 'Specjalne', 'produkt_dnia' => 'Produkt Dnia', 'glowna' => 'Strona Główna');
		foreach($this->oznaczenia as $oznacz => $oznaczenie)
		$this->oznaczenia[$oznacz] = array('nazwa' => $oznaczenie, 'config' => ucfirst($oznacz));
		$this->oznaczenia['promocja_net']['config'] = 'PromocjaNet';
		$this->oznaczenia['produkt_dnia']['config'] = 'ProduktDnia';
		$this->oznaczenia['nowosc']['strona'] = 'Nowości';
		$this->oznaczenia['promocja']['strona'] = 'Promocje';
		$this->oznaczenia['promocja_net']['strona'] = 'Promocje Net';
		$this->oznaczenia['polecane']['strona'] = 'Polecane';
		$this->oznaczenia['wyprzedaz']['strona'] = 'Wyprzedaż';
		$this->oznaczenia['specjalne']['strona'] = 'Specjalne';
		$this->oznaczenia['produkt_dnia']['strona'] = 'Produkty Dnia';
		$this->oznaczenia['glowna']['strona'] = 'Strona Główna';
		//var_dump($this->oznaczenia);die();
		
		$this->polaOpisy = array('jedn_miary_zb_czy' => 'Jedn. miary zbiorcza', 'jedn_miary_zb_typ' => 'Jedn. miary typ');
		$this->polaOnOff = array('widoczny' => 1, 'jedn_miary_zb_czy' => 1, 'negocjuj_cene' => 1);
		
		if($ten)
		{
			$this->obConfig = $this->getObConfig();
			$this->db = $this->getDB($this->obConfig);
			$this->ustawienia = $this->getUstawienia();
			$this->tlumaczenia = $this->getTlumaczenia();
			//$this->translator = Zend_Registry::get('Zend_Translate')->getAdapter();
		}
		//$result = $this->fetchAll($select); - gets cached
		//$result = $this->db->fetchAll($select); - gets cached
		//$result = $this->query($select)->fetchAll(); - does NOT gets cached (currently)
		//$stmt = $this->query($select);$result = $stmt->fetchAll(); - does NOT gets cached (currently)
    }
	function getConfig($type = 'general', $suffix = '')
	{
		if(!isset($this->obConfig))
		{
			$this->obConfig = $this->getObConfig($type, $suffix);
			$this->db = $this->getDB($this->obConfig);
		}
		if(!isset($this->dbName))
		$this->dbName = $this->obConfig->db->config->dbname;
	}
	function getDB($obConfig = null)
	{
		if(!$obConfig) $obConfig = $this->getObConfig('general', '');
		$session = new Zend_Session_Namespace('db'.$obConfig->db->config->dbname);
		if(isset($session->db) && @!$_GET['dbnew']) return $session->db;
        $db = Zend_Db::factory($obConfig->db->adapter, $obConfig->db->config->toArray());
		$db->query('SET NAMES utf8');
		$db->query('SET group_concat_max_len=1000000000');
		if($obConfig->profiler || @$_GET['profiler'])
		{
			$profiler = new Zend_Db_Profiler_Firebug('All DB Queries');
			$profiler->setEnabled(true);
			$db->setProfiler($profiler);
		}
		$session->db = $db;
		return $db;
	}
	function getObConfig($type = 'general', $suffix = '')
	{
		return new Zend_Config_Ini('../application/config'.$suffix.'.ini', $type);
	}
	function getParams()
	{
		$session = new Zend_Session_Namespace();
		return @$session->params;
	}
	function getCurl($url = "", $timeout = 0)
	{
		if(empty($url)) return null;
		if(!function_exists('curl_exec')) return null;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$content = curl_exec($ch);
		if(curl_errno($ch)) $content = curl_errno($ch).':'.curl_error($ch);
		curl_close($ch);
		return $content;
	}
	public function lang($nazwa, $lang = null)
	{
		//$this->translate = $translate = Zend_Registry::get('Zend_Translate');
		if(false && @!empty($lang))
		{
			$locale = new Zend_Locale(); 
			$locale->setLocale($lang);var_dump($lang);
			Zend_Registry::set('locale', $locale->getLanguage());
			Zend_Registry::set(Zend_Application_Resource_Locale::DEFAULT_REGISTRY_KEY,$locale);
			$translate = Zend_Registry::get('Zend_Translate');
            $translate->setLocale($locale);
			$translator = $translate->getAdapter();			
            Zend_Registry::set('Zend_Translate', $translate); 
			return $translator->translate($nazwa);
			echo $translate->_("$nazwa", $nazwa);return;
		}
		if(!isset($this->tlumaczenia)) $this->tlumaczenia = $this->getTlumaczenia();
		return isset($this->tlumaczenia[$nazwa]) ? $this->tlumaczenia[$nazwa] : $nazwa;
		$this->translator = $this->translate->getAdapter();
		return $this->translator->translate($nazwa);
	}
	function getJezyk($module = 'admin')
	{
		$name = $module == 'admin' ? 'jezyk_admin' : 'jezyk';
		$info = new Zend_Session_Namespace($name);
		return @!empty($info->lang) ? $info->lang : 'pl';
	}
	function getJezyki()
	{
		$euro = array('symbol' => '€', 'kod' => 'EUR', 'nazwa' => 'euro', 'name' => 'euro');
		$dolar = array('symbol' => '$', 'kod' => 'USD', 'nazwa' => 'dolar', 'name' => 'dollar');
		$jezyki['pl'] = array('lang' => 'język', 'name' => 'polski', 'nazwa' => 'polski');
		$jezyki['pl']['waluta'] = array('symbol' => 'zł', 'kod' => 'PLN', 'nazwa' => 'złoty', 'name' => 'złoty');
		//$jezyki['en'] = array('lang' => 'language', 'name' => 'english', 'nazwa' => 'angielski');
		//$jezyki['en']['waluta'] = array('symbol' => '£', 'kod' => 'GBP', 'nazwa' => 'funt', 'name' => 'pound');
		//$jezyki['de'] = array('lang' => 'sprache', 'name' => 'deutschland', 'nazwa' => 'niemiecki');
		//$jezyki['de']['waluta'] = $euro;		
		//$jezyki['es'] = array('lang' => 'idioma', 'name' => 'español', 'nazwa' => 'hiszpański');
		//$jezyki['es']['waluta'] = $euro;
		//$jezyki['it'] = array('lang' => 'lingua', 'name' => 'italiano', 'nazwa' => 'włoski');
		//$jezyki['it']['waluta'] = $euro;
		//$jezyki['fr'] = array('lang' => 'langue', 'name' => 'français', 'nazwa' => 'francuski');
		//$jezyki['fr']['waluta'] = $euro;
		//$jezyki['uk'] = array('lang' => 'мову', 'name' => 'Український', 'nazwa' => 'ukraiński');
		//$jezyki['uk']['waluta'] = array('symbol' => '₴', 'kod' => 'UAH', 'nazwa' => 'hrywna', 'name' => 'грн');
		//$jezyki['uk']['waluta'] = $dolar; $jezyki['uk']['waluta']['name'] = 'долар';
		//$jezyki['ru'] = array('lang' => 'язык', 'name' => 'русский', 'nazwa' => 'rosyjski');
		//$jezyki['ru']['waluta'] = array('symbol' => 'R', 'kod' => 'RUB', 'nazwa' => 'rubel', 'name' => 'рубль');
		return $jezyki;
	}
	function getTlumaczenia()
	{
		$tlumaczenia = new Tlumaczenia($this->module);
		return $tlumaczenia->wypisz();
	}
	function getFonts()
	{
		$fonts = array('Arial','Courier New','Helvetica','Symbol','Tahoma','Times New Roman','Verdana');
		return $fonts;
	}
	function getUstawienia()
	{
		$this->getConfig();
		if(isset($_SESSION[$this->dbName]['ustawienia'])) return $_SESSION[$this->dbName]['ustawienia'];
		$settings = new Ustawienia();
		$ustawienia = $settings->wypisz();
		$settingslang = new Ustawienialang($this->module);
		$ustawienialang = $settingslang->wypisz();
		$ustawieniaAll = array_merge($ustawienia, $ustawienialang);
		$ustawieniaAll['title'] = stripslashes($ustawieniaAll['title']);
		$ustawieniaAll['keywords'] = stripslashes($ustawieniaAll['keywords']);
		$ustawieniaAll['description'] = stripslashes($ustawieniaAll['description']);
		$ustawieniaAll['google_analytics'] = stripslashes($ustawieniaAll['google_analytics']);
		$ustawieniaAll['cookiepolicy'] = stripslashes($ustawieniaAll['cookiepolicy']);
		$ustawieniaAll['cookielink'] = stripslashes($ustawieniaAll['cookielink']);
		//var_dump($ustawieniaAll);die();
		return $_SESSION[$this->dbName]['ustawienia'] = $ustawieniaAll;
	}
	function getPodstrona($tytul, $zdj = false, $gal = false, $vid = false)
	{
		$podstrony = new Podstrony('default');
		$podstrony->tytul = $this->lang($tytul);
		$podstrona = $podstrony->getPodstrona(true, $zdj);
		if($gal) $podstrona['galeria'] = $this->getGaleria($podstrona['id'], 'podstrony');
		if($vid) $podstrona['videos'] = $this->getVideos($podstrona['id'], 'podstrony');
		return $podstrona;
	}
	function getPodstrony($zdj = false, $lang = "pl")
	{
		if(empty($lang)) $lang = $this->lang;
		$where = 'p.lang = "'.$lang.'" and r.controller = "oferta"';
		$sql = $this->db->select()->from(array('p' => 'Podstrony'), array('*'));
		$sql->joinleft(array('r' => 'Routers'), 'p.link = r.route', array(''));
		if($zdj && $this->obConfig->podstronyGaleria)
		{
			$whereGal = 'p.id = g.wlasciciel and g.typ = "podstrony" and g.glowne = "T"';
			$whereGal.= ' and g.lang = "'.$lang.'" and g.wyswietl = "1"';
			$sql->joinleft(array('g' => 'Galeria'), $whereGal, array('img', 'img_opis' => 'nazwa'));
		}
		$sql->where($where);
		$results = $this->db->fetchAll($sql);
		foreach($results as $result)
		{
			if(@!empty($result['tekst'])) $result['tekst'] = $this->fixTekst($result['tekst'], true);
			if(@!empty($result['short'])) $result['short'] = $this->fixTekst($result['short'], true);
			$rows[$result['tytul']] = $result;
		}
		//var_dump($rows);die();
		return @$rows;
	}
	function getPodstronaID($rodzaj = null)
	{
		$langID = $this->langID;
		$podstrony['menu'] = 1 + (100 * $langID);
		$podstrony['podstrony'] = 10 + (100 * $langID);
		$podstrony['allegro'] = 21 + (100 * $langID);
		$podstrony['lojalnosc'] = 31 + (100 * $langID);
		$podstrony['bloki'] = 41 + (100 * $langID);
		return empty($rodzaj) ? $podstrony : $podstrony[$rodzaj];
	}
	function getRouteID($rodzaj = null)
	{
		$langID = $this->langID;
		$routes['moduly'] = 1;
		$routes['menu'] = 11 + (100 * $langID);
		$routes['podstrony'] = 20 + (100 * $langID);
		$routes['lojalnosc'] = 31 + (100 * $langID);
		$routes['rozne'] = 41 + (100 * $langID);
		$routes['strony'] = 51 + (100 * $langID);
		$routes['kategorie'] = 1001 + (1000 * $langID);
		$routes['produkty'] = 10001 + (10000 * $langID);
		return empty($rodzaj) ? $routes : $routes[$rodzaj];
	}
	
	function getUprawnienia()
	{
		return array('podstrony', 'zamówienia', 'klienci', 'asortyment', 'ustawienia');
	}
	function getKontrolerToPrawo()
	{
		$prawaMapa['podstrony'] = 'podstrony';
		$prawaMapa['zamowienia'] = 'zamówienia';
		$prawaMapa['kontrahent'] = 'klienci';
		$prawaMapa['oferta'] = 'asortyment';
		$prawaMapa['kategorie'] = 'asortyment';
		$prawaMapa['producent'] = 'asortyment';
		$prawaMapa['wagi'] = 'asortyment';
		$prawaMapa['platnosci'] = 'ustawienia';
		$prawaMapa['jednostkimiary'] = 'ustawienia';
		$prawaMapa['ustawienia'] = 'ustawienia';
		$prawaMapa['index'] = 'ustawienia';
		$prawaMapa['wiadomosci'] = 'ustawienia';
		$prawaMapa['confmail'] = 'ustawienia';
		return $prawaMapa;
	}
	
	function getKosztyAllegro()
	{
		return $kosztyAllegroAll = array('poczta_ekon' => 'Paczka pocztowa ekonomiczna', 'poczta_prior' => 'Paczka pocztowa priorytetowa','list_ekon' => 'List ekonomiczny','list_prior' => 'List priorytetowy','list_ekon_polec' => 'List polecony ekonomiczny','list_prior_polec' => 'List polecony priorytetowy', 'pobranie' => 'Przesyłka pobraniowa', 'pobranie_prior' => 'Przesyłka pobraniowa priorytetowa','kurier' => 'Przesyłka kurierska','kurier_pobranie' => 'Przesyłka kurierska pobraniowa','biznes' => 'Przesyłka biznesowa','biznes_pobranie' => 'Przesyłka biznesowa pobraniowa','paczka_ruch' => 'Odbiór w punkcie - <br/>PACZKA W RUCHu','paczka_ruch_przedplata' => 'Odbiór w punkcie po przedpłacie - <br/>PACZKA W RUCHu','paczkomaty' => 'Odbiór w punkcie - <br/>Paczkomaty 24/7','paczkomaty_przedplata' => 'Odbiór w punkcie po przedpłacie - <br/>Paczkomaty 24/7','e_przesylka' => 'Odbiór w punkcie - <br/>E-PRZESYŁKA','e_przesylka_przedplata' => 'Odbiór w punkcie po przedpłacie - <br/>E-PRZESYŁKA','dhl_przedplata' => 'Odbiór w punkcie po przedpłacie - <br/>DHL SERVICE POINT');
	}
	
	function sortByPole($result = null, $pole = 'id', $multi = null, $pole2 = '')
	{		
		if($result instanceof Zend_Db_Table_Rowset) $result = $result->toArray();
		//var_dump($result);die();
		if(is_array($result) && !empty($result) && count($result) > 0)
		{
			foreach($result as $row)
			{
				if($row instanceof Zend_Db_Table_Row) $row = $row->toArray();
				if(!empty($pole2))
				{
					if($multi === null) $results[$row[$pole]][$row[$pole2]] = $row;
					else if(empty($multi)) $results[$row[$pole]][$row[$pole2]][] = $row;
					else $results[$row[$pole]][$row[$pole2]][$row[$multi]] = $row;
				}
				else
				{
					if($multi === null) $results[$row[$pole]] = $row;
					else if(empty($multi)) $results[$row[$pole]][] = $row;
					else $results[$row[$pole]][$row[$multi]] = $row;
				}
			}
			return @$results;
		}
		else return null;
	}
	
	function wrapLongWord($nazwa = '', $maks = 30, $break = '<br/>', $preg = false)
	{
		if(mb_strlen($nazwa, "UTF-8") <= $maks) return $nazwa;
		$wrap = false;
		$nazwy = explode(" ", $nazwa);
		if(count($nazwy) > 0)
		foreach($nazwy as $n => $name)
		{
			if(mb_strlen($name, "UTF-8") > $maks)
			{
				$wrap = true;
				if($preg) break;
				$nazwy[$n] = chunk_split($name, $maks, $break);
			}
		}
		if($wrap)
		{
			if($preg)
			{
				$nowrap = 'a-zA-z ';
				$nowrap.= $this->getZnaki('pl', 'no', true);
				$nowrap.= $this->getZnaki('PL', 'no', true);
				$nazwa = preg_replace('/[^'.$nowrap.']/', $break, $nazwa);
			}
			else $nazwa = implode(" ", $nazwy);
		}
		return $nazwa;
	}
	
	function changePozycja($table = null, $id = 0, $pozycja = 0, $pole = 'pozycja')
	{
		if(empty($table)) return null;
        $update = array($pole => $pozycja);
        $where = 'id = '.$id;
		$this->db = $this->getDB($this->obConfig);
        $this->db->update(ucfirst($table), $update, $where);
    }
	
	function edytujIdsUpdate($dane, $ids = null, $table = '', $values = false)
	{
		if(empty($table)) return null;
		
        if(!$this->isArray($ids, true)) return null;
		$where = 'id in ('.implode(',', $ids).')';
		
		$sql = 'UPDATE '.ucfirst($table).' SET ';
		$ile = 0;
		if(count($dane) > 0)
		foreach($dane as $co => $dana)
		{
			if(!$values) $sql .= $co.' = '.$dana;
			else $sql .= $co.' = "'.$dana.'"';
			if($ile++ < count($dane) - 1) $sql .= ', ';
		}
		$sql .= ' WHERE '.$where;
		//echo $sql;die();
		$this->db = $this->getDB($this->obConfig);
		$this->db->query($sql);
		$this->cacheRemove(null, ucfirst($table));
    }
	
	function getZamienniki($one = true)
	{
		if($one)
		{
			$strony['aktualnosc'] = 'aktualnosci';
			$strony['artykul'] = 'artykuly';
			$strony['porada'] = 'porady';
			$strony['realizacja'] = 'realizacje';
		}
		else
		{
			$strony['aktualnosci'] = 'aktualnosc';
			$strony['artykuly'] = 'artykul';
			$strony['porady'] = 'porada';
			$strony['realizacje'] = 'realizacja';
		}
		return $strony;
	}
	
	function szukajTresci($fraza = '')
	{
		if(@empty($fraza)) return null;
		
		$this->getConfig();
		
		if($this->obConfig->podstrony)
		{
			$sql = $this->db->select();
			$tekst = 'if(p.short is not null, p.short, p.tekst)';
			$sql->from(array('p'=>'Podstrony'),array('link','nazwa'=>'p.temat','tekst'=>$tekst));
			//$sql->setIntegrityCheck(false);
			$where = 'p.lang = "'.$this->lang.'" and p.lp is not null';
			$where.= ' and (p.temat like "%'.$fraza.'%" or p.tekst like "%'.$fraza.'%")';
			$sql->where($where);
			//$sql->order('p.temat ASC');
			$sqls[] = $sql;
		}
		if($this->obConfig->asortyment)
		{
			$sql = $this->db->select();
			$tekst = 'if(0 && '.@intval($this->ustawienia['skrot']).', p.skrot, p.tekst)';
			$sql->from(array('p'=>'Produkty'),array('link','nazwa','tekst'=>$tekst));
			if($this->obConfig->parametry)
			$sql->joinleft(array('pa'=>'Parametry'),'p.id = pa.id_prod ',array(''));
			//$sql->setIntegrityCheck(false);
			$where = 'p.nazwa <> "" and p.lang = "'.$this->lang.'" and p.zmiana is not NULL and p.widoczny = "1"';
			if($this->obConfig->parametry)
			$where.= ' and (pa.id is null or (pa.lang = "'.$this->lang.'" and pa.widoczny = "1"))';
			$where.= ' and (';
			$where.= 'p.nazwa like "%'.$fraza.'%" or p.skrot like "%'.$fraza.'%" or p.tekst like "%'.$fraza.'%"';
			if($this->obConfig->parametry) $where.= ' or pa.nazwa like "%'.$fraza.'%" or pa.opis like "%'.$fraza.'%"';
			$where.= ')';
			$sql->where($where)->group('p.id');
			//$sql->order('p.temat ASC');
			$sqls[] = $sql;
		}
		if($this->obConfig->kategorie)
		{
			$sql = $this->db->select();
			$tekst = 'nazwa';
			if($this->obConfig->zdjeciaKategorie) $tekst = 'k.img';
			if($this->obConfig->opisKategorii) $tekst = 'k.opis';
			if($this->obConfig->opisKategoriiShort) $tekst = 'k.short';
			$sql->from(array('k'=>'Kategorie'),array('link','nazwa','tekst'=>$tekst));
			//$sql->setIntegrityCheck(false);
			$where = 'k.lang = "'.$this->lang.'" and k.widoczny = 1';
			$where.= ' and (k.nazwa like "%'.$fraza.'%" or k.opis like "%'.$fraza.'%")';
			$sql->where($where);
			//$sql->order('k.nazwa ASC');
			$sqls[] = $sql;
		}
		if($this->obConfig->menuOferta)
		{
			$sql = $this->db->select();
			$tekst = 'm.tekst';
			if($this->obConfig->menuSkrot) $tekst = 'm.skrot';
			$sql->from(array('m'=>'Menu'),array('link','nazwa','tekst'=>$tekst));
			if($this->obConfig->podstronyGaleria)
			{
				$whereGal = 'm.id = g.wlasciciel and g.typ = "menu" and m.lang = g.lang and g.wyswietl';
				$sql->joinleft(array('g'=>'Galeria'), $whereGal, array(''));
			}
			//$sql->setIntegrityCheck(false);
			$where = 'm.lang = "'.$this->lang.'" and m.wyswietl';
			$where.= ' and (';
			$where.= 'm.nazwa like "%'.$fraza.'%" or m.tekst like "%'.$fraza.'%"';
			if($this->obConfig->podstronyGaleria) $where.= ' or g.nazwa like "%'.$fraza.'%"';
			$where.= ')';
			$sql->where($where)->group('m.id');
			//$sql->order('m.nazwa ASC');
			$sqls[] = $sql;
		}
		if($this->obConfig->aktualnosci)
		{
			$sql = $this->db->select();
			$link = 'CONCAT("'.$this->makeLink($this->lang('Aktualnosc')).'/nr/",a.id)';
			$sql->from(array('a'=>'Aktualnosci'), array('link'=>$link,'nazwa'=>'a.temat','tekst'=>'a.skrot'));
			//$sql->setIntegrityCheck(false);
			$where = 'a.lang = "'.$this->lang.'" and a.wyswietl';
			$where.= ' and (a.temat like "%'.$fraza.'%" or a.tekst like "%'.$fraza.'%")';
			$sql->where($where);
			//$sql->order('a.temat ASC');
			$sqls[] = $sql;
		}
		if($this->obConfig->artykuly)
		{
			$sql = $this->db->select();
			$link = 'CONCAT("'.$this->makeLink($this->lang('Artykul')).'/nr/",a.id)';
			$sql->from(array('a'=>'Artykuly'), array('link'=>$link,'nazwa'=>'a.temat','tekst'=>'a.skrot'));
			//$sql->setIntegrityCheck(false);
			$where = 'a.lang = "'.$this->lang.'" and a.wyswietl';
			$where.= ' and (a.temat like "%'.$fraza.'%" or a.tekst like "%'.$fraza.'%")';
			$sql->where($where);
			//$sql->order('a.temat ASC');
			$sqls[] = $sql;
		}
		if($this->obConfig->porady)
		{
			$sql = $this->db->select();
			$link = 'CONCAT("'.$this->makeLink($this->lang('Porada')).'/nr/",p.id)';
			$sql->from(array('p'=>'Porady'), array('link'=>$link,'nazwa'=>'p.temat','tekst'=>'p.skrot'));
			//$sql->setIntegrityCheck(false);
			$where = 'p.lang = "'.$this->lang.'" and p.wyswietl';
			$where.= ' and (p.temat like "%'.$fraza.'%" or p.tekst like "%'.$fraza.'%")';
			$sql->where($where);
			//$sql->order('p.temat ASC');
			$sqls[] = $sql;
		}
		if($this->obConfig->realizacje)
		{
			$sql = $this->db->select();
			$link = 'CONCAT("'.$this->makeLink($this->lang('Realizacje')).'","")';
			$sql->from(array('r'=>'Realizacje'), array('link'=>$link,'nazwa'=>'r.temat','tekst'=>'r.skrot'));
			//$sql->setIntegrityCheck(false);
			$where = 'r.lang = "'.$this->lang.'" and r.wyswietl';
			$where.= ' and (r.temat like "%'.$fraza.'%" or r.tekst like "%'.$fraza.'%")';
			$sql->where($where);
			//$sql->order('r.temat ASC');
			$sqls[] = $sql;
		}
		if($this->obConfig->video)
		{
			$sql = $this->db->select();
			$link = 'CONCAT("'.$this->makeLink($this->lang('Video')).'","s")';
			$sql->from(array('v'=>'Video'), array('link'=>$link,'nazwa'=>'v.nazwa','tekst'=>'v.opis'));
			//$sql->setIntegrityCheck(false);
			$where = 'v.lang = "'.$this->lang.'" and v.wyswietl';
			$where.= ' and (v.nazwa like "%'.$fraza.'%" or v.opis like "%'.$fraza.'%")';
			$sql->where($where);
			//$sql->order('v.temat ASC');
			$sqls[] = $sql;
		}
		
		if(@count($sqls) == 0) return null;
        $sql = $this->db->select()->union($sqls, Zend_Db_Select::SQL_UNION_ALL)->order('nazwa asc');
		//echo $sql;die();
        return $this->db->fetchAll($sql);
	}
	
	function szukajFrazy($fraza = '', $pole = 'nazwa', $exact = false, $like = true, $or = false)
	{
		$where = '';
		//if($or) $where .= ' or (0';
		if($exact) $where .= ' or '.$pole.' = "'.$fraza.'"';
		else
		if($like) $where .= ' or '.$pole.' like "%'.$fraza.'%"';
		else
		{
			$where .= ' or '.$pole.' like "'.$fraza.'%"';
			$where .= ' or '.$pole.' like "% '.$fraza.'%"';
			$where .= ' or '.$pole.' like "%-'.$fraza.'%"';
			//$where.= ' or '.$pole.' like "%'.$fraza.'"';
		}
		//if($or) $where .= ' or 0)';
		return $where;
	}
	function szukajFraz($fraza = '', $pole = 'nazwa', $frazy = false, $exact = false, $like = true, $prod = false, $or = true)
	{
		$where = '';
		if($prod) $pole = 'p.nazwa';
		if(!$frazy)
		{
			$where .= ' and (0';
			$where .= $this->szukajFrazy($fraza, $pole, $exact, $like);
			if($prod && false && $this->obConfig->symbol)
			$where .= $this->szukajFrazy($fraza, 'p.oznaczenie', $exact, $like);
			if($prod && false)
			$where .= $this->szukajFrazy($fraza, 'p.tekst', false, true);
			$where .= ' or 0)';
		}
		else
		{
			$where .= $or ? ' and (0' : ' and (1';
			$words = explode(' ', trim($fraza));
			for($l = 0; $l < count($words); $l++)
			{
				$word = trim($words[$l]);
				if(empty($word)) continue;
				$where .= $or ? ' or (0' : ' and (0';
				$where .= $this->szukajFrazy($word, $pole, $exact, $like, true);
				if($prod && false && $this->obConfig->symbol)
				$where .= $this->szukajFrazy($word, 'p.oznaczenie', $exact, $like, true);
				if($prod && false)
				$where .= $this->szukajFrazy($word, 'p.tekst', false, true, true);
				$where .= $or ? ' or 0)' : ' or 0)';
			}
			$where .= $or ? ' or 0)' : ' and 1)';
		}
		return $where;
	}
	
	function rabat($cena_netto, $ilosc, $rabat, $vat)
	{
		$wartosc_rabat = $cena_netto * ($rabat / 100);
		$wartosc_rabat = floor($wartosc_rabat * 100) / 100;
		$wartosc_rabat *= $ilosc;
		return $this->obliczBruttoFromNettoVat($wartosc_rabat, $vat);
	}
	
	function fixTekst($tekst, $strip = true, $fullPath = false)
	{
		$this->getConfig();
		$linkFull = $this->obConfig->www.'/public/';
		$linkShort = str_replace('//', '/', '/'.$this->obConfig->baseUrl.'/public/');
		$link = $fullPath ? $linkFull : $linkShort;
		if(!empty($tekst))
		{
			if($strip) $tekst = stripslashes($tekst);
			$tekst = str_replace('../', '', $tekst);
			$tekst = str_replace('src="public/', 'src="'.$link, $tekst);
			$tekst = str_replace('href="public/', 'href="'.$link, $tekst);
			if($fullPath)
			{
				$tekst = str_replace('src="'.$linkShort, 'src="'.$linkFull, $tekst);
				$tekst = str_replace('href="'.$linkShort, 'href="'.$linkFull, $tekst);
			}
			if($this->module != 'admin')
			{
				$tekstStripped = trim(strip_tags($tekst, '<img><table><object><iframe>'));
				if(@empty($tekstStripped)) $tekst = '';
			}
		}
		return $tekst;
	}
	
	function getWojew()
	{
		return array('dolnoslaskie' => 'dolnośląskie', 'kujawsko-pomorskie' => 'kujawsko-pomorskie', 'lubelskie' => 'lubelskie',  'lubuskie' => 'lubuskie', 'lodzkie' => 'łódzkie', 'malopolskie' => 'małopolskie',  'mazowieckie' => 'mazowieckie', 'opolskie' => 'opolskie', 'podkarpackie' => 'podkarpackie', 'podlaskie' => 'podlaskie', 'pomorskie' => 'pomorskie', 'slaskie' => 'śląskie', 'swietokrzyskie' => 'świętokrzyskie',  'warminsko-mazurskie' => 'warmińsko-mazurskie', 'wielkopolskie' => 'wielkopolskie', 'zachodniopomorskie' => 'zachodniopomorskie');
	}
	function getMce($lang = 'pl')
	{
		if($lang == 'en') return array(1 => 'january', 2 => 'february', 3 => 'march', 4 => 'april', 5 => 'may',
		6 => 'june', 7 => 'july', 8 => 'august', 9 => 'september ', 10 => 'october', 11 => 'november', 12 => 'december');
		if($lang == 'de') return array(1 => 'januar', 2 => 'februar', 3 => 'märz', 4 => 'april', 5 => 'mai',
		6 => 'juni', 7 => 'juli', 8 => 'august', 9 => 'september', 10 => 'oktober', 11 => 'november', 12 => 'dezember');
		return array(1 => 'styczeń', 2 => 'luty', 3 => 'marzec', 4 => 'kwiecień', 5 => 'maj', 6 => 'czerwiec', 7 => 'lipiec', 8 => 'sierpień', 9 => 'wrzesień', 10 => 'październik', 11 => 'listopad', 12 => 'grudzień');
	}
	function getDni($lang = 'pl', $odmiana = 0)
	{
		if($lang == 'en') return array(1 => 'monday', 2 => 'thuesday', 3 => 'wednesday', 
						4 => 'thursday', 5 => 'friday', 6 => 'saturday', 7 => 'sunday');
		if($odmiana == 1) return array(1 => 'w poniedziałek', 2 => 'we wtorek', 3 => 
		'w środę', 4 => 'w czwartek', 5 => 'w piątek', 6 => 'w sobotę', 7 => 'w niedzielę');
		return array(1 => 'poniedziałek', 2 => 'wtorek', 3 => 'środa', 
		4 => 'czwartek', 5 => 'piątek', 6 => 'sobota', 7 => 'niedziela');		
	}
	static function getRomNum()
	{
		return array(1=>'I',2=>'II',3=>'III',4=>'IV',5=>'V',6=>'VI',7=>'VII',8=>'VIII',9=>'IX',10=>'X');
	}
	static function getMceString()
	{
		return array('January' => 'Styczeń', 'February' => 'Luty', 'March' => 'Marzec', 'April' => 'Kwiecień', 'May' => 'Maj', 'June' => 'Czerwiec', 'July' => 'Lipiec', 'August' => 'Sierpień', 'September' => 'Wrzesień', 'October' => 'Październik', 'November' => 'Listopad', 'December' => 'Grudzień');
	}
	function getJedn($glowna = false)
	{
		if($glowna)
		{
			$jednMiary = new Jednostkimiary($this->module);
			$jedn = $jednMiary->getDomyslna();
		}
		else
		{
			$jednMiary = new Jednostkimiary($this->module);
			$jedn = $jednMiary->wypisz();
		}
		return $jedn;
	}
	function getZamowieniaTypy($zmien = false)
	{
		$wiadomosci = new Wiadomosci();
		$typyAll = $wiadomosci->wypiszTypy('typ');
		
		$typy['system'] = $typyAll['system'];
		$typy['realizacja'] = $typyAll['realizacja'];
		if($zmien)
		$typy['zmiana'] = $typyAll['zmiana'];
		$typy['anulowane'] = $typyAll['anulowane'];
		$typy['wyslane'] = $typyAll['wyslane'];
		$typy['koniec'] = $typyAll['koniec'];
		
		$system = array('pl'=>'nowe','en'=>'new');
		$realizacja = array('pl'=>'w realizacji','en'=>'in realization');
		$zmiana = array('pl'=>'zmienione','en'=>'changed');
		$anulowane = array('pl'=>'anulowane','en'=>'cancelled');
		$wyslane = array('pl'=>'wysłane','en'=>'sent');
		$koniec = array('pl'=>'zrealizowane','en'=>'completed');
		
		$typy['system']['tytul'] = $system[$this->lang];
		$typy['realizacja']['tytul'] = $realizacja[$this->lang];
		if($zmien)
		$typy['zmiana']['tytul'] = $zmiana[$this->lang];
		$typy['anulowane']['tytul'] = $anulowane[$this->lang];
		$typy['wyslane']['tytul'] = $wyslane[$this->lang];
		$typy['koniec']['tytul'] = $koniec[$this->lang];
		
		$typy['system']['label'] = "no_label";
		$typy['realizacja']['label'] = "rel_label";
		if($zmien)
		$typy['zmiana']['label'] = "zmi_label";
		$typy['anulowane']['label'] = "er_label";
		$typy['wyslane']['label'] = "wys_label";
		$typy['koniec']['label'] = "ok_label";
		
		return $typy;
	}
	function getPlatnosci()
	{
		//$platnosci[] = 'błędny numer POS';
		//$platnosci[] = 'błędny podpis';
		$platnosci[] = 'nowa';
		$platnosci[] = 'rozpoczęta';
		$platnosci[] = 'oczekuje na odbiór';
		//$platnosci[] = 'autoryzacja odmowna';
		$platnosci[] = 'zwrot środków do klienta';
		$platnosci[] = 'odrzucona';
		$platnosci[] = 'anulowana';
		$platnosci[] = 'zakończona';
		//$platnosci[] = 'błędny status';
		//$platnosci[] = 'brak statusu';
		return $platnosci;
	}
	function getRodzajePlatnosci()
	{
		$array = array("POBRANIE" => "POBRANIE", "PRZELEW" => "PRZELEW");
		if($this->obConfig->platnosciOnline) $array["PRZELEW"] = "PRZELEW ZWYKŁY";
		if($this->obConfig->platnosciOnline) $array["ONLINE"] = "PRZELEW ONLINE";
		return $array;
	}
	function email($value)
	{
		return preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/', $value);
        //return (!eregi('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$', $value)) ? 0 : 1;
    }
	function isArray($array = null, $ids = false)
	{
		if(!$array || $array === null || empty($array) || !is_array($array) || count($array) == 0) return false;
		if($ids) return $this->isArray(array_filter($array, "is_numeric"), false);
		$reset = trim(reset($array));
		return strlen($reset) > 0;
	}
	function compare($result = null, $asoc = false, $sorter = 'nazwa', $func = '', $order = 'asc')
	{
		if($func == "int") $func = "intval";
		if($func == "float") $func = "floatval";
		$this->sorter = $sorter;
		$this->sortFunc = $func;
		$this->sortOrder = $order;
		if(true && !empty($this->sortFunc))
		if(is_array($result) && !empty($result) && count($result) > 0)
		{
			if($asoc) uasort($result, array($this, 'cmp'));
			else usort($result, array($this, 'cmp'));
		}
		return $result;
	}
	function cmp($a, $b)
	{
        $cmp1 = @$a[$this->sorter];
		$cmp2 = @$b[$this->sorter];
		if(isset($this->sortFunc) && !empty($this->sortFunc) && function_exists($this->sortFunc))
		{
			$sortFunc = $this->sortFunc;
			$cmp1 = $sortFunc($cmp1);
			$cmp2 = $sortFunc($cmp2);
		}
		if($cmp1 == $cmp2) return 0;
		if($this->sortOrder == 'asc') return ($cmp1 < $cmp2) ? -1 : 1;
		if($this->sortOrder == 'desc') return ($cmp1 > $cmp2) ? -1 : 1;
    }
	/*
	function build_sorter($key)
	{
		return function ($a, $b) use ($key)
		{
			//return strnatcmp($a[$key], $b[$key]);
			if ($a[$key] == $b[$key]) return 0;
			return ($a[$key] < $b[$key]) ? -1 : 1;
		};
	}
	*/
	
	function getCeny($keys = false)
	{
		$ceny['cena_netto'] = "Cena netto";
		$ceny['cena_brutto'] = "Cena brutto";
		$ceny['cena_promocji_n'] = "Cena netto promocyjna";
		$ceny['cena_promocji_b'] = "Cena brutto promocyjna";
		$ceny['cena_netto_hurt'] = "Cena netto hurtowa";
		$ceny['cena_brutto_hurt'] = "Cena brutto hurtowa";
		$ceny['cena_promocji_n_hurt'] = "Cena netto promocyjna hurtowa";
		$ceny['cena_promocji_b_hurt'] = "Cena brutto promocyjna hurtowa";
		return $keys ? array_keys($ceny) : $ceny;
	}
	function setCeny($cena = null, $promo = 0, $hurt = 0)
	{
		$ceny['cena_hurt_b'] = $promo ? $cena['cena_promocji_b_hurt'] : $cena['cena_brutto_hurt'];
		$ceny['cena_hurt_n'] = $promo ? $cena['cena_promocji_n_hurt'] : $cena['cena_netto_hurt'];
		$ceny['cena_detal_b'] = $promo ? $cena['cena_promocji_b'] : $cena['cena_brutto'];
		$ceny['cena_detal_n'] = $promo ? $cena['cena_promocji_n'] : $cena['cena_netto'];
		$ceny['cena_promocja_b'] = $hurt ? $cena['cena_promocji_b_hurt'] : $cena['cena_promocji_b'];
		$ceny['cena_promocja_n'] = $hurt ? $cena['cena_promocji_n_hurt'] : $cena['cena_promocji_n'];
		$ceny['cena_bezpromo_b'] = $hurt ? $cena['cena_brutto_hurt'] : $cena['cena_brutto'];
		$ceny['cena_bezpromo_n'] = $hurt ? $cena['cena_netto_hurt'] : $cena['cena_netto'];
		$netto = ($hurt&&$this->obConfig->cenyNettoDomyslneHurt) || (!$hurt&&$this->obConfig->cenyNettoDomyslneDetal);
		$ceny['cena_hurt'] = $netto ? $ceny['cena_hurt_n'] : $ceny['cena_hurt_b'];
		$ceny['cena_detal'] = $netto ? $ceny['cena_detal_n'] : $ceny['cena_detal_b'];
		$ceny['cena_promocja'] = $netto ? $ceny['cena_promocja_n'] : $ceny['cena_promocja_b'];
		$ceny['cena_bezpromo'] = $netto ? $ceny['cena_bezpromo_n'] : $ceny['cena_bezpromo_b'];
		$ceny['brutto'] = $promo ? $ceny['cena_promocja_b'] : $ceny['cena_bezpromo_b'];
		//$ceny['brutto'] = $hurt ? $ceny['cena_hurt_b'] : $ceny['cena_detal_b'];
		$ceny['netto'] = $promo ? $ceny['cena_promocja_n'] : $ceny['cena_bezpromo_n'];
		//$ceny['netto'] = $hurt ? $ceny['cena_hurt_n'] : $ceny['cena_detal_n'];
		$ceny['cena'] = $netto ? $ceny['netto'] : $ceny['brutto'];
		foreach($ceny as $c => $cena)
		$ceny[$c] = floatval($cena);
		return $ceny;
	}

	function obliczRabat($cena, $rabat)
	{
		if($rabat == 0) return floatval($cena);
        return max(0, round(floatval($cena) * (100.00 - floatval($rabat)) / 100, 2));
    }
	function obliczBruttoFromNettoVat($netto, $vat, $round = 2)
	{
        return max(0, round(floatval($netto) + (floatval($netto) * (intval($vat) / 100)), $round));
    }
	function obliczNettoFromBruttoVat($brutto, $vat, $round = 2)
	{
		return max(0, round((floatval($brutto) / (1 + intval($vat) / 100)), $round));
	}
	function obliczMarzaFromZakupNetto($zakup, $netto)
	{
		return round((floatval($netto) / floatval($zakup) - 1) * 100, 2);
	}
	function obliczNettoFromZakupMarza($zakup, $marza)
	{
		return max(0, round(floatval($zakup) + (floatval($zakup) * (floatval($marza) / 100.0)), 2));
	}
	
	public function produktyGaleria($prod, $zalogowanyID = 0, $hurt = false, $netto = false, $admin = false)
	{
		$module = $admin ? 'admin' : 'default';
		$this->getConfig();
		//$this->ustawienia = $this->getUstawienia();
		
		$user = new Zend_Session_Namespace('kontrahent');
		//$hurt = $this->view->typ_klienta;	
		$ids = null;
		for ($i = 0; $i < count($prod); $i++) 
		{
			$ids[] = $prod[$i]['id'];
		}
		
		$produkt = new Produkty($module);
		if(true && count($ids) == 1)
		{
			$galmain = new Galeria($module);
			$zdjecia = $galmain->getZdjeciaForIds($ids);
		}
		if(true && $this->obConfig->kategorie && count($ids) == 1)
		{
			$katprod = new Katprod($module);
			$kategorie = $katprod->getKategorieForIds($ids);
		}
		if($this->obConfig->producent)
		{
			if(!isset($this->producenci))
			{
				$producenci = new Producent($module);
				$this->producenci = $producenci->getProducenci();
			}
			$producenciAll = @$this->producenci;
		}
		if(false && $this->obConfig->tryb == 'rozmiary' && count($ids) == 1)
		{
			//$rozmiarowki = new Rozmiarowka($module);
			$rozmiarowkiAll = @$this->rozmiarowkiAll;
			$rozmiary = new Rozmiarproduktu($module);
			$rozmiaryAll = $rozmiary->wypiszForIds($ids);
		}
		if(true && $this->obConfig->kolory && count($ids) == 1)
		{
			$kolory = new Kolorproduktu($module);
			$koloryAll = $kolory->wypiszForIds($ids);
		}
		$rabaty = $this->obConfig->rabaty && (!$this->obConfig->rabatyKoszyk || @$this->obliczRabaty);
		if(true && $rabaty && $this->obConfig->rabaty)
		{
			//$rabatyUser = new Rabaty($module);
			$rabatyAll = @$this->rabatyAll;
			$rabatUser = $zalogowanyID > 0 ? @floatval($user->dane['rabat']) : 0;
			$rabatySumuj = $this->obConfig->rabatySumuj;
		}
		if(true && $this->obConfig->atrybuty && count($ids) == 1)
		{
			$atrybuty = new Atrybuty($module);
			$atrybutyAll = $atrybuty->getAtrybutyAllForIds($ids);
			//var_dump($atrybutyAll);die();
		}
		if($this->obConfig->ceny)
		{
			$ceny = $this->getCeny();
			if($this->obConfig->atrybutyCeny)
			$cenyAtrybuty = $produkt->getCena(0, $ids, $hurt, 0);
			if($this->obConfig->tryb == 'rozmiary')
			$cenyRozmiary = $rozmiary->wypiszCeny($ids, $hurt);
			//var_dump($ceny);die();
		}
		
		if($prod instanceof Zend_Db_Table_Rowset) $prod = $prod->toArray();
		
        for($i = 0; $i < count($prod); $i++) 
		{
			if($prod[$i] instanceof Zend_Db_Table_Row) $prod[$i] = $prod[$i]->toArray();
			//var_dump($prod);die();
			
			$kategs = @$kategorie[$prod[$i]['id']];
			$promo = $prod[$i]['promocja'];
			
			$tryb = $prod[$i]['rozmiarowka'] > 0 ? 'rozmiary' : 'produkt';
			$trybCena = $prod[$i]['trybCena'];
			
			$producent = @$producenciAll[$prod[$i]['producent']];
			$marka = @$producenciAll[$prod[$i]['marka']];
			$atrybut = @$atrybutyAll[$prod[$i]['id']];
			
			$img_main = array('img' => @$prod[$i]['img'], 'nazwa' => @$prod[$i]['img_opis']);
			if(isset($zdjecia))
			{
				//$img_main = @$zdjecia[$prod[$i]['id']][0][0];				
				if(@empty($img_main['img'])) $img_main = array('img' => 'brak.jpg', 'nazwa' => '');
				//unset($zdjecia[$prod[$i]['id']][0][0]);
				$galeria = @$zdjecia[$prod[$i]['id']][0];
				unset($zdjecia[$prod[$i]['id']][0]);
				$galeriaRozmiary = @$zdjecia[$prod[$i]['id']];
			}
			
			$ileKats = @count($kategs);
			
			$rabat = 0;
			if($rabaty && (!$promo || $this->obConfig->rabatyNaPromocje))
			{
				$rabatUserKateg = 0;
				if($this->obConfig->rabatyKlienciKategorie)
				{
					for($j = $ileKats - 1; $j >= 0; $j--)
					{
						$userKateg = @$rabatyAll['kategorie'][$kategs[$j]['id']];
						$rabatUserKateg = @floatval($userKateg['wartosc_rab']);
						if($rabatUserKateg > 0) break;
					}
					if($rabatySumuj || $rabat == 0) $rabat += $rabatUserKateg;
				}
				$rabatUserProduktGrupa = 0;
				if($this->obConfig->rabatyKlienciProduktyGrupy)
				{
					$rabatKlientProduktGrupa = @$rabatyAll['produktyGrupy'][$prod[$i]['grupa']];
					$rabatUserProduktGrupa = @floatval($rabatKlientProduktGrupa['wartosc_rab']);
					if($rabatySumuj || $rabat == 0) $rabat += $rabatUserProduktGrupa;
				}
				$rabatUserProdukt = 0;
				if($this->obConfig->rabatyKlienciProdukty)
				{
					$rabatKlientProdukt = @$rabatyAll['produkty'][$prod[$i]['id']];
					$rabatUserProdukt = @floatval($rabatKlientProdukt['wartosc_rab']);
					if($rabatySumuj || $rabat == 0) $rabat += $rabatUserProdukt;
				}
				$rabatUserProducent = 0;
				if($this->obConfig->rabatyKlienciProducent)
				{
					$rabatyKlienciProducent = @$rabatyAll['producent'][$prod[$i]['producent']];
					$rabatUserProducent = @floatval($rabatyKlienciProducent['wartosc_rab']);
					if($rabatySumuj || $rabat == 0) $rabat += $rabatUserProducent;
				}
				$rabatKateg = 0;
				if($this->obConfig->rabatyKategorie)
				{
					for($j = $ileKats - 1; $j >= 0; $j--)
					{
						$rabatKateg = @floatval($kategs[$j]['rabat']);
						if($rabatKateg > 0) break;
					}
					if($rabatySumuj || $rabat == 0) $rabat += $rabatKateg;
				}
				$rabatProducent = 0;
				if($this->obConfig->rabatyProducenci)
				{
					$rabatProducent = @floatval($producent['rabat']);
					if($rabatySumuj || $rabat == 0) $rabat += $rabatProducent;
				}
				$rabatProdukt = 0;
				if($this->obConfig->rabatyProdukty)
				{
					$rabatProdukt = @floatval($prod[$i]['rabat']);
					if($rabatySumuj || $rabat == 0) $rabat += $rabatProdukt;
				}
				if($this->obConfig->rabatyMaks)
				$rabat = max($rabatKateg,$rabatUserKateg,$rabatUserProduktGrupa,$rabatUserProdukt,$rabatUserProducent,$rabatProducent,$rabatProdukt,$rabatUser);
			}
			
			//$rozmiary->id = $prod[$i]['id'];
			if($tryb == 'rozmiary')
			{
				$dostepnosc = 0;
				$rozmiar = @$rozmiaryAll[$prod[$i]['id']];//$rozmiary->wypisz($prod[$i]['rozmiarowka']);
				if(count($rozmiar) > 0)
				foreach($rozmiar as $r => $rozm)
				{
					if(!$this->ustawienia['stany_mag']) $rozmiar[$r]['dostepnosc'] = 99999;
					if($this->obConfig->dostepny && $rozmiar[$r]['dostepny'] == 0) $rozmiar[$r]['dostepnosc'] = 0;
					$dostepnosc += $rozmiar[$r]['dostepnosc'];
					if(!$this->obConfig->ceny) continue;
					$cenyFrom = $prod[$i]['trybCena'] == 'rozmiary' ? $rozmiar[$r] : $prod[$i];
					$prodCeny = $this->setCeny($cenyFrom, $promo, $hurt);
					if(true)
					{
						//$brutto = $this->obliczRabat($brutto, $rabat);
						//$netto = $this->obliczRabat($netto, $rabat);
						if(@count($ceny) > 0)
						foreach($ceny as $nazwa => $label)
						$cenyFrom[$nazwa.'_rabat'] = $this->obliczRabat($cenyFrom[$nazwa], $rabat);
						if(count($prodCeny) > 0)
						foreach($prodCeny as $nazwa => $cena)
						$prodCeny[$nazwa.'_rabat'] = $this->obliczRabat($cena, $rabat);
					}
					if(@count($ceny) > 0)
					foreach($ceny as $nazwa => $label)
					$rozmiar[$r][$nazwa] = $cenyFrom[$nazwa];
					if(@count($prodCeny) > 0)
					foreach($prodCeny as $nazwa => $cena)
					$rozmiar[$r][$nazwa] = $cena;
				}
				//var_dump($rozmiar);die();
				$rozmiarowki->id = $prod[$i]['rozmiarowka'];
				$rozmiarowka = @$rozmiarowkiAll[$prod[$i]['rozmiarowka']];//$rozmiarowki->wypiszPojedynczego();
				$rozmiarowka['jedn_miary_ile'] = $prod[$i]['jedn_miary_ile'];
				$rozmiarowka['jedn_miary_opak'] = $prod[$i]['jedn_miary_opak'];
				$rozmiarowka['jedn_miary_zb'] = $prod[$i]['jedn_miary_zb'];
				$rozmiarowka['jedn_miary_zb_ile'] = $prod[$i]['jedn_miary_zb_ile'];
				$rozmiarowka['jedn_miary_zb_typ'] = $prod[$i]['jedn_miary_zb_typ'];
				$rozmiarowka['jedn_miary_zb_czy'] = $prod[$i]['jedn_miary_zb_czy'];
			}
			
			//$kolory->id = $prod[$i]['id'];
			$kolor = @$koloryAll[$prod[$i]['id']];//$kolory->wypisz();
			
			if($trybCena == 'rozmiary')
			{
				$cenyFrom = @$cenyRozmiary[$prod[$i]['id']];
				if($cenyFrom === null) $cenyFrom = $prod[$i];
				$prodCeny = $this->setCeny($cenyFrom, $promo, $hurt);
			}
			if($trybCena == 'produkt')
			{
				$cenyFrom = $prod[$i];
				$prodCeny = $this->setCeny($cenyFrom, $promo, $hurt);
			}
			if(false && isset($prod[$i]['cena_min']))
			{
				if($this->obConfig->cenyNettoDomyslne)
				{
					$netto = $prod[$i]['cena_min'];
					$brutto = $this->obliczBruttoFromNettoVat($netto, $prod[$i]['vat']);
				}
				else
				{
					$brutto = $prod[$i]['cena_min'];
					$netto = $this->obliczNettoFromBruttoVat($brutto, $prod[$i]['vat']);
				}
				$prodCeny['brutto'] = $brutto;
				$prodCeny['netto'] = $netto;
			}
			if($this->obConfig->atrybutyCeny)
			{				
				$cenyFrom = @$cenyAtrybuty[$prod[$i]['id']];
				if(count($cenyFrom) > 0 && @$cenyFrom['cena_min'] > 0)
				{
					$prodCeny = null;
					foreach($cenyFrom as $nazwa => $cena)
					{
						if(strpos($nazwa, 'brutto') !== false)
						{
							$nazwaBrutto = $nazwa;
							$nazwaNetto = str_replace('brutto', 'netto', $nazwa);
						}
						else if(substr($nazwa, -2) == '_b' || strpos($nazwa, '_b_') !== false)
						{
							$nazwaBrutto = $nazwa;
							$nazwaNetto = str_replace('_b', '_n', $nazwa);
						}
						else
						{
							$nazwaBrutto = $nazwa.'_b';
							$nazwaNetto = $nazwa.'_n';
						}
						$prodCeny[$nazwaBrutto] = $cena;
						$prodCeny[$nazwaNetto] = $this->obliczNettoFromBruttoVat($cena, $prod[$i]['vat']);
					}
					$cenyFrom = $this->setCeny($prodCeny, $promo, $hurt);
					if(count($cenyFrom) > 0)
					foreach($cenyFrom as $nazwa => $cena)
					$prodCeny[$nazwa] = $cena;
					$prodCeny['brutto'] = $prodCeny['cena_min_b'];
					$prodCeny['netto'] = $prodCeny['cena_min_n'];
				}
				//var_dump($prodCeny);die();
			}
			if($this->obConfig->ceny)
			{
				//$brutto = $this->obliczRabat($brutto, $rabat);
				//$netto = $this->obliczRabat($netto, $rabat);
				if(@count($ceny) > 0)
				foreach($ceny as $nazwa => $label)
				$prod[$i][$nazwa.'_rabat'] = $this->obliczRabat($prod[$i][$nazwa], $rabat);
				if(count($prodCeny) > 0)
				foreach($prodCeny as $nazwa => $cena)
				$prodCeny[$nazwa.'_rabat'] = $this->obliczRabat($cena, $rabat);
			}
			
			if($tryb == 'produkt')
			{
				$rozm = array();
				$rozm['id'] = 0;
				$rozm['nazwa'] = '';
				$rozm['dostepnosc'] = 99999;
				if($this->ustawienia['stany_mag']) $rozm['dostepnosc'] = $prod[$i]['dostepnosc'];
				if($this->obConfig->dostepny && $prod[$i]['dostepny'] == 0) $rozm['dostepnosc'] = 0;
				$dostepnosc = $rozm['dostepnosc'];
				$rozm['opis'] = $prod[$i]['tekst'];
				//$rozm['netto'] = $netto;
				//$rozm['brutto'] = $brutto;
				if(@count($ceny) > 0)
				foreach($ceny as $nazwa => $label)
				{
					$rozm[$nazwa] = floatval($prod[$i][$nazwa]);
					$rozm[$nazwa.'_rabat'] = $prod[$i][$nazwa.'_rabat'];
				}
				if(@count($prodCeny) > 0)
				foreach($prodCeny as $nazwa => $cena)
				{
					$rozm[$nazwa] = $cena;
				}
				
				$rozmiar = array($rozm);
				//var_dump($rozm);die();				
				$rozmiarow['id'] = 0;
				$rozmiarow['nazwa'] = '';
				$rozmiarow['jedn_miary'] = $prod[$i]['jedn_miary'];
				$rozmiarow['jedn_miary_typ'] = $prod[$i]['jedn_miary_typ'];
				$rozmiarow['jedn_miary_ile'] = $prod[$i]['jedn_miary_ile'];
				$rozmiarow['jedn_miary_opak'] = $prod[$i]['jedn_miary_opak'];
				$rozmiarow['jedn_miary_zb'] = $prod[$i]['jedn_miary_zb'];
				$rozmiarow['jedn_miary_zb_ile'] = $prod[$i]['jedn_miary_zb_ile'];
				$rozmiarow['jedn_miary_zb_typ'] = $prod[$i]['jedn_miary_zb_typ'];
				$rozmiarow['jedn_miary_zb_czy'] = $prod[$i]['jedn_miary_zb_czy'];
				$rozmiarowka = $rozmiarow;
			}
			
			$skrot = $prod[$i]['skrot'];
			if(!empty($skrot)) $skrot = $this->fixTekst($skrot);
			$tekst = $prod[$i]['tekst'];
			if(!empty($tekst)) $tekst = $this->fixTekst($tekst);
			$tekst_allegro = $prod[$i]['tekst_allegro'];
			if(!empty($tekst_allegro)) $tekst_allegro = $this->fixTekst($tekst_allegro);
				
            $produkty[$i] = array
			(
                'id' => $prod[$i]['id'],
				'data' => $prod[$i]['data'],
				'odwiedziny' => $prod[$i]['odwiedziny'],
                'nazwa' => $prod[$i]['nazwa'],
				'nazwa_allegro' => $prod[$i]['nazwa_allegro'],
				'cena_allegro' => $prod[$i]['cena_allegro'],
				'kateg' => $kategs,
				'kod' => $prod[$i]['oznaczenie'], 
                'producent' => $producent,
				'marka' => $marka,
				'vat' => $prod[$i]['vat'],
				'waga' => $prod[$i]['waga'],
				'gabaryt' => $prod[$i]['gabaryt'],
				'rabat' => $prod[$i]['rabat'],
				'rabat_obl' => @$rabat,
				'grupa' => $prod[$i]['grupa'],
                'widoczny' => $prod[$i]['widoczny'],
                'dostepny' => $prod[$i]['dostepny'],
				'dostepne' => $prod[$i]['dostepne'],
				'na_zamow' => $prod[$i]['na_zamow'],
                'promocja' => $prod[$i]['promocja'],
				'promocja_net' => $prod[$i]['promocja_net'],
                'nowosc' => $prod[$i]['nowosc'],
				'glowna' => $prod[$i]['glowna'],
				'polecane' => $prod[$i]['polecane'],
				'wyprzedaz' => $prod[$i]['wyprzedaz'],
				'specjalne' => $prod[$i]['specjalne'],
				'produkt_dnia' => $prod[$i]['produkt_dnia'],
                'skrot' => $skrot,
                'tekst' => $tekst,
				'tekst_allegro' => $tekst_allegro,
                'zdj' => @$img_main,
				'galeria' => @$galeria,
				'galeriaRozmiary' => @$galeriaRozmiary,
                'link' => $prod[$i]['link'],
				'dostepnosc' => @$dostepnosc,
				'jedn_miary' => $prod[$i]['jedn_miary'],
				'jedn_miary_typ' => $prod[$i]['jedn_miary_typ'],
				'jedn_miary_ile' => $prod[$i]['jedn_miary_ile'],
				'jedn_miary_opak' => $prod[$i]['jedn_miary_opak'],
				'jedn_miary_zb' => $prod[$i]['jedn_miary_zb'],				
				'jedn_miary_zb_ile' => $prod[$i]['jedn_miary_zb_ile'],
				'jedn_miary_zb_typ' => $prod[$i]['jedn_miary_zb_typ'],
				'jedn_miary_zb_czy' => $prod[$i]['jedn_miary_zb_czy'],
				'rozmiarowka' => @$rozmiarowka,
				'rozmiary' => @$rozmiar,
				'kolory' => @$kolor,
				'trybCena' => $prod[$i]['trybCena'],
				'atrybuty' => @$atrybut,
				'pozycja' => $prod[$i]['pozycja'],
				'ean' => $prod[$i]['ean'],
				//'stan_min' => $prod[$i]['stan_min'],
				'cena_atryb' => $prod[$i]['cena_atryb'],
				'upust_detal' => $prod[$i]['upust_detal'],
				'upust_hurt' => $prod[$i]['upust_hurt'],
				'upust' => $hurt ? $prod[$i]['upust_hurt'] : $prod[$i]['upust_detal'],
				'facebookpostid' => $prod[$i]['facebookpostid']
            );
			if(isset($prod[$i]['ilosc'])) $produkty[$i]['ilosc'] = $prod[$i]['ilosc'];
			if(isset($prod[$i]['wartosc'])) $produkty[$i]['wartosc'] = $prod[$i]['wartosc'];
			if(isset($prod[$i]['date'])) $produkty[$i]['date'] = $prod[$i]['date'];
			
			if(@count($ceny) > 0)
			foreach($ceny as $nazwa => $label)
			{
				$produkty[$i][$nazwa] = floatval($prod[$i][$nazwa]);
				$produkty[$i][$nazwa.'_rabat'] = $prod[$i][$nazwa.'_rabat'];
			}
			if(@count($prodCeny) > 0)
			foreach($prodCeny as $nazwa => $cena)
			{
				$produkty[$i][$nazwa] = $cena;
			}
			//var_dump($produkty[$i]);die();
        }
        return isset($produkty) ? $produkty : null;
    }
	
	function getZnaki($lang = 'all', $ok = 'no', $asString = false)
	{
		$znaki['pl']['no'] = array('ą','ę','ć','ś','ź','ż','ł','ó','ń');
		$znaki['pl']['ok'] = array('a','e','c','s','z','z','l','o','n');
		$znaki['PL']['no'] = array('Ą','Ę','Ć','Ś','Ź','Ż','Ł','Ó','Ń');
		$znaki['PL']['ok'] = array('A','E','C','S','Z','Z','L','O','N');
		//wegierskie á, Á, é, É, ó, Ó, ö, Ö, ő, Ő, ű, Ű, ú, Ú
		$znaki['all']['no'] = array('Ń','Ä','ä','Ö','ö','Ü','ü','ß','Ñ','ñ','á','Á','í','ó','Ó','ú','Ú','À','à','Â','â','Ç','ç','É','é','È','è','Ê','ê','Ë','ë','Î','î','Ï','ï','Ô','ô','Œ','œ','Ù','ù','Û','û','Ü','ü','Ÿ','ÿ','A','а','Б','б','В','в','Г','г','Ґ','ґ','Д','д','Е','е','Є','є','Ж','ж','З','з','И','и','І','і','Ї','ї','Й','й','К','к','Л','л','М','м','Н','н','О','о','П','п','Р','р','С','с','Т','т','У','у','Ф','ф','Х','х','Ц','ц','Ч','ч','Ш','ш','Щ','щ','Ь','ь','Я','я','Ő','ő','Ű','ű','Ю','ю','Æ','æ');
		$znaki['all']['ok'] = array('N','A','a','O','o','U','u','B','N','n','a','A','i','o','O','u','U','A','a','A','a','C','C','E','e','E','e','E','e','E','e','I','i','I','i','O','o','CE','ce','U','u','U','u','U','u','Y','y','A','a','B','b','B','b','T','t','T','t','Д','д','E','e','E','e','X','x','3','3','N','n','I','i','I','i','N','n','K','K','N','N','M','m','H','h','O','o','N','n','P','p','C','c','T','t','Y','y','Ф','ф','X','x','U','u','Y','y','M','m','M','m','B','b','R','r','O','o','U','u','I0','io','AE','ae');
		return $asString ? @implode('', $znaki[$lang][$ok]) : @$znaki[$lang][$ok];
	}
	
	function makeLink($nazwa, $ucFirst = true)
	{		
		$puste = array('"',"'",'.',',',';',':');
		$nazwa = str_replace($puste, '', $nazwa);
		
		$pauza = array(' ','+','?','*','^','!','/',"\\",'(',')','#','&','--');
		$nazwa = str_replace($pauza, '-', $nazwa);
		
		$polishNO = $this->getZnaki('pl', 'no');
		$polishOK = $this->getZnaki('pl', 'ok');
		$nazwa = str_replace($polishNO, $polishOK, $nazwa);
		
		$polishBigNO = $this->getZnaki('PL', 'no');
		$polishBigOK = $this->getZnaki('PL', 'ok');
		$nazwa = str_replace($polishBigNO, $polishBigOK, $nazwa);
		
		$znakiLangsNO = $this->getZnaki('all', 'no');
		$znakiLangsOK = $this->getZnaki('all', 'ok');
		$nazwa = str_replace($znakiLangsNO, $znakiLangsOK, $nazwa);
		
		//var_dump($nazwa);die();
		
		$nazwa = preg_replace('/[^A-Za-z0-9\-]/', '', $nazwa);
		$nazwa = strtolower($nazwa);
        if($ucFirst) $nazwa = ucfirst($nazwa);
		
        return $nazwa;
    }
	
	function uploadFile($file,$dir,$name,$x=800,$y=600,$ratioX=false,$ratioY=false,$resize=false,$watermark=false,$watermarkLogo='../public/images/strona/logo_small.png',$watermarkX=5,$watermarkY=5,$watermarkPos='LR')
	{
		$handle = @new upload($file);
		if($handle->uploaded) 
		{
			$handle->file_new_name_body = $name;
			$handle->image_resize = $resize;
			$handle->image_x = $x;
			$handle->image_y = $y;
			$handle->image_ratio_x = $ratioX;
			$handle->image_ratio_y = $ratioY;
			if($watermark) 
			{
				$handle->image_watermark = $watermarkLogo;
				$handle->image_watermark_x = $watermarkX;
				$handle->image_watermark_y = $watermarkY;//echo 'yy';
				$handle->image_watermark_position = $watermarkPos;//TBLR
			}
			$handle->process($dir);			
			if($handle->processed) 
			{
				$handle->clean();
				return array('error' => '', 'file' => $handle->file_dst_name);
			}
			else 
			{
				$error = '<div class="k_blad">Upload nieudany. Nie można utworzyć miniaturki.</div>';
				return array('error' => $error, 'file' => '');
			}
		}
		else 
		{
			$error = '<div class="k_blad">Upload nieudany. Nie można przenieść zdjęcia na serwer.</div>';
			return array('error' => $error, 'file' => '');
		}
	}
	
	function galeriaSupport($id, $typ = 'oferta', $_request)
	{
		$img = new Galeria();
		$this->obConfig = $this->getObConfig('image');
		
		$imgMain = intval($_request->getParam('imgMain', 0));
		$imgDel = intval($_request->getParam('imgDel', 0));
		$imgEdit = intval($_request->getParam('imgEdit', 0));
		$imgPoz = intval($_request->getParam('imgPoz', 0));
		$imgShow = intval($_request->getParam('imgShow', 0));
		
		if($imgMain > 0)
		{
			$img->ustawGlowne($imgMain, $id, 0, $typ);
		}
		if($imgDel > 0)
		{
			$img->usunZdjecie($imgDel);
		}
		if($imgPoz > 0)
		{
			$img->changePozycja($imgPoz, intval($_request->getParam('poz', 0)));
		}
		if($imgShow > 0)
		{
			$img->zmienNazwe(array('wyswietl' => intval($_request->getParam('widoczny', 0))), $imgShow);
		}		
		
		$images = $_request->getPost('images');
		//var_dump($_FILES);var_dump($images);die();
		if(count($images) > 0)
		{
			unset($images['zapisz']);
			if(isset($_FILES['zdj']) && @empty($_FILES['zdj']['tmp_name'][0]))
			{
				return null;//'<div id="lista_bledow">Pole plik jest wymagane.</div>';
			}
			else 
			{
				$dir = $this->obConfig->{ucfirst($typ)};
				$sizeX = $this->obConfig->{ucfirst($typ).'X'};
				$sizeY = $this->obConfig->{ucfirst($typ).'Y'};

				if(isset($_FILES['zdj']))
				for($i = 0; $i < count($_FILES['zdj']['tmp_name']); $i++)
				{
					//set_time_limit(60);
					$file = array();
					$file['name'] = $_FILES['zdj']['name'][$i];
					$file['type'] = $_FILES['zdj']['type'][$i];
					$file['size'] = $_FILES['zdj']['size'][$i];
					$file['tmp_name'] = $_FILES['zdj']['tmp_name'][$i];
					$file['error'] = $_FILES['zdj']['error'][$i];
					$pathinfo = pathinfo($file['name']);
					$name = $pathinfo['filename'];

					$result = $this->uploadFile($file, $dir, $name, $sizeX, $sizeY, 0, 0, false);

					if(@empty($result['error']))
					{
						$idNew = $img->zapiszDoBazy($result['file'], $id, $images[$i], 0, $typ);
						$gal = $img->wyswietlGalerie($id, 'desc', 0, $typ);
						if(count($gal) == 1) $img->ustawGlowne($idNew, $id, 0, $typ);
					}
					else 
					{
						return '<div class="k_blad">'.$result['error'].'</div>';
					}
				}
			}
		}
		
		$image = $_request->getPost('image');
		if(count($image) > 0)
		{
			$zmienNazwe = array('nazwa' => $image['nazwa']);
			$img->zmienNazwe($zmienNazwe, $image['id']);
		}
		
		$usunImgs = $_request->getPost('usunImg');
		if(count($usunImgs) > 0)
		{
			foreach($usunImgs as $usunImg)
			{
				$img->usunZdjecie($usunImg);
			}
		}
	}
	
	function videoSupport($id = 0, $typ = '', $_request)
	{
		$vid = new Video();
		$this->obConfig = $this->getObConfig('general');
		
		$vidMain = intval($_request->getParam('vidMain', 0));
		$vidDel = intval($_request->getParam('vidDel', 0));
		$vidEdit = intval($_request->getParam('vidEdit', 0));
		$vidPoz = intval($_request->getParam('vidPoz', 0));
		$vidShow = intval($_request->getParam('vidShow', 0));
		
		if($vidMain > 0)
		{
			//$vid->ustawGlowne($vidMain, $id, 0, $typ);
		}
		if($vidDel > 0)
		{
			$vid->id = $vidDel;
			$vid->usun();
		}
		if($vidPoz > 0)
		{
			$this->changePozycja('Video', $vidPoz, intval($_request->getParam('poz', 0)));
		}
		if($vidShow > 0)
		{
			$vid->id = $vidShow;
			$vid->edytuj(array('wyswietl' => intval($_request->getParam('widoczny', 0))));
		}
		
		$videos = $_request->getPost('videos');
		if(count($videos) > 0)
		{
			$videos['id_ext'] = $id;
			$videos['typ'] = $typ;
			$videos['lang'] = $this->lang;
			$videos['wyswietl'] = '1';
			if(@!empty($videos['www']))
			$vid->dodaj($videos);
		}
		
		$video = $_request->getPost('video');
		if(count($video) > 0)
		{
			unset($video['zmienNazweVideo']);
			$vid->id = @intval($video['id']);
			$vid->edytuj($video);
		}
		
		$usunVids = $_request->getPost('usunVids');
		if(count($usunVids) > 0)
		{
			foreach($usunVids as $usunVid)
			{
				$vid->id = $usunVid;
				$vid->usun();
			}
		}
	}
	
	function paramsSupport($id = 0, $typ = '', $_request)
	{
		$par = new Parametry();
		$this->obConfig = $this->getObConfig('general');
		
		$parMain = intval($_request->getParam('parMain', 0));
		$parDel = intval($_request->getParam('parDel', 0));
		$parDelAll = intval($_request->getParam('parDelAll', 0));
		$parEdit = intval($_request->getParam('parEdit', 0));
		$parPoz = intval($_request->getParam('parPoz', 0));
		$parShow = intval($_request->getParam('parShow', 0));
		
		if($parMain > 0)
		{
			//$par->ustawGlowne($parMain, $id, 0, $typ);
		}
		if($parDel > 0)
		{
			$par->id = $parDel;
			$par->usunID();
		}
		if($parDelAll > 0)
		{
			$par->id = $parDelAll;
			$par->usunParam($id);
		}
		if($parPoz > 0)
		{
			$this->changePozycja('Parametry', $parPoz, intval($_request->getParam('poz', 0)));
		}
		if($parShow > 0)
		{
			$par->id = $parShow;
			$par->edytuj(array('widoczny' => intval($_request->getParam('widoczny', 0))));
		}
		
		//var_dump($_FILES);die();
		if(isset($_FILES['paramOne']) || isset($_FILES['paramsOne']))
		{
			$sizeX = 60;
			$sizeY = 60;
			$dir = '../public/admin/parametry/';
			if(@!empty($_FILES['paramOne']['tmp_name'])) $file = $_FILES['paramOne'];
			if(@!empty($_FILES['paramsOne']['tmp_name'])) $file = $_FILES['paramsOne'];
			if(@count($file) > 0)
			{
				$pathinfo = pathinfo($file['name']);
				$name = $pathinfo['filename'];				
				$result = $this->uploadFile($file, $dir, $name, $sizeX, $sizeY, 0, 0, false);
				if(@empty($result['error']))
				{
					if(@!empty($_FILES['paramOne']['tmp_name'])) $paramImg = $result['file'];
					if(@!empty($_FILES['paramsOne']['tmp_name'])) $paramsImg = $result['file'];
				}
				else 
				{
					return '<div class="k_blad">'.$result['error'].'</div>';
				}
			}
		}
		
		$params = $_request->getPost('params');
		if(count($params) > 0)
		{
			$params['id_prod'] = $id;
			$maxParam = $par->wypiszMaxParamForProdukt($id);
			$params['id_param'] = $maxParam + 1;
			$params['id_atryb'] = 0;
			$params['lang'] = $this->lang;
			$params['widoczny'] = '1';
			if(@!empty($params['nazwa']))
			$par->dodaj($params);
		}		
		$paramsOne = $_request->getPost('paramsOne');
		if(count($paramsOne) > 0)
		{
			$paramsOne['id_prod'] = $id;
			$paramsOne['id_param'] = @intval($paramsOne['id_param']);
			$maxAtryb = $par->wypiszMaxAtrybForProduktParam($id, $paramsOne['id_param']);
			$paramsOne['id_atryb'] = $maxAtryb + 1;
			$paramsOne['img'] = @strval($paramsImg);
			$paramsOne['lang'] = $this->lang;
			$paramsOne['widoczny'] = '1';
			if(@!empty($paramsOne['nazwa']))
			$par->dodaj($paramsOne);
		}
		
		$param = $_request->getPost('param');
		if(count($param) > 0)
		{
			unset($param['zmienNazweParam']);
			$par->id = @intval($param['id']);
			$par->edytuj($param);
		}		
		$paramOne = $_request->getPost('paramOne');
		if(count($paramOne) > 0)
		{
			unset($paramOne['zmienNazweParam']);
			if(@!empty($paramImg))
			$paramOne['img'] = @strval($paramImg);
			$par->id = @intval($paramOne['id']);
			$par->edytuj($paramOne);
		}
		
		$usunPars = $_request->getPost('usunPars');
		if(count($usunPars) > 0)
		{
			foreach($usunPars as $usunPar)
			{
				$par->id = $usunPar;
				$par->usun();
			}
		}
	}
	
	function getGaleria($id = 0, $typ = '', $module = 'default')
	{
		$this->getConfig();
		if($this->obConfig->podstronyGaleria)
		{
			$galeria = new Galeria($module);
			return $galeria->wyswietlGalerie($id, 'desc', 0, $typ, true);
		}
		else return null;
	}
	function getVideos($id = 0, $typ = '')
	{
		$this->getConfig();
		if($this->obConfig->podstronyVideo)
		{
			$video = new Video($module);
			return $video->wypisz(true, $id, $typ, true);
		}
		else return null;
	}
	
	function getTableNameFromSelect($sql)
	{
		//`Routers`.* `Routers` Routers
		$sql = str_replace('`', '', $sql);
		$from = strripos($sql, 'from ');
		//var_dump($sql);
		if($from !== false)
		{
			$from = substr($sql, $from + 5);
			$from = trim($from);
			$spacja = strpos($from, ' ');
			if($spacja == false) $table = $from;
			else $table = substr($from, 0, $spacja);
			$table = preg_replace('/[^A-Za-z0-9_]/', '', $table);
			//var_dump($table);
			return $table;
		}
		return null;
	}
	
	function getCacheData($sql)
	{
		$session = new Zend_Session_Namespace();
        $data['cache'] = $session->cache;
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'general');
		$data['caching'] = $obConfig->caching;
		
		if(!$data['cache'] || !$data['caching']) return $data;
		
		if($sql instanceof Zend_Db_Select) $sql = $sql->assemble();
		$data['tableName'] = $this->getTableNameFromSelect($sql);
		
		if(!empty($data['tableName']))
		{
			$data['cacheName'] = $data['tableName'].'_'.md5($sql);
			$data['cacheTags'] = array($data['tableName'], $data['cacheName']);
		}
		else
		{
			$data['cacheName'] = md5($sql);
			$data['cacheTags'] = array($data['cacheName']);
		}
		//var_dump($sql);
		//var_dump($data['cacheName']);
		return $data;
	}
	
	public function cacheRemove($sql, $table, $where = null)
	{
		//var_dump($sql);
		$session = new Zend_Session_Namespace();
		$cache = $session->cache;
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'general');
		$caching = $obConfig->caching;
		$cachePath = $obConfig->cachePath;//'/public/cache';
		$cachePre = $obConfig->cachePre;//'zend_cache---';
		$cachePrefix = $cachePath.'/'.$cachePre;
		//var_dump($sql);die();
		//$common = new Common();
		$tables = $this->getCacheTables();
		//var_dump($tables);die();
		if($cache && $caching)
		{
			//@unlink($cachePrefix.$table);
			if($table == 'Produkty')
			{
				$settings = new Ustawienia();
				$settings->updateData(array('zmiana' => date('Y-m-d H:i:s')));
			}
			$cache->remove($table);
			$cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, array($table));
			if(isset($tables[$table]) && count($tables[$table]) > 0)
			$cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_TAG, $tables[$table]);
		}
		//$cache->clean(Zend_Cache::CLEANING_MODE_ALL);
	}
	
	function getCacheTables()
	{
		$tables['Allegroaukcje'][] = 'Produkty';
		$tables['Allegroaukcje'][] = 'Rozmiarproduktu';
		$tables['Ankietaodpowiedzi'][] = 'Ankieta';
		$tables['Archiwumkontrahenci'][] = 'Produkty';
		$tables['Archiwumprodukty'][] = 'Produkty';
		$tables['Atrybuty'][] = 'Atrybutygrupy';
		$tables['Atrybuty'][] = 'Atrybutypowiazania';
		$tables['Atrybuty'][] = 'Produkty';
		$tables['Atrybutygrupy'][] = 'Atrybuty';
		$tables['Atrybutygrupy'][] = 'Atrybutypowiazania';
		$tables['Atrybutygrupy'][] = 'Produkty';
		$tables['Atrybutypowiazania'][] = 'Atrybuty';
		$tables['Atrybutypowiazania'][] = 'Atrybutygrupy';
		$tables['Atrybutypowiazania'][] = 'Produkty';		
		$tables['Galeria'][] = 'Produkty';
		$tables['Kategorie'][] = 'Katprod';
		$tables['Kategorie'][] = 'Produkty';
		$tables['Katprod'][] = 'Allegroaukcje';
		$tables['Katprod'][] = 'Archiwumkontrahenci';
		$tables['Katprod'][] = 'Archiwumprodukty';
		$tables['Katprod'][] = 'Kategorie';
		$tables['Katprod'][] = 'Odwiedziny';
		$tables['Katprod'][] = 'Produkty';
		$tables['Katprod'][] = 'Rozmiarproduktu';
		$tables['Katprod'][] = 'Rozmiary';
		$tables['Kolorproduktu'][] = 'Allegroparams';
		$tables['Kolorproduktu'][] = 'Kolory';
		$tables['Kolorproduktu'][] = 'Produkty';
		$tables['Kolory'][] = 'Allegroparams';
		$tables['Kolory'][] = 'Kolorproduktu';
		$tables['Kolory'][] = 'Produkty';
		$tables['Kontrahencigrupy'][] = 'Kontrahenci';
		$tables['Kontrahencilogin'][] = 'Kontrahenci';
		$tables['Kontrahencipolecane'][] = 'Produkty';
		$tables['Odwiedziny'][] = 'Produkty';
		$tables['Parametry'][] = 'Produkty';
		$tables['Producent'][] = 'Produkty';
		$tables['Produkty'][] = 'Allegroaukcje';
		$tables['Produkty'][] = 'Allegrokategorie';
		$tables['Produkty'][] = 'Katprod';
		$tables['Produkty'][] = 'Kategorie';
		$tables['Produktygrupy'][] = 'Produkty';
		$tables['Produktypowiazania'][] = 'Produkty';
		$tables['Rozmiarproduktu'][] = 'Allegroparams';
		$tables['Rozmiarproduktu'][] = 'Produkty';
		$tables['Rozmiarproduktu'][] = 'Rozmiary';
		$tables['Rozmiarproduktu'][] = 'Katprod';
		$tables['Rozmiary'][] = 'Allegroparams';
		$tables['Rozmiary'][] = 'Produkty';
		$tables['Rozmiary'][] = 'Rozmiarproduktu';
		$tables['Rozmiary'][] = 'Katprod';
		$tables['Rozmiary'][] = 'Galeria';
		$tables['Zamowieniakontrahenci'][] = 'Produkty';
		$tables['Zamowieniaprodukty'][] = 'Produkty';
		$tables['Zamowieniaprodukty'][] = 'Katprod';
		return $tables;
	}
	
	function drawBarcode128($typ='CODE128_A', $kod='', $file='', $drawText=0, $skala=2, $grubosc=30, $fontsize=18)
	{
		require_once('barcodegen/class/BCGFont.php');
		require_once('barcodegen/class/BCGColor.php');
		require_once('barcodegen/class/BCGDrawing.php'); 
		include_once('barcodegen/class/BCGcode128.barcode.php'); 
		//include_once('barcodegen/class/BCGgs1128.barcode.php'); 
		$font = new BCGFont('../application/models/barcodegen/class/font/Arial.ttf', $fontsize);
		$color_black = new BCGColor(0, 0, 0);
		$color_white = new BCGColor(255, 255, 255); 
		$code = new BCGcode128();
		$code->setScale($skala); // Resolution
		$code->setThickness($grubosc); // Thickness
		$code->setForegroundColor($color_black); // Color of bars
		$code->setBackgroundColor($color_white); // Color of spaces
		$code->setFont($drawText ? $font : 0); // Font (or 0)
		$code->parse(array(CODE128_A, $kod)); // Text
		//1 - Filename (empty : display on screen) //2 - Background color */
		$drawing = new BCGDrawing($file, $color_white);
		$drawing->setBarcode($code);
		$drawing->draw();
		//header('Content-Type: image/png');
		$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
		//echo '<img src="'.$this->obConfig->www.'/public/'.$file.'">';
	}
	
	public function getPdfLines($text, $max)
	{
		$chars = 0;
		$lineCount = 0;
		$rows = explode('<br>', $text);
		if(count($rows) > 0)
		foreach($rows as $row)
		{
			$words = explode(' ', $row);
			if(count($words) > 0)
			foreach($words as $word)
			{
				if($chars + mb_strlen($word, 'UTF-8') > $max)
				{
					$lineCount++;
					$chars = 0;
				}
				if(!isset($lines[$lineCount])) $lines[$lineCount] = '';
				$lines[$lineCount] .= $word.' ';
				$chars += mb_strlen($word, 'UTF-8') + 1;
			}
			$lineCount++;
			$chars = 0;
		}
		return @$lines;
	}
	
	function convertTableTypes($name, $data)
	{
		$table = new Zend_Db_Table(ucfirst($name));
		$info = $table->info();//$table->getTable()->info();
		//if(false)
		if(count($data) > 0)
		foreach($data as $val => $value)
		{
			$type = @$info['metadata'][$val]['DATA_TYPE'];
			$size = @$info['metadata'][$val]['LENGTH'];
			if($type == 'int') $data->{$val} = intval($value);
			if($type == 'decimal') $data->{$val} = floatval($value);
			if($type == 'varchar') $data->{$val} = strval($value);
			if($type == 'text') $data->{$val} = strval($value);
			//if($type == 'varchar' && intval($size) <= 2) $data->{$val} = (boolean)$value;
		}
		return $data;
	}
	
	function getDostepnosci($dostepnosci = null, $stan_min = 0)
	{
		if($stan_min > 0 && count($dostepnosci) > 0)
		{
			$stan_min = @floor(intval($stan_min) / 1);
			//$this->dostepnoscPrzedzialy = $this->dostepnoscPrzedzialy->toArray();
			if(isset($dostepnosci[1])) $dostepnosci[1]['max'] = max(1, $stan_min - 1);
			if(isset($dostepnosci[2])) $dostepnosci[2]['min'] = $stan_min + 0;
			if(isset($dostepnosci[2])) $dostepnosci[2]['max'] = $stan_min + 0;
			if(isset($dostepnosci[3])) $dostepnosci[3]['min'] = $stan_min + 1;
			//$this->view->dostepnoscPrzedzialy = $this->dostepnoscPrzedzialy;
		}
		return $dostepnosci;
	}
}
?>