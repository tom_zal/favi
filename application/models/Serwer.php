<?php
class Serwer extends Zend_Db_Table
{
	public $adres, $serwer, $login, $haslo, $remote_path, $id;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

	function updateData($array)
	{
		$where = 'id = 1';
		$this->update($array, $where);		
	}
	
	function showData()
	{
		$result = $this->fetchAll();		
		return $result;
	}
	
	function truncate($table)
	{
		$this->obConfig = new Zend_Config_Ini('../application/config.ini', 'general');
		$this->db = Zend_Db::factory($this->obConfig->db->adapter, $this->obConfig->db->config->toArray());
		$this->db->query('SET NAMES utf8');
		$sql = 'TRUNCATE TABLE '.$table;
		$result = $this->db->truncate($sql);
	}
	
	function insertAll($table, $dane)
	{
		$this->obConfig = new Zend_Config_Ini('../application/config.ini', 'general');
		$this->db = Zend_Db::factory($this->obConfig->db->adapter, $this->obConfig->db->config->toArray());
		$this->db->query('SET NAMES utf8');
		foreach($dane as $row)
		{
			$sql = 'INSERT INTO '.$table.' VALUES (';
			$i = 0;
			foreach($row as $value)
			{
				//var_dump($row);
				$sql .= '"'.$value.'"';
				if($i < count($row) - 1) $sql .= ', ';
				$i++;
			}
			$sql .= ')';
			echo $sql;
			$result = $this->db->to($table)->insert($dane);
		}
	}
}
?>