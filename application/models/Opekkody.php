<?php
class Opekkody extends Zend_Db_Table
{
	protected $_name = 'Opekkody';
	private $db, $obConfig;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->module = $module;
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
	function showData()
	{
		$result = $this->fetchAll();		
		return $result;
	}	
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function wypiszJeden()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function wypiszKody()
	{
		//$sql = $this->select()->group('Miasto')->order('Miasto asc');
		$sql = $this->select()->order('Miasto asc');
		//echo $select;return null;
		$result = $this->fetchAll($sql);
		return $result->toArray();
	}
	function szukajMiasto($miasto = '', $kod = '')
	{
		$miasto = trim($miasto);
		//if(empty($miasto)) return null;
		
		if(!empty($kod))
		{
			$where = 'Kod = "'.str_replace('-', '', $kod).'"';
			$sql = $this->select()->where('Miasto = "'.$miasto.'" and '.$where)->group('Stacja');
			//echo $sql;
			$result = $this->fetchAll($sql);
			if(count($result) == 1) return array($result[0]->toArray());
			if(count($result) > 1) return $result->toArray();
			
			$sql = $this->select()->where($where)->group('Stacja');
			//echo $sql;
			$result = $this->fetchAll($sql);
			if(count($result) == 1) return array($result[0]->toArray());
			if(count($result) > 1) return $result->toArray();
		}
		
		$sql = $this->select()->where('Miasto = "'.$miasto.'"')->group('Stacja');
		//echo $sql;
		$result = $this->fetchAll($sql);
		if(count($result) == 1) return array($result[0]->toArray());
		if(count($result) > 1) return $result->toArray();
		
		//$sql = $this->select()->where('Miasto like "%'.$miasto.'%"')->order('Miasto asc');		
		$likes = explode(' ', $miasto);
		$where = '0';
		if(count($likes) > 0)
		foreach($likes as $like)
		{
			$like = trim($like);
			if(empty($like)) continue;
			if(strlen($like) < 3) continue;
			$where .= ' or Miasto like "%'.$like.'%"';
		}		
		$sql = $this->select()->where($where)->group('Stacja')->order('Miasto asc');
		//echo $sql;
		$result = $this->fetchAll($sql);
		return $result->toArray();
	}
}
?>