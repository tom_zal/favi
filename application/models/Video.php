<?php
class Video extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		$this->json = 'http://gdata.youtube.com/feeds/api/videos/kod?v=2&alt=jsonc';
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function wypisz($ukryj = false, $id = 0, $typ = '', $json = false)
	{
		$where = 'lang = "'.$this->lang.'" and id_ext = '.$id.' and typ = "'.$typ.'"';
		if($ukryj) $where .= ' and wyswietl = "1"';
		$sql = $this->select()->where($where)->order(array('pozycja asc', 'nazwa asc'));
		$result = $this->fetchAll($sql);
		if(!$json) return $result;
		
		if(count($result) > 0)
		foreach($result as $video)
		{
			$wideo = $video->toArray();
			$kod = @end(explode('/', $video['www']));
			$url = str_replace('kod', $kod, $this->json);
			$json = @json_decode(file_get_contents($url));
			//if(empty($json)) continue;
			$wideo['json'] = @$json->data;
			$wideo['thumb'] = @$json->data->thumbnail->hqDefault;//sq
			$wideo['kod'] = $kod;
			$wideos[] = $wideo;
		}
		//var_dump($wideos);die();
		return @$wideos;
	}
	function wypiszNajnowsze($id = 0, $typ = '')
	{
        $where = 'lang = "'.$this->lang.'" and wyswietl = "1" and id_ext = '.$id.' and typ = "'.$typ.'"';
        $result = $this->fetchRow($where, 'pozycja asc', 1);
        return $result;
    }
	function getVideo($pole = 'id')
	{
		$sql = $this->select()->where('lang = "'.$this->lang.'"')->order(array('pozycja asc', 'nazwa asc'));
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i][$pole]] = $result[$i]->toArray();
		}
		return @$results;
	}
	function wypiszWhereNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa like "%'.$nazwa.'%"');
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszNazwa($nazwa)
	{
		$sql = $this->select()->where('nazwa = "'.$nazwa.'"');
		$result = $this->fetchAll($sql);
		return $result;
	}	
	function usun()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	function wypiszPojedynczego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}	
}
?>