<?php
class Paczka extends Zend_Db_Table
{
	//public static $obConfig;
	public $link, $id, $typ, $obConfig, $db, $ar2, $arBuffor = array(),$arEnvelope= array(), $errors = array() ,$dane = array(), $error, $dostawca;
	public $przesylkiDodane = array(), $paczkiDodane = array();
	static public $table = "Paczka";
	static $kurierzy = array
	(
		'dhl'=> 'DHL',
		//'siodemka'=> 'Siódemka',
		'pocztapolska'=> 'Poczta Polska',
		'paczkomaty'=> 'InPost - Paczkomaty'
	);

	public function __construct($paczka = "")
	{
		$this->_name = !empty($paczka) ? $paczka : Paczka::$table;
		parent::__construct();
		$common = new Common();
		$this->obConfig = $common->getObConfig();
		$this->db = $common->getDB($this->obConfig);
	}

	function dodaj($dane) {
		$this->insert($dane);
		return $this->getAdapter()->lastInsertId();
	}
	function edytuj($dane, $id_zam = 0) {
		$pole = $id_zam ? 'id_zam':'id';
		$where = $pole.' = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujPoSymbol($dane) {
		$where = 'symbol = "'.$this->symbol.'"';
		$this->update($dane, $where);
	}
	function edytujNumerPrzesylka($dane, $numerPrzesylka = "") {
		$where = 'numerPrzesylka = "'.$numerPrzesylka.'"';
		$this->update($dane, $where);
	}
	function _update($dane) {
		$where = 'id_zam in ('.(implode(',', $this->id)).')';
		$this->update($dane, $where);
	}
	function wypisz($czyList = 0){
		$where = $czyList == 1 ? ' and numerPrzesylka=""': '';
		$where = $czyList == 0 ? ' or numerPrzesylka=""': $where;
		$sql = $this->select()->where('czyList = "'.$czyList.'"'.$where);
		$result = $this->fetchAll($sql)->toArray();
		return $result;
	}
	function wypiszNumeryPrzesylek(){
		$where =  'numerPrzesylka<>""';
		$sql = $this->select()->where($where);
		$result = $this->fetchAll($sql)->toArray();
		return $result;
	}
	function wypiszPoPrzesylka($dane) {
		$array = array();
		if(!empty($dane)) {
			foreach($dane as $d) {
				$array[$d['dostawca']][] = $d;
			}
		}
		return $array;
	}
	function wybrany() {
		$sql = $this->select()->where('id = "'.$this->id.'"');
		return $this->fetchRow($sql)->toArray();
	}	
	function usun($poSymbolu = 0) {
		$pole = $poSymbolu == 1 ? 'symbol': 'id';
		$this->delete($pole.' = "'.$this->id.'"');			
	}
	function sprawdzZamowienie() {
		$sql = $this->select()->where('id_zam = "'.$this->id.'"');
		$res = $this->fetchRow($sql);
		return $res == NULL ? 0 : 1;
	}
	function getMaxSymbol() {
		$sql = $this->select()->where('listaSymbol = "'.date('Y-m-d').'"')->order('listaNr DESC');
		$res = $this->fetchRow($sql);
		return $res == NULL ? 
			array('listaSymbol'=>date('Y-m-d'),'listaNr'=>0) : 
			array('listaSymbol'=>date('Y-m-d'),'listaNr'=>$res['listaNr']+1);
	}
	function sprawdzZamowienieNumer() {
		$sql = $this->select()->where('id_zam = "'.$this->id.'"');
		$res = $this->fetchRow($sql);
		return $res;
	}
	function getSymbol() {
		$sql = $this->select()
				->from($this->_name, array('*', 'data'=>'DATE(utworzono)')) //, 'numer' => Zamowienia::numer))
				->where('numerPrzesylka <> "" and czyList = 1 and symbol <> ""')
				->group('symbol')
				->order(array('utworzono DESC'));
		return $this->fetchAll($sql)->toArray();
	}
	function getPaczkiSymbol() {
		$sql = $this->select()
				->where('numerPrzesylka <> "" and czyList = 1 and symbol <> ""')
				->order(array('dataPrzesylka DESC','listaNr DESC'));
		
		$rows = $this->fetchAll($sql)->toArray();
		return $this->sortSymbol($rows);
	}
	function sortSymbol($rows) {
		$r = array();
		if(!empty($rows)) {
			foreach($rows as $row) {
				$r[$row['symbol']][] = $row;
			}
		}
		return $r;
	}
	static function getNumerPaczka($data, $numer) {
		return str_replace('-','/', $data).'/'.$numer;
	}
	function getNumeryPrzesylek($dane) {
		$ar = array();
		foreach($dane as $row) {
			$ar[] = $row['numerPrzesylka'];
			$this->ar2[$row['numerPrzesylka']] = $row['id'];
		}
		return $ar;
	}
	function getGuid($dane) {
		$ar = array();
		foreach($dane as $row) {
			$ar[] = $row['guid'];
			$this->ar2[$row['guid']] = $row['id'];
			$this->arBuffor[$row['id']] = $row['buffor'];
			$this->arEnvelope[$row['id']] = $row['idEnvelope'];
		}
		return $ar;
	}
	static function getNameKey($key ='dhl', $all = 0)
	{
		$array = array
		(
			'dhl'=> 'DHL',
			//'siodemka'=> 'Siódemka',
			'pocztapolska'=> 'Poczta Polska',
			'paczkomaty'=> 'InPost - Paczkomaty'
		);
		//var_dump($array);die();
		//if($this->obConfig->siodemka) $array['siodemka'] = 'Siódemka';		
		if($all) return $array;
		return @$array[$key];
	}
	function editPrzesylka($przesylka, $dane, $ikWiersz) {
		$this->przesylkiDodane[$this->dostawca][] = $this->id;
		$this->paczkiDodane[$this->dostawca][] = $ikWiersz; 

		$przesylka['utworzono'] = date('Y-m-d H:i:s');
		$przesylka['config'] = $dane['config'] != NULL ? serialize($dane['config']) : '';
		$przesylka['odbiorca'] = serialize($dane['odbiorca']);
		if(!isset($przesylka['dataPrzesylka']))
		$przesylka['dataPrzesylka'] = date('Y-m-d');
		
		$this->edytuj($przesylka, 1);
	}
	function setErrors($error = '') {
		if(is_array($error)) {
			array_push($this->errors, $error);
		}
	}
	function checkErrors($response, $id=0) {
		if(isset($response['error']) && $response['error'] != NULL) {
			$this->errors[$this->dostawca][$id] = $response['error'];
			return false;
		}
		return true;
	}
	function errorsToView($typ='k_blad', $temat = 'Błąd wprowadzonych danych.') {
		$html = '';
		if(is_array($this->error) && count($this->error)) {
			$html .='<div class="error_container">';
			$html .= '<div class="'.$typ.'">'.$temat.'</div>';
			$html .='<div class="lista_bledow">';
			foreach($this->error as $dostawca=> $error) {
				$html .= '<div>'.Paczka::getNameKey($dostawca).'</div>';
			   // foreach($error as $e) {
			   //     $html .= $e.'<br />';
			   // }
				$html .=$error;
			}
			$html .='</div>';
			$html .='</div>';
		}
		return $html;
	}
	function setDefaultDane($kontr, $config, $configs = null)
	{
		$pocztaPolska = new Pocztapolska('admin');
		$pocztaPolskaDane = new Pocztapolskadane('admin');
		$odbiorca = $odbiorcaDostawcy = $configDostawcy = $przesylki = $urzadWydaniaEPrzesylki = array();
		$config['domyslne'] = $config;
		if(!empty($kontr))
		{
			$odbiorca = array();
			foreach($kontr as $kr)
			{
				$krId = $kr['id'];
				$dostawca = $kr['dostawca'];
				$inny = $kr['inny_adres_dostawy'];
				if($dostawca == "dhl")
				{
					$odbiorca[$dostawca][$krId]['name'] = !empty($kr['nazwa_firmy']) ? $kr['nazwa_firmy'] : trim($kr['nazwisko'].' '.$kr['imie']);
					if($inny) $odbiorca[$dostawca][$krId]['name'] = !empty($kr['nazwa_firmy_korespondencja']) ? 
						$kr['nazwa_firmy_korespondencja'] : trim($kr['nazwisko_korespondencja'].' '.$kr['imie_korespondencja']);
					//$odbiorca[$dostawca][$krId]['nip'] = $kr['nip'];
					$odbiorca[$dostawca][$krId]['contactPerson'] = !$inny ? trim($kr['nazwisko'].' '.$kr['imie']) : 
												trim($kr['nazwisko_korespondencja'].' '.$kr['imie_korespondencja']);
					$odbiorca[$dostawca][$krId]['postalCode'] = !$inny ? $kr['kod'] : $kr['kod_korespondencja'];
					$odbiorca[$dostawca][$krId]['city'] = !$inny ? $kr['miasto'] : $kr['miasto_korespondencja'];
					$odbiorca[$dostawca][$krId]['street'] = !$inny ? $kr['ulica'] : $kr['ulica_korespondencja'];
					$odbiorca[$dostawca][$krId]['houseNumber'] = !$inny ? $kr['nr'] : $kr['nr_korespondencja'];
					$odbiorca[$dostawca][$krId]['apartmentNumber'] = !$inny ? $kr['mieszkanie'] : $kr['mieszkanie_korespondencja'];
					$odbiorca[$dostawca][$krId]['contactPhone'] = !$inny ? $kr['komorka'] : $kr['komorka_korespondencja'];
					if(empty($odbiorca[$dostawca][$krId]['contactPhone']))
					$odbiorca[$dostawca][$krId]['contactPhone'] = !$inny ? $kr['telefon'] : $kr['telefon_korespondencja'];
					$odbiorca[$dostawca][$krId]['contactEmail'] = $kr['email'];
					
					$config[$krId] = @$configs['dhl'];
					$czyPobranie = (strpos($kr['wysylka'], 'pobran') !== false) || (strpos($kr['wysylka'], 'Płatność przy odbiorze') !== false);
					$kwotaPobrania = $czyPobranie ? $kr['wartosc_zamowienia'] + $kr['koszt_dostawy'] : 0.00;					
					$config[$krId]['collectOnDelivery'] = $czyPobranie ? '1':'';
					$config[$krId]['collectOnDeliveryValue'] = $kwotaPobrania;
					if($czyPobranie) $config[$krId]['insurance'] = '1';
					//$config[$krId]['insurance'] = $czyPobranie ? '1':'';
					$config[$krId]['insuranceValue'] = $kr['wartosc_zamowienia'] + $kr['koszt_dostawy'];					
					//$config[$krId]['content'] = !empty($kr['dok_DoDokNrPelny'])? 'Dokument: '.$kr['dok_DoDokNrPelny']: '';
				}
				else
				{
					$firma = $inny ? $kr['nazwa_firmy_korespondencja'] : $kr['nazwa_firmy'];
					$nazwa = $inny ? $kr['nazwisko_korespondencja'].' '.$kr['imie_korespondencja'] : $kr['nazwisko'].' '.$kr['imie'];
					$odbiorca[$dostawca][$krId]['czyFirma'] = !empty($kr['nip']) || !empty($firma) ? 'on' : '';			
					$odbiorca[$dostawca][$krId]['nazwa'] = !empty($firma) ? $firma : trim($nazwa);
					$odbiorca[$dostawca][$krId]['nip'] = str_replace("-", "", $kr['nip']);
					$odbiorca[$dostawca][$krId]['nazwisko'] = !$inny ? $kr['nazwisko'] : $kr['nazwisko_korespondencja'];
					$odbiorca[$dostawca][$krId]['imie'] = !$inny ? $kr['imie'] : $kr['imie_korespondencja'];
					$odbiorca[$dostawca][$krId]['kod'] = !$inny ? $kr['kod'] : $kr['kod_korespondencja'];
					$odbiorca[$dostawca][$krId]['miasto'] = !$inny ? $kr['miasto'] : $kr['miasto_korespondencja'];
					$odbiorca[$dostawca][$krId]['ulica'] = !$inny ? $kr['ulica'] : $kr['ulica_korespondencja'];
					$odbiorca[$dostawca][$krId]['nrDom'] = !$inny ? $kr['nr'] : $kr['nr_korespondencja'];
					$odbiorca[$dostawca][$krId]['nrLokal'] = !$inny ? $kr['mieszkanie'] : $kr['mieszkanie_korespondencja'];
					$odbiorca[$dostawca][$krId]['telKontakt'] = !$inny ? $kr['telefon'] : $kr['telefon_korespondencja'];
					$odbiorca[$dostawca][$krId]['komKontakt'] = !$inny ? $kr['komorka'] : $kr['komorka_korespondencja'];
					if(false && empty($odbiorca[$dostawca][$krId]['telKontakt']))
					$odbiorca[$dostawca][$krId]['telKontakt'] = $odbiorca[$dostawca][$krId]['komKontakt'];
					$odbiorca[$dostawca][$krId]['emailKontakt'] = $kr['email'];
					//var_dump($odbiorca[$dostawca]);die();
					
					$wysylka = $kr['wysylka'];
					$config[$krId] = @$configs[$dostawca];
					$czyPobranie = (strpos($wysylka, 'pobran') !== false) || (strpos($wysylka, 'Płatność przy odbiorze') !== false);
					$przedpłata = (strpos($wysylka, 'przedpł') !== false);
					$odbiorPunkt = (strpos($wysylka, 'Odbiór w punkcie') !== false);
					$czyPobranie = $czyPobranie || ($odbiorPunkt && !$przedpłata);
					$kwotaPobrania = $czyPobranie ? $kr['wartosc_zamowienia'] + $kr['koszt_dostawy'] : 0.00;
					$config[$krId]['kwotaPobrania'] = $kwotaPobrania;
					$config[$krId]['kwotaUbezpieczenia'] = $config[$krId]['kwotaPobrania'];
					$config[$krId]['czyPobranie'] = $czyPobranie ? 'on':'';
					$config[$krId]['opisZawartosci'] = @!empty($kr['dok_DoDokNrPelny'])? 'Dokument: '.$kr['dok_DoDokNrPelny']: '';
					
					if($dostawca == "pocztapolska")
					{						
						$typ = "przesylkakurierska48";//Nowy typ przesyłki
						if(strpos($wysylka, "Paczka") !== false) $typ = "paczka";
						if(strpos($wysylka, "pobraniowa") !== false) $typ = "pobraniowa";
						if(strpos($wysylka, "polecona") !== false) $typ = "polecona";
						if(strpos($wysylka, "List") !== false) $typ = "firmowanierejestrowana";
						if(strpos($wysylka, "polecony") !== false) $typ = "firmowapolecona";
						if(strpos($wysylka, "nierejestrowana") !== false) $typ = "nierejestrowana";
						if(strpos($wysylka, "deklaracja") !== false) $typ = "deklaracja";
						if(strpos($wysylka, "usługa") !== false) $typ = "uslugakurierska";
						if(strpos($wysylka, "Paczka48") !== false) $typ = "przesylkakurierska48";
						if(strpos($wysylka, "Pocztex") !== false) $typ = "przesylkakurierska48";
						if(strpos($wysylka, "InPost") !== false) $typ = "przesylkakurierska48";
						$config[$krId]['typ'] = $typ;
						
						$typDef = $pocztaPolska->getTypDef($typ);
						foreach($pocztaPolska->typy as $typDef => $typNazwa)
						{
							$typ = $pocztaPolska->getTyp($typDef);
							$przesylki[$typ][$krId] = @$configs['pocztapolska'][$typDef];
							$przesylki[$typ][$krId]['pobranie'] = $czyPobranie ? '1':'';
							$przesylki[$typ][$krId]['kwota'] = $kwotaPobrania;
							if(@$przesylki[$typ][$krId]['dwartosc'] || $typ == "deklaracja")
							$przesylki[$typ][$krId]['wartosc'] = $kr['wartosc_zamowienia'];
							if($typ == "przesylkakurierska48") // && $config[$krId]['typ'] == $typ)
							{
								$urzadWydaniaEPrzesylki[$krId] = null;
								$pocztaPolskaDane->urzadWydaniaEPrzesylkiPobierz();
								$kod = str_replace("-","",$odbiorca[$dostawca][$krId]['kod']);
								$miasto = $odbiorca[$dostawca][$krId]['miasto']; //if(!empty($miasto))
								if(@!empty($kr['poczta_punkt_odbioru_miasto'])) $miasto = $kr['poczta_punkt_odbioru_miasto'];
								$urzadWydaniaEPrzesylki[$krId] = $pocztaPolskaDane->urzadWydaniaEPrzesylkiSzukaj($miasto);
								if(empty($urzadWydaniaEPrzesylki[$krId])) //if(!empty(str_replace("-","",$kod)))
								$urzadWydaniaEPrzesylki[$krId] = $pocztaPolskaDane->urzadWydaniaEPrzesylkiSzukaj($kod);
								if(strpos($wysylka, "Odbiór w punkcie") !== false && count($urzadWydaniaEPrzesylki[$krId]) == 1)
								$przesylki[$typ][$krId]['urzadWydaniaEPrzesylki'] = key($urzadWydaniaEPrzesylki[$krId]);
								if(@intval($kr['poczta_punkt_odbioru']) > 0)
								$przesylki[$typ][$krId]['urzadWydaniaEPrzesylki'] = $kr['poczta_punkt_odbioru'];
								$przesylki[$typ][$krId]['urzadWydaniaEPrzesylkiInput'] = !empty($miasto) ? $miasto : $kod;
								//var_dump($przesylki[$typ]);die();
							}							
						}
						//var_dump($kr);var_dump($config);var_dump($przesylki);die();
					}
				}
				
				$odbiorcaDostawcy[$dostawca][$krId] = $odbiorca[$dostawca][$krId];
				$configDostawcy[$dostawca][$krId] = $config[$krId];
			}  
		}
		return array('odbiorca'=>$odbiorca, 'config'=>$config, 'przesylki'=>$przesylki, 'urzadWydaniaEPrzesylki'=>$urzadWydaniaEPrzesylki);
	}
	function infoMsg() {
		$messege = '';
		if(count($this->przesylkiDodane) > 0){
			$ileDodano = 0;
			$ileDodanoDostawca = '';
			foreach($this->przesylkiDodane as $dostawcy=>$przesylki) {
				$ileDodano +=count($przesylki);
				$ileDodanoDostawca[$dostawcy] = count($przesylki);
			}
			if($ileDodano > 0) {
				$msg1 = '';
				foreach($ileDodanoDostawca as $dostawca=>$ile) {
					$msg1 .= Paczka::getNameKey($dostawca).': '.$ile.' szt. ';
				}
				$messege .= '<div class="k_ok">
					Przesyłki dodane ilość: '.$ileDodano.' szt. ('.$msg1.')<br /> 
				</div>';
			}
		}
		return $messege;
	}
}
?>