<?php
class Upusty extends Zend_Db_Table
{
	public $link, $id;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function wypisz($id)
	{
		$result = $this->fetchAll($this->select()->where('idkontrahenta = '.$id)->order('od ASC'));
		return $result;
	}
	function usun()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	function znajdz($wartosc,$id)
	{
		$where = 'idkontrahenta = '.$id.' AND od <='.$wartosc.' AND do >'.$wartosc;
		$result = $this->fetchRow($where);
		return $result;
	}		
}
?>