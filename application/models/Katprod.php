<?php

class Katprod extends Zend_Db_Table {

    public $ilosc_na_stronie;
    private $db;

    public function __construct($module = 'admin', $id = 0, $typ = 'kategorie') {
        parent::__construct();
        $this->module = $module;
        $this->common = new Common(false, $module);
        $this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
        $this->lang = $this->common->getJezyk($module);
        $this->ustawienia = $this->common->getUstawienia();
        $this->id = $id;
        $this->typ = $typ;
        $this->tryb = $this->obConfig->tryb;
        $this->trybCena = $this->obConfig->trybCena;
    }

    public function stronicowanie() {
        $select = $this->db->select()
                ->from(array('kp' => 'Katprod'), array('*'))
                ->joinleft(array('p' => 'Produkty'), 'p.id = kp.id_prod', array('*'))
                ->where('kp.id_kat = ' . $this->id)->where('p.widoczny = 1')
                ->group('p.id');

        $result = $this->db->fetchAll($select);
        $ilosc_podstron = ceil(count($result) / 24);
        return $ilosc_podstron;
    }

    public function raportOdwiedzin() {
        $select = $this->db->select()
                        ->from(array('kp' => 'Katprod'), array('*'))
                        ->joinleft(array('p' => 'Produkty'), 'p.id = kp.id_prod', array('*'))
                        ->where('kp.id_kat = ' . $this->id)->where('p.widoczny = 1')
                        ->group('p.id')->order('p.odwiedziny DESC')->limit(30, 0);

        $result = $this->db->fetchAll($select);
        return $result;
    }

    function getKategorieForIds($ids, $order = 'asc') {
        if (!$this->common->isArray($ids, true))
            return null;
        $select = $this->db->select()->from(array('kp' => 'Katprod'), array('id_prod'))
                ->joinleft(array('k' => 'Kategorie'), 'k.id = kp.id_kat', array('*'))
                ->where('id_prod in(' . implode(',', $ids) . ') and k.typ = "' . $this->typ . '"')
                ->order(array('id_prod asc', 'id_kat ' . $order));
        //echo $select;die();
        $result = $this->db->fetchAll($select);
        for ($i = 0; $i < count($result); $i++) {
            $results[$result[$i]['id_prod']][] = $result[$i];
        }
        return @$results;
    }

    function getKategorieForExport($ids) {
        if (!$this->common->isArray($ids, true))
            return null;
        $select = $this->db->select()->from(array('kp' => 'Katprod'), array('id_prod',
                            new Zend_Db_Expr('GROUP_CONCAT(DISTINCT id_kat ORDER BY id_kat ASC SEPARATOR "|") as kats')))
                        ->where('id_prod in(' . implode(',', $ids) . ')')->group('id_prod')->order(array('id_prod asc'));
        //echo $select;die();
        $result = $this->db->fetchAll($select);
        for ($i = 0; $i < count($result); $i++) {
            $results[$result[$i]['id_prod']] = $result[$i]['kats'];
        }
        return @$results;
    }

    function getKatProd() {
        $sql = $this->select();
        $result = $this->fetchAll($sql);
        for ($i = 0; $i < count($result); $i++) {
            $results[$result[$i]['id_prod']][$result[$i]['id_kat']] = $result[$i]['id_kat_prod'];
        }
        return @$results;
    }

    public function countProdukty() {
        //$sql = 'SELECT count(*) FROM Produkty p LEFT JOIN Katprod kat ON(p.id = kat.id_prod) 
        //WHERE kat.id_kat = ' . $this->id . ' GROUP BY p.id';
        $sql = 'SELECT count(*) FROM (select p.nazwa from Produkty p INNER JOIN Katprod kat ON(p.id = kat.id_prod) WHERE kat.id_kat = ' . $this->id . ' GROUP BY p.id order by p.id) as d';
        //echo $sql;
        //return;
        $result = $this->db->fetchAll($sql);
        return $result;
    }

    public function countProduktyWKatNieWPodKat($id) {
        //$sql = 'SELECT count(*) FROM Produkty p LEFT JOIN Katprod kat ON(p.id = kat.id_prod) 
        //WHERE kat.id_kat = ' . $this->id . ' GROUP BY p.id';
        $sql = 'SELECT count(*) FROM Katprod where id_kat = ' . $id . ' and id_prod not in (select distinct id_prod from Kategorie k INNER JOIN Katprod kp ON(k.id = kp.id_kat) WHERE k.rodzic = ' . $id . ')';
        //echo $sql;
        //return;
        $result = $this->db->fetchAll($sql);
        //var_dump($result);
        return @intval($result[0]['count(*)']);
    }

    /// wyswietla produkty przypisane do danej kategorii
    public function selectProdukt($strona = 0, $ilosc = 9999, $sortowanie = null, $szukanie = null, $ile = false, $id = 0, $ids = null, $test = false, $links = false) {
        
        $od = $strona * $ilosc;
        $limit = $ilosc;
        if ($ile) {
            $od = 0;
            $limit = 9999;
        }
        $where = 'p.nazwa <> "" and p.lang = "' . $this->lang . '" and p.zmiana is not NULL';
        if ($id > 0)
            $where .= ' and p.id <> ' . $id;
        if ($this->common->isArray($ids, true))
            $where .= ' and p.id in (' . implode(',', $ids) . ')';

        if ($this->id != -1)
            $where .= intval($this->id) > 0 ? ' and kat.id_kat = ' . $this->id : ' and kat.id_kat is null';
        $katInne = $this->obConfig->kategorieInne && isset($this->nr) && $this->nr != -1;
        $katInneNazwa = $this->obConfig->kategorieInneNazwa;
        if ($katInne)
            $where .= intval($this->nr) > 0 ? ' and inne.id_kat = ' . $this->nr : ' and inne.id_kat is null';

        $select = 'p.id, p.link, p.nazwa, p.oznaczenie, p.producent';
        $having = '1';

        if ($sortowanie == null) {
            $sortowanie = (object) array();
            $sortowanie->sort = 'p.nazwa';
            $sortowanie->order = 'asc';
            $sortowanie->specjalne = '';
        }
        
        $allegroDoKonca = 'time(if(a.wystawiono is not NULL, TIMEDIFF(DATE_ADD(a.wystawiono, INTERVAL a.na_ile DAY), NOW()), ' . ($sortowanie->order == 'asc' ? '"9999:00:00"' : '"0000:00:00"') . '))';

        $producent = $this->obConfig->producent;
        $prodGrupy = $this->obConfig->produktyGrupy;
        $rozmiar = (isset($sortowanie->sort) && ($sortowanie->sort == 'rozmiar') || $sortowanie->sort == 'wkladka');
        $rozmiar = $rozmiar || (isset($szukanie->rozmiar) && $szukanie->rozmiar != 'all');
        $rozmiar = $rozmiar || (isset($szukanie->wkladka) && $szukanie->wkladka != 'all');
        $rozmiar = $rozmiar || ($this->obConfig->trybCena == 'rozmiary');
        $kolor = (isset($sortowanie->sort) && $sortowanie->sort == 'kolor');
        $kolor = $kolor || (isset($szukanie->kolor) && $szukanie->kolor != -1);
        $dostepnosc = (isset($sortowanie->sort) && $sortowanie->sort == 'dostepnosc');
        $rozmiar = $rozmiar || ($dostepnosc && $this->obConfig->tryb == 'rozmiary');
        $kolor = $kolor && !$dostepnosc;
        $allegro = (isset($sortowanie->sort) && $sortowanie->sort == 'allegro_do_konca');
        $allegro = $allegro || (isset($szukanie->aukcje) && $szukanie->aukcje == 'all');
        $odwiedziny = (isset($szukanie->odwiedziny) && $szukanie->odwiedziny != '');
        $odwiedziny = $odwiedziny || (isset($szukanie->dataOdwiedzinOd) && $szukanie->dataOdwiedzinOd != '');
        $sprzedaz = (isset($szukanie->sprzedaz) && $szukanie->sprzedaz != '');
        $sprzedaz = $sprzedaz || (isset($szukanie->dataSprzedazyOd) && $szukanie->dataSprzedazyOd != '');
        $data = (isset($szukanie->dataOd) && $szukanie->dataOd != '');
        $atrybuty = $this->obConfig->atrybuty && ($this->obConfig->atrybutyCeny || (@intval($sortowanie->sort) > 0));
        $atrybCeny = $this->obConfig->atrybuty && ($this->obConfig->atrybutyCenyRozne);
        if ($odwiedziny || $sprzedaz) {
            $atrybuty = false;
            $atrybCeny2 = false;
        }
        $atrybut = ' = -1';
        $rabaty = $this->obConfig->rabaty && !$this->obConfig->rabatyKoszyk;
        $hurt = @$szukanie->typ_klienta || $this->obConfig->tylkoHurt;
        $netto = isset($sortowanie->sort) && $sortowanie->sort == 'cena_netto';
        $netto = $netto || ($hurt && $this->obConfig->cenyNettoDomyslneHurt);
        $netto = $netto || (!$hurt && $this->obConfig->cenyNettoDomyslneDetal);

        if ($rabaty) {
            $rabatKat = '(SELECT rabat FROM Kategorie WHERE id = (select max(id_kat) from Katprod kpr join Kategorie kr on kpr.id_kat = kr.id where kpr.id_prod = p.id and kr.rabat > 0))';
            $user = new Zend_Session_Namespace('kontrahent');
            $userID = @intval($user->id);
            $rabatUser = $userID > 0 ? @floatval($user->dane['rabat']) : 0;
            $rabatUserKat = '(SELECT wartosc_rab FROM Rabaty WHERE kategoria_rab = (select max(id_kat) from Katprod kpr join Rabaty r on kpr.id_kat = r.kategoria_rab where kpr.id_prod = p.id and r.wartosc_rab > 0 and r.id_user = ' . $userID . '))';
            $rabat = 'if(p.promocja, 0, GREATEST(0,';
            if ($this->obConfig->rabatyProdukty)
                $rabat .= 'ifnull(p.rabat, 0),';
            if ($this->obConfig->rabatyProducenci)
                $rabat .= 'ifnull(pp.rabat, 0),';
            if ($this->obConfig->rabatyKategorie)
                $rabat .= 'ifnull(' . $rabatKat . ', 0),';
            if ($userID > 0) {
                if ($this->obConfig->rabatyKlienci)
                    $rabat .= 'ifnull(' . $rabatUser . ', 0),';
                if ($this->obConfig->rabatyKlienciKategorie)
                    $rabat .= 'ifnull(' . $rabatUserKat . ', 0),';
            }
            $rabat .= '0))';
        }

        if ($rozmiar) {
            if ($hurt) {
                $cenaMin = 'if(p.trybCena="rozmiary",';
                $cenaMin.= 'if(p.promocja,';
                $cenaMin.= 'min(if(r.typ>0,rp.cena_promocji_b_hurt,null)),';
                $cenaMin.= 'min(if(r.typ>0,rp.cena_brutto_hurt,null))';
                $cenaMin.= '),';
                $cenaMin.= 'if(p.promocja,p.cena_promocji_b_hurt,p.cena_brutto_hurt)';
                $cenaMin.= ')';
            } else {
                $cenaMin = 'if(p.trybCena="rozmiary",';
                $cenaMin.= 'if(p.promocja,';
                $cenaMin.= 'min(if(r.typ>0,rp.cena_promocji_b,null)),';
                $cenaMin.= 'min(if(r.typ>0,rp.cena_brutto,null))';
                $cenaMin.= '),';
                $cenaMin.= 'if(p.promocja,p.cena_promocji_b,p.cena_brutto)';
                $cenaMin.= ')';
            }
            $cenaSearchClause = 'where';
        } else {
            if ($atrybuty && $this->obConfig->atrybutyCeny) {
                $cenaPromocja = $hurt ? 'cena_promocji_b_hurt' : 'cena_promocji_b';
                $cenaBezPromo = $hurt ? 'cena_brutto_hurt' : 'cena_brutto';
                $atrTyp = '(atp.typatrybut = "lista" or atp.typatrybut = "multi")';
                $promo = 'p.promocja'; // and p.promocjaDo >= "'.date('Y-m-d').'"';
                $cena = 'if(' . $promo . ', p.' . $cenaPromocja . ', p.' . $cenaBezPromo . ')';
                $if11 = $atrTyp . ' and atp.ceny = "1" and atp.cenyTryb = "1"';
                $atr1 = 'if(' . $promo . ',
				min(distinct if(' . $if11 . ',atp.' . $cenaPromocja . ',null)),
				min(distinct if(' . $if11 . ',atp.' . $cenaBezPromo . ',null)))';
                $atr1if = 'sum(if(' . $if11 . ',1,0))>0';
                $if10 = $atrTyp . ' and atp.ceny = "1" and atp.cenyTryb = "0"';
                $atr0 = 'if(' . $promo . ',
				sum(distinct if(' . $if10 . ',atp.' . $cenaPromocja . ',0)),
				sum(distinct if(' . $if10 . ',atp.' . $cenaBezPromo . ',0)))';
                $atr0if = 'sum(if(' . $if10 . ',1,0))>0';
                $cenaMin = 'if(' . $atr1if . ',' . $atr1 . ',if(' . $atr0if . ',' . $cena . '+' . $atr0 . ',' . $cena . '))';
                //$cenaMin = $cena;
                $cenaSearchClause = 'having';
            } else {
                if ($hurt) {
                    if ($this->obConfig->cenyNettoDomyslneHurt)
                        $cenaMin = 'if(p.promocja = 1, p.cena_promocji_n_hurt, p.cena_netto_hurt)';
                    else
                        $cenaMin = 'if(p.promocja = 1, p.cena_promocji_b_hurt, p.cena_brutto_hurt)';
                }
                else {
                    if ($this->obConfig->cenyNettoDomyslneDetal)
                        $cenaMin = 'if(p.promocja = 1, p.cena_promocji_n, p.cena_netto)';
                    else
                        $cenaMin = 'if(p.promocja = 1, p.cena_promocji_b, p.cena_brutto)';
                }
                if (true && $this->obConfig->atrybutyCenyRozne) {
                    $cenaMin = 'if(p.cena_atryb, min(' . str_replace('p.cena', 'atc.cena', $cenaMin) . '), ' . $cenaMin . ')';
                }
                $cenaSearchClause = 'where';
            }
        }
        if ($rabaty)
            $cenaMin = '(' . $cenaMin . ' * (1 - ifnull(' . $rabat . ', 0) / 100))';
        if ($netto)
            $cenaMin = str_replace('_brutto', '_netto', $cenaMin);
        if ($netto)
            $cenaMin = str_replace('_b', '_n', $cenaMin);
        $cenaMinSearch = str_replace('min', '', $cenaMin);
        $cenaMinSearch = ($cenaSearchClause == 'where') ? $cenaMinSearch : 'cena_min';

        if ($ile)
            $select = 'count(distinct p.id)'; //$select .= ', '.$cenaMin.' as cena_min';
        if (!$ile && !$links) {
            $select = 'p.*';
            $select .= ', ' . $cenaMin . ' as cena_min';
            if (false && $rabaty)
                $select .= ', ' . $rabat . ' as rabat_obl';
            if ($rozmiar)
                $select .= ', r.nazwa as rozmiar';
            if ($kolor)
                $select .= ', k.nazwa as kolor';
            if (true)
                $select .= ', ifnull(g.img, "brak.jpg") as img, g.nazwa as img_opis';
            if ($dostepnosc && $rozmiar)
                $select .= ', sum(rp.dostepnosc) as dostepnosc';
            if ($allegro)
                $select .= ', a.wystawiono, a.na_ile, a.nr, ' . $allegroDoKonca;
            if ($allegro)
                $select .= ', a.sztuk_wystawiono, a.sztuk_pozostalo'; //, count(nr) as ile_aukcji';
            if ($odwiedziny)
                $select .= ', sum(o.odwiedziny) as odwiedziny_suma';
            if ($sprzedaz)
                $select .= ', sum(ap.ilosc) as sprzedaz, sum(ap.wartosc) as sprzedaz_wartosc';
            if ($atrybuty)
                $select .= ', atg.nazwa as atrybut,if(at.nazwa is not null,at.nazwa,atp.wartosc) as atrybval';
        }

        $specjalne = '';
        $sort = 'p.nazwa';
        $rozmiaryJoin = 'LEFT';
        if (!$ile)
            if ($sortowanie != null) {
                if ($sortowanie->sort == 'nazwa' && @!empty($szukanie->nazwa))
                    $sort = 'if(p.nazwa like "' . $szukanie->nazwa . '%",1,0) desc, p.nazwa';
                if ($sortowanie->sort == 'oznaczenie')
                    $sort = 'p.oznaczenie';
                if ($sortowanie->sort == 'cena_netto')
                    $sort = 'cena_min';
                if ($sortowanie->sort == 'cena_brutto')
                    $sort = 'cena_min';
                if ($sortowanie->sort == 'data')
                    $sort = 'p.data';
                if ($sortowanie->sort == 'opak')
                    $sort = 'p.jedn_miary_ile';
                if ($sortowanie->sort == 'pozycja')
                    $sort = 'p.pozycja';
                if ($sortowanie->sort == 'rozmiar')
                    $sort = 'r.nazwa';
                if ($sortowanie->sort == 'wkladka')
                    $sort = 'r.wkladka';
                if ($sortowanie->sort == 'kolor')
                    $sort = 'k.nazwa';
                if ($sortowanie->sort == 'allegro_do_konca')
                    $sort = $allegroDoKonca; //'do_konca';
                if ($sortowanie->sort == 'wystawiono')
                    $sort = 'a.wystawiono';
                if ($sortowanie->sort == 'na_ile')
                    $sort = 'a.na_ile';
                if ($sortowanie->sort == 'sztuk_wystawiono')
                    $sort = 'a.sztuk_wystawiono';
                if ($sortowanie->sort == 'sztuk_pozostalo')
                    $sort = 'a.sztuk_pozostalo';
                if ($sortowanie->sort == 'odwiedziny')
                    $sort = 'sum(o.odwiedziny)';
                if ($sortowanie->sort == 'sprzedaz')
                    $sort = 'sum(ap.ilosc)';
                if ($sortowanie->sort == 'sprzedaz_ilosc')
                    $sort = 'sum(ap.ilosc)';
                if ($sortowanie->sort == 'sprzedaz_wartosc')
                    $sort = 'sum(ap.wartosc)';
                if ($sortowanie->sort == 'dostepnosc') {
                    if ($this->obConfig->tryb == 'produkt')
                        $sort = 'p.dostepnosc';
                    else {
                        $sort = 'sum(rp.dostepnosc)';
                        $rozmiaryJoin = 'INNER';
                    }
                }
                if ($sortowanie->specjalne != '' && $sortowanie->specjalne != 'all')
                    $specjalne = $sortowanie->specjalne . ' desc, ';
                if (intval($sortowanie->sort) > 0) {
                    $atrybut = intval($sortowanie->sort);
                    $atrybutygrupy = new Atrybutygrupy();
                    $atrybutygrupy->id = $atrybut;
                    $atryb = $atrybutygrupy->getRow();
                    //var_dump($atryb);
                    if ($atryb['typatrybut'] == 'tekst')
                        $atrybpole = 'atp.wartosc';
                    else
                        $atrybpole = 'at.nazwa';
                    if ($atryb['jedn_miary_typ'] == 'tekst')
                        $sort = $atrybpole;
                    if ($atryb['jedn_miary_typ'] == 'int')
                        $sort = 'CAST(' . $atrybpole . ' AS SIGNED)';
                    if ($atryb['jedn_miary_typ'] == 'float')
                        $sort = 'CAST(' . $atrybpole . ' AS DECIMAL)';
                    $sort = 'if(atp.id_gr = ' . $atrybut . ', ' . $sort . ', "")';
                    $atrybut = ' = ' . $atrybut;
                }
            }

        if ($szukanie != null) {
            //if($szukanie->nazwa != '') $where .= ' and p.nazwa like "%'.$szukanie->nazwa.'%"';
            if ($szukanie->nazwa != '')
                $where .= $this->common->szukajFraz($szukanie->nazwa, 'p.nazwa', 1, 0, 1, $this->module == 'default', 0);
            //if(@!empty($szukanie->nazwa)) $where .= ' and (p.nazwa like "'.$szukanie->nazwa.'%" or 
            //p.nazwa like "% '.$szukanie->nazwa.'%" or p.nazwa like "%-'.$szukanie->nazwa.'%")';
            if ($szukanie->oznaczenie != '')
                $where .= ' and p.oznaczenie like "%' . $szukanie->oznaczenie . '%"';
            //if(@!empty($szukanie->oznaczenie)) $where .= ' and (p.oznaczenie like "'.$szukanie->oznaczenie.'%" or 
            //p.oznaczenie like "% '.$szukanie->oznaczenie.'%" or p.oznaczenie like "%-'.$szukanie->oznaczenie.'%")';
            if (@!empty($szukanie->marka))
                $where .= ' and (pp.nazwa like "%' . $szukanie->marka . '%")';
            if (isset($szukanie->grupa) && @$szukanie->grupa != 'all')
                if ($this->module == 'admin')
                    $where.=' and (p.grupa=' . $szukanie->grupa . ')';
                else
                    $where .= ' and (p.grupa = "' . $szukanie->grupa . '" or p.grupa = -1)';
            if (isset($szukanie->rozmiar) && $szukanie->rozmiar != 'all')
                $where .= ' and r.nazwa = "' . $szukanie->rozmiar . '"';
            if (isset($szukanie->wkladka) && $szukanie->wkladka != 'all')
                $where .= ' and r.wkladka = "' . $szukanie->wkladka . '"';
            if (@$szukanie->kolor != -1 && $kolor)
                $where .= ' and k.id = ' . $szukanie->kolor;
            if (@$szukanie->cena_netto > 0)
                $$cenaSearchClause .= ' and ' . $cenaMinSearch . ' = ' . str_replace(",", ".", $szukanie->cena_netto);
            if (@$szukanie->cena_brutto > 0)
                $$cenaSearchClause .= ' and ' . $cenaMinSearch . ' = ' . str_replace(",", ".", $szukanie->cena_brutto);
            if (@$szukanie->cena_od > 0)
                $$cenaSearchClause .= ' and ' . $cenaMinSearch . ' >= ' . str_replace(",", ".", $szukanie->cena_od);
            if (@$szukanie->cena_do > 0)
                $$cenaSearchClause .= ' and ' . $cenaMinSearch . ' <= ' . str_replace(",", ".", $szukanie->cena_do);
            if (@$szukanie->producent > 0)
                $where .= ' and p.producent = ' . $szukanie->producent . '';
            if (isset($szukanie->nowosc) && $szukanie->nowosc)
                $where .= ' and p.nowosc = "1"';
            if (isset($szukanie->promocja) && $szukanie->promocja)
                $where .= ' and p.promocja = "1"';
            if (isset($szukanie->promocjaNet) && $szukanie->promocjaNet)
                $where .= ' and p.promocja_net = "1"';
            if (isset($szukanie->wyprzedaz) && $szukanie->wyprzedaz)
                $where .= ' and p.wyprzedaz = "1"';
            //if(isset($szukanie->bestseller) && $szukanie->bestseller) $where .= ' and p.bestseller = "1"';
            if (isset($szukanie->polecane) && $szukanie->polecane)
                $where .= ' and p.polecane = "1"';
            if (isset($szukanie->specjalne) && $szukanie->specjalne)
                $where .= ' and p.specjalne = "1"';
            if (isset($szukanie->produktDnia) && $szukanie->produktDnia)
                $where .= ' and p.produkt_dnia = "1"';
            if (isset($szukanie->glowna) && $szukanie->glowna)
                $where .= ' and p.glowna = "1"';
            if (isset($szukanie->widoczny) && $szukanie->widoczny)
                $where .= ' and !p.widoczny';
            else if ($this->module != 'admin')
                $where .= ' and p.widoczny';

            if (isset($szukanie->aukcje) && $szukanie->aukcje == 'all')
                $where .= ' and a.wystawiono is not null and (a.na_ile = 0 or (DATE_ADD(a.wystawiono,INTERVAL a.na_ile DAY)-NOW()) > 0)';

            if (isset($szukanie->odwiedziny) && $szukanie->odwiedziny != '') {
                $where .= ' and o.data is not null';
                if ($szukanie->odwiedziny != 'all') {
                    $where .= ' and o.data >= "' . $szukanie->odwiedziny . '-01-01"';
                    $where .= ' and o.data <= "' . $szukanie->odwiedziny . '-12-31"';
                }
            }
            if (isset($szukanie->dataOdwiedzinOd) && $szukanie->dataOdwiedzinOd != '')
                if (isset($szukanie->dataOdwiedzinDo) && $szukanie->dataOdwiedzinDo != '') {
                    $where .= ' and o.data is not null';
                    if ($szukanie->dataOdwiedzinOd != 'all')
                        if ($szukanie->dataOdwiedzinDo != 'all') {
                            $where .= ' and o.data >= "' . $szukanie->dataOdwiedzinOd . '"';
                            $where .= ' and o.data <= "' . $szukanie->dataOdwiedzinDo . '"';
                        }
                }
            if (isset($szukanie->sprzedaz) && $szukanie->sprzedaz != '') {
                $where .= ' and ak.data is not null';
                if ($szukanie->sprzedaz != 'all') {
                    $where .= ' and ak.data >= "' . $szukanie->sprzedaz . '-01-01 00:00:00"';
                    $where .= ' and ak.data <= "' . $szukanie->sprzedaz . '-12-31 23:59:59"';
                }
            }
            if (isset($szukanie->dataSprzedazyOd) && $szukanie->dataSprzedazyOd != '')
                if (isset($szukanie->dataSprzedazyDo) && $szukanie->dataSprzedazyDo != '') {
                    $where .= ' and ak.data is not null';
                    if ($szukanie->dataSprzedazyOd != 'all')
                        if ($szukanie->dataSprzedazyDo != 'all') {
                            $where .= ' and ak.data >= "' . $szukanie->dataSprzedazyOd . ' 00:00:00"';
                            $where .= ' and ak.data <= "' . $szukanie->dataSprzedazyDo . ' 23:59:59"';
                        }
                }
            if (isset($szukanie->dataOd) && $szukanie->dataOd != '')
                if (isset($szukanie->dataDo) && $szukanie->dataDo != '') {
                    $where .= ' and p.data is not null';
                    if ($szukanie->dataOd != 'all')
                        if ($szukanie->dataDo != 'all') {
                            $where .= ' and p.data >= "' . $szukanie->dataOd . ' 00:00:00"';
                            $where .= ' and p.data <= "' . $szukanie->dataDo . ' 23:59:59"';
                        }
                }
            if (@count($szukanie->atrybuty) > 0) {
                $atrybutypowiazania = new Atrybutypowiazania();
                $prods = $atrybutypowiazania->getProdukty($szukanie->atrybuty);
                if (is_array($prods))
                    $where .= ' and p.id in (' . implode(',', $prods) . ')';
                //$atrybut = -1;//' in ('.implode(',', array_keys($szukanie->atrybuty)).')';
            }
            if (@$szukanie->pokazTylkoProduktyWKatNieWPodKat) {
                $where .= ' and id_prod not in (select distinct id_prod from Kategorie k INNER JOIN Katprod kp ON(k.id = kp.id_kat) WHERE k.rodzic = ' . $this->id . ')';
            }
        } else if ($this->module != 'admin')
            $where .= ' and p.widoczny = "1"';
        if ($this->module != 'admin' && $prodGrupy)
            $where .= ' and (pg.id is null or pg.wyswietl = "1")';
        if ($this->module != 'admin' && $this->ustawienia['produkty_bez_zdjec'])
            $where .= ' and g.img is not null';

        $sql = 'SELECT ' . $select . ' FROM Produkty p ';
        if ($prodGrupy)
            $sql .= 'LEFT JOIN Produktygrupy pg ON(p.grupa = pg.id) ';
        if ($rozmiar)
            $sql .= 'LEFT JOIN Rozmiarproduktu rp ON(p.id = rp.id_produktu) ';
        if ($rozmiar)
            $sql .= 'LEFT JOIN Rozmiary r ON(r.id = rp.id_koloru and r.typ = p.rozmiarowka) ';
        if (true)
            $sql .= 'LEFT JOIN Katprod kat ON(p.id = kat.id_prod and kat.typ = "kategorie") ';
        if ($katInne)
            $sql .= 'LEFT JOIN Katprod inne ON(p.id = inne.id_prod and inne.typ = "' . $katInneNazwa . '") ';
        if ($kolor)
            $sql .= 'LEFT JOIN Kolorproduktu kp ON(p.id = kp.id_produktu) ';
        if ($kolor)
            $sql .= 'LEFT JOIN Kolory k ON(k.id = kp.id_koloru) ';
        if ($producent)
            $sql .= 'LEFT JOIN Producent pp ON(pp.id = p.producent) ';
        if (true)
            $sql .= 'LEFT JOIN Galeria g ON(p.id = g.wlasciciel and g.typ = "oferta" and g.glowne = "T") ';
        if ($allegro)
            $sql .= 'LEFT JOIN Allegroaukcje a ON(a.id_prod = p.id) ';
        if ($odwiedziny)
            $sql .= 'LEFT JOIN Odwiedziny o ON(o.id_prod = p.id and o.typ = "produkty") ';
        if ($sprzedaz)
            $sql .= 'LEFT JOIN Archiwumprodukty ap ON(ap.id_prod = p.id) ';
        if ($sprzedaz)
            $sql .= 'LEFT JOIN Archiwumkontrahenci ak ON(ap.id_zam = ak.id) ';
        if ($atrybuty)
            $sql .= 'LEFT JOIN Atrybutypowiazania atp ON(p.id = atp.id_og) '; // and atp.id_gr '.$atrybut.') ';
        if ($atrybuty)
            $sql .= 'LEFT JOIN Atrybuty at ON(atp.id_at = at.id) ';
        if ($atrybuty)
            $sql .= 'LEFT JOIN Atrybutygrupy atg ON(if(atp.id_gr>0,atp.id_gr,at.id_gr) = atg.id) ';
        if ($atrybCeny)
            $sql .= 'LEFT JOIN Atrybutyceny atc ON(p.id = atc.id_prod) ';
        $sql .= 'WHERE ' . $where . ' ';
        $group = $rozmiar || $allegro || $atrybCeny || strlen($having) > 1;
        $group = $group || (strpos($select, 'sum(') !== false) || (strpos($select, 'count(') !== false);
        $group = $group || (strpos($sort, 'sum(') !== false) || (strpos($sort, 'count(') !== false);
        if (!$ile && $group)
            $sql .= 'GROUP BY p.id HAVING ' . $having . ' ';
        if (!$ile)
            $sql .= 'ORDER BY ' . $specjalne . ' ' . $sort . ' ' . $sortowanie->order . ' ';
        $sql .= 'LIMIT ' . $od . ', ' . $limit . ' ';
        $sql = str_replace('=null', '= "null"', $sql);
        $sql = str_replace('= null', '="null"', $sql);
        if (!$ile && $test)
            echo $sql; //die();
            
//return null;
        $result = $this->db->fetchAll($sql);
        return $ile ? @intval($result[0]['count(distinct p.id)']) : $result;
    }

    public function dodajNowePowiazania($id_kat, $rodzic) {
        $sql = 'insert into Katprod (id_prod, id_kat)';
        $sql.= ' select distinct id_prod, ' . $id_kat . ' from Katprod kp';
        $sql.= ' where kp.id_kat = ' . $rodzic;
        //echo $sql;
        $this->db->query($sql);
        if ($this->obConfig->caching)
            $this->common->cacheRemove($sql, 'Katprod');
    }

    public function usunJedynePowiazanie($id_kat, $rodzic) {
        $sql = 'select distinct id_prod from Katprod where id_kat = ' . $rodzic;
        //echo $sql;
        $wTejKateg = $this->db->fetchAll($sql);
        //var_dump($result);

        $sql = 'select distinct id_prod from Katprod kp join Kategorie k on k.id = kp.id_kat where k.rodzic = ' . $id_kat;
        $sql.= ' group by id_prod having count(*) = 1';
        //echo $sql;
        $wKatWyzej = $this->db->fetchAll($sql);
        //var_dump($result);

        if (@count($wKatWyzej) > 0)
            foreach ($wKatWyzej as $prod)
                $prodsWKatWyzej[$prod['id_prod']] = $prod['id_prod'];

        if (@count($wTejKateg) > 0)
            foreach ($wTejKateg as $prod)
                if (isset($prodsWKatWyzej[$prod['id_prod']]))
                    $prods[] = $prod['id_prod'];

        if (@count($prods) > 0) {
            $sql = 'delete from Katprod where id_kat = ' . $id_kat . ' and id_prod in (' . implode(',', $prods) . ')';
            //echo $sql;
            $this->db->query($sql);
            if ($this->obConfig->caching)
                $this->common->cacheRemove($sql, 'Katprod');
        }
    }

    public function edytuj($id, $data) {
        $where = 'id_kat_prod = "' . $id . '"';
        return $this->update($data, $where);
    }

    public function add($id_kat, $id_prod, $typ = 'kategorie') {
        $dane = array('id_prod' => $id_prod, 'id_kat' => $id_kat, 'typ' => $typ);
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }

    public function addAll($ids, $id_kat, $typ = 'kategorie') {
        $sql = 'insert into Katprod (id_prod, id_kat, typ)';
        $sql.= ' select distinct id, ' . $id_kat . ', "' . $typ . '" from Produkty p';
        $sql.= ' where p.id in (' . $ids . ')';
        //echo $sql;
        $this->db->query($sql);
        if ($this->obConfig->caching)
            $this->common->cacheRemove($sql, 'Katprod');
    }

    /////// wyświetla kategorie przypisane do danego produkty
    public function selectKategorie() {
        $select = $this->db->select()
                        ->from(array('kp' => 'Katprod'), array('*'))
                        ->joinleft(array('k' => 'Kategorie'), 'k.id = kp.id_kat', array('*'))
                        ->where('kp.id_prod = ' . $this->id . ' and k.typ = "' . $this->typ . '"')
                        ->group('k.id')->order(array('if(rodzic=0,id,rodzic)', 'id')); // empty list of columns
        //echo $select;//die();
        $result = $this->db->fetchAll($select);
        //var_dump($result);
        return $result;
    }

    public function getPathString($id, $nawigacja = false, $sep = '/', $last = '>', $spa = ' ') {
        if ($nawigacja === false)
            $nawigacja = $this->kategorieProduktu($id, 'id_kat', 'asc', $this->typ);
        $result = '';
        for ($i = 0; $i < count($nawigacja); $i++) {
            if (@$nawigacja[$i]['typ'] != $this->typ)
                continue;
            $result .= $nawigacja[$i]['nazwa'];
            if ($i < count($nawigacja) - 2)
                $result .= $spa . $sep . $spa;
            if ($i == count($nawigacja) - 2)
                $result .= $spa . $last . $spa;
        }
        return $result;
    }

    public function kategorieProduktu($id, $sort = 'id_kat', $order = 'asc', $typ = '') {
        $select = $this->db->select()
                        ->from(array('kp' => 'Katprod'), array('*'))
                        ->joinleft(array('k' => 'Kategorie'), 'k.id = kp.id_kat', array('*'))
                        ->where('kp.id_prod = ' . $id . ' and ' . (!empty($typ) ? 'k.typ = "' . $typ . '"' : 1))
                        ->group('k.id')->order($sort . ' ' . $order); // empty list of columns

        $result = $this->db->fetchAll($select);
        // print_r($result);
        return $result;
    }

    public function deleteProdukt() {
        return $this->db->delete('Katprod', 'id_prod = ' . $this->id);
    }

    public function deleteProduktyIds($ids, $typ = 'kategorie') {
        $this->db->delete('Katprod', 'id_prod in (' . $ids . ') and typ = "' . $typ . '"');
        $this->common->cacheRemove(null, 'Katprod');
    }

    public function deleteKategoria() {
        $this->db->delete('Katprod', 'id_kat = ' . $this->id);
    }

    public function delete($id) {
        $this->db->delete('Katprod', 'id_kat_prod = "' . $id . '"');
    }

    function wypiszOne($prodID = 0, $katID = 0) {
        $select = $this->db->select()
                ->from(array('kp' => 'Katprod'), array('*'))
                ->where('id_prod = ' . $prodID . ' and id_kat = ' . $katID);
        $result = $this->db->fetchAll($select);
        return $result;
    }

    function wypiszAll() {
        $select = $this->db->select()->from(array('kp' => 'Katprod'), array('*'));
        $result = $this->db->fetchAll($select);
        return $result;
    }

    public function selectKategorieOfe() {
        $select = $this->db->select()
                ->from(array('kp' => 'Katprod'), array('*'))
                ->joinleft(array('k' => 'Kategorie'), 'k.id = kp.id_kat', array('*'))
                ->where("kp.id_prod =$this->id")
                ->group('k.id'); // empty list of columns

        $result = $this->db->fetchAll($select);
        // print_r($result);
        return $result;
    }

    public function wypiszAllFromKateg($katID = 0) {
        $select = $this->db->select()
                        ->from(array('p' => 'Produkty'), array('*'))
                        ->joinleft(array('k' => 'Katprod'), 'p.id = k.id_prod', array(''))
                        ->where('k.id_kat = ' . $katID)->group('p.id');

        $result = $this->db->fetchAll($select);
        // print_r($result);
        return $result;
    }

}

?>