<?php

class AuthDataV2
{
	public $login;
	public $masterFID;
	public $password;
	
	public function __construct($module = 'admin')
	{
		$upsDane = new Upsdane($module);
		$this->upsDane = $upsDane->wypisz();
		$this->login = $this->$upsDane['login'];
		$this->masterFID = $this->$upsDane['FID'];
		$this->password = $this->$upsDane['haslo'];
    }
};

class Ups extends Zend_Db_Table
{
	public $id;
	
	public $url1 = 'https://dpdservices.dpd.com.pl/DPDPackageXmlServicesService/DPDPackageXmlServices?wsdl';
	public $url2 = 'https://dpdservices.dpd.com.pl/DPDPackageObjServicesService/DPDPackageXmlServices?wsdl';
	public $url_test1 = 'https://dpdservicesdemo.dpd.com.pl/DPDPackageXmlServicesService/DPDPackageXmlServices?wsdl';
	public $url_test2 = 'https://dpdservicesdemo.dpd.com.pl/DPDPackageObjServicesService/DPDPackageXmlServices?wsdl';
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
		$this->daneKlient = array('address','city','company','countryCode','email','FID','name','phone','postalCode');
    }
	
	function dodaj($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	function edytuj($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	function edytujRef($dane, $ref)
	{
		$where = 'reference = "'.$ref.'"';
		$this->update($dane, $where);
	}
	function wypisz()
	{
		$result = $this->fetchAll();
		return $result;
	}
	function wypiszZam($id)
	{
		$result = $this->fetchAll('id_zam = '.$id);
		return $result;
	}
	function wypiszRaport($data)
	{
		$result = $this->fetchAll('data >= "'.$data.' 00:00:00" and data <= "'.$data.' 23:59:59"');
		return $result;
	}

	function wypiszJeden()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	function usunJeden()
	{
		$result = $this->delete('id = '.$this->id);
	}
    function usunNumer()
	{
		$result = $this->delete('ShipmentIdentificationNumber = "'.$this->id.'"');
	}
	
    function dane($dane)
	{
		if(is_array($dane))
		{
			foreach($dane as $id => $value)
			$dane[$id] = $this->dane($value);
		}
        else $dane = $this->common->removePolish($dane);
		return $dane;
	}
    function funkcjaSOAP($nazwa, $config = null, $odbiorca = null)
	{
		$upsDane = new Upsdane();
		$daneKurier = $upsDane->wypisz();
        
        $requestoption['RequestOption'] = 'nonvalidate';
        $request['Request'] = $requestoption;
		
		if($nazwa == 'shipmentConfirm' || $nazwa == 'shipmentAccess')
		{
            $shipment['Description'] = $config['Description'];
            $shipper['Name'] = $config['Name'];
            $shipper['AttentionName'] = $config['AttentionName'];
            $shipper['TaxIdentificationNumber'] = $config['TaxIdNumber'];
            $shipper['ShipperNumber'] = $daneKurier['clientid'];
            $address['AddressLine'] = $config['AddressLine'];
            $address['City'] = $config['City'];
            $address['StateProvinceCode'] = $config['StateProvinceCode'];
            $address['PostalCode'] = $config['PostalCode'];
            $address['CountryCode'] = 'PL';//$config['CountryCode'];
            $shipper['Address'] = $address;
            $phone['Number'] = $config['PhoneNumber'];
            $phone['Extension'] = $config['Extension'];
            $shipper['Phone'] = $phone;
            $shipper['EMailAddress'] = $config['EMailAddress'];
            $shipment['Shipper'] = $shipper;

            $shipto['Name'] = $odbiorca['Name'];
            $shipto['AttentionName'] = $odbiorca['AttentionName'];
            $shipto['TaxIdentificationNumber'] = $odbiorca['TaxIdNumber'];
            $addressTo['AddressLine'] = $odbiorca['AddressLine'];
            $addressTo['City'] = $odbiorca['City'];
            $addressTo['StateProvinceCode'] = $odbiorca['StateProvinceCode'];
            $addressTo['PostalCode'] = $odbiorca['PostalCode'];
            $addressTo['CountryCode'] = 'PL';//$odbiorca['CountryCode'];
            //$addressTo['ResidentialAddressIndicator'] = null;
            $phone2['Number'] = $odbiorca['PhoneNumber'];
            $phone2['Extension'] = $odbiorca['Extension'];
            $shipto['Address'] = $addressTo;
            $shipto['Phone'] = $phone2;
            $shipto['EMailAddress'] = $odbiorca['EMailAddress'];
            $shipment['ShipTo'] = $shipto;

            $shipfrom['Name'] = $config['Name'];
            $shipfrom['AttentionName'] = $config['AttentionName'];
            $addressFrom['AddressLine'] = $config['AddressLine'];
            $addressFrom['City'] = $config['City'];
            $addressFrom['StateProvinceCode'] = $config['StateProvinceCode'];
            $addressFrom['PostalCode'] = $config['PostalCode'];
            $addressFrom['CountryCode'] = 'PL';//$config['CountryCode'];
            $phone3['Number'] = $config['PhoneNumber'];
            $shipfrom['Address'] = $addressFrom;
            $shipfrom['Phone'] = $phone3;
            $shipment['ShipFrom'] = $shipfrom;

            $shipmentcharge['Type'] = '01';
            $creditcard['Type'] = $config['CreditCardType'];
            $creditcard['Number'] = $config['CreditCardNumber'];
            $creditcard['SecurityCode'] = $config['SecurityCode'];
            $creditcard['ExpirationDate'] = $config['ExpirationDate'];
            $creditCardAddress['AddressLine'] = $config['CCardAddressLine'];
            $creditCardAddress['City'] = $config['CCardCity'];
            $creditCardAddress['StateProvinceCode'] = $config['CCardProvinceCode'];
            $creditCardAddress['PostalCode'] = $config['CCardPostalCode'];
            $creditCardAddress['CountryCode'] = 'PL';//$config['CCardCountryCode'];
            $creditcard['Address'] = $creditCardAddress;
            if($config['BillShipperCCard']) $billshipper['CreditCard'] = $creditcard;
            else $billshipper['AccountNumber'] = $daneKurier['clientid'];            
            $shipmentcharge['BillShipper'] = $billshipper;
            $paymentinformation['ShipmentCharge'] = $shipmentcharge;
            $shipment['PaymentInformation'] = $paymentinformation;

            $ReferenceNumber['Code'] = 'PO';
            $ReferenceNumber['Value'] = $this->nr;//nr zam
            $shipment['ReferenceNumber'] = $ReferenceNumber;
            $shipment['PackageID'] = $this->id;//Alpha numeric values only

            $service['Code'] = $config['ServiceCode'];
            $service['Description'] = $config['ServiceDescription'];
            $shipment['Service'] = $service;
            //$ShipmentServiceOptions['SaturdayDeliveryIndicator'] = null;
            //$shipment['ShipmentServiceOptions'] = $ShipmentServiceOptions;

            $package['Description'] = $config['PackageDescription'];
            $packaging['Code'] = $config['PackagingCode'];
            $packaging['Description'] = $config['PackagingDescription'];
            $package['Packaging'] = $packaging;
            $unit['Code'] = $config['PackageDimUMCode'];
            $unit['Description'] = $config['PackageDimUMDesc'];
            $dimensions['UnitOfMeasurement'] = $unit;
            $dimensions['Length'] = $config['PackageDimLength'];
            $dimensions['Width'] = $config['PackageDimWidth'];
            $dimensions['Height'] = $config['PackageDimHeight'];
            $package['Dimensions'] = $dimensions;
            $unit2['Code'] = $config['PackageWeightUMCode'];
            $unit2['Description'] = $config['PackageWeightUMDesc'];
            $packageweight['UnitOfMeasurement'] = $unit2;
            $packageweight['Weight'] = $config['PackageWeight'];
            $package['PackageWeight'] = $packageweight;
            //$package['LargePackageIndicator'] = null;
            $shipment['Package'] = $package;

            $labelimageformat['Code'] = $config['LabelFormatCode'];
            $labelimageformat['Description'] = $config['LabelFormatDesc'];
            $labelspecification['LabelImageFormat'] = $labelimageformat;
            $labelspecification['HTTPUserAgent'] = $config['LabelHTTPUserAgent'];
            $shipment['LabelSpecification'] = $labelspecification;
            $request['Shipment'] = $shipment;
		}
        else if($nazwa == 'LabelRecovery')
		{
            $tref['CustomerContext'] = '';
            $req['TransactionReference'] = $tref;
            $request['Request'] = $req;
            $request['TrackingNumber'] = $config;
		}
		else if($nazwa == 'ProcessVoid')
		{
            $tref['CustomerContext'] = '';
            $req['TransactionReference'] = $tref;
            $request['Request'] = $req;
            $voidshipment['ShipmentIdentificationNumber'] = $config;
            $request['VoidShipment'] = $voidshipment;
		}
		else $request = $config;
        
        $request = $this->dane($request);
        
        return $request;
    }
    
    function funkcjaXML($nazwa, $config = null, $odbiorca = null)
	{
		$upsDane = new Upsdane();
		$daneKurier = $upsDane->wypisz();

        $accessSchemaFile = "./ups/Schemas/AccessRequest.xsd";
        $requestSchemaFile = "./ups/Schemas/ShipConfirmRequest.xsd";
        $responseSchemaFile = "./ups/Schemas/ShipConfirmResponse.xsd";
        $ifSchemaFile = " Add IF Schema File";
		//var_dump(class_exists("SDO_DAS_XML"));die();
		if($nazwa == 'shipmentConfirm')
		{
            $das = SDO_DAS_XML::create("$accessSchemaFile");
            $doc = $das->createDocument();
            $root = $doc->getRootDataObject();
            $root->AccessLicenseNumber = $daneKurier['accesskey'];
            $root->UserId = $daneKurier['userid'];
            $root->Password = $daneKurier['haslo'];
            $security = $das->saveString($doc);

            //create ShipConfirmRequest data oject
            $das = SDO_DAS_XML::create("$requestSchemaFile");
            $das->addTypes("$ifSchemaFile");
            $requestDO = $das->createDataObject('','RequestType');
            $requestDO->RequestAction = 'ShipConfirm';
            $requestDO->RequestOption = 'nonvalidate';

            $doc = $das->createDocument('ShipmentConfirmRequest');
            $root = $doc->getRootDataObject();
            $root->Request = $requestDO;

            $labelSpecificationDO = $das->createDataObject('' , 'LabelSpecificationType');
            $labelPrintMethodDO = $das->createDataObject('' , 'LabelPrintMethodCodeDescriptionType');
            $labelPrintMethodDO->Code = $config['LabelFormatCode'];
            $labelPrintMethodDO->Description = $config['LabelFormatDesc'];
            $labelSpecificationDO->LabelPrintMethod = $labelPrintMethodDO;
            $labelSpecificationDO->HTTPUserAgent = $config['LabelHTTPUserAgent'];
            $labelImageFormatDO = $das->createDataObject('' , 'LabelImageFormatCodeDescriptionType');
            $labelImageFormatDO->Code = $config['LabelFormatCode'];
            $labelImageFormatDO->Description = $config['LabelFormatDesc'];
            $labelSpecificationDO->LabelImageFormat = $labelImageFormatDO;
            $root->LabelSpecification = $labelSpecificationDO;

            $shipmentDO = $das->createDataObject('','ShipmentType');
            $rateInfoDO = $das->createDataObject('','RateInformationType');
            $rateInfoDO->NegotiatedRatesIndicator = '';
            $shipmentDO->RateInformation = $rateInfoDO;
            $shipmentDO->Description = '';

            $shipperDO = $das->createDataObject('', 'ShipperType');
            $shipperDO->Name = $config['Name'];
            $shipperDO->PhoneNumber = $config['PhoneNumber'];
            $shipperDO->TaxIdentificationNumber = $config['TaxIdNumber'];
            $shipperDO->ShipperNumber = $daneKurier['userid'];
            $addressDO = $das->createDataObject('' , 'ShipperAddressType');
            $addressDO->AddressLine1 = $config['AddressLine'];
            $addressDO->City = $config['City'];
            $addressDO->StateProvinceCode = $config['StateProvinceCode'];
            $addressDO->PostalCode = $config['PostalCode'];
            $addressDO->CountryCode = $config['CountryCode'];
            $shipperDO->Address = $addressDO;
            $shipmentDO->Shipper = $shipperDO;

            $shipToDO = $das->createDataObject('','ShipToType');
            $shipToDO->CompanyName = $odbiorca['Name'];
            $shipToDO->AttentionName = $odbiorca['AttentionName'];
            $shipToDO->PhoneNumber = $odbiorca['PhoneNumber'];
            $addressToDO = $das->createDataObject('','ShipToAddressType');
            $addressToDO->AddressLine1 = $odbiorca['AddressLine'];
            $addressToDO->City = $odbiorca['City'];
            $addressToDO->StateProvinceCode = $odbiorca['StateProvinceCode'];
            $addressToDO->PostalCode = $odbiorca['PostalCode'];
            $addressToDO->CountryCode = $odbiorca['CountryCode'];
            $shipToDO->Address = $addressToDO;
            $shipmentDO->ShipTo = $shipToDO;

            $shipFromDO = $das->createDataObject('', 'ShipFromType');
            $shipFromDO->CompanyName = $config['Name'];
            $shipFromDO->AttentionName = $config['AttentionName'];
            $shipFromDO->PhoneNumber = $config['PhoneNumber'];
            $shipFromDO->TaxIdentificationNumber = $config['TaxIdNumber'];
            $addressFromDO = $das->createDataObject('','ShipFromAddressType');
            $addressFromDO->AddressLine1 = $config['AddressLine'];
            $addressFromDO->City = $config['City'];
            $addressFromDO->StateProvinceCode = $config['StateProvinceCode'];
            $addressFromDO->PostalCode = $config['PostalCode'];
            $addressFromDO->CountryCode = $config['CountryCode'];
            $shipFromDO->Address = $addressFromDO;
            $shipmentDO->ShipFrom = $shipFromDO;

            $paymentInfoDO = $das->createDataObject('','PaymentInformationType');
            $prepaidDO = $das->createDataObject('' , 'PrepaidType');
            $billshipperDO = $das->createDataObject('', 'BillShipperType');
            $billshipperDO->AccountNumber = $daneKurier['userid'];
            $prepaidDO->BillShipper = $billshipperDO;
            $paymentInfoDO->Prepaid = $prepaidDO;
            $shipmentDO->PaymentInformation = $paymentInfoDO;

            $serviceDO = $das->createDataObject('','ServiceType');
            $serviceDO->Code = $config['ServiceCode'];
            $serviceDO->Description = $config['ServiceDescription'];
            $shipmentDO->Service = $serviceDO;

            $packageDO = $das->createDataObject('' , 'PackageType');
            $packagingTypeDO = $das->createDataObject('' , 'PackagingTypeType');
            $packagingTypeDO->Code = $config['PackagingCode'];
            $packagingTypeDO->Description = $config['PackagingDescription'];
            $packageDO->PackagingType = $packagingTypeDO;
            $packageDO->Description = $config['PackageDescription'];

            $referenceNumberDO = $das->createDataObject('', 'ReferenceNumberType');
            $referenceNumberDO->Code = 'PO';
            $referenceNumberDO->Value = $this->nr;
            $packageDO->ReferenceNumber = $referenceNumberDO;

            $packageWeightDO = $das->createDataObject('' , 'PackageWeightType');
            $unitDO = $das->createDataObject('' , 'UnitOfMeasurementType');
            $unitDO->Code = $config['PackageWeightUMCode'];
            $unitDO->Value = $config['PackageWeightUMDesc'];
            $packageWeightDO->UnitOfMeasurement = $unitDO;            
            $packageWeightDO->Weight = $config['PackageWeight'];
            $packageDO->PackageWeight = $packageWeightDO;
            //$packageDO->LargePackageIndicator = '';
            //$packageDO->AdditionalHandling = '0';
            $shipmentDO->Package = $packageDO;
            $root->Shipment = $shipmentDO;
            $request = $das->saveString($doc);
		}
		else if($nazwa == 'shipmentAccess')
		{
            
		}
		else $dane = $config;
        
        $form = array
        (
            'http' => array
            (
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => "$security$request"
            )
        );
		
        return $form;
    }
}
?>