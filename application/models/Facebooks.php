<?php
class Facebooks extends Zend_Db_Table
{
    public $ID, $login, $choose;
    protected $_name = 'Facebooks';
	public $link, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->ID = 1;
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);		
    }

    function dodaj($dane)
	{
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }
    function edytuj($dane)
	{
        $where = 'id = "'.$this->ID.'"';
        $this->update($dane, $where);
    }
    function usun()
	{
        $where = 'id = "'.$this->ID.'"';
        $this->delete($where);
    }

    function klient()
	{
        $where = 'id = "'.$this->ID.'"';
        $result = $this->fetchRow($where);
        //$res = $result->toArray();
        return $result;
    }
    function listaKlient()
	{
        $result = $this->fetchAll();
        $res = $result->toArray();
        return $res;
    }
	
	function login($page = 1)
	{
		$this->ID = 1;
		$keys = $this->klient();
		
        $this->ID = $page;
		$face = $this->klient();
		
		$faces = array
		(
			'appId' => $keys['appid'],
			'secret' => $keys['secret']
		);
		
		$data['ustaw'] = $face;
		$data['faces'] = $facebook = new Facebook($faces);
		$data['error'] = '';
		
		if(false || empty($face['userid']))
		{
			//$login_url = $facebook->getLoginUrl(array('scope' => 'publish_stream'));
			$data['error'] = 'Błąd, nie podano ID strony';
		}
		
		return $data;
    }
	
	function dodajProdukt($id = 0, $page = 1)
	{
		$this->ustawienia = $this->common->getUstawienia();
		
		$data = $this->login($page);
		if(!empty($data['error'])) return $data['error'];
		$settings = $data['ustaw'];
		$facebook = $data['faces'];
		
		try
		{
			if($page > 1)
			{
				$pageInfo = $facebook->api("/".$data['ustaw']['userid']."?fields=access_token");
				$access_token = @$pageInfo['access_token'];
			}
			
			$produkty = new Produkty();
			$produkty->id = $id;
			$produkt = $produkty->wypiszPojedyncza();
			$this->common = new Common(true);
			$produkt = $this->common->produktyGaleria(array($produkt));
			$produkt = $produkt[0];
		
			$img = new Galeria();
			$zdj = $img->wyswietlGlowneProdukt($id);
			if(@empty($zdj['img'])) $zdj['img'] = 'brak.jpg';
			
			$src = 'public/admin/zdjecia/tiny/'.$zdj['img'];
			$link = $this->obConfig->www.'/'.$produkt['link'];
			$path = $this->obConfig->www.'/'.$src;
			
			$privacy = array
			(
				"description" => "Public",
				"value" => "EVERYONE",
				"friends" => "",
				"networks" => "",
				"allow" => "",
				"deny" => ""
			);
			
			$cena = number_format($produkt['brutto'], 2, ',', ' ');
			
			$opis = $this->ustawienia['skrot'] ? $produkt['skrot'] : $produkt['tekst'];
			$opis = strip_tags($opis);
			$opis = substr($opis, 0, 100);
			$opis = htmlspecialchars_decode($opis);
			$opis = str_replace('&nbsp;', ' ', $opis);
			$opis = str_replace('&oacute;', 'ó', $opis);
			
			$message = $produkt['nazwa']."\n\n";
			if(!empty($opis)) $message .= $opis."\n\n";
			$message .= "Cena: ".$cena." zł\n\n";
			$message .= "Sprawdź na\n".$link;
			
			$params = array
			(
				'name' => $produkt['nazwa'],
				'message' => $message,
				//'access_token' => '',
				'link' => $link,
				'caption' => 'Cena: '.$cena.' zł',
				'picture' => $path,
				'description' => "Więcej szczegółów znajdziesz na\n".$link,
				//'privacy' => $privacy,
				//'type' => "feed",
				//"promotion_status" => "ineligible"
			);
			if(@!empty($access_token)) $params['access_token'] = $access_token;
			//echo nl2br($params['message']);die();
			$post = $facebook->api('/'.$data['ustaw']['userid'].'/feed', 'post', $params);
			
			$facebooksprods = new Facebooksprods();
			$facebookpost = $facebooksprods->wypiszPost($page, $id);
			
			//usuwanie starego wpisu
			if(!empty($facebookpost)) // if($produkt['facebookpostid'] != null)
			{
				$facebook->api('/'.$facebookpost['id_post'].'/', 'DELETE');
				//dump($produkt['facebookpostid']);
			}
			
			if($post != null)
			{
				if(!empty($facebookpost))
				{
					$facebooksprods->ID = $facebookpost['id'];
					$facebooksprods->edytuj(array('id_post' => $post['id']));
				}
				else
				{
					$facebooksprod['id_strona'] = $page;
					$facebooksprod['id_produkt'] = $id;
					$facebooksprod['id_post'] = $post['id'];
					$facebooksprods->dodaj($facebooksprod);
				}
				//$produkty->edytujProdukty(array('facebookpostid' => $post['id']));
			}
		}
		catch(FacebookApiException $e)
		{
			$login_url = $facebook->getLoginUrl(array
			(
				'scope' => 'publish_stream, offline_access, manage_pages'
			));
			return 'Błąd, nie udało się dodać wpisu na Facebook. Wymagane jest zalogowanie się do serwisu: <a href="'.$login_url.'">Zaloguj się</a><br/>'.$e->getMessage();
		}
	}
	
	function usunProdukt($id = 0, $page = 1)
	{		
		$produkty = new Produkty();
		$produkty->id = $id;
		$produkt = $produkty->wypiszPojedyncza();
			
		$facebooksprods = new Facebooksprods();
		$facebookpost = $facebooksprods->wypiszPost($page, $id);
		if(!empty($facebookpost)) // if(@!empty($produkt['facebookpostid']))
		try
		{
			$data = $this->login($page);
			if(!empty($data['error'])) return $data['error'];
			$settings = $data['ustaw'];
			$facebook = $data['faces'];
		
			$facebook->api('/'.$facebookpost['id_post'].'/', 'DELETE');
			//$facebook->api('/'.$produkt['facebookpostid'].'/', 'DELETE');
			$facebooksprods->ID = $facebookpost['id'];
			$facebooksprods->usun();
			//$produkty->edytujProdukty(array('facebookpostid' => ''));
		}
		catch(FacebookApiException $e)
		{
			return $e->getMessage();
		}
	}
}
?>