<?php 
class Ileodwiedziny extends Zend_Db_Table
{
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
	function sprawdz($ip)
	{
		$where = 'czas < "'.(time()-36000).'"';
		$this->delete($where);
		
		$where = 'ip = "'.$ip.'"';
		$select = $this->select()->distinct()->from(array('i' => 'Ileodwiedziny'))->where($where);
		$row = $this->fetchAll($select)->toArray();
		
		if(!empty($row))
		{
			$dane = array('czas' => time());
			$where = 'ip = "'.$ip.'"';
			$this->update($dane, $where);		
		}
		else
		{
			$dane = array('czas' => time(), 'ip' => $ip);
			$this->insert($dane);
			
			$settings = new Ustawienia();
			$ustawienia = $settings->wypisz();
			$odwiedzin = floatval($ustawienia['odwiedzin']) + 1;
			$ustawienia->update(array('odwiedzin' => $odwiedzin, 'id = 1'));
		}
		$settings = new Ustawienia();
		$ustawienia = $settings->wypisz();		
		return @$ustawienia['odwiedzin'];		
	}
}	
?>