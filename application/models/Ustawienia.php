<?php
class Ustawienia extends Zend_Db_Table
{
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
	function updateData($array)
	{
		$where = 'id = 1';
		$this->update($array, $where);
	}
	
	function showData()
	{
		$result = $this->fetchAll();//time());
		return $result;
	}
	
	function wypisz()
	{
		$result = $this->select()->where('id = 1');
		return $this->fetchRow($result)->toArray();
	}
}
?>