<?php
class Atrybutypowiazania extends Zend_Db_Table
{
	public $link, $id, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

	function _save($data)
	{
		$this->insert($data);
		return $this->getAdapter()->lastInsertId();
	}

	function _update($data)
	{
		$this->update($data, 'id = '.$this->id);
	}
	
	function changePositionAjax($position = 0, $id_og = 0, $id_gr = 0)
	{
        $dane['pozycja'] = $position;
        $where = 'id_og = "'.$id_og.'" and id_gr = "'.$id_gr.'" and (id_at = 0 or typatrybut = "lista")';
        $this->update($dane, $where);
    }

	function _delete()
	{
		$this->delete('id_og = '.$this->id);
	}
	function _deleteGroup()
	{
		$this->delete('id_gr = '.$this->id);
	}
	function _deleteAttr()
	{
		$this->delete('id_at = '.$this->id);
	}
	function _deleteProdGroup($id_gr = 0)
	{
		$this->delete('id_og = '.$this->id.' and id_gr = '.$id_gr);
	}
	
	function usunProdukt($id)
	{
		$this->delete('id_og = '.$id);
	}
	function usunProduktyIds($ids)
	{
		$result = $this->delete('id_og in ('.$ids.')');
	}

	function getRow()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	
	function getRows()
	{
		$result = $this->fetchAll('lang="'.$this->lang.'"', 'nazwa ASC');
		return $result;
	}

	function getRowsUnique()
	{
		$result = $this->fetchAll('id_og = "'.$this->id.'" ');
		return $result;
	}
	
	function getRowsUniqueCeny()
	{
		$select = $this->db->select()
			->from(array('atp' => $this->_name), array('id_gr', 'ceny'))
			->joinleft(array('atg' => 'Atrybutygrupy'), 'atp.id_gr = atg.id', array('jedn_miary_typ'))
			->where('atp.id_og = "'.$this->id.'" and (atp.typatrybut != "multi" or atp.id_at = 0)')
			->order(array('atg.poz asc', 'atg.nazwa asc'));
		$result = $this->db->fetchAll($select);
		//echo $select;
		$result = $this->common->sortByPole($result, 'id_gr');
		return $result;
	}
	
	function wypiszJedenNazwa()
	{
		$where = 'atp.ceny and atp.cenyTryb';//(at.id_prod = '.$this->id.' or at.id_prod = 0)';
		$select = $this->db->select()->from(array('atp' => 'Atrybutypowiazania'), 
			array('id as rp.id', 'id_og', 'cena_netto', 'img'))//, 'dostepnosc', 'dostepny'))
			->join(array('at' => 'Atrybuty'), 'at.id = atp.id_at', array('nazwa', 'id_gr'))
			->join(array('atg' => 'Atrybutygrupy'), 'atg.id = at.id_gr', array('grupa' => 'nazwa'))
			->where('atp.id = '.$this->id.' and '.$where)->order(array('at.nazwa'));
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypisz($null = null)
	{
		$where = 'atp.ceny and atp.cenyTryb';//(at.id_prod = '.$this->id.' or at.id_prod = 0)';
		$select = $this->db->select()->from(array('atp' => 'Atrybutypowiazania'), 
			array('id as rp.id', 'id_og', 'cena_netto', 'img'))//, 'dostepnosc', 'dostepny'))
			->join(array('at' => 'Atrybuty'), 'at.id = atp.id_at', array('nazwa', 'id_gr'))
			->join(array('atg' => 'Atrybutygrupy'), 'atg.id = at.id_gr', array('grupa' => 'nazwa'))
			->where('atp.id_og = '.$this->id.' and '.$where)->order(array('at.nazwa'));
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	function wypiszAukcje($null = false)
	{
		$where = 'atp.ceny and atp.cenyTryb';//(at.id_prod = '.$this->id.' or at.id_prod = 0)';
		if($null)
		$where = 'a.wystawiono is not null and (DATE_ADD(a.wystawiono,INTERVAL a.na_ile DAY)-NOW()) > 0';
		$select = $this->db->select()->from(array('atp' => 'Atrybutypowiazania'), 
			array('id as rp.id', 'id_og', 'cena_netto', 'img'))//, 'dostepnosc', 'dostepny'))
			->join(array('at' => 'Atrybuty'), 'at.id = atp.id_at', array('nazwa', 'id_gr'))
			->join(array('atg' => 'Atrybutygrupy'), 'atg.id = at.id_gr', array('grupa' => 'nazwa'))
			->joinleft(array('a' => 'Allegroaukcje'), 'a.id_rozm = atp.id and a.id_prod = '.$this->id, 
					array('id as a.id', 'nr', 'cena', 'sztuk_pozostalo', 'na_ile', 'sztuk_wystawiono', 'wystawiono'))
			->where('atp.id_og = '.$this->id.' and '.$where)->order(array('at.nazwa'));
		//echo $select;
		$result = $this->db->fetchAll($select);
		return $result;
	}
	
	function getProdukty($atrybuty = null)
	{		
		$prods = array(0);
		$atrybutygrupy = new Atrybutygrupy();
		$atryb = $atrybutygrupy->getAtrybuty(array_keys($atrybuty));
		//if(false)
		foreach($atrybuty as $gr => $atr)
		{
			$where = '';
			if(!isset($atryb[$gr])) continue;
			
			if($atryb[$gr]['typatrybut'] == 'tekst')
			{
				if(empty($atr)) continue;
				if(is_array($atr))
				{
					$atrOd = @floatval(str_replace(',', '.', $atr['od']));
					$atrDo = @floatval(str_replace(',', '.', $atr['do']));
					if($atrOd > 0) $where .= ' and wartosc >= '.$atrOd.'';
					if($atrDo > 0) $where .= ' and wartosc <= '.$atrDo.'';
				}
				else
				{
					if(strpos($atr, "Szukaj") === false)
					if($atr != mb_strtolower($atryb[$gr]['nazwa'], 'UTF-8'))
					$where .= ' and wartosc like "%'.$atr.'%"';
				}
			}
			else if($atryb[$gr]['typatrybut'] == 'wybor')
			{
				if($atr > 0) $where .= ' and wartosc = '.$atr;
				if($atr <= 0) continue;
			}
			else if($atryb[$gr]['typatrybut'] == 'multi')
			{
				if(is_array($atr))
				{
					if(count($atr) > 0 && reset($atr) > 0)
					$where .= ' and id_at in ('.implode(",",$atr).')';
					else continue;
				}
				else
				{
					if($atr > 0) $where .= ' and id_at = '.$atr;
					if($atr <= 0) continue;
				}
			}
			else if($atryb[$gr]['typatrybut'] == 'lista')
			{
				if(is_array($atr) && count($atr) > 0 && reset($atr) > 0)
				$where .= ' and id_at in ('.implode(",",$atr).')';
				else if($atr > 0) $where .= ' and id_at = '.$atr;
				if($atr <= 0) continue;
			}

			if(empty($where)) continue;
			$where = 'id_gr = '.$gr.' '.$where;
			
			$select = $this->db->select()->distinct()
				->from(array('atp' => 'Atrybutypowiazania'), array('id_og'))
				->where($where)->order(array('id_og asc'));
			//echo $select.'<br>';
			$rows = null;
			$result = $this->db->fetchAll($select);
			if(@count($result) > 0)
			foreach($result as $row)
			{
				$prod = intval($row['id_og']);
				$prods[$prod] = $prod;
				$rows[$prod] = $row;
			}
			$results[] = $rows;
		}
		//die();

		if(!isset($result)) return null;
		//var_dump($prods);
		
		if(@count($results) > 0)
		foreach($results as $result)
		{
			//var_dump($result);
			if(@count($prods) > 0)
			foreach($prods as $prod)
			{
				if($prod > 0)
				if(!isset($result[$prod]))
				unset($prods[$prod]);
			}
		}
		else $prods = array(0);
		//var_dump($prods);die();
		return $prods;
	}
	
	function getAtrybutyPowiaz($pole = 'id_og', $ids = null)
	{
		if(!$this->common->isArray($ids, true)) return null;
		$sql = $this->select()->where($pole.' <> "" and id_og in ('.implode(',',$ids).')')->order($pole);
		$result = $this->fetchAll($sql);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i]['id_og']][$result[$i]['id_gr']][$result[$i]['id_at']] = $result[$i]->toArray();
		}
        unset($result);
		return @$results;
	}
    function getAtrybutyPowiazForExport($kat = 0)
	{
		$select = $this->db->select()->from(array('ap'=>$this->_name),array('id_og','id_gr','id_at','wartosc'));
		$select->join(array('p' => 'Produkty'), 'ap.id_og = p.id', array(''));
        $select->join(array('kp' => 'Katprod'), 'kp.id_prod = p.id', array(''));
		$select->where('kp.id_kat = '.$kat.' and typatrybut <> "multi"')->order('p.nazwa asc');
		$result = $this->db->fetchAll($select);
		for ($i = 0; $i < count($result); $i++)
		{
			$results[$result[$i]['id_og']][$result[$i]['id_gr']] = $result[$i];
		}
        unset($result);
		return @$results;
	}
}    
?>