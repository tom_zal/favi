<?php
class Przelewy24 extends Zend_Db_Table
{
	public $id = 1, $obConfig;

	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
		$this->lang = $this->common->getJezyk($module);
	}

	public function _get()
	{
		return $this->fetchRow('id = "'.$this->id.'"');
	}

	public function _update($dane)
	{
		$this->update($dane, 'id = "'.$this->id.'"');
	}

	function status($stat, $allreturn = false)
	{
		$array = array
		(
			'err00' => 'Nieprawidłowe wywołanie skryptu.',
			'err01'=> 'Nie uzyskano od sklepu potwierdzenia odebrania odpowiedzi autoryzacyjnej.',
			'err02'=> 'Nie uzyskano odpowiedzi autoryzacyjnej.',
			'err03' => 'To zapytanie było już przetwarzane.',
			'err04' => 'Zapytanie autoryzacyjne niekompletne lub niepoprawne.',
			'err05' => 'Nie udało się odczytać konfiguracji sklepu internetowego.',
			'err06' => 'Nieudany zapis zapytania autoryzacyjnego.',
			'err07' => 'Inna osoba dokonuje płatności.',
			'err08' => 'Nieustalony status połączenia ze sklepem.',
			'err09' => 'Przekroczono dozwoloną liczbę poprawek danych.',
			'err10' => 'Nieprawidłowa kwota transakcjii.',
			'err49' => 'Zbyt wysoki wynik oceny ryzyka transakcji.',
			'err51' => 'Nieprawidłowe wywołanie strony.',
			'err52' => 'Błędna informacja zwrotna o sesji.',
			'err53' => 'Błąd transakcji.',
			'err54' => 'Niezgodność kwoty transakcji.',
			'err55' => 'Nieprawidłowy kod odpowiedzi.',
			'err56' => 'Nieprawidłowa karta.',
			'err57' => 'Niezgodność flagi TEST.',
			'err58' => 'Nieprawidłowy numer sekwencji.',
			'err59' => 'Nieprawidłowa waluta transakcji.',
			'err101' => 'Błąd wywołania strony.',
			'err102' => 'Minął czas na dokonanie transakcji.',
			'err103' => 'Nieprawidłowa kwota przelewu.',
			'err104' => 'Transakcja oczekuje na potwierdzenie.',
			'err105' => 'Transakcja dokonana po dopuszczalnym czasie.',
			'err161' => 'Żądanie transakcji przerwane przez użytkownika. - Klient przerwał procedurę płatności wybierając przycisk "Powrót" na stronie wyboru formy płatności.',
			'err162' => 'Żądanie transakcji przerwane przez użytkownika. - Klient przerwał procedurę płatności wybierając przycisk "Rezygnuj" na stronie z instrukcją płatności.',
			'zakonczona' => 'zakończona',
			'bladserwis' => 'Nieoczekiwany błąd serwisu. Skontaktuj się z administratorem.'
		);
		if(!$allreturn) return @$array[str_replace(' ', '', $stat)];
		else return $array;
	}
	
	static function statusyszukaj()
	{
	   $ob = new Przelewy24();
	   $statusy = $ob->status(null, true);
	   $statusypowartosc = array();
	   foreach($statusy as $key=>$value) $statusypowartosc[] = $value;           
	   return $statusypowartosc;
	}
	
	function p24_weryfikuj($url, $p24_id_sprzedawcy, $p24_session_id, $p24_order_id, $p24_kwota, $p24_crc)
	{
		$P = array(); $RET = array(); $currency = 'PLN';
		$P[] = "p24_id_sprzedawcy=".$p24_id_sprzedawcy;
		$P[] = "p24_session_id=".$p24_session_id;
		$P[] = "p24_order_id=".$p24_order_id;
		$P[] = "p24_kwota=".$p24_kwota;
		$P[] = "p24_currency=".$currency;
		$P[] = "p24_sign=".md5($p24_session_id."|". $p24_order_id."|".$p24_kwota."|".$currency."|".$p24_crc);
		$user_agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		if(count($P)) curl_setopt($ch, CURLOPT_POSTFIELDS,join("&",$P));		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$result = curl_exec($ch);
		curl_close ($ch);
		
		$T = explode(chr(13).chr(10), $result);
		$res = false;
		
		foreach($T as $line)
		{
			$line = @ereg_replace("[\n\r]","",$line);
			if($line != "RESULT" and !$res) continue;
			if($res) $RET[] = $line;
			else $res = true;
		}
		return $RET;
	} 
}
?>