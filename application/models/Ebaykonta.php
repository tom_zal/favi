<?php
class Ebaykonta extends Zend_Db_Table
{
    public $ID, $login, $choose;
    protected $_name = 'ebay_konta';
	public $link, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
	
    function dodaj($dane)
	{
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }

    function edytuj($dane)
	{
        $where = 'id = "'.$this->ID.'"';
        $this->update($dane, $where);
    }

    function usun()
	{
        $where = 'id = "'.$this->ID.'"';
        $this->delete($where);
    }

    function klient()
	{
        $where = 'id = "'.$this->ID.'"';
        $result = $this->fetchRow($where);
        $res = $result->toArray();
        return $res;
    }

    function listaKlient()
	{
        $result = $this->fetchAll();
        $res = $result->toArray();
        return $res;
    }

    function checkLogin()
	{
        if($this->choose == 0)
		{
            $where = 'login = "'.$this->login.'"';
        }
		else
            $where = 'login = "'.$this->login.'" AND id <> "'.$this->ID.'"';

        $result = $this->fetchAll($where);
        $res = $result->toArray();
        return $res;
    }
}
?>