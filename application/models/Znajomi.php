<?php
class Znajomi extends Zend_Db_Table
{
	public $link, $id;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }
		
	function dodajZnajomego($dane)
	{
		$this->insert($dane);
		$id = $this->getAdapter()->lastInsertId();
		return $id;
	}
	
	function edytujZnajomego($dane)
	{
		$where = 'id = '.$this->id;
		$this->update($dane, $where);
	}
	
	function wypiszZnajomych()
	{
		$sql = $this->select()->order('mail');
		$result = $this->fetchAll($sql);
		return $result;
	}
	
	function wypiszZnajomychKlienta($kontr)
	{			
		$where = 'id_kontr = '.$kontr;		 
		$result = $this->fetchAll($where);
		return $result;	
	}
	
	function wyszukajZnajomychKlienta($kontr, $mail)
	{			
		$where = 'id_kontr = '.$kontr.' and mail like "%'.$mail.'%"';	
		$result = $this->fetchAll($where);
		return $result;	
	}
	
	function usunZnajomego()
	{
		$result = $this->delete('id = '.$this->id);			
	}
	
	function wypiszZnajomego()
	{
		$result = $this->fetchRow('id = '.$this->id);
		return $result;
	}
	
	function szukajZnajomego($mail)
	{
		$result = $this->fetchAll('mail = "'.$mail.'"');
		return $result;
	}
}
?>