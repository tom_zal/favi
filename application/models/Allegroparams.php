<?php
class Allegroparams extends Zend_Db_Table
{
    public $id, $login, $choose;
    protected $_name = 'Allegroparams';
	public $link, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

    function dodaj($dane)
	{
        $this->insert($dane);
        $id = $this->getAdapter()->lastInsertId();
        return $id;
    }

    function edytuj($dane)
	{
        $where = 'id = "'.$this->id.'"';
        $this->update($dane, $where);
    }

    function usun()
	{
        $where = 'id = "'.$this->id.'"';
        $this->delete($where);
    }
	
	function getOneByID()
	{
        $where = 'id = "'.$this->id.'"';
        $result = $this->fetchRow($where);
        return $result->toArray();
    }
	
	function getAll()
	{
		$sql = $this->select()->order('id_kat');
        $result = $this->fetchAll($sql);
        $res = $result->toArray();
        return $res;
    }
	
	function getAllForKat($kat)
	{
		$where = 'id_kat = '.$kat.'';
		$sql = $this->select()->where($where)->order('id_sell_form');
        $result = $this->fetchAll($sql);
		//echo $sql;
        return $result;
    }
	
	function getAllForKats($kat, $form)
	{
		$where = 'id_kat = '.$kat.' and id_sell_form = '.$form.'';
		$sql = $this->select()->where($where)->order('id_param');
        $result = $this->fetchAll($sql);
		//echo $sql;
        return $result;
    }
	
	function getOne($kat, $form, $param)
	{
		$where = 'id_kat = '.$kat.' and id_sell_form = '.$form.' and id_param = "'.$param.'"';
        $result = $this->fetchRow($where);
        return $result;
    }
	
	function getParamsKolory($id, $kat, $form)
	{
		$where = 'kp.id_produktu = '.$id.' and id_kat = '.$kat.' and id_sell_form = '.$form.'';
		$sql = $this->select()
			->from(array('a' => 'Allegroparams'), 'sum(distinct value) as suma')
			->join(array('k' => 'Kolory'), 'k.id = a.id_param', array(''))
			->join(array('kp' => 'Kolorproduktu'), 'k.id = kp.id_koloru', array(''))
			->where($where);
		//echo $sql;
		$result = $this->fetchRow($sql);
		return $result;
    }
	function getParamsRozmiary($id, $kat, $form, $nazwa)
	{
		$where = 'rp.id_produktu='.$id.' and id_kat='.$kat.' and id_sell_form='.$form.' and r.nazwa="'.$nazwa.'"';
		$sql = $this->select()
			->from(array('a' => 'Allegroparams'), 'value')
			->join(array('r' => 'Rozmiary'), 'r.nazwa = a.id_param', array(''))
			->join(array('rp' => 'Rozmiarproduktu'), 'r.id = rp.id_koloru', array(''))
			->where($where);
		//echo $sql;
		$result = $this->fetchAll($sql);
		return $result;
    }
	function getParamsWkladki($id, $kat, $form, $nazwa)
	{
		$where = 'rp.id_produktu='.$id.' and id_kat='.$kat.' and id_sell_form='.$form.' and r.wkladka="'.$nazwa.'"';
		$sql = $this->select()
			->from(array('a' => 'Allegroparams'), 'value')
			->join(array('r' => 'Rozmiary'), 'r.wkladka = a.id_param', array(''))
			->join(array('rp' => 'Rozmiarproduktu'), 'r.id = rp.id_koloru', array(''))
			->where($where);
		//echo $sql;
		$result = $this->fetchAll($sql);
		return $result;
    }
}
?>