<?phpclass Platnosci {
    public $tabelaKontrahenci, $tabelaProdukty;
	public $link, $id, $obConfig, $db;		public function __construct($module = 'admin')	{		$this->common = new Common(false, $module);		$this->obConfig = $this->common->getObConfig();        $this->db = $this->common->getDB($this->obConfig);		$this->lang = $this->common->getJezyk($module);
		$this->dostawa = 'Sposobdostawy';        $this->platnosc = 'Sposobplatnosci';        $this->wartosc = 'Platnosci';		$this->online = 'Online';        $this->zamowieniaKontahent = 'Zamowieniakontrahenci';    }		public function zmienStatusZamowienia($id, $dane, $typ)    {        $where2 = 'id = '.$id;        $array2= array('online_typ' => $dane['message'], 'online' => $typ);        $this->db->update($this->zamowieniaKontahent, $array2, $where2);    }    public function zmienOnline($dane) 	{        $where = 'id = 1';        $this->db->update($this->online, $dane, $where);    }    public function wypiszOnline() 	{        $select = $this->db->select()->from($this->online);        $result = $this->db->fetchAll($select);        return $result[0];    }	
    public function deleteWybrany($id) 	{
        $where = 'id = ' . $id;
        $this->db->delete($this->wartosc, $where);
    }			public function deleteWybranyPlatnoscDostawa($platnosc, $dostawa)	{        $where = 'platnosc = ' . $platnosc.' and dostawa = '.$dostawa;        $this->db->delete($this->wartosc, $where);    }
    public function deleteDostawy($id)	{
        $where = 'id = ' . $id;
        $this->db->delete($this->dostawa, $where);
        $where = 'dostawa = ' . $id;
        $this->db->delete($this->wartosc, $where);
    }
    public function deletePlatnosc($id)	{
        $where = 'id = ' . $id;
        $this->db->delete($this->platnosc, $where);
        $where = 'platnosc = ' . $id;
        $this->db->delete($this->wartosc, $where);
    }
    public function addSposobdostawy($dane)	{		$dane['lang'] = $this->lang;
        $this->db->insert($this->dostawa, $dane);		$id = $this->db->lastInsertId($this->dostawa);		return $id;
    }		public function changeSposobdostawy($dane)	{		$where = 'id = '.$this->id;        $this->db->update($this->dostawa, $dane, $where);    }
    public function addSposobplatnosci($dane)	{		$dane['lang'] = $this->lang;
        $this->db->insert($this->platnosc, $dane);		$id = $this->db->lastInsertId($this->platnosc);		return $id;
    }		public function changeSposobplatnosci($dane)	{		$where = 'id = '.$this->id;        $this->db->update($this->platnosc, $dane, $where);    }
    public function addWartosc($dane)	{
        $this->db->insert($this->wartosc, $dane);		$id = $this->db->lastInsertId($this->wartosc);		return $id;
    }		public function changeWartosc($dane)	{		$where = 'id = '.$this->id;        $this->db->update($this->wartosc, $dane, $where);    }
    public function selectdostawy($id)	{
        $where = 'id = ' . $id;
        $select = $this->db->select()->from($this->dostawa)->where($where);
        $result = $this->db->fetchAll($select);
        return $result[0];
    }
    public function selectdostawyAll()	{
        $select = $this->db->select()->from($this->dostawa)->where('lang = "'.$this->lang.'"')->order('nazwa');
        $result = $this->db->fetchAll($select);
        return $result;
    }
    public function selectplatnosci($id)	{
        $where = 'id = ' . $id;
        $select = $this->db->select()->from($this->platnosc)->where($where);
        $result = $this->db->fetchAll($select);
        return $result[0];
    }
    public function selectplatnosciAll()	{
        $select = $this->db->select()->from($this->platnosc)->where('lang = "'.$this->lang.'"')->order('nazwa');
        $result = $this->db->fetchAll($select);
        return $result;
    }		public function selectplatnosciAllForDostawa($dostawa = 0)	{        $select = $this->db->select()->from(array('sp' => $this->platnosc))			->joinleft(array('p' => 'Platnosci'), 'sp.id = p.platnosc and p.dostawa = '.$dostawa, 			array('id as nr', 'koszt'))->where('lang = "'.$this->lang.'"')->order(array('sp.nazwa asc'));		//echo $select;        $result = $this->db->fetchAll($select);        return $result;    }
    public function selectwartosci()	{
        $select = $this->db->select()->from($this->wartosc);
        $result = $this->db->fetchAll($select);
        return $result;
    }		public function selectwartosciOrder()	{        $select = $this->db->select()->from(array('p' => $this->wartosc))			->join(array('sd' => 'Sposobdostawy'), 'sd.id = p.dostawa', array(''))			->join(array('sp' => 'Sposobplatnosci'), 'sp.id = p.platnosc', array(''))			->where('sd.lang = "'.$this->lang.'" and sp.lang = "'.$this->lang.'"')			->order(array('sd.nazwa asc', 'sp.nazwa asc'));		//echo $select;        $result = $this->db->fetchAll($select);        return $result;    }
    public function WypiszDostawyDlaPlatnosci($platnosc)	{
        $where = 'platnosc = ' . $platnosc;
        $select = $this->db->select()->from($this->wartosc)->where($where);
        $result = $this->db->fetchAll($select);
        for($i=0; $i<count($result); $i++)		{
            $where = 'id = ' . $result[$i]['dostawa'];
            $select = $this->db->select()->from($this->dostawa)->where($where);
            $sposobydostawy = $this->db->fetchAll($select);
            $dostawy[] = $sposobydostawy[0];
        }
        return isset($dostawy) ? $dostawy : null;
    }		public function WypiszPlatnosciDlaDostawy($dostawa) 	{        $where = 'dostawa = ' . $dostawa;        $select = $this->db->select()->from($this->wartosc)->where($where);        $result = $this->db->fetchAll($select);        for($i=0; $i<count($result); $i++) 		{            $where = 'id = ' . $result[$i]['platnosc'];            $select = $this->db->select()->from($this->platnosc)->where($where);			//echo $select;            $sposobyplatnosci = $this->db->fetchAll($select);			//var_dump($sposobyplatnosci);			if(count($sposobyplatnosci) > 0)			{				$platnosci[$result[$i]['id']] = $sposobyplatnosci[0];				$platnosci[$result[$i]['id']]['koszt'] = $result[$i]['koszt'];			}        }		//var_dump($platnosci);die();        return isset($platnosci) ? $platnosci : null;    }
    public function WypiszWartoscDostawyPlatnosc($dostawa, $platnosc)	{
        $where = 'platnosc = ' . $platnosc.' AND dostawa = '.$dostawa;
        $select = $this->db->select()->from($this->wartosc)->where($where);
        $result = $this->db->fetchAll($select);
        return isset($result[0]) ? $result[0] : null;
    }		public function WypiszWartoscDostawy($dostawa)	{        $where = 'id = '.$dostawa;        $select = $this->db->select()->from($this->dostawa)->where($where);        $result = $this->db->fetchAll($select);        return isset($result[0]) ? $result[0] : null;    }
    public function platnosc($id, $platnoscID, $dostawaID)	{
        //$where = 'id = ' . $id;		$where = 'platnosc = ' . $platnoscID.' and dostawa = '.$dostawaID;
        $select = $this->db->select()->from($this->wartosc)->where($where);
        $result = $this->db->fetchAll($select);
        $nazwaplatnosci = $this->selectplatnosci(@intval($result[0]['platnosc']));
        $result[0]['platnosc'] = @$nazwaplatnosci['nazwa'];
        $nazwadostawy = $this->selectdostawy(@intval($result[0]['dostawa']));
        $result[0]['dostawa'] = @$nazwadostawy['nazwa'];		$result[0]['wartosc'] = @$nazwadostawy['wartosc'];		$result[0]['apikurier'] = @$nazwadostawy['apikurier'];
        return $result[0];
    }		public function rowAdmin()	{        $where = 'id = "'.$this->id.'"';        $select = $this->db->select()->from($this->wartosc)->where($where);        $stmt = $select->query();        $result = $stmt->fetchAll();        return $result[0];    }    public function updateAdmin($dane)	{        $where = 'id ="'.$this->id.'"';        $this->db->update($this->wartosc, $dane, $where);    }
    public function wypiszWszystkie()	{
        $lista = $this->selectwartosciOrder();
        for ($i = 0; $i < count($lista); $i++)		{
            $nazwaplatnosci = $this->selectplatnosci($lista[$i]['platnosc']);
            $lista[$i]['platnosc'] = $nazwaplatnosci['nazwa'];			$lista[$i]['id_platnosc'] = $nazwaplatnosci['id'];			//$lista[$i]['platnosc_opis'] = $nazwaplatnosci['opis'];
            $nazwadostawy = $this->selectdostawy($lista[$i]['dostawa']);
            $lista[$i]['dostawa'] = $nazwadostawy['nazwa'];			$lista[$i]['id_dostawa'] = $nazwadostawy['id'];			$lista[$i]['dostawa_opis'] = $nazwadostawy['opis'];			$lista[$i]['apikurier'] = $nazwadostawy['apikurier'];						$platnosci[$lista[$i]['id']] = $lista[$i];
        }
        return @$platnosci;
    }
}
?>