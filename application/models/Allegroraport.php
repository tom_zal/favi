<?php
class Allegroraport extends Zend_Db_Table 
{
    public $ID;
	public $link, $obConfig, $db;
	
	public function __construct($module = 'admin')
	{
		parent::__construct();
		$this->common = new Common(false, $module);
		$this->obConfig = $this->common->getObConfig();
        $this->db = $this->common->getDB($this->obConfig);
		$this->lang = $this->common->getJezyk($module);
    }

    function dodaj($dane) 
	{
		if($this->obConfig->allegroRaporty)
		{
			$this->insert($dane);
			$id = $this->getAdapter()->lastInsertId();
			return $id;
		}
		else return 0;
    }

    function edytuj($dane) 
	{
        $where = 'id = "'.$this->ID.'"';
        $this->update($dane, $where);
    }

    function usun() 
	{
        $where = 'id = "'.$this->ID.'"';
        $this->delete($where);
    }
	
	function usunAll() 
	{
        $where = '1';
        $this->delete($where);
    }
	
	function wypisz($niewystawione = true, $od = 0, $limit = 24, $ile = false, $bez_etapow = true)
	{
		$where = '1';
		if($bez_etapow) $where .= ' and id_rozm >= 0';
		if($niewystawione) $where .= ' and status <> "wystawiono"';
		$sql = $this->select()->where($where)->order('data')->limit($ile ? 99999 : $limit, $ile ? 0 : $od * $limit);
		$result = $this->fetchAll($sql);
		return count($result) > 0 ? ($ile ? count($result) : $result->toArray()) : null;
	}
	function wypiszJeden()
	{
		$sql = $this->select()->where('id = '.$this->ID);
		$result = $this->fetchRow($sql);
		return $result;
	}
	function szukaj($prod = 0, $rozm = 0)
	{
		$sql = $this->select()->where('id_prod = '.$prod.' and id_rozm = '.$rozm);
		$result = $this->fetchAll($sql);
		return $result;
	}
	function wypiszNajnowsze()
	{
		$sql = $this->select()->where('1')->order('data desc')->limit(1);
		$result = $this->fetchRow($sql);
		return $result;
	}
}
?>