<?php
class Ajax_IndexController extends Ogolny_Controller_Ajax
{
	private $tryb;
	
    public function init() 
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
		$this->view->obConfig = $this->obConfig = new Zend_Config_Ini('../application/config.ini', 'general');
		$this->tryb = $this->obConfig->tryb;
		$this->trybCena = $this->obConfig->trybCena;
		$this->allegroTryb = $this->obConfig->allegroTryb;
		$this->allegroTrybCena = $this->obConfig->allegroTrybCena;
		//echo 'x';die();
    }
	
    function indexAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		//$SmsClientSoap = new SmsClientSoap();
		//$SmsClientSoap->kartaQR("www.panel.pl");
    }
	
	public function tooltipzamowienieAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$id_zam = $this->_request->getParam('id');
		$tryb = $this->_request->getParam('tryb');
		$typ = $this->_request->getParam('typ');
		$xml = '';
		if(!empty($id_zam))
		{
			if($typ == 'allegro')
			{
				$oAllegroZamowienia = new AllegroZamowienia();
				if($tryb == 'archiwum') $oAllegroZamowienia->findAllProduktArchiwumById($id_zam);
				else $AllegroZamProdukty = $oAllegroZamowienia->findAllProduktById($id_zam);
				$xml = '<root>';
				if(!empty($AllegroZamProdukty))
				{
					foreach($AllegroZamProdukty as $p)
					{
						$xml .= '<produkt>';
						if(!empty($p['id_prod'])) $xml .= '<id>'.$p['id_prod'].'</id>'; else $xml .= '<id></id>';
						if(!empty($p['foto'])) $xml .= '<zdjecie>'.$p['foto'].'</zdjecie>'; 
						else $xml .= '<zdjecie>'.$this->baseUrl.'/public/admin/zdjecia/brak.jpg</zdjecie>';
						if(!empty($p['nazwa'])) $xml .= '<nazwa>'.str_replace('&', '&#38;', $p['nazwa']).'</nazwa>'; else $xml .= '<nazwa></nazwa>';
						if(!empty($p['ilosc'])) $xml .= '<ilosc>'.$p['ilosc'].'</ilosc>'; else $xml .= '<ilosc></ilosc>';
						if(!empty($p['cena_brutto'])) $xml .= '<cena_brutto>'.$p['cena_brutto'].'</cena_brutto>'; else $xml .= '<cena_brutto></cena_brutto>';
						if(!empty($p['wartosc'])) $xml .= '<wartosc>'.$p['wartosc'].'</wartosc>'; else $xml .= '<wartosc></wartosc>';
						$xml .= '</produkt>';
					}
				}
				else
				{
					$xml .='<produkt><id>0</id><zdjecie></zdjecie><nazwa>Brak produktów</nazwa><ilosc></ilosc><cena_brutto></cena_brutto><wartosc></wartosc></produkt>';
				}
				$xml .= '</root>';
				echo $xml;
			}
			else
			{
				$oZamowienia = $tryb == 'archiwum' ? new Archiwum() : new Zamowienia();
				$ZamProdukty = $oZamowienia->selectWybranyProduktAll($id_zam);
				$xml = '<root>';
				if(!empty($ZamProdukty))
				{
					foreach($ZamProdukty as $p)
					{
						$xml .= '<produkt>';
						if(!empty($p['id_prod'])) $xml .= '<id>'.$p['oznaczenie'].'</id>'; else $xml .= '<id></id>';
						if(!empty($p['img'])) $xml .= '<zdjecie>'.$this->baseUrl.'/public/admin/zdjecia/'.$p['img'].'</zdjecie>'; 
						else $xml .= '<zdjecie>'.$this->baseUrl.'/public/admin/zdjecia/brak.jpg</zdjecie>';
						if(!empty($p['nazwa'])) $xml .= '<nazwa>'.str_replace('&', '&#38;', $p['nazwa']).'</nazwa>'; else $xml .= '<nazwa></nazwa>';
						if(!empty($p['ilosc'])) $xml .= '<ilosc>'.$p['ilosc'].'</ilosc>'; else $xml .= '<ilosc></ilosc>';
						if(!empty($p['cena_brutto'])) $xml .= '<cena_brutto>'.$p['cena_brutto'].'</cena_brutto>'; else $xml .= '<cena_brutto></cena_brutto>';
						if(!empty($p['wartosc'])) $xml .= '<wartosc>'.$p['wartosc'].'</wartosc>'; else $xml .= '<wartosc></wartosc>';
						$xml .= '</produkt>';
					}
				}
				else
				{
					$xml .='<produkt><id>0</id><zdjecie></zdjecie><nazwa>Brak produktów</nazwa><ilosc></ilosc><cena_brutto></cena_brutto><wartosc></wartosc></produkt>';
				}
				$xml .= '</root>';
				echo $xml;
			}
		}
	}
	
	function inneAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		
		$typ = strval($this->_request->getParam('typ', ''));
		$id_prod = intval($this->_request->getParam('id_prod', 0));
		if(!empty($typ) && $id_prod > 0)
		{
			$odwiedziny = new Odwiedziny('default');
			$data['id_prod'] = $id_prod;
			$data['typ'] = $typ;
			$odw = $odwiedziny->dodajOdwiedziny($data);
		}
		$ileonline = new Ileonline('default');
		$this->view->liczbauser = $ileonline->sprawdz($_SERVER['REMOTE_ADDR'], $_SERVER['PHP_SELF']);
		echo $this->view->liczbaodwiedzin = $this->ustawienia['odwiedzin'];
	}
	function inneadminAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		$url = strval($this->_request->getParam('url', ''));
		$url = str_replace("_", "/", $url);
		
		$logowania = new KontrahenciLogin();
		$logowania->odswiez($this->adminID, 'login');
		if(!empty($url) && strpos($url, "wyloguj") === false)
		{
			$akcja = new Login();
			$akcja->id = $this->adminID;
			$akcja->edytuj(array('last_link' => $url));
		}		
		
		$ileonline = new Ileonline('admin');
		echo $this->view->liczbauser = $ileonline->sprawdz($_SERVER['REMOTE_ADDR'], $_SERVER['PHP_SELF']);
		//$this->view->liczbaodwiedzin = $this->ustawienia['odwiedzin'];
	}
	
	function marketingAction() 
	{
		$monthsLengths = array(1=>31, 2=>28, 3=>31, 4=>30, 5=>31, 6=>30, 7=>31, 8=>31, 9=>30, 10=>31, 11=>30, 12=>31);
		$this->_helper->viewRenderer->setNoRender();
		
		$now = date("Y-m-d");
		if(@!empty($_GET['now']))
		$now = $_GET['now'];
		$nowTime = strtotime($now);
		$dt1 = new DateTime($now);
		$day1 = intval(date("d", $nowTime));
		$mth1 = intval(date("m", $nowTime));
		$year1 = intval(date("Y", $nowTime));
		$przest1 = intval(date("L", $nowTime));
		
		$ob = new Query('Marketing');
        $ob->where = 'aktywna and czas and czasjednostka > 0 and start <= "'.$now.'" and koniec >= "'.$now.'" and '.date('H').' >= czasjednostkad';
		echo $ob->where.'<br>';
        $ob->order = 'start';
        $ob->page = 0;
        $ob->rowcount = 99999;		
        $rows = $ob->getRowsField();
		//var_dump($rows);
		
		$sub = new Subskrypcja();
		$this->czasTypy = $sub->czasTypy;
		$this->typyAkcjiRozne = $sub->typyAkcjiRozne;
		$this->typyAkcjiOznacz = $sub->typyAkcjiOznacz;
		
		if(count($rows) > 0)
		{
			$conf = new Confmail();
			$data = $conf->showData();
			$daneMail['email'] = $data[0]['From'];
			$daneMail['nazwa'] = $this->view->tytulosoby;
			$wiad = new Wiadomosci();
			
            $conf = new Confsms();
            $data = $conf->wyswietl();
            $daneSMS['nadawca'] = $data['pole_nadawca'];
            $daneSMS['typ'] = $data['typ_wiadomosci'];
            $soap = new SmsClientSoap();
            $smskonto = $soap->wartoscKonta();
        }
		
		if(count($rows) > 0)
		foreach($rows as $row)
		{			
			$ob->id = $row['id'];
			$start = $row['start'];
			if(@!empty($_GET['start']))
			$start = $_GET['start'];
			$koniec = $row['koniec'];
			$startTime = strtotime($start);
			$dt2 = new DateTime($start);
			$diffSec = $nowTime - $startTime;
			$diffHour = $diffSec / (60 * 60);
			$diff = $dt1->diff($dt2);
			$day2 = intval(date("d", $startTime));
			$mth2 = intval(date("m", $startTime));
			$year2 = intval(date("Y", $startTime));
			$przest2 = intval(date("L", $startTime));
			$diffDay = $diffHour / 24;
			$diffMth = $diff->y * 12 + $diff->m;
			$diffYear = $year1 - $year2;			
			$dayFix = false;
			if($day2 >= 29 && $mth1 == 2 && $day1 == 28 && !$przest1) $dayFix = true;
			if($day2 >= 30 && $mth1 == 2 && $day1 == 29 && $przest1) $dayFix = true;
			if($day2 == 31 && $day1 == 30 && $day1 == $monthsLengths[$mth1]) $dayFix = true;
			$sameDay = ($day1 == $day2) || $dayFix;
			$sameMth = $mth1 == $mth2;
			$sameYear = $year1 == $year2;
			if($dayFix) $diffMth++;
			var_dump($diff);
			$czastyp = @intval($row['czastyp']);
			if(@strlen($_GET['czastyp']) == 1) $czastyp = intval($_GET['czastyp']);			
			$coIle = @intval($row['czasjednostka']);
			if(@intval($_GET['czasjedn']) > 0) $coIle = intval($_GET['czasjedn']);
			$godz = @intval($row['godz']);
			if(@strlen($_GET['godz']) >= 1) $godz = intval($_GET['godz']);
			echo $now.': '.$start.' - '.$koniec.' co '.$coIle.' '.@$this->czasTypy[$czastyp].' o godz. '.$godz.'<br/>';
			$send = $start == $now;
			if(!$send)
			switch($czastyp)
			{
				case 3: $send = $sameDay && $sameMth && ($diffYear % $coIle) == 0; break;
				case 2: $send = $sameDay && $diffMth > 0 && ($diffMth % $coIle) == 0; break;
				case 1: $send = ($diffDay % ($coIle * 1)) == 0; break;
				case 0: 
				default:$send = ($diffDay % ($coIle * 7)) == 0; break;
			}
			var_dump($send);
			if(true && $send)
			{
				$klienci = @unserialize($row['klient']);
				$osoby = @unserialize($row['osoby']);
				if($row['klient_all'])
				{
					$kontrahenci = new Kontrahenci();
					$klienci = $kontrahenci->wypiszKlienciDoNewslettera($row['typ'], !empty($row['tryb']) ? "1" : "");
					var_dump($klienci);die();
				}
				if($row['osoba_all'])
				{
					$osoby = $sub->wypiszOsobyDoNewslettera($row['typ'], !empty($row['tryb']) ? "1" : "1");
					var_dump($osoby);die();
				}
				if($row['typ'] == 1)
                {
                    $err = '';
					$error = '';
                    $wyslanych = $row['suma'];
                    $stankonta = $row['wartosc'];
                    $powtorzen = $row['powtorzen'];
                    if(@count($klienci) > 0 || @count($osoby) > 0)
                    {
						if(@!empty($row['tryb']))
						$row['tekst'] = $sub->getTresc($row['tryb'], $row['rodzaj']);
                        if(empty($row['tekst']))
						{
                            $error = '<div>Brak treści</div>';
                        }
						if(@empty($error))
						{
							$k = 0;
							if(is_array($klienci) && count($klienci) > 0)
							foreach($klienci as $key => $value)
							{
								$blad = $soap->send(strip_tags($row['tekst']), $value, $daneSMS['typ'], $daneSMS['nadawca']);
								if(!empty($blad))//isset($blad['error']) && !empty($blad['error']))
								{
									$err .= ++$k.'. Telefon: <strong>'.$value.'</strong> - '.$blad.'<br />';
								}
								else
								{
									$wyslanych++;
									$stankonta += $blad['points'];
								}
							}
							if(is_array($osoby) && count($osoby) > 0)
							foreach($osoby as $key => $value)
							{
								$blad = $soap->send(strip_tags($row['tekst']), $value, $daneSMS['typ'], $daneSMS['nadawca']);
								if(!empty($blad))//isset($blad['error']) && !empty($blad['error']))
								{
									$err .= ++$k.'. Telefon: <strong>'.$value.'</strong> - '.$blad.'<br />';
								}
								else
								{
									$wyslanych++;
									$stankonta += $blad['points'];
								}
							}
							if(!empty($err))
							{
								$error = '<div>Wystąpiły problemy z wysłaniem następujących sms:</div>';
								$error .= '<div>'.$err.'</div>';
								echo $error.'<br>';
							}
							//var_dump($blad);//die();
							$ob->_update(array('datarozpoczecia'=> date('Y-m-d'), 'powtorzen'=>$powtorzen+1, 'suma'=>$wyslanych, 'wartosc'=>$stankonta));
						}
						else echo $error.'<br>';
                    }
                }
                /*END SMS*/
                /*E-MAIL*/
                if($row['typ'] == 0)
                {
                    $err = '';
					$error = '';
                    $wyslanych = $row['suma'];
                    $powtorzen = $row['powtorzen'];
                    if(@count($klienci) > 0 || @count($osoby) > 0)
                    {
                        $k = 0;
                        $daneMail['temat'] = $row['nazwa'];
                        $daneMail['opis'] = stripslashes($row['tekst']);
                        if(@!empty($row['tryb']))
						$daneMail['opis'] = $sub->getTresc($row['tryb'], $row['rodzaj']);
                        if(empty($daneMail['temat']) || empty($daneMail['opis']))
						{
                            $error = '<div>Brak tematu lub treści</div>';
                        }
						if(@empty($error))
						{
							if(is_array($klienci) && count($klienci) > 0)
							foreach($klienci as $key=>$value)
							{
								$daneMail['mail'] = $value;
								$blad = $wiad->mailKlient($daneMail, false);
								if(isset($blad['error']) && !empty($blad['error']))
								{
									$err .= ++$k.'. E-mail: <strong>'.$value.'</strong> - '.$blad.'<br />';
								}
								else
								{
									$wyslanych++;
								}
							}
							if(is_array($osoby) && count($osoby) > 0)
							foreach($osoby as $key=>$value)
							{
								$daneMail['mail'] = $value;
								$blad = $wiad->mailKlient($daneMail, false);
								if(isset($blad['error']) && !empty($blad['error']))
								{
									$err .= ++$k.'. E-mail: <strong>'.$value.'</strong> - '.$blad.'<br />';
								}
								else
								{
									$wyslanych++;
								}
							}
							if(!empty($err))
							{
								$error = '<div>Wystąpiły problemy z wysłaniem następujących wiadomości:</div>';
								$error .= '<div>'.$err.'</div>';
								echo $error.'<br>';
							}
							$ob->_update(array('datarozpoczecia'=> date('Y-m-d'), 'powtorzen'=>$powtorzen+1, 'suma'=>$wyslanych));
						}
						else echo $error.'<br>';
                    }
                }
				break;
			}
		}
	}
	
	function cennikiAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$pdf = strval($this->_request->getParam('pdf', 'pdf'));
		$stron = intval($this->_request->getParam('stron', 1));
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		$zendPDF = new Zend_Pdf();
		//$extractor = new Zend_Pdf_Resource_Extractor();		
		for($strona = 1; $strona <= $stron; $strona++)
		try
		{
			$name = str_replace("_".$stron.".pdf", "_".$strona.".pdf", $pdf);
			$file = Zend_Pdf::load("admin/cennik/".$name);
			$template = clone $file->pages[0];
			$pageNew = new Zend_Pdf_Page($template);
			//$pageNew = $extractor->clonePage($file->pages[0]);
			$zendPDF->pages[] = $pageNew;
			unlink("admin/cennik/".$name);
		}
		catch(Exception $e)
		{
			echo '<error'.$strona.'>'.$e->getMessage().'</error'.$strona.'>';
			continue;
		}
		if(@count($zendPDF->pages) > 0)
		try
		{
			$pdf = str_replace("_".$stron.".pdf", ".pdf", $pdf);
			$zendPDF->save("admin/cennik/".$pdf);
			$href = '<a href="'.$this->baseUrl.'/public/admin/cennik/'.$pdf.'" target="_blank">'.$this->lang('pobierz cennik').'</a>';
			echo '<pdf>'.$pdf.'</pdf>';
		}
		catch(Exception $e)
		{
			echo '<error>'.$e->getMessage().'</error>';
		}
		else echo '<error></error>';
		echo '</root>';
	}
	
	function cennikAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		
		$this->common = new Common(true, 'default');
		$info = new Zend_Session_Namespace('jezyk');
		if(!isset($info->lang)) $info->lang = 'pl';
		$this->view->lang = $this->lang = $lang = $info->lang;
		
		$ile = intval($this->_request->getParam('ile', 40));
		$strona = intval($this->_request->getParam('strona', 0));
		$ile0 = max(1, $ile - 10);
		$ilu = $strona > 0 ? $ile : $ile0;
		$od = $strona > 0 ? $ile0 : 0;
		$page = $strona > 0 ? $strona - 1 : 0;
		$kateg = intval($this->_request->getParam('kategoria', 0));
		$grupa = strval($this->_request->getParam('grupa', 'all'));
		
		$szukanie = (object)array();
		$szukanie->typ_klienta = $this->view->typ_klienta;
		$szukanie->punkty = false;
		$szukanie->nazwa = '';
		$szukanie->oznaczenie = '';
		$szukanie->grupa = $grupa;
		$szukanie->rozmiar = 'all';
		$szukanie->wkladka = 'all';
		$szukanie->kolor = -1;
		$szukanie->cena_od = 0;
		$szukanie->cena_do = 0;
		$szukanie->cena_netto = 0;
		$szukanie->cena_brutto = 0;
		$szukanie->producent = -1;
		$szukanie->producenci = null;
		$szukanie->atrybuty = null;
		
		if($kateg > 0)
		{
			$kategorie = new Kategorie('default');
			$kategorie->lang = $this->lang;
			$kategoria = $kategorie->showWybranaKategoria($kateg);
			$produkty = new Katprod('default');
			$produkty->id = $kateg;
			$produkty->nr = -1;
			$produkty->typ = 'kategoria';
			if($strona == 0) $ilosc = $produkty->selectProdukt(0, 99999, null, $szukanie, true, 0, null, false);
			$all = $produkty->selectProdukt($page, $ilu, null, $szukanie, false, 0, null, false, $od);
		}
		else
		{
			$kateg = 0;
			$produkty = new Produkty('default');
			if($strona == 0) $ilosc = $produkty->wypiszProduktyPodzieloneStrona(true, 0, 99999, null, $szukanie, 0, null, false);
			$all = $produkty->wypiszProduktyPodzieloneStrona(false, $page, $ilu, null, $szukanie, 0, null, false, $od);
		}
		if($grupa > 0)
		{
			$produktyGrupy = new Produktygrupy('default');
			$produktyGrupy->id = $grupa;
			$produktyGrupa = $produktyGrupy->wypiszPojedynczego();
		}
		else $grupa = 0;
		
		//echo '<ilu>'.@intval($ilu).'</ilu>';
		//echo '<strona>'.@intval($strona).'</strona>';
		if($strona == 0) echo '<ilosc>'.@intval($ilosc).'</ilosc>';
		
		if(@count($all) > 0)
		{
			$col_all = true ? '100%' : '700px';
			$col_LP = true ? '6%' : '50px';
			$col_Nazwa = true ? '80%' : '550px';
			$col_Cena = true ? '13%' : '100px';
			$p = 1;
			$html = '<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" style="font-size: 12px;">';
			$this->common->obliczRabaty = $this->obConfig->rabaty;
			$all = $this->produktyGaleria($all);
			//var_dump($all);die();
			if($strona == 0)
			{
				$title = ucfirst($this->lang('cennik')).' - '.ucfirst($this->ustawienia['title']);
				$html .= '<tr style="width:'.$col_all.';">';
				$html .= '<th style="width:'.$col_LP.';text-align:center;">&nbsp;</th>';
				$html .= '<th style="width:'.$col_Nazwa.';text-align:center;"><b>'.@ucfirst($title).'</b></th>';
				$html .= '<th style="width:'.$col_Cena.';text-align:right;"><b>&nbsp;</b></th>';
				$html .= '</tr>';
			}
			if(true)
			{
			if($strona == 0) $html .= '<tr style="width:'.$col_all.';"><th colspan="3">&nbsp;</th></tr>';
			if($strona == 0)
			{
				$html .= '<tr style="width:'.$col_all.';">';
				$html .= '<th style="width:'.$col_LP.';text-align:center;">&nbsp;</th>';
				$html .= '<th style="width:'.$col_Nazwa.';text-align:center;"><b>'.date("Y-m-d H:i").'</b></th>';
				$html .= '<th style="width:'.$col_Cena.';text-align:right;"><b>'.ucfirst($this->lang('data')).'</b></th>';
				$html .= '</tr>';
			}
			if($strona == 0)
			{
				$html .= '<tr style="width:'.$col_all.';">';
				$html .= '<th style="width:'.$col_LP.';text-align:center;">&nbsp;</th>';
				$html .= '<th style="width:'.$col_Nazwa.';text-align:center;"><b>'.ucfirst($this->zalogowany).'</b></th>';
				$html .= '<th style="width:'.$col_Cena.';text-align:right;"><b>'.ucfirst($this->lang('kontrahent')).'</b></th>';
				$html .= '</tr>';
			}
			if($strona == 0) $html .= '<tr style="width:'.$col_all.';"><th colspan="3">&nbsp;</th></tr>';
			if($strona == 0)
			{
				$kategoria = $kateg > 0 ? @stripslashes($kategoria['nazwa']) : $this->lang('wszystkie kategorie');
				$html .= '<tr style="width:'.$col_all.';">';
				$html .= '<th style="width:'.$col_LP.';text-align:center;">&nbsp;</th>';
				$html .= '<th style="width:'.$col_Nazwa.';text-align:center;"><b>'.ucfirst($kategoria).'</b></th>';
				$html .= '<th style="width:'.$col_Cena.';text-align:right;"><b>'.ucfirst($this->lang('kategoria')).'</b></th>';
				$html .= '</tr>';
			}
			if($strona == 0)
			{
				$produktGrupa = $grupa > 0 ? @stripslashes($produktyGrupa['nazwa']) : $this->lang('wszystkie grupy');
				$html .= '<tr style="width:'.$col_all.';">';
				$html .= '<th style="width:'.$col_LP.';text-align:center;">&nbsp;</th>';
				$html .= '<th style="width:'.$col_Nazwa.';text-align:center;"><b>'.ucfirst($produktGrupa).'</b></th>';
				$html .= '<th style="width:'.$col_Cena.';text-align:right;"><b>'.ucfirst($this->lang('grupa')).'</b></th>';
				$html .= '</tr>';
			}
			if($strona == 0) $html .= '<tr style="width:'.$col_all.';"><th colspan="3">&nbsp;</th></tr>';
			if(true)
			{
				$html .= '<tr style="width:'.$col_all.';">';
				$html .= '<th style="width:'.$col_LP.';text-align:center;"><b>'.$this->lang('lp').'</b></th>';
				$html .= '<th style="width:'.$col_Nazwa.';text-align:center;"><b>'.ucfirst($this->lang('nazwa')).'</b></th>';
				$html .= '<th style="width:'.$col_Cena.';text-align:center;"><b>'.ucfirst($this->lang('cena')).'</b></th>';
				$html .= '</tr>';
			}
			if(false)
			{
				$html .= '<tr style="width:'.$col_all.';">';
				$html .= '<td style="width:'.$col_LP.';text-align:center;">0</td>';
				$html .= '<td style="width:'.$col_Nazwa.';text-align:left;">xxx</td>';
				$html .= '<td style="width:'.$col_Cena.';text-align:right;">0.00</td>';
				$html .= '</tr>';
			}
			}
			if(true)
			for($x=0;$x<=0;$x++)
			foreach($all as $prod)
			{
				$lp = $strona > 0 ? $ile0 + ($strona - 1) * $ilu + $p++ : $p++;
				//$html .= '<div style="float:left; clear:left; width:600px;">';
				//$html .= '<span style="float:left; display:inline-block; width:'.$col_LP.';">'.($p++).'</span>';
				//$html .= '<span style="float:left;">'.stripslashes($prod['nazwa']).'</span>';
				//$html .= '<span style="float:right;">'.number_format($prod['cena'], 2, ',', ' ').' '.$this->zl.'</span>';
				//$html .= '</div>';
				$html .= '<tr style="width:'.$col_all.';">';
				$html .= '<td style="width:'.$col_LP.';text-align:center;">'.($lp++).'</td>';
				$html .= '<td style="width:'.$col_Nazwa.';text-align:left;">'.stripslashes($prod['nazwa']).'</td>';
				$html .= '<td style="width:'.$col_Cena.';text-align:right;">'.number_format($prod['cena_rabat'], 2, ',', ' ').' '.$this->zl.'</td>';
				$html .= '</tr>';
				//break;
			}
			$html.= '</table>';
			try
			{
				$css = '<head>'.PHP_EOL.'<style type="text/css">'.PHP_EOL.@$css.PHP_EOL.'</style>'.PHP_EOL.'</head>';
				//$html = '<body>'.PHP_EOL.$html.PHP_EOL.'</body>';
				//echo $html; die();
				$body = '<html>'.PHP_EOL.$css.$html.PHP_EOL.'</html>';
				
				$site = $this->lang('Cennik');
				$link = $this->common->makeLink($site);
				if($strona == 0 || !isset($_SESSION['cennik'][$this->zalogowanyID]['data']))
				$_SESSION['cennik'][$this->zalogowanyID]['data'] = date("Y-m-d_H-i");
				$data = $_SESSION['cennik'][$this->zalogowanyID]['data'];
				$cennik = "admin/cennik/".strtolower($link)."_".$this->zalogowanyID."_".$kateg."_".$grupa."_".$data."_".($strona+1).".pdf";
				
				if(true)
				{
					$objPDF = new tcpdf(PDF_PAGE_ORIENTATION, 'px', PDF_PAGE_FORMAT, true, 'UTF-8', false);
					//$objPDF = new tcpdf(PDF_PAGE_ORIENTATION, 'px', PDF_PAGE_FORMAT, false, 'ISO-8859-2', false);
					$objPDF->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);//PDF_MARGIN_TOP
					$objPDF->SetHeaderMargin(0);//PDF_MARGIN_HEADER
					$objPDF->SetFooterMargin(0);//PDF_MARGIN_FOOTER
					$objPDF->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);//PDF_MARGIN_BOTTOM
					
					$lg = Array();
					$lg['a_meta_charset'] = 'UTF-8';
					//$lg['a_meta_dir'] = 'rtl';
					$lg['a_meta_language'] = 'pl';
					$lg['w_page'] = 'page';
					//$objPDF->setLanguageArray($lg);
					
					$objPDF->setFontSubsetting(true);
					//$objPDF->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
					$fontname = $objPDF->addTTFfont('fonts/arial.ttf', 'TrueTypeUnicode', '', 32);
					//$objPDF->AddFont('Arial','', 'arialpl.php');
					//$objPDF->SetFont($fontname, '', 12, '', true);
					$objPDF->SetFont('freeserif', '', 12, '', true);
					//$objPDF->SetFont('dejavusans', 'B', 12, '', true);
					
					//$html = "ąęśżźćóńł";
					$objPDF->AddPage();
					//echo $html; die();
					//$html = iconv('UTF-8','ISO-8859-2//IGNORE', $html);
					$objPDF->writeHTML($html, true, false, true, false, '');
					$ok = $objPDF->Output($cennik, 'F');
					//var_dump($ok);				
					if(empty($ok))
					{
						$href = '<a href="'.$this->baseUrl.'/public/'.$cennik.'" target="_blank">'.$this->lang('pobierz cennik').'</a>';
						echo '<pdf>'.str_replace("admin/cennik/", "", $cennik).'</pdf>';
					}
					else echo '<error>'.$this->lang('blad').'</error>';
				}
				else
				{
					//require_once 'Zend/Loader.php';
					require_once("dompdf_config.inc.php");
					//require_once("include/autoload.inc.php");
					//Zend_Loader::registerAutoload();
					//spl_autoload_register('DOMPDF_autoload');
					$load = Zend_Loader_Autoloader::getInstance();
					$load->pushAutoloader('DOMPDF_autoload','');
					//require_once("include/dompdf_exception.cls.php");
					//require_once("../application/models/dompdf6/dompdf_config.inc.php");
					$dompdf = new DOMPDF();
					//$dompdf->set_base_path(APPLICATION_PATH."/../");					
					$body = iconv('UTF-8','ISO-8859-2//IGNORE', $body);
					//echo $html; die();					
					$dompdf->load_html($body);
					//echo $dompdf->output_html();die();
					$dompdf->render();
					$pdf = $dompdf->output();
					//$dompdf->stream($cennik, array("Attachment" => false));die();
					//var_dump($pdf);				
					if(file_put_contents($cennik, $pdf) !== false)
					{
						$href = '<a href="'.$this->baseUrl.'/public/'.$cennik.'" target="_blank">'.$this->lang('pobierz cennik').'</a>';
						echo '<pdf>'.str_replace("admin/cennik/", "", $cennik).'</pdf>';
					}
					else echo '<error>'.$this->lang('blad').'</error>';
				}				
			}
			catch(DOMPDF_Exception $e)
			{
				echo '<error>'.$e->getMessage().'</error>';
			}
			catch(Exception $e)
			{
				echo '<error>'.$e->getMessage().'</error>';
			}
		}
		else
		{
			echo '<error>'.$this->lang('brak wynikow').'</error>';
		}
		echo '</root>';
	}	
	
	function emailAction() 
	{
        $this->_helper->viewRenderer->setNoRender();		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		
		$od = $this->_request->getParam('od');
		$mail = $this->_request->getParam('mail');
		$daneMail = $this->_request->getPost();
		$zalaczniki = $this->_request->getPost('pliki');		
		
		$daneMail['mail'] = $mail;
		$daneMail['opis'] = $this->common->fixTekst($daneMail['opis'], true, true);
		//var_dump($_POST);die();
		
		$wiadomosci = new Wiadomosci();
		echo '<od>'.$od.'</od>';
		echo '<mail>'.$mail.'</mail>';
		$blad = $wiadomosci->mailKlient($daneMail, false, $zalaczniki, false, 0);
		if(!empty($blad)) echo '<blad>'.strip_tags($blad).'</blad>'; else echo '<ok>OK</ok>';
		echo '<data>'.date("Y-m-d H:i:s").'</data>';
        
		echo '</root>';
    }
	
	function atrcenyAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		$szuk = intval($this->_request->getParam('atr', 0));
		$atrCeny = new Atrybutypowiazania();
		$atrCeny->id = $id = intval($this->_request->getParam('id', 0));
		$this->atrCeny = $atrCeny->getRowsUniqueCeny();
		$where = 'ac.id_prod = '.$id;
		$break = false;
		$a = 0;
		//var_dump($this->atrCeny);
		if(count($this->atrCeny) > 0)
		foreach($this->atrCeny as $atr => $atrCena)
		{
			if($atr == $szuk) break;
			if(@!$atrCena['ceny']) continue;
			//if($break){$szuk = $atr; break;}
			//if($atr == $szuk) $break = true;
			$val = intval($this->_request->getParam('atr'.$atr, 0));
			if($val > 0 || $a++ > 0)
			$where .= ' and (atr'.$atr.' = '.$val.' or atr'.$atr.' = 0)';
		}
		$atrCeny = new Atrybutyceny();
		$result = $atrCeny->getRowsWhere($where, 'atr'.$szuk, 'nazwa');
		//var_dump($where);var_dump($result);
		$odpowiedz = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
		$odpowiedz.= '<root>';
		//$odpowiedz.= '<atr><id>0</id><nazwa>'.$this->common->lang('wybierz').'</nazwa></atr>';
        if(count($result) > 0)
		{
			$atrybuty = new Atrybuty();
			$atrNazwy = $atrybuty->getAtrybuty(null, 'id', null, $id);
			//var_dump($result);
			if(@intval($result[0]['atr'.$szuk]) == 0) $result = $atrybuty->getAtrybuty(null, 'id', null, $id, $szuk);
			//var_dump($result);
			if(count($result) > 0)
			foreach($result as $r => $row)
			{
				$at = isset($row['atr'.$szuk]) ? $row['atr'.$szuk] : $r;
				$result[$r]['value'] = @$atrNazwy[$at]['wartosc'];
			}
			$typ = @$this->atrCeny[$szuk]['jedn_miary_typ'];
			$result = $this->common->compare($result, true, 'value', $typ, 'asc');
			//var_dump($result);
			if(count($result) > 0)
			foreach($result as $r => $row)
			{
				$at = isset($row['atr'.$szuk]) ? $row['atr'.$szuk] : $r;
				$odpowiedz.= '<atr>';
				$odpowiedz.= '<id>'.$at.'</id>';
				$odpowiedz.= '<nazwa>'.@$row['value'].'</nazwa>';
				//$odpowiedz.= '<nazwa><![CDATA['.$row['value'].']]></nazwa>';
				$odpowiedz.= '</atr>';
			}
        }
		//else $odpowiedz.= '<atr><id>0</id><nazwa>'.$this->common->lang('wybierz').'</nazwa></atr>';
		$odpowiedz.= '</root>';
		echo $odpowiedz;
    }
	function atrcenaAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		$atrCeny = new Atrybutypowiazania('default');
		$atrCeny->id = $id = intval($this->_request->getParam('id', 0));
		$this->atrCeny = $atrCeny->getRowsUniqueCeny();
		$where = 'id_prod = '.$id;
		$break = false;
		$Object = new Atrybutygrupy();
		$atrGrupy = $Object->getAtrybuty();
		$atrybuty = new Atrybuty();
		$atrNazwy = $atrybuty->getAtrybuty(null, 'id', null, $id);		
		//var_dump($atrNazwy);
		if(count($this->atrCeny) > 0)
		foreach($this->atrCeny as $atr => $atrCena)
		{
			if(@!$atrCena['ceny']) continue;
			$val = intval($this->_request->getParam('atr'.$atr, 0));
			$where .= ' and (atr'.$atr.' = '.$val.' or atr'.$atr.' = 0)';
		}
		$atrCeny = new Atrybutyceny('default');
		$result = $atrCeny->getRowsWhere($where, '*', 'id');
		//var_dump($result);
        if(count($result) > 0)
		{
			$produkty = new Produkty('default');
			$produkty->id = $id;
			$produkt = $produkty->wypiszPojedynczaAll();
			$this->ceny = $produkty->obliczCene($id, $result[0]['cena_netto'], $result[0]['cena_brutto']);
			$this->ceny['netto'] = floatval($result[0]['cena_netto']);
			$this->ceny['brutto'] = floatval($result[0]['cena_brutto']);
			$this->ceny['cena_netto'] = floatval($this->ceny['cena_netto']);
			$this->ceny['cena_brutto'] = $this->common->obliczBruttoFromNettoVat($this->ceny['cena_netto'], $produkt['vat']);
			//$this->common->obConfig = $this->common->getObConfig();
			//$this->ceny = $this->common->setCeny($result[0], $produkt['promocja'], @$this->typ_klienta);
			//var_dump($this->ceny);
			//echo @floatval($ceny['cena']);
			//$this->ceny = $this->common->getCeny(true);
			if(@count($this->ceny) > 0)
			{
				$odpowiedz = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
				$odpowiedz.= '<root>';
                $odpowiedz.= '<id>'.@intval($result[0]['id']).'</id>';
				$nrKat = @trim($result[0]['nr_kat'].' '.$result[0]['id_ext']);
				if(@count($result[0]) > 0)
				foreach($result[0] as $co => $val)
				{
					if(strpos($co, "atr") !== false)
					{
						$atr = str_replace("atr", "", $co);
						//echo 'atr = '.$atr.'<br>';
						if(intval($atr) > 0)
						if(intval($val) > 0)
						{
							//echo 'val = '.$val.'<br>';
							$dopisz = @$atrNazwy[$val]['wartosc'];
							//echo 'dopisz = '.$dopisz.'<br>';
							$nr_kat_dopisz = @$atrGrupy[$atr]['nr_kat_dopisz'];
							//echo 'nr_kat_dopisz = '.$nr_kat_dopisz.'<br>';
							if(!empty($nr_kat_dopisz))
							{
								if($nr_kat_dopisz == "caly") $dopisz = $dopisz;
								if($nr_kat_dopisz == "zera") $dopisz = str_replace("0", "", $dopisz);
								if($nr_kat_dopisz == "znak") $dopisz = substr($dopisz, 0, 1);
								if($nr_kat_dopisz == "zero")
								{
									$last = substr($dopisz, -1);
									//echo 'last = '.$last.'<br>';
									$dopisz = $last.substr($dopisz, 0, -1);
									//echo 'dopisz = '.$dopisz.'<br>';
									$dopisz = substr($dopisz, -3);
									//echo 'dopisz = '.$dopisz.'<br>';
								}
								$nrKat .= " ".$dopisz;
							}
						}
					}
				}
				$odpowiedz.= '<id_ext>'.@trim($nrKat).'</id_ext>';
                $odpowiedz.= '<ilosc>'.@intval($result[0]['dostepnosc']).'</ilosc>';
				foreach($this->ceny as $nazwa => $cena)
				{
					$odpowiedz.= '<cena>';
					$odpowiedz.= '<nazwa>'.$nazwa.'</nazwa>';
					$odpowiedz.= '<wartosc>'.floatval($cena).'</wartosc>';
					$odpowiedz.= '</cena>';
				}
				$odpowiedz.= '</root>';
				echo $odpowiedz;
			}
		}
		else echo 0;
    }
	
	function reklamaAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		//echo 'cześć';
		$settings = new Ustawienia();
		$ustawienia = $settings->wypisz();
		if($this->_request->getParam('obraz'))
		{
			if(!empty($ustawienia['reklama_obraz']))
			{
				$file = '../public/admin/reklama/'.$ustawienia['reklama_obraz'];
				if(file_exists($file))
				{
					$size = getimagesize($file);
					header('Content-Type: '.$size['mime']);
					header('Content-Length: '.filesize($file));
					readfile($file);
				}
				//else echo 'brak pliku';
			}
		}
		else if($this->_request->getParam('tekst'))
		{
			echo stripslashes($ustawienia['reklama_tekst']);
		}
		else
		{
			if(!empty($ustawienia['reklama_obraz']))
			{
				$file = '../public/admin/reklama/'.$ustawienia['reklama_obraz'];
				if(file_exists($file))
				{
					echo filesize($file);
				}
				else echo 0;
			}
			else echo 0;
		}
		die();
    }
	
	function koszileAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		echo @count($_SESSION['koszyk']);
    }
	function koszvalAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		$typ_klienta = $this->_request->getParam('typ_klienta');
		echo @floatval(false && $typ_klienta ? $_SESSION['wartosc_koszyka_netto'] : $_SESSION['wartosc_koszyka']);
    }
	function koszykAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		//$_POST = $this->_request->getPost('id');
		//var_dump($_POST);
		$koszyk = new Koszyk($this->moduleAjax);
		echo $koszyk->add();
    }
	function koszAction() 
	{
        //$this->_helper->viewRenderer->setNoRender();
    }
	function koszykiAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		$mail = new Body('default');
		$koszyki = new Koszyki();
		$info = new Wiadomosci();
		$klienci = new Kontrahenci();
		$zamowienie = new Zamowienia();
		$koszyk_dni = @intval($this->ustawienia['koszyk_dni']);
		if($koszyk_dni == 0) die('koszyk_dni = '.$koszyk_dni);
		$koszykiAll = $koszyki->wypiszAll($koszyk_dni);
		$echo = date('Y-m-d H:i');
		if(count($koszykiAll) > 0)
		foreach($koszykiAll as $koszyk)
		{
			$klienci->id = $koszyk['id_user'];
			$kontrahent = $klienci->wypiszPojedynczy()->toArray();
			//var_dump($kontrahent);die();			
			$kontrahent['nr_zam'] = 0;
			$kontrahent['data'] = $koszyk['data'];
			$kontrahent['numer'] = $zamowienie->getNumerZamow($kontrahent);
			$kontrahent['uwagi'] = "";
			$kontrahent['wysylka'] = "";
			$kontrahent['koszt_dostawy'] = 0;
			
			$wartosc_zamowienia = 0;
			$i = 0; $data = null;
			$_SESSION['koszyk'] = @unserialize($koszyk['koszyk']);
			if(count($_SESSION['koszyk']) > 0)
            foreach($_SESSION['koszyk'] as $k => $kosz)
			{
				$ids[] = $_SESSION['koszyk'][$k]['id'];
				foreach($_SESSION['koszyk'][$k]['ilosc'] as $rozmiar => $ile)
				{
					if(!($ile > 0)) continue;
					$data[$i]['id_zam'] = 0;
					$data[$i]['nazwa'] = $_SESSION['koszyk'][$k]['nazwa'];
					$data[$i]['id_prod'] = $_SESSION['koszyk'][$k]['id'];
					$data[$i]['oznaczenie'] = $_SESSION['koszyk'][$k]['oznaczenie'];
					$data[$i]['netto'] = $_SESSION['koszyk'][$k]['netto'][$rozmiar];
					$data[$i]['brutto'] = $_SESSION['koszyk'][$k]['brutto'][$rozmiar];
					$data[$i]['cena_netto'] = $_SESSION['koszyk'][$k]['cena_netto'][$rozmiar];
					$data[$i]['cena_brutto'] = $_SESSION['koszyk'][$k]['cena_brutto'][$rozmiar];
					$data[$i]['ilosc'] = $ile;//$_SESSION['koszyk'][$k]['ilosc'];
					$data[$i]['vat'] = @floatval($_SESSION['koszyk'][$k]['vat']);
					$data[$i]['wartosc_netto'] = $_SESSION['koszyk'][$k]['wartosci_netto'][$rozmiar];
					$data[$i]['wartosc'] = $_SESSION['koszyk'][$k]['wartosci'][$rozmiar];
					$data[$i]['rabat'] = @floatval($_SESSION['koszyk'][$k]['rabat']);
					$data[$i]['upust'] = @floatval($_SESSION['koszyk'][$k]['upust']);
					$data[$i]['rozmiar'] = $rozmiar;
					$data[$i]['kolor'] = @strval($_SESSION['koszyk'][$k]['kolor']);
					$data[$i]['atrybuty'] = @serialize($_SESSION['koszyk'][$k]['atrybuty']);
					$data[$i]['jedn'] =  @strval($_SESSION['koszyk'][$k]['jedn']);
					$data[$i]['jedn_miary'] = @$_SESSION['koszyk'][$k]['jedn_miary'];
					$data[$i]['jedn_miary_typ'] = @$_SESSION['koszyk'][$k]['jedn_miary_typ'];
					$data[$i]['jedn_miary_zb'] = @$_SESSION['koszyk'][$k]['jedn_miary_zb'];
					$data[$i]['jedn_miary_zb_ile'] = @$_SESSION['koszyk'][$k]['jedn_miary_zb_ile'];
					$data[$i]['jedn_miary_zb_typ'] = @$_SESSION['koszyk'][$k]['jedn_miary_zb_typ'];
					//$data[$i]['rozmiar'] = $rozmiary->getNazwa($rozmiar);
					$wartosc_zamowienia += $data[$i]['wartosc'];
					$i++;
				}
			}
			$kontrahent['wartosc_zamowienia'] = $wartosc_zamowienia;
			
			$mail->kontrahent = $kontrahent;
			$mail->prod = $data;
			$body = $mail->stworzTresc(null);
			$msg = '<a href="'.$this->obConfig->www.'/kontrahent/finalizacja/id/'.$koszyk['session_id'].'">'.$this->lang('finalizacja_zamowienia').'</a>';
			$blad = $info->sendMsg("", 'finalizacja_zamowienia', $msg.'<br/><br/>'.$body, 0, $koszyk['id_user']);
			if(empty($blad))
			{
				$koszyki->id = $koszyk['id'];
				$koszyki->edytuj(array('wyslano' => 1), $koszyk['id']);
				$echo .= '<br>wyslano '.$koszyk['session_id'].' do '.$koszyk['id_user'].'<br>';
			}
			else $echo .= '<br/>blad '.$blad.' dla '.$koszyk['session_id'].'<br/>';
		}
		//else $echo = '';
		if(!empty($echo))
		file_put_contents('ajax/finalizacja_'.date('N H').'.txt', $echo);
		die($echo);
    }
	
	function zamowstatusAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		
		if($this->_request->getParam('id_zam')) 
		{
			$id_zam = $this->_request->getParam('id_zam');
			$status = $this->_request->getParam('status');
			$czy_mail = $this->_request->getParam('czy_mail');
			$zamow = new Zamowienia();
			echo $zamow->changeStatusAll($id_zam, $status, $czy_mail);
		}
    }
	
	function obserwowaneAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
        $id = intval($this->_request->getParam('id', 0));
		$user = intval($this->_request->getParam('user', 0));
        $polecane = new Kontrahencipolecane('default');
		$obserwowany = $polecane->dodajProdukt($user, $id, 'obserwowane');
        echo intval($obserwowany);
    }
	function schowekAction()
	{
        $this->_helper->viewRenderer->setNoRender();
        $id = intval($this->_request->getParam('id', 0));
        
        $array = array();
        $schowek = isset($_COOKIE['schowek']) ? unserialize($_COOKIE['schowek']) : '';
        
        if(!empty($schowek) && is_array($schowek))
		{
            foreach($schowek as $key => $value)
			{
                $array[$value] = $value;
            }
        } 
        echo !isset($array[$id]) ? intval($id) : '';
        $array[$id] = $id;

        setcookie('schowek', serialize($array), time() + (3600 * 24 * 14), '/', $_SERVER['HTTP_HOST']);
    }
	function porownywarkaAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
        $id = intval($this->_request->getParam('id', 0));
		$ids = strval($this->_request->getParam('ids', 0));
		$del = intval($this->_request->getParam('del', 0));
		$user = intval($this->_request->getParam('user', 0));
		$ids = !empty($ids) ? explode(",", $ids) : null;
		$ids = count($ids) > 0 ? $ids : array($id);
		if($del > 0)
		{
			unset($_SESSION['porownywarka'][$del]);
			echo $del;
		}
		//var_dump($ids);
		$echo = "";
		if($id > 0)
		foreach($ids as $id)
		{
			if(intval($id) <= 0) continue;
			if(isset($_SESSION['porownywarka'][$id]))
			{
				if(count($ids) == 1) $echo .= "0";
				else $echo .= $this->lang('porownywarka_dodano').'<br/>';
				continue;
			}
			if(@count($_SESSION['porownywarka']) >= $this->obConfig->porownywarkaMaks)
			{
				$echo .= $this->lang('porownywarka_maks');
				break;
			}
			else
			{
				$ok = true;
				$kateg = 0;
				$kategNazwa = '';
				if($this->obConfig->porownywarkaJednaKategoria)
				if(@count($_SESSION['porownywarka']) > 0)
				foreach($_SESSION['porownywarka'] as $prodID => $prod)
				{
					//var_dump($prod);die();
					$kategs = count($prod['kateg']);
					$kateg = $kategs > 0 ? @intval($prod['kateg'][$kategs-1]['id']) : 0;
					if($kateg > 0)
					{
						$ok = false;
						$kategNazwa = @strval($prod['kateg'][$kategs-1]['nazwa']);
						break;
					}
				}
				$produkty = new Produkty('default');
				$produkty->id = $id;
				$prod = $produkty->wypiszPojedynczaAll();
				$produkt = $this->produktyGaleria(array($prod), $user, false, null, false);
				if(!$ok && $kateg > 0)
				if($this->obConfig->porownywarkaJednaKategoria)
				{
					if(@count($produkt[0]['kateg']) > 0)
					foreach($produkt[0]['kateg'] as $kat)
					{
						if($kat['id'] == $kateg)
						{
							$ok = true;
							break;
						}
					}
					//var_dump($kateg);die();
				}
				if($ok)
				{
					$_SESSION['porownywarka'][$id] = $produkt[0];
					if(count($ids) == 1) $echo .= $id;
					else $echo .= $this->lang('porownywarka_dodany').'<br/>';
				}
				else
				{
					$echo .= 'Nie można dodać tego produktu do porównywarki<br/>';
					$echo .= 'Dodaj produkty z kategorii "'.$kategNazwa.'"<br/>';
					$echo .= 'lub usuń wszystkie produkty z porównywarki';
					//echo -1;
				}
			}
		}
		echo $echo;
		//var_dump($_SESSION['porownywarka']);die();
		die();
    }
	function porownywaneAction() 
	{
        //$this->_helper->viewRenderer->setNoRender();
    }
	
	function zmianawidocznyAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		
        $id = intval($this->_request->getParam('prod', 0));
		$pole = $this->_request->getParam('pole', 'widoczny');
		$widok = intval($this->_request->getParam('widok', 0));

        $produkty = new Produkty();
		$produkty->id = $id;
		$prod = $produkty->wypiszPojedyncza();
		
		$gdzie = ''.$prod['nazwa'].' ';		
		if($widok) $gdzie .= 'widoczność włączona';
		if(!$widok) $gdzie .= 'widoczność wyłączona';
		if($pole == "widoczny") $gdzie .= ' w Draco';
		if($pole == "widoczny_ozeo") $gdzie .= ' w Ozeo';
		
		$dane[$pole] = $widok;
		$dane['zmiana'] = date('Y-m-d');//die();
		$ok = $produkty->edytujProdukty($dane);
		if($ok) Events::log($this->loguser, 'zmianawidocznyajax', $id, 'zmianawidoczny', 'index', $widok, $gdzie);
		echo $ok;
		die();
    }
	
	function zmiananazwyAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
        $id = intval($this->_request->getParam('prod', 0));
		$nazwa = $this->_request->getPost('nazwa', '');
		//echo $nazwa = urldecode($nazwa);
		$nazwa = htmlspecialchars($nazwa);
		//$nazwa = htmlspecialchars_decode($nazwa);
		//$nazwa = mb_substr($nazwa, 0, 50, 'UTF-8');
        $produkty = new Produkty();
		$produkty->id = $id;
		$prod = $produkty->wypiszPojedyncza();
		if($nazwa == $prod['nazwa']) { echo $nazwa; die(); }
		$dane['nazwa'] = $nazwa;
		$dane['zmiana'] = date('Y-m-d');
		$ok = $produkty->edytujProdukty($dane);
		if($ok)
		{
			$gdzie = ''.$prod['nazwa'].', zmiana nazwy na '.$nazwa;
			Events::log($this->loguser, 'zmiananazwyajax', $id, 'zmiananazwy', 'index', $nazwa, $gdzie);
			$route = new Routers();
			$sprawdz_link = $route->edytuj($nazwa.'-'.$id, $prod['route_id']);
			if($sprawdz_link['id'] > 0)
			{
				$data['link'] = $sprawdz_link['link'];
				$produkty->edytujProdukty($data);
				echo "OK";
			}
			else echo "OK";
		}
        else echo "BŁĄD";
    }
	
	function zmianacenyAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
        $id = intval($this->_request->getParam('prod', 0));
		$cena = $this->_request->getParam('cena', 0);
		$cena = floatval(str_replace(',', '.', $cena));
        $produkty = new Produkty();
		$produkty->id = $id;
		$produkt = $produkty->wypiszPojedyncza();
		$gdzie = ''.$produkt['nazwa'].', zmiana ceny na '.$cena;
		
		$netto = $this->obConfig->cenyNettoDomyslne;
		if($this->obConfig->cenyNettoDomyslne)
		{
			$cena_netto = max(0, $cena);
			$cena_brutto = $this->common->obliczBruttoFromNettoVat($cena, $produkt['vat']);
		}
		else
		{
			$cena_brutto = max(0, $cena);
			$cena_netto = $this->common->obliczNettoFromBruttoVat($cena, $produkt['vat']);
		}
		if($produkt['promocja'])
		{
			$dane['cena_promocji_n'] = $cena_netto;
			$dane['cena_promocji_b'] = $cena_brutto;
			$cenaOld = $netto ? $produkt['cena_promocji_n'] : $produkt['cena_promocji_b'];
		}
		else
		{
			$dane['cena_netto'] = $cena_netto;
			$dane['cena_brutto'] = $cena_brutto;
			$cenaOld = $netto ? $produkt['cena_netto'] : $produkt['cena_brutto'];
		}
		if($this->obConfig->tylkoHurt)
		{
			if($produkt['promocja'])
			{
				$dane['cena_promocji_n_hurt'] = $cena_netto;
				$dane['cena_promocji_b_hurt'] = $cena_brutto;
				$cenaOld = $netto ? $produkt['cena_promocji_n_hurt'] : $produkt['cena_promocji_b_hurt'];
			}
			else
			{
				$dane['cena_netto_hurt'] = $dane['cena_netto'];
				$dane['cena_brutto_hurt'] = $dane['cena_brutto'];
				$cenaOld = $netto ? $produkt['cena_netto_hurt'] : $produkt['cena_brutto_hurt'];
			}
		}
		
		if($cena == $cenaOld) { echo number_format($cena, 2, '.', ''); die(); }
		$dane['zmiana'] = date('Y-m-d');
		$ok = $produkty->edytujProdukty($dane);
        if($ok) Events::log($this->loguser, 'zmianacenyajax', $id, 'zmianaceny', 'index', $cena, $gdzie);
        echo $ok ? number_format($cena, 2, '.', '') : "BŁĄD";
		die();
    }
	
	function zmianasztukAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
        $id = intval($this->_request->getParam('prod', 0));
		$sztuk = $this->_request->getParam('sztuk', 0);
		$sztuk = intval(str_replace(',', '.', $sztuk));
        $produkty = new Produkty();
		$produkty->id = $id;
		$produkt = $produkty->wypiszPojedyncza();
		$dostepnosc = intval($produkt['dostepnosc']);
		if($sztuk == $dostepnosc) { echo $sztuk; die(); }
		$dane['dostepnosc'] = max(0, $sztuk);
		if(($dostepnosc == 0 && $sztuk > 0) || ($dostepnosc > 0 && $sztuk == 0))
		if(false) $dane['widoczny'] = intval($sztuk) > 0 ? 1 : 0;
		
		$gdzie = ''.$produkt['nazwa'].', zmiana ilości z '.$produkt['dostepnosc'].' na '.$dane['dostepnosc'];
		if(isset($dane['widoczny'])) $gdzie .= ', '.($dane['widoczny']?'włączenie':'wyłączenie').' widoczności';
		
		if(intval($sztuk) == 0)
		{
			if($this->obConfig->allegro)
			{
				$allegro = new Allegroaukcje();
				$allegro->usunProduktAllegro($id);
			}
		}
		if(intval($sztuk) > 0 && intval($produkt['dostepnosc']) == 0)
		{
			if($this->obConfig->obserwowane)
			{
				$polecane = new Kontrahencipolecane();
				$polecane->obsluzObserwowane($id);
			}
		}
		
		$dane['zmiana'] = date('Y-m-d');
		$ok = $produkty->edytujProdukty($dane);
		if($ok) Events::log($this->loguser, 'zmianasztukajax', $id, 'zmianasztuk', 'index', $sztuk, $gdzie);
        echo $ok ? $sztuk : "BŁĄD";
		die();
    }
	
	function captchaAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		
        $captcha = new Zend_Captcha_Image(array
		(
			'font' => $this->path.'/public/admin/captcha/font.ttf'
		));

		$captcha->setImgDir($this->path.'/public/admin/captcha/images');
		$captcha->setWordlen(6);
		$captcha->setWidth(200);
		$captcha->setHeight(40);
		$captcha->setLineNoiseLevel(5);
		$captcha->setDotNoiseLevel(5);

		$captchaImg = $captcha->generate();
		$captchaKod = $captcha->getWord();
		
		echo $captchaImg.'|'.$captchaKod;
    }
	
	function allegrokatsupdateAction() 
	{
        $this->_helper->viewRenderer->setNoRender();		
		set_time_limit(0);		
		ini_set("memory_limit", "512M");		
		ob_clean();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';		
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image');		
		$dane = $this->_request->getPost('dane');
		
		$ile = intval($this->_request->getParam('ile'));
		if($ile == 0) $ile = 1000;
		$od = intval($this->_request->getParam('od', 0));
		$count = intval($this->_request->getParam('count', 0));
		//$count = 0;
		
		$kats = new Allegrokategorie();
		$konto = new Allegrokonta();
		$webapi = $konto->zaloguj();
		if($webapi == null) echo '<error>'.$konto->error.'</error>';		
		if($count == 0)	$count = $kats->getAllegroCount($webapi);
		
		$do = min($od + $ile, $count);
		if($webapi != null)
		$kats->updateFromAllegro($webapi, $ile, $od, $count);
		
		echo '<ile>'.$ile.'</ile>';
		echo '<ileRows>'.($do - $od).'</ileRows>';
		echo '<ileRowsAll>'.$count.'</ileRowsAll>';
		echo '<ileRecords>'.$do.'</ileRecords>';
        echo '</root>';
    }
	function allegropolaupdateAction() 
	{
        $this->_helper->viewRenderer->setNoRender();		
		set_time_limit(0);		
		ini_set("memory_limit", "512M");		
		ob_clean();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';		
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image');		
		$dane = $this->_request->getPost('dane');
		
		$ile = intval($this->_request->getParam('ile'));
		if($ile == 0) $ile = 1000;
		$od = intval($this->_request->getParam('od', 0));
		$count = intval($this->_request->getParam('count', 0));
		//$count = 0;
		
		$pola = new Allegropola();
		$konto = new Allegrokonta();
		$webapi = $konto->zaloguj();
		if($webapi == null) echo '<error>'.$konto->error.'</error>';		
		if($count == 0)	$count = $pola->getAllegroCount($webapi);
		
		$do = min($od + $ile, $count);
		if($webapi != null)
		$pola->updateFromAllegro($webapi, $ile, $od, $count);
		
		echo '<ile>'.$ile.'</ile>';
		echo '<ileRows>'.($do - $od).'</ileRows>';
		echo '<ileRowsAll>'.$count.'</ileRowsAll>';
		echo '<ileRecords>'.$do.'</ileRecords>';
        echo '</root>';
    }
	function allegrokrajeupdateAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		set_time_limit(0);
		ini_set("memory_limit", "512M");
		ob_clean();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image');
		$dane = $this->_request->getPost('dane');
		
		$ile = intval($this->_request->getParam('ile'));
		if($ile == 0) $ile = 1000;
		$od = intval($this->_request->getParam('od', 0));
		$count = intval($this->_request->getParam('count', 0));
		//$count = 0;
		
		$kraje = new Allegrokraje();
		$konto = new Allegrokonta();
		$webapi = $konto->zaloguj();
		if($webapi == null) echo '<error>'.$konto->error.'</error>';
		//if($count == 0)	$count = $kraje->getAllegroCount($webapi);
		
		$do = min($od + $ile, $count);
		if($webapi != null)
		$kraje->updateFromAllegro($webapi, $ile, $od, $count);
		
		echo '<ile>'.$ile.'</ile>';
		echo '<ileRows>'.($do - $od).'</ileRows>';
		echo '<ileRowsAll>'.$count.'</ileRowsAll>';
		echo '<ileRecords>'.$do.'</ileRecords>';
        echo '</root>';
    }
	
	function allegrokatsupdatestatusAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$import = new Import();
		echo $import->getTabelaCount('allegrokategorie');
	}
	function allegropolaupdatestatusAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$import = new Import();
		echo $import->getTabelaCount('allegropola');
	}
	function allegrokrajeupdatestatusAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$import = new Import();
		echo $import->getTabelaCount('allegrokraje');
	}
	
	function allegrokatsupdateallAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		$kats = new Allegrokategorie();
		//echo $kats->getAllegroCount();
		//echo $kats->updateFromAllegro();
		echo $kats->setPaths();
    }
	function allegropolaupdateallAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		$pola = new Allegropola();
		echo $pola->getAllegroCount();
		//echo $pola->updateFromAllegro(null, 1000);
    }
	function allegropolaupdateoneAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		$konto = new Allegrokonta();
		$webapi = $konto->zaloguj();
		$pola = $webapi->objectToArray($webapi->GetSellFormFieldsForCategory(array('category-id'=>135)));
		var_dump($pola);
    }
	function allegrokrajeupdateallAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		$kraje = new Allegrokraje();
		//echo $kraje->getAllegroCount();
		//echo $kraje->updateFromAllegro(null, 1000);
		$konto = new Allegrokonta();
		$webapi = $konto->zaloguj();
		//$wojew = $webapi->objectToArray($webapi->GetStatesInfo(1));
		//var_dump($wojew);
    }
	function allegroshipmentdataAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
		$konto = new Allegrokonta();
		$webapi = $konto->zaloguj();
		$shipments = $webapi->objectToArray($webapi->GetShipmentData());
		var_dump($shipments);
    }
	
	function szukajwojewAction()
	{
		$this->_helper->viewRenderer->setNoRender();

        $q = $this->_request->getParam('q', '');
		
        $kraje = new Allegrokraje();
        $result = $kraje->getRegions($q, false);

		$odpowiedz = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
		$odpowiedz.= '<root>';
		$odpowiedz.= '<wojew><id>0</id><nazwa></nazwa></wojew>';
        if(count($result) > 0)
		{
			foreach($result as $row)
			{
				$odpowiedz.= '<wojew>';
				$odpowiedz.= '<id>'.$row['state-id'].'</id>';
				$odpowiedz.= '<nazwa>'.$row['state-name'].'</nazwa>';
				$odpowiedz.= '</wojew>';
			}
        }
		//else $odpowiedz.= '<wojew><id>0</id><nazwa>'.$this->common->lang('wybierz').'</nazwa></wojew>';
		$odpowiedz.= '</root>';
		
		echo $odpowiedz;
    }
	
    function typszukaniaAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
        $typ = $this->_request->getParam('typ',0);
        //$typ = $_REQUEST['typ'];

        $search = new Zend_Session_Namespace('wyszukiwarka');
        if($typ == '0') 
		{
            $search->typ = 0; //szukam w nazwach produktu
        }
        elseif($typ == '1') 
		{
            $search->typ = 1; //szukam w firmach
        }

        echo 'poszlo';
    }
	
    function odpowiedziAction() 
	{
        $this->_helper->viewRenderer->setNoRender();

        $suggest = new Autouzupelnianie();
        $keyword = $this->_request->getParam('keyword',0);

        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT' );
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . 'GMT');
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: text/xml');
        echo $suggest->getSugestie($keyword);
    }

    function kategorieAction() 
	{
		$this->_helper->viewRenderer->setNoRender();

        $id = $this->_request->getParam('kat', 0);
		$typ = $this->_request->getParam('typ', 'kategorie');
		$all = $this->_request->getParam('all', false);
		if($typ == "_ajax") $typ = "kategorie";
		
        $kategorie = new Kategorie('admin', $typ);
        $result = $kategorie->wypiszDzieci($id);

		if($all == 2) $podkategorie[0] = array('id' => -1, 'nazwa' => 'Wybierz');
		else if($all) $podkategorie[0] = array('id' => -1, 'nazwa' => 'Wszystkie');
        
		for($i=0;$i<count($result);$i++) 
		{
            $podkategorie[$i+1] = array('id' => $result[$i]['id'], 'nazwa' => $result[$i]['nazwa']);
        }
        $licz = count($podkategorie);
		//if(count($result) == 0 && isset($podkategorie[0])) $podkategorie[0]['nazwa'] = '';
        if($licz > 0)
		{
            $odpowiedz = '<?xml version="1.0" encoding="utf-8"?><root>';
            foreach($podkategorie as $podkat)
			{
                $odpowiedz.= '<podkat>'.$podkat['id'].'</podkat>';
                $odpowiedz.= '<nazwa>'.$podkat['nazwa'].'</nazwa>';
            }
            $odpowiedz.= '</root>';
        }
		else 
		{
            $odpowiedz = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><root><podkat>xx</podkat><nazwa>Brak podkategorii</nazwa></root>';
        }
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT' );
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . 'GMT');
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: text/xml');
        echo $odpowiedz;die();
    }
	
	function kategorieallegroAction() 
	{
		$this->_helper->viewRenderer->setNoRender();

        $id = $this->_request->getParam('kat', 0);
		
        $kategorie = new Allegrokategorie();
        $result = $kategorie->getChildren($id);

        for($i=0;$i<count($result);$i++) 
		{
            $podkategorie[] = array('id' => $result[$i]['cat-id'], 'nazwa' => $result[$i]['cat-name']);
        }
        $licz = @count($podkategorie);

        if($licz > 0) 
		{
            $odpowiedz ='<?xml version="1.0" encoding="utf-8"?><root>';
            for($i=0;$i<$licz;$i++) 
			{
                $odpowiedz.= '<podkat>'.$podkategorie[$i]['id'].'</podkat>';
                $odpowiedz.= '<nazwa>'.$podkategorie[$i]['nazwa'].'</nazwa>';
            }
            $odpowiedz.= '</root>';

        }
		else 
		{
            $odpowiedz = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><root><podkat>xx</podkat><nazwa>Brak podkategorii</nazwa></root>';
        }
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT' );
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . 'GMT');
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: text/xml');
        echo $odpowiedz;
    }
	
	function kategorieebayAction()
	{
		$this->_helper->viewRenderer->setNoRender();

        $id = $this->_request->getParam('kat', 0);
		
        $kategorie = new Ebaykategorie();
        $result = $kategorie->getChildren($id);

        for($i=0;$i<count($result);$i++) 
		{
            $podkategorie[] = array('id' => $result[$i]['cat_id'], 'nazwa' => $result[$i]['cat_name']);
        }
        $licz = count($podkategorie);

        if($licz > 0) 
		{
            $odpowiedz ='<?xml version="1.0" encoding="utf-8"?><root>';
            for($i=0;$i<$licz;$i++) 
			{
                $odpowiedz.= '<podkat>'.$podkategorie[$i]['id'].'</podkat>';
                $odpowiedz.= '<nazwa>'.$podkategorie[$i]['nazwa'].'</nazwa>';
            }
            $odpowiedz.= '</root>';

        }
		else 
		{
            $odpowiedz = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><root><podkat>xx</podkat><nazwa>Brak podkategorii</nazwa></root>';
        }
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT' );
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . 'GMT');
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: text/xml');
        echo $odpowiedz;
    }
	
	function autocompleteoldAction() 
	{
		$this->_helper->viewRenderer->setNoRender();

        $q = $this->_request->getParam('q', '');
		
        $produkty = new Produkty();
        $result = $produkty->wyszukajslowo2($q);

        for($i=0;$i<count($result);$i++)
		{
			$kod = $result[$i]['oznaczenie'];
			if(!empty($kod) && stripos($kod, $q) !== false) $echo[$kod] = $kod;
			$nazwa = $result[$i]['nazwa'];
			if(!empty($nazwa) && stripos($nazwa, $q) !== false) $echo[$nazwa] = $nazwa;
			//$producent = $result[$i]['producent'];
			//if(!empty($producent) && stripos($producent, $q) !== false) $echo[$producent] = $producent;
        }
		
		//$producenci = new Producent();
		//$result = $producenci->wypiszProducentowWhereNazwa($q);
		if(false)
		for($i=0;$i<count($result);$i++)
		{
			$producent = $result[$i]['nazwa'];
			if(!empty($producent) && stripos($producent, $q) !== false) $echo[$producent] = $producent;
        }
		
		if(isset($echo)) foreach($echo as $e) echo "$e|$e\n";
		//echo "$nazwa|$nazwa\n";
    }
	function autocompleteAction() 
	{
		$this->_helper->viewRenderer->setNoRender();

        $q = trim($this->_request->getParam('term', ''));
		if(strlen($q) < 5) die('[]');
		//echo $q;
        $produkty = new Produkty($this->moduleAjax);
        $result = $produkty->wyszukajslowo2($q);

		$echoes = array();
        for($i=0;$i<count($result);$i++)
		{
			$echo['id'] = intval($result[$i]['id']);
			$echo['value'] = stripslashes($result[$i]['nazwa']);
			$echo['link'] = strval($result[$i]['link']);
			array_push($echoes, $echo);
			if(count($echoes) > 20) break;
        }
		
		echo json_encode($echoes);
		//foreach($echo as $e) echo "$e\n";
		//echo "$nazwa|$nazwa\n";
    }
	function autocompletesymbolAction() 
	{
		$this->_helper->viewRenderer->setNoRender();

        $q = trim($this->_request->getParam('term', ''));
		$q2 = str_replace(" ", "", $q);
		if(strlen($q2) < 2) die('[]');
		$kat = intval($this->_request->getParam('kat', 0));
		$prod = intval($this->_request->getParam('prod', 0));
		//echo $q;
        $produkty = new Produkty($this->moduleAjax);
        $result = $produkty->wyszukajsymbol2($q, $prod, $kat);

		$echoes = array();
        for($i=0;$i<count($result);$i++)
		{
			$echo['id'] = intval($result[$i]['id']);
			$echo['value'] = stripslashes($result[$i]['oznaczenie']);
			$echo['link'] = strval($result[$i]['link']);
			array_push($echoes, $echo);
			if(count($echoes) > 999) break;
        }
		
		echo json_encode($echoes);
		//foreach($echo as $e) echo "$e\n";
		//echo "$nazwa|$nazwa\n";
    }
	function autocompleteproducentAction() 
	{
		$this->_helper->viewRenderer->setNoRender();

        $q = $this->_request->getParam('q', '');
		//echo $q;
        $produkty = new Produkty();
        $result = $produkty->wyszukajslowo2producent($q);

		$odpowiedz = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
		$odpowiedz.= '<root>';
		$odpowiedz.= '<producent><id>0</id><nazwa>producenci</nazwa></producent>';
		if(count($result) > 0)
		{
			foreach($result as $row)
			{
				$odpowiedz.= '<producent>';
				$odpowiedz.= '<id>'.$row['id'].'</id>';
				$odpowiedz.= '<nazwa>'.stripslashes($row['nazwa']).'</nazwa>';
				$odpowiedz.= '</producent>';
			}
		}
		//else $odpowiedz.= '<producent><id>0</id><nazwa>Brak wyników wyszukiwania</nazwa></producent>';
		$odpowiedz.= '</root>';
		echo $odpowiedz;
    }
	
	function autocompletenazwiskoAction()
	{
        $this->_helper->viewRenderer->setNoRender();
        $query = $this->_request->getParam('term', '');
        
        $prod = new Zamowienia();
        $mode->param = $this->_request->getParam('all');
        $this->view->mode = $mode->param;
        
        $info = new Zend_Session_Namespace('admin');
        //$prod->zalogowany_jako = $info->zalogowany_jako;
        $res = $prod->selectAllAjax($query);
        
		$echoes = array();
        for($i=0;$i<count($res);$i++)
		{
			//echo $res[$i]['nazwa_firmy'].strpos($res[$i]['nazwa_firmy'], $query).$query;
            //$echoes[] = trim("".$res[$i]["nazwa_firmy"]." ".$res[$i]["nazwa"]." ".$res[$i]["nazwisko"]."");
			if(stripos($res[$i]['nazwa_firmy'], $query) !== false)
			{
				if(!isset($rows[$res[$i]['nazwa_firmy']]))
				{
					$echoes[] = stripslashes($res[$i]['nazwa_firmy']);
					$rows[$res[$i]['nazwa_firmy']] = true;
				}
			}
			else if(stripos($res[$i]['nazwisko'], $query) !== false)
			{
				if(!isset($rows[$res[$i]['nazwisko']]))
				{
					$echoes[] = stripslashes($res[$i]['nazwisko']);
					$rows[$res[$i]['nazwisko']] = true;
				}
			}
			else if(stripos($res[$i]['nazwa'], $query) !== false)
			{
				if(!isset($rows[$res[$i]['nazwa']]))
				{
					$echoes[] = stripslashes($res[$i]['nazwa']);
					$rows[$res[$i]['nazwa']] = true;
				}
			}
			if(count($echoes) >= 15) break;
        }
        echo json_encode($echoes);
    }
	function autocompletenazwiskooldAction()
	{          
        $this->_helper->viewRenderer->setNoRender();
        $query = $this->_request->getParam('query',0);
        
        $prod = new Zamowienia();
        $mode->param = $this->_request->getParam('all');
        $this->view->mode = $mode->param;
        
        $info = new Zend_Session_Namespace('admin');
        $prod->zalogowany_jako = $info->zalogowany_jako;
        $res = $prod->selectAllAjax($query);
        
        for($i=0;$i<count($res);$i++)
		{
            $sugestie .= "'".$res[$i]["nazwisko"]."',";
        }
        
        $sugestie .= "'".$query."'";
        
        $zwrot = "
		{
            query: '".$query."',
            suggestions: [".$sugestie."],
            data: ['LR','LY','LI','LT']
        }"; 

        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT' );
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . 'GMT');
        header('Cache-Control: no-cache, must-revalidate');
        header('Pragma: no-cache');
        header('Content-Type: text/xml');
        echo $zwrot;
    }
	
	function autocompleteklientAction()
	{
        $this->_helper->viewRenderer->setNoRender();
        $query = $this->_request->getParam('term', '');
        
        $klienci = new Kontrahenci();
        $res = $klienci->szukaj($query);
        
		$echoes = array();
        for($i=0;$i<count($res);$i++)
		{
			//echo $res[$i]['nazwa_firmy'].strpos($res[$i]['nazwa_firmy'], $query).$query;
            //$echoes[] = trim("".$res[$i]["nazwa_firmy"]." ".$res[$i]["nazwa"]." ".$res[$i]["nazwisko"]."");
			if(stripos($res[$i]['nazwa_firmy'], $query) !== false)
			{
				if(!isset($rows[$res[$i]['nazwa_firmy']]))
				{
					$echoes[] = stripslashes($res[$i]['nazwa_firmy']);
					$rows[$res[$i]['nazwa_firmy']] = true;
				}
			}
			if(stripos($res[$i]['nazwisko'], $query) !== false)
			{
				if(!isset($rows[$res[$i]['nazwisko']]))
				{
					$echoes[] = stripslashes($res[$i]['nazwisko']);
					$rows[$res[$i]['nazwisko']] = true;
				}
			}
			if(stripos($res[$i]['nazwa'], $query) !== false)
			{
				if(!isset($rows[$res[$i]['nazwa']]))
				{
					$echoes[] = stripslashes($res[$i]['nazwa']);
					$rows[$res[$i]['nazwa']] = true;
				}
			}
			if(count($echoes) >= 15) break;
        }
        echo json_encode($echoes);
    }
	
	function szukajklientAction()
	{
        $this->_helper->viewRenderer->setNoRender();
        $query = $this->_request->getParam('term', '');
        //echo $query;die();
        $klienci = new Kontrahenci();
        $result = $klienci->szukaj($query, 10);
        
		$odpowiedz = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
		$odpowiedz.= '<root>';
		//$odpowiedz.= '<klient><id>0</id><nazwa_firmy>producenci</nazwa_firmy></klient>';
		if(count($result) > 0)
		{
			foreach($result as $row)
			{
				$odpowiedz.= '<klient>';
				$odpowiedz.= '<id>'.$row['id'].'</id>';
				$odpowiedz.= '<nazwa_firmy>'.stripslashes($row['nazwa_firmy']).'</nazwa_firmy>';
				$odpowiedz.= '<nazwisko>'.stripslashes($row['nazwisko']).'</nazwisko>';
				$odpowiedz.= '<imie>'.stripslashes($row['imie']).'</imie>';
				$odpowiedz.= '</klient>';
			}
		}
		//else $odpowiedz.= '<klient><id>0</id><nazwa_firmy>Brak wyników wyszukiwania</nazwa_firmy></klient>';
		$odpowiedz.= '</root>';
		echo $odpowiedz;
    }
	
	function szukajprodAction()
	{
		$this->_helper->viewRenderer->setNoRender();

        $q = $this->_request->getParam('q', '');
		
        $produkty = new Produkty();
        $result = $produkty->wyszukajslowo2($q);

        for($i=0;$i<count($result);$i++)
		{
			$id = $result[$i]['id'];
			$kod = $result[$i]['oznaczenie'];
			if(!empty($kod) && stripos($kod, $q) !== false) $echo[$id] = stripslashes($kod);
			$nazwa = $result[$i]['nazwa'];
			if(!empty($nazwa) && stripos($nazwa, $q) !== false) $echo[$id] = stripslashes($nazwa);
			//$producent = $result[$i]['producent'];
			//if(!empty($producent) && stripos($producent, $q) !== false) $echo[$producent] = $producent;
        }
		
		$odpowiedz = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
		$odpowiedz.= '<root>';
		if(isset($echo))
		{
			foreach($echo as $e => $ech)
			{
				$odpowiedz.= '<prod>';
				$odpowiedz.= '<id>'.$e.'</id>';
				$odpowiedz.= '<nazwa>'.stripslashes($ech).'</nazwa>';
				$odpowiedz.= '</prod>';
			}
		}
		else $odpowiedz.= '<prod><id>0</id><nazwa>Brak wyników wyszukiwania</nazwa></prod>';
		$odpowiedz.= '</root>';
		echo $odpowiedz;
		//echo "$nazwa|$nazwa\n";
    }
	
	function szukajgratisAction()
	{
		$this->_helper->viewRenderer->setNoRender();

        $q = $this->_request->getParam('q', '');
		
        $partnerzy = new Partnerzy();
        $result = $partnerzy->znajdz($q, "gratisy");

		$odpowiedz = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
		$odpowiedz.= '<root>';
		if(count($result) > 0)
		{
			foreach($result as $row)
			{
				$odpowiedz.= '<gratis>';
				$odpowiedz.= '<id>'.@intval($row['id']).'</id>';
				$odpowiedz.= '<nazwa>'.@stripslashes($row['nazwa']).'</nazwa>';
				$odpowiedz.= '</gratis>';
			}
		}
		else $odpowiedz.= '<gratis><id>0</id><nazwa>Brak wyników wyszukiwania</nazwa></gratis>';
		$odpowiedz.= '</root>';
		echo $odpowiedz;
		//echo "$nazwa|$nazwa\n";
    }
	
	function newsletterAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		
        $mail = new Subskrypcja();
        $result = $mail->wypisz('pl');

        for($i=0;$i<count($result);$i++)
		{
			$nazwa = $result[$i]['email'];
			echo "$nazwa\n";
        }
    }
	
	function listaAction() 
	{
		$this->view->headLink()->appendStylesheet($this->view->baseUrl.'/public/styles/admin.css');
        //$this->_helper->viewRenderer->setNoRender();
		//return;
		
		$id = intval($this->_request->getParam('id', 0));
		$typ = $this->_request->getParam('typ', '');
		
		$action = $this->_request->getParam('typ', '');
		$action = strtolower($action);
		
		$search = $this->_request->getParam('search', false);
		$norender = $this->_request->getParam('norender', false);
		$namespace = $norender ? '' : 'Strona';
		
		$reset = $this->_request->getParam('reset', false);
		$cenaMax = floatval($this->_request->getParam('cenaMax', 0));
		
		$szukanie = new Zend_Session_Namespace('szukanie'.$namespace);		
		$sortowanie = new Zend_Session_Namespace('sortowanie'.$namespace);		
		
		if(!$norender || $search || $typ)
		{
			$szukanie->nazwa = $this->_request->getParam('nazwa', '');
			$szukanie->oznaczenie = $this->_request->getParam('kod', '');
			
			$szukanie->cena_od = floatval($this->_request->getParam('cena_od', 0));
			$szukanie->cena_do = floatval($this->_request->getParam('cena_do', 0));
			$szukanie->cena_netto = floatval($this->_request->getParam('cena_netto', -1));
			$szukanie->cena_brutto = floatval($this->_request->getParam('cena_brutto', -1));
			
			$szukanie->grupa = $this->_request->getParam('grupa', 'all');
			
			$szukanie->rozmiar = $this->_request->getParam('rozm', 'all');
			if($szukanie->rozmiar != 'all') $szukanie->wkladka = 'all';
			
			$szukanie->wkladka = $this->_request->getParam('wkl', 'all');
			if($szukanie->wkladka != 'all') $szukanie->rozmiar = 'all';
			
			$szukanie->kolor = $this->_request->getParam('kolor', -1);
			$szukanie->producent = $this->_request->getParam('prod', -1);
			
			$szukanie->katMain = $this->_request->getParam('kateg', -1);
			$szukanie->podkat1 = $this->_request->getParam('podkat1', -1);
			$szukanie->podkat2 = $this->_request->getParam('podkat2', -1);
			$szukanie->podkat3 = $this->_request->getParam('podkat3', -1);
			$szukanie->kateg = $szukanie->podkat3>0?$szukanie->podkat3:
				($szukanie->podkat2>0?$szukanie->podkat2:($szukanie->podkat1>0?$szukanie->podkat1:$szukanie->katMain));
			
			if(!$norender || $typ)
			{
				$sortowanie->spec = null;
				$nowosc = $this->_request->getParam('nowosc', false);
				$promocja = $this->_request->getParam('promocja', false);
				$polecane = $this->_request->getParam('polecane', false);
				if($nowosc === 'true') $sortowanie->spec['nowosc'] = 'on';
				if($promocja === 'true') $sortowanie->spec['promocja'] = 'on';
				if($polecane === 'true') $sortowanie->spec['polecane'] = 'on';
				$szukanie->nowosc = ($action === 'nowosci');
				$szukanie->promocja = ($action === 'promocje');
				$szukanie->polecane = ($action === 'polecane');
			}
			//var_dump($typ);var_dump($szukanie->promocja);//return;
		}
		
		if($typ) $szukanie->typ = $typ; else $typ = @$szukanie->typ;
		
		$strona = intval($this->_request->getParam('od', 0));		
		$ile = intval($this->_request->getParam('ile', 25));
		$szukanie->ile = $ile = ($ile == 'all' ? 99999 : $ile);
		$szukanie->od = $od = $strona * $ile;

		if($reset || !isset($szukanie->nazwa))
		{
			$szukanie->nazwa = '';
			$szukanie->oznaczenie = '';
			$szukanie->grupa = 'all';
			$szukanie->rozmiar = 'all';
			$szukanie->wkladka = 'all';
			$szukanie->producent = -1;
			$szukanie->kolor = -1;
			$szukanie->kolory = array(''=>'');
			$szukanie->cena_od = 0;
			$szukanie->cena_do = $cenaMax;
			$szukanie->cena_netto = -1;
			$szukanie->cena_brutto = -1;
			$szukanie->katMain = -1;
			$szukanie->podkat1 = -1;
			$szukanie->podkat2 = -1;
			$szukanie->podkat3 = -1;
			$szukanie->kateg = -1;
			
			$strona = 0;
			$szukanie->od = $od = 0;
			$szukanie->ile = $ile = 25;
			
			$sortowanie->sort = 'nazwa';
			$sortowanie->order = 'asc';
			$sortowanie->specjalne = '';
		}
		
		$sortowanie->sort = $this->_request->getParam('sortuj', $sortowanie->sort);
        $sortowanie->order = $this->_request->getParam('order', $sortowanie->order);
		
		if($szukanie->kateg != -1) $id = $szukanie->kateg;
		if($id <= 0 && $id != -2)
		{
			$produkty = new Produkty();
			$ilosc = $produkty->wypiszProduktyPodzieloneStrona(true, $strona, $ile, $sortowanie, $szukanie);  
			$all = $produkty->wypiszProduktyPodzieloneStrona(false, $strona, $ile, $sortowanie, $szukanie);
			$this->view->link = 'Oferta';
		}
		else
		{
			$produkty = new Katprod();
			$produkty->id = $id;
			$ilosc = $produkty->selectProdukt($strona, $ile, $sortowanie, $szukanie, true);
			$all = $produkty->selectProdukt($strona, $ile, $sortowanie, $szukanie, false);
			if($id > 0)
			{
				$kategorie = new Kategorie();
				$kategoria = $kategorie->showWybranaKategoria($id);
				$this->view->link = $kategoria['link'];
			}
		}
		$strony = ceil($ilosc / $ile);
		$all = $this->produktyGaleria($all);

		$this->view->szukanie = $szukanie;
        $this->view->sortowanie = $sortowanie;
        $this->view->rows = $all;
        $this->view->all = $all;
		$this->view->ilosc = $strony;
        $this->view->strony = $strony;
        $this->view->strona = $strona;
		$this->view->ile = $ile;
		$this->rowscount = $ile;
		$this->view->od = $od;
		$this->view->typ = $typ;
		if($norender)
		$this->view->link = $this->obConfig->www.'/ajax/index/lista/norender/1';///typ/'.$typ;
		$this->view->sortuj = isset($sortowanie->sort) ? $sortowanie->sort : 'nazwa';
        $this->view->order = $sortowanie->order == 'asc' ? 'desc' : 'asc';
		$this->view->kateg = $szukanie->kateg;
		
		if($norender)
		{
			$producenci = new Producent();
			$this->view->producenci = $producenci->wypiszProducentow();
			
			$kolor = new Kolory();
			$this->view->kolory = $kolor->wypiszKolory();
			
			$produktyGrupy = new Produktygrupy();
			$this->view->grupy = $produktyGrupy->wypisz();
			
			$kategoria = new Kategorie();
			$this->view->katMain = $kategoria->wypiszDzieci(0);
			$this->view->podkat1 = $kategoria->wypiszDzieci($szukanie->katMain);
			$this->view->podkat2 = $kategoria->wypiszDzieci($szukanie->podkat1);
			$this->view->podkat3 = $kategoria->wypiszDzieci($szukanie->podkat2);
		}
		
		$this->_helper->viewRenderer->setNoRender();
		
		if($norender) $this->render('getlista');		
		else
		{
			$viewRenderer = $this->_helper->viewRenderer;
			//$viewRenderer->setViewBasePathSpec(APPLICATION_PATH.'/default/views')->initView(); 
			$frontControllerDir = $this->getFrontController()->getControllerDirectory('default');
			$this->view->addBasePath(realpath($frontControllerDir.'/../views'));
			$this->_helper->layout->disableLayout();
			//$this->view->addScriptPath('../../../../');
			$render = array('module' => 'default', 'controller' => 'const');
			$viewRenderer->renderBySpec('item', $render);
		}	
    }
	
	function allegroAction() 
	{
		$this->_helper->viewRenderer->setNoRender();		
		
		$this->konto = new Allegrokonta();
		$this->konto->ID = 1;
		$this->config = $this->konto->klient();
		$this->view->config = $this->config;
		
		//define('ALLEGRO_ID', 'id');
		@define('ALLEGRO_LOGIN', $this->config['login']);
		@define('ALLEGRO_PASSWORD', $this->config['haslo']);
		@define('ALLEGRO_KEY', $this->config['klucz_webapi']);
		@define('ALLEGRO_COUNTRY', 1);
		
		try
		{
			$this->webapi = new AllegroWebAPI();
			$this->webapi->Login();
			@define('ALLEGRO_ID', $this->webapi->GetUserID($this->config['login']));
			$this->webapi = new AllegroWebAPI();
			$this->webapi->Login();
			$this->allegro = new AllegroClientSoap($this->config, null);
		}
		catch(SoapFault $error)
		{
			$this->view->error = 'Błąd '.$error->faultcode.': '.$error->faultstring."";
		}
		catch(Exception $error)
		{
			$this->view->error = 'Błąd '.$error->faultcode.': '.$error->faultstring."";
		}
		
		$produkty = new Produkty();
		
		$point = 1;
		if(isset($this->view->error)) echo 'blad: '.$this->view->error;
		else
		while($point != null)
		{
			$zmiany = $this->webapi->GetSiteJournal(array('starting-point' => $point, 'info-type' => 0));
			$zmiany = $this->webapi->objectToArray($zmiany);
			//var_dump($zmiany);
			
			$point = (count($zmiany) == 100) ? $zmiany[99]['row-id'] : null;
			echo 'zmian w aukcjach: '.count($zmiany)."<br/>\n";
			
			$rozmiary = $this->tryb == 'rozmiary' ? new Rozmiarproduktu() : new Produkty();
			foreach($zmiany as $item)
			try
			{
				set_time_limit(60);
				
				$status = $item['change-type'];
				$data = $item['change-date'];
				if(time() - $data > 25 * 3600) continue;
				echo 'zmiana w aukcji: '.$item['item-id'].' - '.date('Y-m-d', $data).' - '.$status."<br/>\n";
				$opcje = array('item-id' => $item['item-id'], 'get-desc' => 1, 'get-image-url' => 0,
						'get-attribs' => 0, 'get-postage-options' => 0, 'get-company-info' => 0);
				$info = $this->webapi->objectToArray($this->webapi->ShowItemInfoExt($opcje));
				//var_dump($info);
				$aukcjaIloscAllegro = $info['item-list-info-ext']['it-quantity'];
				$allegro = new Allegroaukcje();
				$aukcja = $allegro->szukajAukcji($item['item-id']);
				if($aukcja != null && count($aukcja) > 0)
				{
					echo 'aukcja istnieje: '.$item['item-id']."<br/>\n";
					if($this->ustawienia['stany_mag'])
					if($aukcjaIloscAllegro < $aukcja['sztuk_pozostalo'])
					{						
						$produkty->id = $aukcja['id_prod'];
						$produkt = $produkty->wypiszPojedyncza();
						$rozmiary = $produkt['rozmiarowka'] > 0 ? new Rozmiarproduktu() : new Produkty();
						$rozmiary->id = $produkt['rozmiarowka'] > 0 ? $aukcja['id_rozm'] : $aukcja['id_prod'];
						$rozmiar = $rozmiary->wypiszJeden();
						$roznica = intval($aukcja['sztuk_pozostalo']) - intval($aukcjaIloscAllegro);
						$nowaIlosc = max(0, intval($rozmiar[0]['dostepnosc']) - intval($roznica));
						$allegro->id = $aukcja['id'];
						$allegro->zmien(array('sztuk_pozostalo' => $aukcjaIloscAllegro));
						$rozmiary->zmien(array('dostepnosc' => $nowaIlosc));
						echo 'zmiana ilosci sztuk: '.$aukcja['sztuk_pozostalo'].'-'.$aukcjaIloscAllegro."<br/>\n";
					}
					if($status == 'end')
					{
						$allegro = new Allegroaukcje();
						$allegro->usunAukcje($item['item-id']);
						echo 'koniec aukcji: '.$item['item-id']."<br/>\n";
					}
				}
				else if($status != 'end')//$status == 'start')
				{
					$opis = $info['item-list-info-ext']['it-description'];
					//$opis = '<div style="display:none;">SKU=23=SKU</div>';
					//var_dump($info['item-list-info-ext']);
					//echo 'opis='.$opis."<br/>\n";
					$start = strpos($opis, 'PRODID=') + 7;
					$end = strpos($opis, '=PRODID');
					$prodID = substr($opis, $start, $end - $start);
					$start = strpos($opis, 'ROZMID=') + 7;
					$end = strpos($opis, '=ROZMID');
					$rozmID = substr($opis, $start, $end - $start);
					echo 'PRODID='.substr($prodID, 0, 10)."<br/>\n";
					echo 'ROZMID='.substr($rozmID, 0, 10)."<br/>\n";
					if($prodID === false || intval($prodID) == 0) continue;
					if($rozmID === false) continue;
					
					$dane['id_prod'] = intval($prodID);
					$dane['id_rozm'] = intval($rozmID);
					$dane['nr'] = $item['item-id'];
					$dane['sztuk_wystawiono'] = $info['item-list-info-ext']['it-quantity'];
					$dane['sztuk_pozostalo'] = $info['item-list-info-ext']['it-quantity'];
					$wystawiono = $info['item-list-info-ext']['it-starting-time'];
					$koniec = $info['item-list-info-ext']['it-ending-time'];
					$dane['wystawiono'] = date('Y-m-d H:i:s', $wystawiono);
					$dane['na_ile'] = intval(date('d', $koniec - $wystawiono) - 1);
					$allegro = new Allegroaukcje();
					$allegro->dodaj($dane);
					//var_dump($info);
					//var_dump($dane);
					echo 'dodano aukcje: '.$item['item-id']."<br/>\n";
				}
			}
			catch(SoapFault $error)
			{
				echo 'Błąd '.$error->faultcode.': '.$error->faultstring."<br/>\n";
			}
		}
    }
	
	function parseAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		
		mb_internal_encoding("UTF-8");
		
		$confmail = new Confmail();
        $conf = $confmail->showData();
		
		//$mail = new Zend_Mail_Storage_Pop3
		$mail = new Zend_Mail_Storage_Imap(array('host' => $conf[0]->POP3,
												'user'  => $conf[0]->Username,
											 'password' => $conf[0]->Password,
											 'port'     => 993,
											 'ssl' => 'SSL'));
     
		$ile = $mail->countMessages();
		echo $ile." messages found<br/>\n";
		
		if(false)
		foreach($mail as $messageNum => $message)
		try
		{
			set_time_limit(60);
			echo 'Mail '.$messageNum.' from '.$message->from.': '.$message->subject."<br/>\n";
		}
		catch(Zend_Mail_Exception $e)
		{
			echo 'Blad z mailem '.$messageNum.' - '.$e."<br/>\n";
		}
		
		for($i=$ile;$i>=1;$i--)
		try
		{
			set_time_limit(60);
			//$mail->noop();
			$message = $mail->getMessage($i);
			$date = $message->getHeader('date', 'string');
			if(time() - strtotime($date) > 1 * 24 * 3600) break;
			if(strpos($message->from, 'Allegro') !== false)
			if(strpos($message->subject, 'Oferta') !== false)
			{
				echo "<br/>\n";
				$content = utf8_decode($message->getContent());
				//echo $content;
				
				$start = strpos($content, '<a href="mailto:');
				$length = strpos(substr($content, $start), '">');
				//echo $start.' '.$length.' '.$message->contentType.'<br/>';
				$email = substr($content, $start + 16, $length - 16);
				echo $email."<br/>\n";
				
				$start = strpos($content, '(numer: ');
				$id = substr($content, $start + 8, 10);
				echo $id."<br/>\n";
				
				$find = 'show_user.php?uid=';
				$start = strpos($content, $find);
				$length = strpos(substr($content, $start), '">');
				$uidAll = substr($content, $start + strlen($find), $length);
				$uid = substr($uidAll, 0, 8);
				echo $uid."<br/>\n";
				$login = substr($uidAll, strpos($uidAll, '>') + 1);
				$login = substr($login, 0, strpos($login, '</a>'));
				echo $login."<br/>\n";
				
				$untilName = substr($content, 0, $start);
				$start = strrpos($untilName, '>');
				$end = strrpos($untilName, '<');
				$name = substr($untilName, $start + 1, $end - $start);
				$name = explode(' ', $name);
				$imie = $name[0];
				$nazw = $name[1];
				echo $imie."<br/>\n";
				echo $nazw."<br/>\n";
				
				$start = strpos($content, 'Telefon');				
				$sinceTel = substr($content, $start);
				$start = strpos($sinceTel, '">');
				$tel = substr($sinceTel, $start + 2, 9);
				echo $tel."<br/>\n";
				
				//$subject = explode('-', $message->subject);
				//$id = str_replace('_', '', trim($subject[2]));
				
				$sub = new Subskrypcja();
				$exist = $sub->sprMail($email);
				if(empty($exist))
				{
					$dane['email'] = $email;
					$dane['imie'] = $imie;
					$dane['nazwisko'] = $nazw;
					$dane['telefon'] = $tel;
					$dane['allegro_id'] = $uid;
					$dane['allegro_login'] = $login;
					$dane['allegro_aukcje'] = $id.';';
					$dane['lang'] = 'pl';
					$dane['status'] = 1;
					$sub->dodajAll($dane);
					echo "dodano osobe<br/>\n";
				}
				else
				{
					echo "osoba juz istnieje<br/>\n";
					if(strpos($exist[0]['allegro_aukcje'], $id) === false)
					{
						$sub->edytuj($exist[0]['id'], 
							array('allegro_aukcje' => $exist[0]['allegro_aukcje'].$id.';'));
						echo "dodano aukcje<br/>\n";
					}
					else echo "aukcja juz istnieje<br/>\n";
				}
			}
			//var_dump($message);
		}
		catch(Zend_Mail_Exception $e)
		{
			echo 'Blad z mailem '.$i.' - '.$e."<br/>\n";
		}
	}
	
	function kopiujonlineAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		echo 'Wynik działania online:<br/>';
		//var_dump($_POST);
		$ile = 0;
		$ileExist = 0;
		$ileDelete = 0;
		foreach($_POST as $idOld => $prod)
		{
			//var_dump($prod);
			$id = $prod['id'];
			//unset($prod['id']);
			unset($prod['link']);
			unset($prod['route_id']);
			unset($prod['data']);
			$kats = $prod['kats'];
			unset($prod['kats']);
			$rozm = $prod['rozm'];
			unset($prod['rozm']);
			$kols = $prod['kols'];
			unset($prod['kols']);
			$imgs = $prod['imgs'];
			unset($prod['imgs']);
			
			$produkt = new Produkty();
			$szukaj = $produkt->szukaj($id);
			$exist = false;
			if($szukaj != null && count($szukaj) > 0)
			{
				//echo 'Produkt '.$prod['nazwa'].' już istnieje w panelu online<br/>';
				$exist = true;				
				$produkt->id = $id;
				if($prod['zmiana'] == NULL)
				{
					$produkt->usunProdukty();
					$kat = new Katprod();
					$kat->id = $id;
					$kat->deleteProdukt();
					$img = new Galeria();
					$img->usunGalerie($id);
					$ileDelete++;
					continue;
				}
				else 
				{
					$prod['zmiana'] = '0000-00-00';
					$produkt->edytujProdukty($prod);
					$ileExist++;
				}
			}
			else 
			{
				$ile++;
				$prod['zmiana'] = '0000-00-00';
				$id = $produkt->dodajProdukty($prod);				
				$route = new Routers();
				$sprawdz_link = $route->dodaj($prod['nazwa'].'-'.$id, $id);
				$data['link'] = $sprawdz_link['link'];
				$data['route_id'] = $sprawdz_link['id'];
				$produkt->id = $id;
				$produkt->edytujProdukty($data);
			}			
			
			$kat_prod = new Katprod();
			$kat_prod->id = $id;
			$kat_prod->deleteProdukt();
			if(isset($kats) && count($kats) > 0)
			foreach($kats as $kat)
			$kat_prod->add($kat['id_kat'], $id);
			
			$rozmiar = new Rozmiarproduktu();
			$rozmiar->usunProdukt($id);
			if(isset($rozm) && count($rozm) > 0)
			foreach($rozm as $roz)
			{
				unset($roz['id']);
				$roz['id_produktu'] = $id;
				$rozmiar->dodaj($roz);
			}
			
			$kolor = new Kolorproduktu();
			$kolor->usunProdukt($id);
			if(isset($kols) && count($kols) > 0)
			foreach($kols as $kol)
			{
				unset($kol['id']);
				$kol['id_produktu'] = $id;
				$kolor->dodaj($kol);
			}
			
			$galeria = new Galeria();
			$galeria->usunGalerie($id);
			if(isset($imgs) && count($imgs) > 0)
			foreach($imgs as $img)
			{
				unset($img['id']);
				$img['wlasciciel'] = $id;
				//$img['img'] = str_replace('_', '-', $img['img']);
				$galeria->dodaj($img);
			}
			
			//echo 'Dodano produkt '.$prod['nazwa'].'<br/>';
		}
		echo 'Zmieniono '.$ileExist.' produktów<br/>';
		echo 'Usunięto '.$ileDelete.' produktów<br/>';
		echo 'Dodano '.$ile.' produktów<br/>';
	}
	
	function synchronizujAction() 
	{
		$this->_helper->viewRenderer->setNoRender();

        $tab = $this->_request->getParam('tab');
		switch($tab)
		{
			case 'kategorie':
			$techn = new Kategorie();
			$producenci = $techn->wypiszKategorie();
			break;
			case 'katprod':
			$techn = new Katprod();
			$producenci = $techn->wypiszAll();
			break;
			case 'produkt':
			$techn = new Produkty();
			$producenci = $techn->wypiszProdukty();
			break;
			case 'galeria':
			$techn = new Galeria();
			$producenci = $techn->wypiszAll();
			break;
			case 'rozmprod':
			$techn = new Rozmiarproduktu();
			$producenci = $techn->wypiszAll();
			break;
			case 'kolprod':
			$techn = new Kolorproduktu();
			$producenci = $techn->wypiszAll();
			break;
			case 'producent':
			$techn = new Producent();
			$producenci = $techn->wypiszProducentow();
			break;
			case 'kolory':
			$techn = new Kolory();
			$producenci = $techn->wypiszKolory();
			break;			
			case 'rozmiary':
			$techn = new Rozmiary();
			$producenci = $techn->wypiszKolory();
			break;
		}
		//var_dump($producenci);
		//if(isset($producenci))
		if($tab == 'katprod') echo http_build_query($producenci);
		else echo http_build_query($producenci->toArray());
    }
	
	function testAction()
	{
		set_time_limit(10);
		$i=0;
		while(true)$i++;
	}
	
	function wystawprodAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		
		$this->view->baseUrl = $this->_request->getBaseUrl();
		
		$this->konto = new Allegrokonta();
		$this->konto->ID = $konto = $this->_request->getParam('konto', 1);
		$this->view->config = $this->config = $this->konto->klient();
		
		$raporty = new Allegroraport();
		
		$dostepne = $this->_request->getPost('dostepne', $this->config['dostepne']);
		$istniejaca = $this->_request->getPost('istniejaca', $this->config['istniejaca']);
		$dni = $this->_request->getPost('dni', $this->config['dni']);
		$minut = $this->_request->getPost('minut', $this->config['minut']);
		$minut = max(0, floor(abs(intval($minut)) / 5) * 5);
		$sztuk = $this->_request->getPost('sztuk', $this->config['sztuk']);
		$sztukowanie = $this->_request->getPost('sztukowanie', $this->config['sztukowanie']);
		$wystawionoAll = intval($this->_request->getPost('wystawiono', 0));
		$data = floatval($this->_request->getPost('data', time()));
		
		//$this->config['login'] = 'james890_test';
		//$this->config['haslo'] = 'allegro1';
		//define('ALLEGRO_COUNTRY', 228);
		//define('ALLEGRO_ID', 'id');
		@define('ALLEGRO_LOGIN', $this->config['login']);
		@define('ALLEGRO_PASSWORD', $this->config['haslo']);
		@define('ALLEGRO_KEY', $this->config['klucz_webapi']);
		@define('ALLEGRO_COUNTRY', 1);
		
		try
		{
			$this->webapi = new AllegroWebAPI();
			$this->webapi->Login();
			@define('ALLEGRO_ID', $this->webapi->GetUserID($this->config['login']));
			$this->webapi = new AllegroWebAPI();
			$this->webapi->Login();
			$this->allegro = new AllegroClientSoap($this->config, null);
		}
		catch(SoapFault $error)
		{
			echo '<Error>Błąd '.$error->faultcode.': '.$error->faultstring."</Error></root>";
			$raport['status'] = 'Błąd '.$error->faultcode.': '.$error->faultstring.'';
			$raporty->dodaj($raport);
			exit();
		}
		catch(Exception $error)
		{
			echo '<Error>Błąd '.$error->faultcode.': '.$error->faultstring."</Error>";
		}
		
		$id = $this->_request->getParam('prodID', 0);
		//echo 'ID:'.$id;
		$wystaw = new Zend_Session_Namespace('wystawLista');
		
		if(intval($id) > 0)
		{
			$wystawTeraz = $this->_request->getParam('wystaw', "NIE") === "TAK";
			$produkt = new Produkty();
			$produkt->id = $id;
			$produkty = $produkt->wypiszPojedyncza();
			$lista = array($produkty['id'] => $produkty['nazwa']);
		}
		if(!isset($lista)) return;
		
		if(count($lista) == 0)
		{
			echo '<Error>Nie wybrano produktów do wystawienia</Error>';
			$raport['status'] = 'Nie wybrano produktów do wystawienia';
			$raporty->dodaj($raport);
		}
		else
		{
			$ileProd = 0;
			$ileProdPominietoKategorie = 0;
			$ileProdPominietoProdukt = 0;
			$ileProdPominietoRozmiary = 0;
			$ileProdPominietoOpis = 0;
			$ileProdPominietoCena = 0;
			$ileProdPominietoCenaRozmiar = 0;
			$ileProdPominietoJestAukcja = 0;
			$ileProdPominietoJestAukcjaAll = 0;
			$ileProdPominietoBladAllegro = 0;
			$ileAukcji = 0;
			$ileAukcjiAll = 0;
			$kosztAll = 0;
			$wystawiono = 0;

			$produkty = new Produkty();
			$allegro = new Allegroaukcje();
			
			$listaProd = $lista;
			foreach($listaProd as $id => $nazwa)
			{
				set_time_limit(60);
				
				$ileProdPominietoJestAukcja = 0;			
				
				$produkty->id = $id;
				$produkt = $produkty->wypiszPojedynczaAll();
				$produktyAll = $this->produktyGaleria(array($produkt), 0, false, null, true);
				$produktAll = @reset($produktyAll); // $produktAll[0];
				//var_dump($produktAll);die();
				
				$tryb = $this->allegroTryb;
				if($tryb == 'rozmiary' && $produkt['rozmiarowka'] == 0) $tryb = 'produkt';
				$trybCena = $this->allegroTrybCena;
				if($tryb != 'atrybuty' && $produkt['rozmiarowka'] >= 0) $trybCena = $produkt['trybCena'];
				
				$rozmiary = $trybCena == 'atrybuty' ? new Atrybutypowiazania() : new Rozmiarproduktu();
				$rozmiary->id = $id;
				$rozmiarIle = null;
				
				$raport['id_prod'] = $id;
				$raport['id_rozm'] = 0;
				$raport['nazwa'] = $produkt['nazwa'];
				$raport['koszt'] = 0;				
				
				if($trybCena == 'rozmiary')
				{
					$cenaMin = $rozmiary->wypiszCena($produkt['rozmiarowka'], $produkt['promocja'] ? 'cena_promocji_b' : 'cena_brutto', 'max');
					if(count($cenaMin) > 0)	$cena = $cenaMin[0]['cena'];
				}
				else if($trybCena == 'atrybuty')
				{
					$cena = $produkt['promocja'] ? $produkt['cena_promocji_b'] : $produkt['cena_brutto'];
				}
				else if($trybCena == 'produkt')
				{
					$cena = $produkt['promocja'] ? $produkt['cena_promocji_b'] : $produkt['cena_brutto'];
					if($this->obConfig->allegroCena && $produkt['cena_allegro'] > 0) $cena = $produkt['cena_allegro'];
				}
				if(!isset($cena) || $cena == 0)
				{
					$ileProdPominietoCena++;
					$raport['status'] = 'Brak ceny';
					$raporty->dodaj($raport);
					continue;
				}
				if($produkt['rabat'] > 0) $cena = ($cena * (100.00 - $produkt['rabat'])) / 100;
				
				if(false)
				if(empty($produkt['tekst']))
				{
					$ileProdPominietoOpis++;
					$raport['status'] = 'Brak opisu';
					$raporty->dodaj($raport);
					continue;
				}
				
				$rozmiar = null;
				if($tryb != 'produkt') $rozmiar = $rozmiary->wypiszAukcje();
				else if($tryb == 'produkt') $rozmiar = $produkty->wypiszAukcje();
				
				if(count($rozmiar) > 0)
				foreach($rozmiar as $rozm)
				{
					set_time_limit(60);
					//$dostepne = 'pomin';$sztuk = 1;
					
					if(!isset($rozm['dostepny'])) $rozm['dostepny'] = $produkt['dostepny'];
					if(!isset($rozm['dostepnosc'])) $rozm['dostepnosc'] = $produkt['dostepnosc'];
					
					if($tryb == 'rozmiary')
					{
						//$sumaDost = $rozmiary->wypiszSumaDostepnosc($produkt['rozmiarowka']);
						//if(count($sumaDost) > 0) $rozm['dostepnosc'] = $sumaDost[0]['sumadost'];
						$raport['id_rozm'] = $rozm['rp.id'];
					}
					
					if(@$_GET['xxx']){echo'<pre>';var_dump($rozm);'</pre>';}
					if(!isset($aukcjeIloscAllegro[$rozm['rp.id']])) $aukcjeIloscAllegro[$rozm['rp.id']] = 0;
					if($rozm['nr'] != NULL)
					try
					{
						$opcje = array('item-id' => $rozm['nr'], 'get-desc' => 0, 'get-image-url' => 0,
							'get-attribs' => 0, 'get-postage-options' => 0, 'get-company-info' => 0);
						$info = $this->webapi->objectToArray($this->webapi->ShowItemInfoExt($opcje));
						$aukcjaIloscAllegro = $info['item-list-info-ext']['it-quantity'];
						if($this->ustawienia['stany_mag'])
						if($aukcjaIloscAllegro < $rozm['sztuk_pozostalo'])
						{
							$roznica = intval($rozm['sztuk_pozostalo']) - intval($aukcjaIloscAllegro);
							$nowaIlosc = max(0, intval($rozm['dostepnosc']) - intval($roznica));
							$allegro->id = $rozm['a.id'];
							$allegro->zmien(array('sztuk_pozostalo' => $aukcjaIloscAllegro));
							$nowyRozmiar = $tryb == 'rozmiary' ? new Rozmiarproduktu() : new Produkty();
							$nowyRozmiar->id = $rozm['rp.id'];
							$nowyRozmiar->zmien(array('dostepnosc' => $nowaIlosc));
						}
						if($aukcjaIloscAllegro > 0)
						{
							$aukcjeProduktu[$id][] = array
							(
								'item-id' => $rozm['nr'], 
								'nazwa' => $rozm['nazwa'], 
								'wkladka' => isset($rozm['wkladka']) ? $rozm['wkladka'] : ''
							);
						}
						if(@$_GET['xxx']){echo'<pre>';var_dump($info);'</pre>';}
						$aukcjeIloscAllegro[$rozm['rp.id']] += $aukcjaIloscAllegro;
					}
					catch(SoapFault $error)//if ($error instanceof WSFault
					{
						if($error->faultcode == 'ERR_INVALID_ITEM_ID')
						{
							$allegro->usunAukcje($rozm['nr']);
							continue;
						}
						echo '<Error>Błąd #1 przy przetwarzaniu produktu '.$nazwa.' - '.$error->faultcode.': '.$error->faultstring."</Error>";
						break;
					}
					
					if($istniejaca == 'pomin' && $aukcjeIloscAllegro[$rozm['rp.id']] > 0)
					{
						$ileProdPominietoJestAukcja++;
						$ileProdPominietoJestAukcjaAll++;
						$raport['status'] = 'Aukcja już istnieje';
						$raporty->dodaj($raport);
						continue;
					}
					if($istniejaca == 'min' && $aukcjeIloscAllegro[$rozm['rp.id']] >= $sztuk)
					{
						$ileProdPominietoJestAukcja++;
						$ileProdPominietoJestAukcjaAll++;
						$raport['status'] = 'Aukcja już istnieje';
						$raporty->dodaj($raport);
						continue;
					}
					
					if($dostepne == 'wystawAll')
					{
						$rozmiaryDoWystawienia[$id][$rozm['rp.id']] = $sztuk;
						$rozmiarIle[$rozm['rp.id']] = true;
					}
					else if($dostepne == 'wystawDost')
					{
						if($rozm['dostepny'])
						{
							$rozmiaryDoWystawienia[$id][$rozm['rp.id']] = $sztuk;
							$rozmiarIle[$rozm['rp.id']] = true;
						}
					}
					else if($rozm['dostepnosc'] > 0)
					{
						if($dostepne == 'wystawIle')
						{
							$rozmiaryDoWystawienia[$id][$rozm['rp.id']] = $rozm['dostepnosc'];
							$rozmiarIle[$rozm['rp.id']] = true;
						}
						if($dostepne == 'pomin' && $rozm['dostepnosc'] >= $sztuk)
						{
							$rozmiaryDoWystawienia[$id][$rozm['rp.id']] = $sztuk;
							$rozmiarIle[$rozm['rp.id']] = true;
						}
						if($dostepne == 'min')
						{
							if($rozm['dostepnosc'] >= $sztuk)
							$rozmiaryDoWystawienia[$id][$rozm['rp.id']] = $sztuk;
							else $rozmiaryDoWystawienia[$id][$rozm['rp.id']] = $rozm['dostepnosc'];
							$rozmiarIle[$rozm['rp.id']] = true;
						}
					}
					if(!isset($rozmiarIle[$rozm['rp.id']]))
					{
						$ileProdPominietoRozmiary++;
						$raport['status'] = 'Rozmiar niedostępny';
						$raporty->dodaj($raport);
						continue;
					}
				}
				
				$raport['id_rozm'] = 0;
				
				$kateg = new Kategorie();
				$katprod = new Katprod();
				$katprod->id = $id;
				$kategorie = $katprod->selectKategorie();
				$path = null;
				//if(false)
				//var_dump($kategorie);
				if(count($kategorie) > 0)
				foreach($kategorie as $kat)
				{					
					$pathThis = $kateg->get_path($kat['id']);
					if(count($pathThis) > count($path)) $path = $pathThis;
				}
				
				$kategoriaAllegro = 0;//5542
				if(false)
				foreach($this->cats_list->table['cats-list'] as $kat)
				{
					if($kat['cat-name'] == $path[count($path) - 1]['nazwa'])
					{
						$kategoriaAllegro = $kat['cat-id'];
						break;
					}
				}
				//var_dump($path);
				if(count($path) > 0)
				for($i = count($path) - 1; $i >= 0; $i--)
				{
					if($kategoriaAllegro > 0) break;
					$katThis = $kateg->showWybranaKategoria($path[$i]['id']);
					if(isset($katThis['allegro']) && !empty($katThis['allegro']))
					{
						$kats = explode(';', $katThis['allegro']);
						//var_dump($kats);
						for($j = count($kats) - 1; $j >= 0; $j--)
						{
							if(intval($kats[$j]) > 0)
							{
								$kategoriaAllegro = $kats[$j];
								break;
							}
						}
					}
				}
				
				if($kategoriaAllegro == 0)
				if(count($path) > 0)
				{
					$katsAllegro = new Allegrokategorie();	
					$katAllegro = $katsAllegro->getAllByName($path[count($path) - 1]['nazwa']);
					if(count($katAllegro) == 1) $kategoriaAllegro = $katAllegro[0]['cat-id'];
				}
				if($kategoriaAllegro == 0)
				{
					$ileProdPominietoKategorie++;
					$raport['status'] = 'Nie przypisano kategorii Allegro';
					$raporty->dodaj($raport);
					continue;
				}
				//echo '<echo>'.$kategoriaAllegro.'</echo>';
				
				if(count($rozmiarIle) == 0)
				if($ileProdPominietoJestAukcja == 0)
				{
					$ileProdPominietoProdukt++;
					$raport['status'] = 'Produkt niedostępny';
					$raporty->dodaj($raport);
					continue;
				}
				if(count($rozmiarIle) > 0) $ileProd++;
				$ileAukcji = count($rozmiarIle);
				
				$dane['nazwa'] = $produkt['nazwa'];
				$dane['kat'] = $kategoriaAllegro;
				$dane['dni'] = $dni;
				$dane['wysylka_w_ciagu'] = $this->config['wysylka_w_ciagu'];
				$dane['data'] = $data + ($wystawionoAll + $wystawiono) * $minut * 60;
				$dane['sztuk'] = $sztuk;
				$dane['sztukowanie'] = $sztukowanie;
				$dane['kodPocztowy'] = $this->config['kodPocztowy'];
				$dane['miasto'] = $this->config['miasto'];
				$dane['wojew'] = $this->config['wojew'];
				$dane['kraj'] = $this->config['kraj'];
				$dane['konto1'] = $this->config['konta_opis'] == 'on' ? $this->config['konto1'] : '';
				$dane['konto2'] = $this->config['konta_opis'] == 'on' ? $this->config['konto2'] : '';
				$dane['opcje_przesylki'] = $this->config['opcje_przesylki'];
				$dane['format_sprzedazy'] = $this->config['format_sprzedazy'] > 0 ? $this->config['format_sprzedazy'] : 0;
				$dane['wznowienie'] = $this->config['wznowienie'];
				$dane['formy_platnosci'] = $this->config['formy_platnosci'];
				$dane['transport_kto'] = $this->config['transport_kto'];
				$dane['transport_opcje'] = $this->config['transport_opcje'];
				$dane['info'] = $this->config['info'];
				$dane['opcje_dodatkowe'] = $this->config['opcje_dodatkowe'];
				$dane['opis'] = '<br/><br/>'.$produkt['tekst'];
				$czyKupTeraz = $this->config['czyKupTeraz'];
				//$cena = $produkt['promocja'] ? $produkt['cena_promocji_b'] : $produkt['cena_brutto'];
				$dane['wywolawcza'] = $cena;
				$dane['minimalna'] = 0;
				$dane['kupTeraz'] = $cena;
				$dane['koszty'] = @unserialize($this->config['koszty']);
				$kosztyAllegro = @unserialize($produkt['kosztyAllegro']);
				$cennikAllegro = @intval($produkt['allegroCennik']);
				if($this->obConfig->allegroCenniki && $cennikAllegro > 0)
				{
					$cenniki = new Allegrocenniki();
					$cenniki->id = $cennikAllegro;
					$cennik = $cenniki->wypiszPojedynczego();
					if(count($cennik) > 0)
					{
						$dane['opcje_przesylki'] = $cennik['opcje_przesylki'];
						$dane['koszty'] = @unserialize($cennik['koszty']);
					}
				}
				if(count($dane['koszty']) > 0)
				foreach($dane['koszty'] as $koszt => $koszty)
				{
					if((!isset($koszty['czy']) || $koszty['czy'] != 'on') || ($this->obConfig->produktyKosztyAllegro
					&& (!isset($kosztyAllegro[$koszt]['czy']) || $kosztyAllegro[$koszt]['czy'] != 'on')))
					{
						$dane['koszty'][$koszt]['koszt'] = -1;
						$dane['koszty'][$koszt]['doplata'] = -1;
						$dane['koszty'][$koszt]['paczka'] = -1;
					}
					//echo $koszt.' = '.@$kosztyAllegro[$koszt]['czy'].'<br/>';
					if(false)
					foreach($koszty as $jaki => $wartosc)
					if(floatval($dane['koszty'][$koszt][$jaki]) > 0)
					echo '<koszt>'.$koszt.' '.$jaki.' '.$dane['koszty'][$koszt][$jaki].'</koszt><br/>';
				}
				$dane['local-id'] = rand(100000,999999);//$produkt['id'];//uniqid();
				
				//var_dump($dane);
				
				if(true)
				{
					$mainPhoto = new Galeria();
					$galeria = $mainPhoto->wyswietlGalerie($produkt['id'], 'asc', 0, 'allegro', false, true);
					//var_dump($galeria->toArray());
					if(empty($galeria) || $galeria == null || count($galeria) == 0)
					$galeria = $mainPhoto->wyswietlGalerie($produkt['id'], 'asc', 0, 'oferta', false, true);
					//var_dump($galeria->toArray());
					if(false)
					{
						if(count($galeria) > 0) $zdj = $galeria->toArray();
						$dane['opis'] .= '<div style="clear:all;"><GALERIA></div>';
					}
				}
				if(true)
				if(isset($galeria) && count($galeria) > 0)
				{
					$dane['opis'] .= '<div style="float:left; clear:left; margin-top:20px;">';
					foreach($galeria as $img)
					{
						$path = 'http://'.$_SERVER['HTTP_HOST'].$this->view->baseUrl.'/public/admin/zdjecia/'.$img['img'];
						$dane['opis'] .= '<img src="'.$path.'" alt="" height="300" />';
					}
					$dane['opis'] .= '</div>';
				}
				if(true)
				for($i=0;$i<8;$i++)
				{
					if(!isset($zdj) || !isset($zdj[$i])) $dane['zdj'][$i] = '';
					else
					{
						$path = 'http://'.$_SERVER['HTTP_HOST'].$this->view->baseUrl.'/public/admin/zdjecia/'.$zdj[$i]['img'];
						$dane['zdj'][$i] = $this->allegro->resize($path);
					}
				}
				if(true)
				{
					$path = $this->view->baseUrl.'/public/admin/zdjecia/';
					$file = 'brak.jpg';
					if(isset($galeria) && $galeria != null && count($galeria) > 0)
					{
						$file = $galeria[count($galeria) - 1]['img'];
						$headers = @get_headers('http://'.$_SERVER['HTTP_HOST'].$path.$file);
						if(!$headers || !preg_match('/200/', $headers[0])) $file = 'brak.jpg';
						if(false) if(!file_exists($path.$file)) $file = 'brak.jpg';
					}
					$fullPath = 'http://'.$_SERVER['HTTP_HOST'].$path.$file;
					$dane['zdj'][0] = $this->allegro->resize($fullPath);
				}				
				echo '<fullPath>'.$fullPath.'</fullPath>';
				//echo '<zdj0>'.$dane['zdj'][0].'</zdj0>';
				
				$options = $this->allegro->wystawAukcje($dane);
				//var_dump($options);
				//if(false)
				$fields = $options['fields'];
				for($i=0;$i<count($fields);$i++)
				{
					$fid = $options['fields'][$i]['fid'];
					//var_dump($fid);
					//echo $fid.' '.$options['fields'][$i]['fvalue-int'].'<br>';
					
					if($fid >= 36 && $fid <= 60)
					{
						//echo $fid.' x1 '.$options['fields'][$i]['fvalue-int'].'<br>';
						if(true && $options['fields'][$i]['fvalue-float'] < 0)
						unset($options['fields'][$i]);
					}
					
					if($fid >= 136 && $fid <= 160)
					{
						//echo $fid.' x2 '.$options['fields'][$i]['fvalue-int'].'<br>';
						if(true && $options['fields'][$i]['fvalue-float'] < 0)
						unset($options['fields'][$i]);
					}
					
					if($fid >= 236 && $fid <= 260)
					{
						//echo $fid.' x3 '.$options['fields'][$i]['fvalue-int'].'<br>';
						if(true && $options['fields'][$i]['fvalue-int'] < 0)
						unset($options['fields'][$i]);
					}
					
					if($fid == 6)
					if($this->config['format_sprzedazy'] != 0)
					unset($options['fields'][$i]);
					
					if($fid == 7)
					if($this->config['format_sprzedazy'] != 0 || $this->config['czyMinimalna'] != 'on')
					unset($options['fields'][$i]);
					
					if($fid == 8)
					if($this->config['format_sprzedazy'] == 0 && $this->config['czyKupTeraz'] != 'on')
					unset($options['fields'][$i]);
				}
				//var_dump(count($options['fields']));
				//var_dump($options['fields']);
				
				$koszt = 0;
				$optionsTemp = $options;
				
				//$rozmiar = null;
				if($tryb == 'rozmiary') $rozmiar = $rozmiary->wypisz($produkt['rozmiarowka']);
				else if($tryb == 'produkt') $rozmiar = $produkty->wypiszJeden();//wypiszPojedynczaAll
				
				//if(false)
				if(count($rozmiar) > 0)
				foreach($rozmiar as $rozm)
				try
				{					
					if(!isset($rozmiaryDoWystawienia[$id][$rozm['id']])) continue;
					
					set_time_limit(60);
					
					$options = $optionsTemp;
					
					$rozmID = $tryb != 'produkt' ? $rozm['id'] : 0;
					
					$raport['id_rozm'] = $rozmID;
					$raport['koszt'] = 0;
					
					if(@!empty($rozm['img']))
					{
						$fullPath = 'http://'.$_SERVER['HTTP_HOST'].$path.$rozm['img'];
						$options['fields'][15]['fvalue-image'] = $this->allegro->resize($fullPath);
					}
					
					if(true)
					{
						$nazwa = $this->obConfig->allegroNazwa && @!empty($produkt['nazwa_allegro']) ? $produkt['nazwa_allegro'] : $produkt['nazwa'];
						//$nazwa .= ' '.$kategorie[count($kategorie) - 1]['nazwa'];
						//$nazwa .= ' '.$produkt['oznaczenie'];
						if($tryb == 'rozmiary') 
						{
							if(@!empty($rozm['nazwa']))
							$nazwa .= ' '.$rozm['nazwa'];
							if(@!empty($rozm['wkladka']))
							$nazwa .= '/'.$rozm['wkladka'];
						}
						if(false)
						{
							$producenci = new Producent();
							$producenci->id = $produkt['producent'];
							$producent = $producenci->wypiszPojedynczego();
							$nazwa .= ' '.@$producent['nazwa'];
						}
						$options['fields'][0]['fvalue-string'] = $this->allegro->getNazwaAllegro($nazwa);
					}
					//$options['fields'][0]['fvalue-string'] .= ' TEST PROSZE NIE LICYTOWAC';
					
					$sztuki = intval($rozmiaryDoWystawienia[$id][$rozm['id']]);
					$options['fields'][4]['fvalue-int'] = $sztuki;
					
					$dane['data'] = $data + ($wystawionoAll + $wystawiono) * $minut * 60;
					$options['fields'][2]['fvalue-datetime'] = $dane['data'];
					
					if($trybCena != 'produkt' && isset($rozm['cena_brutto']))
					{
						$cena = $produkt['promocja'] ? $rozm['cena_promocji_b'] : $rozm['cena_brutto'];
						if($cena == 0)
						{
							$ileProdPominietoCenaRozmiar++;
							$ileAukcji--;
							$raport['status'] = 'Brak ceny';
							$raporty->dodaj($raport);
							continue;
						}
						if($produkt['rabat'] > 0) $cena = ($cena * (100.00 - $produkt['rabat'])) / 100;
						$cena = number_format($cena, 2, '.', '');
						if(isset($options['fields'][5])) $options['fields'][5]['fvalue-float'] = $cena;
						if(isset($options['fields'][7])) $options['fields'][7]['fvalue-float'] = $cena;
					}
					if(isset($options['fields'][6]))
					{
						$minimalna = floatval($cena) + 0.01;
						if($sztuki > 1 || $czyKupTeraz == 'on') $minimalna = 0;
						$options['fields'][6]['fvalue-float'] = number_format($minimalna, 2, '.', '');
					}
					if(isset($options['fields'][5]) && isset($options['fields'][7]))
					{
						if($sztuki > 1)
						{
							if($czyKupTeraz == 'on')
							unset($options['fields'][5]);
							else
							unset($options['fields'][7]);
						}
					}
					echo '<cena>'.$options['fields'][7]['fvalue-float'].'</cena>';
					
					//if($wystawTeraz)
					if(function_exists('curl_exec'))
					{
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, "http://".$_SERVER['HTTP_HOST'].$this->view->baseUrl.'/szablon/index/index/konto/'.$konto.'/id/'.$produkt['id'].'/rozm/'.$rozmID);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_setopt($ch, CURLOPT_TIMEOUT, 30);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						$opis = curl_exec($ch);
						if(curl_errno($ch))
						{
							echo '<Error>Błąd #0 przy przetwarzaniu produktu '.$nazwa.' - '.curl_errno($ch).':'.curl_error($ch).'</Error>';
							$raport['status'] = curl_errno($ch).':'.curl_error($ch);
							$raporty->dodaj($raport);
							continue;
						}
						else $options['fields'][23]['fvalue-string'] = $opis;
						curl_close($ch);
					}
					if(!function_exists('curl_exec'))
					{
						$SKU = '<div style="display:none;">SKU='.$rozmID.'=SKU</div>';
						$opis = $this->obConfig->allegroOpis && @!empty($produkt['tekst_allegro']) ? $produkt['tekst_allegro'] : $produkt['tekst'];
						$options['fields'][23]['fvalue-string'] = $SKU.'<br/><br/>'.$nazwa.$dane['opis'];
					}
					if(!function_exists('curl_exec'))
					if(isset($aukcjeProduktu[$id]) && count($aukcjeProduktu[$id]) > 0)
					{
						$options['fields'][23]['fvalue-string'] .= '<br clear="all" /><br/>Zobacz inne aukcje tego produktu:<br/>';
						foreach($aukcjeProduktu[$id] as $aukcjaProd)
						{
							$nazwa = '<a href="http://allegro.pl/item'.$aukcjaProd['item-id'].'.html" rel="" >';
							$nazwa .= $produkt['nazwa'];
							if($tryb == 'rozmiary')
							{
								$nazwa .= ' - rozmiar '.$aukcjaProd['nazwa'];
								if(!empty($aukcjaProd['wkladka']))
								$nazwa .= ' - dł. wkładki '.$aukcjaProd['wkladka'];
							}
							$nazwa .= '</a><br/>';
							$options['fields'][23]['fvalue-string'] .= $nazwa;
						}
					}
						
					//var_dump($options);
					//echo $options['fields'][0]['fvalue-string'];
					
					if(isset($katThis['allegro']) && $this->obConfig->allegroParametry)
					{
						$pola = new Allegropola();
						$allegroparams = new Allegroparams();
						$paramsAll = $pola->getPolaForKat($katThis['allegro']);
						//var_dump($paramsAll);
						if(count($paramsAll) > 0)
						foreach($paramsAll as $kat => $array)
						foreach($array as $ide => $params)
						{
							$fid = $params['sell-form-id'];
							$typ = $params['sell-form-res-type'];
							$title = $params['sell-form-title'];
							$values = $params['sell-form-opts-values'];
							$desc = $params['sell-form-field-desc'];
							unset($val);
							
							if($title == 'Marka' && $this->obConfig->producent)
							{
								$value = $allegroparams->getOne($kat, $fid, $produkt['producent']);
								$val = isset($value['value']) ? $value['value'] : null;
							}
							elseif($title == 'Kolor' && $this->obConfig->kolory)
							{
								$suma = $allegroparams->getParamsKolory($id, $kat, $fid);
								$val = $suma['suma'];
							}
							elseif(false && $title == 'Rozmiar' && $tryb == 'rozmiary')
							{
								if(empty($values)) 
								{
									$val = trim(str_replace(',', '.', str_replace('cm', '', $rozm['nazwa'])));
								}
								else
								{
									$value = $allegroparams->getParamsRozmiary($id, $kat, $fid, $rozm['nazwa']);
									$val = isset($value[0]) ? $value[0]['value'] : null;
								}
							}
							else
							{
								$value = $allegroparams->getAllForKats($kat, $fid);
								//var_dump($value);
								$val = isset($value[0]) ? $value[0]['value'] : null;
							}
							if(false && $title == 'Stan' && (stripslashes($desc) == 'globalny "Stan"'))
							{
								if(@intval($val) == 0) $val = 1;
							}
							if(isset($val) && $val && $val != -1)
							{
								$nowyFID = $this->allegro->getPoleFID($fid, $val, $typ);
								$options['fields'][$fid - 1] = $nowyFID;
								//echo '<echo>'.$title.'='.$val.'</echo>';
							}
							//else echo '<echo>'.$title.'</echo>';
						}
					}
					
					//$wystawTeraz = false;
					if(!$wystawTeraz)
					{
						if($koszt === 0)
						{
							$koszty = $this->webapi->CheckNewAuctionExt($options['fields']);
							$koszt = str_replace(' zł', '', $koszty['item-price']);
							$koszt = floatval(str_replace(',', '.', $koszt));
						}
						$kosztAll += $koszt;
						$raport['koszt'] = $koszt;
						$raport['status'] = 'obliczono';
						$raporty->dodaj($raport);
						//echo '<czas>'.date('Y-m-d H:i:s', $dane['data']).'</czas>';
						//$wystawiono++;
					}
					else
					if($wystawTeraz)
					{
						$item = $this->webapi->NewAuctionExt($options);
						$check = $this->webapi->VerifyItem($dane['local-id']);
						//echo $rozm['id'];
						//$item['item-id'] = 111;
						//$item['item-info'] = '12,25 zł';
						//$check['item-id'] = 111;
						if(isset($item) && isset($check) && 
						($item['item-id'] == $check['item-id'] || $check['item-listed'] != -1))
						{
							$dana['id_prod'] = $id;
							$dana['id_rozm'] = $rozmID;
							$dana['cena'] = $cena;
							$dana['nr'] = $item['item-id'];
							$dana['wystawiono'] = date('Y-m-d H:i:s', $dane['data']);
							$dana['sztuk_wystawiono'] = $sztuki;
							$dana['sztuk_pozostalo'] = $sztuki;
							$allegroDni = array(0 => 3, 1 => 5, 2 => 7, 3 => 10, 4 => 14, 5 => 30, 99 => 0);
							$dana['na_ile'] = $allegroDni[$dni];
							$allegro = new Allegroaukcje();
							$allegro->dodaj($dana);
							//echo '<czas>'.$dana['wystawiono'].'</czas>';
							$wystawiono++;
							$koszt = str_replace(' zł', '', $item['item-info']);
							$koszt = floatval(str_replace(',', '.', $koszt));
							$kosztAll += $koszt;
							$raport['koszt'] = $koszt;
							$raport['status'] = 'wystawiono';
							$raporty->dodaj($raport);
						}
						else if(isset($check))
						{
							if($check['item-listed'] == 1)
							echo '<check>aukcja została wystawiona poprawnie</check>';
							else if($check['item-listed'] == 0)
							echo '<check>aukcja czeka w kolejce na wystawienie</check>';
							else if($check['item-listed'] == -1)
							echo '<check>aukcja nie została i nie zostanie już wystawiona z powodu problemów występujących po stronie Allegro lub z powodu podania niepoprawnego lokalnego identyfikatora</check>';
							else echo '<Error>Błąd nr '.$check['item-listed'].' przy wystawianiu aukcji</Error>';
							$raport['status'] = 'Błąd nr '.$check['item-listed'].' przy wystawianiu aukcji';
							$raporty->dodaj($raport);
						}
					}
				}
				catch(SoapFault $error)
				{
					$raport['id_rozm'] = 0;
					if($error->faultcode == 'ERR_CATEGORY_NOT_FOUND')
					{
						$ileAukcji = 0;
						$ileProdPominietoKategorie++;
						$raport['status'] = 'Zła kategoria Allegro';
						$raporty->dodaj($raport);
						break;
					}
					if($error->faultcode == 'ERR_LIST_THUMB_AND_NO_PHOTO')
					{
						$ileAukcji = 0;
						$raport['status'] = 'Brak miniaturki';
						$raporty->dodaj($raport);
						break;
					}
					if($error->faultcode == 'ERR_NO_SESSION')
					{
						$ileAukcji = 0;
						$raport['status'] = 'Błąd sesji';
						$raporty->dodaj($raport);
						break;
					}
					$ileAukcji--;
					$ileProdPominietoBladAllegro++;
					echo '<Error>Błąd #2 przy przetwarzaniu produktu '.$nazwa.' - '.$error->faultcode.': '.$error->faultstring."</Error>";
					$raport['status'] = $error->faultcode.':'.$error->faultstring;
					$raporty->dodaj($raport);
					continue;
				}
				$ileAukcjiAll += $ileAukcji;
				if($ileAukcji == 0) $ileProd--;
			}
			if(true || $wystawTeraz)
			{
				echo '<wystawiono>'.$wystawiono.'</wystawiono>';
			}
			echo '<data>'.$data.'</data>';
			echo '<ileProd>'.max(0, $ileProd).'</ileProd>';
			echo '<ileAukcji>'.max(0, $ileAukcji).'</ileAukcji>';
			echo '<ileAukcjiAll>'.max(0, $ileAukcjiAll).'</ileAukcjiAll>';
			echo '<kosztAll>'.max(0, $kosztAll).'</kosztAll>';
			echo '<ileProdPominietoKategorie>'.max(0, $ileProdPominietoKategorie).'</ileProdPominietoKategorie>';
			echo '<ileProdPominietoProdukt>'.max(0, $ileProdPominietoProdukt).'</ileProdPominietoProdukt>';
			echo '<ileProdPominietoRozmiary>'.max(0, $ileProdPominietoRozmiary).'</ileProdPominietoRozmiary>';
			echo '<ileProdPominietoOpis>'.max(0, $ileProdPominietoOpis).'</ileProdPominietoOpis>';
			echo '<ileProdPominietoCena>'.max(0, $ileProdPominietoCena).'</ileProdPominietoCena>';
			echo '<ileProdPominietoCenaRozmiar>'.max(0, $ileProdPominietoCenaRozmiar).'</ileProdPominietoCenaRozmiar>';
			echo '<ileProdPominietoJestAukcja>'.max(0, $ileProdPominietoJestAukcjaAll).'</ileProdPominietoJestAukcja>';
			echo '<ileProdPominietoBladAllegro>'.max(0, $ileProdPominietoBladAllegro).'</ileProdPominietoBladAllegro>';
		}
		
		echo '</root>';
	}
	
	public function getNazwaAllegro($nazwa)
	{
		// < i > - 4 znaki,  & - 5 znaków, " - 6 znaków
		$nazwa = htmlspecialchars_decode($nazwa);
		$ileBracketLeft = mb_substr_count($nazwa, '<', 'UTF-8');
		$ileBracketRight = mb_substr_count($nazwa, '>', 'UTF-8');
		$ileAmpersand = mb_substr_count($nazwa, '&', 'UTF-8');
		$ileQuot = mb_substr_count($nazwa, '"', 'UTF-8');
		$nazwa = str_replace('"', '\'', $nazwa);
		$nazwa = str_replace('’', '\'', $nazwa);
		$nazwa = str_replace('`', '\'', $nazwa);
		$maxZnakow = 50;
		$maxZnakow -= $ileBracketLeft * 4;
		$maxZnakow -= $ileBracketRight * 4;
		$maxZnakow -= $ileAmpersand * 5;
		//$maxZnakow -= $ileQuot * 6;
		$nazwaNew = mb_substr($nazwa, 0, $maxZnakow, 'UTF-8');
		//echo '<nazwaIle>'.$ileBracketLeft.' '.$ileBracketRight.' '.$ileAmpersand.' '.$ileQuot.'</nazwaIle>';
		//echo '<nazwa>'.$nazwaNew.'</nazwa>';
		return $nazwaNew;
	}
	
	public function kategoriezwinAction()
	{
        $this->_helper->viewRenderer->setNoRender();

        $id = intval($this->_request->getParam('id',0));
        $zw = intval($this->_request->getParam('zw',0));
		if($id > 0) $_SESSION['zwin'][$id] = $zw;
		else if($id < 0) $_SESSION['katMove'] = $zw;
		else
		{
			if(@count($_SESSION['zwin']) > 0)
			foreach($_SESSION['zwin'] as $id => $zwin)
			{
				$_SESSION['zwin'][$id] = $zw;
			}
		}
		echo $id.'='.$zw;
    }
	
	public function kategoriesortAction()
	{
        $this->_helper->viewRenderer->setNoRender();

        $record = $this->_request->getPost('action');
        $page = $this->_request->getPost('pageval');
        $position = $this->_request->getPost('rowPos');

        if(isset($record) && $record == 'changePosition')
		{
			$records = $position;
			$counter = $page;

			$real = new Kategorie();

			foreach($records as $onerec)
			{
				$real->changePositionAjax($counter, $onerec);
				$counter = $counter + 1;
			}
        }
    }
	
	public function kategoriechangeAction()
	{
        $this->_helper->viewRenderer->setNoRender();

        $id = $this->_request->getParam('id', 0);
        $rodzic = $this->_request->getParam('rodzic', -1);
		if($id > 0 && $rodzic >= 0)
		{
			$kategorie = new Kategorie();
			$katprod = new Katprod();
			$route = new Routers();
			
			if($rodzic > 0)
			{
				$katprod->dodajNowePowiazania($rodzic, $id);
				//echo 'dodaj dla '.$rodzic.' '.$id.'\n<br/>';
			}
			$kategoria = $kategorie->showWybranaKategoria($rodzic);
			while($kategoria['rodzic'] > 0)
			{
				$katprod->dodajNowePowiazania($kategoria['rodzic'], $id);
				//echo 'dodaj dla '.$kategoria['rodzic'].' '.$id.'\n<br/>';
				$kategoria = $kategorie->showWybranaKategoria($kategoria['rodzic']);
			}
			
			$kategoria = $kategorie->showWybranaKategoria($id);
			while($kategoria['rodzic'] > 0)
			{
				$katprod->usunJedynePowiazanie($kategoria['rodzic'], $id);
				//echo 'usun dla '.$kategoria['rodzic'].'\n<br/>';
				$kategoria = $kategorie->showWybranaKategoria($kategoria['rodzic']);
			}
			
			$kategorie->edytuj($id, array('rodzic' => $rodzic));
			
			if(false)
			{
				$kategoria = $kategorie->showWybranaKategoria($id);
				$kategoriaID = $kategoria;
				$link = $kategoriaID['nazwa'];
				while($kategoria['rodzic'] > 0)
				{
					$kategoria = $kategorie->showWybranaKategoria($kategoria['rodzic']);
					$link = $kategoria['nazwa'].'-'.$link;
				}
				$ret = $route->edytuj($link, $kategoriaID['route_id']);
				if($ret['id'] > 0)
				$kategorie->edytuj($id, array('link' => $ret['link']));
			}
		}
    }
	
	public function menusortAction()
	{
        $this->_helper->viewRenderer->setNoRender();

        $record = $this->_request->getPost('action');
        $page = $this->_request->getPost('pageval');
        $position = $this->_request->getPost('rowPos');

        if(isset($record) && $record == 'changePosition')
		{
			$records = $position;
			$counter = $page;

			$real = new Menu();

			foreach($records as $onerec)
			{
				$real->changePositionAjax($counter, $onerec);
				$counter = $counter + 1;
			}
        }
    }
	
	public function menuchangeAction()
	{
        $this->_helper->viewRenderer->setNoRender();

        $id = $this->_request->getParam('id', 0);
        $rodzic = $this->_request->getParam('rodzic', -1);
		if($id > 0 && $rodzic >= 0)
		{
			$menu = new Menu();
			$menu->edytuj($id, array('rodzic' => $rodzic));
		}
    }
	
	public function produktysortAction()
	{
        $this->_helper->viewRenderer->setNoRender();

        $record = $this->_request->getPost('action');
        $page = intval($this->_request->getPost('pageval', 0));
		$count = intval($this->_request->getPost('pagecount', 0));
        $position = $this->_request->getPost('rowPos');

        if(isset($record) && $record == 'changePosition')
		{
			$records = $position;
			$counter = $page * $count;

			$real = new Produkty();

			foreach($records as $onerec)
			{
				$real->changePositionAjax($counter, $onerec);
				$counter = $counter + 1;
			}
        }
    }
	
	public function galeriasortAction()
	{
        $this->_helper->viewRenderer->setNoRender();

        $record = $this->_request->getPost('action');
        $page = $this->_request->getPost('pageval');
        $position = $this->_request->getPost('rowPos');

        if(isset($record) && $record == 'changePosition')
		{
			$records = $position;
			$counter = @intval($page);

			$real = new Galeria();

			foreach($records as $onerec)
			{
				$real->changePositionAjax($counter, $onerec);
				$counter = $counter + 1;
			}
        }
    }
	
	public function sortAction()
	{
        $this->_helper->viewRenderer->setNoRender();
		
		$common = new Common();
		$table = $this->_request->getParam('table');
		if(empty($table)) return null;

        $od = @intval($this->_request->getPost('pageval'));
        $position = $this->_request->getPost('rowPos');
		$pole = $this->_request->getParam('pole', 'pozycja');

        if(count($position) > 0)
		{
			foreach($position as $pos)
			{
				$common->changePozycja($table, $pos, ++$od, $pole);
			}
        }
    }
	
	public function atrybpowiazsortAction()
	{
        $this->_helper->viewRenderer->setNoRender();
		$powiaz = new Atrybutypowiazania();	
		$id = intval($this->_request->getParam('id'));
        $position = $this->_request->getPost('atryb');
		//var_dump($position);die();
        if(count($position) > 0)
		{
			foreach($position as $pos => $atr)
			{
				$powiaz->changePositionAjax($pos, $id, $atr);
			}
        }
    }
	
	function ankietaAction()
	{
		$this->_helper->viewRenderer->setNoRender();

		$record = $this->_request->getPost('action');
		$position = $this->_request->getPost('rowPos');

		if(isset($record) && $record == 'changePosition')
		{
			$records = $position;
			$counter = 0;

			$real = new Ankietaodpowiedzi();

			foreach($records as $onerec)
			{
			   $real->changePosition($counter, $onerec);
			   $counter = $counter + 1;
			}
		}
    }
}
?>