<?php
class Ajax_CronController extends Ogolny_Controller_Ajax
{
	private $tryb;
	
    public function init() 
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
		$this->view->obConfig = $this->obConfig = new Zend_Config_Ini('../application/config.ini', 'general');
		$this->tryb = $this->obConfig->tryb;
		$this->trybCena = $this->obConfig->trybCena;
		$this->allegroTryb = $this->obConfig->allegroTryb;
		$this->allegroTrybCena = $this->obConfig->allegroTrybCena;
		//echo 'x';die();
    }
	
   
	function allegrodealsAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		
		if($this->view->adminID == 0)
		{
			$this->view->adminID = $this->_request->getParam('adminID', 0);
			$info = new Zend_Session_Namespace('admin');
			$info->id = $this->view->adminID;
		}
		
		$godzin = $this->_request->getParam('godzin', 0);
		$minut = $this->_request->getParam('minut', 15);
		$nr = $this->_request->getParam('nr', 0);
		//if(isset($this->view->godzin) && $this->view->godzin > 0) $godzin = $this->view->godzin;
		//if(isset($this->view->minut) && $this->view->minut > 0) $minut = $this->view->minut;
		//if(isset($this->view->nr) && $this->view->nr > 0) $nr = $this->view->nr;
		
		$this->konto = new Allegrokonta();                
		$pobierane = $this->konto->listapobierzKlient();
		for($p=0;$p<count($pobierane);$p++)
		{
		$this->konto->ID = $pobierane[$p]['id'];
		$this->config = $this->konto->zobaczklient();
		$this->view->config = $this->config;
		
		//$settings = new Ustawienia();
		//$ustawienia = $settings->showData($this->view->adminID);
		
		//define('ALLEGRO_ID', 'id');
		@define('ALLEGRO_LOGIN', $this->config['login']);
		@define('ALLEGRO_PASSWORD', $this->config['haslo']);
		@define('ALLEGRO_KEY', $this->config['klucz_webapi']);
		@define('ALLEGRO_COUNTRY', 1);
		
		try
		{
			$this->webapi = new AllegroWebAPI();
			$this->webapi->Login();
			@define('ALLEGRO_ID', $this->webapi->GetUserID($this->config['login']));
			$this->webapi = new AllegroWebAPI();
			$this->webapi->Login();
			$this->allegro = new AllegroClientSoap($this->config, null);
		}
		catch(SoapFault $error)
		{
			$this->view->error = 'Błąd '.$error->faultcode.': '.$error->faultstring."";
		}
		catch(Exception $error)
		{
			$this->view->error = 'Błąd '.$error->faultcode.': '.$error->faultstring."";
		}
		
		//$allegro = new Allegroaukcje();
		//$allegrozakupy = new Allegrozakupy();
		//$sonia = new Sonia();
		//$produkty = new Produkty();
		$przesylki1 = $this->webapi->objectToArray($this->webapi->GetShipmentData());
		$przesylki = $przesylki1['shipment-data-list'];


		$zamowienia = new AllegroZamowienia();
		if($this->config['point1']>0){
		$point = $this->config['point1'];
                }else{
                    $point=0;
                }
		if(isset($this->view->error)) echo 'blad: '.$this->view->error;
		else
		while($point !== null)
		{
			//$zmianyInfo = $this->webapi->GetSiteJournalDealsInfo(array('journal-start' => $point));
			//$zmianyInfo = $this->webapi->objectToArray($zmianyInfo);
			//var_dump($zmianyInfo);
			$zmiany = $this->webapi->GetSiteJournalDeals(array('journal-start' => $point));
			$zmiany = $this->webapi->objectToArray($zmiany);
			//var_dump($zmiany);
			$point = (count($zmiany) == 100) ? $zmiany[99]['deal-event-id'] : null;
			//echo date('Y-m-d H:i:s').' dealów w aukcjach: '.count($zmiany).' point = '.$point.'<br/>';
			//echo 'pierwsza zmiana: '.@(date('Y-m-d H:i:s', $zmiany[0]['deal-event-time'])).'<br/>';
			//echo 'ostatnia zmiana: '.@(date('Y-m-d H:i:s', $zmiany[count($zmiany) - 1]['deal-event-time'])).'<br/>';
			if($godzin > 0) $point = null;
			if($point !== null && (time() - $zmiany[99]['deal-event-time'] > ($godzin * 3600 + $minut * 60)))
			{
				$this->konto->edytuj(array('point1' => $point));
				//continue;
			}
			//die();
			//echo'<pre>';
                        //var_dump($zmiany);
                        //echo '</pre><br />----------------------------------------------------------------------------------------<br />';
                        //die();
			//if(false)
			foreach($zmiany as $item)
			try
			{
				set_time_limit(120);				
				
				$time = $item['deal-event-time'];
				if(time() - $time > ($godzin * 3600 + $minut * 60)) continue;
				if($nr > 0 && $nr != $item['deal-item-id']) continue;
				$date = date('Y-m-d H:i:s', $time);
				//echo date('Y-m-d H:i:s').' zmiana '.$item['deal-item-id'].'-'.$date.'-'.$item['deal-event-type']."<br/>";
				
				//var_dump($item);
				//$aukcja = $allegro->szukajAukcji($this->view->adminID, $item['deal-item-id']);
				//if(count($aukcja) == 0) continue;
				//$allegro->id = $aukcja['id'];
				//$produkty->id = $aukcja['id_prod'];				
				//$produkt = $produkty->wypiszPojedyncza($this->view->adminID);
				//if(count($produkt) == 0) continue;
				
				//$array = array('item-id' => $item['deal-item-id'], 'buyer-id' => $item['deal-buyer-id']);
				//$info = $this->webapi->GetDeals($array);	
				$info = null;
				if($item['deal-transaction-id'] > 0)
				{
					$array = array('transactions-ids-array' => $item['deal-transaction-id']);
					$info = $this->webapi->GetPostBuyFormsDataForSellers($array);
					//var_dump($info);
					//if(count($info) == 0) continue;
					if(count($info) > 0) $info = $info[0];
					//echo date('Y-m-d H:i:s').' po GetPostBuyFormsDataForSellers<br/>';
				}
                                $opcjeuser = array('user-id' => $item['deal-buyer-id'], 'user-login' => 0);
                                $user = $this->webapi->objectToArray($this->webapi->ShowUser($opcjeuser));
				
                                $opcje = array('item-id' => $item['deal-item-id'], 'get-desc' => 0, 'get-image-url' => 1,
							'get-attribs' => 0, 'get-postage-options' => 0, 'get-company-info' => 0);
					$infoAll1 = $this->webapi->objectToArray($this->webapi->ShowItemInfoExt($opcje));
					$info1 = $infoAll1['item-list-info-ext'];
                                        $foto1 = $infoAll1['item-img-list'];
				unset($dane);
				//print_r($info1);
                                //die();
                                
                                if($item['deal-event-type']==1){
                                        
                                        //echo $item['deal-item-id'];
                                        
                                        //$user = $userAll[0]['users-post-buy-data'];
                                        
                                        $sprawdz = $zamowienia->selectWybraneZamowienieAll($item['deal-id']);
                                        
                                        if(isset($sprawdz) && !empty($sprawdz)) {
                                            
                                        }else{
                                        
                                            $opcje1 = array('items-array' => $item['deal-item-id']);
                                            //echo $item['deal-item-id'];
                                            //print_r($opcje1);
                                            $userinfo = $this->webapi->objectToArray($this->webapi->GetPostBuyData($opcje1));
                                            //print_r($user1);
                                            //die();
                                            for($a=0;$a<count($userinfo);$a++){
                                                if($userinfo[0]['users-post-buy-data'][$a]['user-data']['user-login']==$user['user-login']){
                                                    $user1 = $userinfo[0]['users-post-buy-data'][$a];
                                                }
                                            }
                                            
                                            //print_r($userinfo);
                                            
                                            //die();
                                     $kontrahent['nr_zam']=$item['deal-id'];
                                     $kontrahent['id_oferty'] = $item['deal-item-id'];
                                     $kontrahent['id_buyer'] = $item['deal-buyer-id'];
                                     $kontrahent['nazwa']=$user1['user-data']['user-login'];
                                     $kontrahent['punkty']=$user1['user-data']['user-rating'];
                                     $kontrahent['sprzedawca'] = $info1['it-seller-login'];
                                     $kontrahent['online_typ']='nowa';
                                     $kontrahent['email'] = $user1['user-data']['user-email'];
                                     //$kontrahent['nazwa'] = $info['post-buy-form-buyer-login'];
                                     //$kontrahent['kwotazaplata'] = $info['post-buy-form-amount'];
                                     //$kontrahent['koszt_dostawy'] = $info['post-buy-form-postage-amount'];
                                     //$kontrahent['uwagi'] = $info['post-buy-form-msg-to-seller'];
                                     $kontrahent['status'] = 'allsystem';
                                     $kontrahent['platnosc']='Kupujący nie wypełnił formularza pozakupowego';
                                     
                                     $kontrahent['nazwa_firmy2'] = $user1['user-data']['user-company'];
                                     $kontrahent['imie2'] = $user1['user-data']['user-first-name'].' '.$user1['user-data']['user-last-name'];
                                     $kontrahent['telefon2'] = $user1['user-data']['user-phone'];
                                     $kontrahent['kod2'] = $user1['user-data']['user-postcode'];
                                     $kontrahent['miasto2'] = $user1['user-data']['user-city'];
                                     $kontrahent['ulica2'] = $user1['user-data']['user-address'];
                                     
                                     
                                     if($user1['user-sent-to-data']['user-id']>0){
                                     //adres do wysyłki
                                     $kontrahent['inny_adres_dostawy'] = 1;
                                     $kontrahent['nazwa_firmy_korespondencja'] = $user1['user-sent-to-data']['user-company'];
                                     $kontrahent['imie_korespondencja'] = $user1['user-sent-to-data']['user-first-name'].' '.$user1['user-sent-to-data']['user-last-name'];
                                     //$kontrahent['telefon_korespondencja'] = $user1['user-sent-to-data']['post-buy-form-adr-phone'];
                                     $kontrahent['kod_korespondencja'] = $user1['user-sent-to-data']['user-postcode'];
                                     $kontrahent['miasto_korespondencja'] = $user1['user-sent-to-data']['user-city'];
                                     $kontrahent['ulica_korespondencja'] = $user1['user-sent-to-data']['user-address'];
                                     }
                                     else{
                                         $kontrahent['inny_adres_dostawy'] = 1;
                                     $kontrahent['nazwa_firmy_korespondencja'] = $user1['user-data']['user-company'];
                                     $kontrahent['imie_korespondencja'] = $user1['user-data']['user-first-name'].' '.$user1['user-data']['user-last-name'];
                                     $kontrahent['telefon_korespondencja'] = $user1['user-data']['user-phone'];
                                     $kontrahent['kod_korespondencja'] = $user1['user-data']['user-postcode'];
                                     $kontrahent['miasto_korespondencja'] = $user1['user-data']['user-city'];
                                     $kontrahent['ulica_korespondencja'] = $user1['user-data']['user-address'];
                                     }
                                     //faktura
                                     //$kontrahent['faktura'] = $info['post-buy-form-invoice-option'];
                                     $kontrahent['nazwa_firmy'] = $user1['user-data']['user-company'];
                                     $kontrahent['imie'] = $user1['user-data']['user-first-name'].' '.$user1['user-data']['user-last-name'];
                                     $kontrahent['telefon'] = $user1['user-data']['user-phone'];
                                     //$kontrahent['nip'] = $user1['user-data']['user-email'];
                                     $kontrahent['kod'] = $user1['user-data']['user-postcode'];
                                     $kontrahent['miasto'] = $user1['user-data']['user-city'];
                                     $kontrahent['ulica'] = $user1['user-data']['user-address'];
                                    
                                     //print_r($kontrahent);
                                     //die();
                                     if(isset($sprawdz) && !empty($sprawdz)){
                                         $edytujKontrahenta = $zamowienia->edytuj($sprawdz['id'], $kontrahent);
                                         
                                     }else{
                                         $dodajKontrahenta = $zamowienia->addKontrahent($kontrahent);
                                     }
                                        
                                     //###############################################################3
                                     if(isset($sprawdz) && !empty($sprawdz)){
                                        $produkt['id_zam']=$sprawdz['id'];
                                     }
                                     else{
                                         $produkt['id_zam']=$dodajKontrahenta;
                                     }
                                     
                                      $produkt['nazwa']=$info1['it-name'];
                                         $produkt['cena_brutto']=$info1['it-price'];
                                         $produkt['wartosc']=$info1['it-price']*$item['deal-quantity'];
                                         $produkt['ilosc']=$item['deal-quantity'];
                                         $produkt['jedn']='szt';
                                         $produkt['id_prod']=$item['deal-item-id'];
                                        
                                         if(!empty($foto)){
                                            $produkt['foto']=$foto1[0]['image-url'];
                                        }
                                        
                                     
                                     
                                     
                                     
                                        $dodajProdukt = $zamowienia->addProdukt($produkt);
                                        
                                       //$data=$produkt;
                                       
                                        //$wiadomosci = new Wiadomosci();
                                     //$blad = $wiadomosci->noweZamowienieAllegro($kontrahent, $data, $this->www, false, $this->obConfig->tryb, true);
                                        }
                                        unset($foto);
                                        unset($kontrahent);
                                        unset($user);
                                        
                                        unset($infoAll);
                                        unset($info);
                                       
                                    
                                }
                                elseif($item['deal-event-type']==2){
                                     $sprawdz = $zamowienia->selectWybraneZamowienieAll($item['deal-id']);
                                     $kontrahent['nr_zam']=$item['deal-id'];
                                     $kontrahent['nazwa']=$user['user-login'];
                                     $kontrahent['punkty']=$user['user-rating'];
                                     $kontrahent['sprzedawca'] = $info1['it-seller-login'];
                                     $kontrahent['online_typ']='nowa';
                                     $kontrahent['email'] = $info['post-buy-form-buyer-email'];
                                     $kontrahent['nazwa'] = $info['post-buy-form-buyer-login'];
                                     $kontrahent['kwotazaplata'] = $info['post-buy-form-amount'];
                                     $kontrahent['koszt_dostawy'] = $info['post-buy-form-postage-amount'];
                                     $kontrahent['uwagi'] = $info['post-buy-form-msg-to-seller'];
                                     $kontrahent['status'] = 'allsystem';
                                     for($a=0;$a<count($przesylki);$a++){
                                         
                                         if($przesylki[$a]['shipment-id']==$info['post-buy-form-shipment-id']){
                                             $kontrahent['wysylka']=$przesylki[$a]['shipment-name'];
                                         }
                                         
                                     }
                                     if($info['post-buy-form-pay-type']=='collect_on_delivery'){
                                         $kontrahent['potwierdzenie']=1;
                                         $kontrahent['platnosc']='Płacę przy odbiorze';
                                     }
                                     elseif($info['post-buy-form-pay-type']=='wire_transfer'){
                                         $kontrahent['potwierdzenie']=1;
                                         $kontrahent['platnosc']='Zwykły przelew';
                                     }
                                     elseif($info['post-buy-form-pay-type']=='co'){
                                         $kontrahent['platnosc']='Checkout PayU';
                                          $kontrahent['online']=1;
                                     }
                                     elseif($info['post-buy-form-pay-type']=='ai'){
                                         $kontrahent['platnosc']='Raty PayU';
                                         $kontrahent['online']=1;
                                     }
                                     else{
                                         $kontrahent['platnosc']='PayU';
                                         $kontrahent['online']=1;
                                     }
                                     
                                     
                                     
                                     //adres do wysyłki
                                     $kontrahent['inny_adres_dostawy'] = 1;
                                     $kontrahent['nazwa_firmy_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-company'];
                                     $kontrahent['imie_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-full-name'];
                                     $kontrahent['telefon_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-phone'];
                                     $kontrahent['kod_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-postcode'];
                                     $kontrahent['miasto_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-city'];
                                     $kontrahent['ulica_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-street'];
                                     
                                     //faktura
                                     $kontrahent['faktura'] = $info['post-buy-form-invoice-option'];
                                     $kontrahent['nazwa_firmy'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-company'];
                                     $kontrahent['imie'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-full-name'];
                                     $kontrahent['telefon'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-phone'];
                                     $kontrahent['nip'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-nip'];
                                     $kontrahent['kod'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-postcode'];
                                     $kontrahent['miasto'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-city'];
                                     $kontrahent['ulica'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-street'];
                                    
                                     
                                     if(isset($sprawdz) && !empty($sprawdz)){
                                         $edytujKontrahenta = $zamowienia->edytuj($sprawdz['id'], $kontrahent);
                                         
                                     }else{
                                         $dodajKontrahenta = $zamowienia->addKontrahent($kontrahent);
                                     }
                                     //produkty
                                     for($p=0;$p<count($info['post-buy-form-items']);$p++){
                                         $produkt['nazwa']=$info['post-buy-form-items'][$p]['post-buy-form-it-title'];
                                         $produkt['cena_brutto']=$info['post-buy-form-items'][$p]['post-buy-form-it-price'];
                                         $produkt['wartosc']=$info['post-buy-form-items'][$p]['post-buy-form-it-amount'];
                                         $produkt['ilosc']=$info['post-buy-form-items'][$p]['post-buy-form-it-quantity'];
                                         $produkt['jedn']='szt';
                                         $produkt['id_prod']=$info['post-buy-form-items'][$p]['post-buy-form-it-id'];
                                         $opcje1 = array('item-id' => $info['post-buy-form-items'][$p]['post-buy-form-it-id'], 'get-desc' => 0, 'get-image-url' => 1,
							'get-attribs' => 0, 'get-postage-options' => 0, 'get-company-info' => 0);
					$infoAll3 = $this->webapi->objectToArray($this->webapi->ShowItemInfoExt($opcje1));
					$info3 = $infoAll3['item-list-info-ext'];
                                        $foto = $infoAll3['item-img-list'];
                                         if(!empty($foto)){
                                            $produkt['foto']=$foto[0]['image-url'];
                                        }
                                        if(isset($sprawdz) && !empty($sprawdz)){
                                            $produkt['id_zam']=$sprawdz['id'];
                                        }else{
                                            $produkt['id_zam']=$dodajKontrahenta;
                                        }
                                        if(isset($sprawdz) && !empty($sprawdz)){
                                            $edytujProdukt1 = $zamowienia->edytujProduktAll($sprawdz['id'], $produkt['nazwa'], $produkt);
                                            if(!isset($edytujProdukt1) || empty($edytujProdukt1)){
                                                $edytujProdukt = $zamowienia->addProdukt($produkt);
                                            }
                                        }else{
                                         $edytujProdukt = $zamowienia->addProdukt($produkt);
                                        
                                        $data[$p]=$produkt;
                                        
                                        }
                                         
                                     }
                                     
                                     $wiadomosci = new Wiadomosci();
                                     $blad = $wiadomosci->noweZamowienieAllegro($kontrahent, $data, $this->www, false, $this->obConfig->tryb, true);
                                     
                                     unset($foto);
                                        unset($kontrahent);
                                        unset($user);
                                        unset($produkt);
                                        unset($infoAll);
                                        unset($info);
                                     
                                     
                                     
                                     //$edytujProdukt = $zamowienia->edytujProdukt($sprawdz['id'], $produkt);
                                    
                            /*        
                                    echo'<pre>';
                        var_dump($info);
                        echo '</pre><br />----------------------------------------------------------------------------------------<br />';
                        echo'<pre>';
                        //var_dump($foto);
                        echo '</pre><br />----------------------------------------------------------------------------------------<br />';
                        
                        
                        echo'<pre>';
                        //var_dump($user);
                        echo '</pre><br />----------------------------------------------------------------------------------------<br />';
                     */
                       
                                }
                                elseif($item['deal-event-type']==3){
                                    $sprawdz = $zamowienia->selectWybraneZamowienieAll($item['deal-id']);
                                    $kontrahent['potwierdzenie']=2;
                                     if(isset($sprawdz) && !empty($sprawdz)){
                                         $edytujKontrahenta = $zamowienia->edytuj($sprawdz['id'], $kontrahent);
                                         
                                     }
                                    
                                    
                                    unset($foto);
                                        unset($kontrahent);
                                        unset($user);
                                        unset($produkt);
                                        unset($infoAll);
                                        unset($info);
                                     
                        
                                }
                                elseif($item['deal-event-type']==4){
                                      $sprawdz = $zamowienia->selectWybraneZamowienieAll($item['deal-id']);
                                    $kontrahent['online_typ']='zakończona';
                                    $kontrahent['potwierdzenie']=1;
                                     if(isset($sprawdz) && !empty($sprawdz)){
                                         $edytujKontrahenta = $zamowienia->edytuj($sprawdz['id'], $kontrahent);
                                         
                                         
                                         
                                         
                                     }
                                     unset($foto);
                                        unset($kontrahent);
                                        unset($user);
                                        unset($produkt);
                                        unset($infoAll);
                                        unset($info);
                                     
                        
                                }
				
			}
			catch(SoapFault $error)
			{
				echo 'Błąd '.$error->faultcode.': '.$error->faultstring."<br/>\n";
			}
		}
		//echo date('Y-m-d H:i:s').' po deals petla<br/>';
		//$allegrozakupy->usunDuplikaty($this->view->adminID);
                }
    }
    function fedexAction(){
        $this->_helper->viewRenderer->setNoRender();
        $fedex = new FedexWebApi();
        
        
        
        
        $test = $fedex->AddressValidation();
        
        
    }
    
    function enAction(){
        $this->_helper->viewRenderer->setNoRender();
        
        $en = new ElektronicznyNadawca();
        try {
                $czysc = new clearEnvelope();
				$parametry = new getEnvelopeBufor();
				$bufor = $en->getEnvelopeBufor($parametry);
				$bufor = @$bufor['przesylka'];
                $wyczysc = $en->clearEnvelope($czysc);
				$pocztapolskadane = new Pocztapolskadane();
				if(count($bufor) > 0)
				{
					if(!isset($bufor[0])) $bufor = array(0 => $bufor);
					foreach($bufor as $przes)
					{
						$pocztapolskadane->usunGUID($przes['guid']);
					}
				}
            } catch (SoapFault $error) {
                
            }
    }
	
}
?>