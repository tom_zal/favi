<?php

    class Ajax_UpominkiController extends Ogolny_Controller_Ajax
    {
        private $id;

        public function init()
        {
            parent::init();
            $this->view->www = $_SERVER['HTTP_HOST'];
            $this->view->baseUrl = $this->baseUrl = $this->_request->getBaseUrl();
            $this->view->path = $this->baseUrl.str_replace('..', '', $this->ImageDir->Upominki);
            $this->root = $_SERVER['DOCUMENT_ROOT'];
        }
        function indexAction()
        {
            $ob = new Query('Upominki');
            $this->view->tryb = $this->_getParam('tryb');
            $this->view->rows = $ob->getRows();
        }
        function kartyAction()
        {
            $ob = new Query('Kontrahenci');
            $ob->column = 'kartaaktywna';
            $ob->order = 'karta';  
            $ob->value = 0;
            $this->view->rows = $ob->getRowsValue();
        }
        function kartaAction()
        {
            $this->_helper->viewRenderer->setNoRender();
            $ob = new Query('Kontrahenci');
            $ob->column = 'karta';
            $ob->order = 'id';  
            $ob->value = $this->_getParam('karta');
            $row = $ob->getRowsValue();
            
            $typ = $this->_getParam('typ') ? 0 : 1;
            
            if(!empty($row) && isset($row[0]['kartaaktywna']) && $row[0]['kartaaktywna'] == $typ) echo true;
            else echo false;    
        }
        function plikiAction()
        {
            $ob = new Query('Lojalnoscpliki');
            $this->view->rows = $ob->getRows();
        }
        function kartyznajdzAction()
        {
            $ob = new Query('Kontrahenci');
            $ob->column = 'kartaaktywna';
            $ob->order = 'karta';  
            $ob->value = 0;
            $this->view->rows = $ob->getRowsValue();
        }
        function dodajzakupptAction() {
            $post = $this->_getParam('post');
            $ob1 = new Query('Kontrahenci');
            $ob2 = new Query('Lojalnosczakupy');
            $ob3 = new Query('Lojalnoscrabatypole');
            $ob4 = new Query('Upominki');
            $ob1->id = intval($post['id']);
            $row = $ob1->getRow();
            $daneklient = array();
            $dane = array();
            
            if(isset($post['rabaton'])) 
            {
                if($post['rabaton'] == 1)
                {
                    $rabatod = 0;
                } 
                else
                {
                    $rabatod = 1;
                }    
            } 
            else 
            {
                $rabatod = 0;
            }
            
            if($rabatod) 
            {
                $daneklient['konto'] = number_format($row['konto'] + $this->session->rabatst,2,'.','');
                $dane['kwotazaplata'] = $this->session->kwota;
                $dane['rabatwyk'] = 0.00;
                $daneklient['konto'] = $daneklient['konto'] > 0 ? $daneklient['konto'] : 0.00;
            }
            else
            {
                $kontorab = isset($this->session->rabatst)? $row['konto'] + $this->session->rabatst : $row['konto'];
                $dane['rabatwyk'] = ($kontorab > $this->session->kwota) ? $this->session->kwota : $kontorab;
                $daneklient['konto'] = number_format($row['konto'] - $dane['rabatwyk'],2,'.','');
                $daneklient['konto'] = $daneklient['konto'] > 0 ? $daneklient['konto'] : 0.00;
                $dane['kwotazaplata'] = ($this->session->kwota < $kontorab) ? 0.00 : number_format(($this->session->kwota - $kontorab),2,'.','');
            }    
            
            $ob2->update(array('ostatnizakup'=>0), 'idkl="'.intval($post['id']).'"');
            
            $dane['typdokumentu'] = 'paragon';
            $dane['numerdokumentu'] = $this->session->nrdowodu;
            $dane['ostatnizakup'] = 1;
            $dane['idkl'] = intval($post['id']);
            $data = date('Y-m-d H:i:s');
            $dane['data'] = $data;
            $dataex = explode(' ', $data);
            $datac = $dataex[0];
            $dataex  = explode('-', $datac);
            $dane['rok'] = $dataex[0];
            $dane['miesiac'] = $dataex[1];
            $dane['dzien'] = $dataex[2];
            $dane['kwota'] = $this->session->kwota;
            $dane['dziennazwa'] = date("l",strtotime($datac)); 
            $dane['rabatotrz'] = isset($this->session->rabatst)? $this->session->rabatst: 0.00;
            $dane['polecajacy'] = isset($this->session->polid) ? $this->session->polid : '';
            $dane['rabatpolecajacy'] = isset($this->session->rabatpol)? $this->session->rabatpol : 0.00;

            if(isset($post['upominek'])) 
            {
                $ob4->id = intval($post['upominek']);
                $row = $ob4->getRow();
                if(!empty($row))
                {
                    $dane['upominekwyk'] = $row['nazwa'].'|-|'.$row['id'];
                    $this->view->upominekotrz = $row['nazwa'];
                }    
            }    
            
            $idzam = $ob2->_save($dane);
            if(isset($daneklient)) $ob1->update($daneklient, 'id="'.intval($post['id']).'"');
            
            if(isset($this->session->polid)) 
            {
                $dane2['idzak'] = $idzam;
                $dane2['upominkilista'] = $this->session->upominekpolar;
                $dane2['idpol'] = $this->session->polid;
                $dane2['idkl'] = intval($post['id']);
                $dane2['wartosc'] = $this->session->kwota;
                $dane2['idpol'] = $this->session->polid;
                $dane2['rabat'] = isset($this->session->rabatpol)? $this->session->rabatpol : 0.00;
                $dane2['data'] = $dane['data'];
                $dane2['upominek'] = isset($this->session->upominekpol)? $this->session->upominekpol : 0;
                
                $ob3->_save($dane2);
                
                if(!empty($dane2['rabat'])) 
                {
                    $ob1->id = intval($this->session->polid);
                    $row = $ob1->getRow();
                    $ob1->_update(array('konto'=>number_format($row['konto']+$dane2['rabat'],2,'.','')));
                }
            }
            
            $this->view->rabatwyk = $dane['rabatwyk'];
            $this->view->kwotazap = $dane['kwotazaplata'];
            $this->view->rabatotrz = $dane['rabatotrz'];
            
            if($this->session->rabatod == 1) 
            {
                $this->view->stankonta = $daneklient['konto'];
            }
            
            if(isset($this->session->rabatst)) unset($this->session->rabatst);
            if(isset($this->session->rabatpol)) unset($this->session->rabatpol);
            if(isset($this->session->rabatod)) unset($this->session->rabatod);
            if(isset($this->session->stankonta)) unset($this->session->stankonta);
            if(isset($this->session->kwota)) unset($this->session->kwota);
            if(isset($this->session->polid)) unset($this->session->polid);
            if(isset($this->session->nrdowodu)) unset($this->session->nrdowodu);
        }
        
        function dodajzakupAction()
        {
            $post = $this->_getParam('post');
			$get = false;
			if($get)
			{
				$post['id'] = $this->_getParam('id');
				$post['kwota'] = $this->_getParam('kwota');
				$post['nrdowodu'] = $this->_getParam('nr');
				//var_dump($post);
			}
            $ob = new Query('Lojalnoscustawienia');
            $ob->id = 1;
            $ust = $ob->getRow();
            $this->view->ustawienia = $ust;
            $ob = new Query('Kontrahenci');
            $ob->id = intval($post['id']);
            $klient = $ob->getRow();
            $ob = new Query('Lojalnosczakupy');
            $post['kwota'] = str_replace(',','.',$post['kwota']);
            $this->view->danezakup = $post; 
            $this->view->daneklient = $klient;
            $this->view->rabatodloz = $ust['rabat_rodzaj'];
            /*
             * Wyczyszczenie zmiennych rabatu przed dodaniem nowego zakupu
             */
            if(isset($this->session->rabatst)) unset($this->session->rabatst);
            if(isset($this->session->rabatpol)) unset($this->session->rabatpol);
            if(isset($this->session->rabatod)) unset($this->session->rabatod);
            if(isset($this->session->stankonta)) unset($this->session->stankonta);
            if(isset($this->session->kwota)) unset($this->session->kwota);
            if(isset($this->session->polid)) unset($this->session->polid);
            if(isset($this->session->nrdowodu)) unset($this->session->nrdowodu);
            
            $this->session->kwota = $post['kwota'];
            $this->session->nrdowodu = $post['nrdowodu'];
                    
            if(!empty($ust) && !empty($klient)) 
            {
                $start = strtotime($ust['start']);
                $koniec = strtotime($ust['koniec']);
                $obecna = strtotime(date('Y-m-d'));
                /*
                 * Sprawdzam ramy czasowe programu
                 */
                if(!empty($start) && !empty($koniec) && $obecna >= $start && $obecna <= $koniec) 
                {
                    /*
                     * RABAT STANDARD
                     * Sprawdzam czy rabat standardowy wlaczony
                     */
                    if($ust['rabat_st_on']) 
                    {
                        /*
                         *  Sprawdzam czy przydzielac rabat tylko do pierwszego czy kazdego zakupu 
                         */
                        if($ust['rabat_od'] == 1) 
                        {
                            $ob->order = 'id';
                            $ob->column = 'idkl';
                            $ob->value = intval($post['id']);
                            $row = $ob->getRowsValue();
                            
                            if(isset($row[0]['id'])) 
                            {
                                $rabatst = 0;
                            } 
                            else 
                            {
                                $rabatst = 1;
                            }
                        } 
                        else 
                        {
                            $rabatst = 1;
                        }
                        /*
                         * Sprawdzam czy rabat jest nalezny
                         */
                        if($rabatst) 
                        {
                            $this->view->rabatst = $this->session->rabatst = $this->rabat = number_format($ust['rabat_standard'] * $post['kwota'] * 0.01,2, '.','');
                            /*
                             * Sprawdzam czy odjąc rabat od calego zamowienia / czy odlozyc na konto klienta
                             */
                            if($ust['rabat_rodzaj'] == 0) 
                            {
                                $stanplus = 0;
                            } 
                            else 
                            {
                                $stanplus = 1;
                            }
                            $this->view->rabatodloz = $this->session->rabatod = $stanplus;
                            $this->view->stankonta = $this->session->stankonta = $stanplus == 1 ? $klient['konto']+$this->rabat : $klient['konto'];
                        }
                        /*
                         * Sprawdzam czy mozna przydzielac upominki w standardzie
                         */
                        if($ust['upominek_st_on'])
                        {   
                            /*
                             * Sprawdzam czy zakup przekroczyl kwote minimalna, aby otrzymac upominek
                             */
                            if($post['kwota'] > $ust['upominek_st_kwota']) 
                            {
                                /*
                                 * Spradzam czy przydzielone sa upominki
                                 */
                                $ob = new Query('Upominki');
                                $ob->order = 'nazwa';
                                $ob->value = 1;
                                $ob->column = 'polecany';
                                $row = $ob->getRowsValue();
                                if(isset($row[0]['id'])) 
                                {
                                    $this->view->upominki = $row;
                                }
                            }
                        }
                    }
                    
                    /*
                     * RABAT POLECAJACY
                     * Sprawdzam czy rabat dla polecajacych wlaczony
                     */
                    if($ust['rabat_pol_on'] && $klient['polecajacy'] != 0) 
                    {
                        /*
                         * Sprawdzam czy sa ograniczenia czasowe dla przydzielania rabatu
                         */
                        if($ust['rabat_pol_dni'] == 0) 
                        {
                            /*
                             * Sprawdzam ile dni uplynelo od rejestracji klienta do zakupu
                             */
                            $datarej = strtotime($klient['data_rej']);
                            $roznicadat = $obecna - $datarej;
                            $datazakupu = round($roznicadat / 86400);
                            
                            /*
                             * W przypadku , gdy czas od rejestracji do zakupu 
                             * byl mniejszy niz czas w ustawieniach przydzielam rabat
                             */
                            if($datazakupu < $ust['rabat_pol_od']) 
                            {
                                $rabatpol = 1;
                            } 
                            else
                            {
                                $rabatpol = 0;
                            }    
                        } 
                        else 
                        {
                            $rabatpol = 1;
                        }
                        
                        if($rabatpol) 
                        {
                            /*
                             * Spradzam czy rabat moge przydzielic do pierwszego czy do kazdego zakupu
                             */
                            if($ust['rabat_pol_wyp_od'] == 1) 
                            {
                                $ob = new Query('Lojalnosczakupy');
                                $ob->order = 'id';
                                $ob->column = 'idkl';
                                $ob->value = intval($post['id']);
                                $row = $ob->getRowsValue();

                                if(isset($row[0]['id'])) 
                                {
                                    $rabatpol = 0;
                                } 
                                else 
                                {
                                    $rabatpol = 1;
                                }
                            } 
                            else
                            {
                                $rabatpol = 1;
                            }   
                            
                            if($rabatpol) 
                            {
                                $this->session->polid = $klient['polecajacy'];
                                $this->view->rabatpol = $this->session->rabatpol = $this->rabatpol = number_format($ust['rabat_polecajacy'] * $post['kwota'] * 0.01,2, '.','');
                            }
                        }
                    }
                    /*
                    * Sprawdzam czy mozna przydzielac upominki w standardzie
                    */
                    if($ust['upominek_pol_on'])
                    {   
                        /*
                         * Sprawdzam czy zakup przekroczyl kwote minimalna, aby otrzymac upominek
                         */
                        if($post['kwota'] > $ust['upominek_pol_kwota']) 
                        {
                            /*
                             * Spradzam czy przydzielone sa upominki
                             */
                            $ob = new Query('Upominki');
                            $ob->order = 'nazwa';
                            $ob->value = 1;
                            $ob->column = 'polecajacy';
                            $row = $ob->getRowsValue();
                            if(isset($row[0]['id'])) 
                            {
                                $this->view->upominkipol = $row;
                                $this->session->upominekpol = 1;
                                $this->session->upominekpolar = serialize($row);
                            }
							//var_dump($this->view->upominkipol);
                        }
                    }
                }
            }
        }
        function dodajAction()
        {

            $ob = new Query('Upominki');
            $post = $this->_getAllParams();
            unset($post['action']);
            unset($post['module']);
            unset($post['controller']);
            unset($post['post']);
            unset($post['tryb']);
            $this->view->tryb = $this->_getParam('tryb');

            if($this->_request->isPost())
            {
                if(!empty($post['nazwa']))
                {
                    $post['img'] = $post['fileCVName'];
                    if(!empty($post['img']))
                    {
                        $handle = new upload($_SERVER['DOCUMENT_ROOT'].$this->view->path.''.$post['img']);
                        if ($handle->uploaded)
                        {
                            $handle->image_resize = true;
                            $handle->image_ratio = true;
                            $handle->file_new_name_body = $post['img'];
                            $handle->image_y = $this->ImageDir->min_y_2;
                            $handle->image_x = $this->ImageDir->min_x_2;
                            
                            $handle->process($this->ImageDir->Upominki);

                            if ($handle->processed)
                            {
                                $handle->clean();
                                $post['img'] = $handle->file_dst_name;
                            } else {
                                $post['img'] = '';
                            }
                        }
                    }
                    unset($post['fileCVName']);

                    $post['cena'] = str_replace(',','.', $post['cena']);
                    $ob->_save($post);
                    $this->_helper->viewRenderer->setNoRender();
                    echo $redirect = 'http://'.$this->view->www.''.$this->view->baseUrl.'/ajax/upominki/index/tryb/'.$this->view->tryb;
                }
            }
        }
        function edytujAction()
        {
            $id = $this->_getParam('id');
            $ob = new Query('Upominki');
            $post = $this->_getAllParams();
            unset($post['action']);
            unset($post['module']);
            unset($post['controller']);
            unset($post['post']);
            unset($post['tryb']);
            $this->view->tryb = $this->_getParam('tryb');
            $ob->id = intval($id);
            $this->view->dane = $ob->getRow();
            if($this->_request->isPost())
            {
                if(!empty($post['nazwa']))
                {
                    $post['img'] = $post['fileCVName'];
                    if(!empty($post['img']))
                    {
                        $handle = new upload($_SERVER['DOCUMENT_ROOT'].$this->view->path.''.$post['img']);
                        if ($handle->uploaded)
                        {
                            $handle->image_resize = true;
                            $handle->image_ratio = true;
                            $handle->file_new_name_body = $post['img'];
                            $handle->image_y = $this->ImageDir->min_y_2;
                            $handle->image_x = $this->ImageDir->min_x_2;

                            $handle->process($this->ImageDir->Upominki);

                            if ($handle->processed)
                            {
                                $handle->clean();
                                $post['img'] = $handle->file_dst_name;
                                !empty($this->view->dane['img']) ? @unlink($this->ImageDir->Upominki.'/'.$this->view->dane['img']) : null;
                            } else {
                                $post['img'] = '';
                            }
                        }
                    }
                    unset($post['fileCVName']);

                    $post['cena'] = str_replace(',','.', $post['cena']);

                    if(empty($post['img'])) unset($post['img']);

                    $ob->_update($post);
                    $this->_helper->viewRenderer->setNoRender();
                    echo $redirect = 'http://'.$this->view->www.''.$this->baseUrl.'/ajax/upominki/index/tryb/'.$this->view->tryb;
                }
            }
        }
        function usunAction()
        {
            $this->_helper->viewRenderer->setNoRender();
            $id = $this->_getParam('id');
            $img = $this->_getParam('img');
            $tryb = $this->_getParam('tryb');
            $ob = new Query('Upominki');
            $ob->id = intval($id);
            $this->view->tryb = $tryb;
            if(!empty($id))
            {
                $ob->_delete();
                !empty($img) && $img != 'brak' ? @unlink($this->ImageDir->Upominki.'/'.$img) : null;

                $ob->order = 'nazwa';
                $trybe = $tryb == 0 ? 'polecany' : 'polecajacy';
                $ob->column = $trybe;
                $ob->value = 1;
                $this->view->rows = $ob->getRowsValue();

                $arr = array('href'=>'http://'.$this->view->www.''.$this->baseUrl.'/ajax/upominki/index/tryb/'.$tryb.'', 'tryb'=>$tryb, 'html' => $this->view->render('upominki/upominek.phtml'));
                
                $trybe = $tryb == 1 ? 'polecany' : 'polecajacy';
                $ob->column = $trybe;
                $ob->value = 1;
                $this->view->rows = $ob->getRowsValue();
                $arr['html2'] = $this->view->render('upominki/upominek.phtml');

                echo json_encode($arr);
            }
        }
        function usunplikiAction()
        {
            $this->view->cfg = $this->cfg = new Zend_Config_Ini('../application/config.ini', 'lojalnosc');
            $this->_helper->viewRenderer->setNoRender();
            $id = $this->_getParam('id');
            $img = $this->_getParam('img');
            $ar = $this->_getParam('deletevalue');

            if($id) 
            {
                $ar[$id] = $img;
            }
            
            $ob = new Query('Lojalnoscpliki');
            foreach($ar as $id => $value) 
            {
                $ob->id = intval($id);
                $ob->_delete();
                @unlink($this->root.''.$this->cfg->pathpdf.'/'.$value);
            }

            $this->view->rows = $ob->getRows();
            echo json_encode(array('href'=>'http://'.$this->view->www.''.$this->view->baseUrl.'/ajax/upominki/pliki/', 'html' => $this->view->render('upominki/pliki.phtml')));
        }
        function upominekAction() {
            $ob = new Query('Upominki');
            $this->view->tryb = $this->_getParam('tryb');
            $tryb = $this->_getParam('tryb') == 0 ? 'polecany' : 'polecajacy';
            $ob->column = $tryb;
            $ob->value = 1;
            $ob->_updateValue(array($tryb=>0));

            $zaznaczone = $this->_getParam('deletevalue');
            if(!empty($zaznaczone))
            {
                foreach($zaznaczone as $key=>$value)
                {
                    $ob->id = intval($key);
                    $ob->_update(array($tryb=>1));
                }
            }
            $ob->order = 'nazwa';
            $this->view->rows = $ob->getRowsValue();
        }

        function getclientAction()
        {
            $this->rowscount = 15;

            $kontrahent = new Kontrahenci();
			$subskrypcja = new Subskrypcja();
            $szukanie = new Zend_Session_Namespace('szukanieKontrMark');
			if($this->_request->getParam('tryb'))
			$szukanie->tryb = $this->_request->getParam('tryb');
            if($this->_request->getParam('reset'))
            {
                $szukanie->nazwa = '';
                $szukanie->grupa = 'all';
                $szukanie->plec = 'all';
                $szukanie->miasto = 'all';
                $szukanie->wojew = 'all';
                $szukanie->dataOd = '2010-01-01';
                $szukanie->dataDo = date('Y-m-d');
                $szukanie->data_ur = '';
                $szukanie->data_rej = '1';
                $szukanie->od = 0;
                $szukanie->ile = $this->rowscount;
                $szukanie->karta = '';
				$szukanie->newsletter = false;
				//$szukanie->tryb = 'kontrahenci';
            }
            $sortuj = '';
            $search = $this->_getParam('search');
            if(isset($search) && $search == 1)
            {
                $szukanie->nazwa = $this->_request->getParam('nazwa');
                if($szukanie->nazwa == 'imię, nazwisko, email') $szukanie->nazwa = '';
                $szukanie->grupa = $this->_request->getParam('grupa');
                $szukanie->plec = $this->_request->getParam('plec');
                $szukanie->miasto = $this->_request->getParam('miasto');
                $szukanie->wojew = $this->_request->getParam('wojew');
                $od = $this->_request->getParam('dataOd');
                //$szukanie->dataOd = $od['rok'].'-'.$od['mc'].'-'.$od['day'];
                $szukanie->dataOd = $od;
                $do = $this->_request->getParam('dataDo');
               // $szukanie->dataDo = $do['rok'].'-'.$do['mc'].'-'.$do['day'];
                $szukanie->dataDo = $do;
                //$szukanie->data_ur = $this->_request->getParam('data_ur', '');
                $szukanie->data_ur = $od.''.$do;
                $szukanie->zalogowano = $this->_request->getParam('zalogowano', '');
                $szukanie->data_rej = $this->_request->getParam('data_rej', '1');
                $szukanie->od = 0;
                $szukanie->ile = $this->rowscount;
                $szukanie->karta = $this->_request->getParam('karta');
				$szukanie->newsletter = $this->_request->getParam('newsletter');
				//$szukanie->tryb = $this->_request->getParam('tryb');
            }
            if ($this->_request->getParam('ile'))
            {
				$szukanie->ile = $this->_request->getParam('ile');
				$szukanie->ile = $szukanie->ile == 'all' ? 99999 : $szukanie->ile;
				$szukanie->od = 0;
            }
            if ($this->_request->getParam('sortuj'))
            {
				$szukanie->sortuj = $this->_request->getParam('sortuj');
				$sortuj = $szukanie->sortuj;
            }
            if ($this->_request->getParam('od') != '')
            {
				$szukanie->od = $this->_request->getParam('od');
            }
            if (!isset($szukanie->nazwa)) $szukanie->nazwa = '';
            if (!isset($szukanie->grupa)) $szukanie->grupa = 'all';
            if (!isset($szukanie->plec)) $szukanie->plec = 'all';
            if (!isset($szukanie->miasto)) $szukanie->miasto = 'all';
            if (!isset($szukanie->wojew)) $szukanie->wojew = 'all';
            if (!isset($szukanie->dataOd)) $szukanie->dataOd = '2010-01-01';
            if (!isset($szukanie->dataDo)) $szukanie->dataDo = date('Y-m-d');
            if (!isset($szukanie->data_ur)) $szukanie->data_ur = '';
            if (!isset($szukanie->zalogowano)) $szukanie->zalogowano = '';
            if (!isset($szukanie->data_rej)) $szukanie->data_rej = '1';
            if (!isset($szukanie->ile)) $szukanie->ile = $this->rowscount;
            if (!isset($szukanie->od)) $szukanie->od = 0;
            if (!isset($szukanie->karta)) $szukanie->karta = '';
			if (!isset($szukanie->newsletter)) $szukanie->newsletter = false;
			if (!isset($szukanie->tryb)) $szukanie->tryb = 'kontrahenci';

            $ile = $szukanie->ile;
            $od = floor($szukanie->od / $ile);
            $od = $szukanie->od;

            $sortowanie = new Zend_Session_Namespace('sortowanieKontrLojalMark');
            if($this->_request->getParam('reset'))
            {
				$sortowanie->sort = 'data_rej';
				$sortowanie->order = 'desc';
            }
            if($this->_request->getParam('sortuj'))
            {
				$sortowanie->sort = $this->_request->getParam('sort', 'data_rej');
				$sortowanie->order = $this->_request->getParam('order', 'desc');
            }
            if(!isset($sortowanie->sort))
            {
				$sortowanie->sort = 'data_rej';
				$sortowanie->order = 'desc';
            }

			if($szukanie->tryb == "subskrypcja")
			{
				$ilosc = $subskrypcja->wypiszOsoby(true, $od, $ile, $sortowanie, $szukanie, null);
				$all = $subskrypcja->wypiszOsoby(false, $od, $ile, $sortowanie, $szukanie, null);
			}
			else
			{
				$ilosc = $kontrahent->wypiszKontr(true, $od, $ile, $sortowanie, $szukanie, true);
				$all = $kontrahent->wypiszKontr(false, $od, $ile, $sortowanie, $szukanie, true);
			}

            /*
            $this->page = $this->_getParam('od', 0);
            $this->rowscount = 15;
            $ob = new Query('Kontrahenci');
            $ob->where = 'kartaaktywna <>0';
            $this->view->sort = $ob->order = 'id';
            $ob->page = $this->page+1;
            $ob->rowcount = $this->rowscount;
            $this->pager = $ob->getRowsFieldCount();

            $this->view->strona = $this->page;
            $this->view->strony = ceil($this->pager / $this->rowscount);
            $this->view->link = 'http://'.$this->view->www.$this->view->baseUrl.'/ajax/upominki/getclient';
            $this->view->rows = $ob->getRowsField();
            */
            $this->view->ilosc = ceil($ilosc / $ile);
            $this->view->rows = $all;
            $this->view->szukanie = $szukanie;
            $this->view->sortowanie = $sortowanie;
            $this->view->sortuj = isset($szukanie->sortuj) ? $szukanie->sortuj : 'za';
            $this->view->od = $od;
            $this->view->strona = $szukanie->od;
            $this->view->ile = $ile;
            $this->view->strony = ceil($ilosc / $ile);
            $this->view->link = 'http://'.$this->view->www.$this->view->baseUrl.'/ajax/upominki/getclient';
            $this->view->order = $sortowanie->order == 'asc' ? 'desc' : 'asc';
			
			$this->view->tryb = $szukanie->tryb;
            $this->view->miasta = $kontrahent->wypiszMiasta();
        }
    }

?>