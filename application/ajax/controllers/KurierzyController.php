<?php

class Ajax_KurierzyController extends Ogolny_Controller_Ajax 
{
    public function init() 
	{
        parent::init();
		$this->local_path = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].$this->baseUrl);
		$this->local_path.= '/public/admin/kurierzy';
    }
	
	function pocztastatusAction()
	{
		$this->_helper->viewRenderer->setNoRender();		
		require_once "../library/nusoap/nusoap.php"; //używamy biblioteki nusoap		
		$NAMESPACE = "http://sledzenie.pocztapolska.pl"; //uwaga na brak "-" w namespace

		$url = "https://tt.poczta-polska.pl/Sledzenie/services/Sledzenie?wsdl";
		//$url = "https://ws.poczta-polska.pl/Sledzenie/services/Sledzenie?wsdl";
		
		$client = new nusoap_client($url, "wsdl");
		$client->soap_defencoding = "UTF-8";
		$client->decode_utf8 = false;
		
		$login = "sledzeniepp";
		$pass = "PPSA";

		//nagłówek z użytkownikiem i hasłem trzeba przekazać jako gotowy kod XML
		$header = "<wsse:Security xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>";
		$header.= "<wsse:UsernameToken xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>";
		$header.= "<wsse:Username>".$login."</wsse:Username>";
		$header.= "<wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>$pass</wsse:Password>";
		$header.= "</wsse:UsernameToken>";
		$header.= "</wsse:Security>";

		$err = $client->getError();
		if($err)
		{
			die('<div class="k_blad">Wystąpił błąd: '.$err.'</div>');
		}
		
		$przesylkaID = $this->_request->getParam('przesylka');
		
		$zamowienia = new Zamowienia();
		$wiadomosci = new Wiadomosci();
		$pocztapolskadane = new Pocztapolskadane();
		if(!empty($przesylkaID))
		{
			$przesylka = $pocztapolskadane->wypiszNumer($przesylkaID);
			if(is_array($przesylka) && count($przesylka) > 0) $przesylki = array($przesylka);
			else die('<div class="k_blad">Nie znaleziono przesyłki: <b>'.$przesylkaID.'</b></div>');
			//var_dump(count($przesylka));
		}
		else $przesylki = $pocztapolskadane->wypiszSprStatus();
		if(count($przesylki) > 0)
		foreach($przesylki as $przesylka)
		{
			//var_dump($przesylka);
			$numer = "testp0"; // testp0, testp1, testp-1, testp-2, testp-99
			$numer = $przesylka['nr_nad'];
			$result = $client->call("sprawdzPrzesylkePl", array("numer" => $numer), $NAMESPACE, '', $header);
			//var_dump($result);

			if($client->fault)
			{
				echo '<div class="k_blad">Błąd przesyłki <b>'.$numer.'</b></div>';
				continue;
			}
			else
			{
				$return = @$result['return'];
				$status = @$return['status'];
				//var_dump($return);
				if($status == 0)
				{
					$danePrzesylki = $return['danePrzesylki']; //var_dump($danePrzesylki);
					$zdarzenia = @$danePrzesylki['zdarzenia']['zdarzenie']; //var_dump($zdarzenia);
					if(empty($przesylkaID) && count($zdarzenia) > 0)
					{
						$z = intval($this->_request->getParam('z', 9999));
						$z = min($z, count($zdarzenia) - 1);
						$zdarzenie = $zdarzenia[$z]; //var_dump($zdarzenie);
						if(@strtotime($zdarzenie['czas']) > strtotime($przesylka['status_czas']))
						{
							if(@!in_array($zdarzenie['kod'], array('P_NAD','P_WD','P_A','P_D'))) $blad = "";
							else $blad = $wiadomosci->wyslijStatusPoczta($przesylka, $danePrzesylki, false);
							if(empty($blad))
							{
								$pocztapolskadane->id = $przesylka['id'];
								$dane['status_czas'] = $zdarzenie['czas'].':00';
								$dane['status_konczace'] = intval($zdarzenie['konczace'] == 'true');
								//var_dump($dane);
								$pocztapolskadane->edytuj($dane);
								echo 'numer '.$numer.' = '.$zdarzenie['czas'].': '.$dane['status_konczace'].'<br/>';
								if($zdarzenie['kod'] == "P_NAD")
								{
									$stat = $zamowienia->changeStatusAll($przesylka['id_zam'], 'wyslane', 'on', null);
									echo 'Zdarzenie przesyłki <b>'.$numer.'</b> = '.$zdarzenie['kod'].': '.$stat.'<br/>';
								}
							}
							else
							{
								echo 'email '.$numer.' = '.$zdarzenie['czas'].': '.$blad.'<br/>';
							}
						}
					}
					if(!empty($przesylkaID))
					{
						echo $wiadomosci->wyslijStatusPoczta($przesylka, $danePrzesylki, true);
					}
				}
				else
				{
					$echo = 'Status przesyłki <b>'.$numer.'</b> = '.$status.': ';
					if($status == 1) $echo.= 'są inne przesyłki o takim numerze';
					if($status == 2) $echo.= 'przesyłka o podanym numerze jest w systemie, ale nie ma zdarzeń w podanym okresie';
					if($status == -1) $echo.= 'w systemie nie ma przesyłki o takim numerze.<br/>Uwaga! Web service uwzględnia jedynie przesyłki posiadające choć jedno zdarzenie w przeciągu ostatnich 30 dni.';
					if($status == -2) $echo.= 'podany numer przesyłki jest błędny';
					if($status == -99) $echo.= 'inny błąd';
					if(empty($przesylkaID)) echo $echo.'<br/>';
					else echo '<div class="k_blad">'.$echo.'</div>';
				}
			}
			//break;
		}
		else die('<div class="k_blad">Nie znaleziono przesyłek</div>');
	}
	
	function gusAction()
	{
		require_once '../application/models/simplehtmldom/simple_html_dom.php';
		$this->_helper->viewRenderer->setNoRender();
		
		if(function_exists('curl_exec'))
		{
			$URL = "http://www.stat.gov.pl/regon/";
			//echo phpinfo();
			$cookie = $_SERVER['DOCUMENT_ROOT'].'/'.$this->baseUrl.'/public/cookies.txt';//dirname(__FILE__)
			$captchaURL = $URL."Captcha.jpg?".rand(1,999);
			if(!$this->_request->isPost())
			{
				$captcha = file_get_contents($captchaURL);
				file_put_contents("captcha.jpg", $captcha);
				echo $captchaSRC = '<img src="'.$this->baseUrl.'/public/captcha.jpg" alt="captcha.jpg" />';
				//echo 'x';
				$head = array
				(
					'POST /regon/ HTTP/1.1',
					'Host: www.stat.gov.pl',
					'User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:18.0) Gecko/20100101 Firefox/18.0 FirePHP/0.7.1',
					'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
					'Accept-Language: pl,en-us;q=0.7,en;q=0.3',
					'Accept-Encoding: gzip, deflate',
					'Referer: http://www.stat.gov.pl/regon/',
					'x-insight: activate',
					'Connection: keep-alive'
				); 
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $URL);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
				curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
				//curl_setopt($ch, CURLOPT_POST, true);
				//curl_setopt($ch, CURLOPT_POSTFIELDS, 'queryTypeRBSet=1nip&crmr300=&crmr311=7872110362&verifCodeTF=crmr3');
				$opis = curl_exec($ch);
				//var_dump($opis);
				if(curl_errno($ch))
				{
					echo ''.curl_errno($ch).':'.curl_error($ch).'';
				}
				//else echo $opis;
				curl_close($ch);
				
				$html = str_get_html($opis);
				$formDiv = $html->find('div[id=formDiv]', 0);
				$regonlineForm = $formDiv->find('form[id=regonlineForm]', 0);
				$regonlineForm->action = "";
				$by_1nip_RB = $formDiv->find('input[id=by_1nip_RB]', 0);
				$by_1nip_RB->checked = "checked";
				$criterion1TF = $formDiv->find('input[id=criterion1TF]', 0);
				$criterion1TF->value = 6871792935;
				$captchaImg = $formDiv->find('span[id=captchaImg] img', 0);
				$captchaImg->src = $captchaURL;
				$verifCodeTF = $formDiv->find('input[id=verifCodeTF]', 0);
				$verifCodeTF->value = "";
				$sendQueryB = $formDiv->find('input[id=sendQueryB]', 0);
				$sendQueryB->type = "submit";
				//echo $captchaImg;
				echo $formDiv;
			}
			//$dbResponseDiv = $html->find('div[id=dbResponseDiv]', 0);
			//var_dump($div->innertext);
			if($this->_request->isPost())
			{
				//var_dump($_POST);//die();
				unset($_POST['sendQueryB']);
				echo $post = http_build_query($_POST);
				//die();
				$head = array
				(
					'POST /regon/ HTTP/1.1',
					'Host: www.stat.gov.pl',
					'User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:18.0) Gecko/20100101 Firefox/18.0 FirePHP/0.7.1',
					'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
					'Accept-Language: pl,en-us;q=0.7,en;q=0.3',
					'Accept-Encoding: gzip, deflate',
					'Referer: http://www.stat.gov.pl/regon/',
					'x-insight: activate',
					'Connection: keep-alive'
				); 
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $URL);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				//curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
				//curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
				//curl_setopt($ch, CURLOPT_POSTFIELDS, 'queryTypeRBSet=1nip&crmr300=&crmr311=7872110362&verifCodeTF=crmr3');
				$opis = curl_exec($ch);
				//var_dump($opis);
				if(curl_errno($ch))
				{
					echo ''.curl_errno($ch).':'.curl_error($ch).'';
				}
				//else echo $opis;
				curl_close($ch);
				
				$ok = true;
				$html = str_get_html($opis);
				$nocookieT = $html->find('p[id=nocookieT]', 0);
				if(@strlen($nocookieT->innertext) > 0)
				{ echo $nocookieT; $ok = false; }
				$messageP = $html->find('p[class=messageP]', 0);
				if(@strlen($messageP->innertext) > 0)
				{ echo $messageP; $ok = false; }
			
				if($ok) echo $html;
			}
		}
	}
	
	function inposttestAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		require_once 'inpost/inpost.php';
		$paczkomaty = new Paczkomaty();
		$inpost = new Paczkomatydane();
		$dane = $inpost->wypisz();
		//$result = inpost_get_params();
		//$result = inpost_check_environment(1);
		$email = 'james890@gmail.com';		
		//$result = inpost_find_customer($email);
		//$result = inpost_pay_for_pack($dane['login'], $dane['haslo'], $packkode);
		//$result = inpost_set_customer_ref($dane['login'], $dane['haslo'], $packkode, $customerRef);
		$parameters['startdate'] = date('Y-m-d', time() - 3600 * 24 * 1);
		$parameters['enddate'] = date('Y-m-d');
		//$result = inpost_get_cod_report($dane['login'], $dane['haslo'], $parameters);
		//$parameters['status'] = 'Prepared';
		//$parameters['is_conf_printed'] = 1;
		//$result = inpost_get_packs_by_sender($dane['login'], $dane['haslo'], $parameters);
		//$result = inpost_create_customer_partner($dane['login'], $dane['haslo'], $parameters);
		@var_dump($result);die();
	}
	
	function paczkomatyAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
		require_once 'inpost/inpost.php';		
		
		$town = $this->_request->getParam('town');
		$pobr = $this->_request->getParam('pobr');
		$code = $this->_request->getParam('code');
		if(!empty($code))
		$paczkomaty = inpost_find_nearest_machines($code, $pobr);
		else
		$paczkomaty = inpost_get_machine_list($town, $pobr);
		//var_dump($paczkomaty);die();
		
		$odpowiedz = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
		$odpowiedz.= '<root>';
        if(is_array($paczkomaty) && count($paczkomaty) > 0)
		{
			foreach($paczkomaty as $paczkomat)
			{
				$odpowiedz.= '<paczkomat>';
				if(count($paczkomat) > 0)
				foreach($paczkomat as $id => $value)
				{
					$odpowiedz.= '<'.$id.'>'.$value.'</'.$id.'>';
				}
				$odpowiedz.= '</paczkomat>';
			}
        }
		$odpowiedz.= '</root>';
		
		echo $odpowiedz;
	}
	
	function stripAccents($nazwa)
	{
		//var_dump($nazwa);		
		if(false)
		$nazwa = strtr
		(
			utf8_decode($nazwa),
			utf8_decode($polishNO.$polishBigNO), 
			$polishOK.$polishBigOK
		);		
		//var_dump($nazwa);//die();
		return $nazwa;
	}
	
	function urzadwydaniaeprzesylkiAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		
        $input = $this->_request->getParam('fraza', '');
		
		//$miasta = array('ssss', 'tttt', 'śśśśś');
		//setlocale(LC_ALL, 'pl_PL.UTF-8');
		//$ok = setlocale(LC_COLLATE, 'pl_PL.UTF-8');
		//var_dump(resourcebundle_locales());
		//sort($miasta, SORT_LOCALE_STRING);
		//$col = new Collator('bg_BG');
		//$col->asort( $miasta );
		//usort($miasta, 'strcoll');
		//usort($miasta, array($this, 'cmpPL'));
		//var_dump($miasta);die();
		
		$pocztaPolskaDane = new Pocztapolskadane('admin');
		$pocztaPolskaDane->urzadWydaniaEPrzesylkiPobierz();
		
		$odpowiedz = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
		$odpowiedz.= '<root>';
		if(@count($_SESSION['urzadWydaniaEPrzesylki']) > 0)		
		{
			foreach($_SESSION['urzadWydaniaEPrzesylki'] as $urzad)
			{
				if(mb_stripos($urzad['miejscowosc'], $input, 0, "UTF-8") !== false				
				//|| mb_stripos($urzad['wojewodztwo'], $input, 0, "UTF-8") !== false
				//|| mb_stripos($urzad['powiat'], $input, 0, "UTF-8") !== false
				//|| mb_stripos($urzad['miejsce'], $input, 0, "UTF-8") !== false
				|| mb_stripos($urzad['kodPocztowy'], $input, 0, "UTF-8") !== false)
				//|| @mb_stripos($urzad['ulica'], $input, 0, "UTF-8") !== false
				//|| @mb_stripos($urzad['numerDomu'], $input, 0, "UTF-8") !== false
				{
					$urzadWydaniaEPrzesylki[$urzad['id']] = $urzad;
				}
			}
			
			if(@count($urzadWydaniaEPrzesylki) > 0)
			{
				$urzadWydaniaEPrzesylki = $pocztaPolskaDane->urzadWydaniaEPrzesylkiSort($urzadWydaniaEPrzesylki);
				//var_dump(($urzadWydaniaEPrzesylki));die();
				foreach($urzadWydaniaEPrzesylki as $urzad)
				{
					$odpowiedz.= '<urzad>';
					$odpowiedz.= '<id>'.$urzad['id'].'</id>';
					$odpowiedz.= '<wojewodztwo>'.stripslashes($urzad['wojewodztwo']).'</wojewodztwo>';
					$odpowiedz.= '<powiat>'.stripslashes($urzad['powiat']).'</powiat>';
					$odpowiedz.= '<miejsce>'.stripslashes($urzad['miejsce']).'</miejsce>';
					$odpowiedz.= '<kodPocztowy>'.stripslashes($urzad['kodPocztowy']).'</kodPocztowy>';
					$odpowiedz.= '<miejscowosc>'.stripslashes($urzad['miejscowosc']).'</miejscowosc>';
					$odpowiedz.= '<ulica>'.@stripslashes($urzad['ulica']).'</ulica>';
					$odpowiedz.= '<numerDomu>'.@stripslashes($urzad['numerDomu']).'</numerDomu>';
					$odpowiedz.= '</urzad>';
				}			
			}
			else $odpowiedz.= '<urzad><id>0</id><miejscowosc>Brak wyników wyszukiwania</miejscowosc></urzad>';
		}
		else $odpowiedz.= '<urzad><id>0</id><miejscowosc>Nie można pobrać punktów odbioru przesyłek</miejscowosc></urzad>';
		$odpowiedz.= '</root>';
		
		echo $odpowiedz; die();
		//echo "$nazwa|$nazwa\n";
    }
}
?>