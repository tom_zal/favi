<?php
class Ajax_CronController extends Ogolny_Controller_Ajax
{
	private $tryb;
	
    public function init() 
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
		$this->view->obConfig = $this->obConfig = new Zend_Config_Ini('../application/config.ini', 'general');
		$this->tryb = $this->obConfig->tryb;
		$this->trybCena = $this->obConfig->trybCena;
		$this->allegroTryb = $this->obConfig->allegroTryb;
		$this->allegroTrybCena = $this->obConfig->allegroTrybCena;
		//echo 'x';die();
    }
	
	function testAction()
	{
		set_time_limit(0);
		$this->_helper->viewRenderer->setNoRender();
		
		$konto = $this->_request->getParam('konto', 1);
		$this->konto = new Allegrokonta();
		$this->webapi = $this->konto->zaloguj($konto);
		
		$nr = $this->_request->getParam('nr', 0);
		$array = array('transactions-ids-array' => $nr);
		$info = $this->webapi->GetPostBuyFormsDataForSellers($array);
		echo '<pre>';var_dump($info);echo '</pre>';
		
		if(@count($info[0]['post-buy-form-gd-address']) > 0)
		{
			$gdAddress = $info[0]['post-buy-form-gd-address'];
			$name = $gdAddress['post-buy-form-adr-full-name'];
			if(true && strpos($name, "Paczkomat") !== false)
			{
				$kontrahent['paczkomat'] = trim(str_replace("Paczkomat", "", $name));
				$kontrahent['paczkomat_miasto'] = $gdAddress['post-buy-form-adr-city'];
			}
			if(true && strpos($name, "UP ") !== false)
			{
				$pocztaPolskaDane = new Pocztapolskadane('admin');
				$street = $gdAddress['post-buy-form-adr-street'];
				$postcode = $gdAddress['post-buy-form-adr-postcode'];
				$city = $gdAddress['post-buy-form-adr-city'];
				$inputs = array('ulica' => $street, 'kodPocztowy' => str_replace("-","",$postcode), 'miejscowosc' => $city);
				$urzadWydaniaEPrzesylki = $pocztaPolskaDane->urzadWydaniaEPrzesylkiSzukaj("", $inputs, false);
				if(count($urzadWydaniaEPrzesylki) == 1)
				$kontrahent['poczta_punkt_odbioru'] = key($urzadWydaniaEPrzesylki);
				$kontrahent['poczta_punkt_odbioru_miasto'] = $city;
				echo '<br/><pre>';var_dump($inputs);echo '</pre><br/>';
				echo '<pre>';var_dump($urzadWydaniaEPrzesylki);echo '</pre>';
			}
			var_dump($kontrahent);
		}
		//else var_dump($info[0]['post-buy-form-gd-address']);
		
		$przesylki1 = $this->webapi->objectToArray($this->webapi->GetShipmentData());
		$przesylki = $przesylki1['shipment-data-list']; 
		echo '<pre>';var_dump($przesylki);echo '</pre>';
		
		die();
	}
   
	function allegrodealsAction() 
	{
		set_time_limit(0);
		$start = date("Y-m-d H-i");
		$this->_helper->viewRenderer->setNoRender();
		
		$oAllegrokonta = new Allegrokonta();
		$aKonto = $oAllegrokonta->findByCronStart();
		$allegroKontoID = count($aKonto) > 0 ? intval($aKonto['id']) : 1; 
		$allegroKontoID = intval($this->_request->getParam('konto', $allegroKontoID));
		
		$godzin = $this->_request->getParam('godzin', 4);
		$minut = $this->_request->getParam('minut', 0);
		$nr = $this->_request->getParam('nr', 0);
		//if(isset($this->view->godzin) && $this->view->godzin > 0) $godzin = $this->view->godzin;
		//if(isset($this->view->minut) && $this->view->minut > 0) $minut = $this->view->minut;
		//if(isset($this->view->nr) && $this->view->nr > 0) $nr = $this->view->nr;
		
		$this->konto = new Allegrokonta();
		//$pobierane = $this->konto->listapobierzKlient();		
		//for($pl=0;$pl<count($pobierane);$pl++){
		//$this->konto->ID = $pobierane[$pl]['id'];
		$this->konto->ID = $allegroKontoID;
		$this->config = $config = $this->konto->zobaczklient();
		//print_r($this->config->toArray());		
		$this->view->config = $this->config;
		
		//define('ALLEGRO_ID', 'id');	
		@define('ALLEGRO_COUNTRY', 1);
	
		try
		{
			//$this->webapi = new AllegroWebAPI($this->config['login'],$this->config['haslo'],$this->config['klucz_webapi']);
			//@define('ALLEGRO_ID', $this->webapi->GetUserID($this->config['login']));
			//$this->webapi = new AllegroWebAPI($config['login'],$config['haslo'],$config['klucz_webapi'],$this->webapi->GetUserID($config['login']));
			//$this->webapi->Login();
			$this->webapi = $this->konto->zaloguj($allegroKontoID);
			//$this->allegro = new AllegroClientSoap($this->config, null);
			if($this->webapi == null) $this->view->error = 'Błąd logowania';//die($this->config);
			if($this->webapi == null) $oAllegrokonta->findCronStartNext($allegroKontoID);
		}
		catch(SoapFault $error)
		{
			$this->view->error = 'Błąd1 '.$error->faultcode.': '.$error->faultstring."";
		}
		catch(Exception $error)
		{
			$this->view->error = 'Błąd2 '.$error->faultcode.': '.$error->faultstring."";
		}
		
		//$allegro = new Allegroaukcje();
		//$produkty = new Produkty();
		$przesylki1 = $this->webapi->objectToArray($this->webapi->GetShipmentData());
		$przesylki = $przesylki1['shipment-data-list']; 

		$pocztaPolskaDane = new Pocztapolskadane('admin');
        
		$zamowienia = new AllegroZamowienia();
		if(false && $this->config['point1'] > 0)
		{
			$point = $this->config['point1'];
		}
		else
		{
			$point = 0;
		}
		$point = $this->_request->getParam('point', $point);
		
		$log = 'start='.$start.' konto='.$allegroKontoID.' point='.$point.' godzin='.$godzin.' nr='.$nr.'<br>';
		
		if(isset($this->view->error))
		{
			$log .= ''.$this->view->error.'<br>';
		}
		else
		while($point !== null)
		{
			$zmiany = $this->webapi->GetSiteJournalDeals(array('journal-start' => $point));
			$zmiany = $this->webapi->objectToArray($zmiany);			
			
			$point = (count($zmiany) == 100) ? $zmiany[99]['deal-event-id'] : null;
			if($point === null) $point2 = @$zmiany[count($zmiany)-1]['deal-event-id'];
			
			$log .= 'zmiany='.count($zmiany).' point='.$point.'<br>';
			
			$z = 0;
			$licznik = 0;
			foreach($zmiany as $item)
			{
				$log .= date("Y-m-d H:i", $item['deal-event-time']).' item='.$item['deal-item-id'].' event='.$item['deal-event-type'];
				$log .= ' deal='.$item['deal-id'].' transaction='.$item['deal-transaction-id'].' buyer='.$item['deal-buyer-id'].'<br>';
				//if($z++ > 0) break;
				try
				{
					$time = $item['deal-event-time'];
					if(time() - $time > ($godzin * 3600 + $minut * 60)) continue;
					if($nr > 0 && $nr != $item['deal-item-id']) continue;
					$date = date('Y-m-d H:i:s', $time);
					
					//if($nr > 0 || @$_GET['item']){echo '<pre>';var_dump($item);echo '</pre>';}
					
					$info = null;
					if($item['deal-transaction-id'] > 0)
					{
						$array = array('transactions-ids-array' => $item['deal-transaction-id']);
						$info = $this->webapi->GetPostBuyFormsDataForSellers($array);
						//if($nr > 0 && @$_GET['info']){echo '<pre>';var_dump($info);echo '</pre>';}
						//print_r($info);echo "<br />";
						//if(count($info) == 0) continue;
						if(count($info) > 0) $info = $info[0];
						//echo date('Y-m-d H:i:s').' po GetPostBuyFormsDataForSellers<br/>';
					}
									
					$opcjeuser = array('user-id' => $item['deal-buyer-id'], 'user-login' => 0);
					$wtf = $this->webapi->GetUserLogin($item['deal-buyer-id']);
					if($wtf == 'niezarejestrowany')
					{
						
					}
					else
					{
						$user = $this->webapi->objectToArray($this->webapi->ShowUser($opcjeuser));
						//print_r($user);
					}
					
					$opcje = array
					(
						'item-id' => $item['deal-item-id'], 
						'get-desc' => 0, 
						'get-image-url' => 1,
						'get-attribs' => 0, 
						'get-postage-options' => 0, 
						'get-company-info' => 0
					);

					$infoAll1 = $this->webapi->objectToArray($this->webapi->ShowItemInfoExt($opcje));
						
					$info1 = $infoAll1['item-list-info-ext'];
					$foto1 = $infoAll1['item-img-list'];
					
					unset($dane);
				
					if($item['deal-event-type'] == 1)
					{
						$sprawdz = $zamowienia->selectWybraneZamowienieAll($item['deal-id']);
				
						if(isset($sprawdz) && !empty($sprawdz)) 
						{
							$log .= 'event=1 istnieje deal '.$item['deal-id'].' id '.$sprawdz['id'].'<br>';
						}
						else
						{
							$log .= 'event=1 nie istnieje deal '.$item['deal-id'].'<br>';
							
							$opcje1 = array('items-array' => $item['deal-item-id']);
							$userinfo = $this->webapi->objectToArray($this->webapi->GetPostBuyData($opcje1));
							
							for($a=0;$a<count($userinfo[0]['users-post-buy-data']);$a++)
							{
								if($userinfo[0]['users-post-buy-data'][$a]['user-data']['user-login']==@$user['user-login'])
								{
									$user1 = $userinfo[0]['users-post-buy-data'][$a];
								}
							}

							$kontrahent['nr_zam']=$item['deal-id'];
							$kontrahent['deal_transaction_id'] = $item['deal-transaction-id'];
							$kontrahent['id_oferty'] = $item['deal-item-id'];
							$kontrahent['id_buyer'] = $item['deal-buyer-id'];
							$kontrahent['nazwa']=$user1['user-data']['user-login'];
							$kontrahent['punkty']=$user1['user-data']['user-rating'];
							$kontrahent['sprzedawca'] = $info1['it-seller-login'];
							$kontrahent['online_typ']='nowa';
							$kontrahent['email'] = $user1['user-data']['user-email'];
							//$kontrahent['nazwa'] = $info['post-buy-form-buyer-login'];
							//$kontrahent['kwotazaplata'] = $info['post-buy-form-amount'];
							//$kontrahent['koszt_dostawy'] = $info['post-buy-form-postage-amount'];
							//$kontrahent['uwagi'] = $info['post-buy-form-msg-to-seller'];
							//$kontrahent['status'] = 'allsystem';
							$kontrahent['platnosc']='Kupujący nie wypełnił formularza pozakupowego';
							
							$kontrahent['nazwa_firmy2'] = $user1['user-data']['user-company'];
							$kontrahent['imie2'] = $user1['user-data']['user-first-name'].' '.$user1['user-data']['user-last-name'];
							$kontrahent['nazwisko2'] = $user1['user-data']['user-last-name'];
							$kontrahent['telefon2'] = $user1['user-data']['user-phone'];
							$kontrahent['kod2'] = $user1['user-data']['user-postcode'];
							$kontrahent['miasto2'] = $user1['user-data']['user-city'];
							$kontrahent['ulica2'] = $user1['user-data']['user-address'];							
							
							if($user1['user-sent-to-data']['user-id'] > 0)
							{
								//adres do wysyłki
								$kontrahent['inny_adres_dostawy'] = 1;
								$kontrahent['nazwa_firmy_korespondencja'] = $user1['user-sent-to-data']['user-company'];
								$kontrahent['imie_korespondencja'] = $user1['user-sent-to-data']['user-first-name'];
								$kontrahent['imie_korespondencja'].= ' '.$user1['user-sent-to-data']['user-last-name'];
								$kontrahent['nazwisko_korespondencja'] = $user1['user-sent-to-data']['user-last-name'];
								//$kontrahent['telefon_korespondencja'] = $user1['user-sent-to-data']['post-buy-form-adr-phone'];
								$kontrahent['kod_korespondencja'] = $user1['user-sent-to-data']['user-postcode'];
								$kontrahent['miasto_korespondencja'] = $user1['user-sent-to-data']['user-city'];
								$kontrahent['ulica_korespondencja'] = $user1['user-sent-to-data']['user-address'];
							}
							else
							{
								$kontrahent['inny_adres_dostawy'] = 1;
								$kontrahent['nazwa_firmy_korespondencja'] = $user1['user-data']['user-company'];
								$kontrahent['imie_korespondencja'] = $user1['user-data']['user-first-name'];
								$kontrahent['imie_korespondencja'].= ' '.$user1['user-data']['user-last-name'];
								$kontrahent['nazwisko_korespondencja'] = $user1['user-data']['user-last-name'];
								$kontrahent['telefon_korespondencja'] = $user1['user-data']['user-phone'];
								$kontrahent['kod_korespondencja'] = $user1['user-data']['user-postcode'];
								$kontrahent['miasto_korespondencja'] = $user1['user-data']['user-city'];
								$kontrahent['ulica_korespondencja'] = $user1['user-data']['user-address'];
							}
							//faktura
							//$kontrahent['faktura'] = $info['post-buy-form-invoice-option'];
							//$kontrahent['nazwa_firmy'] = $user1['user-data']['user-company'];
							//$kontrahent['imie'] = $user1['user-data']['user-first-name'];
							//$kontrahent['nazwisko'] = $user1['user-data']['user-last-name'];
							//$kontrahent['telefon'] = $user1['user-data']['user-phone'];
							//$kontrahent['nip'] = $user1['user-data']['user-email'];
							//$kontrahent['kod'] = $user1['user-data']['user-postcode'];
							//$kontrahent['miasto'] = $user1['user-data']['user-city'];
							//$kontrahent['ulica'] = $user1['user-data']['user-address'];
							
							$kontrahent['faktura'] = $info['post-buy-form-invoice-option'];
							$kontrahent['nazwa_firmy'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-company'];
							$kontrahent['imie'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-full-name'];
							$kontrahent['telefon'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-phone'];
							$kontrahent['nip'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-nip'];
							$kontrahent['kod'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-postcode'];
							$kontrahent['miasto'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-city'];
							$kontrahent['ulica'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-street'];
							
							//var_dump($kontrahent);
							//die();
							$licznik++;
		
							if(isset($sprawdz) && !empty($sprawdz))
							{
								if(empty($sprawdz['uwagi']))
								{
									$kontrahent['uwagi'] = $info['post-buy-form-msg-to-seller'];
								}
								$edytujKontrahenta = $zamowienia->edytuj($sprawdz['id'], $kontrahent);
								$log .= 'event=1 edycja zam '.$sprawdz['id'].' = '.$edytujKontrahenta.'<br>';
							}
							else
							{
								$kontrahent['status'] = 'allsystem';
								$kontrahent['uwagi'] = $info['post-buy-form-msg-to-seller'];
								$dodajKontrahenta = $zamowienia->addKontrahent($kontrahent);
								$log .= 'event=1 dodano zam '.$dodajKontrahenta.'<br>';
							}
							   
							//###############################################################3
							if(isset($sprawdz) && !empty($sprawdz))
							{
								$produkt['id_zam']=$sprawdz['id'];
							}
							else
							{
								$produkt['id_zam']=$dodajKontrahenta;
							}
							 
							$produkt['nazwa']=$info1['it-name'];
							$produkt['cena_brutto']=$info1['it-price'];
							$produkt['wartosc']=$info1['it-price']*$item['deal-quantity'];
							$produkt['ilosc']=$item['deal-quantity'];
							$produkt['jedn']='szt';
							$produkt['id_prod']=$item['deal-item-id'];
							
							if(!empty($foto))
							{
							   $produkt['foto']=$foto1[0]['image-url'];
							}
						
							$dodajProdukt = $zamowienia->addProdukt($produkt);
							$zamowienia->updateWartoscZamow($dodajKontrahenta);
							$log .= 'event=1 dodano prod '.$produkt['id_prod'].' = '.$dodajProdukt.'<br>';
						}
						unset($foto);
						unset($kontrahent);
						unset($user);
						unset($user1);						
						unset($infoAll);
						unset($info);
					} 
					elseif($item['deal-event-type'] == 2) 
					{
						//print_r($item);
						$sprawdz = $zamowienia->selectWybraneZamowienieAll($item['deal-id']);
						$sprawdz2 = $zamowienia->selectWybraneZamowienieAllid($item['deal-transaction-id']);
						
						$kontrahent['deal_transaction_id'] = $item['deal-transaction-id'];
						$kontrahent['id_oferty'] = $item['deal-item-id'];
						$kontrahent['id_buyer'] = $item['deal-buyer-id'];
						$kontrahent['nr_zam']=$item['deal-id'];
						$kontrahent['nazwa']=@$user['user-login'];
						$kontrahent['punkty']=@$user['user-rating'];
						$kontrahent['sprzedawca'] = $info1['it-seller-login'];
						$kontrahent['online_typ']='nowa';
						$kontrahent['email'] = $info['post-buy-form-buyer-email'];
						$kontrahent['nazwa'] = $info['post-buy-form-buyer-login'];
						$kontrahent['kwotazaplata'] = $info['post-buy-form-amount'];
						$kontrahent['koszt_dostawy'] = $info['post-buy-form-postage-amount'];
						if(@floatval($sprawdz['kwot_wplacona_przez_kupujacego_payu']) >= 0)
						{
							$kontrahent['kwot_wplacona_przez_kupujacego_payu'] = $info['post-buy-form-payment-amount'];
						}
						if(floatval($info['post-buy-form-payment-amount']) > 0) $kontrahent['online_typ'] = 'zakończona';
						if(floatval($sprawdz['kwot_wplacona_przez_kupujacego_payu']) > 0) $kontrahent['online_typ'] = 'zakończona';
						//$kontrahent['uwagi'] = $info['post-buy-form-msg-to-seller'];
						//$kontrahent['status'] = 'allsystem';
						for($a=0;$a<count($przesylki);$a++)
						{
							if($przesylki[$a]['shipment-id'] == $info['post-buy-form-shipment-id'])
							{
								$dostawca = "";
								if(@$_GET['xxx']) echo $item['deal-transaction-id'].'_';
								$kontrahent['wysylka'] = $wysylka = $przesylki[$a]['shipment-name'];								
								if(strpos($wysylka, "List") !== false) $dostawca = "pocztapolska";
								if(strpos($wysylka, "Paczka") !== false) $dostawca = "pocztapolska";
								if(strpos($wysylka, "Paczka24") !== false) $dostawca = "pocztapolska";
								if(strpos($wysylka, "Paczka48") !== false) $dostawca = "pocztapolska";
								if(strpos($wysylka, "Pocztex") !== false) $dostawca = "pocztapolska";
								if(strpos($wysylka, "Kurier48") !== false) $dostawca = "pocztapolska";
								if(strpos($wysylka, "Paczkomaty") !== false) $dostawca = "paczkomaty";
								if(strpos($wysylka, "InPost") !== false) $dostawca = "paczkomaty";
								if(strpos($wysylka, "kurierska") !== false) $dostawca = "dhl";
								$kontrahent['dostawca'] = $dostawca;
								if(@$_GET['xxx']) echo $dostawca.'_';
							
								if(@count($info['post-buy-form-gd-address']) > 0)
								{
									$gdAddress = $info['post-buy-form-gd-address'];
									$name = $gdAddress['post-buy-form-adr-full-name'];
									if($dostawca == "paczkomaty" && strpos($name, "Paczkomat") !== false)
									{
										$kontrahent['paczkomat'] = trim(str_replace("Paczkomat", "", $name));
										$kontrahent['paczkomat_miasto'] = $gdAddress['post-buy-form-adr-city'];
										if(@$_GET['xxx']) echo $kontrahent['paczkomat'].'-'.$kontrahent['paczkomat_miasto'];
									}
									if($dostawca == "pocztapolska" && strpos($name, "UP ") !== false)
									{
										$street = $gdAddress['post-buy-form-adr-street'];
										$postcode = $gdAddress['post-buy-form-adr-postcode'];
										$city = $gdAddress['post-buy-form-adr-city'];
										$inputs = array('ulica' => $street, 'kodPocztowy' => str_replace("-","",$postcode), 'miejscowosc' => $city);
										$urzadWydaniaEPrzesylki = $pocztaPolskaDane->urzadWydaniaEPrzesylkiSzukaj("", $inputs, false);
										if(count($urzadWydaniaEPrzesylki) == 1)
										$kontrahent['poczta_punkt_odbioru'] = key($urzadWydaniaEPrzesylki);
										$kontrahent['poczta_punkt_odbioru_miasto'] = $city;
										if(@$_GET['xxx']) echo $kontrahent['poczta_punkt_odbioru'].'-'.$kontrahent['poczta_punkt_odbioru_miasto'];
									}
								}
								if(@$_GET['xxx']) echo '<br>';
							}
						}
						if($info['post-buy-form-pay-type']=='collect_on_delivery')
						{
							$kontrahent['potwierdzenie']=1;
							$kontrahent['platnosc']='Płacę przy odbiorze';
							$kontrahent['online_typ']='pobranie';
						}
						elseif($info['post-buy-form-pay-type']=='wire_transfer')
						{
							$kontrahent['potwierdzenie']=1;
							$kontrahent['platnosc']='Zwykły przelew';
						}
						elseif($info['post-buy-form-pay-type']=='co')
						{
							$kontrahent['platnosc']='Checkout PayU';
							$kontrahent['online']=1;
						}
						elseif($info['post-buy-form-pay-type']=='ai')
						{
							$kontrahent['platnosc']='Raty PayU';
							$kontrahent['online']=1;
						}
						else
						{
							$kontrahent['platnosc']='PayU';
							$kontrahent['online']=1;
						}						
						
						//adres do wysyłki
						if(isset($sprawdz) && !empty($sprawdz))
						{
							$sprawdz['nazwa_firmy_korespondencja'] = trim($sprawdz['nazwa_firmy_korespondencja']);
							$sprawdz['imie_korespondencja'] = trim($sprawdz['imie_korespondencja']);
							$sprawdz['telefon_korespondencja'] = trim($sprawdz['telefon_korespondencja']);
							$sprawdz['kod_korespondencja'] = trim($sprawdz['kod_korespondencja']);
							$sprawdz['miasto_korespondencja'] = trim($sprawdz['miasto_korespondencja']);
							$sprawdz['ulica_korespondencja'] = trim($sprawdz['ulica_korespondencja']);
						}
						
						$kontrahent['alert_zm_danych_w_korespondencja'] = 0;
						$kontrahent['inny_adres_dostawy'] = 1;
						if(empty($sprawdz['nazwa_firmy_korespondencja'])){ $kontrahent['nazwa_firmy_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-company'];} elseif($sprawdz['nazwa_firmy_korespondencja'] !== trim($info['post-buy-form-shipment-address']['post-buy-form-adr-company'])) { $kontrahent['alert_zm_danych_w_korespondencja'] = 1; $kontrahent['nazwa_firmy_korespondencja2'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-company']; $kontrahent['nazwa_firmy_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-company']; }
						if(empty($sprawdz['imie_korespondencja'])){ $kontrahent['imie_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-full-name'];} elseif($sprawdz['imie_korespondencja'] !== trim($info['post-buy-form-shipment-address']['post-buy-form-adr-full-name'])) { $kontrahent['alert_zm_danych_w_korespondencja'] = 2; $kontrahent['imie_korespondencja2'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-full-name']; $kontrahent['imie_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-full-name']; }
						if(empty($sprawdz['telefon_korespondencja'])){ $kontrahent['telefon_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-phone'];} elseif($sprawdz['telefon_korespondencja'] !== trim($info['post-buy-form-shipment-address']['post-buy-form-adr-phone'])) { $kontrahent['alert_zm_danych_w_korespondencja'] = 3; $kontrahent['telefon_korespondencja2'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-phone']; $kontrahent['telefon_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-phone']; }
						if(empty($sprawdz['kod_korespondencja'])){ $kontrahent['kod_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-postcode'];} elseif($sprawdz['kod_korespondencja'] !== trim($info['post-buy-form-shipment-address']['post-buy-form-adr-postcode'])) { $kontrahent['alert_zm_danych_w_korespondencja'] = 4; $kontrahent['kod_korespondencja2'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-postcode']; $kontrahent['kod_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-postcode']; }
						if(empty($sprawdz['miasto_korespondencja'])){ $kontrahent['miasto_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-city'];} elseif($sprawdz['miasto_korespondencja'] !== trim($info['post-buy-form-shipment-address']['post-buy-form-adr-city'])) { $kontrahent['alert_zm_danych_w_korespondencja'] = 5; $kontrahent['miasto_korespondencja2'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-city']; $kontrahent['miasto_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-city']; }
						if(empty($sprawdz['ulica_korespondencja'])){ $kontrahent['ulica_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-street'];} elseif($sprawdz['ulica_korespondencja'] !== trim($info['post-buy-form-shipment-address']['post-buy-form-adr-street'])) { $kontrahent['alert_zm_danych_w_korespondencja'] = 6; $kontrahent['ulica_korespondencja2'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-street']; $kontrahent['ulica_korespondencja'] = $info['post-buy-form-shipment-address']['post-buy-form-adr-street']; }
					
						$kontrahent['data'] = $info['post-buy-form-shipment-address']['post-buy-form-created-date'];
					
						//faktura
						if(empty($sprawdz['faktura'])){ $kontrahent['faktura'] = $info['post-buy-form-invoice-option'];}
						if(empty($sprawdz['nazwa_firmy'])){ $kontrahent['nazwa_firmy'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-company'];}
						if(empty($sprawdz['imie'])){ $kontrahent['imie'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-full-name'];}
						if(empty($sprawdz['telefon'])){ $kontrahent['telefon'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-phone'];}
						if(empty($sprawdz['nip'])){ $kontrahent['nip'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-nip'];}
						if(empty($sprawdz['kod'])){ $kontrahent['kod'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-postcode'];}
						if(empty($sprawdz['miasto'])){ $kontrahent['miasto'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-city'];}
						if(empty($sprawdz['ulica'])){ $kontrahent['ulica'] = $info['post-buy-form-invoice-data']['post-buy-form-adr-street'];}
					
						$log .= 'event=2 kwot_wplacona_przez_kupujacego_payu = '.@$kontrahent['kwot_wplacona_przez_kupujacego_payu'].'<br>';
						if(isset($sprawdz2) && !empty($sprawdz2))
						{
							$log .= 'event=2 istnieje trans '.$item['deal-transaction-id'].' id '.$sprawdz2['id'].'<br>';
							if(empty($sprawdz2['uwagi']))
							{
								$kontrahent['uwagi'] = $info['post-buy-form-msg-to-seller'];
							}
							//if($nr > 0){echo '<pre>';var_dump($kontrahent);echo '</pre>';}
							$edytujKontrahenta = $zamowienia->edytuj($sprawdz2['id'], $kontrahent);
							$log .= 'event=2 edycja zam '.$sprawdz2['id'].' = '.$edytujKontrahenta.'<br>';
							$usunProduktAll = $zamowienia->usunProduktAll($sprawdz2['id']);
							$log .= 'event=2 usunieto prods '.$sprawdz2['id'].' = '.$usunProduktAll.'<br>';
							if(isset($sprawdz) && !empty($sprawdz) && $sprawdz['id'] != $sprawdz2['id'])
							{
								$log .= 'event=2 istnieje deal '.$item['deal-id'].' id '.$sprawdz2['id'].'<br>';
								$zamowienia->deleteWybrany($sprawdz['id']);
								$log .= 'event=2 usuwanie zam '.$sprawdz['id'].'<br>';
							}
						}
						elseif(isset($sprawdz) && !empty($sprawdz))
						{
							$log .= 'event=2 istnieje deal '.$item['deal-id'].' id '.$sprawdz['id'].'<br>';
							if(empty($sprawdz['uwagi']))
							{
								$kontrahent['uwagi'] = $info['post-buy-form-msg-to-seller'];
							}
							//if($nr > 0){echo '<pre>';var_dump($kontrahent);echo '</pre>';}
							$edytujKontrahenta = $zamowienia->edytuj($sprawdz['id'], $kontrahent);
							$log .= 'event=2 edycja zam '.$sprawdz['id'].' = '.$edytujKontrahenta.'<br>';
							$usunProduktAll = $zamowienia->usunProduktAll($sprawdz['id']);
							$log .= 'event=2 usunieto prods '.$sprawdz['id'].' = '.$usunProduktAll.'<br>';
						}
						else
						{
							$log .= 'event=2 nie istnieje deal '.$item['deal-id'].' ani trans '.$item['deal-transaction-id'].'<br>';
							$kontrahent['uwagi'] = $info['post-buy-form-msg-to-seller'];
							$dodajKontrahenta = $zamowienia->addKontrahent($kontrahent);
							$log .= 'event=2 dodano zam '.$dodajKontrahenta.'<br>';
						}
					
						//produkty
						for($p=0;$p<count($info['post-buy-form-items']);$p++)
						{
							$produkt['nazwa']=$info['post-buy-form-items'][$p]['post-buy-form-it-title'];
							$produkt['cena_brutto']=$info['post-buy-form-items'][$p]['post-buy-form-it-price'];
							$produkt['wartosc']=$info['post-buy-form-items'][$p]['post-buy-form-it-amount'];
							$produkt['ilosc']=$info['post-buy-form-items'][$p]['post-buy-form-it-quantity'];
							$produkt['jedn']='szt';
							$produkt['id_prod']=$info['post-buy-form-items'][$p]['post-buy-form-it-id'];
							$opcje1 = array
							(
								'item-id' => $info['post-buy-form-items'][$p]['post-buy-form-it-id'], 
								'get-desc' => 0, 
								'get-image-url' => 1,
								'get-attribs' => 0, 
								'get-postage-options' => 0, 
								'get-company-info' => 0
							);
							$infoAll3 = $this->webapi->objectToArray($this->webapi->ShowItemInfoExt($opcje1));
							$info3 = $infoAll3['item-list-info-ext'];
							$foto = $infoAll3['item-img-list'];
							if(!empty($foto))
							{
								$produkt['foto']=$foto[0]['image-url'];
							}
							if(isset($sprawdz2) && !empty($sprawdz2))
							{
								$produkt['id_zam']=$sprawdz2['id'];
							}
							elseif(isset($sprawdz) && !empty($sprawdz))
							{
								$produkt['id_zam']=$sprawdz['id'];
							}
							else
							{
								$produkt['id_zam']=$dodajKontrahenta;
							}
							/*	
							if(empty($produkt['id_zam']))
							{
								 print_r($produkt); 
								 print_r($sprawdz2); 
								 print_r($sprawdz); 
								 die(); 
							}
							*/
							if(isset($sprawdz2) && !empty($sprawdz2))
							{
								$edytujProdukt1 = $zamowienia->edytujProduktAll($sprawdz2['id'], $produkt['id_prod'], $produkt);
								$log .= 'event=2 edycja prods '.$sprawdz2['id'].' = '.$edytujProdukt1.'<br>';
								if(!isset($edytujProdukt1) || empty($edytujProdukt1))
								{
									$edytujProdukt = $zamowienia->addProdukt($produkt);
									$log .= 'event=2 dodano prod '.$edytujProdukt.'<br>';
								}
							}
							elseif(isset($sprawdz) && !empty($sprawdz))
							{
								$edytujProdukt1 = $zamowienia->edytujProduktAll($sprawdz['id'], $produkt['id_prod'], $produkt);
								$log .= 'event=2 edycja prods '.$sprawdz['id'].' = '.$edytujProdukt1.'<br>';
								if(!isset($edytujProdukt1) || empty($edytujProdukt1))
								{
									$edytujProdukt = $zamowienia->addProdukt($produkt);
									$log .= 'event=2 dodano prod '.$edytujProdukt.'<br>';
								}
							}
							else
							{
								$edytujProdukt = $zamowienia->addProdukt($produkt);
								$log .= 'event=2 dodano prod '.$edytujProdukt.'<br>';
							}
							$data[$p]=$produkt;
						}
						
						if(isset($sprawdz2) && !empty($sprawdz2))
						{
							$zamowienia->updateWartoscZamow($sprawdz2['id']);
						}
						elseif(isset($sprawdz1) && !empty($sprawdz1))
						{
							$zamowienia->updateWartoscZamow($sprawdz['id']);
							$mess = true;
						}
						else
						{
							$zamowienia->updateWartoscZamow($dodajKontrahenta);
							$mess = true;
						}
						$wiadomosci = new Wiadomosci();
						if(isset($mess) && false)
						{
							$blad = $wiadomosci->noweZamowienieAllegro($kontrahent, $data, $this->www, false, $this->obConfig->tryb, true);
						}
						
						unset($foto);
						unset($kontrahent);
						unset($user);
						unset($produkt);
						unset($infoAll);
						unset($info);
					}
					elseif($item['deal-event-type'] == 3)
					{
						$sprawdz = $zamowienia->selectWybraneZamowienieAll($item['deal-id']);
						$sprawdz2 = $zamowienia->selectWybraneZamowienieAllid($item['deal-transaction-id']);						
						
						$kontrahent['deal_transaction_id'] = $item['deal-transaction-id'];
						$kontrahent['id_oferty'] = $item['deal-item-id'];
						$kontrahent['id_buyer'] = $item['deal-buyer-id'];
						$kontrahent['potwierdzenie']=2;
						//$kontrahent['status']='allanulowane';
						if(isset($sprawdz) && !empty($sprawdz))
						{
							//if($nr > 0){echo '<pre>';var_dump($kontrahent);echo '</pre>';}
							$edytujKontrahenta = $zamowienia->edytuj($sprawdz['id'], $kontrahent);
							$log .= 'event=3 edycja zam '.$sprawdz['id'].' = '.$edytujKontrahenta.'<br>';
						}
						else $log .= 'event=3 nie istnieje deal '.$item['deal-id'].'<br>';
						
						unset($foto);
						unset($kontrahent);
						unset($user);
						unset($produkt);
						unset($infoAll);
						unset($info);
					}
					elseif($item['deal-event-type'] == 4)
					{
						//if($nr > 0){echo '<pre>';var_dump($info);echo '</pre>';}
						$sprawdz = $zamowienia->selectWybraneZamowienieAll($item['deal-id']);
						$sprawdz2 = $zamowienia->selectWybraneZamowienieAllid($item['deal-transaction-id']);

						$kontrahent['deal_transaction_id'] = $item['deal-transaction-id'];
						$kontrahent['id_oferty'] = $item['deal-item-id'];
						$kontrahent['id_buyer'] = $item['deal-buyer-id'];
						$kontrahent['online_typ'] = 'zakończona';
						//$kontrahent['status']='allrealizacja';
						$kontrahent['potwierdzenie'] = 1;
						
						if(true)
						{
							$kontrahent['kwot_wplacona_przez_kupujacego_payu'] = $info['post-buy-form-payment-amount'];
							$log .= 'event=4 kwot_wplacona_przez_kupujacego_payu = '.$info['post-buy-form-payment-amount'].'<br>';
						}
						
						if(isset($sprawdz) && !empty($sprawdz))
						{
							//if($nr > 0){echo '<pre>';var_dump($kontrahent);echo '</pre>';}
							$edytujKontrahenta = $zamowienia->edytuj($sprawdz['id'], $kontrahent); 
							$log .= 'event=4 edycja zam '.$sprawdz['id'].' = '.$edytujKontrahenta.'<br>';
						}
						else $log .= 'event=4 nie istnieje deal '.$item['deal-id'].'<br>';
						
						unset($foto);
						unset($kontrahent);
						unset($user);
						unset($produkt);
						unset($infoAll);
						unset($info);
					}
					$log .= 'koniec eventa<br>';
				}
				catch(SoapFault $error)
				{
					$log .= 'Błąd '.$error->faultcode.': '.$error->faultstring.'<br>';
				}
			}
			//if($godzin > 0) $point = null;
			if(@$point2 !== null) // && (time() - $zmiany[99]['deal-event-time'] > ($godzin * 3600 + $minut * 60)))
			{
				$this->konto->edytuj(array('point1' => $point2));
				$log .= 'Zmiana point1 na '.$point2.'<br>';
			}
			$log .= 'Pobrano zamówienia<br>';
		}
		
		$log .= 'koniec='.date("Y-m-d H-i").'<br>';
		file_put_contents("logs/allegro/".$start.'.txt', str_replace("<br>", PHP_EOL, $log));
		echo $log;
		$oAllegrokonta->findCronStartNext($allegroKontoID);
		
		$time = time();
		$now = date("Y-m-d");
		$logi  = glob("logs/allegro/*.txt");
		if(count($logi) > 0)
		foreach($logi as $log)
		{
			$file = basename($log);
			//$file = str_replace("log_", "", $file);
			$date = substr($file, 0, 10);
			//var_dump($date);die();
			if($time - strtotime($date) > 3600 * 24 * 31) unlink($log);
		}
		//var_dump($logi);die();
    }	
	
    function fedexAction()
	{
        $this->_helper->viewRenderer->setNoRender();
        $fedex = new FedexWebApi();        
        $test = $fedex->AddressValidation();         
    }
    
    function enAction()
	{
        $this->_helper->viewRenderer->setNoRender();
        
        $en = new ElektronicznyNadawca();
        try
		{
			$czysc = new clearEnvelope();
			$parametry = new getEnvelopeBufor();
			$bufor = $en->getEnvelopeBufor($parametry);
			$bufor = @$bufor['przesylka'];
			$wyczysc = $en->clearEnvelope($czysc);
			$pocztapolskadane = new Pocztapolskadane();
			if(count($bufor) > 0)
			{
				if(!isset($bufor[0])) $bufor = array(0 => $bufor);
				foreach($bufor as $przes)
				{
					$pocztapolskadane->usunGUID($przes['guid']);
				}
			}
        }
		catch(SoapFault $error)
		{
			
		}
    }
}
?>