<?php

class Ajax_PorownywarkiController extends Ogolny_Controller_Ajax 
{
    public function init() 
	{
        parent::init();
		$this->local_path = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].$this->baseUrl);
		$this->local_path.= '/public/admin/porownywarki';
		//http://www.jakubmielnikiewicz.pl/sklep/public/admin/porownywarki/nokaut.xml
    }
	
	function indexAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
	}
	
	function exportxmlAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		
		ob_clean();		
		set_time_limit(0);		
		ini_set("memory_limit", "512M");
		
		$typy = array('nokaut', 'ceneo', 'skapiec');
		
		$typ = $this->_request->getParam('typ');
		if(!empty($typ)) $typy = array($typ);
		
		$produkty = new Produkty();
		$all = $produkty->wypiszProduktyPodzieloneStrona(false, 0, 3);
		$all = $this->produktyGaleria($all);
		//var_dump($all[2]);die();
		$kategs = new Kategorie();
		$kategs->showAll(0, 0, 'id');
		$kategs->getPaths();
		$kategorie = $kategs->katAll;
		//var_dump($this->obConfig);die();
		$katprod = new Katprod();
		$kolprod = new Kolorproduktu();
		$rozmprod = new Rozmiarproduktu();
		
		if(@count($typy) > 0)
		foreach($typy as $typ)
		{
		
		$XML_FILE = $this->local_path.'/'.$typ.'.xml';

		if(false && is_writable($XML_FILE) === false)
		{
			echo ('Nie mam praw do zapisu pliku ' . $XML_FILE).'<br/>';
			continue;
		}
		
		$xmlAll = '';
		$fxml = fopen($XML_FILE, "w");

		$openingTag = '<' . '?';
		$closingTag = '?' . '>';

		if($typ == 'nokaut')
		{
			$xmlForHeader = '<?xml version="1.0" encoding="UTF-8" ?>'.PHP_EOL;
			$xmlForHeader.= '<!DOCTYPE nokaut SYSTEM "http://www.nokaut.pl/integracja/nokaut.dtd">'.PHP_EOL;
			$xmlForHeader.= '<nokaut generator="BigComCMS" ver="1.0">'.PHP_EOL;
			$xmlForHeader.= '<offers>'.PHP_EOL;
		}
		if($typ == 'ceneo')
		{
			$xmlForHeader = '<?xml version="1.0" encoding="UTF-8" ?>'.PHP_EOL;
			$xmlForHeader.= '<offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1">'.PHP_EOL;
		}
		if($typ == 'skapiec')
		{
			$xmlForHeader = '<?xml version="1.0" encoding="UTF-8" ?>'.PHP_EOL;
			$xmlForHeader.= '<xmldata>'.PHP_EOL;
			$xmlForHeader.= '<version>12.0</version>'.PHP_EOL;
			$xmlForHeader.= '<header>'.PHP_EOL;
			$xmlForHeader.= '	<name>'.$this->ustawienia['title'].'</name>'.PHP_EOL;
			$xmlForHeader.= '	<www>'.$this->www.'</www>'.PHP_EOL;
			$xmlForHeader.= '	<time>'.date('Y-m-d').'</time>'.PHP_EOL;
			$xmlForHeader.= '</header>'.PHP_EOL;
			$xmlForHeader.= '<category>'.PHP_EOL;
			if(count($kategorie) > 0)
			foreach($kategorie as $kateg)
			{
			$xmlForHeader.= '	<catitem>'.PHP_EOL;
			$xmlForHeader.= '		<catid>'.$kateg['id'].'</catid>'.PHP_EOL;
			$xmlForHeader.= '		<catname><![CDATA['.$kateg['path'].']]></catname>'.PHP_EOL;
			$xmlForHeader.= '	</catitem>'.PHP_EOL;
			}
			$xmlForHeader.= '</category>'.PHP_EOL;
			$xmlForHeader.= '<data>'.PHP_EOL;
		}
		
		$xmlAll .= $xmlForHeader;
		fwrite($fxml, $xmlForHeader);
		
		if(@count($all) > 0)
		foreach($all as $offer)
		{
			$id = intval($offer['id']);
			$nazwa = trim($offer['nazwa']);
			$tekst = stripslashes($offer['tekst']);
			$link = $this->www.'/'.$offer['link'];
			$imgBig = $this->www.'/public/admin/zdjecia/'.$offer['zdj']['img'];
			$imgSmall = $this->www.'/public/admin/zdjecia/miniaturki/'.$offer['zdj']['img'];
			$producent = @strval($offer['producent']['nazwa']);
			$kategoria = count($offer['kateg']) > 0 ? $offer['kateg'][count($offer['kateg'])-1] : null;
			$kategoriaID = count($kategoria) > 0 ? $kategoria['id'] : 0;
			$kategoriaNazwa = count($kategoria) > 0 ? $kategoria['nazwa'] : '';
			if($typ == 'ceneo')
			$kategoriaPath = $katprod->getPathString($id, $offer['kateg'], '/', '/', '');
			else
			$kategoriaPath = $katprod->getPathString($id, $offer['kateg'], '/', '>', ' ');
			$kolor = @strval($offer['kolory'][0]['nazwa']);
			$kolory = $kolprod->getKoloryString($id, $offer['kolory'], ',', ' ');
			$rozmiarowka = @intval($offer['rozmiarowka']['id']);
			if($typ == 'ceneo')
			$rozmiary = $rozmprod->getRozmiaryString($id, $offer['rozmiary'], $rozmiarowka, ';', '');
			else
			$rozmiary = $rozmprod->getRozmiaryString($id, $offer['rozmiary'], $rozmiarowka, ',', ' ');
			
			if($typ == 'nokaut')
			{
				$xmlForOffer = "<offer>".PHP_EOL;
				$xmlForOffer.= "	<id>{$id}</id>".PHP_EOL;
				$xmlForOffer.= "	<name><![CDATA[{$nazwa}]]></name>".PHP_EOL;
				$xmlForOffer.= "	<description><![CDATA[{$tekst}]]></description>".PHP_EOL;
				$xmlForOffer.= "	<url>{$link}</url>".PHP_EOL;
				$xmlForOffer.= "	<image>{$imgBig}</image>".PHP_EOL;
				$xmlForOffer.= "	<price>{$offer['brutto']}</price>".PHP_EOL;
				$xmlForOffer.= "	<category><![CDATA[{$kategoriaPath}]]></category>".PHP_EOL;
				$xmlForOffer.= "	<producer><![CDATA[{$producent}]]></producer>".PHP_EOL;
				$xmlForOffer.= "	<property name='rozmiary'>{$rozmiary}</property>".PHP_EOL;
				$xmlForOffer.= "	<property name='kolory'>{$kolory}</property>".PHP_EOL;
				//$xmlForOffer.= "	<property name='EAN'>{$offer['ean']}</property>".PHP_EOL;
				$xmlForOffer.= "	<instock>{$offer['dostepnosc']}</instock>".PHP_EOL;
				$xmlForOffer.= "	<availability>0</availability>".PHP_EOL;
				//$xmlForOffer.= "	<warranty></warranty>".PHP_EOL;
				//$xmlForOffer.= "	<shipping>12</shipping>".PHP_EOL;
				$xmlForOffer.= "	<code><![CDATA[{$offer['kod']}]]></code>".PHP_EOL;
				if(@floatval($offer['waga']) > 0)
				$xmlForOffer.= "	<weight>{$offer['waga']}</weight>".PHP_EOL;
				//$xmlForOffer.= "	<variant>{$wariant}</variant>".PHP_EOL;
				//$xmlForOffer.= "	<promo></promo>".PHP_EOL;
				$xmlForOffer.= "</offer>".PHP_EOL;
			}
			
			if($typ == 'ceneo')
			{
				$grupaCeneo = @!empty($offer['grupa_ceneo']) ? $offer['grupa_ceneo'] : 'other';
				$xmlForOffer = "<group name='{$grupaCeneo}'>".PHP_EOL;
				$params = " id='{$id}'";
				$params.= " url='{$link}'";
				$params.= " price='{$offer['brutto']}'";
				$params.= " avail='1'";
				$params.= " set='0'";
				if(@floatval($offer['waga']) > 0)
				$params.= " weight='{$offer['waga']}'";
				$params.= " basket='1'";
				$params.= " stock='{$offer['dostepnosc']}'";
				$xmlForOffer.= "<o{$params}>".PHP_EOL;
				$xmlForOffer.= "	<cat><![CDATA[{$kategoriaPath}]]></cat>".PHP_EOL;
				$xmlForOffer.= "	<name><![CDATA[{$nazwa}]]></name>".PHP_EOL;
				$xmlForOffer.= "	<imgs>".PHP_EOL;
				$xmlForOffer.= "	<main url='{$imgBig}'/>".PHP_EOL;
				$xmlForOffer.= "	<i url='{$imgSmall}'/>".PHP_EOL;
				$xmlForOffer.= "	</imgs>".PHP_EOL;
				$xmlForOffer.= "	<desc><![CDATA[{$tekst}]]></desc>".PHP_EOL;
				$attrs = "		<a name='Producent'><![CDATA[{$producent}]]></a>".PHP_EOL;
				$attrs.= "		<a name='Model'><![CDATA[{$offer['kod']}]]></a>".PHP_EOL;
				//$attrs.= "<a name='EAN'><![CDATA[{$offer['ean']}]]></a>".PHP_EOL;
				if($grupaCeneo == 'clothes')
				{
					$attrs.= "		<a name='Kolor'><![CDATA[{$kolor}]]></a>".PHP_EOL;
					$attrs.= "		<a name='Rozmiar'><![CDATA[{$rozmiary}]]></a>".PHP_EOL;
					$attrs.= "		<a name='Kod_produktu'><![CDATA[]]></a>".PHP_EOL;
					$attrs.= "		<a name='Sezon'><![CDATA[]]></a>".PHP_EOL;
					$attrs.= "		<a name='Fason'><![CDATA[]]></a>".PHP_EOL;
					$attrs.= "		<a name='ProductSetId'><![CDATA[]]></a>".PHP_EOL;
				}
				else
				{
					$attrs.= "		<a name='Kod_producenta'><![CDATA[]]></a>".PHP_EOL;
				}
				$xmlForOffer.= "	<attrs>".PHP_EOL."{$attrs}	</attrs>".PHP_EOL;
				$xmlForOffer.= "</o>".PHP_EOL;
				$xmlForOffer.= "</group>".PHP_EOL;
			}
			
			if($typ == 'skapiec')
			{
				$xmlForOffer = "<item>".PHP_EOL;
				$xmlForOffer.= "	<compid>{$id}</compid>".PHP_EOL;
				$xmlForOffer.= "	<vendor><![CDATA[{$producent}]]></vendor>".PHP_EOL;
				$xmlForOffer.= "	<name><![CDATA[{$nazwa}]]></name>".PHP_EOL;
				$xmlForOffer.= "	<price>{$offer['brutto']}</price>".PHP_EOL;
				//$xmlForOffer.= "	<partnr></partnr>".PHP_EOL;
				$xmlForOffer.= "	<catid>{$kategoriaID}</catid>".PHP_EOL;
				$xmlForOffer.= "	<foto>{$imgBig}</foto>".PHP_EOL;
				$xmlForOffer.= "	<desclong><![CDATA[{$tekst}]]></desclong>".PHP_EOL;
				$xmlForOffer.= "	<availability>2</availability>".PHP_EOL;
				//$xmlForOffer.= "	<ean>{$offer['ean']}</ean>".PHP_EOL;
				$xmlForOffer.= "	<url>{$link}</url>".PHP_EOL;
				if(false)
				{
				$xmlForOffer.= "	<delivery>".PHP_EOL;
				$xmlForOffer.= "		<pcash></pcash>".PHP_EOL;
				$xmlForOffer.= "		<ptransfer></ptransfer>".PHP_EOL;
				$xmlForOffer.= "		<pcard></pcard>".PHP_EOL;
				$xmlForOffer.= "		<ccash></ccash>".PHP_EOL;
				$xmlForOffer.= "		<ctransfer></ctransfer>".PHP_EOL;
				$xmlForOffer.= "		<ccard></ccard>".PHP_EOL;
				$xmlForOffer.= "	</delivery>".PHP_EOL;
				}
				$xmlForOffer.= "	<attribute name='model'><![CDATA[{$offer['kod']}]]></attribute>".PHP_EOL;
				$xmlForOffer.= "	<attribute name='rozmiary'>{$rozmiary}</attribute>".PHP_EOL;
				$xmlForOffer.= "	<attribute name='kolory'>{$kolory}</attribute>".PHP_EOL;
				$xmlForOffer.= "</item>".PHP_EOL;
			}
			
			$xmlAll .= $xmlForOffer;
			fwrite($fxml, $xmlForOffer);
		}

		if($typ == 'nokaut')
		{
			$xmlForFooter = '</offers>'.PHP_EOL;
			$xmlForFooter .= '</nokaut>';
		}
		if($typ == 'ceneo')
		{
			$xmlForFooter = '</offers>';
		}
		if($typ == 'skapiec')
		{
			$xmlForFooter = '</data>'.PHP_EOL;
			$xmlForFooter .= '</xmldata>';
		}

		$xmlAll .= $xmlForFooter;
		fwrite($fxml, $xmlForFooter);
		
		echo str_replace(PHP_EOL, '<br/>', htmlspecialchars($xmlAll)).'<br/><br/>';
		
		fclose($fxml);
		}
    }
}
?>