<?php
class Ajax_ImportController extends Ogolny_Controller_Page
{
	private $tryb;
	
    public function init() 
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
		$this->tryb = $this->obConfig->allegroTryb;
		$this->trybCena = $this->obConfig->allegroTrybCena;
		$this->test = $this->_request->getParam('test', false);
		
		set_time_limit(0);
		ini_set("memory_limit", "512M");
		
		flush();
		ob_flush();
		ob_clean();
		
		if(false)
		{
			echo '<br/>'.memory_get_usage(0)/1024/1024;
			echo '<br/>'.memory_get_usage(1)/1024/1024;
			echo '<br/>'.memory_get_peak_usage(0)/1024/1024;
			echo '<br/>'.memory_get_peak_usage(1)/1024/1024;
		}
		//die();
		
		$frontController = Zend_Controller_Front::getInstance();
		$frontController->setParam('disableOutputBuffering', true);
    }
	
	function indexAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
        echo 'ok';
    }
	
	function exportxls2Action()
	{
		$this->_helper->viewRenderer->setNoRender();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image');
		
		$dane = $this->_request->getPost('dane');
		
		$file = $this->_request->getParam('file');
		$sort = $this->_request->getParam('sort', 'nazwa');
		$ile = intval($this->_request->getParam('ile', 500));
		$od = intval($this->_request->getParam('od', 0));
		$zipIt = intval($this->_request->getParam('zip', 1));
		
		$tabelaCount = 0;
		$zip = new ZipArchive();
		
		$import = new Import();		
		try
		{
			$produkty = new Produkty();
			$zawartosc = $produkty->wypiszProduktyEksport($sort, $od > 0);
			if(count($zawartosc) == 0)
			{
				echo '<error>Brak produktów</error>';
				break;
			}
			
			$tabelaCount = count($zawartosc);
			$xls = $file.'-'.date('Y-m-d').'.xls';
			//echo '<file>'.$file.'</file>';
			
			$filenamexls = 'admin/bazy/'.$file.'-'.date('Y-m-d').'.xls';
			$filenamezip = 'admin/bazy/'.$file.'-'.date('Y-m-d').'.zip';
			//@mkdir('admin/bazy/'.$file.'-'.date('Y-m-d'));
			
			if($zipIt)
			if($zip->open($filenamezip, ZIPARCHIVE::CREATE) !== TRUE) 
			{
				echo '<error>Nie można utworzyć archiwum zip</error>';
			}
			
			if(true)
			{
				$workbook = new Spreadsheet_Excel_Writer($filenamexls);
				$workbook->setVersion(8);
				$workbook->setCountry(48);
				$format_bold = & $workbook->addFormat();
				$format_bold->setBold();
				$format_bold->setAlign('center');
				$format_bold->setHAlign('center');
				$format_bold->setTextWrap();
				$format_center = & $workbook->addFormat();
				$format_center->setAlign('center');
				$format_left = & $workbook->addFormat();
				$format_left->setAlign('left');
				$format_right = & $workbook->addFormat();
				$format_right->setAlign('right');
				$worksheet = & $workbook->addWorksheet();
				$worksheet->setInputEncoding('UTF-8');
			}
			if(true)
			{
				$worksheet->setColumn(0, 0, 5);
				$worksheet->setColumn(1, 1, 50);
				$worksheet->setColumn(2, 30, 12);
				$i = 0;
				$worksheet->write(0, $i++, 'LP', $format_bold);
				foreach($import->opisy as $co => $nazwa)
				if(!isset($import->czyPole[$co]) || $import->czyPole[$co])
				$worksheet->write(0, $i++, ucfirst($nazwa), $format_bold);
			}
			if($od > 0)
			{
				$data = new Spreadsheet_Excel_Reader();
				$data->setUTFEncoder('iconv');
				$data->setOutputEncoding('UTF-8');
				$data->read($filenamexls);
				if(@count($data->sheets) > 0)
				for($i = 2; $i <= $data->sheets[0]['numRows']; $i++)
				{
					$cells = @$data->sheets[0]['cells'][$i];
					$j = 0;
					
					if(count($cells) > 0)
					if(true)
					{
						$worksheet->writeRow($i - 1, $j, $cells);
					}
					else
					{
						$worksheet->write($i - 1, $j++, $i - 1, $format_center);
						if(count($import->opisy) > 0)
						foreach($import->opisy as $co => $nazwa)
						if(!isset($import->czyPole[$co]) || $import->czyPole[$co])
						{
							$format = $format_center;
							if(in_array($co, $import->leftAligns)) $format = $format_left;
							if(in_array($co, $import->rightAligns)) $format = $format_right;
							
							if(in_array($co, $import->floats))
							$cell = @trim(str_replace(",", ".", $cells[$j + 1]));
							else $cell = @trim($cells[$j + 1]);
							
							if(!empty($cell))
							{
								if(in_array($co, $import->strings))
								$worksheet->writeString($i - 1, $j, $cell, $format);
								else
								$worksheet->write($i - 1, $j, $cell, $format);
							}
							$j++;
						}
					}
				}
			}			
		}
		catch(Exception $e)
		{
			echo '<error>'.$e->getMessage().'</error>';
		}
		
		$k = 0;
		if(@count($zawartosc) > 0)
		try
		{
			$odIle = $od;// * $ile + 0;
			$doIle = min($odIle + $ile, $tabelaCount);
			$next = ($doIle >= $tabelaCount);
			//if(false)
			for($i = $odIle; $i < $doIle; $i++)
			{
				$j = 0;
				$worksheet->write($i + 1, $j++, $i + 1, $format_center);
				foreach($import->opisy as $co => $nazwa)
				if(!isset($import->czyPole[$co]) || $import->czyPole[$co])
				{
					$format = $format_center;
					if(in_array($co, $import->leftAligns)) $format = $format_left;
					if(in_array($co, $import->rightAligns)) $format = $format_right;
					
					if(in_array($co, $import->strings))
					$worksheet->writeString($i + 1, $j++, $zawartosc[$i][$co], $format);
					else
					$worksheet->write($i + 1, $j++, $zawartosc[$i][$co], $format);
				}
				//break;
				$k++;
				//if($i==$odIle) echo '<i>'.$i.'</i>';
				//if($i==$doIle-1) echo '<ii>'.$i.'</ii>';
			}
			$workbook->close();
			unset($workbook);
		}
		catch(Exception $e)
		{
			echo '<error>'.$e->getMessage().'</error>';
		}
		else $next = 1;
		
		if($zipIt && $next)
		{
			if(@file_exists($filenamexls))
			{
				if($zip->addFile($filenamexls, $xls) !== TRUE) 
				echo '<error>Nie można dodać '.$filenamexls.' do archiwum</error>';
			}
			else echo '<error>Nie udało się stworzyć pliku '.$filenamexls.'</error>';
			
			$zip->close();
		}
		
		echo '<ileRows>'.($doIle - $odIle).'</ileRows>';
		echo '<ileRowsAll>'.$tabelaCount.'</ileRowsAll>';
		echo '<ileRecords>'.($k+0).'</ileRecords>';
		echo '<next>'.$next.'</next>';
        echo '</root>';
	}
	
	function exportxlsAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image');
		
		$session = new Zend_Session_Namespace('eksport_produkty');
		
		$tryb = $this->_request->getParam('tryb', 'panel');
		$dane = $this->_request->getPost('dane');
		
		$file = $this->_request->getParam('file');
		$sort = $this->_request->getParam('sort', 'nazwa');
		$ile = intval($this->_request->getParam('ile', 1000));
		$od = intval($this->_request->getParam('od', 0));
		$zipIt = intval($this->_request->getParam('zip', 1));
		
		$tabelaCount = 0;
		$zip = new ZipArchive();
		
		$import = new Import($tryb);		
		try
		{
			$produkty = new Produkty();
			$zawartosc = $produkty->wypiszProduktyEksport($sort, $od > 0);
			if(count($zawartosc) == 0)
			{
				echo '<error>Brak produktów</error>';
				break;
			}
			
			$tabelaCount = count($zawartosc);
			$xls = $file.'-'.date('Y-m-d').'.xls';
			//echo '<file>'.$file.'</file>';
			
			$filenamexls = 'admin/bazy/'.$file.'-'.date('Y-m-d').'.xls';
			$filenamezip = 'admin/bazy/'.$file.'-'.date('Y-m-d').'.zip';
			//@mkdir('admin/bazy/'.$file.'-'.date('Y-m-d'));
			
			if($zipIt)
			if($zip->open($filenamezip, ZIPARCHIVE::CREATE) !== TRUE) 
			{
				echo '<error>Nie można utworzyć archiwum zip</error>';
			}
			
			include '../library/PHPExcel/Classes/PHPExcel.php';
			include '../library/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
			
			if($od == 0)
			{
				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getProperties()->setCreator("BigCom");
				$objPHPExcel->getProperties()->setLastModifiedBy("BigCom");
				$objPHPExcel->getProperties()->setTitle("Produkty");
				$objPHPExcel->getProperties()->setSubject("Produkty");
				$objPHPExcel->getProperties()->setDescription("Produkty");
				$objPHPExcel->setActiveSheetIndex(0);
				$sheet = $objPHPExcel->getActiveSheet();
				$sheet->setTitle('Produkty');
			}
			else
			{
				$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
				$cacheSettings = array('memoryCacheSize' => '8MB');
				PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

				//available readers in PHPExcel\Classes\PHPExcel\Reader\
				$objReader = PHPExcel_IOFactory::createReader('Excel5');
				//$objReader = PHPExcel_IOFactory::createReaderForFile($filenamexls);
				//$objReader->setSheetIndex(0);
				//$objReader->setReadDataOnly(true);				
				$objReader->setLoadSheetsOnly(array("Produkty"));
				$session->loading = true;
				$objPHPExcel = $objReader->load($filenamexls);
				//$objPHPExcel = PHPExcel_IOFactory::load($plik);
				$session->loading = false;
				$sheet = $objPHPExcel->getActiveSheet();
				//$rowIterator = $sheet->getRowIterator();				
			}
			//var_dump($objPHPExcel);
			$bold = array('font' => array('bold' => true));

			$horiz_center = PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
			$horiz_left = PHPExcel_Style_Alignment::HORIZONTAL_LEFT;
			$horiz_right = PHPExcel_Style_Alignment::HORIZONTAL_RIGHT;
			$vert_center = PHPExcel_Style_Alignment::VERTICAL_CENTER;
			$vert_top = PHPExcel_Style_Alignment::VERTICAL_TOP;
			$vert_bottom = PHPExcel_Style_Alignment::VERTICAL_BOTTOM;

			$sheet->getDefaultStyle()->getFont()->setName('Arial');
			$sheet->getDefaultStyle()->getFont()->setSize(11);
			if($tryb == 'optima')
			{
				$sheet->getDefaultStyle()->getFont()->setSize(10);
			}
			if($tryb == 'hermes')
			{
				$sheet->getDefaultStyle()->getFont()->setSize(9);
			}
			if($tryb == 'cdn_xl')
			{
				$sheet->getDefaultStyle()->getFont()->setSize(10);
			}
			if($od == 0)
			{
				if($tryb == 'panel')
				{
					$sheet->getStyle('A1')->getAlignment()->setHorizontal($horiz_center)->setVertical($vert_center);
					$sheet->getStyle('A1')->applyFromArray($bold);
					$sheet->SetCellValue('A1', 'LP');
				}
				if($tryb == 'hermes')
				{
					$sheet->getStyle('A1')->getAlignment()->setVertical($vert_center);
					$sheet->getStyle('A1')->getFont()->getColor()->setRGB('008000');
					$sheet->getStyle('A1')->getFont()->setSize(12);
					$sheet->getStyle('A1')->getFont()->setBold(true);
					$sheet->SetCellValue('A1', 'Aktualny stan magazynów');
				}
				if($tryb == 'cdn_xl')
				{
					$sheet->SetCellValue('A1', 'Lista towarów');
				}
				$i = $import->dataColumn;
				if(count($import->opisy) > 0)
				foreach($import->opisy as $co => $nazwa)
				if($tryb != 'panel' || $import->czyPole[$co])
				{
					if($nazwa === '') $nazwa = $co;
					if($nazwa === null) $nazwa = '';
					$colGroup = floor($i / 26) > 0 ? chr(ord('A') + floor($i / 26) - 1) : '';
					$col = $colGroup.chr(ord('A') + $i % 26);
					$colHead = $col.$import->headersRow;
					if(false)
					{
						$format = $horiz_center;
						if(in_array($co, $import->leftAligns)) $format = $horiz_left;
						if(in_array($co, $import->rightAligns)) $format = $horiz_right;
						$sheet->getStyle($col)->getAlignment()->setHorizontal($format);
					}
					if(true)
					{
						$sheet->getStyle($colHead)->getAlignment()->setWrapText(false);
						if(in_array($co, $import->strings))
						$sheet->getStyle($col)->getAlignment()->setHorizontal($horiz_left);
					}
					if($tryb == 'panel')
					{
						$sheet->getColumnDimension($col)->setWidth(12);
						$sheet->getStyle($colHead)->getAlignment()->setWrapText(true);
						$sheet->getStyle($colHead)->getAlignment()->setHorizontal($horiz_center);
						$sheet->getStyle($colHead)->getAlignment()->setVertical($vert_center);
						$sheet->getStyle($colHead)->applyFromArray($bold);
					}
					if($tryb == 'hermes')
					{
						$sheet->getStyle($colHead)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
						$sheet->getStyle($colHead)->getFill()->getStartColor()->setRGB('008000');
						$sheet->getStyle($colHead)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_YELLOW);
						$sheet->getStyle($colHead)->getAlignment()->setVertical($vert_center);
					}
					$sheet->SetCellValue($colHead, ucfirst($nazwa));
					$i++;
				}
				if($tryb == 'panel')
				{
					$sheet->getColumnDimension('A')->setWidth(5);
					$sheet->getColumnDimension('B')->setWidth(5);
					$sheet->getColumnDimension('C')->setWidth(50);
					$sheet->getRowDimension(1)->setRowHeight(50);//-1
				}
				if($tryb == 'optima')
				{
					$sheet->getColumnDimension('A')->setWidth(30);
					$sheet->getColumnDimension('B')->setWidth(70);
					$sheet->getColumnDimension('C')->setWidth(10);
					$sheet->getColumnDimension('D')->setWidth(15);
					$sheet->getColumnDimension('E')->setWidth(10);
					$sheet->getColumnDimension('F')->setWidth(15);
				}
				if($tryb == 'hermes')
				{
					$sheet->getColumnDimension('A:Z')->setWidth(10);
					$sheet->getColumnDimension('B')->setWidth(20);
					$sheet->getRowDimension(1)->setRowHeight(15);
					$sheet->getRowDimension(2)->setRowHeight(15);
				}
				if($tryb == 'cdn_xl')
				{
					$sheet->getColumnDimension('A')->setWidth(20);
					$sheet->getColumnDimension('B')->setWidth(55);
					$sheet->getColumnDimension('C')->setWidth(10);
					$sheet->getColumnDimension('D')->setWidth(7);
					$sheet->getColumnDimension('E')->setWidth(5);
					$sheet->getColumnDimensionByColumn(5)->setWidth(18);
					$sheet->getColumnDimensionByColumn(6)->setWidth(18);
					$sheet->getColumnDimensionByColumn(7)->setWidth(18);
					$sheet->getColumnDimensionByColumn(8)->setWidth(18);
				}
				//$sheet->getColumnDimensionByColumn(0, 0)->setWidth(10);				
			}
		}
		catch(Exception $e)
		{
			echo '<error>'.$e->getMessage().'</error>';
		}
		
		$k = 0;
		if(@count($zawartosc) > 0)
		try
		{			
			$odIle = $od;// * $ile + 0;
			$doIle = min($odIle + $ile, $tabelaCount);
			$next = ($doIle >= $tabelaCount);
			
			$ids = null;
			for($i = $odIle; $i < $doIle; $i++)
			{
				$ids[] = $zawartosc[$i]['id'];
			}
			
			if(@$import->czyPole['kategorie'])
			{
				$katprod = new Katprod();
				$kategorie = $katprod->getKategorieForExport(@$ids);
			}
			if(@$import->czyPole['kolory'])
			{
				$koloryprod = new Kolorproduktu();
				$kolory = $koloryprod->getKoloryForExport(@$ids);
			}
			if(@$import->czyPole['platnosci'])
			{
				$platnosciprodukty = new PlatnosciProdukty();
				$platnosci = $platnosciprodukty->getPlatnosciForExport(@$ids);
			}
			if(@$import->czyPole['powiazania'])
			{
				$produktypowiazania = new Produktypowiazania();
				$powiazania = $produktypowiazania->getPowiazaniaForExport(@$ids);
			}			
			
			//if(false)
			for($i = $odIle; $i < $doIle; $i++)
			{
				if($i % 100 == 0) @file_put_contents('../public/ajax/exportxls.txt', $i);
				if($tryb == 'panel')
				$sheet->SetCellValue('A'.($import->headersRow + $i + 1), $i + 1);
				$j = $import->dataColumn;
				if(count($import->opisy) > 0)
				foreach($import->opisy as $co => $nazwa)
				if($tryb != 'panel' || $import->czyPole[$co])
				{
					$colGroup = floor($j / 26) > 0 ? chr(ord('A') + floor($j / 26) - 1) : '';
					$col = $colGroup.chr(ord('A') + $j % 26).($import->headersRow + $i + 1);
					//echo $j++.' '.$colGroup.' '.$col.'<br>';
					$j++;
					
					if($co == 'kategorie') $zawartosc[$i][$co] = @strval($kategorie[$zawartosc[$i]['id']]);
					if($co == 'kolory') $zawartosc[$i][$co] = @strval($kolory[$zawartosc[$i]['id']]);
					if($co == 'platnosci') $zawartosc[$i][$co] = @strval($platnosci[$zawartosc[$i]['id']]);
					if($co == 'powiazania') $zawartosc[$i][$co] = @strval($powiazania[$zawartosc[$i]['id']]);
					
					//if($co == 'tekst') $zawartosc[$i][$co] = str_replace("&oacute;", 'ó', $zawartosc[$i][$co]);
					if(in_array($co, $import->strings))
					if(strlen($zawartosc[$i][$co]) >= 32767)
					$zawartosc[$i][$co] = strip_tags($zawartosc[$i][$co]);
					$sheet->SetCellValue($col, $zawartosc[$i][$co]);
				}
				//break;
				$k++;
				//if($i==$odIle) echo '<i>'.$i.'</i>';
				//if($i==$doIle-1) echo '<ii>'.$i.'</ii>';
			}
			@file_put_contents('../public/ajax/exportxls.txt', $i);
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			//$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save($filenamexls);
			
			$objPHPExcel->disconnectWorksheets();
			unset($objPHPExcel);
		}
		catch(Exception $e)
		{
			echo '<error>'.$e->getMessage().'</error>';
		}
		else $next = 1;
		
		if($zipIt && $next)
		{
			if(@file_exists($filenamexls))
			{
				if($zip->addFile($filenamexls, $xls) !== TRUE) 
				echo '<error>Nie można dodać '.$filenamexls.' do archiwum</error>';
			}
			else echo '<error>Nie udało się stworzyć pliku '.$filenamexls.'</error>';
			
			$zip->close();
		}
		
		echo '<ileRows>'.($doIle - $odIle).'</ileRows>';
		echo '<ileRowsAll>'.$tabelaCount.'</ileRowsAll>';
		echo '<ileRecords>'.($i+0).'</ileRecords>';
		echo '<next>'.$next.'</next>';
        echo '</root>';
	}
	
	function exportxlsklienciAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image');
		
		$session = new Zend_Session_Namespace('eksport_klienci');
		
		$tryb = $this->_request->getParam('tryb', 'panel');
		$dane = $this->_request->getPost('dane');
		
		$file = $this->_request->getParam('file');
		$sort = $this->_request->getParam('sort', 'nazwa');
		$ile = intval($this->_request->getParam('ile', 1000));
		$od = intval($this->_request->getParam('od', 0));
		$zipIt = intval($this->_request->getParam('zip', 1));
		
		$tabelaCount = 0;
		$zip = new ZipArchive();
		
		$import = new Import($tryb, 'klienci');		
		try
		{
			$klienci = new Kontrahenci();
			$zawartosc = $klienci->wypiszKlienciEksport($sort, $od > 0);
			if(count($zawartosc) == 0)
			{
				echo '<error>Brak klientów</error>';
				break;
			}
			
			$tabelaCount = count($zawartosc);
			$xls = $file.'-'.date('Y-m-d').'.xls';
			//echo '<file>'.$file.'</file>';
			
			$filenamexls = 'admin/bazy/'.$file.'-'.date('Y-m-d').'.xls';
			$filenamezip = 'admin/bazy/'.$file.'-'.date('Y-m-d').'.zip';
			//@mkdir('admin/bazy/'.$file.'-'.date('Y-m-d'));
			
			if($zipIt)
			if($zip->open($filenamezip, ZIPARCHIVE::CREATE) !== TRUE) 
			{
				echo '<error>Nie można utworzyć archiwum zip</error>';
			}
			
			include '../library/PHPExcel/Classes/PHPExcel.php';
			include '../library/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
			
			if($od == 0)
			{
				$objPHPExcel = new PHPExcel();
				$objPHPExcel->getProperties()->setCreator("BigCom");
				$objPHPExcel->getProperties()->setLastModifiedBy("BigCom");
				$objPHPExcel->getProperties()->setTitle("Kontrahenci");
				$objPHPExcel->getProperties()->setSubject("Kontrahenci");
				$objPHPExcel->getProperties()->setDescription("Kontrahenci");
				$objPHPExcel->setActiveSheetIndex(0);
				$sheet = $objPHPExcel->getActiveSheet();
				$sheet->setTitle('Kontrahenci');
			}
			else
			{
				$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
				$cacheSettings = array('memoryCacheSize' => '8MB');
				PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

				//available readers in PHPExcel\Classes\PHPExcel\Reader\
				$objReader = PHPExcel_IOFactory::createReader('Excel5');
				//$objReader = PHPExcel_IOFactory::createReaderForFile($filenamexls);
				//$objReader->setSheetIndex(0);
				//$objReader->setReadDataOnly(true);				
				$objReader->setLoadSheetsOnly(array("Kontrahenci"));
				$session->loading = true;
				$objPHPExcel = $objReader->load($filenamexls);
				//$objPHPExcel = PHPExcel_IOFactory::load($plik);
				$session->loading = false;
				$sheet = $objPHPExcel->getActiveSheet();
				//$rowIterator = $sheet->getRowIterator();				
			}
			//var_dump($objPHPExcel);
			$bold = array('font' => array('bold' => true));

			$horiz_center = PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
			$horiz_left = PHPExcel_Style_Alignment::HORIZONTAL_LEFT;
			$horiz_right = PHPExcel_Style_Alignment::HORIZONTAL_RIGHT;
			$vert_center = PHPExcel_Style_Alignment::VERTICAL_CENTER;
			$vert_top = PHPExcel_Style_Alignment::VERTICAL_TOP;
			$vert_bottom = PHPExcel_Style_Alignment::VERTICAL_BOTTOM;

			$sheet->getDefaultStyle()->getFont()->setName('Arial');
			$sheet->getDefaultStyle()->getFont()->setSize(11);
			if($od == 0)
			{
				if($tryb == 'panel')
				{
					$sheet->getStyle('A1')->getAlignment()->setHorizontal($horiz_center)->setVertical($vert_center);
					$sheet->getStyle('A1')->applyFromArray($bold);
					$sheet->SetCellValue('A1', 'LP');
				}
				$i = $import->dataColumn;
				if(count($import->opisy) > 0)
				foreach($import->opisy as $co => $nazwa)
				if($tryb != 'panel' || $import->czyPole[$co])
				{
					if($nazwa === '') $nazwa = $co;
					if($nazwa === null) $nazwa = '';
					$colGroup = floor($i / 26) > 0 ? chr(ord('A') + floor($i / 26) - 1) : '';
					$col = $colGroup.chr(ord('A') + $i % 26);
					$colHead = $col.$import->headersRow;
					if(false)
					{
						$format = $horiz_center;
						if(in_array($co, $import->leftAligns)) $format = $horiz_left;
						if(in_array($co, $import->rightAligns)) $format = $horiz_right;
						$sheet->getStyle($col)->getAlignment()->setHorizontal($format);
					}
					if(true)
					{
						$sheet->getStyle($colHead)->getAlignment()->setWrapText(false);
						if(in_array($co, $import->strings))
						$sheet->getStyle($col)->getAlignment()->setHorizontal($horiz_left);
					}
					if($tryb == 'panel')
					{
						$sheet->getColumnDimension($col)->setWidth(12);
						$sheet->getStyle($colHead)->getAlignment()->setWrapText(true);
						$sheet->getStyle($colHead)->getAlignment()->setHorizontal($horiz_center);
						$sheet->getStyle($colHead)->getAlignment()->setVertical($vert_center);
						$sheet->getStyle($colHead)->applyFromArray($bold);
					}
					$sheet->SetCellValue($colHead, ucfirst($nazwa));
					$i++;
				}
				$sheet->getColumnDimension('A:Z')->setWidth(100);
				if($tryb != 'panel')
				{
					$sheet->getColumnDimension('A')->setWidth(5);
					$sheet->getColumnDimension('B')->setWidth(5);
					$sheet->getColumnDimension('C')->setWidth(50);
					$sheet->getRowDimension(1)->setRowHeight(50);//-1
				}
				//$sheet->getColumnDimensionByColumn(0, 0)->setWidth(10);				
			}
		}
		catch(Exception $e)
		{
			echo '<error>'.$e->getMessage().'</error>';
		}
		
		$k = 0;
		if(@count($zawartosc) > 0)
		try
		{			
			$odIle = $od;// * $ile + 0;
			$doIle = min($odIle + $ile, $tabelaCount);
			$next = ($doIle >= $tabelaCount);
			
			$ids = null;
			for($i = $odIle; $i < $doIle; $i++)
			{
				$ids[] = $zawartosc[$i]['id'];
			}	
			
			//if(false)
			for($i = $odIle; $i < $doIle; $i++)
			{
				if($i % 100 == 0) @file_put_contents('../public/ajax/exportxlsklienci.txt', $i);
				if($tryb == 'panel')
				$sheet->SetCellValue('A'.($import->headersRow + $i + 1), $i + 1);
				$j = $import->dataColumn;
				if(count($import->opisy) > 0)
				foreach($import->opisy as $co => $nazwa)
				if($tryb != 'panel' || $import->czyPole[$co])
				{
					$colGroup = floor($j / 26) > 0 ? chr(ord('A') + floor($j / 26) - 1) : '';
					$col = $colGroup.chr(ord('A') + $j % 26).($import->headersRow + $i + 1);
					//echo $j++.' '.$colGroup.' '.$col.'<br>';
					$j++;
					
					//if($co == 'tekst') $zawartosc[$i][$co] = str_replace("&oacute;", 'ó', $zawartosc[$i][$co]);
					if(in_array($co, $import->strings))
					if(strlen($zawartosc[$i][$co]) >= 32767)
					$zawartosc[$i][$co] = strip_tags($zawartosc[$i][$co]);
					$sheet->SetCellValue($col, $zawartosc[$i][$co]);
				}
				//break;
				$k++;
				//if($i==$odIle) echo '<i>'.$i.'</i>';
				//if($i==$doIle-1) echo '<ii>'.$i.'</ii>';
			}
			@file_put_contents('../public/ajax/exportxlsklienci.txt', $i);
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			//$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save($filenamexls);
			
			$objPHPExcel->disconnectWorksheets();
			unset($objPHPExcel);
		}
		catch(Exception $e)
		{
			echo '<error>'.$e->getMessage().'</error>';
		}
		else $next = 1;
		
		if($zipIt && $next)
		{
			if(@file_exists($filenamexls))
			{
				if($zip->addFile($filenamexls, $xls) !== TRUE) 
				echo '<error>Nie można dodać '.$filenamexls.' do archiwum</error>';
			}
			else echo '<error>Nie udało się stworzyć pliku '.$filenamexls.'</error>';
			
			$zip->close();
		}
		
		echo '<ileRows>'.($doIle - $odIle).'</ileRows>';
		echo '<ileRowsAll>'.$tabelaCount.'</ileRowsAll>';
		echo '<ileRecords>'.($i+0).'</ileRecords>';
		echo '<next>'.$next.'</next>';
        echo '</root>';
	}
	
	function exportxlsstatusAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		echo 0;
	}
	
	function importxlsAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image');
		
		$dane = $this->_request->getPost('dane');
		
		$tryb = $this->_request->getParam('tryb', 'panel');
		$rodzaj = $this->_request->getParam('rodzaj', 'produkty');
		$typ = $this->_request->getParam('typ', '');
		$file = $this->_request->getParam('file');
		$nowe = $this->_request->getParam('nowe');
		$ile = intval($this->_request->getParam('ile', 200));
		$od = intval($this->_request->getParam('od', 0));
		$zipIt = intval($this->_request->getParam('zip', 0));
		
		$next = 0;
		$plik = 'xls';
		$tabelaCount = 0;
		$zip = new ZipArchive();
		$import = new Import($tryb, $rodzaj, $typ, 'admin', $plik);
		$importData = new Zend_Session_Namespace('importData'.$tryb.$rodzaj.$typ.$plik);
		if($od == 0 || !isset($importData->data)) $importData->data = date('Y-m-d H:i:s');
		$import->dane['data'] = $importData->data;
		
		try
		{
			$produkty = new Produkty();
			
			//$xls = str_replace('.zip', '.xls', $file);
			//echo '<file>'.$file.'</file>';
			$filenamexls = 'admin/zip/ext/'.$file;//.'-'.date('Y-m-d').'.xls';
			$filenamezip = 'admin/bazy/'.$file;//.'-'.date('Y-m-d').'.zip';
			//@mkdir('admin/bazy/'.$file.'-'.date('Y-m-d'));
			
			if($zipIt && $od == 0)
			{
				$import->wypakuj($obConfig->ZipExtDir, $filenamezip, $xls);
				if(strlen($import->bledy) > 0)
				{
					echo '<error>'.$import->bledy.'</error>';
					$next = 1;
				}
			}
			if(@!$next)
			{
				//$dane = $import->wczytajDaneXls($filenamexls);
				if($od == 0 || !isset($importData->tabelaCount)) //$importData->tabelaCount = 0;
				$importData->tabelaCount = $import->wczytajDaneXls($filenamexls, $od, $ile, true);
				$tabelaCount = $importData->tabelaCount;
				$dane = $import->wczytajDaneXls($filenamexls, $od, $ile, false);
				if(strlen($import->bledy) > 0)
				{
					echo '<error><![CDATA['.$import->bledy.']]></error>';
				}
				else if(count($dane) == 0)
				{
					echo '<error>Brak produktów</error>';
				}
			}
			//$tabelaCount = @count($dane);
		}
		catch(Exception $e)
		{
			echo '<error>'.$e->getMessage().'</error>';
		}
		
		if($od == 0) $import->wyczysc();
		
		$k = 0;
		$odIle = $od;// * $ile + 0;
		$doIle = min($odIle + $ile, $tabelaCount);
		if(@count($dane) > 0)
		try
		{
			$next = ($doIle >= $tabelaCount);
			//var_dump($dane);die();
			//if(false)
			$results = $import->wgrajNoweProdukty($file, $dane, $odIle, $doIle);
			if($nowe && $next) $stare = $import->usunStareProdukty($dane);
		}
		catch(Exception $e)
		{
			echo '<error>'.$e->getMessage().'</error>';
		}
		else $next = 1;
		
		echo '<ileRows>'.($doIle - $odIle).'</ileRows>';
		echo '<ileRowsAll>'.$tabelaCount.'</ileRowsAll>';
		echo '<ileRecords>'.@intval($results['nowe']).'</ileRecords>';
		echo '<ileExist>'.@intval($results['exist']).'</ileExist>';
		echo '<ileStare>'.@intval($stare).'</ileStare>';
		if(@count($results['pola']) > 0)
		foreach($results['pola'] as $co => $ilosc)
		{
			echo '<pole>';
			echo '<id>'.$co.'</id>';
			echo '<ile>'.$ilosc.'</ile>';
			echo '</pole>';
		}		
		echo '<next>'.$next.'</next>';
        echo '</root>';
	}
	
	function exportsqlAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image');
		
		$dane = $this->_request->getPost('dane');
		
		$file = $this->_request->getParam('file');
		$tabela = $this->_request->getParam('tabela');
		$ile = intval($this->_request->getParam('ile', 1000));
		$od = intval($this->_request->getParam('od', 0));
		
		$tabelaCount = 0;
		$multiInsert = true;
		$zip = new ZipArchive();
		
		$import = new Import();		
		if(!in_array(strtolower($tabela), $import->notAllowed))
		{
			$tabela = $import->getTabela($tabela);
			if(count($tabela) == 0)
			{
				echo '<error>Brak tabeli '.$this->_request->getParam('tabela').'</error>';
				$next = 1;
				break;
			}
			
			$tabelaCount = $import->getTabelaCount($tabela['Name']);
			$sql = $file.'-'.date('Y-m-d').'-'.$tabela['Name'].'.sql';
			$filenamesql = 'admin/bazy/'.$file.'-'.date('Y-m-d').'/'.$sql;	
			//echo '<filenameSQL>'.$filenameSQL.'</filenameSQL>';
			
			$filenamezip = 'admin/bazy/'.$file.'-'.date('Y-m-d').'.zip';
			@mkdir('admin/bazy/'.$file.'-'.date('Y-m-d'));
				
			if($zip->open($filenamezip, ZIPARCHIVE::CREATE) !== TRUE) 
			{
				echo '<error>Nie można utworzyć archiwum zip</error>';
				$next = 1;
			}
			
			$select = 'CREATE TABLE IF NOT EXISTS `'.$tabela['Name'].'` (';
					
			$kolumny = $import->getKolumny($tabela['Name']);
			if(count($kolumny) > 0)
			foreach($kolumny as $rows)
			{
				$select .= '`'.$rows['Field'].'`';
				$select .= ' '.$rows['Type'].' ';
				$select .= ($rows['Null'] == 'NO') ? 'NOT NULL' : 'NULL';
				$select .= ($rows['Default'] !== null) ? ' DEFAULT \''.$rows['Default'].'\'' : '';
				$select .= ($rows['Extra'] == 'auto_increment') ? ' AUTO_INCREMENT' : '';
				if($rows['Key'] == 'PRI') $key = $rows['Field'];
				$select .= ', ';
			}
			if(isset($key)) $select .= 'PRIMARY KEY (`'.$key.'`)';
			$select .= ')';
			$select .= ' ENGINE='.$tabela['Engine'];
			$charset = explode('_', $tabela['Collation']);
			$select .= ' DEFAULT CHARSET='.$charset[0];
			$select .= ' AUTO_INCREMENT=1';//.$tabela['Auto_increment'];
			$select .= ';';
			//echo $select.'<br>';
			$tableSql = $select;
			$select = '';
			unset($rowSqls);
			
			$wiersze = $import->getWiersze($tabela['Name'], $od * $ile, $ile);
		}
		else $next = 1;
		
		$k = 0;
		if(@count($wiersze) > 0)
		{
			$odIle = $od * $ile + 0;
			$doIle = min($odIle + $ile, $tabelaCount);
			$next = ($doIle >= $tabelaCount);

			for($i = $odIle; $i < $doIle; $i++)
			{
				$wiersz = '';
				for($j = 0; $j < count($kolumny); $j++)
				{
					$int = (strpos($kolumny[$j]['Type'], 'int') !== false);
					$float = (strpos($kolumny[$j]['Type'], 'float') !== false);
					$decimal = (strpos($kolumny[$j]['Type'], 'decimal') !== false);
					$double = (strpos($kolumny[$j]['Type'], 'double') !== false);
					$numeric = ($int || $float || $decimal || $double);
					$value = $wiersze[$i - $odIle][$kolumny[$j]['Field']];
					$null = ($value === NULL);
					if($null) $value = 'NULL';
					if(!$numeric && !$null) $wiersz .= '"';
					//$select .= str_replace('"', '\"', $value);
					$wiersz .= addslashes($value);
					if(!$numeric && !$null) $wiersz .= '"';
					if($j < count($kolumny) - 1) $wiersz .= ', ';
				}
				
				if($multiInsert)
				{
					//if($i % 100 == 0)
					if($i == $odIle || empty($select) || (mb_strlen($select.$wiersz, 'UTF-8') + 2 >= 1024 * 1024))
					{
						if(!empty($select)) $rowSqls[] = $select.';';
						$select = 'INSERT IGNORE INTO `'.$tabela['Name'].'` VALUES';
					}
					else $select.= ',';
				}
				else $select = 'INSERT IGNORE INTO `'.$tabela['Name'].'` VALUES';
				
				$select.= '(';
				$select.= $wiersz;
				$select.= ')';
				
				if(!$multiInsert)
				{
					$select .= ';';
					$rowSqls[] = $select;
				}
				//break;
				$k++;
				//if($i==$odIle) echo '<i>'.$i.'</i>';
				//if($i==$doIle-1) echo '<ii>'.$i.'</ii>';
			}
			if(!empty($select)) $rowSqls[] = $select.';';
		}
		else $next = 1;
		
		if(is_array($tabela) && count($tabela) > 0)
		if(!in_array(strtolower($tabela['Name']), $import->notAllowed))
		{
			$file = @fopen($filenamesql, $od == 0 ? 'w' : 'a');
			{
				if($od == 0 && @!empty($tableSql))
				fwrite($file, $tableSql.'/*DELIM*/'.PHP_EOL);
				if(isset($rowSqls) && count($rowSqls) > 0)
				foreach($rowSqls as $row)
				fwrite($file, $row.'/*DELIM*/'.PHP_EOL);
			}
			@fclose($file);

			if(@file_exists($filenamesql))
			{
				if($zip->addFile($filenamesql, $sql) !== TRUE) 
				echo '<error>Nie można dodać '.$filenamesql.' do archiwum</error>';
			}
			else echo '<error>Nie udało się stworzyć pliku '.$tabela['Name'].'</error>';
			
			$zip->close();
		}		
		
		echo '<ileRows>'.($doIle - $odIle).'</ileRows>';
		echo '<ileRowsAll>'.$tabelaCount.'</ileRowsAll>';
		echo '<ileRecords>'.(@count($rowSqls)+0).'</ileRecords>';
		echo '<next>'.$next.'</next>';
        echo '</root>';
	}
	
	function importsqlAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image');
		
		$dane = $this->_request->getPost('dane');
		
		$file = $this->_request->getParam('file');
		$tabela = $this->_request->getParam('tabela');
		$ile = intval($this->_request->getParam('ile', 100));
		$od = intval($this->_request->getParam('od', 0));
		
		$import = new Import();
		if(!in_array(strtolower($tabela), $import->notAllowed))
		{
			//$filename = $obConfig->ZipFileDir.$file;
			//$filename = substr($file, strlen($obConfig->ZipFileDir), -4);
			$filenameSQL = $obConfig->ZipExtDir.''.str_replace('.zip', '', $file).'-'.$tabela.'.sql';
			//echo '<filenameSQL>'.$filenameSQL.'</filenameSQL>';
			$dane = $import->wczytajDaneSqlAll($filenameSQL);
			if(empty($dane))
			{
				echo '<error>Brak pliku lub brak danych w pliku '.$filenameSQL.'</error>';
				$next = 1;
			}
		}
		else $next = 1;
		
		$j = 0;
		if(@count($dane) > 0)
		{
			if($od == 0)
			{
				$import->wyczyscTabele($tabela);
				if(@!empty($dane[0]))
				$import->insertSql($tabela, $dane[0]);
			}
			
			$odIle = $od * $ile + 1;
			$doIle = min($odIle + $ile, count($dane));
			$next = ($doIle >= count($dane));

			for($i = $odIle; $i < $doIle; $i++)
			{
				$select = trim($dane[$i]);
				if($i > 0 && !empty($select)) $j++;
				if(!empty($select))
				try
				{
					$import->insertSql($tabela, $select);
				}
				catch(Exception $e)
				{
					echo '<error>'.$e->getMessage().'</error>';
					$next = 1;
					break;
				}
				//if($i==$odIle) echo '<i>'.$i.'</i>';
				//if($i==$doIle-1) echo '<ii>'.$i.'</ii>';
			}
		}
		
		echo '<ileRows>'.($j+0).'</ileRows>';
		echo '<ileRowsAll>'.max(0,@count($dane)-2).'</ileRowsAll>';
		echo '<next>'.$next.'</next>';
        echo '</root>';
	}
	
	function importsqlstatusAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$tabela = $this->_request->getParam('tabela');
		$import = new Import();
		echo $import->getTabelaCount($tabela);
	}
	
	function importcsvAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image');
		
		$dane = $this->_request->getPost('dane');
		$tryb = $this->_request->getParam('tryb', 'panel');
		$rodzaj = $this->_request->getParam('rodzaj', 'produkty');
		$typ = $this->_request->getParam('typ', '');
		$ile = intval($this->_request->getParam('ile', 200));
		$od = intval($this->_request->getParam('od', 0));
		$file = $this->_request->getParam('file', '');
		$adres = $this->_request->getParam('adres', '');
		$adresLength = mb_strlen($adres, 'UTF-8');
		$firstRun = $this->_request->getParam('firstrun', true);
		$edycja = $this->_request->getParam('edycja', true);
		
		$common = new Common();
		$import = new Import($tryb, $rodzaj, $typ);
		$importData = new Zend_Session_Namespace('importData'.$tryb.$rodzaj.$typ);
		if($od == 0 || !isset($importData->data)) $importData->data = date('Y-m-d H:i:s');
		$import->dane['data'] = $importData->data;
		$produkty = new Produkty();
		$producenci = new Producent();
		$jednMiary = new Jednostkimiary();
		$route = new Routers();
		$img = new Galeria();
		$zdj = new uploadJpg();
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image'); 
		$sciezka = $obConfig->ImageDir;
		$kategs = new Kategorie();
		$katProd = new Katprod();
		$powiazania = new Produktypowiazania();
		$grupy = new Produktygrupy();
		
		$platnosci = new Platnosci();
		$platnosciAll = $platnosci->wypiszWszystkie();
		$platnosciDlaProdukt = new PlatnosciProdukty();
		
        $local_path = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].$this->view->baseUrl);
		$filename = $local_path.'/public/admin/zip/ext/'.$file;
		//var_dump($filename);
		$result = file_get_contents($filename);
		if(true && $import->encoding != 'UTF-8')
		$result = iconv($import->encoding, 'UTF-8', $result);
		//echo mb_detect_encoding($result);die();
		$results = explode("\n", $result);
		$tabelaCount = @count($results) - $import->headersRow;
		
		$odIle = $od;// * $ile + 0;		
		$doIle = min($odIle + $ile, $tabelaCount);
		$next = ($doIle >= $tabelaCount);		
		//var_dump($results);die();
		
		$produktyAll = $produkty->wypiszProduktyImport($import->pole, $od > 0);
		if(!$firstRun && $tryb == 'clickshop')
		$produktyAllKod = $produkty->wypiszProduktyImport('oznaczenie', $od > 0);
		$producenciAll = $producenci->getProducenci('nazwa');
		$jednMiaryAll = $jednMiary->getJednMiary('nazwa');
		$kategorieAll = $kategs->getKategorie('nazwa');
		$podKatAll = $katProd->getKatProd();
		$galeriaAll = $img->getZdjecia();
		$grupyAll = $grupy->getGrupy('nazwa');
		
		if(false)
        for($i = $odIle; $i <= $doIle; $i++)
		{
			$row = @str_replace('""', '"', $results[$i]);
			$dane = @explode("$import->delim", $row);
			$nazwa = @str_replace('"', '', $dane[0]);
			$produkt = @$produktyAll[trim($import->pole)];
			if(@intval($produkt['id']) > 0) $ids[] = $produkt['id'];
		}
		
		$ile = 0;
		$nowe = 0;
		$existed = 0;
		$stare = 0;
		$pomin = 0;
		for($j = $odIle; $j <= $doIle; $j++)
		{
			if($j % 20 == 0) @file_put_contents('../public/ajax/importcsv.txt', $j);
			
			$row = @$results[$j];
			if(empty($row)) continue;
			//$row = str_replace('","0,', '","0.', $row);
			do $row = preg_replace('/,"([^,"]*),([^"]*)",/', ',"$1.$2",', $row, -1, $preg);
			while($preg > 0);
			$row = str_replace('""', '"', $row);
			$dane = explode("$import->delim", $row);
			//var_dump($dane);
			if($import->columnCount > 0 && count($dane) != $import->columnCount)
			{
				if($j == 0)
				{
					$die = true;
					echo '<die><![CDATA[';
					echo 'Spodziewana ilość kolumn = '.$import->columnCount.'<br/>';
					echo 'Rzeczywista ilość kolumn = '.count($dane).'<br/>';
					echo ']]></die>';
					break;
				}
				else
				{
					$pomin++;
					continue;
				}
			}
			
			if($j < $import->headersRow) continue;
			//if($ile++ > 0) break;
			
			if(false && count($dane) > 0)
			foreach($dane as $d => $dana)
			{
				$dana[$d] = stripslashes($dana);
			}
			//var_dump($dane);continue;
			
			if(true)
			{
				$pole = '';
				if(is_array($import->poleID) && count($import->poleID) > 0)
				foreach($import->poleID as $poleID)
				$pole .= str_replace('"', '', trim($dane[$poleID]));
				else 
				$pole = str_replace('"', '', $dane[$import->poleID]);
				
				$pole = trim($pole);
				if(empty($pole)) continue;
				//$pole = htmlspecialchars($pole);
				//$data[$import->pole] = $pole;
				//var_dump($pole);
			}
			
			//$exist = $produkty->szukajNazwa($data['nazwa']);
			$exist = @$produktyAll[$pole];
			//var_dump($exist);
			
			if($firstRun)
			if($edycja || count($exist) == 0)
			{
				if($tryb == 'clickshop')
				{
					$kategoria = str_replace('"', '', $dane[0]);
					$kategorie = explode(" > ", $kategoria);
					$data['oznaczenie'] = $dane[1];
					$data['promocja'] = $dane[2];
					$producent = str_replace('"', '', trim($dane[3]));
					$data['waga'] = floatval($dane[4]);
					$data['dostepnosc'] = intval($dane[5]);
					$data['cena_brutto'] = floatval($dane[6]);
					if(@$data['promocja'])
					$data['cena_promocji_b'] = floatval($dane[6]);
					$data['vat'] = intval($dane[7]);
					$data['nazwa'] = str_replace('"', '', $dane[8]);
					$aktywnoscPL = $dane[9];
				}
				
				if($tryb == 'fpp' && $typ == 'inwentarz')
				{
					$data['oznaczenie'] = str_replace('"', '', $dane[1]);
					$data['nazwa'] = str_replace('"', '', $dane[2]);
					$data['jedn_miary'] = str_replace('"', '', $dane[3]);
					$data['dostepnosc'] = intval($dane[4]);
					$data['cena_brutto'] = floatval($dane[5]);
					$data['tekst'] = str_replace('"', '', $dane[8]);
				}
				if($tryb == 'fpp' && $typ == 'cennik')
				{
					$grupa = str_replace('"', '', $dane[0]);
					$data['oznaczenie'] = str_replace('"', '', $dane[1]);
					$data['nazwa'] = str_replace('"', '', $dane[2]);
					$data['pkwiu'] = str_replace('"', '', $dane[4]);
					$producent = str_replace('"', '', $dane[5]);
					$data['jedn_miary'] = str_replace('"', '', $dane[7]);
					$data['jedn_miary_zb'] = str_replace('"', '', $dane[8]);
					$data['jedn_miary_zb_ile'] = floatval($dane[9]);
					$data['vat'] = floatval($dane[10]);
					$data['cena_netto'] = floatval($dane[15]);
					$stan = floatval($dane[31]);
					$data['tekst'] = str_replace('"', '', $dane[34]);
				}
				if(empty($data['nazwa'])) continue;
				//var_dump($dane);var_dump($data);die();
				
				if(!isset($data['vat'])) $data['vat'] = 23;
				
				$data['cena_brutto'] = $common->obliczBruttoFromNettoVat($data['cena_netto'], $data['vat']);
				//$data['cena_netto'] = $common->obliczNettoFromBruttoVat($data['cena_brutto'], $data['vat']);
				if(@$data['promocja'])
				$data['cena_promocji_b'] = $common->obliczBruttoFromNettoVat($data['cena_promocji_n'], $data['vat']);
				//$data['cena_promocji_n'] = $common->obliczNettoFromBruttoVat($data['cena_promocji_b'], $data['vat']);
				if(true)
				{
					$data['cena_netto_hurt'] = $data['cena_netto'];
					$data['cena_brutto_hurt'] = $data['cena_brutto'];
					if(@$data['promocja'])
					{
						$data['cena_promocji_n_hurt'] = $data['cena_promocji_n'];
						$data['cena_promocji_b_hurt'] = $data['cena_promocji_b'];
					}
				}
				
				if($tryb == 'clickshop')
				{
					$tekst = '';
					for($i = 10; $i < count($dane); $i++)
					if(!empty($dane[$i]) && mb_substr($dane[$i],0,$adresLength,'UTF-8') != $adres) $tekst.= $dane[$i];
					if(mb_substr($tekst, 0, 1, 'UTF-8') == '"') $tekst = mb_substr($tekst, 1, 99999, 'UTF-8');
					$len = mb_strlen($tekst,'UTF-8');
					if(mb_substr($tekst,-1,99999,'UTF-8')=='"') $tekst = mb_substr($tekst, 0, $len-1,'UTF-8');
					$data['tekst'] = $tekst;
				}
				
				if($tryb == 'clickshop')
				{
					$images = null;
					for($i = 10; $i < count($dane); $i++)
					if(!empty($dane[$i]) && mb_substr($dane[$i], 0, $adresLength, 'UTF-8') == $adres)
					$images[] = $dane[$i];
				}
				
				if($this->obConfig->produktyGrupy && @!empty($grupa))
				{
					//$existGrupa = $grupy->wypiszNazwa($grupa);
					$existGrupa = @$grupyAll[$grupa];
					if(count($existGrupa) > 0) $data['grupa'] = intval($existGrupa['id']);
					else
					{
						$daneGrupa = array('nazwa' => $grupa);
						$nowaGrupa = $grupy->dodaj($daneGrupa);
						$daneGrupa['id'] = $nowaGrupa;
						$grupyAll[$grupa] = $daneGrupa;
						$data['grupa'] = $nowaGrupa;
					}
				}
				
				if(false)
				if($this->obConfig->producent && @!empty($producent))
				{
					//$existProd = $producenci->wypiszProducentaNazwa($producent);
					$existProd = @$producenciAll[$producent];
					if(count($existProd) > 0) $data['producent'] = intval($existProd['id']);
					else
					{
						$daneProducent = array('nazwa' => $producent);
						$nowyProducent = $producenci->dodajProducenta($daneProducent);
						$daneProducent['id'] = $nowyProducent;
						$producenciAll[$producent] = $daneProducent;
						$data['producent'] = $nowyProducent;
					}
				}
				
				if($this->obConfig->jednostki && @!empty($data['jedn_miary']))
				{
					$existJedn = @$jednMiaryAll[$data['jedn_miary']];
					if(count($existJedn) == 0)
					{
						$daneJednMiary = array('nazwa' => $data['jedn_miary'], 'typ' => 'int');
						$nowaJednMiary = $jednMiary->dodaj($daneJednMiary);
						$daneJednMiary['id'] = $nowaJednMiary;
						$jednMiaryAll[$data['jedn_miary']] = $daneJednMiary;
					}
					else $data['jedn_miary_typ'] = $existJedn['typ'];
				}
				if($this->obConfig->jednostki && @!empty($data['jedn_miary_zb']))
				{
					$existJedn = @$jednMiaryAll[$data['jedn_miary_zb']];
					if(count($existJedn) == 0)
					{
						$daneJednMiary = array('nazwa' => $data['jedn_miary_zb'], 'typ' => 'int');
						$nowaJednMiary = $jednMiary->dodaj($daneJednMiary);
						$daneJednMiary['id'] = $nowaJednMiary;
						$jednMiaryAll[$data['jedn_miary_zb']] = $daneJednMiary;
					}
					else $data['jedn_miary_typ'] = $existJedn['typ'];
				}
			}
			
			if(!$firstRun)
			if($tryb == 'clickshop')
			if($this->obConfig->produktyPowiazania)
			{
				$z = 10;
				if(!empty($dane[$z]))
				for($i = $z; $i < count($dane); $i++)
				if(!empty($dane[$i]))
				{
					$tekst = $dane[$i];
					if(mb_substr($tekst, -1, 99999, 'UTF-8') == '"')
					{
						$z = $i + 1;
						break;
					}
				}
				
				$powiazane = null;
				if($z > 0)
				for($i = $z; $i < count($dane); $i++)
				if(!empty($dane[$i]))
				{
					if(strlen($dane[$i]) > 5) continue;
					if(intval($dane[$i]) == 0) continue;
					$powiazane[] = intval($dane[$i]);
					echo $ile.' - '.$i.' = '.$dane[$i].'<br/>';
				}
				
				if(count($exist) > 0)				
				{
					$powiazaneDane['id_prod'] = $exist['id'];
					//$powiazania->usunProdukt($exist['id']);
					if(count($powiazane) > 0)
					foreach($powiazane as $powiazany)
					{
						//$existKod = $produkty->wyszukajPoKodzie($powiazany);
						$existKod = @$produktyAllKod[trim($powiazany)];
						if(count($existKod) > 0)
						{
							$powiazaneDane['powiazany'] = $existKod['id'];
							$powiazania->dodaj($powiazaneDane);
						}
					}
				}
			}
		
			if($firstRun)
			if(count($exist) == 0)
			{
				$data['widoczny'] = '1';
				//if(isset($stan) && $stan == 0) $data['widoczny'] = '0';
				$data['zmiana'] = date('Y-m-d');
				if(@empty($data['jedn_miary']))	$data['jedn_miary'] = 'szt.';
				if(@empty($data['jedn_miary_typ'])) $data['jedn_miary_typ'] = 'int';
				unset($data['id']);				
				$id = $produkty->dodajProdukty($data);
				if($id > 0)
				{
					$data['id'] = $id;
					$produktyAll[$pole] = $data;
					unset($data['id']);
					if(true) $import->dodajWpis($id, 'dodaj', null, $data);
					$nowe++;
				}				
				unset($data['zmiana']);
				unset($data['widoczny']);
				
				if($id > 0)
				{
					$sprawdz = $route->dodaj($data['nazwa'].'-'.$id, $id);
					$produkty->id = $id;
					$data2['link'] = $sprawdz['link'];
					$data2['route_id'] = $sprawdz['id'];
					$produkty->edytujProdukty($data2);

					if($this->obConfig->platnosciProdukt)
					{
						if(count($platnosciAll) > 0)
						foreach($platnosciAll as $platnosc)
						{
							$data3 = array('id_produkt' => $id, 'id_platnosci' => $platnosc['id']);
							$platnosciDlaProdukt->addWartosc($data3);
						}
					}
				}
			}
			else if($edycja)
			{
				$id = $exist['id'];
				//if(isset($stan) && $stan == 0) $data['widoczny'] = '0';
				if(count($data) > 0)
				foreach($data as $co => $dana)
				{
					if(!isset($update[$co])) $update[$co] = 0;
					if($dana == $exist[$co]) unset($data[$co]);
				}
				if(count($data) > 0)
				{
					$produkty->id = $exist['id'];
					$updateOK = $produkty->edytujProdukty($data);
					$existed += intval($updateOK);
					//var_dump($data);die();
					if($updateOK)
					{
						foreach($data as $co => $dana)
						if($dana != $exist[$co]) $update[$co]++;
						if(true) $import->dodajWpis($id, 'edytuj', $exist, $data);
					}
				}
				//unset($data['widoczny']);
			}
			
			if($firstRun)
			if($edycja || count($exist) == 0)
			{
				$rodzic = 0;
				$last_id = 0;
				//var_dump($kategorie);
				if(@count($kategorie) > 0)
				foreach($kategorie as $kateg)
				{
					//$path = $kategs->get_path($rodzic);
					$existKateg = @$kategorieAll[$kateg];
					//$existKateg = $kategs->szukajNazwaRodzic($kateg, $rodzic);
					if(count($existKateg) > 0) $last_id = $existKateg['id'];
					else
					{
						$nazwa =  $kateg;
						$last_id = $kategs->dodaj_nowa($rodzic, $nazwa);
						$kategorieAll[$kateg] = array('id' => $last_id, 'nazwa' => $nazwa);

						if(false && @count($path) > 0)
						$nazwa = $path[count($path)-1]['link'].'-'.$nazwa;
						else $nazwa = $nazwa;

						$sprawdz = $route->dodaj($nazwa, $last_id, 'default', 'oferta', 'kategorie');
						if(true || $sprawdz['id'] > 0)
						{
							$data1['link'] = $sprawdz['link'];
							$data1['route_id'] = $sprawdz['id'];
							$kategs->edytuj($last_id, $data1);
						}
					}
					if(!isset($podKatAll[$id][$last_id]))
					{
						$kp = $katProd->add($last_id, $id);
						$podKatAll[$id][$last_id] = $kp;
					}
					$rodzic = $last_id;
				}
				
				if(@count($images) > 0)
				foreach($images as $image)
				if(!empty($image))
				{
					$imageData = pathinfo($image);
					//var_dump($imageData);
					if(!isset($galeriaAll[$id][$imageData['basename']]))
					{
						//$zdjecie = @file_get_contents();
						$zdj->nazwa = $imageData['basename'];
						//$zdj->nazwa = md5(uniqid(mt_rand(), true)).'.jpg';
						$zdj->typ = 'image/'.$imageData['extension'];
						//$zdj->typ = 'image/jpeg';
						$zdj->sciezka = str_replace('//', '/', $sciezka);
						$zdj->sciezkaSmall = $zdj->sciezka.'/miniaturki';
						$zdj->sciezkaThumb = $zdj->sciezka.'/thumbs';
						$zdj->sciezkaTiny = $zdj->sciezka.'/tiny';
						$zdj->lokalizacja = $zdj->sciezka.''.$zdj->nazwa;
						//var_dump($zdj->lokalizacja);
						if(@copy($image, $zdj->lokalizacja))
						{
							$size = @getimagesize($zdj->lokalizacja);
							$zdj->size = @filesize($zdj->lokalizacja);
							$zdj->tmp = $zdj->lokalizacja;
							$zdj->error = 0;
							$zdj->miniPhoto('small', 'width');
							$zdj->miniPhoto('thumb', 'width');
							$zdj->miniPhoto('tiny', 'width');
							$idNew = $img->zapiszDoBazy($zdj->nazwa, $id, '');
							//$gal = $img->wyswietlGalerie($id);
							$galeriaAll[$id][$zdj->nazwa] = $idNew;
							if(@count($galeriaAll[$id]) == 1) $img->ustawGlowne($idNew, $id);
						}
					}
				}
			}
		}
		@file_put_contents('../public/ajax/importcsv.txt', $j);
		//var_dump($j);
		//var_dump($nazwy);
		//var_dump($jednAll);
		if($next && @!$die) $import->importDaty('produkty', $file);
		
		echo '<ileRows>'.($doIle - $odIle).'</ileRows>';
		echo '<ileRowsAll>'.$tabelaCount.'</ileRowsAll>';
		echo '<ileRecords>'.$nowe.'</ileRecords>';
		//echo '<ileRecords>'.max(0, $j - 1).'</ileRecords>';
		echo '<ileExist>'.$existed.'</ileExist>';
		echo '<ileStare>'.$stare.'</ileStare>';
		echo '<ilePomin>'.$pomin.'</ilePomin>';
		if(@count($update) > 0)
		foreach($update as $co => $ilosc)
		{
			echo '<pole>';
			echo '<id>'.$co.'</id>';
			echo '<ile>'.$ilosc.'</ile>';
			echo '</pole>';
		}	
		echo '<next>'.$next.'</next>';
        echo '</root>';
		die();
    }
	
	function importcsvklienciAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image');
		
		$dane = $this->_request->getPost('dane');
		$tryb = $this->_request->getParam('tryb', 'panel');
		$rodzaj = $this->_request->getParam('rodzaj', 'klienci');
		$typ = $this->_request->getParam('typ', '');
		$ile = intval($this->_request->getParam('ile', 200));
		$od = intval($this->_request->getParam('od', 0));
		$file = $this->_request->getParam('file', '');
		$adres = $this->_request->getParam('adres', 'http://aquatika.home.pl');
		$adresLength = mb_strlen($adres, 'UTF-8');
		$firstRun = $this->_request->getParam('firstrun', true);
		$edycja = $this->_request->getParam('edycja', true);
		
		$common = new Common();
		$import = new Import($tryb, $rodzaj, $typ);
		$importData = new Zend_Session_Namespace('importData'.$tryb.$rodzaj.$typ);
		if($od == 0 || !isset($importData->data)) $importData->data = date('Y-m-d H:i:s');
		$import->dane['data'] = $importData->data;
		$klienci = new Kontrahenci();
		$grupy = new Kontrahencigrupy();
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image'); 
		
        $local_path = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].$this->view->baseUrl);
		$filename = $local_path.'/public/admin/zip/ext/'.$file;
		//var_dump($filename);
		$result = file_get_contents($filename);
		if(true && $import->encoding != 'UTF-8')
		$result = iconv($import->encoding, 'UTF-8', $result);
		//echo mb_detect_encoding($result);die();
		$results = explode("\n", $result);
		$tabelaCount = @count($results) - $import->headersRow;
		
		$odIle = $od;// * $ile + 0;		
		$doIle = min($odIle + $ile, $tabelaCount);
		$next = ($doIle >= $tabelaCount);		
		
		//var_dump($results);
		if(false)
		{
			$dane = explode(",", $results[0]);
			for($i=1;$i<count($results);$i++)
			{
				$row = explode(",", $results[$i]);
				for($j=0;$j<count($row);$j++)
				{
					$rows[$i][$dane[$j].$j] = $row[$j];
					unset($row[$j]);
				}
			}
			//var_dump($rows);
			foreach($rows as $r => $row)
			{
				$kody[@$row['"KOD"14']][] = $row['"NAZWA1"0'];
				if($row['"NAZWA1"0'] == '"10 Dąbrowska Halina"') var_dump($row);
			}
			//@var_dump($kody);
			foreach($kody as $k => $kod)
			if(count($kod) > 1)
			{
				echo $k;
				var_dump($kod);
			}
			die();
		}
		
		$klienciAll = $klienci->wypiszKlienciImport($import->pole, $od > 0);
		$grupyAll = $grupy->wypiszForAll('nazwa');
		
		if(false)
        for($i = $odIle; $i <= $doIle; $i++)
		{
			$row = @str_replace('""', '"', $results[$i]);
			$dane = @explode("\t", $row);
			$nazwa = @str_replace('"', '', $dane[0]);
			$produkt = @$klienciAll[trim($import->pole)];
			if(@intval($produkt['id']) > 0) $ids[] = $produkt['id'];
		}		
		
		$ile = 0;
		$nowe = 0;
		$existed = 0;
		$stare = 0;
		for($j = $odIle; $j <= $doIle; $j++)
		{
			if($j % 20 == 0) @file_put_contents('../public/ajax/importcsvklienci.txt', $j);
			
			$row = $results[$j];
			$row = str_replace('""', '"', $row);
			$dane = explode("$import->delim", $row);
			if($import->columnCount > 0 && count($dane) != $import->columnCount)
			{
				if($j == 0)
				{
					$die = true;
					echo '<die><![CDATA[';
					echo 'Spodziewana ilość kolumn = '.$import->columnCount.'<br/>';
					echo 'Rzeczywista ilość kolumn = '.count($dane).'<br/>';
					echo ']]></die>';
					break;
				}
				else continue;
			}
			
			if($j < $import->headersRow) continue;
			//if($ile++ > 0) break;
			
			if(true && count($dane) > 0)
			foreach($dane as $d => $dana)
			{
				//if(false) $dane[$d] = stripslashes($dana);
				if($tryb == 'fpp') $dane[$d] = trim(str_replace('"', '', trim($dana)));
			}
			//var_dump($dane);continue;
			
			if(true)
			{
				$pole = '';
				if(is_array($import->poleID) && count($import->poleID) > 0)
				foreach($import->poleID as $poleID)
				$pole .= str_replace('"', '', trim($dane[$poleID]));
				else 
				$pole = str_replace('"', '', $dane[$import->poleID]);
				
				$pole = trim($pole);
				if(empty($pole)) continue;
				$pole = htmlspecialchars($pole);
				//$data[$import->pole] = $pole;
				//var_dump($pole);
			}
			
			//$exist = $klienciAll->szukajNazwa($data['nazwa']);
			$exist = @$klienciAll[$pole];
			//var_dump($exist);
			
			if($edycja || count($exist) == 0)
			{				
				if($tryb == 'fpp')
				{
					$data['nazwa_firmy'] = $dane[0];
					$data['nazwa'] = $dane[1];
					$data['ulica'] = $dane[2];
					$data['kod'] = $dane[3];
					$data['miasto'] = $dane[4];
					$data['nazwisko'] = $dane[5];
					//$data['ulica_korespondencja'] = $dane[6];
					$data['telefon'] = $dane[7];
					//$data['telefax'] = $dane[8];
					$data['nip'] = $dane[9];
					//$data['bank_nazwa'] = $dane[10];
					//$data['bank_adres'] = $dane[11];
					//$data['bank_konto'] = $dane[12];
					//$data['uwagi'] = $dane[13];
					$data['id_ext'] = $dane[14];
					//$grupa = $dane[15];
					$data['email'] = $dane[16];
					$data['www'] = $dane[17];
					//$data['gln_iln'] = $dane[18];
					//$data['id_ext'] = $dane[19];
					$data['rabat'] = floatval(str_replace(',','.',$dane[24]));
					//if($ile > 10) break;
					if(empty($data['nazwa_firmy']) && empty($data['nazwisko'])) continue;
					//var_dump($dane);var_dump($data);//die();					
					
					if($this->obConfig->klienciGrupy && @!empty($grupa))
					{
						//$existGrupa = $grupy->wypiszNazwa($grupa);
						$existGrupa = @$grupyAll[$grupa];
						if(count($existGrupa) > 0) $data['grupa'] = intval($existGrupa);
						else
						{
							$daneGrupa = array('nazwa' => $grupa);
							$nowaGrupa = $grupy->dodaj($daneGrupa);
							$daneGrupa['id'] = $nowaGrupa;
							$grupyAll[$grupa] = $nowaGrupa;
							$data['grupa'] = $nowaGrupa;
						}
					}
				}
			}
		
			if(count($exist) == 0)
			{
				if(@!empty($data['nazwa_firmy']))
				$data['faktura'] = '1';
				$data['weryfikacja'] = '1';
				unset($data['id']);
				$id = $klienci->dodaj($data);
				if($id > 0)
				{
					$data['id'] = $id;
					$klienciAll[$pole] = $data;
					unset($data['id']);
					$nowe++;
					if(true) $import->dodajWpis($id, 'dodaj', null, $data);
				}
			}
			else if($edycja)
			{
				$id = $exist['id'];
				if(count($data) > 0)
				foreach($data as $co => $dana)
				{
					if(!isset($update[$co])) $update[$co] = 0;
					if($dana == $exist[$co]) unset($data[$co]);
				}
				if(count($data) > 0)
				{
					$klienci->id = $exist['id'];
					//$data['zmiana'] = date('Y-m-d');
					$updateOK = $klienci->edytuj($data);
					//unset($data['zmiana']);
					$existed += intval($updateOK);
					if($updateOK)
					{
						foreach($data as $co => $dana)
						if($dana != $exist[$co]) $update[$co]++;
						if(true) $import->dodajWpis($exist['id'], 'edytuj', $exist, $data);
					}
				}
			}
			
			if($edycja || count($exist) == 0)
			{
				
			}
		}
		@file_put_contents('../public/ajax/importcsvklienci.txt', $j);
		//var_dump($ile);
		if($next && @!$die) $import->importDaty('klienci', $file);
		
		echo '<ileRows>'.($doIle - $odIle).'</ileRows>';
		echo '<ileRowsAll>'.$tabelaCount.'</ileRowsAll>';
		echo '<ileRecords>'.$nowe.'</ileRecords>';
		//echo '<ileRecords>'.max(0, $j - 1).'</ileRecords>';
		echo '<ileExist>'.$existed.'</ileExist>';
		echo '<ileStare>'.$stare.'</ileStare>';
		if(@count($update) > 0)
		foreach($update as $co => $ilosc)
		{
			echo '<pole>';
			echo '<id>'.$co.'</id>';
			echo '<ile>'.$ilosc.'</ile>';
			echo '</pole>';
		}	
		echo '<next>'.$next.'</next>';
        echo '</root>';
		die();
    }
	
	function importxlsgrupyAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image');
		
		$dane = $this->_request->getPost('dane');
		
		$tryb = $this->_request->getParam('tryb', 'panel');
		$rodzaj = $this->_request->getParam('rodzaj', 'produktygrupy');
		$typ = $this->_request->getParam('typ', '');
		$file = $this->_request->getParam('file');
		$nowe = $this->_request->getParam('nowe');
		$ile = intval($this->_request->getParam('ile', 200));
		$od = intval($this->_request->getParam('od', 0));
		$zipIt = intval($this->_request->getParam('zip', 0));
		
		$next = 0;
		$tabelaCount = 0;
		$zip = new ZipArchive();
		
		$import = new Import($tryb, $rodzaj, $typ);
		$importData = new Zend_Session_Namespace('importData'.$tryb.$rodzaj.$typ);
		if($od == 0 || !isset($importData->data)) $importData->data = date('Y-m-d H:i:s');
		$import->dane['data'] = $importData->data;
		try
		{
			$produktyGrupy = new Produktygrupy();
			
			//$xls = str_replace('.zip', '.xls', $file);
			//echo '<file>'.$file.'</file>';			
			$filenamexls = 'admin/zip/ext/'.$file;//.'-'.date('Y-m-d').'.xls';
			$filenamezip = 'admin/bazy/'.$file;//.'-'.date('Y-m-d').'.zip';
			//@mkdir('admin/bazy/'.$file.'-'.date('Y-m-d'));
			
			if($zipIt && $od == 0)
			{
				$import->wypakuj($obConfig->ZipExtDir, $filenamezip, $xls);
				if(strlen($import->bledy) > 0)
				{
					echo '<error>'.$import->bledy.'</error>';
					$next = 1;
				}
			}
			if(@!$next)
			{
				$dane = $import->wczytajDaneXls($filenamexls);
				if(strlen($import->bledy) > 0)
				{
					echo '<error><![CDATA['.$import->bledy.']]></error>';
				}
				else if(count($dane) == 0)
				{
					echo '<error>Brak produktów</error>';
				}
			}
			$tabelaCount = @count($dane);
		}
		catch(Exception $e)
		{
			echo '<error>'.$e->getMessage().'</error>';
		}
		
		$nowe = 0;
		$exist = 0;
		$stare = 0;
		
		$k = 0;
		$odIle = $od;// * $ile + 0;
		$doIle = min($odIle + $ile, $tabelaCount);
		if(@count($dane) > 0)
		try
		{
			$next = ($doIle >= $tabelaCount);//if(false)
			//$results = $import->wgrajNoweProdukty($file, $dane, $odIle, $doIle);
			//if($nowe && $next) $stare = $import->usunStareProdukty($dane);
			$grupyAll = $produktyGrupy->getGrupy($import->pole);
			//var_dump($dane);die();
			for($i = $odIle; $i < $doIle; $i++)
			{
				if(@empty($dane[$i]['nazwa'])) continue;
				$pole = @trim($dane[$i][$import->pole]);
				if($import->pole != 'id' && @empty($pole)) continue;
				//var_dump($dane[$i]['nazwa']);//continue;
				if($i % 100 == 0) @file_put_contents('../public/ajax/importxlsgrupy.txt', $nowe);
				
				$typ = @intval($dane[$i]['typ']);
				if($typ > 0) continue;
				unset($dane[$i]['typ']);
				
				$grupa = @$grupyAll[$pole];
				if(@count($grupa) > 0)
				{
					$grupaID = $grupa['id'];
					//$exist++;
				}
				else
				{
					$noweID = $produktyGrupy->dodaj($dane[$i]);
					if($noweID > 0)
					{
						$nowe++;
						if(true) $import->dodajWpis($noweID, 'dodaj', null, $dane[$i]);
					}
					if($import->pole == 'id') $dane[$i]['id'] = $noweID;
					$grupyAll[$pole] = $dane[$i];
				}
			}
			@file_put_contents('../public/ajax/importxlsgrupy.txt', $nowe);
			if($next && @!$die) $import->importDaty('grupy', $file);
		}
		catch(Exception $e)
		{
			echo '<error>'.$e->getMessage().'</error>';
		}
		else $next = 1;
		
		echo '<ileRows>'.($doIle - $odIle).'</ileRows>';
		echo '<ileRowsAll>'.$tabelaCount.'</ileRowsAll>';
		echo '<ileRecords>'.@intval($nowe).'</ileRecords>';
		echo '<ileExist>'.@intval($exist).'</ileExist>';
		echo '<ileStare>'.@intval($stare).'</ileStare>';
		if(@count($results['pola']) > 0)
		foreach($results['pola'] as $co => $ilosc)
		{
			echo '<pole>';
			echo '<id>'.$co.'</id>';
			echo '<ile>'.$ilosc.'</ile>';
			echo '</pole>';
		}		
		echo '<next>'.$next.'</next>';
        echo '</root>';
		die();
	}
	
	function importcsvrabatyAction() 
	{
		$this->_helper->viewRenderer->setNoRender();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image');
		
		$dane = $this->_request->getPost('dane');
		$tryb = $this->_request->getParam('tryb', 'panel');
		$rodzaj = $this->_request->getParam('rodzaj', 'rabaty');
		$typ = $this->_request->getParam('typ', '');
		$ile = intval($this->_request->getParam('ile', 2000));
		$od = intval($this->_request->getParam('od', 0));
		$file = $this->_request->getParam('file', '');
		
		$common = new Common();
		$import = new Import($tryb, $rodzaj, $typ);
		$importData = new Zend_Session_Namespace('importData'.$tryb.$rodzaj.$typ);
		if($od == 0 || !isset($importData->data)) $importData->data = date('Y-m-d H:i:s');
		$import->dane['data'] = $importData->data;
		$rabaty = new Rabaty();
		$klienci = new Kontrahenci();
		$produkty = new Produkty();
		$grupy = new Produktygrupy();
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image'); 
		
        $local_path = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'].$this->view->baseUrl);
		$filename = $local_path.'/public/admin/zip/ext/'.$file;
		//var_dump($filename);
		$result = file_get_contents($filename);
		if(true && $import->encoding != 'UTF-8')
		$result = iconv($import->encoding, 'UTF-8', $result);
		//echo mb_detect_encoding($result);die();
		$results = explode("\n", $result);
		$tabelaCount = @count($results) - $import->headersRow;
		
		//$ile = 100;
		$odIle = $od;// * $ile + 0;
		$doIle = min($odIle + $ile, $tabelaCount);
		$next = ($doIle >= $tabelaCount);		
		//var_dump($results);die();
		
		$rabatyAll = $rabaty->getRabaty($import->pole, $od > 0);
		$klienciAll = $klienci->wypiszKlienciImport('id_ext', $od > 0);
		$produktyAll = $produkty->wypiszProduktyImport('oznaczenie', $od > 0);
		$grupyAll = $grupy->getGrupy('nazwa');
		
		$ile = 0;
		$nowe = 0;
		$existed = 0;
		$stare = 0;
		for($j = $odIle; $j < $doIle; $j++)
		{
			if($j % 100 == 0) @file_put_contents('../public/ajax/importcsvrabaty.txt', $j);//.' '.date('H:i:s'));
			
			$row = $results[$j];
			//$row = str_replace('""', '"', $row);
			$dane = explode("$import->delim", $row);
			//var_dump($dane);die();
			
			if(false)
			if($import->columnCount > 0 && count($dane) != $import->columnCount)
			{
				if($j == 0)
				{
					$die = true;
					echo '<die><![CDATA[';
					echo 'Spodziewana ilość kolumn = '.$import->columnCount.'<br/>';
					echo 'Rzeczywista ilość kolumn = '.count($dane).'<br/>';
					echo ']]></die>';
					break;
				}
				else continue;
			}
			
			//if($j < $import->headersRow) continue;
			//if($ile++ > 0) break;
			
			if(true && count($dane) > 0)
			foreach($dane as $d => $dana)
			{
				//if(false) $dane[$d] = stripslashes($dana);
				if($tryb == 'fpp') $dane[$d] = trim($dana);
			}
			//var_dump($dane);die();
			
			if(false)
			{
				$pole = '';
				if(is_array($import->poleID) && count($import->poleID) > 0)
				foreach($import->poleID as $poleID)
				$pole .= str_replace('"', '', trim($dane[$poleID]));
				else 
				$pole = str_replace('"', '', $dane[$import->poleID]);
				
				$pole = trim($pole);
				if(empty($pole)) continue;
				$pole = htmlspecialchars($pole);
				//$data[$import->pole] = $pole;
				//var_dump($pole);
			}		
			
			if(true || count($exist) == 0)
			{
				if($tryb == 'fpp')
				{
					$flaga = @intval($dane[2]);
					if($flaga != 1 && $flaga != 21) continue;
					$data['flaga'] = $flaga;
					
					$kodUser = $dane[0];
					$data['id_user'] = 0;
					if(!empty($kodUser))
					{
						$user = @$klienciAll[$kodUser];
						if(count($user) == 0) continue;
						else $data['id_user'] = $user['id'];
					}
					$data['id_user_grupa'] = 0;
					
					$kodProd = '';
					$data['id_prod'] = 0;
					if($flaga == 1)
					{
						$kodProd = $dane[1];
						if(!empty($kodProd))
						{
							$prod = @$produktyAll[$kodProd];
							if(count($prod) == 0) continue;
							else $data['id_prod'] = $prod['id'];
						}
						else continue;
					}
					$kodGrupa = '';
					$data['id_prod_grupa'] = 0;
					if($flaga == 21)
					{
						$kodGrupa = $dane[1];
						if(!empty($kodGrupa))
						{
							$grupa = @$grupyAll[$kodGrupa];
							if(count($grupa) == 0) // continue;
							{
								$daneGrupa = array('nazwa' => $kodGrupa);
								$nowaGrupa = $grupy->dodaj($daneGrupa);
								$daneGrupa['id'] = $nowaGrupa;
								$grupyAll[$kodGrupa] = $nowaGrupa;
								$data['id_prod_grupa'] = $nowaGrupa;
							}
							else $data['id_prod_grupa'] = $grupa['id'];
						}
						else continue;
					}
					
					//$data['data'] = $dane[3];
					//$data['termin'] = $dane[4];
					$data['wartosc_rab'] = @floatval(str_replace(',', '.', trim($dane[5])));
					//if($data['wartosc_rab'] == 0) continue;
					$data['typ_rab'] = @mb_strtoupper($dane[6], 'UTF-8');
					//if(empty($data['typ_rab'])) $data['typ_rab'] = '%';
					//$data['ile'] = $dane[7];
					//$data['opis'] = $dane[8];
					//var_dump($dane);var_dump($data);die();
					$d = $dane;
				}
			}
			
			//$exist = $rabaty->szukajNazwa($data['nazwa']);
			$pole = $data['id_user'].$data['id_prod'].$data['id_prod_grupa'].$data['flaga'];
			$exist = @$rabatyAll[$pole];
			//var_dump($exist);die();
			$info['kod_user'] = $kodUser;
			$info['kod_prod'] = $kodProd;
			$info['kod_grupa'] = $kodGrupa;
		
			if(count($exist) == 0)
			{
				if($data['wartosc_rab'] == 0) continue;
				unset($data['id']);
				$id = $rabaty->dodaj($data);
				$data['id'] = $id;
				$rabatyAll[$pole] = $data;
				unset($data['id']);
				if($id > 0)
				{
					$nowe++;
					if(true) $import->dodajWpis($id, 'dodaj', null, $data, $info);
				}
			}
			else
			{
				$id = $exist['id'];
				if($data['wartosc_rab'] > 0)
				{
					$dd = $data;
					if(count($data) > 0)
					foreach($data as $co => $dana)
					{
						if(true)
						{
							$dana = mb_strtoupper($dana,'UTF-8');
							$exist[$co] = mb_strtoupper($exist[$co],'UTF-8');
						}
						
						if(!isset($update[$co])) $update[$co] = 0;
						if($dana == $exist[$co]) unset($data[$co]);
						
						if($this->test)
						{
							//if($dd['id_user'] == '4405') echo $co.' '.($dana == $exist[$co]).'<br>';
							//if($dana != $exist[$co]) 
							//for($x=0;$x<mb_strlen($dana,'UTF-8');$x++) echo ord($dana[$x]).' ';
							if($dana != $exist[$co]) echo '<br>'.$co.' '.$dana.' '.$exist[$co].'<br>';
							//if($dana != $exist[$co]) echo strcasecmp($dana, $exist[$co]);
							//for($x=0;$x<mb_strlen($exist[$co],'UTF-8');$x++) echo ord($exist[$co][$x]).' ';
						}
					}
					if(count($data) > 0)
					{
						$rabaty->id = $exist['id'];
						$updateOK = $rabaty->edytuj($data);
						if($this->test)
						{
							var_dump($data);var_dump($exist['id']);
							//var_dump($exist);var_dump($d);var_dump($dd);die();
						}
						$existed += intval($updateOK);
						if($updateOK)
						{
							foreach($data as $co => $dana)
							if($dana != $exist[$co]) $update[$co]++;
							if(true) $import->dodajWpis($id, 'edytuj', $exist, $data, $info);
						}
					}
				}
				else
				{
					$rabaty->id = $id;
					$delOK = $rabaty->usunID($id);
					$stare += intval($delOK);
				}
			}
		}
		@file_put_contents('../public/ajax/importcsvrabaty.txt', $j);
		//var_dump($ile);
		if($next && @!$die) $import->importDaty('rabaty', $file);
		
		echo '<ileRows>'.($doIle - $odIle).'</ileRows>';
		echo '<ileRowsAll>'.$tabelaCount.'</ileRowsAll>';
		echo '<ileRecords>'.$nowe.'</ileRecords>';
		//echo '<ileRecords>'.max(0, $j - 1).'</ileRecords>';
		echo '<ileExist>'.$existed.'</ileExist>';
		echo '<ileStare>'.$stare.'</ileStare>';
		if(@count($update) > 0)
		foreach($update as $co => $ilosc)
		{
			echo '<pole>';
			echo '<id>'.$co.'</id>';
			echo '<ile>'.$ilosc.'</ile>';
			echo '</pole>';
		}	
		echo '<next>'.$next.'</next>';
        echo '</root>';
		die();
    }
	
	function importxmlAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		
		echo '<?xml version="1.0" encoding="utf-8"?><root>';
		
		$obConfig = new Zend_Config_Ini('../application/config.ini', 'image');
		
		$dane = $this->_request->getPost('dane');
		
		$tryb = $this->_request->getParam('tryb', 'panel');
		$rodzaj = $this->_request->getParam('rodzaj', 'produkty');
		$typ = $this->_request->getParam('typ', '');
		$file = $this->_request->getParam('file');
		$nowe = $this->_request->getParam('nowe');
		$ile = intval($this->_request->getParam('ile', 200));
		$od = intval($this->_request->getParam('od', 0));
		$zipIt = intval($this->_request->getParam('zip', 0));
		
		$next = 0;
		$tabelaCount = 0;
		$zip = new ZipArchive();
		$plik = 'xml';
		$import = new Import($tryb, $rodzaj, $typ, 'admin', $plik);
		$importData = new Zend_Session_Namespace('importData'.$tryb.$rodzaj.$typ.$plik);
		if($od == 0 || !isset($importData->data)) $importData->data = date('Y-m-d H:i:s');
		$import->dane['data'] = $importData->data;
		
		try
		{
			$produkty = new Produkty();
			
			//$xml = str_replace('.zip', '.xml', $file);
			//echo '<file>'.$file.'</file>';			
			$filenamexml = 'admin/zip/ext/'.$file;//.'-'.date('Y-m-d').'.xml';
			$filenamezip = 'admin/bazy/'.$file;//.'-'.date('Y-m-d').'.zip';
			//@mkdir('admin/bazy/'.$file.'-'.date('Y-m-d'));
			
			if($zipIt && $od == 0)
			{
				$import->wypakuj($obConfig->ZipExtDir, $filenamezip, $xml);
				if(strlen($import->bledy) > 0)
				{
					echo '<error>'.$import->bledy.'</error>';
					$next = 1;
				}
			}
			if(@!$next)
			{
				//$dane = $import->wczytajDaneXml($filenamexml);
				if($od == 0 || !isset($importData->tabelaCount)) //$importData->tabelaCount = 4863;
				$importData->tabelaCount = $import->wczytajDaneXml($filenamexml, $od, $ile, true);
				$tabelaCount = $importData->tabelaCount;
				$dane = $import->wczytajDaneXml($filenamexml, $od, $ile, false);
				if(strlen($import->bledy) > 0)
				{
					echo '<error><![CDATA['.$import->bledy.']]></error>';
				}
				else if(count($dane) == 0)
				{
					echo '<error>Brak produktów</error>';
				}
			}
			//$tabelaCount = @count($dane);
		}
		catch(Exception $e)
		{
			echo '<error>'.$e->getMessage().'</error>';
		}
		
		
		$k = 0;
		$odIle = $od;// * $ile + 0;
		$doIle = min($odIle + $ile, $tabelaCount);
		if(@count($dane) > 0)
		try
		{
			$next = ($doIle >= $tabelaCount);
			//var_dump($dane);die();
			//if(false)
			//$results = $import->wgrajNoweProdukty($file, $dane, $odIle, $doIle);
			//if($nowe && $next) $stare = $import->usunStareProdukty($dane);
		}
		catch(Exception $e)
		{
			echo '<error>'.$e->getMessage().'</error>';
		}
		else $next = 1;
		
		echo '<ileRows>'.($doIle - $odIle).'</ileRows>';
		echo '<ileRowsAll>'.$tabelaCount.'</ileRowsAll>';
		echo '<ileRecords>'.@intval($results['nowe']).'</ileRecords>';
		echo '<ileExist>'.@intval($results['exist']).'</ileExist>';
		echo '<ileStare>'.@intval($stare).'</ileStare>';
		if(@count($results['pola']) > 0)
		foreach($results['pola'] as $co => $ilosc)
		{
			echo '<pole>';
			echo '<id>'.$co.'</id>';
			echo '<ile>'.$ilosc.'</ile>';
			echo '</pole>';
		}		
		echo '<next>'.$next.'</next>';
        echo '</root>';
	}
}
?>