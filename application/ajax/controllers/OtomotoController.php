<?php
class Ajax_OtomotoController extends Ogolny_Controller_Page
{
    public function init() 
	{
        parent::init();
        $this->view->baseUrl = $this->_request->getBaseUrl();
		
		define('BLAD_NIEOKRESLONY', -1);
		define('OTOMOTO_BLAD_TYPU', -2);
		
		$this->otoMoto = new Otomoto();
		$this->otoMoto->ID = 1;
		$this->otoMotoDane = $this->otoMoto->klient();
		$this->view->otoMoto = $this->otoMotoDane;
		$this->webapi = $this->otoMotoDane['klucz'];
		$this->parametry = array
		(
			"dealer-login" => $this->otoMotoDane['login'],
			"dealer-password" => $this->otoMotoDane['haslo'],
			"country-code" => "1",
			"webapi-key" => $this->webapi
		);
		$this->adres_otomoto = $this->otoMoto->adres_otomoto;
		$this->klientOtomoto = new SoapClient($this->adres_otomoto);
		$_SESSION['adres_otomoto'] = $this->adres_otomoto;
		try
		{
			$this->kontoOtomoto = $this->klientOtomoto->__soapCall('doDealerLogin', $this->parametry);
			$_SESSION['sidotomoto'] = $this->kontoOtomoto['session-id'];
			$this->serwisNiedostepny = false;
		}
		catch(SoapFault $error)
		{
			$this->error = 'Błąd '.$error->faultcode.': '.$error->faultstring;
			$this->serwisNiedostepny = true;
		}
		catch(Exception $error)
		{
			$this->error = 'Błąd '.$error;
			$this->serwisNiedostepny = true;
		}
    }
		
    function indexAction() 
	{
        $this->_helper->viewRenderer->setNoRender();
        echo 'ok';
    }
	
	function pobierzmarkeAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		
		if($this->serwisNiedostepny === false)
		{
			$parametry = array
			(
				'type' => $this->_request->getPost('typ_pojazdu'),
				'webapi-key' => $this->webapi
			);
			$marki = $this->klientOtomoto->__soapCall('getMakes', $parametry);
			$select = '';
			$select .= '<option value="-1">-- wybierz --</option>';
			if(!empty($marki))
			{
				foreach($marki as $marka)
				{
					$select .= '<option value="'.$marka->id.'">'.$marka->name.'</option>';
				}
			}
			echo $select;
		}
		else echo '<option value="-1">-- Błąd połączenia ! --</option>';
	}
	function pobierzmodelAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		
		if($this->serwisNiedostepny === false)
		{
			$marka_id = $this->_request->getPost('marka_id');
			if($marka_id != '-1')
			{
				$parametry = array
				(
					'make-id' => $marka_id,
					'webapi-key' => $this->webapi,
					'country-code' => '1',
					'type' => $this->_request->getPost('typ_pojazdu')
				);
				$modele = $this->klientOtomoto->__soapCall('getModels', $parametry);
				$modele = $this->otoMoto->sortujTabObiektow($modele);
			}
			$select = '';
			$select .= '<option value="-1">-- wybierz --</option>';
			if(!empty($modele))
			{
				foreach($modele as $model)
				{
					$select .= '<option value="'.$model->id.'">'.$model->name.'</option>';
				}
			}
			echo $select;
		}
		else echo '<option value="-1">-- Błąd połączenia ! --</option>';
	}
	function pobierzwersjeAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		
		if($this->serwisNiedostepny === false)
		{
			$parametry = array
			(
				'make-id' => $this->_request->getPost('model_id'),
				'model-id' => $this->_request->getPost('wersja_id'),
				'webapi-key' => $this->webapi,
				'country-code' => '1',
				'type' => $this->_request->getPost('typ_pojazdu')
			);
			$wersje = $this->klientOtomoto->__soapCall('getVersions', $parametry);
			$select = '';
			$select .= '<option value="-1">-- wybierz --</option>';
			if(!empty($wersje))
			{
				foreach($wersje as $wersja)
				{
					$select .= '<option value="'.$wersja->id.'">'.$wersja->name.'</option>';
				}
			}
			echo $select;
		}
		else echo '<option value="-1">-- Błąd połączenia ! --</option>';
	}
	function pobierztypnadwoziaAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();

		if($this->serwisNiedostepny === false)
		{
			$parametry = array
			(
				'type' => $this->_request->getPost('typ_pojazdu'),
				'webapi-key' => $this->webapi
			);
			$typy_nadwozi = $this->klientOtomoto->__soapCall('getVehicleCategories', $parametry);
			$select = '';
			$select .= '<option value="-1">-- wybierz --</option>';
			if(!empty($typy_nadwozi))
			{
				foreach($typy_nadwozi as $typ_nadwozia)
				{
					$select .= '<option value="'.$typ_nadwozia->key.'">'.$typ_nadwozia->name.'</option>';
				}
			}
			echo $select;
		}
		else echo '<option value="-1">-- Błąd połączenia ! --</option>';
	}
	function pobierzdodatkoweinfoAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		
		if($this->serwisNiedostepny === false)
		{
			$parametry = array
			(
				'type' => $this->_request->getPost('typ_pojazdu'),
				'webapi-key' => $this->webapi
			);
			$dodatkowe_info = $this->klientOtomoto->__soapCall('getVehicleFeatures', $parametry);
			$select = '';
			$select .= '<option value="-1">-- wybierz --</option>';
			if(!empty($dodatkowe_info))
			{
				foreach($dodatkowe_info as $info)
				{
					$select .= '<option value="'.$info->key.'">'.$info->name.'</option>';
				}
			}
			echo $select;
		}
		else echo '<option value="-1">-- Błąd połączenia ! --</option>';
	}
	function pobierzdodatkowewyposazAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		
		if($this->serwisNiedostepny === false)
		{
			$parametry = array
			(
				'type' => $this->_request->getPost('typ_pojazdu'),
				'webapi-key' => $this->webapi
			);
			$wyposaz_dodatkowe = $this->klientOtomoto->__soapCall('getVehicleExtras', $parametry);
			$select = '';
			$select .= '<option value="-1">-- wybierz --</option>';
			if(!empty($wyposaz_dodatkowe))
			{
				foreach($wyposaz_dodatkowe as $wyposazenie)
				{
					$select .= '<option value="'.$wyposazenie->key.'">'.$wyposazenie->name.'</option>';
				}
			}
			echo $select;
		}
		else echo '<option value="-1">-- Błąd połączenia ! --</option>';
	}
	function pobierzrodzajnapeduAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();

		if($this->serwisNiedostepny === false)
		{
			$parametry = array
			(
				'type' => $this->_request->getPost('typ_pojazdu'),
				'webapi-key' => $this->webapi
			);
			$rodzaje_napedu = $this->klientOtomoto->__soapCall('getTransmissionTypes', $parametry);
			$select = '';
			$select .= '<option value="-1">-- wybierz --</option>';
			if(!empty($rodzaje_napedu))
			{
				foreach($rodzaje_napedu as $rodzaj_napedu)
				{
					$select .= '<option value="'.$rodzaj_napedu->key.'">'.$rodzaj_napedu->name.'</option>';
				}
			}
			echo $select;
		}
		else echo '<option value="-1">-- Błąd połączenia ! --</option>';
	}
	function pobierzinfooaktywnosciAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();	

		try
		{
			if($this->serwisNiedostepny === false)
			{
				$parameters = array
				(
					"insertion" => array('id' => $this->_request->getPost('id_ogloszenia')),
					"session-id" => $_SESSION['sidotomoto'],
					"webapi-key" => $this->webapi
				);
				$response = $this->klientOtomoto->__soapCall('getInsertion', $parameters);

				$response = get_object_vars($response);
				$info = '';
				if(strtoupper($response['status']) == 'ACT')
				{
					$info .= 'aktywne|';
				}
				else
				{
					$info .= 'nieaktywne|';
				}
				$info .= $response['expiration-date'].'|';
				$info .= $response['short-link'];

				echo $info;
			}
			else
			{
				echo 'false';
			}
		}
		catch(Exception $e)
		{
			if($e->faultcode == 'ERR_INVALID_SESSION_ID') // ponownie logujemy do otomoto
			{
				$this->kontoOtomoto = $this->klientOtomoto->__soapCall('doDealerLogin', $this->parametry);
				$_SESSION['sidotomoto'] = $this->kontoOtomoto['session-id'];
				$_SESSION['adres_otomoto'] = $adres_otomoto;
			}
			else
			{
				echo 'false';
			}
		}
	}
	function pobierzliczbewolnychogloszenAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		try
		{
			if($this->serwisNiedostepny === false)
			{
				$response = $this->klientOtomoto->__soapCall('doDealerLogin', $this->parametry);
				$response = get_object_vars($response['dealer']);
				$info = $response['current-exposure-counter'].'|'.$response['max-exposures'];
				echo $info;
			}
			else
			{
				echo 'false';
			}
		}
		catch(Exception $e)
		{
			echo 'false';
		}
	}	
	function zmienstatusogloszeniaAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		
		if($this->serwisNiedostepny === false)
		try
		{
			$akcja = $this->_request->getPost('akcja');
			$parameters = array
			(
				"insertion" => array('id' => $this->_request->getPost('id_ogloszenia')),
				"session-id" => $_SESSION['sidotomoto'],
				"webapi-key" => $this->webapi
			);
			if($akcja == 'aktywuj')
			{
				$response = $this->klientOtomoto->__soapCall('doDealerInsertionActivate', $parameters);
			}
			if($akcja == 'deaktywuj')
			{
				$response = $this->klientOtomoto->__soapCall('doDealerInsertionInactivate', $parameters);
			}
			echo 'true';
		}
		catch(Exception $e)
		{
			echo 'false';
		}
		else echo 'false';
	}
	
	function pobkatallegroivAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		
		if($this->serwisNiedostepny === false)
		{
			$kategoria_id = $this->_request->getPost('kategoria_allegro');
			$typ_pojazdu = $this->_request->getPost('typ_pojazdu');
			$parametry = array
			(
				'webapi-key' => $this->webapi
			);
			$kategorie_allegro = $this->klientOtomoto->__soapCall('getAllegroCategories', $parametry);
			$select = '';
			if(!empty($kategorie_allegro))
			{
				foreach($kategorie_allegro as $kategoria_I)
				foreach($kategoria_I->children as $kategoria_II)
				if(strtoupper($kategoria_II->name) == $typ_pojazdu) //samochody lub motocykle
				{
					foreach($kategoria_II->children as $kategoria_III)
					foreach($kategoria_III->children as $kategoria_IV)
					foreach($kategoria_IV->children as $kategoria_V)
					if($kategoria_V->id == $kategoria_id)
					{
						foreach($kategoria_V->children as $kategoria_VI)
						{
							$select .= '<option value="'.$kategoria_VI->id.'">'.$kategoria_VI->name.'</option>';
						}
						break;
					}
				}
			}
			if(empty($select)) $select = '<option value="-1">-- Brak podkategorii ! --</option>';
			echo $select;
		}
		else echo '<option value="-1">-- Błąd połączenia ! --</option>';
	}
	function pobkatallegroiiiAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		
		if($this->serwisNiedostepny === false)
		{
			$kategoria_id = $this->_request->getPost('kategoria_allegro');
			$typ_pojazdu = $this->_request->getPost('typ_pojazdu');
			$parametry = array
			(
				'webapi-key' => $this->webapi
			);
			$kategorie_allegro = $this->klientOtomoto->__soapCall('getAllegroCategories', $parametry);
			$select = '';
			if(!empty($kategorie_allegro))
			{
				foreach($kategorie_allegro as $kategoria_I)
				foreach($kategoria_I->children as $kategoria_II)
				if(strtoupper($kategoria_II->name) == $typ_pojazdu) // samochody lub motocykle
				{
					foreach($kategoria_II->children as $kategoria_III)
					foreach($kategoria_III->children as $kategoria_IV)
					{
						if($kategoria_IV->id == $kategoria_id)
						{
							foreach($kategoria_IV->children as $kategoria_V)
							{
								$select .= '<option value="'.$kategoria_V->id.'">'.$kategoria_V->name.'</option>';
							}
							break;
						}
					}
				}
			}
			if(empty($select)) $select = '<option value="-1">-- Brak podkategorii ! --</option>';
			echo $select;
		}
		else echo '<option value="-1">-- Błąd połączenia ! --</option>';
	}
	function pobkatallegroiiAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		
		if($this->serwisNiedostepny === false)
		{
			$kategoria_id = $this->_request->getPost('kategoria_allegro');
			$typ_pojazdu = $this->_request->getPost('typ_pojazdu');
			$parametry = array
			(
				'webapi-key' => $this->webapi
			);
			$kategorie_allegro = $this->klientOtomoto->__soapCall('getAllegroCategories', $parametry);
			$select = '';
			if(!empty($kategorie_allegro))
			{
				foreach($kategorie_allegro as $kategoria_I)
				foreach($kategoria_I->children as $kategoria_II)
				if(strtoupper($kategoria_II->name) == $typ_pojazdu) // samochody lub motocykle
				{
					foreach($kategoria_II->children as $kategoria_III)
					if($kategoria_III->id == $kategoria_id)
					{
						foreach($kategoria_III->children as $kategoria_IV)
						{
							$select .= '<option value="'.$kategoria_IV->id.'">'.$kategoria_IV->name.'</option>';
						}
						break;
					}
				}
			}
			if(empty($select)) $select = '<option value="-1">-- Brak podkategorii ! --</option>';
			echo $select;
		}
		else echo '<option value="-1">-- Błąd połączenia ! --</option>';
	}
	function pobkatallegroiAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
		
		if($this->serwisNiedostepny === false)
		{
			$typ_pojazdu = $this->_request->getPost('typ_pojazdu');
			$parametry = array
			(
				'webapi-key' => $this->webapi
			);
			$kategorie_allegro = $this->klientOtomoto->__soapCall('getAllegroCategories', $parametry);
			$select = '';
			if(!empty($kategorie_allegro))
			{
				foreach($kategorie_allegro as $kategoria_I)
				foreach($kategoria_I->children as $kategoria_II)
				{
					if(strtoupper($kategoria_II->name) == $typ_pojazdu) // samochody lub motocykle
					{
						foreach($kategoria_II->children as $kategoria_III)
						{
							$select .= '<option value="'.$kategoria_III->id.'">'.$kategoria_III->name.'</option>';
						}
						break;
					}
				}
			}
			if(empty($select)) $select = '<option value="-1">-- Brak podkategorii ! --</option>';
			echo $select;
		}
		else echo '<option value="-1">-- Błąd połączenia ! --</option>';
	}
}
?>