<?php
class Ajax_WebserviceController extends Ogolny_Controller_Ajax
{
	private $id;

	public function init()
	{
		parent::init();
		$this->view->www = $_SERVER['HTTP_HOST'];
		$this->view->baseUrl = $this->_request->getBaseUrl();
		$this->view->path = str_replace('//', '/', $this->view->www.'/'.$this->view->baseUrl);
		$this->root = $_SERVER['DOCUMENT_ROOT'];
		
		$this->url = 'http://'.$this->view->path.'/ajax/webservice/soap';
	}

	function indexAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		echo $this->url;
	}
	
	function wsdlAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		
		$lojalnosc = new Lojalnosc();
		$login = $lojalnosc->login;
		$haslo = $lojalnosc->haslo;
		
		ini_set("soap.wsdl_cache_enabled", "0");
		
		$vars = array
		(
			"trace" => 1,
			"exceptions" => 1,
			//"soap_version" => SOAP_1_1,
			'cache_wsdl' => WSDL_CACHE_NONE
		);
		$credentials = array
		(
			"login" => $login,
			"haslo" => $haslo
			//"ApiKey" => ""
		);
		$credentials = (object)$credentials;
		
		if(false)
		{
			$soap = new SoapClient($this->url, $vars);
			$result = $soap->Add(2,3,4);
			var_dump($result);
			$request = $soap->__getLastRequest();
			echo htmlspecialchars($request);
			$response = $soap->__getLastResponse();
			var_dump($response);
		}
		
		$options = array
		(
			'location' => $this->url,
			'uri'      => $this->url.'?wsdl'
		);
		
		//if(false)
		try
		{
			//$client = new SoapClient($this->url, $vars);
			//$client = new Zend_Soap_Client($this->url);
			$client = new Zend_Soap_Client(null, $options);
			//$result = $client->Test();
			//$result = $client->ZwrocBlad('blad');
			$result = $client->SprawdzLogin($credentials);
			var_dump($result);
			//$request = $client->getLastRequest();
			//echo '<br>req=<br><br>'.(str_replace('&gt;&lt;','&gt;<br/>&lt;',htmlspecialchars($request))).'<br>';
			//$response = $client->getLastResponse();
			//echo '<br>res=<br><br>'.(str_replace('&gt;&lt;','&gt;<br/>&lt;',htmlspecialchars($response))).'<br>';
		}
		catch(SoapFault $sf)
		{
			die('SoapFault: [' . $sf->faultcode . '] ' . $sf->faultstring);
		}
		catch(SoapError $se)
		{
			die('SoapError: [' . $se->faultcode . '] ' . $se->faultstring);
		}
		catch(Exception $e)
		{
			die('Exception: ' . $e->getMessage());
		}
	}
	
	function soapAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		
		if(isset($_GET['wsdl']))
		{
			ini_set("soap.wsdl_cache_enabled", "0");
			$autodiscover = new Zend_Soap_AutoDiscover('Zend_Soap_Wsdl_Strategy_ArrayOfTypeComplex');
			//$strategy = new Zend_Soap_Wsdl_Strategy_ArrayOfTypeComplex();
			//$strategy->setAdditionalAttributes(array('minOccurs','maxOccurs'));
			//$autodiscover->setComplexTypeStrategy($strategy);
			if(false)
			$autodiscover->setOperationBodyStyle
			(
				array
				(
					'use' => 'encoded',//'literal',
					'namespace' => 'http://schemas.xmlsoap.org/soap/encoding/'//http://framework.zend.com'
				)
			);
			if(false)
			$autodiscover->setBindingStyle
			(
				array
				(
					'style' => 'rpc',//'document',
					'transport' => 'http://schemas.xmlsoap.org/soap/http'//http://framework.zend.com'
				)
			);
			$autodiscover->setClass('Lojalnosc');
			//$autodiscover->addFunction('ZwrocBlad');
			//$autodiscover->setUri($this->url);
			$autodiscover->handle();
		}
		else
		{
			ini_set("soap.wsdl_cache_enabled", "0");
			// pointing to the current file here
			//die();
			//$server = new SoapServer($wsdl);
			//$server = new Zend_Soap_Server($wsdl.'?wsdl');
			$server = new Zend_Soap_Server(null, array('uri' => $this->url.'?wsdl'));
			$server->setClass('Lojalnosc');
			//$server->setObject(new My_SoapServer_Class());
			//$server->addFunction('ZwrocBlad');
			$server->setReturnResponse(false);
			//$server->setRequest();
			//$request = $server->getLastRequest();
			$server->handle();				
			//return $response;
		}
	}
}
?>