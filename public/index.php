<?php 
	ini_set("memory_limit", "512M");
	ini_set('max_input_vars', 9999);
	date_default_timezone_set('Europe/Warsaw');
	//setlocale(LC_ALL, array('pl_PL', 'pl', 'Polish_Poland.28592'));
	
	$local = ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'www.local.d');
	if(false || $local || @$_GET['debug'])
	{
		if($local) session_save_path('../temp');
		error_reporting(E_ALL);
		ini_set('display_errors', true);
	}
	else
	{
		error_reporting(0);
		ini_set('display_errors', false);
	}
	
	// Define path to application directory
	defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

	// Define application environment
	defined('APPLICATION_ENV') || define('APPLICATION_ENV', 
		(getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production')); //development

	//echo realpath(APPLICATION_PATH . '/../../__library');die();
	set_include_path
	('.'
		. PATH_SEPARATOR . '../library'
		. PATH_SEPARATOR . realpath(APPLICATION_PATH . '/../../__library')
		. PATH_SEPARATOR . '../library/PEAR'
		. PATH_SEPARATOR . '../library/PHPExcel/Classes'
		. PATH_SEPARATOR . realpath(APPLICATION_PATH . '/../../__library/PHPExcel/Classes')
		. PATH_SEPARATOR . '../application/models'
		. PATH_SEPARATOR . realpath(APPLICATION_PATH . '/../../__models')
		. PATH_SEPARATOR . '../public/scripts/fckeditor'
		. PATH_SEPARATOR . realpath(APPLICATION_PATH . '/../../__scripts/fckeditor')
		. PATH_SEPARATOR . '../public/scripts/ckeditor'
		. PATH_SEPARATOR . realpath(APPLICATION_PATH . '/../../__scripts/ckeditor')
		. PATH_SEPARATOR . '../library/PDFMerger'
		. PATH_SEPARATOR . realpath(APPLICATION_PATH . '/../../__library/PDFMerger')
		. PATH_SEPARATOR . get_include_path()
	);
        
	require_once 'Zend/Loader/Autoloader.php';	
	require_once 'Dump.php';
        
	$autoloader = Zend_Loader_Autoloader::getInstance();
	$autoloader->setFallbackAutoloader(true);
	
	$config = new Zend_Config_Ini('../application/config.ini', 'general');
	if($config->edytor == 'fck') require_once 'fckeditor.php';
	if($config->edytor == 'cke') require_once 'ckeditor.php';
	//var_dump($config->baseUrl);
    //Zend_Session::$_unitTestEnabled = true;
	
	//setup controller
	$frontController = Zend_Controller_Front::getInstance();
	$frontController->throwExceptions(true);
	$frontController->setBaseUrl('/'.$config->baseUrl.'/');
	$frontController->setParam('useDefaultControllerAlways', true);
	$frontController->setControllerDirectory
	(
		array
		(
			'default' => '../application/default/controllers',
			'admin' => '../application/admin/controllers',
			'sklep' => '../application/sklep/controllers',
			'szablon' => '../application/szablon/controllers',
			'platnosci' => '../application/platnosci/controllers',
			'ajax' => '../application/ajax/controllers'
		)
	);
		
	/* Dom�lny router */
	try
    {
        $db = Zend_Db::factory($config->db->adapter, $config->db->config->toArray());
        Zend_Db_Table::setDefaultAdapter($db);
        $db->query('SET NAMES UTF8');
		$db->query('SET group_concat_max_len=1000000000');
    } 
    catch(Zend_Db_Adapter_Exception $e)
    {
        header('Content-type: text/html; charset=utf-8');
        die('Przepraszamy, b��d serwera bazy danych / Sorry, database server error');
    } 
    catch (Zend_Exception $e) 
    {
        header('Content-type: text/html; charset=utf-8');
        die('Przepraszamy, b��d serwera bazy danych / Sorry, database server error.');
    }	
	
	if(true)
	{
		/* Cache describe tabeli*/
		$frontendOptions = array('automatic_serialization' => true);
		$backendOptions  = array('cache_dir' => '../public/cache');
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
		
		/* Cache inne zapytania do bazy*/
		$frontend_ = array('lifetime' => 600, 'automatic_serialization' => true);
		$backend_ = array('cache_dir' => '../public/cache');
		$cache_ = Zend_Cache::factory('Core', 'File', $frontend_, $backend_);
		$session = new Zend_Session_Namespace();
		$session->cache = $cache_;
		$session->cache_ = $cache;
	}
	
	if($config->profiler)
	{
		$profiler = new Zend_Db_Profiler_Firebug('All DB Queries');
		$profiler->setEnabled(true);
		$db->setProfiler($profiler);
	}
	
	if($config->logger)
	{
		$logger = new Zend_Log();
		$writer = new Zend_Log_Writer_Firebug();
		$writer->setEnabled(true);
		$logger->addWriter($writer);
		Zend_Registry::set('logger', $logger);
	}
	
	$routers = new Routers();
	//$trasy = $routers->wczytaj();	
	$pathUrl = $_SERVER['REQUEST_URI'];
	if(!empty($config->baseUrl))
	$pathUrl = str_replace($config->baseUrl.'/', '', $pathUrl);
	$pathUrl = explode('/', $pathUrl);
	$pathUrl = array_slice($pathUrl, 0);
	if(!empty($pathUrl))
	{
		$trasy = $routers->wczytajPojedynczy($pathUrl);
		//print_r($pathUrl);
		$config_tras = new Zend_Config($trasy);
		$router = $frontController->getRouter();
		$router->addConfig($config_tras, 'routes');
	}
	
	$frontController->registerPlugin(new Ogolny_Controller_Plugin_Notfound());
    $frontController->registerPlugin(new Ogolny_Controller_Plugin_Translate());
	$frontController->dispatch();	
?>