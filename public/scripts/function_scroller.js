var easingTypes = new Array();
easingTypes[0] = ['linear', 'swing'];
easingTypes[1] = ['easeInQuad', 'easeOutQuad', 'easeInOutQuad'];
easingTypes[2] = ['easeInCubic', 'easeOutCubic', 'easeInOutCubic'];
easingTypes[3] = ['easeInQuart', 'easeOutQuart', 'easeInOutQuart'];
easingTypes[4] = ['easeInQuint', 'easeOutQuint', 'easeInOutQuint'];
easingTypes[5] = ['easeInSine', 'easeOutSine', 'easeInOutSine'];
easingTypes[6] = ['easeInExpo', 'easeOutExpo', 'easeInOutExpo'];
easingTypes[7] = ['easeInCirc', 'easeOutCirc', 'easeInOutCirc'];
easingTypes[8] = ['easeInElastic', 'easeOutElastic', 'easeInOutElastic'];
easingTypes[9] = ['easeInBack', 'easeOutBack', 'easeInOutBack'];
easingTypes[10] = ['easeInBounce', 'easeOutBounce', 'easeInOutBounce'];
var easingGroup = Math.floor(Math.random() * easingTypes.length);
var easingType = Math.floor(Math.random() * easingTypes[easingGroup].length);
var easing = easingTypes[easingGroup][easingType];

if(false)
{
	if(false)
	{
		var prod = new Array();
		var prods = new Array();
		var prodsTimer = new Array();
		function skrolujProdukty(typ, ile, time, random)
		{
			if(ile == undefined) ile = 1;
			if(time == undefined) time = 5000;
			if(random == undefined) random = true;
			var ilosc = $('.items' + typ + ' .item').size();
			if(ilosc < ile) ile = ilosc;
			$('.items' + typ).find('.item').hide();		
			if(prod[typ] == undefined) prod[typ] = 0;
			if(!random)
			{
				$('.items' + typ + ' .scrollers a').removeClass('scroller_active');
				$('.items' + typ + ' .scroller' + prod[typ] / ile).addClass('scroller_active');
			}
			prods[typ] = new Array();
			for(i = 0; i < ile; i++)
			{
				if(random)
				{
					do rand = Math.floor(Math.random() * ilosc);
					while(prods[typ][rand] != undefined);
					$($('.items' + typ + ' .item').get(rand)).show();
					prods[typ][rand] = rand;
					//alert(rand);
				}
				else
				{
					$($('.items' + typ + ' .item').get(prod[typ])).show();
					prod[typ]++;
					if(prod[typ] >= ilosc)
					{
						prod[typ] = 0;
						break;
					}
				}	
			}		
			prodsTimer[typ] = setTimeout('skrolujProdukty("' + typ + '", ' + ile + ', ' + time + ', ' + random + ')', time);
		}
		function skrolujProduktyRecznie(ten, i, typ, ile, time, random)
		{
			var ilosc = $('.items' + typ + ' .item').size();
			if(prod[typ] < 0) prod[typ] = 0;
			if(i == "left") prod[typ] = Math.max(0, Math.ceil(prod[typ]/ile)*ile - ile*2);
			if(i == "right" && ((prod[typ] + 0)) >= ilosc) return;
			if(i == "right") prod[typ] = prod[typ] + 0;
			//console.log(prod[typ] + " " + ilosc + " " + ile);
			if(!isNaN(i))
			{
				prod[typ] = i * ile;
				$('.items' + typ + ' .scrollers a').removeClass('scroller_active');
				$(ten).addClass('scroller_active');
			}
			if(prodsTimer[typ] != undefined) clearTimeout(prodsTimer[typ]);
			skrolujProdukty(typ, ile, time, random);
		}
	}

	if(false)
	{
		var imgNow = 1;
		var galeriaKrok = 0;
		var galeriaAuto = true;
		var galeriaTimer = null;
		function skrolujImg(typ, ile, time)
		{
			if(ile == undefined) ile = 1;
			if(time == undefined) time = 5000;
			var ilosc = $('#' + typ).find('a.img img').size();
			if(ilosc < 2) return;
			if(ilosc < ile) ile = ilosc;
			$('#' + typ).find('a.img img').hide();
			$('#' + typ).find('a.span').hide();
			if(++imgNow == ilosc) imgNow = 0;
			var img = $('#' + typ).find('a.img img').get(imgNow);
			$(img).show();
			var span = $('#' + typ).find('a.span').get(imgNow);
			$(span).show();
			//alert(ile);
			//if(ilosc > ile)
			setTimeout('skrolujImg("' + typ + '", ' + ile + ')', time);
		}
		function skrolujGalerie(typ, ile, time)
		{
			if(!galeriaAuto) return;
			
			if(ile == undefined) ile = 1;
			if(time == undefined) time = timeDef;
			if(time == 0) time = 5;
			time *= 1000;
			
			var ilosc = $('#' + typ).find('img').size();
			if(galeriaKrok == -1) galeriaKrok = ilosc - 1;
			var currentLi = $('#' + typ).find('li').get(galeriaKrok);
			var current = $('#' + typ).find('img').get(galeriaKrok);
			
			var fade = fadeDef;
			var fadeSpeed = fadeSpeedDef;
			//if(fadeSpeed == 0) fadeSpeed = 2;
			fadeSpeed *= 1000;
			if(fade == 100) fadeSpeed = 0;
			var zoom = zoomDef;
			//if(zoom == 0) zoom = 100;
			
			$('#' + typ).find('img').hide();
			$('#' + typ).find('table').hide();
			
			$(current).show();
			//$(current).css('zIndex', 2);
			$('#' + typ).find('li').css('zIndex', 2);
			
			$(current).animate(
			{
				opacity: 0
				//width: '+=100'
			}, 
			fadeSpeed, 
			function()
			{
				$(this).hide();
			});
			
			if(++galeriaKrok == ilosc) galeriaKrok = 0;
			
			var next = $('#' + typ).find('img').get(galeriaKrok);
			var nextLi = $('#' + typ).find('li').get(galeriaKrok);
			$(nextLi).css('zIndex', 3);
			//$(next).css('zIndex', 3);
			$(next).css('opacity', fade);
			$(next).css('width', 980);
			$(next).show();
			
			var currentTable = $('#' + typ).find('table').get(galeriaKrok);
			$(currentTable).show();
			
			$('.slajd').removeClass('slajdActive');
			$('#slajd' + (galeriaKrok + 1)).addClass('slajdActive');
			
			$(next).animate(
			{
				opacity: 1
				//width: '960'
			}, 
			fadeSpeed, 
			function()
			{
				$(this).show();
				galeriaTimer = setTimeout('skrolujGalerie("' + typ + '", ' + ile + ')', time);
				
				$(this).animate(
				{
					width: '+=' + zoom + '%',
					left: '-=' + zoom + '%'
				}, 
				time, 
				function()
				{
					$(this).hide();
				});
			});	
			//$('#' + typ).children('img').hide();
			//$($('#' + typ).children('img').get(galeriaKrok)).show();
			//setTimeout('skrolujGalerie("' + typ + '", ' + ile + ')', time);
		}
		function skrolujGalerieRecznie(i, typ)
		{
			clearTimeout(galeriaTimer);
			typ = 'slider';
			$('#' + typ).find('img').stop(true);
			var ilosc = $('#' + typ).find('img').size();
			galeriaKrok = i - 2;
			if(galeriaKrok < 0) galeriaKrok = ilosc - 1;
			galeriaAuto = true;
			skrolujGalerie(typ);
		}
	}
}

if(obConfig['slider'] == 'jcarousel')
{
	function mycarousel_itemFirstInCallbackBefore(carousel, li, index, state)
	{
		var indeks = (index % ileSlajdow);
		if(indeks == 0) indeks = ileSlajdow;
		if(indeks < 0) indeks = ileSlajdow + indeks;
		carousel.options.auto = parseInt($('#slajd' + indeks).attr('rel'));
		//alert(index);
		easingGroup = Math.floor(Math.random() * easingTypes.length);
		easingType = Math.floor(Math.random() * easingTypes[easingGroup].length);
		//carousel.options.easing = easingTypes[easingGroup][easingType];
	}
	function mycarousel_itemFirstInCallback(carousel, li, index, state)
	{
		var indeks = (index % ileSlajdow);
		if(indeks == 0) indeks = ileSlajdow;
		if(indeks < 0) indeks = ileSlajdow + indeks;
		$('.slajd').removeClass('slajdActive').addClass('slajdInactive');
		$('#slajd' + indeks).removeClass('slajdInactive').addClass('slajdActive');
		//slajdIndex = index;
		if(false)
		{
			var href = $('.jcarousel-item-' + index).children('a').attr('href');
			$('#flash').css('cursor', href == undefined ? 'default' : 'pointer');
			$('#mycarousel-more').toggle(href != undefined);
			if(href == undefined) href = 'javascript:void(0);';
			$('#flash').attr('href', href);
			$('#mycarousel-more').attr('href', href);
		}
	}
	function mycarousel_initCallback(carousel) 
	{
		$('.slajd').bind('click', function() 
		{
			//carousel.options.auto = parseInt($(this).attr('rel'));
			carousel.scroll(jQuery.jcarousel.intval($(this).text()));
			return false;
		});
		$('#mycarousel-next').bind('click', function() 
		{
			carousel.next();
			return false;
		});
		$('#mycarousel-prev').bind('click', function() 
		{
			carousel.prev();
			return false;
		});		
	};
}

if(false)
{
	function karuzela(id, time, size, align, wrap, scroll, auto, lisize)
	{
		var autop;
		var scrollp;

		/**Sprawdzenie czy mozna wlaczyc slider**/
		var counter = jQuery('#' + id + ' ul').children().length;

		if(true && counter < size)
		{
			autop = 0;
			scrollp = 0;
			oncar = 0;
		}
		else
		{
			autop = 1;
			oncar = 1;
			scrollp = scroll;
		}
		
		if(auto == 0) autop = 0;
		
		var lite = true;
		if(lite == false)
		{
			jQuery('#' + id).jcarousel(
			{
				vertical: + align,
				size: + lisize,
				auto: + autop,
				scroll: + scrollp,
				animation: + time,
				wrap: + wrap,
				initCallback: hoverCarousel
			});
		}
		else
		{
			if(oncar)
			{
				$("#" + id).jCarouselLite(
				{
					auto: auto,
					speed: time,
					visible: size,
					vertical: true,
					hoverPause: true
				});
			}
		}
	}

	/**OBSLUGA NAJAZDU NA KARUZELE**/
	function hoverCarousel(carousel)
	{
		carousel.clip.hover(function()
		{
			carousel.stopAuto();
		},
		function()
		{
			carousel.startAuto();
		});
	}
	function afterAnimation(carousel)
	{
		if(carousel.autoStopped)
		{
			carousel.startAuto()
		}
	}
}

if(obConfig['scroller'])
function getjTscrollerSize(div, typ)
{
	var width = 0;
	var marginLeft = 0;
	var marginRight = 0;
	var poprawka = 0;
	$('#' + div + ' .jTscroller a').each(function(key, val)
	{
		if(typ == 'horiz') width += parseInt($(this).width());
		if(typ == 'vert') width += parseInt($(this).height());
		if(typ == 'horiz')
		{
			marginLeft += parseInt($(this).css('marginLeft'));
			marginRight += parseInt($(this).css('marginRight'));
		}
		if(typ == 'vert')
		{
			marginLeft += parseInt($(this).css('marginTop'));
			marginRight += parseInt($(this).css('marginBottom'));
		}
		poprawka += 2;
		//alert($(this).width());
	});
	//alert(div + ' ' + width + ' ' + marginLeft + ' ' + marginRight + ' ' + poprawka + ' ' + widthAll);
	var widthAll = width + marginLeft + marginRight + poprawka;
	if(typ == 'horiz') $('#' + div + ' .jTscroller').width(widthAll);
	if(typ == 'vert') $('#' + div + ' .jTscroller').height(widthAll);
}

if(obConfig['slajdy'])
$(document).ready(function()
{
	if(obConfig['slider'] == 'nivoSlider')
	$('#slider').nivoSlider(
	{
		effect: 'random', // Specify sets like: 'fold,fade,sliceDown'
		//sliceDown sliceDownLeft sliceUp sliceUpLeft sliceUpDown sliceUpDownLeft fold fade random 
		//slideInRight slideInLeft boxRandom boxRain boxRainReverse boxRainGrow boxRainGrowReverse
		slices: 15, // For slice animations
		boxCols: 8, // For box animations
		boxRows: 4, // For box animations
		animSpeed: 500, // Slide transition speed
		pauseTime: 5000, // How long each slide will show
		startSlide: 0, // Set starting Slide (0 index)
		directionNav: false, // Next & Prev navigation
		directionNavHide: true, // Only show on hover
		controlNav: false, // 1,2,3... navigation
		controlNavThumbs: false, // Use thumbnails for Control Nav
		controlNavThumbsFromRel: false, // Use image rel for thumbs
		controlNavThumbsSearch: '.jpg', // Replace this with...
		controlNavThumbsReplace: '_thumb.jpg', // ...this in thumb Image src
		keyboardNav: true, // Use left & right arrows
		pauseOnHover: true, // Stop animation while hovering
		manualAdvance: false, // Force manual transitions
		captionOpacity: 0.8, // Universal caption opacity
		prevText: 'Poprzedni', // Prev directionNav text
		nextText: 'Nastêpny', // Next directionNav text
		randomStart: false, // Start on a random slide
		beforeChange: function(){}, // Triggers before a slide transition
		afterChange: function(){}, // Triggers after a slide transition
		slideshowEnd: function(){}, // Triggers after all slides have been shown
		lastSlide: function(){}, // Triggers when last slide is shown
		afterLoad: function(){} // Triggers when slider has loaded
	});
	
	if(obConfig['slider'] == 'jcarousel')
	$('#slider').jcarousel(
	{
		vertical : false,//horizontal or vertical orientation
		rtl : false,//RTL (Right-To-Left) mode
		start : 1,//The index of the item to start with.
		offset : 1,//The index of the first available item at initialisation.
		size: slajdyIle,//The number of total items.
		scroll : 1,//The number of items to scroll by.
		visible : null,//If passed, the width/height of the items will be calculated and set depending on the width/height of the clipping, so that exactly that number of items will be visible.
		animation: "slow",//speed of scroll animation ("slow" or "fast") or milliseconds as integer
		easing : "swing",//The name of the easing effect that you want to use: null, "linear", "swing" or jquery UI
		auto : slajdyCzas,//how many seconds to autoscroll the content
		wrap : "circular",//"first", "last", "both" or "circular" If set to null, wrapping is turned off
		itemFallbackDimension: null,
		initCallback: mycarousel_initCallback,
		buttonNextHTML: null,
		buttonPrevHTML: null,
		itemFirstInCallback: 
		{
			onBeforeAnimation: mycarousel_itemFirstInCallbackBefore,
			onAfterAnimation: mycarousel_itemFirstInCallback
		}
	});
	$('.jcarousel-clip').append($('.jcarousel-control'));
});

$(window).load(function()
{	
	if(obConfig['scroller']) getjTscrollerSize('partnerzy', 'horiz');
	if(obConfig['scroller']) getjTscrollerSize('producenci', 'horiz');
	 //determine ThumbnailScroller on your own, gets width = 0 for images when in $(document).ready
	
	if(obConfig['scroller'])
	$("#partnerzy .jThumbnailScroller").thumbnailScroller(
	{
		scrollerType:"clickButtons",//hoverPrecise,hoverAccelerate,clickButtons
		scrollerOrientation:"horizontal",//horizontal,vertical
		scrollSpeed:1,//scrolling speed
		scrollEasing:"easeOutCirc",//only for “hoverPrecise”; jquery ui easing types
		scrollEasingAmount:800,//only for “hoverPrecise” and “clickButtons” (ms)
		acceleration:4,//only for “hoverAccelerate”
		scrollSpeed:800,//only for “clickButtons” (ms)
		noScrollCenterSpace:10,//only for “hoverAccelerate”
		autoScrolling:9999,//amount of auto-scrolling loops (integer)
		autoScrollingSpeed:($('#partnerzy .logo').size() * 5000),//initial auto-scrolling speed (ms)
		autoScrollingEasing:"easeInOutQuad",//initial auto-scrolling easing type; jquery ui easing types
		autoScrollingDelay:500//initial auto-scrolling delay for each loop (ms)
	});
	if(obConfig['scroller'])
	$("#producenci .jThumbnailScroller").thumbnailScroller(
	{
		scrollerType:"hoverPrecise",//hoverPrecise,hoverAccelerate,clickButtons
		scrollerOrientation:"horizontal",//horizontal,vertical
		scrollSpeed:1,//scrolling speed
		scrollEasing:"easeOutCirc",//only for “hoverPrecise”; jquery ui easing types
		scrollEasingAmount:800,//only for “hoverPrecise” and “clickButtons” (ms)
		acceleration:4,//only for “hoverAccelerate”
		scrollSpeed:800,//only for “clickButtons” (ms)
		noScrollCenterSpace:10,//only for “hoverAccelerate”
		autoScrolling:9999,//amount of auto-scrolling loops (integer)
		autoScrollingSpeed:($('#producenci .logo').size() * 5000),//initial auto-scrolling speed (ms)
		autoScrollingEasing:"easeInOutQuad",//initial auto-scrolling easing type; jquery ui easing types
		autoScrollingDelay:500//initial auto-scrolling delay for each loop (ms)
	});
	
	if(obConfig['scroller'])
	{
		$('.jTscrollerPrevButton').click(function()
		{
			//alert('x');
			$('.jcarousel-prev').trigger('click');
			return false;
		});
		$('.jTscrollerNextButton').click(function()
		{
			$('.jcarousel-next').trigger('click');
			return false;
		});
	}
});
