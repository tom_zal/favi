if(false)
function zmienStatusZamow(id_zam, status, czy_mail, print)
{
	if(edycjazablokowana) return;
	var param = 'id_zam/' + id_zam + '/status/' + status + '/czy_mail/' + czy_mail;
	//if(false)
	$.ajax(
	{
		url: baseUrl+"/ajax/index/zamowstatus/" + param,
		type: "GET",
		success: function(data)
		{
			//if(print > 0) printZam(print);
			if(print == 0) $.jGrowl(data, { life: 5000, position: 'center' });
		},
		error: function(data)
		{
			
		}
	});
	$('select[name=status]').val(status);
	if(print > 0) printZam(print, status);
}
function zamowListaOpcjeAllWiecej(ten, url)
{
	var akcja = '';
	var uwaga = '';
	var val = ten.value;
	if(val == '') { $(ten).val('wiecej'); return; }
	if(val == 'wiecej') return;
	var ileZazn = $('.checkbox:checked').length;
	var ileAll = 0;//<?php echo isset($this->ileProd) ? $this->ileProd : 0; ?>;
	var tryb = 'zadania_wybrane';//$(ten).attr('name') == 'opcje3' ? 'wyszukiwarka' : 'zadania_wybrane';
	var ile = tryb == 'wyszukiwarka' ? ileAll : ileZazn;
	var czego = '';
	
	if(ile == 0)
	{
		if(tryb == 'zadania_wybrane') alert('Nie zaznaczono żadnych zamówień!');
		if(tryb == 'wyszukiwarka') alert('Na liście nie ma zamówień!');
		$(ten).val('wiecej');
		return;
	}
	
	switch(val)
	{
		case 'usun' : 
		akcja = 'admin/zamowienia/wypisz/delids'; 
		uwaga = 'Czy na pewno chcesz usunąć zaznaczone zamówienia ?';
		czego = 'usunięcia';
		break;
		case 'paczki':
		akcja = 'admin/paczka/utworz';
		break;
		case 'wysylkowe':
		akcja = 'admin/paczka/dodaj';
		break;
		case 'usunlist':
		akcja = 'admin/paczka/utworz';
		uwaga = 'Czy na pewno chcesz usunąć zaznaczone paczki ?';
		czego = 'usunięcia';
		break;
		case 'usunwysylka':
		akcja = 'admin/paczka/lista';
		uwaga = 'Czy na pewno chcesz usunąć zaznaczone listy wysyłkowe ?';
		czego = 'usunięcia';
		break;
		default : 
		akcja = 'admin/zamowienia/wypisz';
	}
	var klasa = $(ten).find('option:selected').attr('class');
	if(klasa != undefined && klasa.indexOf("_label") > 0)
	{
		var status = $(ten).find('option:selected').text();
		uwaga = 'Czy na pewno chcesz zmienić status zaznaczonym zamówieniom na: ' + status + ' ?';
	}
	
	$('#form').attr('action', url + '/' + akcja + '/all');
	if(uwaga.length == 0 || window.confirm(uwaga))
	{
		if(false) ProducentWindow(tryb, ile, ten);
		else $('#form').submit();
	}
	else $(ten).val('wiecej');
}
function zamowListaOpcjeAllWiecejAll(ten, url, mode)
{	
	var akcja = '';
	var uwaga = '';
	var val = ten.value;
	if(val == '') { $(ten).val('wiecej'); return; }
	if(val == 'wiecej') return;
	var ileZazn = $('.checkbox:checked').length;
	var ileAll = 0;//<?php echo isset($this->ileProd) ? $this->ileProd : 0; ?>;
	var tryb = 'zadania_wybrane';//$(ten).attr('name') == 'opcje3' ? 'wyszukiwarka' : 'zadania_wybrane';
	var ile = tryb == 'wyszukiwarka' ? ileAll : ileZazn;
	var czego = '';
	
	if(ile == 0)
	{
		if(tryb == 'zadania_wybrane') alert('Nie zaznaczono żadnych zamówień!');
		if(tryb == 'wyszukiwarka') alert('Na liście nie ma zamówień!');
		$(ten).val('wiecej');
		return;
	}
	
	switch(val)
	{
		case 'paczki_allegro':
		akcja = 'admin/paczka/utworz';
		uwaga = 'Czy na pewno chcesz utworzyć paczki z zaznaczonych zamówień ?';
		break;
		case 'usunnazawsze' : 
		akcja = 'admin/allegrozamowienia/wypisz/delids'; 
		uwaga = 'Czy na pewno chcesz usunąć zaznaczone zamówienia ?';
		czego = 'usunięcia';
		break;
		case 'adres_zamien_wszystkie' : 
		akcja = 'admin/allegrozamowienia/wypisz/tryb/aktualne/zadaniezamien/1/mode'; 
		uwaga = 'Czy na pewno chcesz wykonać operacje zamiany adresów dla wszystkich widocznych zamówień ?';
		break;
		default : 
		akcja = 'admin/allegrozamowienia/wypisz';
		if(mode.length > 0) { akcja += '/mode_do_druku/'+mode;}
	}
	
	$('#form').attr('action', url + '/' + akcja + '/all');
	if(uwaga.length == 0 || window.confirm(uwaga))
	{
		if(val == 'producent') ProducentWindow(tryb, ile, ten);
		else $('#form').submit();
	}
	else $(ten).val('wiecej');
}

function kopiuj(id, typ, kurier)
{
    switch(typ)
	{
        case 'paczka':
            var Replace = 'jq-danePaczka-';
            var obecna = id.replace(Replace,'');
            var ids = znajdzIdPaczek(obecna, '.jq-wymiary', Replace, kurier);
            kopiujPola('#'+id+'-con', ids);
        break;
        case 'usluga':
            var Replace = 'jq-daneUsluga-';
            var obecna = id.replace(Replace,'');
            var ids = znajdzIdPaczek(obecna, '.jq-uslugi',Replace);
            console.log(ids);
            console.log(obecna);
            kopiujPola('#'+id+'-con', ids);
        break;  
    }
}
function kopiujPola(idElement, ids)
{
    var intRegex = /[0-9]+/;
    $(idElement).find('input, select, textarea').each(function() //pobranie kopiowanych elementow
	{
        var name = this.name;
        var value = this.value;
        var type= this.type;
        var input = this;
        $.each(ids, function( index, idk ) //id pola do ktorych mamy wrzucic kopiowane wartosci
		{
            var newname  = name.replace(intRegex, idk);
            switch(type)
			{
                case 'select-one':
                    $('select[name="'+newname+'"] option').removeAttr('selected');
                    $('select[name="'+newname+'"] option[value="'+value+'"]').attr("selected", 'selected');
                break;
                case 'checkbox':
                    if(input.checked)
					{
                        $('input[name="'+newname+'"]').attr("checked", 'checked');
                    }
                    else
					{
                        $('input[name="'+newname+'"]').removeAttr('checked'); 
                    }
                break;
                case 'textarea':
                    $('textarea[name="'+newname+'"]').text(value); 
                break;
                default:
                    $('input[name="'+newname+'"]').val(value);
                break;    
            }
        });
    });
}
function znajdzIdPaczek(obecna, klasa, Replace, kurier)
{
    var tab = [];
    var i =0;
    $('.paczka'+obecna).next('.jedna_paczka.'+kurier).find(klasa).each(function()
	{
        var id = this.id.replace(Replace,'');
        id = id.replace('-con','');
        if(id !==obecna)
		{
            tab[i] = id;
            i++;
        } 
    });
    return tab;
}
function showRow(symbol)
{
    $('.paczkaListaKontener').hide();
    $('.blokPaczek').hide();
    $('#list'+symbol).show();
    $('.blokPaczek').toggle();
}

$(document).ready(function()
{
	$('a.podglad_zam').hover
	(
		function()
		{
			var value = $(this).attr('link');
			var typ = $(this).attr('data-val-typ');
			if(value.indexOf('typ/allegro') > 0) typ = 'allegro';
			$('table#tablea_zam_prod').empty();
			$('table#tablea_zam_prod').append('<tr class="white"><td align="center" width="20"><b>LP</b></td><td align="center" width="80"><b>'+(typ=='allegro'?'ID oferty':'Symbol')+'</b></td><td align="center" width="100" class="zdj"><b>Miniaturka</b></td><td align="center"><b>Nazwa</b></td><td align="center" width="80"><b>Ilość</b></td><td align="center" width="70"><b>Cena brutto</b></td><td align="center" width="70"><b>Wartość brutto</b></td></tr>');
			$('div#podglad_zam').css('display','block');			
			$.ajax(
			{
				url: baseUrl+"/ajax/index/tooltipzamowienie"+value,
				type: "GET",
				async: false,
				success: function(html)
				{
					var xml = $.parseXML(html);
					//alert($(xml).find('root'));
					var ile = $(xml).find('produkt').length;
					if(ile > 0)
					{
						$(xml).find('produkt').each(function(i, v)
						{							
							var id = $(this).find('id').text();
							var zdjecie = $(this).find('zdjecie').text();
							var nazwa = $(this).find('nazwa').text();
							var ilosc = $(this).find('ilosc').text();
							var cena_brutto = $(this).find('cena_brutto').text();
							var wartosc = $(this).find('wartosc').text();
							$('table#tablea_zam_prod').append('<tr><td align="center">'+(i+1)+'</td><td align="center">'+id+'</td><td align="center"><img alt="" width="100" src="'+zdjecie+'" /></td><td align="center">'+nazwa+'</td><td align="center">'+ilosc+'</td><td align="center">'+cena_brutto+'</td><td align="center">'+wartosc+'</td></tr>');
						});
					}
				}
			});
		},
		function()
		{
			$('div#podglad_zam').css('display','none');
			//$('table#tablea_zam_prod').empty();
		}
	);
});

function zamowKuriera(ten, dostawca)
{
	if(dostawca == "dhl") KurierDHLWindow(ten);
	return false;
}
function KurierDHLWindow(ten)
{
	$('#kurierDHLWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 500,
		height: 280,
		title: "Zamów kuriera DHL",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			if($('#pickupDate').val().length == 0)
			$('#pickupDate').val($(ten).attr('data-val-dataPrzesylka'));
		},
		buttons: 
		{
			'Anuluj': function()
			{
				$(this).dialog('close');
			},
			'Ok': function()
			{
				if(window.confirm('Czy na pewno chcesz zamówić kuriera dla tej listy wysyłkowej ?'))
				{
					$('#kurierDHLWindowForm').attr('action', $(ten).attr('href')+'?debug=1');
					$('#kurierDHLWindowForm').submit();
				}
				//else $(this).dialog('close');
			}
		},
		beforeClose: function(event, ui) 
		{
			
		},
		close: function(event, ui)
		{
			
		}
	});
}