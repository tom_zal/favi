if(obConfig['produktyPowiazania'] || obConfig['rabatyKlienciProdukty'])
{
	$(document).ready(function()
	{
		$('#jQuery_zapisz_btn').click(function(e)
		{
			$('#powiazania option').attr('selected', 'selected');
			//e.preventDefault();
			return true;
		});
	});
	function szukajProd(ten)
	{
		if(ten.value.length < 3)
		{
			$('#powiazanie').html('<option value="0">Wpisz co najmniej 3 znaki!</option>');
		}
		else
		$.ajax(
		{
			url: baseUrl+'/ajax/index/szukajprod/q/'+encodeURIComponent(ten.value),
			cache: false,
			timeout : 5000,
			error: function(jqXHR, textStatus, errorThrown)
			{
				$('#powiazanie').html('<option value="0">'+errorThrown+'</option>');
			},
			success: function(html, textStatus, jqXHR)
			{
				var xml = $.parseXML(html);
				var ile = $(xml).find('prod').length;
				$('#powiazanie').html('');
				if(ile > 0)
				{
					$(xml).find('prod').each(function()
					{
						var id = $(this).find('id').text();
						var nazwa = $(this).find('nazwa').text();
						$('#powiazanie').append('<option value="'+id+'">'+nazwa+'</option>');
					});
				}
			}
		});
	}
	function zaznaczPowiazania(src, zazn)
	{
		$('#'+src).find('option').each(function()
		{
			var id = $(this).val();
			$(this).attr('selected', zazn && parseInt(id) > 0 ? 'selected' : false);
		});
	}
	function dodajPowiazania(src, dest, all)
	{
		$('#'+src).find('option').each(function()
		{
			var id = $(this).val();
			var nazwa = $(this).text();
			//console.log(id + " " + nazwa+ " " + $(this).is(':selected'));
			if(all || $(this).is(':selected'))
			if(parseInt(id) > 0 && $('#'+dest+' option[value="'+id+'"]').attr('value') == undefined)
			$('#'+dest).append('<option value="'+id+'">'+nazwa+'</option>');
		});
	}
	function usunPowiazania(src, all)
	{
		$('#'+src).find('option').each(function()
		{
			var id = $(this).val();
			//var nazwa = $(this).text();
			//console.log(id + " " + nazwa+ " " + $(this).is(':selected'));
			if(all || $(this).is(':selected'))
			if(parseInt(id) > 0 && $('#'+src+' option[value="'+id+'"]').attr('value') != undefined)
			$('#'+src).find('option[value="'+id+'"]').remove();
		});
	}
	function dodajPowiazanie(src, dest)
	{
		var id = $('#'+src).val();
		var nazwa = $('#'+src).find('option[value="'+id+'"]').text();
		//console.log(id + " " + nazwa+ " " + $('#'+dest+' option[value="'+id+'"]').attr('value'));
		if(parseInt(id) > 0 && $('#'+dest+' option[value="'+id+'"]').attr('value') == undefined)
		$('#'+dest).append('<option value="'+id+'">'+nazwa+'</option>');
	}
	function usunPowiazanie(src)
	{
		var id = $('#'+src).val();
		//var nazwa = $('#'+src).find('option[value="'+id+'"]').text();
		//console.log(id + " " + nazwa+ " " + $('#'+src).find('option[value="'+id+'"]').attr('value'));
		if(parseInt(id) > 0 && $('#'+src).find('option[value="'+id+'"]').attr('value') != undefined)
		$('#'+src).find('option[value="'+id+'"]').remove();
	}
}

if(obConfig['produktyGratisy'])
function szukajGratis(ten)
{
	if(ten.value.length < 3)
	{
		$('#gratis').html('<option value="0">Wpisz co najmniej 3 znaki!</option>');
	}
	else
	$.ajax(
	{
		url: baseUrl+'/ajax/index/szukajgratis/q/'+encodeURIComponent(ten.value),
		cache: false,
		timeout : 5000,
		error: function(jqXHR, textStatus, errorThrown)
		{
			$('#gratis').html('<option value="0">'+errorThrown+'</option>');
		},
		success: function(html, textStatus, jqXHR)
		{
			var xml = $.parseXML(html);
			var ile = $(xml).find('gratis').length;
			$('#gratis').html('');
			if(ile > 0)
			{
				$(xml).find('gratis').each(function()
				{
					var id = $(this).find('id').text();
					var nazwa = $(this).find('nazwa').text();
					$('#gratis').append('<option value="'+id+'">'+nazwa+'</option>');
				});
			}
		}
	});
}