$(document).ready(function()
{
	if(false)
	$('#producent').selectbox(
	{
		onChange: function (val, inst) 
		{
			var nazwa = $(this).attr('name');
			var value = $(this).children('option:selected').val();
			if(true)
			window.location = baseUrl + '/' + wynikiLink + '/' + nazwa + '/' + value;
		},
		onOpen: function (inst) 
		{
			selectboxheightfix($(this));
		}
	});
	
	$('.sort span.select select').selectbox(
	{
		onChange: function (val, inst) 
		{
			var bg = $(this).children("option:selected").css("background-image");
			$(this).parent().find('.sbSelector').css('background-image', bg);
			
			var ide = $(this).attr('id');
			if(ide.substring(0, 7) == 'atrybut')
			{
				var atr = parseInt(ide.substring(7));
				changeListaAttr($(this), 'atr', 'atrybut', atr);
			}
			else changeListaAttr($(this));//if(ide != 'producent')
		},
		onOpen: function (inst) 
		{
			selectboxheightfix($(this));
		},
		onClose: function (inst) 
		{

		}
	});
	
	$('#produktKoszyk span.select select').selectbox(
	{
		onChange: function (val, inst) 
		{
			var bg = $(this).children("option:selected").css("background-image");
			$(this).parent().find('.sbSelector').css('background-image', bg);
			var ide = $(this).attr('id');
			if(ide == 'rozmiar')
			{
				$('#ilosc').attr('name', 'ilosc['+val+']');
				if(trybCena)
				$('#liczba').text($('[name=\'cena_brutto['+val+']\']').val());
			}
			if(ide.substring(0, 7) == 'atrybut')
			{
				var atr = parseInt(ide.substring(7));
				//if(true || obConfig['atrybutyCeny'])
				changeCenaAtryb(atr, $(this).val());
			}					
		},
		onOpen: function (inst) 
		{
			selectboxheightfix($(this));
		},
		onClose: function (inst) 
		{

		}
	});
	
	$('span.select.selectIkona select, #lang').each(function(key, val)
	{
		var bg = $(this).children("option:selected").css("background-image");
		$(this).parent().find('.sbSelector').css('background-image', bg);
	});

	$(document).click(function()
	{
		$('span.select select').selectbox('close');
		$('#producent').selectbox('close');
		$('#lang').selectbox('close');
	});
	
	$('.sbHolder').click(function(event)
	{
		event.stopPropagation();
	});
});