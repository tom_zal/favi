var geocoder;
var map;
var marker;

var latitude = 50.02120009999999;
var longitude = 21.974877099999958;
   
function initialize()
{
	latitude = $('#latitude').text();
	longitude = $('#longitude').text();
	
	var latlng = new google.maps.LatLng(latitude, longitude);
	var options = 
	{
		zoom: 16,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.MAP // SATELLITE
	};
	
	map = new google.maps.Map(document.getElementById("map_canvas"), options);
	
	geocoder = new google.maps.Geocoder();
	
	marker = new google.maps.Marker(
	{
		map: map,
		draggable: true
	});

	updateLocationFromMarker(latitude, longitude);	
	getAddressFromMarker(marker);
	
	//if(false)
	google.maps.event.addListener(marker, 'drag', function()
	{
		getAddressFromMarker(marker);
	});
	
	//if(false)
	$("#address").autocomplete(
	{
		//This it uses the geocoder to fetch address values
		source: function(request, response)
		{
			geocoder.geocode( {'address': request.term }, function(results, status)
			{
				//console.log(results);
				response($.map(results, function(item)
				{
					var temp = 
					{
						label: item.formatted_address,
						value: item.formatted_address,
						latitude: item.geometry.location.lat(),
						longitude: item.geometry.location.lng()
					}
					return temp;
				}));
			})
		},
		//Thisit is executed upon selection of an address
		select: function(event, ui)
		{
			updateLocationFromMarker(ui.item.latitude, ui.item.longitude);
		}
	});
}

function getAddressFromMarker(marker)
{
	geocoder.geocode({'latLng': marker.getPosition()}, function(results, status)
	{
		if(status == google.maps.GeocoderStatus.OK)
		{
			if(results[0])
			{
				$('#address').val(results[0].formatted_address);
				$('#latitude').text(marker.getPosition().lat());
				$('#longitude').text(marker.getPosition().lng());
			}
		}
	});
}
function updateLocationFromMarker(latitude, longitude)
{
	$("#latitude").text(latitude);
	$("#longitude").text(longitude);
	var location = new google.maps.LatLng(latitude, longitude);
	marker.setPosition(location);
	map.setCenter(location);
	//return location;
}

$(document).ready(function()
{
	initialize();
});