function odswiezProducenci(producent)
{
	$(producent).html($('#producenciAll').html());
	$(producent).selectbox('detach');
	$(producent).selectbox('attach');
	$('.sbHolder').click(function(event)
	{
		event.stopPropagation();
		//alert('click0');
	});
}
	
function getProducenci(fraza, selekt)
{
	$.ajax(
	{
		url: baseUrl + "/ajax/index/autocompleteproducent",
		type: "GET",
		data: "q=" + $(fraza).val(),
		success: function(data)
		{
			var xml = $.parseXML($.trim(data));
			var ile = $(xml).find('producent').length;
			//alert(ile);
			if(ile > 0)
			{
				$(selekt).html('');
				$(xml).find('producent').each(function()
				{
					var id = $(this).find('id').text();
					var name = $(this).find('nazwa').text();
					var sel = (id == producent) ? ' selected="selected"' : '';
					$(selekt).append('<option value="'+id+'"'+sel+'>'+name+'</option>');
				});
				$(selekt).selectbox('detach');
				$(selekt).selectbox('attach');
				$('.sbHolder').click(function(event)
				{
					event.stopPropagation();
					//alert('click0');
				});
			}
			//else $(selekt).html('<option value="0">Brak wynik�w</option>');
		},
		error: function(data)
		{

		}
	});
}