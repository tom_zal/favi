<?php if(true): ?>
$('input[name=uploadedfile]').uploadify(
{
	'uploader'  : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/uploadify.swf',
	'script'    : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/uploadify2.php',
	'cancelImg' : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/cancel.png',
	'folder'    : '<?php echo $this->baseUrl; ?>/public/<?php echo @$this->folder; ?>',
	'auto'      : true,
	'multi'		: false,
	'removeCompleted' : false,
	'buttonText' : 'Wybierz',
	//'buttonImg'   : '<?php echo $this->baseUrl; ?>/public/images/admin/przegladaj.png',
	'width' : 120,
	'height' : 34,
	'wmode'		: 'transparent',
	'fileExt'   : '<?php echo @strval($this->rozsz['name']); ?><?php echo @strval($this->rozsz2['name']); ?>',
	'fileDesc'    : '<?php echo @strval($this->rozsz['desc']); ?><?php echo @strval($this->rozsz2['desc']); ?>',
	//'sizeLimit'   : 2 * 1024 * 1024,
	'simUploadLimit' : 1,
	'queueSizeLimit' : 1,
	'onComplete'  : function(event, ID, fileObj, response, data) 
	{
		$('#file').val(response);
		//alert(response);
	},
	'onCancel'  : function(event, ID, fileObj, response, data) 
	{
		$('#file').val('');
		//alert(response);
	}
});

if(true)
$('#zakladkaGaleria #box_display1 #filesToUpload').uploadify(
{
	'uploader'  : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/uploadify.swf',
	'script'    : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/uploadify2.php',
	'cancelImg' : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/cancel.png',
	'folder'    : '<?php echo $this->baseUrl; ?>/public/admin/zdjecia',
	'auto'      : true,
	'multi'		: true,
	'removeCompleted' : false,
	'buttonText' : 'Wybierz',
	//'buttonImg'   : '<?php echo $this->baseUrl; ?>/public/images/admin/przegladaj.png',
	'width' : 120,
	'height' : 34,
	'wmode'		: 'transparent',
	'fileExt'   : '*.jpg;*.jpeg;*.pjpg;*.pjpeg;*.png;*.gif',
	'fileDesc'    : 'Zdjęcia (*.jpg; *.png; *.gif)',
	'sizeLimit'   : 4 * 1024 * 1024,
	'simUploadLimit' : 1,
	//'queueSizeLimit' : 1,
	'onSelect'  : function(event, ID, fileObj, data)
	{
		$("#filesToUploadQueue").sortable({ opacity: 0.6, cursor: 'move'});
	},
	'onComplete'  : function(event, ID, fileObj, response, data) 
	{
		var img = '<img src="<?php echo $this->baseUrl;?>/public/admin/zdjecia/' + response + '" alt="" />';
		var zdj = '<div class="file"><span class="thumbnail">'+img+'<span>'+img+'<br/></span></span></div>';
		$('#filesToUpload'+ID).prepend(zdj);
		var opis = '<div class="opis"><br/>Opis <input type="text" name="images[]" value="" /></div>';
		$('#filesToUpload'+ID).append(opis);
		$('#filesToUpload'+ID).append('<input type="hidden" name="img[]" value="'+response+'" />');
		$('#filesToUpload'+ID).append('<span class="ico_drag"></span>');
	},
	'onCancel'  : function(event, ID, fileObj, response, data) 
	{
		//alert(response);
	}
});

<?php if($this->obConfig->atrybutyZdjecia): ?>
$('#atrybuty input[type=file]').each(function()
{
	var id = $(this).attr('id');
	var name = $(this).prev().attr('name');
	var id0 = $(this).prev().attr('id').replace(id+'_ID_', '');
	$(this).uploadify(
	{
		'uploader'  : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/uploadify.swf',
		'script'    : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/uploadify2.php',
		'cancelImg' : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/cancel.png',
		'folder'    : '<?php echo $this->baseUrl; ?>/public/admin/zdjecia',
		'auto'      : true,
		'multi'		: false,
		'removeCompleted' : false,
		//'buttonText' : 'Wybierz',
		'buttonImg'   : '<?php echo $this->baseUrl; ?>/public/images/admin/agile/przegladaj-a.png',
		'width' : 93,
		'height' : 22,
		'wmode'		: 'transparent',
		'fileExt'   : '*.jpg;*.jpeg;*.pjpg;*.pjpeg;*.png;*.gif',
		'fileDesc'    : 'Zdjęcia (*.jpg; *.png; *.gif)',
		'sizeLimit'   : 4 * 1024 * 1024,
		'simUploadLimit' : 1,
		//'queueSizeLimit' : 1,
		'onInit'   : function(instance)
		{
			//alert($('#'+id+'Queue').size());
		},
		'onSelect'  : function(event, ID, fileObj, data)
		{
			//alert('#'+id+''+ID);
			//$('#'+id+'Queue').remove();
			$('#'+id+'_ID_'+id0).remove();
			$('#'+id+id0).remove();
		},
		'onComplete'  : function(event, ID, fileObj, response, data) 
		{
			var path = '<?php echo $this->baseUrl;?>/public/admin/zdjecia';
			$('#'+id+''+ID).parent().parent().find('.img > img').attr('src', path + '/' + response);
			$('#'+id+''+ID).parent().parent().find('.thumbnail img').attr('src', path + '/' + response);
			$('#'+id+''+ID).append('<input type="hidden" id="'+id+'_ID_'+ID+'" name="'+name+'" value="'+response+'" />');
			//$('#'+id+'_ID_'+ID).val(response);
		},
		'onCancel'  : function(event, ID, fileObj, response, data) 
		{
			var path = '<?php echo $this->baseUrl;?>/public/admin/zdjecia';
			if(ID == id0)
			{
				$('#'+id+''+ID).parent().find('.img > img').attr('src', path + '/brak.jpg');
				$('#'+id+''+ID).parent().find('.thumbnail img').attr('src', path + '/brak.jpg');
			}
			else
			{
				$('#'+id+''+ID).parent().parent().find('.img > img').attr('src', path + '/brak.jpg');
				$('#'+id+''+ID).parent().parent().find('.thumbnail img').attr('src', path + '/brak.jpg');
			}
			$('#'+id+'_ID_'+ID).val('');
		}
	});
});
<?php endif; ?>

<?php if($this->obConfig->allegroSzablony): ?>
$('#szablonnowy').uploadify(
{
	'uploader'  : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/uploadify.swf',
	'script'    : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/uploadifyallegro.php',
	'cancelImg' : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/cancel.png',
	'folder'    : '<?php echo $this->baseUrl; ?>/public/admin/szablony/',
	'auto'      : true,
	'multi'		: false,
	'removeCompleted' : false,
	'buttonText' : 'Wybierz',
	'fileExt'   : '*.htm;*.html',
	'fileDesc'    : 'Pliki HTML (.htm, .html)',
	'onSelectOnce' : function(event, ID, fileObj, response, data) 
	{
		alert('Po poprawnym załadowaniu szablonu poczekaj na przeładowanie strony');
	},
	'onComplete'  : function(event, ID, fileObj, response, data) 
	{
		$('#szablonfile').val(response);
		$('#jQuery_zapisz_btn').trigger('click');
	}
});
$('#imgszablon').uploadify(
{
	'uploader'  : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/uploadify.swf',
	'script'    : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/uploadifyallegro.php',
	'cancelImg' : '<?php echo $this->baseUrl; ?>/public/scripts/jquery.uploadify/cancel.png',
	'folder'    : '<?php echo $this->baseUrl; ?>/public/admin/szablony/<?php echo @$this->file.'_pliki';?>',
	'auto'      : true,
	'multi'		: true,
	'removeCompleted' : true,
	'buttonText' : 'Wybierz',
	'fileExt'   : '*.jpg;*.jpeg;*.png;*.gif;*.css',
	'fileDesc'    : 'Pliki JPG, PNG, GIF, CSS (.jpg, .jpeg, .png, .gif, .css)',
	'onSelectOnce' : function(event, ID, fileObj, response, data) 
	{
		alert('Po poprawnym załadowaniu plików poczekaj na przeładowanie strony');
	},
	'onAllComplete'  : function(event, ID, fileObj, response, data) 
	{
		window.location.reload();
	}
});
<?php endif; ?>

<?php endif; ?>