/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function Oblicz(ten)
{
	var cena = $(ten).attr('id');
	//alert(cena);
	cena_netto = 0;
	cena_netto_hurt = 0;
	cena_netto_promocja = 0;
	cena_netto_promocja_hurt = 0;
	cena_netto_prom_net = 0;
	cena_netto_prom_net_hurt = 0;
	cena_netto_wyprzedaz = 0;

	if(document.forma.cena_netto != undefined)
	if(document.forma.cena_netto.value != undefined)
	cena_netto = document.forma.cena_netto.value.replace(",",".");

	if(true && document.forma.cena_netto_hurt != undefined)
	cena_netto_hurt = document.forma.cena_netto_hurt.value.replace(",",".");
	else cena_netto_hurt = cena_netto;

	if(document.forma.cena_promocji_n != undefined)
	cena_netto_promocja = document.forma.cena_promocji_n.value.replace(",",".");

	if(true && document.forma.cena_promocji_n_hurt != undefined)
	cena_netto_promocja_hurt = document.forma.cena_promocji_n_hurt.value.replace(",",".");
	else cena_netto_promocja_hurt = cena_netto_promocja;

	if(document.forma.cena_prom_net_n != undefined)
	cena_netto_prom_net = document.forma.cena_prom_net_n.value.replace(",",".");

	if(true && document.forma.cena_prom_net_hurt_n != undefined)
	cena_netto_prom_net_hurt = document.forma.cena_prom_net_hurt_n.value.replace(",",".");
	else cena_netto_prom_net_hurt = cena_netto_prom_net;

	if(document.forma.cena_wyprzedaz_n != undefined)
	cena_netto_wyprzedaz = document.forma.cena_wyprzedaz_n.value.replace(",",".");

	vat = document.forma.vat.value;
	if(isNaN(vat)) vat = 0;
	cena_netto = 1*cena_netto;
	cena_netto_hurt = 1*cena_netto_hurt;
	cena_netto_promocja = 1*cena_netto_promocja;   
	cena_netto_promocja_hurt = 1*cena_netto_promocja_hurt;
	cena_netto_prom_net = 1*cena_netto_prom_net;   
	cena_netto_prom_net_hurt = 1*cena_netto_prom_net_hurt;
	cena_netto_wyprzedaz = 1*cena_netto_wyprzedaz;
	vat = 1*vat;
	cena_brutto = Math.round((cena_netto+(cena_netto*vat/100))*100)/100;
	cena_brutto_hurt = Math.round((cena_netto_hurt+(cena_netto_hurt*vat/100))*100)/100;
	cena_promocji_b = Math.round((cena_netto_promocja+(cena_netto_promocja*vat/100))*100)/100;  
	cena_promocji_b_hurt = Math.round((cena_netto_promocja_hurt+(cena_netto_promocja_hurt*vat/100))*100)/100;
	cena_prom_net_b = Math.round((cena_netto_prom_net+(cena_netto_prom_net*vat/100))*100)/100;  
	cena_prom_net_hurt_b = Math.round((cena_netto_prom_net_hurt+(cena_netto_prom_net_hurt*vat/100))*100)/100;
	cena_wyprzedaz_b = Math.round((cena_netto_wyprzedaz+(cena_netto_wyprzedaz*vat/100))*100)/100;

	if(cena != 'cena_netto_hurt' && document.forma.cena_netto_hurt != undefined)
	document.forma.cena_netto_hurt.value = cena_netto_hurt;
	if(cena != 'cena_brutto' && document.forma.cena_brutto != undefined)
	document.forma.cena_brutto.value = cena_brutto;
	if(cena != 'cena_brutto_hurt' && document.forma.cena_brutto_hurt != undefined)
	document.forma.cena_brutto_hurt.value = cena_brutto_hurt;
	if(cena != 'cena_promocji_n_hurt' && document.forma.cena_promocji_n_hurt != undefined)
	document.forma.cena_promocji_n_hurt.value = cena_netto_promocja_hurt;
	if(cena != 'cena_promocji_b' && document.forma.cena_promocji_b != undefined)
	document.forma.cena_promocji_b.value = cena_promocji_b;  
	if(cena != 'cena_promocji_b_hurt' && document.forma.cena_promocji_b_hurt != undefined)
	document.forma.cena_promocji_b_hurt.value = cena_promocji_b_hurt;
	if(cena != 'cena_prom_net_hurt_n' && document.forma.cena_prom_net_hurt_n != undefined)
	document.forma.cena_prom_net_hurt_n.value = cena_netto_prom_net_hurt;
	if(cena != 'cena_prom_net_b' && document.forma.cena_prom_net_b != undefined)
	document.forma.cena_prom_net_b.value = cena_prom_net_b;  
	if(cena != 'cena_prom_net_hurt_b' && document.forma.cena_prom_net_hurt_b != undefined)
	document.forma.cena_prom_net_hurt_b.value = cena_prom_net_hurt_b;
	if(cena != 'cena_wyprzedaz_b' && document.forma.cena_wyprzedaz_b != undefined)
	document.forma.cena_wyprzedaz_b.value = cena_wyprzedaz_b;
}

function nettohurtowa(wartosc,cel)
{
    wartosc = wartosc.replace(',','.');
	vat = document.getElementById('vat').value;
	if(isNaN(vat)) vat = 0;
	przelicznik = 0;
	if(vat==23) przelicznik = 18.70;
    if(vat==22) przelicznik = 18.03;
	if(vat==8) przelicznik = 7.41;
    if(vat==7) przelicznik = 6.54;
	if(vat==5) przelicznik = 4.76;
    if(vat==3) przelicznik = 2.91;
    if(vat==0) przelicznik = 0;
	przelicznik = przelicznik/100;
    document.getElementById(cel).value =  Math.round(((wartosc*1-(wartosc*przelicznik))*100))/100;
}

function obliczbrutto(wartosc,cel)
{
    wartosc = wartosc.replace(',','.');
    vat = document.getElementById('vat').value;
    wartosc = 1*wartosc;
    vat = 1*vat;
    document.getElementById(cel).value =  Math.round((wartosc+(wartosc*vat/100))*100)/100;
}

function cenyoblicz(wartosc,nazwa)
{
	vat = document.forma.vat.value;
	if(isNaN(vat)) vat = 0;
	cel = nazwa;
	
		 if(nazwa.indexOf('netto') > 0) cel = nazwa.replace('netto', 'brutto');
	else if(nazwa.indexOf('brutto') > 0) cel = nazwa.replace('brutto', 'netto');
	else if(nazwa.indexOf('_n') > 0) cel = nazwa.replace('_n', '_b');
	else if(nazwa.indexOf('_b') > 0) cel = nazwa.replace('_b', '_n');
	
	if(nazwa.indexOf('netto') > 0 || nazwa.indexOf('_n') > 0)
	{
		wartosc = 1 * wartosc.replace(",",".");
		document.getElementById(cel).value = Math.round((wartosc+(wartosc*vat/100))*100)/100;
	}
	if(nazwa.indexOf('brutto') > 0 || nazwa.indexOf('_b') > 0)
	{
		nettohurtowa(wartosc, cel);
	}
}

function obliczNettoFromZakupMarza()
{
	cena_zakupu = document.getElementById('cena_zakupu').value;
    cena_zakupu = cena_zakupu.replace(',','.');
	marza = document.getElementById('marza').value;
	marza = 1*marza/100;
    document.getElementById('cena_netto').value = Math.round((cena_zakupu*1+cena_zakupu*marza)*100)/100;
	
	obliczBruttoFromNettoVat();
}
function obliczBruttoFromNettoVatProdukt(typ, ten)
{
	var prodID = parseInt($(ten).attr('id').replace(typ, ''));
	var netto = parseFloat($(ten).val().replace(',','.'));
	var vat = parseFloat($('#vat' + prodID).val().replace(',','.'));
	var brutto = Math.round(netto * (1 + vat / 100) * 100) / 100;
    $('#' + typ.replace('netto', 'brutto') + prodID).val(brutto);
}
function obliczNettoFromBruttoVatProdukt(typ, ten)
{
	var prodID = parseInt($(ten).attr('id').replace(typ, ''));
	var brutto = parseFloat($(ten).val().replace(',','.'));
	var vat = parseFloat($('#vat' + prodID).val().replace(',','.'));
	var netto = Math.round(brutto / (1 + vat / 100) * 100) / 100;
    $('#' + typ.replace('brutto', 'netto') + prodID).val(netto);
}
function obliczBruttoFromNettoVat()
{
	cena_netto = document.getElementById('cena_netto').value;
    cena_netto = cena_netto.replace(',','.');
	vat = document.getElementById('vat').value;
	vat = 1*vat/100;
    document.getElementById('cena_brutto').value = Math.round((cena_netto*1+cena_netto*vat)*100)/100;
	
	obliczMarzaFromZakupNetto();
}
function obliczNettoFromBruttoVat()
{
	cena_brutto = document.getElementById('cena_brutto').value;
    cena_brutto = cena_brutto.replace(',','.');
	vat = document.getElementById('vat').value;
	vat = 1*vat/100;
    document.getElementById('cena_netto').value = Math.round((cena_brutto/(1 + vat))*100)/100;
	
	obliczMarzaFromZakupNetto();
}
function obliczZakupFromNettoMarza()
{
	cena_netto = document.getElementById('cena_netto').value;
    cena_netto = cena_netto.replace(',','.');
	marza = document.getElementById('marza').value;
	marza = 1*marza;
    document.getElementById('cena_zakupu').value = Math.round((cena_netto/(1 + marza))*100)/100;
}
function obliczMarzaFromZakupNetto()
{
	cena_zakupu = document.getElementById('cena_zakupu').value;
    cena_zakupu = cena_zakupu.replace(',','.');
	cena_netto = document.getElementById('cena_netto').value;
    cena_netto = cena_netto.replace(',','.');
    document.getElementById('marza').value = Math.round((cena_netto/cena_zakupu - 1)*100);
}
