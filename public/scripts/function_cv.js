$('#fileFile').uploadify(
{
	'uploader'  : baseUrl + '/public/scripts/jquery.uploadify/uploadify.swf',
	'script'    : baseUrl + '/public/scripts/jquery.uploadify/uploadify.php',
	'cancelImg' : baseUrl + '/public/scripts/jquery.uploadify/cancel.png',
	'folder'    : baseUrl + '/public/admin/cv',
	'auto'      : true,
	'multi'		: false,
	'removeCompleted' : false,
	'hideButton'	: true,
	'width'		: 100,
	'height'	: 160,
	'wmode'		: 'transparent',
	'fileExt'   : '*.jpg;*.jpeg',
	'fileDesc'    : 'Pliki JPG (.jpg, .jpeg)',
	'onCancel'  : function(event, ID, fileObj, data)
	{
		$('#file').replaceWith('<div id="file2" class="file">Dodaj zdj�cie</div>');
		$('#fileCVName').val('');
		$('#fileUsun').remove();
	},
	'onComplete'  : function(event, ID, fileObj, response, data) 
	{
		//data.speed in kbs, response from php
		$('.file').replaceWith('<div id="file" class="file"><div id="fileInner"><span></span><img id="fileImg" src="'+baseUrl+'/public/admin/cv/_' + response + '" alt="" /></div></div>');
		$('#fileCVName').val(response);
		$('#fileUsun').remove();
		//window.alert(response);
	}
});