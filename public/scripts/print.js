function printZam(i, status)
{
	$('.table_order td').css('padding', '0px 5px');
	var zdj = $('#drukujZdjecia' + i).attr('checked');
	if(!zdj) $('.zdj').remove();
	printAdmin();
	drukuj();
}

function printZams()
{
	$('.prod, .prods').removeClass('dn').show();
	printAdmin();	
	$('.prod td').css('padding', '0px 2px');
	drukuj();
}

function drukuj()
{
	window.print();
	window.onfocus = function(){ location.reload(true); }
	location.reload(true);
}

function printAdmin(now)
{
	$('body').css('background', 'none');
	$('.page').css('width', 690);
	$('.page').css('padding', 0);
	$('.content').css('width', '100%');
	$('.content').css('padding', 0);
	$('.content').css('border', 0);
	$('.box_form').css('padding', 0);
	$('.container').css('padding', 0);
	$('.all_container').css('padding', 0);
	$('.top_container').remove();
	$('.navi_box').remove();
	$('.box_title').remove();
	$('.box_main_title').remove();
	$('.box_link_navi').remove();
	$('.dn, .dni').remove();
	$('.drukuj').remove();
	$('.noprint').remove();
	$('.error_info').remove();
	$('.box_search').remove();
	$('.foto_box th').css('padding', '0px 5px');
	$('.foto_box td').css('padding', '0px 5px');
	$('.required').removeClass('required');
	$('.info_labels').remove();	
	$('.button_form').remove();
	$('#stronicowanie, .box_pager').remove();
	$('#ileNaStrone').remove();
	$('#zadania, .zadamia').remove();
	$('.right_menu').remove();
	if(now) drukuj();
}

function printFV()
{
	printAdmin();
	drukuj();
}

function printOferta()
{
	printAdmin();
	$('.box_search2').remove();
	drukuj();
}

function printZamKoszyk()
{
	//$('body').css('width', 700);
	$('body').css('background', 'none');
	$('body').css('font-size', '14px');
	$('#topAll').remove();
	$('#topLeft').css('background', 'none');
	$('#topRight').css('background', 'none');
	$('#body').css('width', 900);
	$('#bodyMain').css('width', 900);
	$('#body').css('background', 'none');	
	$('#main').css('background', 'none');
	$('#mainBody').css('width', '100%');
	//$('#bodyMain').css('boxShadow ', '0px 0px 0px #fff');
	$('.box_prod').removeClass('box_prod');
	$('h3.title3').css('font-size', '20px');
	$('.td_label').css('width', '150px');
	$('.td_label').css('font-size', '14px');
	$('.td_label').removeClass('td_label');
	$('.tabela_koszyk').css('font-size', '14px');
	$('.tabela_koszyk_oplaty').css('font-size', '14px');
	$('.tabela_koszyk_oplaty th').css('font-size', '16px');
	$('.tabela_koszyk_logowanie th').css('font-size', '16px');
	$('#bodyLeft').remove();
	$('#bodyRight').remove();
	$('#stopkaAll').remove();
	$('.zakladka').remove();
	$('#temp').remove();
	$('.box_lang').remove();
	$('#koszykTab').remove();
	$('#logo').remove();
	$('#logoNapis').remove();
	$('#menuTop').remove();
	$('#mainLeft').remove();
	$('#mainRight').remove();
	$('#menu').remove();	
	$('#baner').remove();
	$('#sciezkaAll').remove();
	$('#naglowek').remove();
	$('#podstronaNagl').remove();
	$('#podstronaTekst').remove();
	$('#sciezka').remove();
	$('.k_uwaga').remove();
	$('.noprint').remove();
	$('.button').remove();
	$('.edycja_danych_przycisk').remove();
	//$('.ui-spinner').css('float', 'none');
	$('.ui-spinner-input').removeClass('input_ilosc');
	$('.ui-spinner-input').css('margin', '0px');
	$('.ui-spinner-button').remove();
	var zdj = $('#zdjecia').attr('checked');
	if(!zdj) $('.zdj').remove();
	drukuj();
}