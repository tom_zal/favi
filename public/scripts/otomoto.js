$(document).ready(function()
{
	//var $pojazdTyp = $('#s_pojazd_typ');
	var $pojazdTyp = $('#i_pojazd_typ');
	var nazwa_typu_pojazdu = $pojazdTyp.val() == undefined?'':$pojazdTyp.val().toUpperCase();

	var tab_param = null;
	var $typStrony = $('#typ-strony');
	var $pojazdMarka = $('#s_pojazd_marka');
	var $pojazdModel = $('#s_pojazd_model');
	var $pojazdModelNazwaWl = $('#i_pojazd_model_nazwa_wl');
	var $pojazdWersja = $('#s_pojazd_wersja');
	var $pojazdTypNadwozia = $('#s_pojazd_typ_nadwozia');
	var $pojazdStanTechniczny = $('#s_pojazd_stan_techniczny');
	var $pojazdPrzebieg = $('#i_pojazd_przebieg');
	var $pojazdPrzegladMiesiac = $('#s_pojazd_przeglad_miesiac');
	var $pojazdPrzegladRok = $('#s_pojazd_przeglad_rok');
	var $pojazdUbezpOcMiesiac = $('#s_pojazd_ubezp_oc_miesiac');
	var $pojazdUbezpOcRok = $('#s_pojazd_ubezp_oc_rok');
	var $pojazdKrajPochodzenia = $('#s_pojazd_kraj_pochodzenia');
	var $pojazdKrajAktRej = $('#s_pojazd_kraj_akt_rej');
	var $pojazdStatPojSprow = $('#s_pojazd_stat_poj_sprow');
	var $pojazdPierwRejMiesiac = $('#s_pojazd_pierw_rej_miesiac');
	var $pojazdPierwRejRok = $('#s_pojazd_pierw_rej_rok');
	var $pojazdUszkodzony = $('#s_pojazd_uszkodzony');
	var $pojazdDodOpisModel = $('#i_pojazd_dod_opis_model');
	var $pojazdLiczbaDrzwi = $('#s_pojazd_liczba_drzwi');
	var $pojazdPojSkokowa = $('#s_pojazd_poj_skokowa');
	var $pojazdLadowalnosc = $('#i_pojazd_ladowalnosc');
	var $pojazdRodzajNapedu = $('#s_pojazd_rodzaj_napedu');
	var $pojazdLiczbaOsi = $('#i_pojazd_liczba_osi');
	var $pojazdPublWTablicaPl = $('#ch_pojazd_publ_w_tablica_pl');
	var $pojazdDodInfo = $('#s_pojazd_dod_info');
	var $pojDodInfoNoAccident = $('#ch_poj_dod_info_no_accident');
	var $pojDodInfoAsoService = $('#ch_poj_dod_info_aso_service');
	var $pojDodInfoGaraged = $('#ch_poj_dod_info_garaged');
	var $pojDodInfoOldtimer = $('#ch_poj_dod_info_oldtimer');
	var $pojDodInfoFirstOwner = $('#ch_poj_dod_info_first_owner');
	var zawartosc_s_dod_info_ajax = null;
	var $pojazdNDodInfo = $('#nastepna-dod-informacja');
	var $pojazdDodWyposaz = $('#s_pojazd_dod_wyposaz');
	var zawartosc_s_dod_wyposaz_ajax = null;
	var $pojazdNDodWyposaz = $('#nastepne-dod-wyposazenie');
	var licznik_dod_info = 1;
	var maks_licznik_dod_info = 20;
	var $s_pojazd_dod_info = $('#s_pojazd_dod_info select');
	var $s_pojazd_dod_info_html = $s_pojazd_dod_info.html();
	var licznik_dod_wyposazenie = 1;
	var maks_licznik_dod_wyposazenie = 50;
	var $s_pojazd_dod_wyposaz = $('#s_pojazd_dod_wyposaz select');
	var $s_pojazd_dod_wyposaz_html = $s_pojazd_dod_wyposaz.html();
	var blokujMarkaModel = false;
	var $i_pojazd_cena = $('#i_pojazd_cena');
	var $s_pojazd_typ_ceny = $('#s_pojazd_typ_ceny');
	var $s_pojazd_waluta = $('#s_pojazd_waluta');
	var $i_pojazd_kwota_odstepnego = $('#i_pojazd_kwota_odstepnego');
	var $i_pojazd_wysokosc_raty = $('#i_pojazd_wysokosc_raty');
	var $s_pojazd_waluta_odstepnego = $('#s_pojazd_waluta_odstepnego');
	var $i_pojazd_ilosc_pozos_rat = $('#i_pojazd_ilosc_pozos_rat');
	var $i_pojazd_wartosc_wykupu = $('#i_pojazd_wartosc_wykupu');
	var $ch_pojazd_w_leasingu = $('#ch_pojazd_w_leasingu');
	var $ch_widocznyOtotmoto = $('#widoczny_otomoto');
	var zaznWidocznyOtomoto = $ch_widocznyOtotmoto.is(':checked'); 
	//czy zaznaczone pole widoczny otomoto na początku (zaraz po renderowaniu strony i wyświetleniu)
	var $informacje_otomoto = $('#INFORACJE-OTOMOTO');
	$informacje_otomoto.css('visibility', 'hidden');

	var pobierzMarke = function()
	{
		nazwa_typu_pojazdu = $pojazdTyp.val().toUpperCase();
		if(nazwa_typu_pojazdu != '-1')
		{
			$.post(baseUrl+'/ajax/otomoto/pobierzmarke/', 
			{
				'typ_pojazdu': nazwa_typu_pojazdu
			}, 
			function(data)
			{
				$pojazdMarka.html(data);
			});
		}
	}
	var pobierzModel = function()
	{
		nazwa_typu_pojazdu = $pojazdTyp.val().toUpperCase();
		if(nazwa_typu_pojazdu != 'TRUCK')
		{
			$.post(baseUrl+'/ajax/otomoto/pobierzmodel/', 
			{
				'typ_pojazdu': nazwa_typu_pojazdu, 
				'marka_id': $pojazdMarka.val()
			}, 
			function(data)
			{
				$pojazdModel.html(data);
				$.post(baseUrl+'/ajax/otomoto/pobierzwersje/', 
				{
					'typ_pojazdu': nazwa_typu_pojazdu, 
					'wersja_id': $pojazdModel.val()
				}, 
				function(data)
				{
					$pojazdWersja.html(data);
				});
			});
		}
		else
		{
			$pojazdModel.html('<option value="-1">niedostępny</option>');
			$pojazdWersja.html('<option value="-1">niedostępna</option>');
		}
	}
	var pobierzWersje = function()
	{
		nazwa_typu_pojazdu = $pojazdTyp.val().toUpperCase();
		if(nazwa_typu_pojazdu == 'CAR')
		{
			$.post(baseUrl+'/ajax/otomoto/pobierzwersje/', 
			{
				'typ_pojazdu': nazwa_typu_pojazdu, 
				'model_id': $pojazdMarka.val(), 
				'wersja_id': $pojazdModel.val()
			}, 
			function(data)
			{
				$pojazdWersja.html(data);
			});
		}
		else
		{
			$pojazdWersja.html('<option value="-1">niedostępna</option>');
		}
	}
	
	var pusteOption = '<option value="-1">------------------------</option>';
	var numWybrKatAllegro = $('#num-wybr-kat-allegro');

	var przepiszKategorieAllegro = function(numerKategorii)
	{
		if(numerKategorii != '-1')
		{
			numWybrKatAllegro.val(numerKategorii);
		}
		else
		{
			numWybrKatAllegro.val('');
		}
	}
	var pobierzKategorieAllegroI = function()
	{
		nazwa_typu_pojazdu = $pojazdTyp.val().toUpperCase();
		if(nazwa_typu_pojazdu == 'MOTORBIKE') //wybieramy spośród car, truck i motorbike
		{
			nazwa_typu_pojazdu = "MOTOCYKLE";
		}
		else
		{
			nazwa_typu_pojazdu = "SAMOCHODY";
		}
		$kategoria_allegro_I.html(pusteOption);
		$.post(baseUrl+'/ajax/otomoto/pobkatallegroi/', 
		{
			'typ_pojazdu': nazwa_typu_pojazdu
		}, 
		function(data)
		{
			$kategoria_allegro_I.html(data);
		});
	}
	var pobierzKategorieAllegroII = function()
	{
		nazwa_typu_pojazdu = $pojazdTyp.val().toUpperCase();
		if(nazwa_typu_pojazdu == 'MOTORBIKE')
		{  //wybieramy z pośród car, truck i motorbike
			nazwa_typu_pojazdu = "MOTOCYKLE";
		}
		else
		{
			nazwa_typu_pojazdu = "SAMOCHODY";
		}
		$kategoria_allegro_II.html(pusteOption);
		$.post(baseUrl+'/ajax/otomoto/pobkatallegroii/', 
		{
			'typ_pojazdu': nazwa_typu_pojazdu, 
			'kategoria_allegro': $kategoria_allegro_I.val()
		}, 
		function(data)
		{
			$kategoria_allegro_II.html(data);
		});
		$kategoria_allegro_III.html(pusteOption);
		$kategoria_allegro_IV.html(pusteOption);
	}
	var pobierzKategorieAllegroIII = function()
	{
		nazwa_typu_pojazdu = $pojazdTyp.val().toUpperCase();
		if(nazwa_typu_pojazdu == 'MOTORBIKE')
		{  //wybieramy z pośród car, truck i motorbike
			nazwa_typu_pojazdu = "MOTOCYKLE";
		}
		else
		{
			nazwa_typu_pojazdu = "SAMOCHODY";
		}
		$kategoria_allegro_III.html(pusteOption);
		$.post(baseUrl+'/ajax/otomoto/pobkatallegroiii/',
		{
			'typ_pojazdu': nazwa_typu_pojazdu, 
			'kategoria_allegro': $kategoria_allegro_II.val()
		}, 
		function(data)
		{
			$kategoria_allegro_III.html(data);
		});
		$kategoria_allegro_IV.html(pusteOption);
	}
	var pobierzKategorieAllegroIV = function()
	{
		nazwa_typu_pojazdu = $pojazdTyp.val().toUpperCase();
		if(nazwa_typu_pojazdu == 'MOTORBIKE')
		{  //wybieramy z pośród car, truck i motorbike
			nazwa_typu_pojazdu = "MOTOCYKLE";
		}
		else
		{
			nazwa_typu_pojazdu = "SAMOCHODY";
		}
		$kategoria_allegro_IV.html(pusteOption);
		$.post(baseUrl+'/ajax/otomoto/pobkatallegroiv/',
		{
			'typ_pojazdu': nazwa_typu_pojazdu, 
			'kategoria_allegro': $kategoria_allegro_III.val()
		},
		function(data)
		{
			$kategoria_allegro_IV.html(data);
		});
	}


	$pojazdTyp.change(function()
	{
		if(blokujMarkaModel != true)
		{
			pobierzMarke();
		}
	});
	$pojazdMarka.change(function()
	{
		if(blokujMarkaModel != true)
		{
			pobierzModel();
		}
	});
	$pojazdModel.change(function()
	{
		if(blokujMarkaModel != true)
		{
			pobierzWersje();
		}
	});	

	//kategorie allegro - początek
	var $kategoria_allegro_I = $('#s_pojazd_kategoria_allegro');
	var $kategoria_allegro_II = $('#s_pojazd_kategoria_allegro_II');
	var $kategoria_allegro_III = $('#s_pojazd_kategoria_allegro_III');
	var $kategoria_allegro_IV = $('#s_pojazd_kategoria_allegro_IV');
	$kategoria_allegro_I.change(function()
	{
		if($(this).val() != '-1')
		{
			pobierzKategorieAllegroII();
			przepiszKategorieAllegro($kategoria_allegro_I.val());
		}
	});
	$kategoria_allegro_II.change(function()
	{
		if($(this).val() != '-1')
		{
			pobierzKategorieAllegroIII();
			przepiszKategorieAllegro($kategoria_allegro_II.val());
		}
	});
	$kategoria_allegro_III.change(function()
	{
		if($(this).val() != '-1')
		{
			pobierzKategorieAllegroIV();
			przepiszKategorieAllegro($kategoria_allegro_III.val());
		}
	});
	$kategoria_allegro_IV.change(function()
	{
		if($(this).val() != '-1')
		przepiszKategorieAllegro($kategoria_allegro_IV.val());
	});
	//kategorie allegro - koniec

	
	//blokowanie marki i modelu jeżeli, ogłoszenie jest dodane do otomoto - początek
	if($typStrony.val() == 'edycja') 
	{
		if(zaznWidocznyOtomoto == true)
		{
			var pierwotnaWartoscMarka = $pojazdMarka.val();
			var pierwotnaWartoscModel = $pojazdModel.val();
			$pojazdMarka.click(function()
			{
				blokujMarkaModel = true;
			});
			$pojazdModel.click(function()
			{
				blokujMarkaModel = true;
			});
			$pojazdMarka.change(function()
			{
				$pojazdMarka.val(pierwotnaWartoscMarka);
			});
			$pojazdModel.change(function()
			{
				$pojazdModel.val(pierwotnaWartoscModel);
			});
		}
	}
	//blokowanie marki i modelu jeżeli ogłoszenie jest dodane do otomoto - koniec

	//obsługa pól leasingu
	var aktualizujLeasing = function()
	{
		if($ch_pojazd_w_leasingu.is(':checked'))
		{
			$i_pojazd_cena.attr('disabled', true);
			$s_pojazd_typ_ceny.attr('readolny', true);
			$s_pojazd_waluta.attr('disabled', true);

			$i_pojazd_kwota_odstepnego.attr('disabled', false);
			$s_pojazd_waluta_odstepnego.attr('disabled', false);
			$i_pojazd_wysokosc_raty.attr('disabled', false);
			$i_pojazd_ilosc_pozos_rat.attr('disabled', false);
			$i_pojazd_wartosc_wykupu.attr('disabled', false);
			$i_pojazd_kwota_odstepnego.parent().parent().parent().css('display', 'block');
		}
		else
		{
			$i_pojazd_cena.attr('disabled', false);
			$s_pojazd_typ_ceny.attr('readolny', false);
			$s_pojazd_waluta.attr('disabled', false);

			$i_pojazd_kwota_odstepnego.attr('disabled', true);
			$s_pojazd_waluta_odstepnego.attr('disabled', true);
			$i_pojazd_wysokosc_raty.attr('disabled', true);
			$i_pojazd_ilosc_pozos_rat.attr('disabled', true);
			$i_pojazd_wartosc_wykupu.attr('disabled', true);
			$i_pojazd_kwota_odstepnego.parent().parent().parent().css('display', 'none');
		}
	}
	$ch_pojazd_w_leasingu.change(aktualizujLeasing);
	aktualizujLeasing();

	var aktualizujStanTechniczny = function()
	{
		if($pojazdStanTechniczny.val() == 'new')
		{
			$pojazdPrzebieg.attr('disabled', true);
			$pojazdPrzegladMiesiac.attr('disabled', true);
			$pojazdPrzegladRok.attr('disabled', true);
			$pojazdUbezpOcMiesiac.attr('disabled', true);
			$pojazdUbezpOcRok.attr('disabled', true);
			$pojazdKrajPochodzenia.attr('disabled', true);
			$pojazdKrajAktRej.attr('disabled', true);
			$pojazdStatPojSprow.attr('disabled', true);
			$pojazdPierwRejMiesiac.attr('disabled', true);
			$pojazdPierwRejRok.attr('disabled', true);
			$pojDodInfoNoAccident.attr('disabled', true);
			$pojDodInfoAsoService.attr('disabled', true);
			$pojDodInfoGaraged.attr('disabled', true);
			$pojDodInfoOldtimer.attr('disabled', true);
			$pojDodInfoFirstOwner.attr('disabled', true);
		}
		else
		{
			$pojazdPrzebieg.attr('disabled', false);
			$pojazdPrzegladMiesiac.attr('disabled', false);
			$pojazdPrzegladRok.attr('disabled', false);
			$pojazdUbezpOcMiesiac.attr('disabled', false);
			$pojazdUbezpOcRok.attr('disabled', false);
			$pojazdKrajPochodzenia.attr('disabled', false);
			$pojazdKrajAktRej.attr('disabled', false);
			$pojazdStatPojSprow.attr('disabled', false);
			$pojazdPierwRejMiesiac.attr('disabled', false);
			$pojazdPierwRejRok.attr('disabled', false);
			$pojDodInfoNoAccident.attr('disabled', false);
			$pojDodInfoAsoService.attr('disabled', false);
			$pojDodInfoGaraged.attr('disabled', false);
			$pojDodInfoOldtimer.attr('disabled', false);
			$pojDodInfoFirstOwner.attr('disabled', false);
		}
	}
	$pojazdStanTechniczny.change(aktualizujStanTechniczny);
	aktualizujStanTechniczny();

	//pobieranie informacji dodatkowych o ogłoszeniu
	var $typ_podstrony = $('#TYP-PODSTRONY');
	var $i_id_ogloszenia = $('#ID-OGLOSZENIA');

	if($i_id_ogloszenia.val().length > 0) // if($typ_podstrony.val() == 'edycja')
	{
		$.post(baseUrl+'/ajax/otomoto/pobierzinfooaktywnosci/', 
		{
			'id_ogloszenia': $i_id_ogloszenia.val()
		}, 
		function(data)
		{
			//alert("Informacja diagnostyczna:\n\n"+data);
			if(data != 'false' && data != null)
			{
				$statusOgloszenia = $('#STATUS-OGLOSZENIA');
				$zmienStatusOgloszenia = $('#ZMIEN-STATUS-OGLOSZENIA');
				$przedluzOgloszenie = $('#PRZEDLUZ-OGLOSZENIE');
				$statusCzasWygOgloszenia = $('#CZAS-WYG-OGLOSZENIA');
				$statusPodgladOgloszenia = $('#PODGLAD-OGLOSZENIA');
				$informacje_otomoto.css('visibility', 'visible');
				var aktywujOgloszenie = function(event)
				{
					event.preventDefault();
					$.post(baseUrl+'/ajax/otomoto/zmienstatusogloszenia/', 
					{
						'akcja': 'aktywuj', 
						'id_ogloszenia': $i_id_ogloszenia.val()
					}, 
					function()
					{
						location.reload(true);
					});
				}
				var deaktywujOgloszenie = function(event)
				{
					event.preventDefault();
					$.post(baseUrl+'/ajax/otomoto/zmienstatusogloszenia/', 
					{
						'akcja': 'deaktywuj', 
						'id_ogloszenia': $i_id_ogloszenia.val()
					},
					function()
					{
						location.reload(true);
					});
				}
				var info_tab = data.split('|');

				$statusOgloszenia.html(info_tab[0]);
				if(info_tab[0] == 'aktywne')
				{
					var img = '<img src="'+baseUrl+'/public/images/admin/ikony/deaktywuj.png" alt="deaktywuj" />';
					$zmienStatusOgloszenia.html(img);
					$zmienStatusOgloszenia.attr('title', 'deaktywuj ogłoszenie');
					$zmienStatusOgloszenia.click(deaktywujOgloszenie);
					$przedluzOgloszenie.css('visibility', 'visible');
					$przedluzOgloszenie.click(aktywujOgloszenie);
					$statusCzasWygOgloszenia.html('Koniec aktywności: '+info_tab[1]);
				}
				else
				{
					var img = '<img src="'+baseUrl+'/public/images/admin/ikony/aktywuj.png" alt="aktywuj" />';
					$zmienStatusOgloszenia.html(img);
					$zmienStatusOgloszenia.attr('title', 'aktywuj ogłoszenie');
					$zmienStatusOgloszenia.click(aktywujOgloszenie);
					$przedluzOgloszenie.css('visibility', 'hidden');
					$statusCzasWygOgloszenia.html('Data usunięcia: '+info_tab[1]);
				}
				$statusPodgladOgloszenia.html(info_tab[2]);
				$statusPodgladOgloszenia.attr('href', info_tab[2]);
				$statusPodgladOgloszenia.css('font-weight', 'bold');
			}
			else
			{
				if(zaznWidocznyOtomoto === true)
				{
					$informacje_otomoto.html('Nie można pobrać informacji na temat tego ogłoszenia. Być może zostało ono usunięte w serwisie Otomoto lub serwis jest niedostępny.');
					$informacje_otomoto.css({'visibility': 'visible', 'text-align': 'justify'});
				}
			}
		});
	}

	//pobieranie informacji użytkowniku
	var $typ_podstrony = $('#TYP-PODSTRONY');
	if($typ_podstrony.val() == 'edycja')//'dodawanie')
	{
		$.post(baseUrl+'/ajax/otomoto/pobierzliczbewolnychogloszen/', function(data)
		{
			if(data != 'false' && data != null)
			{
				$liczbaWolnychOgloszen = $('#LICZBA-WOLNYCH-OGLOSZEN');
				if(data != 'false')
				{
					var info_tab = data.split('|');
					$liczbaWolnychOgloszen.html('Liczba wystawionych ogłoszeń: '+info_tab[0]+'<br/>Abonament: '+info_tab[1]);
				}
			}
		});
	}

	//reszta kodu
	$uwagi = $('.uwaga-informacja');
	$uwagi_cz = $('.uwaga-informacja-cz');
	var pozycja = null;
	var obsluzUwagi = function()
	{
		$(this).toggle(function(event)
		{
			event.preventDefault();
			pozycja = $(this).position();
			var temp = '<div class="uwaga-komunikat">'+$(this).attr('title')+'</div>';
			$komunikat = $(this).parent().append(temp).children('.uwaga-komunikat');
			$tab_param = 
			{
				'display': 'block',
				'position': 'absolute',
				'left': parseInt(pozycja.left),
				'top': parseInt(pozycja.top+20),
				'width': '320px',
				'border': '2px solid orange',
				'background-color': '#ffffff',
				'font-size': '15px',
				'cursor': 'pointer',
				'padding': '5px'
			}
			$komunikat.css($tab_param);
		}, 
		function(event)
		{
			event.preventDefault();
			$komunikat = $(this).parent().children('.uwaga-komunikat');
			$komunikat.css('display', 'none');
		});
	}
	$uwagi.each(obsluzUwagi);
	$uwagi_cz.each(obsluzUwagi);

});

function pokazKomunikat(tresc)
{
	$('#komunikat').show().html(tresc);
}
