function changeListaAttr(ten, name, extra, extraval)
{
	if(obConfig['ofertaAjax'])
	{
		getListaProd(false, 0, 0);
	}
	else
	{
		var nazwa = $(ten).attr('name');
		if(name != undefined) nazwa = name;
		var value = $(ten).find('option:selected').val();
		if(value == undefined) value = $(ten).val();
		if(value == undefined) value = '';
		if($(ten).is(":checkbox")) value = $(ten).is(":checked") ? value : "0";
		var extras = '';
		if(extra != undefined && extraval != undefined)
		extras = '/' + extra + '/' + extraval;		
		//if(false)
		window.location = baseUrl + '/' + searchLink + '/' + nazwa + '/' + value + extras;
	}
}
	
function getListaProd(reset, ile, strona)
{
	if(!obConfig['ofertaAjax'])
	{
		return true;
	}
	if(reset == undefined || reset == 'false') reset = 0;
	if(ile == 0 || ile == undefined) ile = ileNaStrone; else ileNaStrone = ile;
	if(strona == -1 || strona == undefined) strona = 0; else ktoraStrona = strona;
	
	$('.items').css('opacity', 0.25);
	$('.items').addClass('loading');
	var ilosc = $("#ile option:selected").val(); if(ilosc == undefined) ilosc = ile;
	var cenaOd = $("#cenaOd option:selected").val(); if(cenaOd == undefined) cenaOd = '0';
	var cenaDo = $("#cenaDo option:selected").val(); if(cenaDo == undefined) cenaDo = '0';
	var rozm = $('#rozmiar option:selected').val(); if(rozm == undefined) rozm = 'all';
	var wkl = $('#wkladka option:selected').val(); if(wkl == undefined) wkl = 'all';
	var kolor = $('#kolor option:selected').val(); if(kolor == undefined) kolor = '-1';
	var prod = $('#producent option:selected').val(); if(prod == undefined) prod = '-1';
	var sort = $('#sorter option:selected').val(); if(sort == undefined) sort = 'nazwa';
	var order = $('#order option:selected').val(); if(order == undefined) order = 'asc';
	var rozmWkl = rozm != 'all' ? rozm : (wkl != 'all' ? wkl : 'all');
	var nowosc = 0;//$('#specNowosc').is(':checked');
	var promocja = 0;//$('#specPromocja').is(':checked');
	var polecane = 0;//$('#specPolecane').is(':checked');
	//if(false)
	$.ajax(
	{
		url: baseUrl + "/ajax/index/lista",
		type: "GET",
		data: "id=" + id + "&od=" + cenaOd + '&do=' + cenaDo + '&rozm=' + rozm + '&wkl=' + wkl + '&kolor=' + kolor + '&prod=' + prod + '&sort=' + sort + '&order=' + order + '&nowosc=' + nowosc + '&promocja=' + promocja + '&polecane=' + polecane + '&reset=' + reset + '&cenaMax=' + cenaMax + '&ile=' + ilosc + '&strona=' + strona + '&akcja=' + action,
		dataType: 'html',
		success: function(data)
		{
			var dane = data.split("|");
			$('.items').replaceWith(dane[0]);
			$('.box_pager').replaceWith(dane[1]);
			$('.items').removeClass('loading');
			$('.items').css('opacity', 1);
		},
		error: function(data)
		{

		}
	});		
	return false;
}