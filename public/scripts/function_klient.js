function szukajWojew(ten, name)
{
	if(ten.value.length < 3)
	{
		$('#' + name).html('<option value="0">Wpisz co najmniej 3 znaki!</option>');
	}
	else
	$.ajax(
	{
		url: baseUrl + '/ajax/index/szukajwojew/q/'+encodeURIComponent(ten.value),
		cache: false,
		timeout : 5000,
		error: function(jqXHR, textStatus, errorThrown)
		{
			$('#' + name).html('<option value="0">'+errorThrown+'</option>');
		},
		success: function(html, textStatus, jqXHR)
		{
			var xml = $.parseXML(html);
			var ile = $(xml).find('wojew').length;
			$('#' + name).html('');
			if(ile > 0)
			{
				$(xml).find('wojew').each(function()
				{
					var id = $(this).find('id').text();
					var nazwa = $(this).find('nazwa').text();
					$('#' + name).append('<option value="'+nazwa+'">'+nazwa+'</option>');
				});
			}
		}
	});
}