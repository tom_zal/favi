//--------------------------
//          menu.js
// interactive drop-down-menu
//      Copyright 2010
//       Nathan Osman
//--------------------------

// This class keeps track of
// global information

function GlobalManager()
{
    // Number of IDs handed out
    this.id_count = 0;
    // Associative array of menus by ID
    this.items_by_id = new Array();
    // Fetches new ID
    this.GetNewID = GlobalManager_GetID;
    // Holds the active menus on the stack
    this.active_menu_items = new Array();
    // Clears the active menu stack to specified level
    this.ClearStackToLevel = GlobalManager_ClearStackToLevel;
}

// Given an object, returns a new
// ID for that object
function GlobalManager_GetID(item)
{
    this.items_by_id[this.id_count] = item;
    return this.id_count++;
}

// Clears the stack to the specified level
function GlobalManager_ClearStackToLevel(level)
{
    for(var i = (this.active_menu_items.length - 1);i >= level;--i)
    {
        // Get the top-of-stack
        var item_id = this.active_menu_items.pop();
        
        // Stop any pending animations
        // and hide the item
        $('#menu_' + item_id).stop(true,true).css({ left: 0, top: 0 }).hide();
    }
}

var g_global_manager = new GlobalManager();

// This is the root object that
// manages the top level menu

function RootMenu()
{
    // List of sub-items
    this.items = new Array();
    // Adds item to the root menu
    this.AddItem = RootMenu_AddItem;
    // Positions the given item
    this.Position = RootMenu_Position;
    // Shows the given root menu
    this.ShowRootMenu = RootMenu_ShowRootMenu;
    // Shows the given item
    this.ShowSubMenu = RootMenu_ShowSubMenu;
    // returns HTML for the root menu
    this.AddToDocument = RootMenu_AddToDocument;
}

function RootMenu_AddItem(item)
{
    this.items.push(item);
}

// This positions the menu on the screen
// next to its parent
//   id           - the menu's id
//   parent_name  - the name of the parent div
//   align_item   - the item's alignment
//   align_parent - the parent's alignment
//   offset       - the offset for the menu

function RootMenu_Position(item_name,parent_name,align)
{
    // First get the width
    // of this element
    
    var element = $(item_name);
    var el_width = element.outerWidth();

    // Now the parent

    var parent = $(parent_name);
    var p_width = parent.outerWidth();
    var p_height = parent.outerHeight();
    var p_pos = parent.position();
    var p_top = p_pos.top;
    var p_left = p_pos.left;

    var el_left = p_left + p_width / 2 - el_width / 2;
    
    if(align == 'm')    
        $(item_name).css({ 'top': p_top + p_height, 'left': el_left });
    else
    {
        // Get the menu                  UL      DIV
        var root_pos = $(parent_name).parent().parent().position();

        $(item_name).css({ 'top': root_pos.top + p_top, 'left': root_pos.left + p_width });
    }
}

// Shows the given root menu (given an ID)
function RootMenu_ShowRootMenu(id)
{
    // Skip if the menu to show is
    // on the top of the stack
    // (avoids flicker)
    if(g_global_manager.active_menu_items.length)
        if(g_global_manager.active_menu_items[g_global_manager.active_menu_items.length-1] == id)
            return;
    
    // Clear the stack
    g_global_manager.ClearStackToLevel(0);
    
    // Add this item to the list of
    // active items
    g_global_manager.active_menu_items.push(id);
    
    // Position the menu
    var parent = '#root_'+id;
	
    this.Position('#menu_' + id,parent,'m');
	
    // Now show the menu
    $('#menu_' + id).slideDown();
}

// Shows the given menu item
//   id              - the id of the menu to show
//   recursion_level - this items recursion level
function RootMenu_ShowSubMenu(id,recursion_level)
{
    // Skip if the menu to show is
    // on the top of the stack
    // (avoids flicker)
    if(g_global_manager.active_menu_items[g_global_manager.active_menu_items.length-1] == id)
        return;
    
    // Clear the stack up until the
    // level this item is on
    g_global_manager.ClearStackToLevel(recursion_level);
    
	// Add this item to the list of
    // active items
    g_global_manager.active_menu_items.push(id);
    
    // Position the menu
    this.Position('#menu_' + id,'#li_' + id,'s');
    
    // Now show it!
    $('#menu_' + id).slideDown();
}

// Adds the menu to the page
// in given item
function RootMenu_AddToDocument(item_to_add_to)
{
    // the left side of the menu
    $(item_to_add_to).append("<div class='menu_left'></div>");
    
    // The root div/ul
    var root_div = $("<div class='root_menu' id='root'></div>");
    var root_ul  = $("<ul></ul>");
    
    root_div.append(root_ul);
    
    // loop thru the root menus
    for(var i = 0;i < this.items.length;++i)
    {
        var new_item = $("<li id='root_" + this.items[i].id + "'>" + this.items[i].text + '</li>');
        
        var self = this;
        new_item.data('item_id',self.items[i].id);
        
        new_item.mouseenter(function() { self.ShowRootMenu($(this).data('item_id')); });
        
        root_ul.append(new_item);
        
        // Get children
        this.items[i].GetHTML(1,item_to_add_to);
    }
    
    $(item_to_add_to).append(root_div);
    
    // the right side of the menu
    $(item_to_add_to).append("<div class='menu_right'></div>");
}

// This class represents a menu item
// Note: the root menu must be passed along
// to the item

function MenuItem(root,text,click_handler)
{
    // the root element
    this.root = root
    // The menu caption
    this.text = text;
    // Adds a sub-menu
    this.AddItem = MenuItem_AddItem;
    // List of sub-items
    this.items = new Array();
    // returns HTML for this menu
    this.GetHTML = MenuItem_GetMenuHTML;
    // Get a new ID for this element
    this.id = g_global_manager.GetNewID(this);
    // The function to call on click event

    // We need to check to see if this is a string
    if(typeof click_handler == 'string')
        this.click_handler = function() { window.location.href=click_handler };
    else
        this.click_handler = click_handler;
}

// Adds given item to menu

function MenuItem_AddItem(new_item)
{
    this.items.push(new_item);
}

// Returns the HTML for this item and
// its children

function MenuItem_GetMenuHTML(recursion_level,item_to_add_to)
{
    // HTML for this menu
    var root_div = $("<div class='submenu' id='menu_" + this.id + "'></div>");
    var root_ul  = $("<ul></ul>");
    
    root_div.append(root_ul);
    
    // Loop thru sub-items
    for(var i=0;i<this.items.length;++i)
    {
        // Does this item have children?
        var has_children = false;
        var children_text = '';
        
        if(this.items[i].items.length)
        {
            has_children = true;
            children_text = ' &#x25b6;';
        }
        
        var child = $("<li id='li_" + this.items[i].id + "'>" + this.items[i].text + children_text + "</li>");
        
        if(has_children)
        {
			child.data('self',this);
			child.data('item_id',this.items[i].id);
			child.data('recursion_level',recursion_level);
			
	        child.mouseenter(function() {
	            $(this).data('self').root.ShowSubMenu($(this).data('item_id'),
	                                                  $(this).data('recursion_level')); });
	        
	        // Get the child's children
	        this.items[i].GetHTML(recursion_level + 1,item_to_add_to);
	    }
	    else
	    {
	        // Otherwise, clear the stack
	        // and register the click identifier
	        
	        child.data('self',this);
			child.data('recursion_level',recursion_level);
	        child.data('click_handler',this.items[i].click_handler);
	        
	        child.mouseenter(function() {
	            g_global_manager.ClearStackToLevel($(this).data('recursion_level')); });
	        
	        child.click(function() {
	            if($(this).data('click_handler'))
	                $(this).data('click_handler')(); });
	    }
        
        root_ul.append(child);
    }
    
    $(item_to_add_to).append(root_div);
}
