if(false)
{
	document.onmousedown = disableclick;
	function disableclick(e)
	{
		if(event.button == 2)
		{
			alert("Right Click Disabled");
			return false;
		}
	}
}

$(document).ready(function()
{
	if(false)
	$("#data").datepicker(
	{
		changeYear: true,
		yearRange: '1900:2010'
		//altField: '#boksWystawData',
		//altFormat: 'dd-mm-yy',
		dateFormat: 'dd-mm-yy'
		//minDate: new Date()
		//maxDate: '+1m',
		//showOn: "button",
		//buttonImage: baseUrl + "/public/images/admin/data.png",
		//buttonImageOnly: true
	});
	
	if(false)
	$("#polecEmoc").emoticate(
	{
		icon: 'smiley-icon', replacediv: 'list'
	});
		
	if(false)
	$('.input_ilosc').spinner(
	{
		min: 1,
		max: 999,
		step: 1,
		page: 5,
		numberFormat: "n",
		incremental: false,
		disabled: false
		//culture: "fr"
		//icons: { down: "custom-down-icon", up: "custom-up-icon" }
	});
	
	if(false)
	$("#produkt").tabs(
	{
		active: 0,
		collapsible: true,
		//disabled: [ 0, 2 ],
		//event: "mouseover",
		heightStyle: "content",
		show: { effect: "blind", duration: 1000, delay: 0, easing: "swing" },
		hide: { effect: "explode", duration: 1000, delay: 0, easing: "swing" }
	});
	
	if(false)//IE6
	$('.ui-state-default').hover
	(
		function()
		{
			$(this).children('#paramMenuUL').show();
		},
		function()
		{
			$(this).children('#paramMenuUL').hide();
		}
	);
	
	if(false)//IE6
	$("#menuLinki ul li").each(function()
	{
		$(this).hover
		(
			function()
			{
				$(this).children('.menuLinkiSubMenu').show();
			},
			function()
			{
				$(this).children('.menuLinkiSubMenu').hide();
			}
		);
	});
	
	if(false)//IE>6
	$('.scrollbar').mCustomScrollbar(
	{
		theme:"light",
		horizontalScroll:true,			
		autoDraggerLength:true,
		autoHideScrollbar:false,
		mouseWheel:true,
		mouseWheelPixels: "auto",
		scrollInertia: 3000,			
		scrollButtons:
		{
			enable:true,
			scrollType:"continuous",//pixels
			scrollSpeed:"auto",
			scrollAmount:160
		},
		advanced:
		{
			updateOnBrowserResize: false,
			updateOnContentResize: true,
			autoExpandHorizontalScroll: false
		},
		callbacks:
		{
			onScrollStart: function(){},
			onScroll: function(){},
			onTotalScroll: function(){},
			onTotalScrollBack: function(){},
			//onTotalScrollOffset: 0,
			//onTotalScrollBackOffset: 0,
			whileScrolling: function(){}
		}			
	});
	
	$(function()//IE7 fix
	{
		var zIndexNumber = 1000;
		$('div, span, label').each(function() 
		{
			$(this).css('zIndex', zIndexNumber);
			zIndexNumber -= 10;
		});
		$('span').each(function() 
		{
			$(this).css('zIndex', zIndexNumber);
			zIndexNumber -= 10;
		})
	});
});

function selectJedn(val, typ)
{
	var selekty = '.items' + typ + ' .itemBottom select';
	if(typ == undefined) selekty = '.typy';
	
	if(val == 'opak_jedn')
	$(selekty).each(function(key, val)
	{
		//$(this).find('option').removeAttr('selected');
		//$(this).find('option:first').attr('selected','selected');
		$(this).val($(this).find('option:first').val());
	});
	if(val == 'opak_zb')
	$(selekty).each(function(key, val)
	{
		//$(this).find('option').removeAttr('selected');
		//$(this).find('option').last().attr('selected','selected');
		$(this).val($(this).find('option:last').val());
	});
}

function setOpakZb(typ)
{
	if(typ == undefined) typ = '';
	var bottoms = '.items' + typ + ' .itemBottom';
	
	$(bottoms).each(function(key, val)
	{
		var zb = parseInt($(this).find('.opak_zb.non_zb span').text());
		if(zb > 0) $(this).find('input[type=text]').val(zb);
	});
}

if(false)
$(document).ready(function()
{
	if($("body").height() > $(window).height())
	{
        $('#jq_bottom').css('position', 'static');
    }
	else
	{
        $('#jq_bottom').css('position', 'fixed');
        $('#jq_bottom').css('bottom', '0px');
    }
    jQuery('.hov').hover
	(
        function()
		{
            jQuery(this).addClass('hover');
        },
        function ()
		{
            jQuery(this).removeClass("hover");
        }
    );
	
    var tooltip = document.getElementsByClassName('jQ_tooltip');
    if(tooltip)
    {
        try
        {
            $('.jQ_tooltip').tooltip(
			{
                delay: 0,
                showURL: false,
                fade: 250
            });
        } 
        catch(err)
        {
            //alert(err);
        }
    }
	
    var date = document.getElementsByClassName('calendar');
    if(date)
	{
        try
		{
			jQuery(".dataStart").datepicker(
			{
				dateFormat: 'yy-mm-dd', showOn: 'button',
				buttonImage: '/public/images/admin/ikony/calendar.gif',
				buttonImageOnly: true,
				changeMonth: true,
				changeYear: true,
				yearRange: '-90:+10'
			});
        }
        catch(err)
        {
            //alert(err);
        }
    } 
	
    /*POBRANIE WYSOKOSCI BLOKU PRODUKTU I NAGLOWKA*/
    $("div.blok").attr("id", function (arr) { return "blokwys" + arr; }) .each(function ()
	{
		var tab = new Array();
		var tab2 = new Array();
		var id = this.id;

        $('#'+id).find("div.one_product").each(function(index, item)
		{
            tab[index] = $(item).outerHeight();
            tab2[index] = $(item).find("h3").outerHeight();

            var max = Math.max.apply(null, tab);
            var max2 = Math.max.apply(null, tab2);
            var min2 = Math.min.apply(null, tab2);

            if(max2 != min2)
			{
                /*Roznica pomiedzy najmniejsza a najwieksza wysokosc oraz paddingiem*/
                var tmp = max2 - min2 - 10;
                $('#'+id).find("div.one_product h3").css('height', max2-10);
                max = max + tmp;
            }
            $('#'+id).find("div.one_product").css('height', max-20);
        });
    });
});

function zwin(co, btn)
{
    jQuery('#'+co).toggle('slow');
    var text = jQuery('#'+btn).hasClass('zwin');
    if(text)
	{
        jQuery('#'+btn).html('rozwin &or;');
        jQuery('#'+btn).addClass('rozwin').removeClass('zwin');
    }
	else
	{
        jQuery('#'+btn).html('zwin &and;');
        jQuery('#'+btn).addClass('zwin').removeClass('rozwin');
    }
}