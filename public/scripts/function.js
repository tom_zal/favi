var popupTimeout = null;
function popup(message, timeout)
{
	if(timeout == undefined) timeout = 3000;
	//$.jGrowl(data, { life: timeout, position: 'center' });
	$('#popupMessage').dialog(
	{
		autoOpen: true,
		modal: true,
		width: "auto",
		height: "auto",
		minWidth: 250,
		minHeight: 100,
		title: "Komunikat",
		resizable: false,
		closeText: "X",
		//dialogClass: 'popupMessage',
		position: ['center','center'],
		open: function() 
		{
			$('.ui-dialog').attr("tabIndex", -1);
			$('#popupMessage').html(message);
			var width = parseInt($('#popupMessage').css('width').replace('px',''));
			$('.ui-dialog').css('left', (($(window).width() - width) / 2) + 'px');
			//popupTimeout = setTimeout('$("#popupMessage").dialog("close")', timeout);
		},
		close:function()
		{
			//clearTimeout(popupTimeout);
			$('#popupMessage').html('');
		}
	});
}

if(typeof String.prototype.trim !== 'function')
{
	String.prototype.trim = function()
	{
		return this.replace(/^\s+|\s+$/g, ''); 
	}
}

function number_format(number, decimals, dec_point, thousands_sep)
{
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
	prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
	sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
	dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
	s = '',
	toFixedFix = function (n, prec)
	{
		var k = Math.pow(10, prec);
		return '' + Math.round(n * k) / k;
	};
	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3)
	{
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec)
	{
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
}

function htmlspecialchars(p_string) 
{
	p_string = p_string.replace(/&/g, '&amp;');
	p_string = p_string.replace(/</g, '&lt;');
	p_string = p_string.replace(/>/g, '&gt;');
	p_string = p_string.replace(/"/g, '&quot;');
	p_string = p_string.replace(/'/g, '&#039;');
	return p_string;
};
function htmlspecialchars_decode(p_string) 
{
	p_string = p_string.replace(/&amp;/g, '&');
	p_string = p_string.replace(/&lt;/g, '<');
	p_string = p_string.replace(/&gt;/g, '>');
	p_string = p_string.replace(/&quot;/g, '"');
	p_string = p_string.replace(/&#039;/g, "'");
	return p_string;
};

function selectboxheightfix(ten) 
{
	var size = $(ten).parent().find('.sbOptions').children('li').length;
	var height = $(ten).parent().find('.sbOptions').children('li').first().height();
	var heightOld = parseInt($(ten).parent().find('.sbOptions').css('max-height'));
	var heightNew = 2;//sbOptions border
	$(ten).parent().find('.sbOptions').children('li').each(function(key, val)
	{
		heightNew += parseInt($(ten).height());
		//heightNew++; //sbOptions a border dotted
		//heightNew+=2; //sbOptions a padding
		//alert($(ten).height());
	});
	var scrollTop = $(window).scrollTop();
	var offsetTop = $(ten).parent().offset().top;
	var windowHeight = $(window).height();
	var maxHeight = windowHeight - scrollTop;
	//alert(heightNew + ' ' + scrollTop + ' ' + offsetTop + ' ' + windowHeight + ' ' + maxHeight);
	if(heightNew > offsetTop && heightNew > maxHeight) heightNew = offsetTop;
	//var top = parseInt(size) * parseInt(height);		
	if(heightOld < heightNew && heightNew > maxHeight)
	{
		if(heightNew > 300) heightNew = 300;
		$(ten).parent().find('.sbOptions').css('top', -heightNew);
		$(ten).parent().find('.sbOptions').css('max-height', heightNew);
	}
};
