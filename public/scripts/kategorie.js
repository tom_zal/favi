var select = createObject();

var id;
var par = '';

function createObject()
{
	// przechowa odowłanie do obiektu XMLHttpRequest
	var xmlHttp;
	// powinno zadziałać dla wszystkich przeglądarem z wyjątkiem IE6 i starszych
	try
	{
		// próbuje utworzyć obiekt XMLHttpRequest
		xmlHttp = new XMLHttpRequest();
	}
	catch(e)
	{
		// zakładając, że IE6 lub starsza
		var XmlHttpVersions = new Array
		(
			"MSXML2.XMLHTTP.6.0",
			"MSXML2.XMLHTTP.5.0",
			"MSXML2.XMLHTTP.4.0",
			"MSXML2.XMLHTTP.3.0",
			"MSXML2.XMLHTTP",
			"Microsoft.XMLHTTP"
		);
		// próbuje wszystkie wartości progId, aż jedna zadziała
		for(var i=0; i<XmlHttpVersions.length && !xmlHttp; i++)
		{
			try
			{
				// próbuje stworzyć obiekt XMLHttpRequest
				xmlHttp = new ActiveXObject(XmlHttpVersions[i]);
			}
			catch(e){}
		}
	}
	// zwraca utworzony obiekt lub wyświetla komunikat o błędzie
	if (!xmlHttp)
	alert("Błąd podczas tworzenia obiektu XMLHttpRequest.");
	else
	return xmlHttp;
}


function getName(newid, typ, all, parent)
{
	var elements = null;
	//if(document.forms['dodaj'] != undefined)
    //elements = document.forms['dodaj'].elements;
    //alert(newid + ' ' + typ + ' ' + all + ' ' + parent);
	elements = new Array('pierwszy', 'drugi', 'trzeci', 'czwarty', 'piaty', 'szosty');
    for(var i = 0; i < elements.length; i++)
	{
		//alert(elements[i] + newid);
        if((elements[i] + typ) == newid)
		{
            //var xx = i + 2;
            //alert(i+1);
            pid = elements[i + 1] + typ;
            var x = i + 1;
            i = i + 2;
        }
        // usuwa dane z pozostałych elementów
        if(i < elements.length - 1 && i > x)
		{
            czysc = elements[i] + typ;
			element = '#' + czysc;
			if(par != '')
			element = '#' + par + ' ' + element;
			if($(element) != null)
			{
				$(element).html('');
				//if(all)$(element).html('<option value="-1">Wybierz</option>');
			}
        }
    }
    return pid;
}

function getNextCategory(kat, getid, base_url, action, typ, all, parent)
{
    if(select)
	{
        try
		{
			var ifAll = (all == true) ? 1 : 0;
			if(parseInt(all)) ifAll = parseInt(all);
			if(parent != undefined) par = parent;
			if(typ == undefined) typ = '';
			id = getName(getid, typ, ifAll, par);
			if(typ.length > 0) action = action + "/typ/" + typ;
            var param = "kat/" + kat + "/all/" + ifAll;
            select.open("GET", base_url+"/ajax/index/" + action + "/" + param, true);
            select.onreadystatechange = obslugaCategory;
            select.send(null);
        }
		catch(e)
		{
            console.log(e + " brak polaczenia z serwerem!!")
        }
    }
}

function obslugaCategory()
{
    glowna = document.getElementById(id);
    x = document.getElementById('wait');
    if (select.readyState != 4)
	{
        if(x == null)
		{
            wait = document.createElement('option');
            wait.id = 'wait';
            wait.innerHTML = 'Czekaj !!';
            glowna.appendChild(wait);
        }
    }

    if(select.readyState == 4)
	{
        if(select.status == 200)
		{
            try
			{
                requestCategory();
            }
			catch(e)
			{
                //alert("błąd obiektu 1" + e.toString());
            }
        }
		else
		{
            //alert("problem z danymi");
        }
    }
}

function requestCategory()
{
    /////pobiera element do którego dane mają zostać załadowane
	var el = '#' + id;
	if(par != '')
	el = '#' + par + ' ' + el;
    glowna = $(el);
	//alert(el);

    /////////czysci wczeniejsze dane
    $(el).html('');
    //////pobiera odpowiedz w postaci XML
    var odp = select.responseXML;
	//var odp = $.parseXML(select);
	//alert(odp);
    ////////pobiera element dokument pliku XML
    root = odp.documentElement;
	//alert(root);
    /////// pobiera tablice podkat i nazwa
    x = root.getElementsByTagName('podkat');
    y = root.getElementsByTagName('nazwa');
	//console.log(odp);
    licz = y.length;
    //alert(y);

    for(var i = 0; i < licz; i++)
	{
        ///////pobiera wartosc pierwszych elementów
        podkat = x.item(i).firstChild.data;
        nazwa  = y.item(i).firstChild.data;

        //// tworzy nowy alement option
        newoption = document.getElementById('wait');
        wait = document.createElement('option');
		//alert(nazwa);
		 
        ///////// przypisuje mu wartosc podkat
        wait.value = podkat;
		if (wait.value == 'xx')
		{
			wait.setAttribute("disabled", "true");
		}
		wait.id = 'wait';
        /////////wstawia tekst nazwa
        wait.innerHTML = nazwa;
        // dodaje element do glowna
        $(el).append(wait);
    }
}