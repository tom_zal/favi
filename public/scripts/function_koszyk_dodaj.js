$(document).ready(function()
{
	$(".ui-dialog").bind("focus", function (e)
	{
		e.preventDefault();
		e.stopPropagation();
		return false;
	});
	$(".items form .input_ilosc").bind("blur", function (e)
	{
		//e.preventDefault();
		e.stopPropagation();
		//return false;
	});
	$(".items form .input_ilosc").bind("keypress", function (e)
	{
		//console.log("key" + e.keyCode);
		if (e.keyCode == 13)
		{
			//e.keyCode = 9;
			var inputs = $(this).closest('form').find(':input.input_ilosc:visible');
			if(inputs.eq(inputs.index(this) + 1).attr('id') != undefined)
			inputs.eq(inputs.index(this) + 1).focus();
			else $(this).blur();
			e.preventDefault();
			e.stopPropagation();
			return false;
		}
	});
	$("#produktKoszykAll form .input_ilosc").bind("keypress", function (e)
	{
		//alert(e.keyCode);
		if (e.keyCode == 13)
		{
			doKoszyka($(this), $('input[name=id]').val(), true);
			e.preventDefault(); //Disable standard Enterkey action
			e.stopPropagation();
			return false;
		}
	});
	$("form .tabela_koszyk .ilosci").bind("keypress", function (e)
	{
		//alert(e.keyCode);
		if (e.keyCode == 13)
		{
			var inputs = $(this).closest('form').find(':input.ilosci:visible');
			if(inputs.eq(inputs.index(this) + 1).attr('name') != undefined)
			inputs.eq(inputs.index(this) + 1).focus();
			else $(this).blur();
			e.preventDefault(); //Disable standard Enterkey action
			e.stopPropagation();
			return false;
		}
	});
	$("#model").bind("keypress", function (e)
	{
		//console.log("key" + e.keyCode);
		if (e.keyCode == 13)
		{
			e.preventDefault();
			e.stopPropagation();
			//if(szukajMarkaModel())
			$("#prodSubmit").trigger('click');
			return false;
		}
	});
	$("#fraza").bind("keypress", function (e)
	{
		//console.log("key" + e.keyCode);
		if (e.keyCode == 13)
		{
			e.preventDefault();
			e.stopPropagation();
			//if(szukajNazwa())
			$("#coSubmit").trigger('click');
			return false;
		}
	});
});