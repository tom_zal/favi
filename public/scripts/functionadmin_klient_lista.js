function klienciListaOpcjeAllWiecej(ten, url)
{
	var akcja = '';
	var uwaga = '';
	var val = ten.value;
	if(val == '') { $(ten).val('wiecej'); return; }
	if(val == 'wiecej') return;
	var ileZazn = $('.checkbox:checked').length;
	var ileAll = 0;//<?php echo isset($this->ileProd) ? $this->ileProd : 0; ?>;
	var tryb = 'zadania_wybrane';//$(ten).attr('name') == 'opcje3' ? 'wyszukiwarka' : 'zadania_wybrane';
	var ile = tryb == 'wyszukiwarka' ? ileAll : ileZazn;
	var czego = '';
	
	if(ile == 0)
	{
		if(tryb == 'zadania_wybrane') alert('Nie zaznaczono żadnych klientów!');
		if(tryb == 'wyszukiwarka') alert('Na liście nie ma klientów!');
		$(ten).val('wiecej');
		return;
	}
	
	switch(val)
	{
		case 'usun' : 
		akcja = 'admin/kontrahent/lista/delids'; 
		uwaga = 'Czy na pewno chcesz usunąć zaznaczonych klientów ?';
		czego = 'usunięcia';
		break;
		case 'mail' : 
		akcja = 'admin/kontrahent/mail/mailids'; 
		break;
		case 'sms' : 
		akcja = 'admin/kontrahent/sms/smsids'; 
		break;
		case 'trasa' : 
		akcja = 'admin/kontrahent/lista/trasaids'; 
		break;
		case 'ukryj' : 
		akcja = 'admin/kontrahent/lista/widoczny/0'; 
		uwaga = 'Czy na pewno chcesz ukryć zaznaczonych klientów ?';
		break;
		case 'odkryj' : 
		akcja = 'admin/kontrahent/lista/widoczny/1'; 
		uwaga = 'Czy na pewno chcesz pokazać zaznaczonych klientów ?';
		break;
		case 'rabat_zapytaj_1' : 
		akcja = 'admin/kontrahent/lista/rabat_zapytaj/1'; 
		uwaga = 'Czy na pewno chcesz ustawić "Zapytaj o rabat" zaznaczonym klientom ?';
		break;
		case 'rabat_zapytaj_0' : 
		akcja = 'admin/kontrahent/lista/rabat_zapytaj/0';
		uwaga = 'Czy na pewno chcesz usunąć "Zapytaj o rabat" zaznaczonym klientom ?';
		break;
		default : 
		akcja = 'admin/kontrahent/lista';
	}
	
	$('#form').attr('action', url + '/' + akcja + '/all');
	if(uwaga.length == 0 || window.confirm(uwaga))
	{
		if(false) ProducentWindow(tryb, ile, ten);
		else $('#form').submit();
	}
	else $(ten).val('wiecej');
}