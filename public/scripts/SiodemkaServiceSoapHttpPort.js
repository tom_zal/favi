function firstChildElement (node) {
    if (!node)        return null;    var child = node.firstChild;
    while (child) {
        if (child.nodeType == 1)
            return child;
        child = child.nextSibling;
    }
    return null;
}

function nextSiblingElement (node) {
    if (!node)        return null;    var sibling = node.nextSibling;
    while (sibling) {
        if (sibling.nodeType == 1)
            return sibling;
        sibling = sibling.nextSibling;
    }
    return null;
}

function getText (node) {
    if (!node)        return null;    var text = '';
    var child = node.firstChild;
    while (child) {
        if (child.nodeType == 3) {
            text = text + child.nodeValue;
        }
        child = child.nextSibling;
    }
    return text;
}

function invokeSync (url, xmlDoc) {
    var req = null;    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (req) {
        req.open("POST", url, false);
        req.setRequestHeader("Content-Type", "text/xml");
		try{
        req.send(xmlDoc);
		}
		catch(e){}
        return req.responseXML;
    }
}

function invokeAsync (urll, xmlDoc, callback) {
	
	//wczytajAjax(url, xmlDoc);
	
	$('#siodemka').val(encodeURIComponent($(xmlDoc).xml()));
	$('#submit').trigger('click');
	//alert($(xmlDoc).xml());
	//var xml = $.parseXML(xml);parses xml string to xml doc
	
	/*
	 $.ajax({
	   url: urll,
	   processData: false,
	   data: xmlDoc,
	   type: "POST",
	   dataType: "xml",
	   success: function(msg){
         alert(msg);}
	 });
	*/
	return;
    
	
	var req = null;
    if (window.XMLHttpRequest) {
        req = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    if (req) {
        req.onreadystatechange = function () {
            if (req.readyState == 4) {
                if (req.status == 200 || req.status == 0) {
                    //callback(req.responseXML);
					alert(req.responseXML);
                } 
				
            }
			
			//alert('state'+req.readyState);
			//alert('status'+req.status);
        }
        req.open("POST", url, true);
        req.setRequestHeader("Content-Type", "text/xml");
		try{
        req.send(xmlDoc);
		}catch(e){}
    }
}

function createNewDocument () {
    var xmlDoc = null;
    if (document.implementation && document.implementation.createDocument) {
        xmlDoc = document.implementation.createDocument("", "", null);
    } else if (window.ActiveXObject){
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
    }
    return xmlDoc;
}

function createElementNS (xmlDoc, namespace, localName) {
    var element = null;
    if (typeof xmlDoc.createElementNS != 'undefined') {
        element = xmlDoc.createElementNS(namespace, localName);
    }
    else if (typeof xmlDoc.createNode != 'undefined') {
        if (namespace) {
            element = xmlDoc.createNode(1, localName, namespace);
        } else {
            element = xmlDoc.createElement(localName);
        }
    }
    return element;
}

function localName (element) {
    if (element.localName)
        return element.localName;
    else
        return element.baseName;
}



function SiodemkaServiceSoapHttpPort_dodajOdbiorce(_daneKlienta, _klucz) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'dodajOdbiorceElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'daneKlienta');
    SiodemkaServiceSoapHttpPort_serialize_Kontrahent(xmlDoc, paramEl, _daneKlienta, 'http://app.siodemka.com/mm7ws/type/');
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = SiodemkaServiceSoapHttpPort_deserialize_Kontrahent(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function SiodemkaServiceSoapHttpPort_dodajOdbiorceAsync(_daneKlienta, _klucz, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'dodajOdbiorceElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'daneKlienta');
    SiodemkaServiceSoapHttpPort_serialize_Kontrahent(xmlDoc, paramEl, _daneKlienta, 'http://app.siodemka.com/mm7ws/type/');
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
    var resultsObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = SiodemkaServiceSoapHttpPort_deserialize_Kontrahent(resultEl);
    else
        resultObj = null;
    callback(resultObj);
    }
    invokeAsync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc, resultsProcessor);
}

function SiodemkaServiceSoapHttpPort_wydrukEtykietaPdf(_numery, _klucz, _separator) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'wydrukEtykietaPdfElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numery');
    paramEl.appendChild(xmlDoc.createTextNode(_numery));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'separator');
    paramEl.appendChild(xmlDoc.createTextNode(_separator));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj =  getText(resultEl);
    return resultObj;
}

function SiodemkaServiceSoapHttpPort_wydrukEtykietaPdfAsync(_numery, _klucz, _separator, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'wydrukEtykietaPdfElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numery');
    paramEl.appendChild(xmlDoc.createTextNode(_numery));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'separator');
    paramEl.appendChild(xmlDoc.createTextNode(_separator));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
    var resultsObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj =  getText(resultEl);
    callback(resultObj);
    }
    invokeAsync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc, resultsProcessor);
}

function SiodemkaServiceSoapHttpPort_statusyPrzesylki(_numerListu, _czyOstatni, _klucz) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'statusyPrzesylkiElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numerListu');
    paramEl.appendChild(xmlDoc.createTextNode(_numerListu));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'czyOstatni');
    paramEl.appendChild(xmlDoc.createTextNode(_czyOstatni));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj = [];
    if (resultEl) {
        for (var achild=firstChildElement(resultEl.parentNode); achild; achild = nextSiblingElement(achild)) {
            resultObj[resultObj.length] = SiodemkaServiceSoapHttpPort_deserialize_PaczkaStatus('result', resultEl.parentNode);
        }
    }
    return resultObj;
}

function SiodemkaServiceSoapHttpPort_statusyPrzesylkiAsync(_numerListu, _czyOstatni, _klucz, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'statusyPrzesylkiElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numerListu');
    paramEl.appendChild(xmlDoc.createTextNode(_numerListu));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'czyOstatni');
    paramEl.appendChild(xmlDoc.createTextNode(_czyOstatni));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
    var resultsObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj = [];
    if (resultEl) {
        for (var achild=firstChildElement(resultEl.parentNode); achild; achild = nextSiblingElement(achild)) {
            resultObj[resultObj.length] = SiodemkaServiceSoapHttpPort_deserialize_PaczkaStatus('result', resultEl.parentNode);
        }
    }
    callback(resultObj);
    }
    invokeAsync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc, resultsProcessor);
}

function SiodemkaServiceSoapHttpPort_nowyDokumentWydania(_kurier, _numery, _klucz, _separator) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nowyDokumentWydaniaElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'kurier');
    paramEl.appendChild(xmlDoc.createTextNode(_kurier));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numery');
    paramEl.appendChild(xmlDoc.createTextNode(_numery));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'separator');
    paramEl.appendChild(xmlDoc.createTextNode(_separator));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj =  getText(resultEl);
    return resultObj;
}

function SiodemkaServiceSoapHttpPort_nowyDokumentWydaniaAsync(_kurier, _numery, _klucz, _separator, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nowyDokumentWydaniaElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'kurier');
    paramEl.appendChild(xmlDoc.createTextNode(_kurier));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numery');
    paramEl.appendChild(xmlDoc.createTextNode(_numery));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'separator');
    paramEl.appendChild(xmlDoc.createTextNode(_separator));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
    var resultsObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj =  getText(resultEl);
    callback(resultObj);
    }
    invokeAsync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc, resultsProcessor);
}

function SiodemkaServiceSoapHttpPort_wydrukListPdf(_numer, _klucz) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'wydrukListPdfElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numer');
    paramEl.appendChild(xmlDoc.createTextNode(_numer));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj =  getText(resultEl);
    return resultObj;
}

function SiodemkaServiceSoapHttpPort_wydrukListPdfAsync(_numer, _klucz, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'wydrukListPdfElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numer');
    paramEl.appendChild(xmlDoc.createTextNode(_numer));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
    var resultsObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj =  getText(resultEl);
    callback(resultObj);
    }
    invokeAsync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc, resultsProcessor);
}

function SiodemkaServiceSoapHttpPort_pobierzDokumentWydania(_numerDokumentu, _klucz) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'pobierzDokumentWydaniaElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numerDokumentu');
    paramEl.appendChild(xmlDoc.createTextNode(_numerDokumentu));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj =  getText(resultEl);
    return resultObj;
}

function SiodemkaServiceSoapHttpPort_pobierzDokumentWydaniaAsync(_numerDokumentu, _klucz, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'pobierzDokumentWydaniaElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numerDokumentu');
    paramEl.appendChild(xmlDoc.createTextNode(_numerDokumentu));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
    var resultsObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj =  getText(resultEl);
    callback(resultObj);
    }
    invokeAsync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc, resultsProcessor);
}

function SiodemkaServiceSoapHttpPort_edytujKlienta(_daneKlienta, _klucz) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'edytujKlientaElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'daneKlienta');
    SiodemkaServiceSoapHttpPort_serialize_Kontrahent(xmlDoc, paramEl, _daneKlienta, 'http://app.siodemka.com/mm7ws/type/');
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = SiodemkaServiceSoapHttpPort_deserialize_Kontrahent(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function SiodemkaServiceSoapHttpPort_edytujKlientaAsync(_daneKlienta, _klucz, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'edytujKlientaElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'daneKlienta');
    SiodemkaServiceSoapHttpPort_serialize_Kontrahent(xmlDoc, paramEl, _daneKlienta, 'http://app.siodemka.com/mm7ws/type/');
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
    var resultsObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = SiodemkaServiceSoapHttpPort_deserialize_Kontrahent(resultEl);
    else
        resultObj = null;
    callback(resultObj);
    }
    invokeAsync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc, resultsProcessor);
}

function SiodemkaServiceSoapHttpPort_pobierzNumerDokumentuList(_numerListu, _klucz) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'pobierzNumerDokumentuListElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numerListu');
    paramEl.appendChild(xmlDoc.createTextNode(_numerListu));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj =  getText(resultEl);
    return resultObj;
}

function SiodemkaServiceSoapHttpPort_pobierzNumerDokumentuListAsync(_numerListu, _klucz, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'pobierzNumerDokumentuListElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numerListu');
    paramEl.appendChild(xmlDoc.createTextNode(_numerListu));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
    var resultsObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj =  getText(resultEl);
    callback(resultObj);
    }
    invokeAsync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc, resultsProcessor);
}

function SiodemkaServiceSoapHttpPort_listNadanie(_przesylka, _klucz) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'listNadanieElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'przesylka');
    SiodemkaServiceSoapHttpPort_serialize_List(xmlDoc, paramEl, _przesylka, 'http://app.siodemka.com/mm7ws/type/');
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = SiodemkaServiceSoapHttpPort_deserialize_List(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function SiodemkaServiceSoapHttpPort_listNadanieAsync(_przesylka, _klucz, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'listNadanieElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'przesylka');
    SiodemkaServiceSoapHttpPort_serialize_List(xmlDoc, paramEl, _przesylka, 'http://app.siodemka.com/mm7ws/type/');
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
    var resultsObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = SiodemkaServiceSoapHttpPort_deserialize_List(resultEl);
    else
        resultObj = null;
    callback(resultObj);
    }
    invokeAsync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc, resultsProcessor);
}

function SiodemkaServiceSoapHttpPort_wydrukEtykietaEPL(_numery, _klucz, _separator) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'wydrukEtykietaEPLElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numery');
    paramEl.appendChild(xmlDoc.createTextNode(_numery));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'separator');
    paramEl.appendChild(xmlDoc.createTextNode(_separator));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj =  getText(resultEl);
    return resultObj;
}

function SiodemkaServiceSoapHttpPort_wydrukEtykietaEPLAsync(_numery, _klucz, _separator, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'wydrukEtykietaEPLElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numery');
    paramEl.appendChild(xmlDoc.createTextNode(_numery));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'separator');
    paramEl.appendChild(xmlDoc.createTextNode(_separator));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
    var resultsObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj =  getText(resultEl);
    callback(resultObj);
    }
    invokeAsync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc, resultsProcessor);
}

function SiodemkaServiceSoapHttpPort_wydrukEtykietaPng(_numer, _klucz) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'wydrukEtykietaPngElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numer');
    paramEl.appendChild(xmlDoc.createTextNode(_numer));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc);
    var resultObj = null;
	//if(responseDoc != null)
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj =  getText(resultEl);
    return resultObj;
}

function SiodemkaServiceSoapHttpPort_wydrukEtykietaPngAsync(_numer, _klucz, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'wydrukEtykietaPngElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numer');
    paramEl.appendChild(xmlDoc.createTextNode(_numer));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
    var resultsObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj =  getText(resultEl);
    callback(resultObj);
    }
    invokeAsync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc, resultsProcessor);
}

function SiodemkaServiceSoapHttpPort_dodajNadawce(_daneKlienta, _klucz) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'dodajNadawceElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'daneKlienta');
    SiodemkaServiceSoapHttpPort_serialize_Kontrahent(xmlDoc, paramEl, _daneKlienta, 'http://app.siodemka.com/mm7ws/type/');
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = SiodemkaServiceSoapHttpPort_deserialize_Kontrahent(resultEl);
    else
        resultObj = null;
    return resultObj;
}

function SiodemkaServiceSoapHttpPort_dodajNadawceAsync(_daneKlienta, _klucz, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'dodajNadawceElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'daneKlienta');
    SiodemkaServiceSoapHttpPort_serialize_Kontrahent(xmlDoc, paramEl, _daneKlienta, 'http://app.siodemka.com/mm7ws/type/');
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
    var resultsObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    if (resultEl)
        resultObj = SiodemkaServiceSoapHttpPort_deserialize_Kontrahent(resultEl);
    else
        resultObj = null;
    callback(resultObj);
    }
    invokeAsync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc, resultsProcessor);
}

function SiodemkaServiceSoapHttpPort_szukajKlienta(_daneKlienta, _klucz, _tryb) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'szukajKlientaElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'daneKlienta');
    SiodemkaServiceSoapHttpPort_serialize_Kontrahent(xmlDoc, paramEl, _daneKlienta, 'http://app.siodemka.com/mm7ws/type/');
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'tryb');
    paramEl.appendChild(xmlDoc.createTextNode(_tryb));
    parameterParent.appendChild(paramEl);
    var responseDoc = invokeSync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc);
    var resultObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj = [];
    if (resultEl) {
        for (var achild=firstChildElement(resultEl.parentNode); achild; achild = nextSiblingElement(achild)) {
            resultObj[resultObj.length] = SiodemkaServiceSoapHttpPort_deserialize_Kontrahent('result', resultEl.parentNode);
        }
    }
    return resultObj;
}

function SiodemkaServiceSoapHttpPort_szukajKlientaAsync(_daneKlienta, _klucz, _tryb, callback) {
    var xmlDoc = createNewDocument();
    var envelope = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Envelope');
    xmlDoc.appendChild(envelope);
    var body = createElementNS(xmlDoc, 'http://schemas.xmlsoap.org/soap/envelope/', 'Body');
    envelope.appendChild(body);
    var parameterParent = body;
    parameterParent = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'szukajKlientaElement');
    body.appendChild(parameterParent);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'daneKlienta');
    SiodemkaServiceSoapHttpPort_serialize_Kontrahent(xmlDoc, paramEl, _daneKlienta, 'http://app.siodemka.com/mm7ws/type/');
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'klucz');
    paramEl.appendChild(xmlDoc.createTextNode(_klucz));
    parameterParent.appendChild(paramEl);
    var paramEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'tryb');
    paramEl.appendChild(xmlDoc.createTextNode(_tryb));
    parameterParent.appendChild(paramEl);
    var resultsProcessor = function (responseDoc) {
    var resultsObj = null;
    body = firstChildElement(responseDoc.documentElement);
    if (localName(body) != 'Body') {
        body = nextSiblingElement(body);
    }

    var resultEl = firstChildElement(body);
    resultEl = firstChildElement(resultEl);
    resultObj = [];
    if (resultEl) {
        for (var achild=firstChildElement(resultEl.parentNode); achild; achild = nextSiblingElement(achild)) {
            resultObj[resultObj.length] = SiodemkaServiceSoapHttpPort_deserialize_Kontrahent('result', resultEl.parentNode);
        }
    }
    callback(resultObj);
    }
    invokeAsync('http://testws.siodemka.com:80/mm7ws/SiodemkaServiceSoapHttpPort', xmlDoc, resultsProcessor);
}

function SiodemkaServiceSoapHttpPort_serialize_Kontrahent (xmlDoc, parentEl, adt, namespaceURI) {
    var partEl;
    if (adt.ulica) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'ulica');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.ulica));
    }
    if (adt.emailKontakt) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'emailKontakt');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.emailKontakt));
    }
    if (adt.imie) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'imie');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.imie));
    }
    if (adt.numer) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numer');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.numer));
    }
    if (adt.nazwisko) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nazwisko');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nazwisko));
    }
    if (adt.nrDom) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nrDom');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nrDom));
    }
    if (adt.nrLokal) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nrLokal');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nrLokal));
    }
    if (adt.kod) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'kod');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.kod));
    }
    if (adt.nrExt) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nrExt');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nrExt));
    }
    if (adt.nazwa) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nazwa');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nazwa));
    }
    if (adt.telKontakt) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'telKontakt');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.telKontakt));
    }
    if (adt.nip) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nip');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nip));
    }
    if (adt.czyFirma) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'czyFirma');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.czyFirma));
    }
    if (adt.kodKraju) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'kodKraju');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.kodKraju));
    }
    if (adt.miasto) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'miasto');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.miasto));
    }
    if (adt.fax) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'fax');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.fax));
    }
    if (adt.telefonKom) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'telefonKom');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.telefonKom));
    }
    if (adt.czyNadawca) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'czyNadawca');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.czyNadawca));
    }
}

function SiodemkaServiceSoapHttpPort_serialize_List (xmlDoc, parentEl, adt, namespaceURI) {
    var partEl;
    if (adt.nrPrzesylki) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nrPrzesylki');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nrPrzesylki));
    }
    if (adt.nrExt) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nrExt');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nrExt));
    }
    if (adt.mpk) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'mpk');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.mpk));
    }
    if (adt.rodzajPrzesylki) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'rodzajPrzesylki');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.rodzajPrzesylki));
    }
    if (adt.placi) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'placi');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.placi));
    }
    if (adt.formaPlatnosci) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'formaPlatnosci');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.formaPlatnosci));
    }
    if (adt.nadawca) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nadawca');
        parentEl.appendChild(partEl);
        SiodemkaServiceSoapHttpPort_serialize_KontrahentNadawca(xmlDoc, partEl, adt.nadawca, 'http://app.siodemka.com/mm7ws/type/');
    }
    if (adt.odbiorca) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'odbiorca');
        parentEl.appendChild(partEl);
        SiodemkaServiceSoapHttpPort_serialize_KontrahentOdbiorca(xmlDoc, partEl, adt.odbiorca, 'http://app.siodemka.com/mm7ws/type/');
    }
    if (adt.platnik) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'platnik');
        parentEl.appendChild(partEl);
        SiodemkaServiceSoapHttpPort_serialize_KontrahentPlatnik(xmlDoc, partEl, adt.platnik, 'http://app.siodemka.com/mm7ws/type/');
    }
    if (adt.uslugi) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'uslugi');
        parentEl.appendChild(partEl);
        SiodemkaServiceSoapHttpPort_serialize_Uslugi(xmlDoc, partEl, adt.uslugi, 'http://app.siodemka.com/mm7ws/type/');
    }
    if (adt.paczki) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'paczki');
        parentEl.appendChild(partEl);
        SiodemkaServiceSoapHttpPort_serialize_Paczki(xmlDoc, partEl, adt.paczki, 'http://app.siodemka.com/mm7ws/type/');
    }
    if (adt.potwierdzenieNadania) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'potwierdzenieNadania');
        parentEl.appendChild(partEl);
        SiodemkaServiceSoapHttpPort_serialize_PotwierdzenieNadania(xmlDoc, partEl, adt.potwierdzenieNadania, 'http://app.siodemka.com/mm7ws/type/');
    }
    if (adt.uwagi) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'uwagi');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.uwagi));
    }
}

function SiodemkaServiceSoapHttpPort_serialize_KontrahentNadawca (xmlDoc, parentEl, adt, namespaceURI) {
    var partEl;
    if (adt.numer) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numer');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.numer));
    }
    if (adt.nazwisko) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nazwisko');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nazwisko));
    }
    if (adt.imie) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'imie');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.imie));
    }
    if (adt.telKontakt) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'telKontakt');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.telKontakt));
    }
    if (adt.emailKontakt) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'emailKontakt');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.emailKontakt));
    }
}

function SiodemkaServiceSoapHttpPort_serialize_KontrahentOdbiorca (xmlDoc, parentEl, adt, namespaceURI) {
    var partEl;
    if (adt.numer) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numer');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.numer));
    }
    if (adt.nrExt) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nrExt');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nrExt));
    }
    if (adt.czyFirma) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'czyFirma');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.czyFirma));
    }
    if (adt.nazwa) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nazwa');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nazwa));
    }
    if (adt.nip) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nip');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nip));
    }
    if (adt.nazwisko) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nazwisko');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nazwisko));
    }
    if (adt.imie) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'imie');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.imie));
    }
    if (adt.kodKraju) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'kodKraju');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.kodKraju));
    }
    if (adt.kod) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'kod');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.kod));
    }
    if (adt.miasto) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'miasto');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.miasto));
    }
    if (adt.ulica) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'ulica');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.ulica));
    }
    if (adt.nrDom) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nrDom');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nrDom));
    }
    if (adt.nrLokal) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nrLokal');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nrLokal));
    }
    if (adt.telKontakt) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'telKontakt');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.telKontakt));
    }
    if (adt.emailKontakt) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'emailKontakt');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.emailKontakt));
    }
}

function SiodemkaServiceSoapHttpPort_serialize_KontrahentPlatnik (xmlDoc, parentEl, adt, namespaceURI) {
    var partEl;
    if (adt.numer) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numer');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.numer));
    }
    if (adt.telKontakt) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'telKontakt');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.telKontakt));
    }
}

function SiodemkaServiceSoapHttpPort_serialize_Uslugi (xmlDoc, parentEl, adt, namespaceURI) {
    var partEl;
    if (adt.nrBezpiecznejKoperty) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nrBezpiecznejKoperty');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nrBezpiecznejKoperty));
    }
    if (adt.zkld) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'zkld');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.zkld));
    }
    if (adt.zd) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'zd');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.zd));
    }
    if (adt.ubezpieczenie) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'ubezpieczenie');
        parentEl.appendChild(partEl);
        SiodemkaServiceSoapHttpPort_serialize_Ubezpieczenie(xmlDoc, partEl, adt.ubezpieczenie, 'http://app.siodemka.com/mm7ws/type/');
    }
    if (adt.pobranie) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'pobranie');
        parentEl.appendChild(partEl);
        SiodemkaServiceSoapHttpPort_serialize_Pobranie(xmlDoc, partEl, adt.pobranie, 'http://app.siodemka.com/mm7ws/type/');
    }
    if (adt.awizacjaTelefoniczna) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'awizacjaTelefoniczna');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.awizacjaTelefoniczna));
    }
    if (adt.potwNadEmail) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'potwNadEmail');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.potwNadEmail));
    }
    if (adt.potwDostEmail) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'potwDostEmail');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.potwDostEmail));
    }
    if (adt.potwDostSMS) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'potwDostSMS');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.potwDostSMS));
    }
    if (adt.skladowanie) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'skladowanie');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.skladowanie));
    }
    if (adt.nadOdbPKP) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nadOdbPKP');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nadOdbPKP));
    }
    if (adt.odbNadgodziny) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'odbNadgodziny');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.odbNadgodziny));
    }
    if (adt.odbWlas) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'odbWlas');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.odbWlas));
    }
    if (adt.palNextDay) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'palNextDay');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.palNextDay));
    }
    if (adt.osobaFiz) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'osobaFiz');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.osobaFiz));
    }
    if (adt.market) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'market');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.market));
    }
    if (adt.zastrzDorNaGodz) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'zastrzDorNaGodz');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.zastrzDorNaGodz));
    }
    if (adt.zastrzDorNaDzien) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'zastrzDorNaDzien');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.zastrzDorNaDzien));
    }
}

function SiodemkaServiceSoapHttpPort_serialize_Paczki (xmlDoc, parentEl, adt, namespaceURI) {
    var partEl;
    if (adt.paczka) {
        SiodemkaServiceSoapHttpPort_serialize_arrayof_Paczka(xmlDoc, parentEl, adt.paczka, 'http://app.siodemka.com/mm7ws/type/', 'paczka');
    }
}

function SiodemkaServiceSoapHttpPort_serialize_PotwierdzenieNadania (xmlDoc, parentEl, adt, namespaceURI) {
    var partEl;
    if (adt.dataNadania) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'dataNadania');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.dataNadania));
    }
    if (adt.numerKuriera) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'numerKuriera');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.numerKuriera));
    }
    if (adt.podpisNadawcy) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'podpisNadawcy');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.podpisNadawcy));
    }
}

function SiodemkaServiceSoapHttpPort_serialize_Ubezpieczenie (xmlDoc, parentEl, adt, namespaceURI) {
    var partEl;
    if (adt.kwotaUbezpieczenia) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'kwotaUbezpieczenia');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.kwotaUbezpieczenia));
    }
    if (adt.opisZawartosci) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'opisZawartosci');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.opisZawartosci));
    }
}

function SiodemkaServiceSoapHttpPort_serialize_Pobranie (xmlDoc, parentEl, adt, namespaceURI) {
    var partEl;
    if (adt.kwotaPobrania) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'kwotaPobrania');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.kwotaPobrania));
    }
    if (adt.formaPobrania) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'formaPobrania');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.formaPobrania));
    }
    if (adt.nrKonta) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nrKonta');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nrKonta));
    }
}

function SiodemkaServiceSoapHttpPort_serialize_arrayof_Paczka (xmlDoc, parentEl, adt, namespaceURI, localPart) {
    for(var i=0; i<adt.length; i++) {
        var elementEl = createElementNS(xmlDoc, namespaceURI, localPart);
        parentEl.appendChild(elementEl);
        SiodemkaServiceSoapHttpPort_serialize_Paczka(xmlDoc, elementEl, adt[i], namespaceURI);
    }
}

function SiodemkaServiceSoapHttpPort_serialize_Paczka (xmlDoc, parentEl, adt, namespaceURI) {
    var partEl;
    if (adt.nrpp) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'nrpp');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.nrpp));
    }
    if (adt.typ) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'typ');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.typ));
    }
    if (adt.waga) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'waga');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.waga));
    }
    if (adt.gab1) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'gab1');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.gab1));
    }
    if (adt.gab2) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'gab2');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.gab2));
    }
    if (adt.gab3) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'gab3');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.gab3));
    }
    if (adt.ksztalt) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'ksztalt');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.ksztalt));
    }
    if (adt.wagaGabaryt) {
        partEl = createElementNS(xmlDoc, 'http://app.siodemka.com/mm7ws/type/', 'wagaGabaryt');
        parentEl.appendChild(partEl);
        partEl.appendChild(xmlDoc.createTextNode(adt.wagaGabaryt));
    }
}

function SiodemkaServiceSoapHttpPort_deserialize_Kontrahent(valueEl) {
    var resultsObject = {};
    for (var child=firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'ulica') {
            resultsObject.ulica = getText(child);
        }
        if (localName(child) == 'emailKontakt') {
            resultsObject.emailKontakt = getText(child);
        }
        if (localName(child) == 'imie') {
            resultsObject.imie = getText(child);
        }
        if (localName(child) == 'numer') {
            resultsObject.numer = getText(child);
        }
        if (localName(child) == 'nazwisko') {
            resultsObject.nazwisko = getText(child);
        }
        if (localName(child) == 'nrDom') {
            resultsObject.nrDom = getText(child);
        }
        if (localName(child) == 'nrLokal') {
            resultsObject.nrLokal = getText(child);
        }
        if (localName(child) == 'kod') {
            resultsObject.kod = getText(child);
        }
        if (localName(child) == 'nrExt') {
            resultsObject.nrExt = getText(child);
        }
        if (localName(child) == 'nazwa') {
            resultsObject.nazwa = getText(child);
        }
        if (localName(child) == 'telKontakt') {
            resultsObject.telKontakt = getText(child);
        }
        if (localName(child) == 'nip') {
            resultsObject.nip = getText(child);
        }
        if (localName(child) == 'czyFirma') {
            resultsObject.czyFirma = getText(child);
        }
        if (localName(child) == 'kodKraju') {
            resultsObject.kodKraju = getText(child);
        }
        if (localName(child) == 'miasto') {
            resultsObject.miasto = getText(child);
        }
        if (localName(child) == 'fax') {
            resultsObject.fax = getText(child);
        }
        if (localName(child) == 'telefonKom') {
            resultsObject.telefonKom = getText(child);
        }
        if (localName(child) == 'czyNadawca') {
            resultsObject.czyNadawca = getText(child);
        }
    }
    return resultsObject;
}

function SiodemkaServiceSoapHttpPort_deserialize_PaczkaStatus(valueEl) {
    var resultsObject = {};
    for (var child=firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'skrot') {
            resultsObject.skrot = getText(child);
        }
        if (localName(child) == 'oddSymbol') {
            resultsObject.oddSymbol = getText(child);
        }
        if (localName(child) == 'nrP') {
            resultsObject.nrP = getText(child);
        }
        if (localName(child) == 'dataS') {
            resultsObject.dataS = getText(child);
        }
        if (localName(child) == 'opis') {
            resultsObject.opis = getText(child);
        }
    }
    return resultsObject;
}

function SiodemkaServiceSoapHttpPort_deserialize_List(valueEl) {
    var resultsObject = {};
    for (var child=firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'nrPrzesylki') {
            resultsObject.nrPrzesylki = getText(child);
        }
        if (localName(child) == 'nrExt') {
            resultsObject.nrExt = getText(child);
        }
        if (localName(child) == 'mpk') {
            resultsObject.mpk = getText(child);
        }
        if (localName(child) == 'rodzajPrzesylki') {
            resultsObject.rodzajPrzesylki = getText(child);
        }
        if (localName(child) == 'placi') {
            resultsObject.placi = getText(child);
        }
        if (localName(child) == 'formaPlatnosci') {
            resultsObject.formaPlatnosci = getText(child);
        }
        if (localName(child) == 'nadawca') {
            resultsObject.nadawca = SiodemkaServiceSoapHttpPort_deserialize_KontrahentNadawca(child);
        }
        if (localName(child) == 'odbiorca') {
            resultsObject.odbiorca = SiodemkaServiceSoapHttpPort_deserialize_KontrahentOdbiorca(child);
        }
        if (localName(child) == 'platnik') {
            resultsObject.platnik = SiodemkaServiceSoapHttpPort_deserialize_KontrahentPlatnik(child);
        }
        if (localName(child) == 'uslugi') {
            resultsObject.uslugi = SiodemkaServiceSoapHttpPort_deserialize_Uslugi(child);
        }
        if (localName(child) == 'paczki') {
            resultsObject.paczki = SiodemkaServiceSoapHttpPort_deserialize_Paczki(child);
        }
        if (localName(child) == 'potwierdzenieNadania') {
            resultsObject.potwierdzenieNadania = SiodemkaServiceSoapHttpPort_deserialize_PotwierdzenieNadania(child);
        }
        if (localName(child) == 'uwagi') {
            resultsObject.uwagi = getText(child);
        }
    }
    return resultsObject;
}

function SiodemkaServiceSoapHttpPort_deserialize_KontrahentNadawca(valueEl) {
    var resultsObject = {};
    for (var child=firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'numer') {
            resultsObject.numer = getText(child);
        }
        if (localName(child) == 'nazwisko') {
            resultsObject.nazwisko = getText(child);
        }
        if (localName(child) == 'imie') {
            resultsObject.imie = getText(child);
        }
        if (localName(child) == 'telKontakt') {
            resultsObject.telKontakt = getText(child);
        }
        if (localName(child) == 'emailKontakt') {
            resultsObject.emailKontakt = getText(child);
        }
    }
    return resultsObject;
}

function SiodemkaServiceSoapHttpPort_deserialize_KontrahentOdbiorca(valueEl) {
    var resultsObject = {};
    for (var child=firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'numer') {
            resultsObject.numer = getText(child);
        }
        if (localName(child) == 'nrExt') {
            resultsObject.nrExt = getText(child);
        }
        if (localName(child) == 'czyFirma') {
            resultsObject.czyFirma = getText(child);
        }
        if (localName(child) == 'nazwa') {
            resultsObject.nazwa = getText(child);
        }
        if (localName(child) == 'nip') {
            resultsObject.nip = getText(child);
        }
        if (localName(child) == 'nazwisko') {
            resultsObject.nazwisko = getText(child);
        }
        if (localName(child) == 'imie') {
            resultsObject.imie = getText(child);
        }
        if (localName(child) == 'kodKraju') {
            resultsObject.kodKraju = getText(child);
        }
        if (localName(child) == 'kod') {
            resultsObject.kod = getText(child);
        }
        if (localName(child) == 'miasto') {
            resultsObject.miasto = getText(child);
        }
        if (localName(child) == 'ulica') {
            resultsObject.ulica = getText(child);
        }
        if (localName(child) == 'nrDom') {
            resultsObject.nrDom = getText(child);
        }
        if (localName(child) == 'nrLokal') {
            resultsObject.nrLokal = getText(child);
        }
        if (localName(child) == 'telKontakt') {
            resultsObject.telKontakt = getText(child);
        }
        if (localName(child) == 'emailKontakt') {
            resultsObject.emailKontakt = getText(child);
        }
    }
    return resultsObject;
}

function SiodemkaServiceSoapHttpPort_deserialize_KontrahentPlatnik(valueEl) {
    var resultsObject = {};
    for (var child=firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'numer') {
            resultsObject.numer = getText(child);
        }
        if (localName(child) == 'telKontakt') {
            resultsObject.telKontakt = getText(child);
        }
    }
    return resultsObject;
}

function SiodemkaServiceSoapHttpPort_deserialize_Uslugi(valueEl) {
    var resultsObject = {};
    for (var child=firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'nrBezpiecznejKoperty') {
            resultsObject.nrBezpiecznejKoperty = getText(child);
        }
        if (localName(child) == 'zkld') {
            resultsObject.zkld = getText(child);
        }
        if (localName(child) == 'zd') {
            resultsObject.zd = getText(child);
        }
        if (localName(child) == 'ubezpieczenie') {
            resultsObject.ubezpieczenie = SiodemkaServiceSoapHttpPort_deserialize_Ubezpieczenie(child);
        }
        if (localName(child) == 'pobranie') {
            resultsObject.pobranie = SiodemkaServiceSoapHttpPort_deserialize_Pobranie(child);
        }
        if (localName(child) == 'awizacjaTelefoniczna') {
            resultsObject.awizacjaTelefoniczna = getText(child);
        }
        if (localName(child) == 'potwNadEmail') {
            resultsObject.potwNadEmail = getText(child);
        }
        if (localName(child) == 'potwDostEmail') {
            resultsObject.potwDostEmail = getText(child);
        }
        if (localName(child) == 'potwDostSMS') {
            resultsObject.potwDostSMS = getText(child);
        }
        if (localName(child) == 'skladowanie') {
            resultsObject.skladowanie = getText(child);
        }
        if (localName(child) == 'nadOdbPKP') {
            resultsObject.nadOdbPKP = getText(child);
        }
        if (localName(child) == 'odbNadgodziny') {
            resultsObject.odbNadgodziny = getText(child);
        }
        if (localName(child) == 'odbWlas') {
            resultsObject.odbWlas = getText(child);
        }
        if (localName(child) == 'palNextDay') {
            resultsObject.palNextDay = getText(child);
        }
        if (localName(child) == 'osobaFiz') {
            resultsObject.osobaFiz = getText(child);
        }
        if (localName(child) == 'market') {
            resultsObject.market = getText(child);
        }
        if (localName(child) == 'zastrzDorNaGodz') {
            resultsObject.zastrzDorNaGodz = getText(child);
        }
        if (localName(child) == 'zastrzDorNaDzien') {
            resultsObject.zastrzDorNaDzien = getText(child);
        }
    }
    return resultsObject;
}

function SiodemkaServiceSoapHttpPort_deserialize_Paczki(valueEl) {
    var resultsObject = {};
    resultsObject.paczka = [];
    for (var child=firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'paczka') {
            resultsObject.paczka[resultsObject.paczka.length] = SiodemkaServiceSoapHttpPort_deserialize_Paczka(child);
        }
    }
    return resultsObject;
}

function SiodemkaServiceSoapHttpPort_deserialize_PotwierdzenieNadania(valueEl) {
    var resultsObject = {};
    for (var child=firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'dataNadania') {
            resultsObject.dataNadania = getText(child);
        }
        if (localName(child) == 'numerKuriera') {
            resultsObject.numerKuriera = getText(child);
        }
        if (localName(child) == 'podpisNadawcy') {
            resultsObject.podpisNadawcy = getText(child);
        }
    }
    return resultsObject;
}

function SiodemkaServiceSoapHttpPort_deserialize_Ubezpieczenie(valueEl) {
    var resultsObject = {};
    for (var child=firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'kwotaUbezpieczenia') {
            resultsObject.kwotaUbezpieczenia = getText(child);
        }
        if (localName(child) == 'opisZawartosci') {
            resultsObject.opisZawartosci = getText(child);
        }
    }
    return resultsObject;
}

function SiodemkaServiceSoapHttpPort_deserialize_Pobranie(valueEl) {
    var resultsObject = {};
    for (var child=firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'kwotaPobrania') {
            resultsObject.kwotaPobrania = getText(child);
        }
        if (localName(child) == 'formaPobrania') {
            resultsObject.formaPobrania = getText(child);
        }
        if (localName(child) == 'nrKonta') {
            resultsObject.nrKonta = getText(child);
        }
    }
    return resultsObject;
}

function SiodemkaServiceSoapHttpPort_deserialize_Paczka(valueEl) {
    var resultsObject = {};
    for (var child=firstChildElement(valueEl); child; child = nextSiblingElement(child)) {
        if (localName(child) == 'nrpp') {
            resultsObject.nrpp = getText(child);
        }
        if (localName(child) == 'typ') {
            resultsObject.typ = getText(child);
        }
        if (localName(child) == 'waga') {
            resultsObject.waga = getText(child);
        }
        if (localName(child) == 'gab1') {
            resultsObject.gab1 = getText(child);
        }
        if (localName(child) == 'gab2') {
            resultsObject.gab2 = getText(child);
        }
        if (localName(child) == 'gab3') {
            resultsObject.gab3 = getText(child);
        }
        if (localName(child) == 'ksztalt') {
            resultsObject.ksztalt = getText(child);
        }
        if (localName(child) == 'wagaGabaryt') {
            resultsObject.wagaGabaryt = getText(child);
        }
    }
    return resultsObject;
}

