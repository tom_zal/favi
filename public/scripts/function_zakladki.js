var zakladkiKierunek = 'right';
var zakladkiSzerokosc = 377;
var zakladkiWidoczne = 89;
var zakladkiNieWidoczne = zakladkiSzerokosc - zakladkiWidoczne;

function getZakladka(zakladka, od, load, show, tryb)
{
	if(load)
	{
		var img = '<img src="' + baseUrl + '/public/images/strona/loading.gif" alt="" />';
		var loading = '<div class="naglowek"><br/>' + img + '&nbsp; �adowanie... &nbsp;' + img + '</div>';
		$('#' + zakladka).find(".zakladkiLeft").html(loading);
	}
	$.ajax(
	{
		url: baseUrl + "/ajax/zakladki/" + zakladka,
		type: "GET",
		data: "od=" + od + '&tryb=' + tryb,
		success: function(data)
		{
			$('#' + zakladka).find(".zakladkiLeft").html(data);
			if(show) showZakladka(1000);
		}
	});
}

function showZakladka(zakladka, zakladkiKierunek, czas, hide)
{
	//console.log(zakladka + " " + zakladkiKierunek + " " + czas);
	$('.zakladka').hide();
	$('#' + zakladka).show();
	//$('#' + zakladka).css('zIndex', 9999);
	if(zakladkiKierunek == 'left')
	$('#' + zakladka).stop(true, false).animate( { left : "0" }, czas, function() 
	{
		if(hide) setTimeout('hideZakladka("' + zakladka + '", "' + zakladkiKierunek + '", ' + czas + ')', 1000);
	});
	if(zakladkiKierunek == 'right')
	$('#' + zakladka).stop(true, false).animate( { right : "0" }, czas, function() 
	{
		if(hide) setTimeout('hideZakladka("' + zakladka + '", "' + zakladkiKierunek + '", ' + czas + ')', 1000);
	});
}

function hideZakladka(zakladka, zakladkiKierunek, czas)
{
	$('.zakladka').show();
	//$('#' + zakladka).css('zIndex', 1);
	if(zakladkiKierunek == 'left')
	$('#' + zakladka).stop(true, false).animate( {left : "-" + zakladkiNieWidoczne }, czas);
	if(zakladkiKierunek == 'right')
	$('#' + zakladka).stop(true, false).animate( {right : "-" + zakladkiNieWidoczne }, czas);
}

function menuZakladki()
{
	var menuslide = false;
	jQuery("#jQ-menu").find('a').each(function()
	{
		var $class = this.className;
		jQuery('.'+$class).next().each(function()
		{
			jQuery('.'+$class).css('width', jQuery(this).innerWidth()+'px');
			var active = jQuery('.'+$class).parent().hasClass('active');
			if(active == false)
			{
				if(menuslide) jQuery(this).addClass('noactive');
				jQuery(this).css('position','relative');
				if(!menuslide) jQuery(this).css('bottom','0');
			}
		});
	});
	
	/*MENU LOJALNOSC PODSWIETLENIE ELEMENTOW*/
	jQuery("#jQ-menu a").hover
	(
		function()
		{
			if(menuslide)jQuery(this).next().removeClass('noactive');
			jQuery(this).next().animate({"bottom": '-15'}, "medium");
			jQuery(this).next().animate({"bottom": 0}, "medium");
		},
		function()
		{
			var active = jQuery(this).parent().hasClass('active');
			if(active == false)
			{
				if(menuslide)jQuery(this).next().addClass('noactive');
				if(menuslide)jQuery(this).next().animate({"bottom": "-15"}, "slow");
			}
		}
	);
}

$(document).ready(function()
{
	$(".zakladka").each(function()
	{
		$(this).hover
		(
			function()
			{
				showZakladka($(this).attr('id'), $(this).attr('data-val-kier'), 1000, false);
				//alert($(this).attr('id'));
			},
			function()
			{
				hideZakladka($(this).attr('id'), $(this).attr('data-val-kier'), 1000);
			}
		)
	});
});