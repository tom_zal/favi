
function start(firma, zaznacz)
{
	if(firma==1)
	{
		Firma();
	}	
	else
	{
		Prywatna();
	}
	if(zaznacz)
	{
		Brak();
	}
	else
	{
		
		Alternatywny();
	}
}

function Prywatna()
{
	document.kontrahent.nazwa.disabled=true;
	if(!obConfig['nipOsobaPrywatna'])
	document.kontrahent.nip.disabled=true;
	document.kontrahent.regon.disabled=true;
	document.kontrahent.pkd.disabled=true;
	document.kontrahent.krs.disabled=true;	
	document.kontrahent.pesel.disabled=false;
	document.kontrahent.plec.disabled=false;
	document.kontrahent.nazwa.style.background='#d6d7d8';
	if(!obConfig['nipOsobaPrywatna'])
	document.kontrahent.nip.style.background='#d6d7d8';
	document.kontrahent.regon.style.background='#d6d7d8';
	document.kontrahent.pkd.style.background='#d6d7d8';
	document.kontrahent.krs.style.background='#d6d7d8';
	document.kontrahent.pesel.style.background='#d6d7d8';
}
function Firma()
{
	document.kontrahent.nazwa.style.background='white';
	document.kontrahent.nip.style.background='white';
	document.kontrahent.regon.style.background='white';
	document.kontrahent.pkd.style.background='white';
	document.kontrahent.krs.style.background='white';
	document.kontrahent.pesel.style.background='white';
	document.kontrahent.nazwa.disabled=false;
	document.kontrahent.nip.disabled=false;	
	document.kontrahent.regon.disabled=false;
	document.kontrahent.pkd.disabled=false;
	document.kontrahent.krs.disabled=false;
	document.kontrahent.pesel.disabled=true;
	document.kontrahent.plec.disabled=true;	
}
function Alternatywny()
{
	document.kontrahent.nazwaAlternatywny.disabled=true;
	document.kontrahent.nrAlternatywny.disabled=true;
	document.kontrahent.miejscowoscAlternatywny.disabled=true;
	document.kontrahent.ulicaAlternatywny.disabled=true;
	document.kontrahent.kodAlternatywny.disabled=true;

	
	document.kontrahent.nazwaAlternatywny.style.background='#d6d7d8';
	document.kontrahent.nrAlternatywny.style.background='#d6d7d8';
	document.kontrahent.miejscowoscAlternatywny.style.background='#d6d7d8';
	document.kontrahent.ulicaAlternatywny.style.background='#d6d7d8';
	document.kontrahent.kodAlternatywny.style.background='#d6d7d8';
}
function Brak()
{
	document.kontrahent.nazwaAlternatywny.disabled=false;
	document.kontrahent.nrAlternatywny.disabled=false;
	document.kontrahent.miejscowoscAlternatywny.disabled=false;
	document.kontrahent.ulicaAlternatywny.disabled=false;
	document.kontrahent.kodAlternatywny.disabled=false;
	document.kontrahent.nazwaAlternatywny.style.background='white';
	document.kontrahent.nrAlternatywny.style.background='white';
	document.kontrahent.miejscowoscAlternatywny.style.background='white';
	document.kontrahent.ulicaAlternatywny.style.background='white';
	document.kontrahent.kodAlternatywny.style.background='white';
}
function Zmiana(wartosc)
{
	if(wartosc == true)
	{
		Brak();
	}
	else
	{
		Alternatywny();
	}
}