function htmlspecialchars(p_string) 
{
	p_string = p_string.replace(/&/g, '&amp;');
	p_string = p_string.replace(/</g, '&lt;');
	p_string = p_string.replace(/>/g, '&gt;');
	p_string = p_string.replace(/"/g, '&quot;');
	p_string = p_string.replace(/'/g, '&#039;');
	return p_string;
};
function htmlspecialchars_decode(p_string) 
{
	p_string = p_string.replace(/&amp;/g, '&');
	p_string = p_string.replace(/&lt;/g, '<');
	p_string = p_string.replace(/&gt;/g, '>');
	p_string = p_string.replace(/&quot;/g, '"');
	p_string = p_string.replace(/&#039;/g, "'");
	return p_string;
};

if(typeof String.prototype.trim !== 'function')
{
	String.prototype.trim = function()
	{
		return this.replace(/^\s+|\s+$/g, ''); 
	}
}

function number_format(number, decimals, dec_point, thousands_sep)
{
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
	prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
	sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
	dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
	s = '',
	toFixedFix = function (n, prec)
	{
		var k = Math.pow(10, prec);
		return '' + Math.round(n * k) / k;
	};
	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3)
	{
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec)
	{
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
}

function selectboxheightfix(ten) 
{
	var size = $(ten).parent().find('.sbOptions').children('li').length;
	var height = $(ten).parent().find('.sbOptions').children('li').first().height();
	var heightOld = parseInt($(ten).parent().find('.sbOptions').css('max-height'));
	var heightNew = 8;//sbOptions border
	$(ten).parent().find('.sbOptions').children('li').each(function(key, val)
	{
		heightNew += parseInt($(ten).height());
		//heightNew++; //sbOptions a border dotted
		//heightNew+=2; //sbOptions a padding
		//alert($(ten).height());
	});
	var scrollTop = $(window).scrollTop();
	var offsetTop = $(ten).parent().offset().top;
	var windowHeight = $(window).height();
	var maxHeight = windowHeight - scrollTop;
	//alert(heightNew + ' ' + scrollTop + ' ' + offsetTop + ' ' + windowHeight + ' ' + maxHeight);
	if(heightNew > offsetTop && heightNew > maxHeight) heightNew = offsetTop;
	//var top = parseInt(size) * parseInt(height);		
	if(heightOld < heightNew && heightNew > maxHeight)
	{
		$(ten).parent().find('.sbOptions').css('top', -heightNew);
		$(ten).parent().find('.sbOptions').css('max-height', heightNew);
	}
};

function szukajWojew(ten, name)
{
	if(ten.value.length < 3)
	{
		$('#' + name).html('<option value="0">Wpisz co najmniej 3 znaki!</option>');
	}
	else
	$.ajax(
	{
		url: baseUrl+'/ajax/index/szukajwojew/q/'+encodeURIComponent(ten.value),
		cache: false,
		timeout : 5000,
		error: function(jqXHR, textStatus, errorThrown)
		{
			$('#' + name).html('<option value="0">'+errorThrown+'</option>');
		},
		success: function(html, textStatus, jqXHR)
		{
			var xml = $.parseXML(html);
			var ile = $(xml).find('wojew').length;
			$('#' + name).html('');
			if(ile > 0)
			{
				$(xml).find('wojew').each(function()
				{
					var id = $(this).find('id').text();
					var nazwa = $(this).find('nazwa').text();
					$('#' + name).append('<option value="'+nazwa+'">'+nazwa+'</option>');
				});
			}
		}
	});
}

var popupTimeout = null;
function popup(message, timeout)
{
	if (timeout == undefined) timeout = 3000;
	//$.jGrowl(data, { life: timeout, position: 'center' });
	$('#popupMessage').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 250,
		height: "auto",
		minWidth: 250,
		minHeight: 100,
		title: "Komunikat",
		resizable: false,
		//dialogClass: 'popupMessage',
		position: ['center', 'center'],
		open: function()
		{
			$('#popupMessage').html(message)
			//popupTimeout = setTimeout('$("#popupMessage").dialog("close")', timeout);
		},
		close:function()
		{
			//clearTimeout(popupTimeout);
			$('#popupMessage').html('');
		}
	});
}
	
function appendRowLista($this, typ)
{
    var checked = $($this).attr('checked');
    if(true)
    {
        var ar = $this.value.split('|--|');
        var hidden = '<input type="hidden" id="'+typ+ar[0]+'" name="'+typ+'['+ar[0]+']" value="'+ar[0]+'" />';
		if(checked != undefined && $('#'+typ+ar[0]).attr('id') == undefined)
        $('#jQ_client_'+typ).append('<span class="db cb lf"><span class="lf"><b>'+ar[1]+'</b>&nbsp;&nbsp;</span><a onclick="return removeRow(this);" title="usuń" class="lf del_ico silver"></a>'+hidden+'</span>');
		if(checked == undefined && $('#'+typ+ar[0]).attr('id') != undefined)
		$('#'+typ+ar[0]).parent().remove();
    }
}
function appendRowAllLista($this, typ, check)
{
    jQuery('#'+$this).find('input[type="checkbox"]').each(function()
	{
        var checked = $(this).attr('checked');
        if(true)
        {
            var ar = this.value.split('|--|');
            $(this).attr('checked', check);
			var hidden = '<input type="hidden" id="'+typ+ar[0]+'" name="'+typ+'['+ar[0]+']" value="'+ar[0]+'" />';
			if(check && $('#'+typ+ar[0]).attr('id') == undefined)
			$('#jQ_client_'+typ).append('<span class="db cb lf"><span class="lf"><b>'+ar[1]+'</b>&nbsp;&nbsp;</span><a onclick="return removeRow(this);" title="usuń" class="lf del_ico silver"></a>'+hidden+'</span>');
			if(!check && $('#'+typ+ar[0]).attr('id') != undefined)
			$('#'+typ+ar[0]).parent().remove();
        }
    });
}

function mbstrlen(str)
{
    var len = 0;
    for(var i = 0; i < str.length; i++)
	{
        len += str.charCodeAt(i) < 0 || str.charCodeAt(i) > 255 ? 2 : 1;
    }
    return len;
}

$(document).ready(function() 
{	
	$("#co").autocomplete(
	{
		disabled : false,
		autoFocus : false,
		delay : 300,
		minLength : 2,
		source : baseUrl+'/ajax/index/autocomplete',
		select: function(event, ui) 
		{
			$('#co').val(ui.item.value);
			$('#szukaj').click(); 
		}
	});
	
	if(false)
	$("#polecEmoc").emoticate(
	{
		icon: 'smiley-icon', replacediv: 'list'
	});
	
	$("#data").datepicker(
	{
		changeYear: true,
		yearRange: '2010:c',
		//altField: '#boksWystawData',
		altFormat: 'yy-mm-dd',
		dateFormat: 'yy-mm-dd',
		//minDate: new Date(),
		//maxDate: new Date(),
		showOn: "button",
		buttonImage: baseUrl + "/public/images/admin/ikony/calendar.gif",
		buttonImageOnly: true
	});
	$(".dataOdDo").datepicker(
	{
		changeYear: true,
		yearRange: '2010:<?php echo date("Y");?>',
		//altField: '#boksWystawData',
		altFormat: 'yy-mm-dd',
		dateFormat: 'yy-mm-dd',
		//minDate: new Date(),
		//maxDate: new Date(),
		showOn: "button",
		buttonImage: baseUrl + "/public/images/admin/ikony/calendar.gif",
		buttonImageOnly: true
	});
	
	$('.colorSelector').each(function()
	{
		$(this).ColorPicker(
		{
			flat : false,
			livePreview : true,
			color: '#' + $(this).find('input[type=hidden]').val(),
			onShow: function(colpkr) 
			{
				$(colpkr).fadeIn(500);
				return false;
			},
			onBeforeShow: function(colpkr) 
			{
				return false;
			},
			onHide: function(colpkr) 
			{
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function(hsb, hex, rgb) 
			{
				return false;
			},
			onSubmit: function(hsb, hex, rgb, el) 
			{
				$(el).css('backgroundColor', '#' + hex);
				$(el).children('input').val(hex);
				$(el).ColorPickerHide();
			}
		});
	});
	
	//$('#wykres').visualize({type: 'line', height: '300px', width: '800px'});
	
	if(false)
	$('.zadania').selectbox(
	{
		onChange: function (val, inst) 
		{
			if(val != '' && val != 'wiecej')
			ofertaListaOpcjeAllWiecej($(this), baseUrl);
		},
		onOpen: function (inst) 
		{
			selectboxheightfix($(this));
		},
		onClose: function (inst) 
		{
			//var option = $("select[name=myselect]").find("option[value="+myvalue+"]");
			//$("select[name=myselect]").selectbox("change", option.attr('value'), option.html() );
			//$('.zadania').selectbox("change", 'wiecej', 'Operacje hurtowe');
		}
	});
	$(document).click(function()
	{
		//$('.zadania').selectbox('close');
	});
	$('.sbHolder').click(function(event)
	{
		event.stopPropagation();
	});
});