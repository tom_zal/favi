function zmienStan(id, stan, prodID)
{
	$('#rozm' + id).replaceWith('<input type="text" name="zmienStan[' + id + ']" size="2" style="border:1px solid #E3E3E3;" value="' + stan + '" />');
	$('#submitRozm' + prodID).show();
}
function zmienWidocznosc(ten, id, widok)
{
	//console.log(widok);
	$('#loader').show();
	//if(!edycjazablokowana)
	$.ajax(
	{
		url: baseUrl+'/ajax/index/zmianawidoczny/prod/' + id + '/widok/' + (widok ? 1 : 0),
		cache: false,
		timeout: 5000,
		error: function(jqXHR, textStatus, errorThrown)
		{
			$('#loader').hide();
			popup('Błąd zmiany widoczności: ' + errorThrown);			
		},
		success: function(html, textStatus, jqXHR)
		{
			//console.log(widok);
			$('#loader').hide();
			if(html == "1")
			{
				popup('Zapisano');
				if(widok) $(ten).attr('checked', 'checked');
				else $(ten).removeAttr('checked');
			}
			else
			{
				popup('Błąd odpowiedzi z serwera. Odśwież stronę aby zobaczyć czy zmiany zostały zapisane.');
			}
		}
	});
	return false;
}
function zmianaNazwy(id, ten, edit)
{
	if(edycjazablokowana) edit = false;
	if(edit)
	{
		$('#loader').show();
		$.ajax(
		{
			url: baseUrl + '/ajax/index/zmiananazwy/prod/' + id,
			cache: false,
			type : 'POST',
			timeout: 5000,
			data : 'nazwa=' + encodeURIComponent(ten.value),
			error: function(jqXHR, textStatus, errorThrown)
			{
				$('#loader').hide();
				$('#nazwa' + id).hide();
				$('#link' + id).show();
				$('#edit' + id).show();
				popup('Błąd zmiany nazwy: ' + errorThrown);
				return false;
			},
			success: function(html, textStatus, jqXHR)
			{
				$('#loader').hide();
				//var nazwa = htmlspecialchars_decode(html);
				if(html == "OK")
				{
					var nazwa = $('#nazwa' + id).val();
					$('#link' + id + ' b').text(nazwa);
					//$('#nazwa' + id).val(nazwa);
					//$(ten).hide();
					popup('Zapisano nazwę:<br/>' + nazwa);
				}
				else
				{
					popup('Błąd odpowiedzi z serwera. Odśwież stronę aby zobaczyć czy zmiany zostały zapisane.');
				}
				$('#nazwa' + id).hide();
				$('#link' + id).show();
				$('#edit' + id).show();
			}
		});
	}
	else
	{
		$('#nazwa' + id).hide();
		$('#link' + id).show();
		$('#edit' + id).show();
	}
}
function zmianaCeny(id, ten)
{
	if(edycjazablokowana)
	{
		$('#price' + id).hide();
		$('#cena' + id).show();
		return;
	}
	$('#loader').show();
	$.ajax(
	{
		url: baseUrl + '/ajax/index/zmianaceny/prod/' + id + '/cena/' + ten.value,
		cache: false,
		timeout: 5000,
		error: function(jqXHR, textStatus, errorThrown)
		{
			$('#loader').hide();
			$('#price' + id).hide();
			$('#cena' + id).show();
			popup('Błąd zmiany ceny: ' + errorThrown);
			return false;
		},
		success: function(html, textStatus, jqXHR)
		{
			$('#loader').hide();
			if(html == "0.00")
			{
				$('#cena' + id).text("0");
				popup('Zapisano cenę zerową');
				//$('#price' + id).text("0");
			}
			else if(parseFloat(html) > 0)
			{
				$('#cena' + id).text(html);
				popup('Zapisano cenę: ' + html);
				//$('#price' + id).text(html);
			}
			else
			{
				popup('Błąd odpowiedzi z serwera. Odśwież stronę aby zobaczyć czy zmiany zostały zapisane.');
			}
			$('#price' + id).hide();
			$('#cena' + id).show();
		}
	});
}

function zmianaSztuk(id, ten)
{
	if(edycjazablokowana)
	{
		$('#stan' + id).hide();
		$('#ile' + id).show();
		return;
	}
	$('#loader').show();
	$.ajax(
	{
		url: baseUrl + '/ajax/index/zmianasztuk/prod/' + id + '/sztuk/' + ten.value,
		cache: false,
		timeout: 5000,
		error: function(jqXHR, textStatus, errorThrown)
		{
			$('#loader').hide();
			$('#stan' + id).hide();
			$('#ile' + id).show();
			popup('Błąd zmiany ilości: ' + errorThrown);
			return false;
		},
		success: function(html, textStatus, jqXHR)
		{
			var ile = parseInt($('#ile' + id).text());
			var stan = parseInt($('#stan' + id).text());
			$('#loader').hide();
			if(html == "0")
			{
				$('#ile' + id).text("0");
				var msg = 'Zapisano stan zerowy';
				if(false && ile > 0)
				{
					msg = msg + ', wyłączono widoczność';
					$('#widoczny' + id).removeAttr('checked');
				}
				popup(msg);
			}
			else if(parseInt(html) > 0)
			{
				$('#ile' + id).text(html);
				var msg = 'Zapisano stan:' + html;
				if(false && ile == 0)
				{
					msg = msg + ', włączono widoczność';
					$('#widoczny' + id).attr('checked', 'checked');
				}
				popup(msg);
			}
			else
			{
				popup('Błąd odpowiedzi z serwera. Odśwież stronę aby zobaczyć czy zmiany zostały zapisane.');
			}
			$('#stan' + id).hide();
			$('#ile' + id).show();
		}
	});
}

var facebookProduktID = 0;
function ofertaListaOpcjeAllWiecej(ten, url)
{
	var akcja = '';
	var uwaga = '';
	var val = $(ten).val();
	if(val == '')
	{
		$(ten).val('wiecej');
		$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		return;
	}
	if(val == 'wiecej') return;
	var ileZazn = $('.checkbox:checked').length;
	var ileAll = 0;//<?php echo isset($this->ileProd) ? $this->ileProd : 0; ?>;
	var tryb = 'zadania_wybrane';//$(ten).attr('name') == 'opcje3' ? 'wyszukiwarka' : 'zadania_wybrane';
	var ile = tryb == 'wyszukiwarka' ? ileAll : ileZazn;
	var czego = '';
	
	if(ile == 0)
	{
		if(tryb == 'zadania_wybrane') alert('Nie zaznaczono żadnych produktów!');
		if(tryb == 'wyszukiwarka') alert('Na liście nie ma produktów!');
		$(ten).val('wiecej');
		$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		return;
	}
	
	switch(val)
	{
		case 'kopiuj' : 
		akcja = 'admin/oferta/powiel/tryb/kopia/ids'; 
		uwaga = 'Czy na pewno chcesz skopiować zaznaczone produkty ?';
		czego = 'kopiowania';
		break;
		case 'powiel' : 
		akcja = 'admin/oferta/powiel/tryb/langs/ids'; 
		//uwaga = 'Czy na pewno chcesz powielić zaznaczone produkty ?';
		//czego = 'powielenia';
		break;
		case 'przypisz' : 
		akcja = 'admin/oferta/lista/przypisz/1'; 
		uwaga = 'Czy na pewno chcesz dodać zaznaczonym produktom wybrane zdjęcie produktu '+$('#przypisz').val()+' ?';
		czego = 'przypisania wybranego zdjęcia';
		break;
		case 'przypisz_allegro' : 
		akcja = 'admin/oferta/lista/przypisz/1/allegro/1'; 
		uwaga = 'Czy na pewno chcesz dodać zaznaczonym produktom wybrane zdjęcie produktu '+$('#przypisz').val()+' jako zdjęcie Allegro ?';
		czego = 'przypisania wybranego zdjęcia';
		break;
		case 'ustaw_glowne' : 
		akcja = 'admin/oferta/lista/przypisz/1/glowne/1'; 
		uwaga = 'Czy na pewno chcesz wybrane zdjęcie produktu '+$('#przypisz').val()+' ustawić jako zdjęcie główne zaznaczonym produktom ?';
		czego = 'przypisania wybranego zdjęcia';
		break;
		case 'ustaw_glowne_usun' : 
		akcja = 'admin/oferta/lista/przypisz/1/glowne/1/usun/1'; 
		uwaga = 'Czy na pewno chcesz wybrane zdjęcie produktu '+$('#przypisz').val()+' ustawić jako zdjęcie główne zaznaczonym produktom i usunąć ich pozostałe zdjęcia ?';
		czego = 'przypisania wybranego zdjęcia';
		break;
		case 'ustaw_glowne_allegro' : 
		akcja = 'admin/oferta/lista/przypisz/1/glowne/1/allegro/1'; 
		uwaga = 'Czy na pewno chcesz wybrane zdjęcie produktu '+$('#przypisz').val()+' ustawić jako zdjęcie główne Allegro zaznaczonym produktom ?';
		czego = 'przypisania wybranego zdjęcia';
		break;
		case 'ustaw_glowne_usun_allegro' : 
		akcja = 'admin/oferta/lista/przypisz/1/glowne/1/usun/1/allegro/1'; 
		uwaga = 'Czy na pewno chcesz wybrane zdjęcie produktu '+$('#przypisz').val()+' ustawić jako zdjęcie główne Allegro zaznaczonym produktom i usunąć ich pozostałe zdjęcia Allegro ?';
		czego = 'przypisania wybranego zdjęcia';
		break;
		case 'powiaz' : 
		akcja = 'admin/oferta/lista/powiazane/1'; 
		uwaga = 'Czy na pewno chcesz powiązać zaznaczone produkty z '+$('#powiaz').val()+' ?';
		czego = 'powiązania';
		break;
		case 'powiaz_wybierz' : 
		akcja = 'admin/oferta/lista/powiaz_wybierz/1'; 
		uwaga = 'Czy na pewno chcesz wybrać zaznaczone produkty do późniejszego powiązania ?';
		czego = 'powiązania';
		break;
		case 'powiaz_multi' : 
		akcja = 'admin/oferta/lista/powiaz_multi/1'; 
		uwaga = 'Czy na pewno chcesz powiązać zaznaczone produkty z wcześniej wybranymi ?';
		czego = 'powiązania';
		break;
		case 'powiaz_multi_del' : 
		akcja = 'admin/oferta/lista/powiaz_multi_del/1'; 
		uwaga = 'Czy na pewno chcesz usunąć powiązania zaznaczonych produktów z wcześniej wybranymi ?';
		czego = 'powiązania';
		break;
		case 'facebook' : 
		akcja = 'admin/oferta/lista/facebook/all'; 
		break;
		case 'facebook_add' : 
		akcja = 'admin/oferta/lista/facebook/1'; 
		uwaga = 'Czy na pewno chcesz dodać posty z zaznaczonych produktów do konta Facebook ?';
		czego = 'dodania postów na konto Facebook';
		break;
		case 'facebook_del' : 
		akcja = 'admin/oferta/lista/facebook/0'; 
		uwaga = 'Czy na pewno chcesz usunąć posty z zaznaczonych produktów z konta Facebook ?';
		czego = 'usunięcia postów z konta Facebook';
		break;
		case 'allegro' : 
		akcja = 'admin/allegroprodukt/wystawprod/tryb/produkty'; 
		//uwaga = 'Czy na pewno chcesz wystawić na Allegro zaznaczone produkty ?';
		//czego = 'wystawienia na Allegro';
		break;
		case 'usun_allegro' : 
		akcja = 'admin/oferta/lista/delnrs'; 
		uwaga = 'Czy na pewno chcesz usunąć aukcje zaznaczonych produktów ?';
		czego = 'usunięcia aukcji';
		break;
		case 'usun' : 
		akcja = 'admin/oferta/lista/delids'; 
		uwaga = 'Czy na pewno chcesz usunąć zaznaczone produkty ?';
		czego = 'usunięcia';
		break;
		case 'ukryj' : 
		akcja = 'admin/oferta/lista/widoczny/0';
		uwaga = 'Czy na pewno chcesz ukryć zaznaczone produkty ?';
		czego = 'ukrycia';
		break;
		case 'odkryj' : 
		akcja = 'admin/oferta/lista/widoczny/1';
		uwaga = 'Czy na pewno chcesz odkryć zaznaczone produkty ?';
		czego = 'odkrycia';
		break;
		case 'jedn_miary_zb_czy_0' : 
		akcja = 'admin/oferta/lista/jedn_miary_zb_czy/0';
		uwaga = 'Czy na pewno chcesz ustawić zamówienia składane na sztuki zaznaczonym produktom ?';
		czego = 'ustawienia zamówień składanych na sztuki ';
		break;
		case 'jedn_miary_zb_czy_1' : 
		akcja = 'admin/oferta/lista/jedn_miary_zb_czy/1';
		uwaga = 'Czy na pewno chcesz ustawić zamówienia składane na opakowania zbiorcze zaznaczonym produktom ?';
		czego = 'ustawienia zamówień składanych na opakowania zbiorcze ';
		break;
		case 'jedn_miary_zb_typ_0' : 
		akcja = 'admin/oferta/lista/jedn_miary_zb_typ/0';
		uwaga = 'Czy na pewno chcesz ustawić zamówienia składane na opakowania zbiorcze tylko w całości zaznaczonym produktom ?';
		czego = 'ustawienia zamówień składanych na opakowania zbiorcze tylko w całości';
		break;
		case 'jedn_miary_zb_typ_1' : 
		akcja = 'admin/oferta/lista/jedn_miary_zb_typ/1';
		uwaga = 'Czy na pewno chcesz ustawić zamówienia składane na opakowania zbiorcze również w częściach zaznaczonym produktom ?';
		czego = 'ustawienia zamówień składanych na opakowania zbiorcze również w częściach';
		break;
		case 'negocjuj_cene_0' : 
		akcja = 'admin/oferta/lista/negocjuj_cene/0';
		uwaga = 'Czy na pewno chcesz ustawić "negocjuj cene" zaznaczonym produktom ?';
		czego = 'ustawienia "negocjuj cene" ';
		break;
		case 'negocjuj_cene_1' : 
		akcja = 'admin/oferta/lista/negocjuj_cene/1';
		uwaga = 'Czy na pewno chcesz usunąć "negocjuj cene" zaznaczonym produktom ?';
		czego = 'usunięcia "negocjuj cene" ';
		break;
		case 'glowna_1' : 
		akcja = 'admin/oferta/lista/glowna/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Główna" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Główna" ';
		break;
		case 'glowna_0' : 
		akcja = 'admin/oferta/lista/glowna/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Główna" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Główna" ';
		break;
		case 'nowosc_1' : 
		akcja = 'admin/oferta/lista/nowosc/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Nowość" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Nowość" ';
		break;
		case 'nowosc_0' : 
		akcja = 'admin/oferta/lista/nowosc/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Nowość" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Nowość" ';
		break;
		case 'promocja_1' : 
		akcja = 'admin/oferta/lista/promocja/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Promocja" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Promocja" ';
		break;
		case 'promocja_0' : 
		akcja = 'admin/oferta/lista/promocja/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Promocja" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Promocja" ';
		break;
		case 'promocja_net_1' : 
		akcja = 'admin/oferta/lista/promocja_net/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Promocja Internetowa" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Promocja Internetowa" ';
		break;
		case 'promocja_net_0' : 
		akcja = 'admin/oferta/lista/promocja_net/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Promocja Internetowa" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Promocja Internetowa" ';
		break;
		case 'polecane_1' : 
		akcja = 'admin/oferta/lista/polecane/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Polecane" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Polecane" ';
		break;
		case 'polecane_0' : 
		akcja = 'admin/oferta/lista/polecane/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Polecane" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Polecane" ';
		break;
		case 'produkt_dnia_1' : 
		akcja = 'admin/oferta/lista/produkt_dnia/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Produkt Dnia" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Produkt Dnia" ';
		break;
		case 'produkt_dnia_0' : 
		akcja = 'admin/oferta/lista/produkt_dnia/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Produkt Dnia" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Produkt Dnia" ';
		break;
		case 'wyprzedaz_1' : 
		akcja = 'admin/oferta/lista/wyprzedaz/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Wyprzedaż" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Wyprzedaż" ';
		break;
		case 'wyprzedaz_0' : 
		akcja = 'admin/oferta/lista/wyprzedaz/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Wyprzedaż" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Wyprzedaż" ';
		break;
		case 'specjalne_1' : 
		akcja = 'admin/oferta/lista/specjalne/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Specjalne" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Specjalne" ';
		break;
		case 'specjalne_0' : 
		akcja = 'admin/oferta/lista/specjalne/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Specjalne" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Specjalne" ';
		break;
		case 'kategorie' : 
		akcja = 'admin/oferta/lista/zmienkat'; 
		//uwaga = 'Czy na pewno chcesz zmienić kategorie zaznaczonym produktom ?';
		czego = 'zmiany kategorii';
		break;
		case kategorieInne : 
		akcja = 'admin/oferta/lista/zmienkat'; 
		//uwaga = 'Czy na pewno chcesz zmienić ' + kategorieInne + ' zaznaczonym produktom ?';
		czego = 'zmiany ' + kategorieInne;
		break;
		case 'producent' : 
		akcja = 'admin/oferta/lista/zmienprod'; 
		//uwaga = 'Czy na pewno chcesz zmienić producenta zaznaczonym produktom ?';
		czego = 'zmiany producenta';
		break;
		case 'grupa' : 
		akcja = 'admin/oferta/lista/zmiengrupa'; 
		//uwaga = 'Czy na pewno chcesz zmienić grupę zaznaczonym produktom ?';
		czego = 'zmiany grupy';
		break;
		case 'rabat' : 
		akcja = 'admin/oferta/lista/zmienrabat'; 
		//uwaga = 'Czy na pewno chcesz zmienić rabat zaznaczonym produktom ?';
		czego = 'zmiany rabatu';
		break;
		case 'ceny' : 
		case 'ceny_hurt' :
		case 'ceny_allegro' :
		akcja = 'admin/oferta/lista/zmienceny'; 
		//uwaga = 'Czy na pewno chcesz zmienić ceny zaznaczonym produktom ?';
		czego = 'zmiany ceny';
		break;
		default : 
		akcja = 'admin/oferta/lista';
	}
	
	//uwaga = '';
	//if(czego.length > 0) uwaga = 'Przygotowano do ' + czego + ' ' + ile + ' produkty(ów)';
	//$('#zadaniaTryb').val(tryb);
	//$('#opcje3').val('wiecej');
	//$('#zadaniaAkcja').val(tryb == 'wyszukiwarka' ? val : 'wiecej');
	
	$('#wystawForm').attr('action', url + '/' + akcja + '/all');
	if(uwaga.length == 0 || window.confirm(uwaga))
	{
		if(val == 'producent') ProducentWindow(tryb, ile, ten);
		else if(val == 'grupa') GrupaWindow(tryb, ile, ten);
		else if(val == 'kategorie') KategoriaWindow(tryb, ile, ten);
		else if(val == kategorieInne) KategoriaInneWindow(tryb, ile, ten);
		else if(val == 'ceny') ZmianaCenyWindow(tryb, ile, ten);
		else if(val == 'ceny_hurt') ZmianaCenyHurtWindow(tryb, ile, ten);
		else if(val == 'ceny_allegro') ZmianaCenyAllegroWindow(tryb, ile, ten);
		else if(val == 'rabat') ZmianaRabatWindow(tryb, ile, ten);
		else if(val == 'powiel') PowielWindow(tryb, ile, ten);
		else if(val == 'opis') OpisWindow(tryb, ile, ten);
		else if(val == 'opis_allegro') OpisAllegroWindow(tryb, ile, ten);
		else if(val == 'atrybuty') AtrybutyWindow(tryb, ile, ten);
		else if(val == 'facebook') FacebookMultiWindow(ten);
		//else if(val.substr(0, 7) == 'allegro') WystawBoksWindow(tryb, ile, ten, akcja, val);
		else $('#wystawForm').submit();
	}
	else
	{
		$(ten).val('wiecej');
		$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
	}
}

function ProducentWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#producentWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 200,
		height: 130,
		title: "Wybierz producenta",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#producent').val('0');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$('#producent').val('0');
				$('#producentOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#producentOK').val($('#producent').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			
		}
	});
}
function GrupaWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#grupaWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 200,
		height: 130,
		title: "Wybierz grupę",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#grupa').val('0');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$('#grupa').val('0');
				$('#grupaOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#grupaOK').val($('#grupa').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			
		}
	});
}
function AtrybutyWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#atrybutWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 500,
		height: $(window).height() - 100,
		title: "Ustaw atrybuty",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#dodajAtrybut').val('');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$('#dodajAtrybut').val('');
				$('#atrybutyOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				//alert($('#atrybuty :input').serialize());
				$('#atrybutyOK').val($('#atrybuty :input').serialize());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			
		}
	});
}
function KategoriaWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#kategorieWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 340,
		height: 250,
		title: "Wybierz kategorie",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#kategorieWindow select').val('-1');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$('#kategorieWindow select').val('-1');
				$('#kategorieOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#kategorieOK').val($('#kategorieWindow select').serialize());
				//alert($('#kategorieOK').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			
		}
	});
}
function KategoriaInneWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#kategorieInneWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 340,
		height: 250,
		title: "Wybierz " + kategorieInne,
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#kategorieInneWindow select').val('-1');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$('#kategorieInneWindow select').val('-1');
				$('#kategorieInneOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#kategorieInneOK').val($('#kategorieInneWindow select').serialize());
				//alert($('#kategorieInneOK').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			
		}
	});
}
function ZmianaRabatWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#rabatyWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 200,
		height: 130,
		title: "Wpisz rabat",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#rabat').val('');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$('#rabat').val('');
				$('#rabatOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#rabatOK').val($('#rabat').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			
		}
	});
}
function ZmianaCenyWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#CenyWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 200,
		height: 130,
		title: "Wpisz cenę",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#cena').val('');
		},
		buttons: 
		{
			'Cancel': function() 
			{
				$('#cena').val('');
				$('#cenaOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#cenaOK').val($('#cena').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{

		}
	});
}
function ZmianaCenyHurtWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#CenyHurtWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 200,
		height: 130,
		title: "Wpisz cenę hurtową",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#cenaHurt').val('');
		},
		buttons: 
		{
			'Cancel': function() 
			{
				$('#cenaHurt').val('');
				$('#cenaHurtOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#cenaHurtOK').val($('#cenaHurt').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{

		}
	});
}
function ZmianaCenyAllegroWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#CenyAllegroWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 200,
		height: 130,
		title: "Wpisz cenę Allegro",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#cenaAllegro').val('');
		},
		buttons: 
		{
			'Cancel': function() 
			{
				$('#cenaAllegro').val('');
				$('#cenaAllegroOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#cenaAllegroOK').val($('#cenaAllegro').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{

		}
	});
}
function PowielWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#powielWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 250,
		height: 130,
		title: "Wybierz język",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#lang').val('');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$('#lang').val('');
				$('#langOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#langOK').val($('#lang').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			
		}
	});
}
var editorInit = true;
var instance = "tekst";
function OpisWindow(tryb, ile, ten)
{
	if(editorInit)
	for(name in CKEDITOR.instances)
    {
        CKEDITOR.instances[name].destroy();
		editorInit = false;
    }
	$('#opisWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 880,
		height: 520,
		title: "Wpisz opis",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			instance = "tekst";
			//$('#opis').val('');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				//$('#opis').val('');
				$('#opisOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#opisOK').val(CKEDITOR.instances[instance].getData());
				$('#wystawForm').submit();
			}
		},
		beforeclose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close:function()
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		}
	});
}
function OpisAllegroWindow(tryb, ile, ten)
{
	if(editorInit)
	for(name in CKEDITOR.instances)
    {
        CKEDITOR.instances[name].destroy();
		editorInit = false;
    }
	$('#opisAllegroWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 880,
		height: 520,
		title: "Wpisz opis Allegro",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			instance = "tekst_allegro";
			//$('#opis').val('');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				//$('#opis').val('');
				$('#opisAllegroOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#opisAllegroOK').val(CKEDITOR.instances[instance].getData());
				$('#wystawForm').submit();
			}
		},
		beforeclose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close:function()
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		}
	});
}

function facebookSprPosty(ten)
{
	if(facebookProduktID > 0)
	{
		var page = $('#page').val();
		var exist = parseInt($('#post'+facebookProduktID+'_'+page).val());
		console.log(facebookProduktID+' '+page+' '+exist);
		$('#facebook_add').attr('checked', !exist ? 'checked' : false);
		//$('#facebook_add').attr('disabled', exist ? 'disabled' : false);
		//$('#facebook_add').toggle(exist ? false : true);
		//$('label[for=facebook_add]').toggle(exist ? false : true);
		//$('label[for=facebook_add]').css('color', exist ? '#c3c3c3' : '#000000');
		$('#facebook_del').attr('checked', exist ? 'checked' : false);
		$('#facebook_del').attr('disabled', !exist ? 'disabled' : false);
		//$('#facebook_del').toggle(exist ? true : false);
		//$('label[for=facebook_del]').toggle(exist ? true : false);
		$('label[for=facebook_del]').css('color', !exist ? '#c3c3c3' : '#000000');
	}
	else
	{
		$('#facebook_add').attr('checked', 'checked');
		$('#facebook_del').attr('checked', false);
		$('#facebook_del').attr('disabled', false);
		$('label[for=facebook_del]').css('color', '#000000');
	}
}

function FacebookSingleWindow(id, od)
{
	//var width = $(this).parent().width();
	facebookProduktID = id;	
	$('#posty').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 400,
		height: 230,
		title: "Zarządzaj postami Facebook - produkt #" + id,
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#page').val('1');
			facebookSprPosty();
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				var akcja = $('.facebook:checked').val();
				var page = $('#page').val();
				var link = 'admin/oferta/lista/od/'+od+'/id/'+id;
				var www = url+'/'+link+'/facebook/'+akcja+'/page/'+page;
				window.location = www;
			}
		},
		beforeClose: function(event, ui) 
		{
			//$(ten).val('wiecej');
		},
		close: function(event, ui)
		{
			
		}
	});
}
function FacebookMultiWindow(ten)
{
	//var width = $(this).parent().width();
	facebookProduktID = 0;	
	$('#posty').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 400,
		height: 230,
		title: "Zarządzaj postami Facebook - wybrane produkty",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#page').val('1');
			$('#facebook').val('');
			$('#facebook_page').val('0');
			facebookSprPosty();
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#facebook').val($('.facebook:checked').val());
				$('#facebook_page').val($('#page').val());
				$('#wystawForm').submit();
				//$(this).dialog('close');
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			$('#facebook').val('');
			$('#facebook_page').val('0');
		}
	});
}