<?php
/*
Uploadify v2.1.4
Release Date: November 8, 2010

Copyright (c) 2010 Ronnie Garcia, Travis Nickels

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
//localhost/buciarnia/public/scripts/jquery.uploadify/uploadify.php

//require_once('../../../application/models/upload.php');
//echo getcwd();
//$_FILES['Filedata']['tmp_name'] = 'brak.jpg';
//$_FILES['Filedata']['name'] = 'brak.jpg';
//$_REQUEST['folder'] = '/public/admin/cv';
//$_REQUEST['fileext'] = 'jpg';

if(!empty($_FILES))
{
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';
	$targetTempFile = str_replace('//','/',$targetPath) . '_' . $_FILES['Filedata']['name'];
	//$srcFile =  '../../../public/admin/'.$_GET['path'].'/' . '_' . $_FILES['Filedata']['name'];
	$targetFile =  str_replace('//','/',$targetPath) . $_FILES['Filedata']['name'];
	
	//$fileTypes  = str_replace('*.','',$_REQUEST['fileext']);
	//$fileTypes  = str_replace(';','|',$fileTypes);
	//$typesArray = explode('\|',$fileTypes);
	//$fileParts  = pathinfo($_FILES['Filedata']['name']);
    $extension = @end(explode('.', $_FILES['Filedata']['name']));	
	$name = md5(uniqid(mt_rand(), true)).'.'.$extension;//.$fileParts['extension'];
	if(@$_GET['old']) $name = $_FILES['Filedata']['name'];
	if(@!empty($_GET['name'])) $name = $_GET['name'];
	$targetTempFile = str_replace('//','/',$targetPath).''.$name;
	        
	if(true || in_array($fileParts['extension'], $typesArray))
	{
		// Uncomment the following line if you want to make the directory if it doesn't exist
		// mkdir(str_replace('//','/',$targetPath), 0755, true);
		
		move_uploaded_file($tempFile, $targetTempFile);
		
		die($name);
	}
	else 
	{
	 	echo 'Invalid file type.';
	}
}
?>