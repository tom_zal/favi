if(false)
jQuery(document).ready(function()
{
    /*MENU LOJALNOSC POBRANIE SZEROKOSC I UKRYCIE ELEMENTOW*/
    var menuslide = false;
    jQuery("#jQ-menu").find('a').each(function () {
        var $class = this.className;
        jQuery('.'+$class).next().each(function(){
            jQuery('.'+$class).css('width', jQuery(this).innerWidth()+'px');
            var active = jQuery('.'+$class).parent().hasClass('active');
            if(active == false) {
                if(menuslide) jQuery(this).addClass('noactive');
                jQuery(this).css('position','relative');
                if(!menuslide) jQuery(this).css('bottom','0');
            }
        });
    });
    
    /*MENU LOJALNOSC PODSWIETLENIE ELEMENTOW*/
    jQuery("#jQ-menu a").hover(
        function(){
            if(menuslide)jQuery(this).next().removeClass('noactive');
            jQuery(this).next().animate({"bottom": '-15'}, "medium");
            jQuery(this).next().animate({"bottom": 0}, "medium");
        },
        function () {
            var active = jQuery(this).parent().hasClass('active');
            if(active == false) {
                if(menuslide)jQuery(this).next().addClass('noactive');
                if(menuslide)jQuery(this).next().animate({"bottom": "-15"}, "slow");
            }
        }
    );
    
    /*SPOLECZNOSC POBRANIE ELEMENTOW Z POTOMKAMI*/
    /*jQuery("#polecenia").find('li').each(function () {
        var el = this;
        jQuery(this).has('ul').each(function(){
            jQuery('#'+el.id+' a.link').addClass('hasPeople');
        });
    });
    */
    var data = document.getElementsByClassName('calendar');
    if(data)
	{
        jQuery(".dataStart").datepicker({dateFormat: 'yy-mm-dd', showOn: 'button',
            buttonImage: baseUrl + '/public/images/admin/ikony/calendar.gif',
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '-90:+10'
        });

    }
    var tooltip = document.getElementsByClassName('jQ_tooltip');
    if(tooltip)
    {
        $('.jQ_tooltip').tooltip({
            delay: 0,
            showURL: false,
            fade: 250
        });
    }
});

function getClientNumberCard(value)
{    
    if(!isNaN(value)) {
        jQuery('#jQ-error').addClass('dn');
        jQuery('#jQ-error').html('');
        if(value.length > 0) {
            jQuery('#jQ-error').addClass('dn');
            jQuery('#jQ-error').html('');
            
            var timeout = 8000;
            
            jQuery.ajax({
                url: "http://"+www+baseUrl+"/ajax/index/karta/karta/"+value,
                type: "GET",
                timeout: timeout,
                async: true,
                success: function(html) {
                    jQuery('#jQ-lista').html(html);
                    jQuery('a[rel*=facebox]').facebox() ;
                }
            });
            
        } else {
            jQuery('#jQ-error').removeClass('dn');
            jQuery('#jQ-error').html('<div class="o">Podaj minimum 1 cyfry numeru karty.</div>');
        }
    } else {
        jQuery('#jQ-error').removeClass('dn');
        jQuery('#jQ-error').html('<div class="b">Podaj numer karty.</div>');
    }
    jQuery('#jQ-one').addClass('dn');    
}

function appendRow($this)
{
    var checked = $($this).attr('checked');
    if(checked != undefined)
    {
        var ar = $this.value.split('|--|');
        var email = '';
        var sms = '';
        if(ar[1])
        {
            var email = ', e-mail: '+ar[1];
        }
        if(ar[0])
        {
            var sms = ', sms: '+ar[0];
        }

        var hidden = '<input type="hidden" name="post[klient]['+ar[4]+']" value="'+ar+'" />';
        $('#jQ_client').append('<span class="db cb lf"><span class="lf"><b>'+ar[2]+' '+ar[3]+'</b>'+sms+email+'&nbsp; &nbsp;</span> <a onclick="return removeRow(this);" title="usuń z akcji" class="lf del_ico silver"></a>'+hidden+'</span>');
    }
}
function appendRowLista($this, typ)
{
    var checked = $($this).attr('checked');
    if(true)
    {
        var ar = $this.value.split('|--|');
        var hidden = '<input type="hidden" id="'+typ+ar[0]+'" name="'+typ+'['+ar[0]+']" value="'+ar[0]+'" />';
		if(checked != undefined && $('#'+typ+ar[0]).attr('id') == undefined)
        $('#jQ_client_'+typ).append('<span class="db cb lf"><span class="lf"><b>'+ar[1]+'</b>&nbsp;&nbsp;</span><a onclick="return removeRow(this);" title="usuń" class="lf del_ico silver"></a>'+hidden+'</span>');
		if(checked == undefined && $('#'+typ+ar[0]).attr('id') != undefined)
		$('#'+typ+ar[0]).parent().remove();
    }
}

function appendRowAll($this)
{
    jQuery('#'+$this).find('input[type="checkbox"]').each(function()
	{
        var checked = $(this).attr('checked');
        if(checked == undefined)
        {
            var ar = this.value.split('|--|');
            var email = '';
            var sms = '';
            if(ar[1])
            {
                var email = ', e-mail: '+ar[1];
            }
            if(ar[0])
            {
                var sms = ', sms: '+ar[0];
            }
            $(this).attr('checked','true');

            var hidden = '<input type="hidden" name="post[klient]['+ar[4]+']" value="'+ar+'" />';
            $('#jQ_client').append('<span class="db cb lf"><span class="lf"><b>'+ar[2]+' '+ar[3]+'</b>'+sms+email+'&nbsp; &nbsp;</span> <a onclick="return removeRow(this);" title="usuń z akcji" class="lf del_ico silver"></a>'+hidden+'</span>');
        }
    });
}
function appendRowAllLista($this, typ, check)
{
    jQuery('#'+$this).find('input[type="checkbox"]').each(function()
	{
        var checked = $(this).attr('checked');
        if(true)
        {
            var ar = this.value.split('|--|');
            $(this).attr('checked', check);
			var hidden = '<input type="hidden" id="'+typ+ar[0]+'" name="'+typ+'['+ar[0]+']" value="'+ar[0]+'" />';
			if(check && $('#'+typ+ar[0]).attr('id') == undefined)
			$('#jQ_client_'+typ).append('<span class="db cb lf"><span class="lf"><b>'+ar[1]+'</b>&nbsp;&nbsp;</span><a onclick="return removeRow(this);" title="usuń" class="lf del_ico silver"></a>'+hidden+'</span>');
			if(!check && $('#'+typ+ar[0]).attr('id') != undefined)
			$('#'+typ+ar[0]).parent().remove();
        }
    });
}

function removeRow($this)
{
    $($this).parent().remove();
}
function mbstrlen(str)
{
    var len = 0;
    for(var i = 0; i < str.length; i++)
	{
        len += str.charCodeAt(i) < 0 || str.charCodeAt(i) > 255 ? 2 : 1;
    }
    return len;
}

function lengthInput($this, output)
{
    $('#'+output).html(mbstrlen($this.value));
}

function toggleC(id, pow)
{
    if(pow) $('#'+pow).addClass('dn').removeClass('db').fadeOut(3000);
    if(id) $('#'+id+'_open').addClass('db').removeClass('dn').fadeIn(3000);
}

function getEmptyCard(value,typ)
{    
    if(!isNaN(value)) {
        jQuery('#jQ-error'+typ).addClass('dn');
        jQuery('#jQ-error'+typ).html('');
        if(value.length == 5) {
            jQuery('#jQ-error'+typ).addClass('dn');
            jQuery('#jQ-error'+typ).html('');
            
            var timeout = 8000;
            
            jQuery.ajax({
                url: "http://"+www+baseUrl+"/ajax/upominki/karta/karta/"+value+'/typ/'+typ,
                type: "GET",
                timeout: timeout,
                async: true,
                success: function(html) {
                    window.console.log(html);
                    jQuery('#jQ-error'+typ).removeClass('dn');
                    if(html){
                        jQuery('#jQ-error'+typ).html('<div class="o">Numer karty jest prawidłowy.</div>');
                    } else {
                        jQuery('#jQ-error'+typ).html('<div class="b">Błędny numer karty.</div>');
                    }
                    
                }
            });
            
        } else {
            jQuery('#jQ-error'+typ).removeClass('dn');
            jQuery('#jQ-error'+typ).html('<div class="o">Podaj 5 cyfr numeru karty.</div>');
        }
    } else {
        jQuery('#jQ-error'+typ).removeClass('dn');
        jQuery('#jQ-error'+typ).html('<div class="b">Podaj numer karty.</div>');
    }
    
}

function getOneCard(id)
{
    jQuery("."+id).each(function () {
        jQuery('#'+this.id+'-value').text(this.value);
    });
    jQuery('#jQ-one').removeClass('dn');
}
function requestAjax(href)
{
    var timeout = 8000;
    jQuery.ajax(
	{
        url: href,
        type: "get",
        timeout: timeout,
        async: true,
        success: function(html) {
            var obj = html.substr(0,1) == '{' ? jQuery.parseJSON(html) : '';
            if(obj.href != undefined) {
                requestAjax(obj.href);
                if(obj.tryb != undefined) jQuery('#jQ_tryb_gift_'+obj.tryb).html(obj.html);
                if(obj.tryb != undefined) {
                    tryb = obj.tryb == 0 ? 1 : 0;
                    if(obj.html2 != undefined) jQuery('#jQ_tryb_gift_'+tryb).html(obj.html);
                }
            } else {
                jQuery('#facebox div.content').html(html);
            }
        }
    });

}

function save(href, id)
{
    var data = $('#'+id).serializeArray();
    if(data[0].value.length == 0)
	{
        jQuery('#jQ-message div').html('1. Pole nazwa jest wymagane.').css('display', 'block');
    }
	else
	{
        jQuery('#jQ-message div').html('').css('display', 'none');
        var timeout = 8000;
        jQuery.ajax(
		{
            url: href,
            type: "post",
            timeout: timeout,
            data: data,
            async: true,
            success: function(html)
			{
                requestAjax(html);
                return false;
            }
        });
    }
}

function requestpostAjax(href, id)
{
    var data = $('#'+id).serializeArray();
    var timeout = 8000;
    jQuery.ajax(
	{
        url: href,
        type: "post",
        timeout: timeout,
        data: data,
        async: true,
        success: function(html)
		{
            var obj = html.substr(0,1) == '{' ? jQuery.parseJSON(html) : '';
            if(obj.href != undefined)
			{
				requestAjax(obj.href);
            }
			else
			{
                jQuery('#facebox div.content').html(html);
            }
        }
    });
}

function addOrder(id, href)
{
    var data = $('#'+id+'-form').serializeArray();
    window.console.log(data);
    var timeout = 8000;
    jQuery.ajax(
	{
        url: href,
        type: "post",
        timeout: timeout,
        data: data,
        async: true,
        success: function(html)
		{
           jQuery('#facebox div.content').html(html);
        }
    });
}
function copyvalue(id, value)
{
    jQuery('#'+id+'-pole').val(value);
}

var checkboxs_checked = false;

function zaznacz(form)
{
	if(document.getElementById(form) == undefined) return;
    var checkboxy = document.getElementById(form).getElementsByTagName('input');
    for(i = 0; i < checkboxy.length; i++)
    {
		if (checkboxy[i].type == 'checkbox')
		{
			checkboxy[i].checked = checkboxs_checked ? false : true;
		}
    }
    checkboxs_checked = checkboxs_checked ? false : true;
}
function zaznaczWybrane(form, typ)
{
	if(document.getElementById(form) == undefined) return;
    var checkboxy = document.getElementById(form).getElementsByTagName('input');
    for(i = 0; i < checkboxy.length; i++)
    {
		if (checkboxy[i].type == 'checkbox')
		{
			var ar = checkboxy[i].value.split('|--|');
			checkboxy[i].checked = ($('#'+typ+ar[0]).attr('id') == undefined) ? false : true;
		}
    }
}

function saveItem(href, id)
{
    var data = $('#'+id).serializeArray();
    var timeout = 8000;
    var tryb = jQuery('input[name="tryb"]').val();
    jQuery.ajax(
	{
        url: href,
        type: "post",
        timeout: timeout,
        data: data,
        async: true,
        success: function(html)
		{
            jQuery('#jQ_tryb_gift_'+tryb).html(html);
            return false;
        }
    });
}
function saveZakup(href, id)
{
    var data = $('#'+id).serializeArray();
    var timeout = 8000;
    jQuery.ajax(
	{
        url: href,
        type: "post",
        timeout: timeout,
        data: data,
        async: true,
        success: function(html)
		{
            jQuery('#facebox div.content').html(html);
        }
    });
}
function searchAjax(href,id,typ)
{
    var data = $('#'+id).serializeArray();
    var timeout = 8000;
    jQuery.ajax(
	{
        url: href,
        type: "get",
        timeout: timeout,
        data: data,
        async: true,
        success: function(html)
		{
            jQuery('#facebox div.content').html(html);
			zaznaczWybrane('jQ-forma2', typ);
        }
    });
}

function getListaKlienci(href, page)
{
    var timeout = 8000;
    jQuery.ajax(
	{
        url: href,
        type: "get",
        timeout: timeout,
        async: true,
        success: function(html)
		{
            jQuery('#facebox div.content').html(html);
        }
    });
}
function wybierzkarta(karta)
{
    jQuery('#jQ_numerkarta').val(karta);
    jQuery('#facebox a.close').trigger('click');
}

var facebookProduktID = 0;
function ofertaListaOpcjeAllWiecej(ten, url)
{
	var akcja = '';
	var uwaga = '';
	var val = $(ten).val();
	if(val == '')
	{
		$(ten).val('wiecej');
		$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		return;
	}
	if(val == 'wiecej') return;
	var ileZazn = $('.checkbox:checked').length;
	var ileAll = 0;//<?php echo isset($this->ileProd) ? $this->ileProd : 0; ?>;
	var tryb = 'zadania_wybrane';//$(ten).attr('name') == 'opcje3' ? 'wyszukiwarka' : 'zadania_wybrane';
	var ile = tryb == 'wyszukiwarka' ? ileAll : ileZazn;
	var czego = '';
	
	if(ile == 0)
	{
		if(tryb == 'zadania_wybrane') alert('Nie zaznaczono żadnych produktów!');
		if(tryb == 'wyszukiwarka') alert('Na liście nie ma produktów!');
		$(ten).val('wiecej');
		$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		return;
	}
	
	switch(val)
	{
		case 'kopiuj' : 
		akcja = 'admin/oferta/powiel/tryb/kopia/ids'; 
		uwaga = 'Czy na pewno chcesz skopiować zaznaczone produkty ?';
		czego = 'kopiowania';
		break;
		case 'powiel' : 
		akcja = 'admin/oferta/powiel/tryb/langs/ids'; 
		//uwaga = 'Czy na pewno chcesz powielić zaznaczone produkty ?';
		//czego = 'powielenia';
		break;
		case 'facebook' : 
		akcja = 'admin/oferta/lista/facebook/all'; 
		break;
		case 'facebook_add' : 
		akcja = 'admin/oferta/lista/facebook/1'; 
		uwaga = 'Czy na pewno chcesz dodać posty z zaznaczonych produktów do konta Facebook ?';
		czego = 'dodania postów na konto Facebook';
		break;
		case 'facebook_del' : 
		akcja = 'admin/oferta/lista/facebook/0'; 
		uwaga = 'Czy na pewno chcesz usunąć posty z zaznaczonych produktów z konta Facebook ?';
		czego = 'usunięcia postów z konta Facebook';
		break;
		case 'allegro' : 
		akcja = 'admin/allegroprodukt/wystawprod/tryb/produkty'; 
		//uwaga = 'Czy na pewno chcesz wystawić na Allegro zaznaczone produkty ?';
		//czego = 'wystawienia na Allegro';
		break;
		case 'usun_allegro' : 
		akcja = 'admin/oferta/lista/delnrs'; 
		uwaga = 'Czy na pewno chcesz usunąć aukcje zaznaczonych produktów ?';
		czego = 'usunięcia aukcji';
		break;
		case 'usun' : 
		akcja = 'admin/oferta/lista/delids'; 
		uwaga = 'Czy na pewno chcesz usunąć zaznaczone produkty ?';
		czego = 'usunięcia';
		break;
		case 'ukryj' : 
		akcja = 'admin/oferta/lista/widoczny/0';
		uwaga = 'Czy na pewno chcesz ukryć zaznaczone produkty ?';
		czego = 'ukrycia';
		break;
		case 'odkryj' : 
		akcja = 'admin/oferta/lista/widoczny/1';
		uwaga = 'Czy na pewno chcesz odkryć zaznaczone produkty ?';
		czego = 'odkrycia';
		break;
		case 'jedn_miary_zb_czy_0' : 
		akcja = 'admin/oferta/lista/jedn_miary_zb_czy/0';
		uwaga = 'Czy na pewno chcesz ustawić zamówienia składane na sztuki zaznaczonym produktom ?';
		czego = 'ustawienia zamówień składanych na sztuki ';
		break;
		case 'jedn_miary_zb_czy_1' : 
		akcja = 'admin/oferta/lista/jedn_miary_zb_czy/1';
		uwaga = 'Czy na pewno chcesz ustawić zamówienia składane na opakowania zbiorcze zaznaczonym produktom ?';
		czego = 'ustawienia zamówień składanych na opakowania zbiorcze ';
		break;
		case 'jedn_miary_zb_typ_0' : 
		akcja = 'admin/oferta/lista/jedn_miary_zb_typ/0';
		uwaga = 'Czy na pewno chcesz ustawić zamówienia składane na opakowania zbiorcze tylko w całości zaznaczonym produktom ?';
		czego = 'ustawienia zamówień składanych na opakowania zbiorcze tylko w całości';
		break;
		case 'jedn_miary_zb_typ_1' : 
		akcja = 'admin/oferta/lista/jedn_miary_zb_typ/1';
		uwaga = 'Czy na pewno chcesz ustawić zamówienia składane na opakowania zbiorcze również w częściach zaznaczonym produktom ?';
		czego = 'ustawienia zamówień składanych na opakowania zbiorcze również w częściach';
		break;
		case 'negocjuj_cene_0' : 
		akcja = 'admin/oferta/lista/negocjuj_cene/0';
		uwaga = 'Czy na pewno chcesz ustawić "negocjuj cene" zaznaczonym produktom ?';
		czego = 'ustawienia "negocjuj cene" ';
		break;
		case 'negocjuj_cene_1' : 
		akcja = 'admin/oferta/lista/negocjuj_cene/1';
		uwaga = 'Czy na pewno chcesz usunąć "negocjuj cene" zaznaczonym produktom ?';
		czego = 'usunięcia "negocjuj cene" ';
		break;
		case 'glowna_1' : 
		akcja = 'admin/oferta/lista/glowna/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Główna" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Główna" ';
		break;
		case 'glowna_0' : 
		akcja = 'admin/oferta/lista/glowna/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Główna" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Główna" ';
		break;
		case 'nowosc_1' : 
		akcja = 'admin/oferta/lista/nowosc/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Nowość" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Nowość" ';
		break;
		case 'nowosc_0' : 
		akcja = 'admin/oferta/lista/nowosc/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Nowość" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Nowość" ';
		break;
		case 'promocja_1' : 
		akcja = 'admin/oferta/lista/promocja/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Promocja" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Promocja" ';
		break;
		case 'promocja_0' : 
		akcja = 'admin/oferta/lista/promocja/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Promocja" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Promocja" ';
		break;
		case 'promocja_net_1' : 
		akcja = 'admin/oferta/lista/promocja_net/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Promocja Internetowa" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Promocja Internetowa" ';
		break;
		case 'promocja_net_0' : 
		akcja = 'admin/oferta/lista/promocja_net/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Promocja Internetowa" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Promocja Internetowa" ';
		break;
		case 'polecane_1' : 
		akcja = 'admin/oferta/lista/polecane/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Polecane" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Polecane" ';
		break;
		case 'polecane_0' : 
		akcja = 'admin/oferta/lista/polecane/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Polecane" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Polecane" ';
		break;
		case 'produkt_dnia_1' : 
		akcja = 'admin/oferta/lista/produkt_dnia/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Produkt Dnia" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Produkt Dnia" ';
		break;
		case 'produkt_dnia_0' : 
		akcja = 'admin/oferta/lista/produkt_dnia/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Produkt Dnia" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Produkt Dnia" ';
		break;
		case 'wyprzedaz_1' : 
		akcja = 'admin/oferta/lista/wyprzedaz/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Wyprzedaż" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Wyprzedaż" ';
		break;
		case 'wyprzedaz_0' : 
		akcja = 'admin/oferta/lista/wyprzedaz/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Wyprzedaż" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Wyprzedaż" ';
		break;
		case 'specjalne_1' : 
		akcja = 'admin/oferta/lista/specjalne/1';
		uwaga = 'Czy na pewno chcesz ustawić oznaczenie "Specjalne" zaznaczonym produktom ?';
		czego = 'ustawienia oznaczenia "Specjalne" ';
		break;
		case 'specjalne_0' : 
		akcja = 'admin/oferta/lista/specjalne/0';
		uwaga = 'Czy na pewno chcesz usunąć oznaczenie "Specjalne" zaznaczonym produktom ?';
		czego = 'usunięcia oznaczenia "Specjalne" ';
		break;
		case 'kategorie' : 
		akcja = 'admin/oferta/lista/zmienkat'; 
		//uwaga = 'Czy na pewno chcesz zmienić kategorie zaznaczonym produktom ?';
		czego = 'zmiany kategorii';
		break;
		case kategorieInne : 
		akcja = 'admin/oferta/lista/zmienkat'; 
		//uwaga = 'Czy na pewno chcesz zmienić ' + kategorieInne + ' zaznaczonym produktom ?';
		czego = 'zmiany ' + kategorieInne;
		break;
		case 'producent' : 
		akcja = 'admin/oferta/lista/zmienprod'; 
		//uwaga = 'Czy na pewno chcesz zmienić producenta zaznaczonym produktom ?';
		czego = 'zmiany producenta';
		break;
		case 'grupa' : 
		akcja = 'admin/oferta/lista/zmiengrupa'; 
		//uwaga = 'Czy na pewno chcesz zmienić grupę zaznaczonym produktom ?';
		czego = 'zmiany grupy';
		break;
		case 'rabat' : 
		akcja = 'admin/oferta/lista/zmienrabat'; 
		//uwaga = 'Czy na pewno chcesz zmienić rabat zaznaczonym produktom ?';
		czego = 'zmiany rabatu';
		break;
		case 'ceny' : 
		akcja = 'admin/oferta/lista/zmienceny'; 
		//uwaga = 'Czy na pewno chcesz zmienić ceny zaznaczonym produktom ?';
		czego = 'zmiany ceny';
		break;
		default : 
		akcja = 'admin/oferta/lista';
	}
	
	//uwaga = '';
	//if(czego.length > 0) uwaga = 'Przygotowano do ' + czego + ' ' + ile + ' produkty(ów)';
	//$('#zadaniaTryb').val(tryb);
	//$('#opcje3').val('wiecej');
	//$('#zadaniaAkcja').val(tryb == 'wyszukiwarka' ? val : 'wiecej');
	
	$('#wystawForm').attr('action', url + '/' + akcja + '/all');
	if(uwaga.length == 0 || window.confirm(uwaga))
	{
		if(val == 'producent') ProducentWindow(tryb, ile, ten);
		else if(val == 'grupa') GrupaWindow(tryb, ile, ten);
		else if(val == 'kategorie') KategoriaWindow(tryb, ile, ten);
		else if(val == kategorieInne) KategoriaInneWindow(tryb, ile, ten);
		else if(val == 'ceny') ZmianaCenyWindow(tryb, ile, ten);
		else if(val == 'rabat') ZmianaRabatWindow(tryb, ile, ten);
		else if(val == 'powiel') PowielWindow(tryb, ile, ten);
		else if(val == 'opis') OpisWindow(tryb, ile, ten);
		else if(val == 'facebook') FacebookMultiWindow(ten);
		//else if(val.substr(0, 7) == 'allegro') WystawBoksWindow(tryb, ile, ten, akcja, val);
		else $('#wystawForm').submit();
	}
	else
	{
		$(ten).val('wiecej');
		$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
	}
}

function ProducentWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#producentWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 200,
		height: 130,
		title: "Wybierz producenta",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#producent').val('0');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$('#producent').val('0');
				$('#producentOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#producentOK').val($('#producent').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			
		}
	});
}
function GrupaWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#grupaWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 200,
		height: 130,
		title: "Wybierz grupę",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#grupa').val('0');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$('#grupa').val('0');
				$('#grupaOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#grupaOK').val($('#grupa').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			
		}
	});
}
function KategoriaWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#kategorieWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 340,
		height: 250,
		title: "Wybierz kategorie",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#kategorieWindow select').val('-1');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$('#kategorieWindow select').val('-1');
				$('#kategorieOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#kategorieOK').val($('#kategorieWindow select').serialize());
				//alert($('#kategorieOK').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			
		}
	});
}
function KategoriaInneWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#kategorieInneWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 340,
		height: 250,
		title: "Wybierz " + kategorieInne,
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#kategorieInneWindow select').val('-1');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$('#kategorieInneWindow select').val('-1');
				$('#kategorieInneOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#kategorieInneOK').val($('#kategorieInneWindow select').serialize());
				//alert($('#kategorieInneOK').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			
		}
	});
}
function ZmianaRabatWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#rabatyWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 200,
		height: 130,
		title: "Wpisz rabat",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#rabat').val('');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$('#rabat').val('');
				$('#rabatOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#rabatOK').val($('#rabat').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			
		}
	});
}
function ZmianaCenyWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#CenyWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 200,
		height: 130,
		title: "Wpisz cenę",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#cena').val('');
		},
		buttons: 
		{
			'Cancel': function() 
			{
				$('#cena').val('');
				$('#cenaOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#cenaOK').val($('#cena').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{

		}
	});
}
function PowielWindow(tryb, ile, ten)
{
	//var width = $(this).parent().width();
	$('#powielWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 250,
		height: 130,
		title: "Wybierz język",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#lang').val('');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$('#lang').val('');
				$('#langOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#langOK').val($('#lang').val());
				$('#wystawForm').submit();
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			
		}
	});
}
var editorInit = true;
var instance = "tekst";
function OpisWindow(tryb, ile, ten)
{
	if(editorInit)
	for(name in CKEDITOR.instances)
    {
        CKEDITOR.instances[name].destroy();
		editorInit = false;
    }
	$('#opisWindow').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 880,
		height: 520,
		title: "Wpisz opis",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			//$('#opis').val('');
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				//$('#opis').val('');
				$('#opisOK').val('');
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#opisOK').val(CKEDITOR.instances[instance].getData());
				$('#wystawForm').submit();
			}
		},
		beforeclose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close:function()
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		}
	});
}

function facebookSprPosty(ten)
{
	if(facebookProduktID > 0)
	{
		var page = $('#page').val();
		var exist = parseInt($('#post'+facebookProduktID+'_'+page).val());
		console.log(facebookProduktID+' '+page+' '+exist);
		$('#facebook_add').attr('checked', !exist ? 'checked' : false);
		//$('#facebook_add').attr('disabled', exist ? 'disabled' : false);
		//$('#facebook_add').toggle(exist ? false : true);
		//$('label[for=facebook_add]').toggle(exist ? false : true);
		//$('label[for=facebook_add]').css('color', exist ? '#c3c3c3' : '#000000');
		$('#facebook_del').attr('checked', exist ? 'checked' : false);
		$('#facebook_del').attr('disabled', !exist ? 'disabled' : false);
		//$('#facebook_del').toggle(exist ? true : false);
		//$('label[for=facebook_del]').toggle(exist ? true : false);
		$('label[for=facebook_del]').css('color', !exist ? '#c3c3c3' : '#000000');
	}
	else
	{
		$('#facebook_add').attr('checked', 'checked');
		$('#facebook_del').attr('checked', false);
		$('#facebook_del').attr('disabled', false);
		$('label[for=facebook_del]').css('color', '#000000');
	}
}

function FacebookSingleWindow(id, od)
{
	//var width = $(this).parent().width();
	facebookProduktID = id;	
	$('#posty').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 400,
		height: 230,
		title: "Zarządzaj postami Facebook - produkt #" + id,
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#page').val('1');
			facebookSprPosty();
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				var akcja = $('.facebook:checked').val();
				var page = $('#page').val();
				var link = 'admin/oferta/lista/od/'+od+'/id/'+id;
				var www = url+'/'+link+'/facebook/'+akcja+'/page/'+page;
				window.location = www;
			}
		},
		beforeClose: function(event, ui) 
		{
			//$(ten).val('wiecej');
		},
		close: function(event, ui)
		{
			
		}
	});
}
function FacebookMultiWindow(ten)
{
	//var width = $(this).parent().width();
	facebookProduktID = 0;	
	$('#posty').dialog(
	{
		autoOpen: true,
		modal: true,
		width: 400,
		height: 230,
		title: "Zarządzaj postami Facebook - wybrane produkty",
		resizable: false,
		//dialogClass: 'dialogBoks',
		position: ['center','center'],
		open: function() 
		{
			$('#page').val('1');
			$('#facebook').val('');
			$('#facebook_page').val('0');
			facebookSprPosty();
		},
		buttons: 
		{
			'Anuluj': function() 
			{
				$(this).dialog('close');
			},
			'Ok': function() 
			{
				$('#facebook').val($('.facebook:checked').val());
				$('#facebook_page').val($('#page').val());
				$('#wystawForm').submit();
				//$(this).dialog('close');
			}
		},
		beforeClose: function(event, ui) 
		{
			$(ten).val('wiecej');
			$(ten).selectbox('change', 'wiecej', 'Operacje hurtowe');
		},
		close: function(event, ui)
		{
			$('#facebook').val('');
			$('#facebook_page').val('0');
		}
	});
}

function klienciListaOpcjeAllWiecej(ten, url)
{
	var akcja = '';
	var uwaga = '';
	var val = ten.value;
	if(val == '') { $(ten).val('wiecej'); return; }
	if(val == 'wiecej') return;
	var ileZazn = $('.checkbox:checked').length;
	var ileAll = 0;//<?php echo isset($this->ileProd) ? $this->ileProd : 0; ?>;
	var tryb = 'zadania_wybrane';//$(ten).attr('name') == 'opcje3' ? 'wyszukiwarka' : 'zadania_wybrane';
	var ile = tryb == 'wyszukiwarka' ? ileAll : ileZazn;
	var czego = '';
	
	if(ile == 0)
	{
		if(tryb == 'zadania_wybrane') alert('Nie zaznaczono żadnych klientów!');
		if(tryb == 'wyszukiwarka') alert('Na liście nie ma klientów!');
		$(ten).val('wiecej');
		return;
	}
	
	switch(val)
	{
		case 'usun' : 
		akcja = 'admin/kontrahent/lista/delids'; 
		uwaga = 'Czy na pewno chcesz usunąć zaznaczonych klientów ?';
		czego = 'usunięcia';
		break;
		case 'mail' : 
		akcja = 'admin/kontrahent/mail/mailids'; 
		break;
		case 'sms' : 
		akcja = 'admin/kontrahent/sms/smsids'; 
		break;
		case 'trasa' : 
		akcja = 'admin/kontrahent/lista/trasaids'; 
		break;
		case 'ukryj' : 
		akcja = 'admin/kontrahent/lista/widoczny/0'; 
		uwaga = 'Czy na pewno chcesz ukryć zaznaczonych klientów ?';
		break;
		case 'odkryj' : 
		akcja = 'admin/kontrahent/lista/widoczny/1'; 
		uwaga = 'Czy na pewno chcesz pokazać zaznaczonych klientów ?';
		break;
		case 'rabat_zapytaj_1' : 
		akcja = 'admin/kontrahent/lista/rabat_zapytaj/1'; 
		uwaga = 'Czy na pewno chcesz ustawić "Zapytaj o rabat" zaznaczonym klientom ?';
		break;
		case 'rabat_zapytaj_0' : 
		akcja = 'admin/kontrahent/lista/rabat_zapytaj/0';
		uwaga = 'Czy na pewno chcesz usunąć "Zapytaj o rabat" zaznaczonym klientom ?';
		break;
		default : 
		akcja = 'admin/kontrahent/lista';
	}
	
	$('#form').attr('action', url + '/' + akcja + '/all');
	if(uwaga.length == 0 || window.confirm(uwaga))
	{
		if(val == 'trasa') TrasaWindow(tryb, ile, ten);
		else $('#form').submit();
	}
	else $(ten).val('wiecej');
}

function zamowListaOpcjeAllWiecej(ten, url)
{
	var akcja = '';
	var uwaga = '';
	var val = ten.value;
	if(val == '') { $(ten).val('wiecej'); return; }
	if(val == 'wiecej') return;
	var ileZazn = $('.checkbox:checked').length;
	var ileAll = 0;//<?php echo isset($this->ileProd) ? $this->ileProd : 0; ?>;
	var tryb = 'zadania_wybrane';//$(ten).attr('name') == 'opcje3' ? 'wyszukiwarka' : 'zadania_wybrane';
	var ile = tryb == 'wyszukiwarka' ? ileAll : ileZazn;
	var czego = '';
	
	if(ile == 0)
	{
		if(tryb == 'zadania_wybrane') alert('Nie zaznaczono żadnych zamówień!');
		if(tryb == 'wyszukiwarka') alert('Na liście nie ma zamówień!');
		$(ten).val('wiecej');
		return;
	}
	
	switch(val)
	{
		case 'usun' : 
		akcja = 'admin/zamowienia/wypisz/delids'; 
		uwaga = 'Czy na pewno chcesz usunąć zaznaczone zamówienia ?';
		czego = 'usunięcia';
		break;
		default : 
		akcja = 'admin/zamowienia/wypisz';
	}
	
	$('#form').attr('action', url + '/' + akcja + '/all');
	if(uwaga.length == 0 || window.confirm(uwaga))
	{
		if(val == 'producent') ProducentWindow(tryb, ile, ten);
		else $('#form').submit();
	}
	else $(ten).val('wiecej');
}