$(document).ready(function () {
    var wysokosc = 0;
    var wysokosc_max = 0;
    var arr = [];
    $(".item-wrapper").each(function () {
        wysokosc = $(this).height();
        if (wysokosc_max <= wysokosc) {
            wysokosc_max = wysokosc;
        }
    });
    $(".item-wrapper").css('height', wysokosc_max + 'px');


    $('.filtrowanie_pole .lf').each(function () {
        arr.push($(this).height());
    });
    $('.filtrowanie_pole .lf').css('height', Math.max.apply(Math, arr) + 'px');

    $('.produktParameter').children(':nth-child(2)').css('color', 'black');

    var offsetStart = $("#lista_kat ul li").offset();
    $("#lista_kat ul li").each(function () {
        var offsetCurr = $(this).offset();

        if (offsetCurr.top != offsetStart.top) {
            $(this).prev().children().css("border-right", "none");
            return false;
        }
    });

    $('.product_content h3').wrap('<div class = "nagl"></div>');

    $('#my_sort_wrapper').css('height', $('#my_search_wrapper').height() + 'px');

    $("#menuLinki .expand2").hover(
            function () {
                $(this).find('ul').stop().show('fast');
                var str = $(this).find('ul').html();
                if (str){
                    str = str.trim();
                    if (str == '') {
                        $(this).find('.expand3').css('border', 'none !important');
                    }
                }
            },
            function () {
                $(this).find('ul').stop().hide("fast");
            }
    );
    
    $("#lista-produktow .itemsNagl").css('height', ($(window).height() - 255 - $("#stopka-wrapper").height() - $("#projekt-wrapper").height() - 20 - 19) + 'px');
//    $("#body").css('min-height', ($(window).height() - $("#stopka-wrapper").height() - $("#sciezka").height() - $("#menu").height() - $("#top").height() + 2) + 'px');
    
//    $(".stopkaBlok").css('width', $(".stopkaBlokNapis", this).width() + 'px');
    
//    $(".stopkaBlok").each(function(){
//        $(this).css('width', $(this).find('.stopkaBlokNapis').width() + 44 + 'px');
//    });
    
//    $("#lista-produktow").css('min-height', ($(window).height() - 255 - $("#stopka-wrapper").height() - $("#projekt-wrapper").height()-10-33) + 'px');
//    $("#myRight").css('min-height', ($(window).height() - 255 - $("#stopka-wrapper").height() - $("#projekt-wrapper").height()-10-33) + 'px');






    $('#prodAll').css('opacity', '1');
    
//    $("#logotypy .row .logotypy-images img").wrap("<div></div>");

});
