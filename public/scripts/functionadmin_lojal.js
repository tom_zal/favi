var typ = '';
function countRows(typek)
{
	if(typ == '' && typek != undefined) typ = typek;
	var ile = $('#jQ_client_' + typ).children('span').size();
	$('#wybrano_' + typ).text(ile);
}

//if(false)
$(document).ready(function()
{
    /*MENU LOJALNOSC POBRANIE SZEROKOSC I UKRYCIE ELEMENTOW*/
    var menuslide = false;
    jQuery("#jQ-menu").find('a').each(function()
	{
        var $class = this.className;
        jQuery('.'+$class).next().each(function()
		{
            jQuery('.'+$class).css('width', jQuery(this).innerWidth()+'px');
            var active = jQuery('.'+$class).parent().hasClass('active');
            if(active == false)
			{
                if(menuslide) jQuery(this).addClass('noactive');
                jQuery(this).css('position','relative');
                if(!menuslide) jQuery(this).css('bottom','0');
            }
        });
    });
    
    /*MENU LOJALNOSC PODSWIETLENIE ELEMENTOW*/
    jQuery("#jQ-menu a").hover
	(
        function()
		{
            if(menuslide)jQuery(this).next().removeClass('noactive');
            jQuery(this).next().animate({"bottom": '-15'}, "medium");
            jQuery(this).next().animate({"bottom": 0}, "medium");
        },
        function
		()
		{
            var active = jQuery(this).parent().hasClass('active');
            if(active == false)
			{
                if(menuslide)jQuery(this).next().addClass('noactive');
                if(menuslide)jQuery(this).next().animate({"bottom": "-15"}, "slow");
            }
        }
    );
    
    /*SPOLECZNOSC POBRANIE ELEMENTOW Z POTOMKAMI*/
    /*jQuery("#polecenia").find('li').each(function()
	{
        var el = this;
        jQuery(this).has('ul').each(function()
		{
            jQuery('#'+el.id+' a.link').addClass('hasPeople');
        });
    });
    */
    var data = document.getElementsByClassName('calendar');
    if(data)
	{
        jQuery(".dataStart").datepicker(
		{
			dateFormat: 'yy-mm-dd', 
			showOn: 'button',
            buttonImage: baseUrl + '/public/images/admin/ikony/calendar.gif',
            buttonImageOnly: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '-90:+10'
        });
    }
    var tooltip = document.getElementsByClassName('jQ_tooltip');
    if(tooltip)
    {
        $('.jQ_tooltip').tooltip(
		{
            delay: 0,
            showURL: false,
            fade: 250
        });
    }
	
	if(false)
	$("a[rel*=facebox]").first().click(function()
	{
		$.facebox(function()
		{
			$.facebox.loading(); 
			$.get($(this).attr('href'), function(data)
			{
				$.facebox.close_loading();
				$.facebox(data);
			})
		})
	});
	//if(false)
	$('a[rel*=facebox]').facebox(
	{
		loadingImage : baseUrl + '/public/images/admin/facebox/loading.gif',
		closeImage   : baseUrl + '/public/images/admin/facebox/closelabel.png'
	});
	//if(false)
	$('a.facebox').facebox(
	{
		loadingImage : baseUrl + '/public/images/admin/facebox/loading.gif',
		closeImage   : baseUrl + '/public/images/admin/facebox/closelabel.png'
	});
	$(document).bind('reveal.facebox', function()
	{
		$('#facebox').focus();
		$('#reset').click();
		typ = $('#facebox .modul_container').attr('id');
		zaznaczWybrane('jQ-forma2', typ);
		$("#co").autocomplete(
		{
			disabled : false,
			autoFocus : false,
			delay : 300,
			minLength : 2,
			source : baseUrl + '/ajax/index/autocomplete',
			select: function(event, ui) 
			{
				$('#co').val(ui.item.value);
				$('#szukaj').click(); 
			}
		});
	});
	$(document).bind('afterClose.facebox', function()
	{
		countRows();
	});
});

function getClientNumberCard(value)
{    
    if(!isNaN(value)) {
        jQuery('#jQ-error').addClass('dn');
        jQuery('#jQ-error').html('');
        if(value.length > 0) {
            jQuery('#jQ-error').addClass('dn');
            jQuery('#jQ-error').html('');
            
            var timeout = 8000;
            
            jQuery.ajax({
                url: "http://"+www+baseUrl+"/ajax/index/karta/karta/"+value,
                type: "GET",
                timeout: timeout,
                async: true,
                success: function(html) {
                    jQuery('#jQ-lista').html(html);
                    jQuery('a[rel*=facebox]').facebox() ;
                }
            });
            
        } else {
            jQuery('#jQ-error').removeClass('dn');
            jQuery('#jQ-error').html('<div class="o">Podaj minimum 1 cyfry numeru karty.</div>');
        }
    } else {
        jQuery('#jQ-error').removeClass('dn');
        jQuery('#jQ-error').html('<div class="b">Podaj numer karty.</div>');
    }
    jQuery('#jQ-one').addClass('dn');    
}

function appendRow($this)
{
    var checked = $($this).attr('checked');
    if(checked != undefined)
    {
        var ar = $this.value.split('|--|');
        var email = '';
        var sms = '';
        if(ar[1])
        {
            var email = ', e-mail: '+ar[1];
        }
        if(ar[0])
        {
            var sms = ', sms: '+ar[0];
        }

        var hidden = '<input type="hidden" name="post[klient]['+ar[4]+']" value="'+ar+'" />';
        $('#jQ_client').append('<span class="db cb lf"><span class="lf"><b>'+ar[2]+' '+ar[3]+'</b>'+sms+email+'&nbsp; &nbsp;</span> <a onclick="return removeRow(this);" title="usuń z akcji" class="lf del_ico silver"></a>'+hidden+'</span>');
    }
}
function deleteRowPrice(idprz)
{
    var timeout = 8000;
    $.ajax(
	{
        url: base+"/ajax/kalkulator/przedzialusun/id/"+idprz,
        type: "GET",
        timeout: timeout,
        async: true,
        success: function(html)
		{
            $('#jq-rowprice-'+idprz).remove();
            var rows = jQuery('#jQ-price-table tbody tr').length;
            if(rows === 0) jQuery('#jQ-price-table').removeClass('db').addClass('dn').hide('fast');
        }
    });
}
function addRowPrice(idprz)
{
    var timeout = 8000;
    $.ajax(
	{
        url: base+"/ajax/kalkulator/przedzial/id/"+idprz,
        type: "GET",
        timeout: timeout,
        async: true,
        success: function(html)
		{
            jQuery('#jQ-price-table tbody').append(html);
            jQuery('#jQ-price-table').removeClass('dn').show('fast');
        }
    });
}
function appendRowLista($this, typ)
{
    var checked = $($this).attr('checked');
    if(true)
    {
        var ar = $this.value.split('|--|');
        var hidden = '<input type="hidden" id="'+typ+ar[4]+'" name="post['+typ+']['+ar[4]+']" value="'+ar+'" />';
		if(checked != undefined && $('#'+typ+ar[4]).attr('id') == undefined)
        $('#jQ_client_'+typ).append('<span class="db cb lf"><span class="lf">'+ar[3]+' <b>'+ar[1]+'</b> <b>'+ar[0]+'</b>&nbsp;&nbsp;</span><a onclick="return removeRow(this);" title="usuń" class="lf del_ico silver"></a>'+hidden+'</span>');
		if(checked == undefined && $('#'+typ+ar[4]).attr('id') != undefined)
		$('#'+typ+ar[4]).parent().remove();
    }
}

function appendRowAll($this)
{
    jQuery('#'+$this).find('input[type="checkbox"]').each(function()
	{
        var checked = $(this).attr('checked');
        if(checked == undefined)
        {
            var ar = this.value.split('|--|');
            var email = '';
            var sms = '';
            if(ar[1])
            {
                var email = ', e-mail: '+ar[1];
            }
            if(ar[0])
            {
                var sms = ', sms: '+ar[0];
            }
            $(this).attr('checked','true');

            var hidden = '<input type="hidden" name="post[klient]['+ar[4]+']" value="'+ar+'" />';
            $('#jQ_client').append('<span class="db cb lf"><span class="lf"><b>'+ar[2]+' '+ar[3]+'</b>'+sms+email+'&nbsp; &nbsp;</span> <a onclick="return removeRow(this);" title="usuń z akcji" class="lf del_ico silver"></a>'+hidden+'</span>');
        }
    });
}
function appendRowAllLista($this, typ, check)
{
    jQuery('#'+$this).find('input[type="checkbox"]').each(function(i)
	{
        var checked = $(this).attr('checked');
        if(true)
        {
            var ar = this.value.split('|--|');
            $(this).attr('checked', check);
			//console.log(i + " " + ar[0] + " " + ar[1] + " " + $('#'+typ+ar[0]).attr('id'));
			var hidden = '<input type="hidden" id="'+typ+ar[4]+'" name="post['+typ+']['+ar[4]+']" value="'+ar+'" />';
			if(check && $('#'+typ+ar[4]).attr('id') == undefined)
			$('#jQ_client_'+typ).append('<span class="db cb lf"><span class="lf">'+ar[3]+' <b>'+ar[1]+'</b> <b>'+ar[0]+'</b>&nbsp;&nbsp;</span><a onclick="return removeRow(this);" title="usuń" class="lf del_ico silver"></a>'+hidden+'</span>');
			if(!check && $('#'+typ+ar[4]).attr('id') != undefined)
			$('#'+typ+ar[4]).parent().remove();
        }
    });
}

function removeRow($this)
{
    $($this).parent().remove();
}
function mbstrlen(str)
{
    var len = 0;
    for(var i = 0; i < str.length; i++)
	{
        len += str.charCodeAt(i) < 0 || str.charCodeAt(i) > 255 ? 2 : 1;
    }
    return len;
}

function lengthInput($this, output)
{
    $('#'+output).html(mbstrlen($this.value));
}

function toggleC(id, pow)
{
    if(pow) $('#'+pow).addClass('dn').removeClass('db').fadeOut(3000);
    if(id) $('#'+id+'_open').addClass('db').removeClass('dn').fadeIn(3000);
}

function getEmptyCard(value,typ)
{    
    if(!isNaN(value)) {
        jQuery('#jQ-error'+typ).addClass('dn');
        jQuery('#jQ-error'+typ).html('');
        if(value.length == 5) {
            jQuery('#jQ-error'+typ).addClass('dn');
            jQuery('#jQ-error'+typ).html('');
            
            var timeout = 8000;
            
            jQuery.ajax({
                url: "http://"+www+baseUrl+"/ajax/upominki/karta/karta/"+value+'/typ/'+typ,
                type: "GET",
                timeout: timeout,
                async: true,
                success: function(html) {
                    window.console.log(html);
                    jQuery('#jQ-error'+typ).removeClass('dn');
                    if(html){
                        jQuery('#jQ-error'+typ).html('<div class="o">Numer karty jest prawidłowy.</div>');
                    } else {
                        jQuery('#jQ-error'+typ).html('<div class="b">Błędny numer karty.</div>');
                    }
                    
                }
            });
            
        } else {
            jQuery('#jQ-error'+typ).removeClass('dn');
            jQuery('#jQ-error'+typ).html('<div class="o">Podaj 5 cyfr numeru karty.</div>');
        }
    } else {
        jQuery('#jQ-error'+typ).removeClass('dn');
        jQuery('#jQ-error'+typ).html('<div class="b">Podaj numer karty.</div>');
    }
    
}

function getOneCard(id)
{
    jQuery("."+id).each(function()
	{
        jQuery('#'+this.id+'-value').text(this.value);
    });
    jQuery('#jQ-one').removeClass('dn');
}
function requestAjax(href)
{
    var timeout = 8000;
    jQuery.ajax(
	{
        url: href,
        type: "get",
        timeout: timeout,
        async: true,
        success: function(html)
		{
            var obj = html.substr(0,1) == '{' ? jQuery.parseJSON(html) : '';
            if(obj.href != undefined)
			{
                requestAjax(obj.href);
                if(obj.tryb != undefined) jQuery('#jQ_tryb_gift_'+obj.tryb).html(obj.html);
                if(obj.tryb != undefined)
				{
                    tryb = obj.tryb == 0 ? 1 : 0;
                    if(obj.html2 != undefined) jQuery('#jQ_tryb_gift_'+tryb).html(obj.html);
                }
            }
			else
			{
                jQuery('#facebox div.content').html(html);
            }
        }
    });

}

function save(href, id)
{
    var data = $('#'+id).serializeArray();
    if(data[0].value.length == 0)
	{
        jQuery('#jQ-message div').html('1. Pole nazwa jest wymagane.').css('display', 'block');
    }
	else
	{
        jQuery('#jQ-message div').html('').css('display', 'none');
        var timeout = 8000;
        jQuery.ajax(
		{
            url: href,
            type: "post",
            timeout: timeout,
            data: data,
            async: true,
            success: function(html)
			{
                requestAjax(html);
                return false;
            }
        });
    }
}

function requestpostAjax(href, id)
{
    var data = $('#'+id).serializeArray();
    var timeout = 8000;
    jQuery.ajax(
	{
        url: href,
        type: "post",
        timeout: timeout,
        data: data,
        async: true,
        success: function(html)
		{
            var obj = html.substr(0,1) == '{' ? jQuery.parseJSON(html) : '';
            if(obj.href != undefined)
			{
				requestAjax(obj.href);
            }
			else
			{
                jQuery('#facebox div.content').html(html);
            }
        }
    });
}

function addOrder(id, href)
{
    var data = $('#'+id+'-form').serializeArray();
    window.console.log(data);
    var timeout = 8000;
    jQuery.ajax(
	{
        url: href,
        type: "post",
        timeout: timeout,
        data: data,
        async: true,
        success: function(html)
		{
           jQuery('#facebox div.content').html(html);
        }
    });
}
function copyvalue(id, value)
{
    jQuery('#'+id+'-pole').val(value);
}

var checkboxs_checked = false;

function zaznacz(form)
{
	if(document.getElementById(form) == undefined) return;
    var checkboxy = document.getElementById(form).getElementsByTagName('input');
    for(i = 0; i < checkboxy.length; i++)
    {
		if (checkboxy[i].type == 'checkbox')
		{
			checkboxy[i].checked = checkboxs_checked ? false : true;
		}
    }
    checkboxs_checked = checkboxs_checked ? false : true;
}
function zaznaczWybrane(form, typ)
{
	if(document.getElementById(form) == undefined) return;
    var checkboxy = document.getElementById(form).getElementsByTagName('input');
    for(i = 0; i < checkboxy.length; i++)
    {
		if (checkboxy[i].type == 'checkbox')
		{
			var ar = checkboxy[i].value.split('|--|');
			checkboxy[i].checked = ($('#'+typ+ar[0]).attr('id') == undefined) ? false : true;
		}
    }
}

function saveItem(href, id)
{
    var data = $('#'+id).serializeArray();
    var timeout = 8000;
    var tryb = jQuery('input[name="tryb"]').val();
    jQuery.ajax(
	{
        url: href,
        type: "post",
        timeout: timeout,
        data: data,
        async: true,
        success: function(html)
		{
            jQuery('#jQ_tryb_gift_'+tryb).html(html);
            return false;
        }
    });
}
function saveZakup(href, id)
{
    var data = $('#'+id).serializeArray();
    var timeout = 8000;
    jQuery.ajax(
	{
        url: href,
        type: "post",
        timeout: timeout,
        data: data,
        async: true,
        success: function(html)
		{
            jQuery('#facebox div.content').html(html);
        }
    });
}
function searchAjax(href,id,typ)
{
    var data = $('#'+id).serializeArray();
    var timeout = 8000;
    jQuery.ajax(
	{
        url: href,
        type: "get",
        timeout: timeout,
        data: data,
        async: true,
        success: function(html)
		{
            jQuery('#facebox div.content').html(html);
			zaznaczWybrane('jQ-forma2', typ);
        }
    });
}

function getListaKlienci(href, page)
{
    var timeout = 8000;
    jQuery.ajax(
	{
        url: href,
        type: "get",
        timeout: timeout,
        async: true,
        success: function(html)
		{
            jQuery('#facebox div.content').html(html);
        }
    });
}
function wybierzkarta(karta)
{
    jQuery('#jQ_numerkarta').val(karta);
    jQuery('#facebox a.close').trigger('click');
}