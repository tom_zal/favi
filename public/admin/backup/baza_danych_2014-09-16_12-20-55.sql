

-- Dumping structure for table: aktualnosci

CREATE TABLE `aktualnosci` (
  `id` int(11) NULL auto_increment ,
  `typ` int(11) NULL ,
  `temat` varchar(255) NULL ,
  `skrot` text NULL ,
  `tekst` text NULL ,
  `data` datetime NULL ,
  `wyswietl` varchar(11) NULL ,
  `glowna` varchar(11) NULL ,
  `lang` varchar(2) NULL ,
  `status` int(11) NULL ,
  `pozycja` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: aktualnosci



-- Dumping structure for table: allegroaukcje

CREATE TABLE `allegroaukcje` (
  `id` int(11) NULL auto_increment ,
  `id_prod` int(11) NULL ,
  `id_rozm` int(11) NULL ,
  `nr` bigint(11) NULL ,
  `cena` decimal(12,2) NULL ,
  `na_ile` int(11) NULL ,
  `wystawiono` timestamp NULL ,
  `sztuk_wystawiono` int(11) NULL ,
  `sztuk_pozostalo` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: allegroaukcje



-- Dumping structure for table: allegrocenniki

CREATE TABLE `allegrocenniki` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `koszty` text NULL ,
  `opcje_przesylki` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: allegrocenniki



-- Dumping structure for table: allegroparams

CREATE TABLE `allegroparams` (
  `id` int(11) NULL auto_increment ,
  `id_kat` int(11) NULL ,
  `id_sell_form` int(11) NULL ,
  `id_param` varchar(255) NULL ,
  `value` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: allegroparams

INSERT INTO `allegroparams` VALUES('1', '1520', '20364', '0', '0');
INSERT INTO `allegroparams` VALUES('2', '1520', '20699', '0', '0');
INSERT INTO `allegroparams` VALUES('3', '1520', '22148', '0', '');
INSERT INTO `allegroparams` VALUES('4', '122599', '22021', '0', '0');
INSERT INTO `allegroparams` VALUES('5', '121882', '22131', '0', '');


-- Dumping structure for table: allegroraport

CREATE TABLE `allegroraport` (
  `id` int(11) NULL auto_increment ,
  `id_prod` int(11) NULL ,
  `id_rozm` int(11) NULL ,
  `nazwa` varchar(255) NULL ,
  `status` text NULL ,
  `koszt` float NULL ,
  `data` timestamp NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: allegroraport



-- Dumping structure for table: allegroszablony

CREATE TABLE `allegroszablony` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `tekst` longtext NULL ,
  `szablon` varchar(255) NULL ,
  `data` timestamp NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: allegroszablony



-- Dumping structure for table: ankieta

CREATE TABLE `ankieta` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `widoczny` varchar(1) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: ankieta



-- Dumping structure for table: ankietaodpowiedzi

CREATE TABLE `ankietaodpowiedzi` (
  `id` int(11) NULL auto_increment ,
  `nazwaod` varchar(255) NULL ,
  `glosy` int(11) NULL ,
  `idan` int(11) NULL ,
  `pozycja` int(11) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: ankietaodpowiedzi



-- Dumping structure for table: archiwumkontrahenci

CREATE TABLE `archiwumkontrahenci` (
  `id` int(11) NULL auto_increment ,
  `nr_zam` int(11) NULL ,
  `email` varchar(255) NULL ,
  `id_ext` varchar(255) NULL ,
  `symbol` varchar(255) NULL ,
  `nazwa_firmy` varchar(255) NULL ,
  `nazwa` varchar(255) NULL ,
  `nazwisko` varchar(255) NULL ,
  `imie` varchar(255) NULL ,
  `telefon` varchar(255) NULL ,
  `komorka` varchar(255) NULL ,
  `nip` varchar(255) NULL ,
  `pesel` varchar(255) NULL ,
  `regon` varchar(255) NULL ,
  `pkd` varchar(255) NULL ,
  `krs` varchar(255) NULL ,
  `kod` varchar(255) NULL ,
  `miasto` varchar(255) NULL ,
  `powiat` varchar(50) NULL ,
  `ulica` varchar(255) NULL ,
  `nr` varchar(255) NULL ,
  `mieszkanie` int(11) NULL ,
  `wojew` varchar(255) NULL ,
  `kraj` varchar(255) NULL ,
  `inny_adres_dostawy` varchar(2) NULL ,
  `nazwa_firmy_korespondencja` varchar(255) NULL ,
  `imie_korespondencja` varchar(255) NULL ,
  `nazwisko_korespondencja` varchar(255) NULL ,
  `telefon_korespondencja` varchar(30) NULL ,
  `komorka_korespondencja` varchar(255) NULL ,
  `kod_korespondencja` varchar(255) NULL ,
  `miasto_korespondencja` varchar(255) NULL ,
  `powiat_korespondencja` varchar(50) NULL ,
  `ulica_korespondencja` varchar(255) NULL ,
  `nr_korespondencja` varchar(255) NULL ,
  `mieszkanie_korespondencja` int(11) NULL ,
  `wojew_korespondencja` varchar(255) NULL ,
  `kraj_korespondencja` varchar(255) NULL ,
  `faktura` varchar(3) NULL ,
  `potwierdzenie` tinyint(1) NULL ,
  `potwierdzenie_kod` varchar(255) NULL ,
  `wartosc_zamowienia` decimal(12,2) NULL ,
  `rabat_upust` decimal(12,2) NULL ,
  `rabat_wartosc` decimal(12,2) NULL ,
  `rabatotrz` decimal(12,2) NULL ,
  `rabatwyk` decimal(12,2) NULL ,
  `rabatpolecajacy` decimal(12,2) NULL ,
  `kwotazaplata` decimal(12,2) NULL ,
  `koszt_dostawy` decimal(12,2) NULL ,
  `wysylka` varchar(255) NULL ,
  `status` varchar(255) NULL ,
  `uwagi` varchar(500) NULL ,
  `data` timestamp NULL ,
  `rodzaj_klienta` int(11) NULL ,
  `plec` varchar(1) NULL ,
  `data_rej` timestamp NULL ,
  `online` varchar(255) NULL ,
  `online_typ` varchar(255) NULL ,
  `www` varchar(255) NULL ,
  `prowizja` int(11) NULL ,
  `polecajacy` int(11) NULL ,
  `upominekwyk` varchar(255) NULL ,
  `lang` varchar(2) NULL ,
  `fa_vat` varchar(2) NULL ,
  `gratisy` text NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: archiwumkontrahenci

INSERT INTO `archiwumkontrahenci` VALUES('3', '2', 'james890@gmail.com', '', '', '', '', 'mmmmm', 'Jakub', '234234', '', '', '', '', '', '', '32-657', 'dfhd', '', 'sdfwegf', '1234', '234', '', '', '0', '', '', '', '', '', '', '', '', '', '', '0', '', '', '0', '0', '7c9eaf8743521b55b9ad658d4a08185a', '12.30', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '12.00', 'za pobraniem - Kurier', 'wyslane', 'test', '2012-05-11 00:00:00', '0', 'M', '2011-07-11 15:20:48', '', 'nowa', '', '0', '0', '', 'pl', '', '');
INSERT INTO `archiwumkontrahenci` VALUES('4', '3', 'james890@gmail.com', '', '', '', '', 'mmmmm', 'Jakub', '234234', '', '', '', '', '', '', '32-657', 'dfhd', '', 'sdfwegf', '1234', '234', '', '', '0', '', '', '', '', '', '', '', '', '', '', '0', '', '', '0', '0', '7c9eaf8743521b55b9ad658d4a08185a', '7.38', '0.00', '0.01', '0.00', '0.00', '0.00', '0.00', '12.00', 'za pobraniem - Kurier', 'wyslane', 'test', '2012-05-11 00:00:00', '0', 'M', '2011-07-11 15:20:48', '', 'zako?czona', '', '0', '0', '', 'pl', '', '');


-- Dumping structure for table: archiwumprodukty

CREATE TABLE `archiwumprodukty` (
  `id` int(11) NULL auto_increment ,
  `id_prod` int(11) NULL ,
  `id_zam` int(11) NULL ,
  `nazwa` varchar(255) NULL ,
  `oznaczenie` varchar(255) NULL ,
  `netto` decimal(12,2) NULL ,
  `brutto` decimal(12,2) NULL ,
  `cena_netto` decimal(12,2) NULL ,
  `cena_brutto` decimal(12,2) NULL ,
  `ilosc` decimal(12,2) NULL ,
  `dostepnosc` int(11) NULL ,
  `vat` decimal(12,2) NULL ,
  `wartosc_netto` decimal(12,2) NULL ,
  `wartosc` decimal(12,2) NULL ,
  `rabat` decimal(12,2) NULL ,
  `upust` decimal(12,2) NULL ,
  `rozmiar` varchar(255) NULL ,
  `kolor` varchar(255) NULL ,
  `atrybut` int(11) NULL ,
  `atrybuty` text NULL ,
  `jedn` varchar(20) NULL ,
  `jedn_miary` varchar(20) NULL ,
  `jedn_miary_typ` varchar(5) NULL ,
  `jedn_miary_zb` varchar(255) NULL ,
  `jedn_miary_zb_ile` int(11) NULL ,
  `jedn_miary_zb_typ` varchar(5) NULL ,
  `opis` text NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: archiwumprodukty

INSERT INTO `archiwumprodukty` VALUES('1', '1', '3', 'Produkt testowy', 'Test', '0.00', '0.00', '10.00', '12.30', '1.00', '0', '0.00', '10.00', '12.30', '0.00', '0.00', '1', '', '0', '', 'szt.', '', 'int', '', '0', 'int', '');
INSERT INTO `archiwumprodukty` VALUES('2', '1', '4', 'Produkt testowy', 'Test', '0.00', '0.00', '1.00', '1.23', '6.00', '0', '0.00', '6.00', '7.38', '0.10', '0.00', '1', '', '0', '', 'szt.', 'szt.', 'int', 'paleta', '1000', 'int', '');


-- Dumping structure for table: artykuly

CREATE TABLE `artykuly` (
  `id` int(11) NULL auto_increment ,
  `typ` int(11) NULL ,
  `temat` varchar(255) NULL ,
  `skrot` text NULL ,
  `tekst` text NULL ,
  `data` datetime NULL ,
  `wyswietl` varchar(11) NULL ,
  `glowna` varchar(11) NULL ,
  `lang` varchar(2) NULL ,
  `status` int(11) NULL ,
  `pozycja` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: artykuly



-- Dumping structure for table: atrybuty

CREATE TABLE `atrybuty` (
  `id` int(11) NULL auto_increment ,
  `id_gr` int(11) NULL ,
  `id_prod` int(11) NULL ,
  `nazwa` varchar(255) NULL ,
  `status` varchar(1) NULL ,
  `wyszukiwarka` varchar(1) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: atrybuty

INSERT INTO `atrybuty` VALUES('1', '2', '0', 'x1', '1', '1', 'pl');
INSERT INTO `atrybuty` VALUES('2', '3', '0', '2.34', '1', '1', 'pl');


-- Dumping structure for table: atrybutyceny

CREATE TABLE `atrybutyceny` (
  `id` int(11) NULL auto_increment ,
  `id_prod` int(11) NULL ,
  `id_ext` varchar(50) NULL ,
  `id_oferta` int(11) NULL ,
  `dostepnosc` int(11) NULL ,
  `cena_netto` decimal(12,2) NULL ,
  `cena_brutto` decimal(12,2) NULL ,
  `cena_promocji_n` decimal(12,2) NULL ,
  `cena_promocji_b` decimal(12,2) NULL ,
  `cena_netto_hurt` decimal(12,2) NULL ,
  `cena_brutto_hurt` decimal(12,2) NULL ,
  `cena_promocji_n_hurt` decimal(12,2) NULL ,
  `cena_promocji_b_hurt` decimal(12,2) NULL ,
  `cena_allegro` decimal(12,2) NULL ,
  `atr0` int(11) NULL ,
  `atr1` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: atrybutyceny



-- Dumping structure for table: atrybutygrupy

CREATE TABLE `atrybutygrupy` (
  `id` int(11) NULL auto_increment ,
  `id_rodzaj` int(11) NULL ,
  `id_prod` int(11) NULL ,
  `id_kat` text NULL ,
  `nazwa` varchar(255) NULL ,
  `wyszukiwarka` varchar(1) NULL ,
  `status` varchar(1) NULL ,
  `glowny` varchar(1) NULL ,
  `lang` varchar(2) NULL ,
  `typatrybut` varchar(15) NULL ,
  `jedn_miary` varchar(255) NULL ,
  `jedn_miary_typ` varchar(5) NULL ,
  `poz` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: atrybutygrupy

INSERT INTO `atrybutygrupy` VALUES('1', '0', '0', '2', 'test', '1', '1', '0', 'pl', 'tekst', '', 'tekst', '0');
INSERT INTO `atrybutygrupy` VALUES('2', '1', '0', '1;2', 'xxx', '1', '1', '0', 'pl', 'multi', '', 'int', '0');
INSERT INTO `atrybutygrupy` VALUES('3', '0', '0', '1', 'yyy', '1', '1', '0', 'pl', 'lista', '', 'float', '0');
INSERT INTO `atrybutygrupy` VALUES('4', '0', '0', '', 'dtgetge', '1', '1', '0', 'pl', 'wybor', '', 'tekst', '0');


-- Dumping structure for table: atrybutypowiazania

CREATE TABLE `atrybutypowiazania` (
  `id` int(11) NULL auto_increment ,
  `id_og` int(11) NULL ,
  `id_at` int(11) NULL ,
  `wartosc` varchar(255) NULL ,
  `typatrybut` varchar(15) NULL ,
  `id_gr` int(11) NULL ,
  `ceny` varchar(2) NULL ,
  `cenyTryb` varchar(2) NULL ,
  `cena_netto` decimal(12,2) NULL ,
  `cena_brutto` decimal(12,2) NULL ,
  `cena_promocji_n` decimal(12,2) NULL ,
  `cena_promocji_b` decimal(12,2) NULL ,
  `cena_netto_hurt` decimal(12,2) NULL ,
  `cena_brutto_hurt` decimal(12,2) NULL ,
  `cena_promocji_n_hurt` decimal(12,2) NULL ,
  `cena_promocji_b_hurt` decimal(12,2) NULL ,
  `pozycja` int(11) NULL ,
  `img` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: atrybutypowiazania

INSERT INTO `atrybutypowiazania` VALUES('23', '1', '1', '', 'multi', '2', '1', '1', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0', '202c471c6c171b9b52377da347f17fc3.jpg');


-- Dumping structure for table: confsms

CREATE TABLE `confsms` (
  `id` int(11) NULL auto_increment ,
  `login` varchar(60) NULL ,
  `password` varchar(255) NULL ,
  `typ_wiadomosci` tinyint(1) NULL ,
  `pole_nadawca` varchar(60) NULL ,
  `admin_sms` varchar(60) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: confsms

INSERT INTO `confsms` VALUES('1', '', '', '1', '', '');


-- Dumping structure for table: cv

CREATE TABLE `cv` (
  `id` int(11) NULL auto_increment ,
  `email` varchar(255) NULL ,
  `nazwisko` varchar(255) NULL ,
  `imie` varchar(255) NULL ,
  `telefon` varchar(255) NULL ,
  `kod` varchar(255) NULL ,
  `miasto` varchar(255) NULL ,
  `ulica` varchar(255) NULL ,
  `nr` varchar(255) NULL ,
  `mieszkanie` int(11) NULL ,
  `plec` varchar(1) NULL ,
  `data_ur` date NULL ,
  `wyksztalcenie` text NULL ,
  `doswiadczenie` text NULL ,
  `umiejetnosci` text NULL ,
  `zainteresowania` text NULL ,
  `opis` text NULL ,
  `img` varchar(255) NULL ,
  `data_wysylki` timestamp NULL ,
  `woj` varchar(255) NULL ,
  `data_ur_opis` varchar(255) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: cv



-- Dumping structure for table: dealerzy

CREATE TABLE `dealerzy` (
  `id` int(11) NULL auto_increment ,
  `firma` varchar(255) NULL ,
  `miasto` varchar(255) NULL ,
  `kod` varchar(255) NULL ,
  `ulica` varchar(255) NULL ,
  `kier` varchar(255) NULL ,
  `tel` varchar(255) NULL ,
  `fax` varchar(255) NULL ,
  `mail` varchar(255) NULL ,
  `woj` varchar(255) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: dealerzy



-- Dumping structure for table: dostepnosc

CREATE TABLE `dostepnosc` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(20) NULL ,
  `min` int(11) NULL ,
  `max` int(11) NULL ,
  `kolor` varchar(6) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: dostepnosc

INSERT INTO `dostepnosc` VALUES('1', 'ma?o', '0', '10', 'ff0000', 'pl');
INSERT INTO `dostepnosc` VALUES('2', '?rednio', '11', '20', 'ffff00', 'pl');
INSERT INTO `dostepnosc` VALUES('3', 'du?o', '21', '99999', '00ff00', 'pl');


-- Dumping structure for table: dpd

CREATE TABLE `dpd` (
  `id` int(11) NULL auto_increment ,
  `id_zam` int(11) NULL ,
  `id_user` int(11) NULL ,
  `SessionId` int(11) NULL ,
  `reference` varchar(20) NULL ,
  `packageId` int(11) NULL ,
  `parcelId` int(11) NULL ,
  `waybill` varchar(20) NULL ,
  `DocumentId` int(11) NULL ,
  `data` timestamp NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: dpd



-- Dumping structure for table: facebooks

CREATE TABLE `facebooks` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `www` varchar(255) NULL ,
  `userid` varchar(255) NULL ,
  `appid` varchar(255) NULL ,
  `secret` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: facebooks

INSERT INTO `facebooks` VALUES('1', 'Strona prywatna', 'http://www.facebook.com/pages/panel', '100002607420492', '121223374704405', 'e588945fe980d19acdb0a284090d3d59');


-- Dumping structure for table: facebooksprods

CREATE TABLE `facebooksprods` (
  `id` int(11) NULL auto_increment ,
  `id_strona` int(11) NULL ,
  `id_produkt` int(11) NULL ,
  `id_post` varchar(50) NULL ,
  `data` timestamp NULL on update CURRENT_TIMESTAMP ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: facebooksprods



-- Dumping structure for table: galeria

CREATE TABLE `galeria` (
  `id` int(11) NULL auto_increment ,
  `wlasciciel` int(11) NULL ,
  `id_rozm` int(11) NULL ,
  `typ` varchar(20) NULL ,
  `img` varchar(50) NULL ,
  `nazwa` varchar(255) NULL ,
  `glowne` varchar(1) NULL ,
  `poz` int(11) NULL ,
  `lang` varchar(2) NULL ,
  `wyswietl` varchar(2) NULL ,
  `zdj` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: galeria

INSERT INTO `galeria` VALUES('1', '1', '0', 'oferta', '81793133ae15359b007304587b99955a.jpeg', 'brak', 'T', '0', 'pl', '1', '');
INSERT INTO `galeria` VALUES('9', '1', '0', 'oferta', 'bdaa5551c38a14868ec8157fc1c78c37.jpeg', 'x', 'N', '0', 'pl', '1', 'bdaa5551c38a14868ec8157fc1c78c37.jpeg');


-- Dumping structure for table: ileonline

CREATE TABLE `ileonline` (
  `id` int(11) NULL auto_increment ,
  `czas` int(15) NULL ,
  `ip` varchar(40) NULL ,
  `plik` varchar(100) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: ileonline

INSERT INTO `ileonline` VALUES('964', '1410862855', '127.0.0.1', '/panel/public/index.php');


-- Dumping structure for table: import

CREATE TABLE `import` (
  `id` int(11) NULL auto_increment ,
  `tryb` varchar(20) NULL ,
  `rodzaj` varchar(20) NULL ,
  `typ` varchar(20) NULL ,
  `id_ext` int(11) NULL ,
  `old` text NULL ,
  `new` text NULL ,
  `akcja` varchar(20) NULL ,
  `data` datetime NULL ,
  `czas` timestamp NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: import



-- Dumping structure for table: kategorie

CREATE TABLE `kategorie` (
  `id` int(11) NULL auto_increment ,
  `typ` varchar(20) NULL ,
  `rodzic` int(11) NULL ,
  `nazwa` varchar(255) NULL ,
  `img` text NULL ,
  `img_opis` varchar(255) NULL ,
  `img1` varchar(255) NULL ,
  `img1_opis` varchar(255) NULL ,
  `img2` varchar(255) NULL ,
  `img2_opis` varchar(255) NULL ,
  `lang` varchar(2) NULL ,
  `pozycja` int(11) NULL ,
  `link` varchar(255) NULL ,
  `route_id` int(11) NULL ,
  `allegro` varchar(255) NULL ,
  `ebay` varchar(255) NULL ,
  `short` text NULL ,
  `opis` text NULL ,
  `sprzedawca` int(11) NULL ,
  `rabat` decimal(12,2) NULL ,
  `marza` float NULL ,
  `widoczny` varchar(2) NULL ,
  `data` timestamp NULL ,
  `grupa` int(11) NULL ,
  `szablon` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: kategorie

INSERT INTO `kategorie` VALUES('1', 'kategorie', '0', 'Kategoria testowa2', 'baner.jpg', '', '', '', '', '', 'pl', '0', 'Kategoria-testowa2', '1001', '-1;-1;-1;-1;-1;-1', '11232;35064;-1;-1;-1;-1', '', '', '0', '0.00', '0', '1', '2014-01-07 11:14:36', '0', '0');
INSERT INTO `kategorie` VALUES('2', 'kategorie', '1', 'Test', 'brak.jpg', '', '', '', '', '', 'pl', '1', 'test', '1002', '', '', '', '', '0', '0.00', '0', '1', '0000-00-00 00:00:00', '0', '0');


-- Dumping structure for table: kategoriegrupy

CREATE TABLE `kategoriegrupy` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: kategoriegrupy

INSERT INTO `kategoriegrupy` VALUES('1', 'Testowa', 'pl');


-- Dumping structure for table: katprod

CREATE TABLE `katprod` (
  `id_kat_prod` int(11) NULL auto_increment ,
  `id_prod` int(11) NULL ,
  `id_kat` int(11) NULL ,
  `typ` varchar(20) NULL ,
  PRIMARY KEY  (`id_kat_prod`)
);


-- Dumping data for table: katprod

INSERT INTO `katprod` VALUES('1', '1', '1', 'kategorie');


-- Dumping structure for table: kolorproduktu

CREATE TABLE `kolorproduktu` (
  `id` int(11) NULL auto_increment ,
  `id_produktu` int(11) NULL ,
  `id_koloru` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: kolorproduktu



-- Dumping structure for table: kolory

CREATE TABLE `kolory` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `color` varchar(6) NULL ,
  `img` varchar(255) NULL ,
  `pozycja` int(11) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: kolory

INSERT INTO `kolory` VALUES('1', 'czarny', '000000', '', '0', 'pl');
INSERT INTO `kolory` VALUES('2', 'bia?y', '000000', '', '0', 'pl');


-- Dumping structure for table: kontrahenci

CREATE TABLE `kontrahenci` (
  `id` int(11) NULL auto_increment ,
  `email` varchar(255) NULL ,
  `id_ext` varchar(255) NULL ,
  `symbol` varchar(255) NULL ,
  `nazwa_firmy` varchar(255) NULL ,
  `nazwa` varchar(255) NULL ,
  `nazwisko` varchar(255) NULL ,
  `imie` varchar(255) NULL ,
  `telefon` varchar(255) NULL ,
  `komorka` varchar(255) NULL ,
  `grupa` int(11) NULL ,
  `nip` varchar(255) NULL ,
  `pesel` varchar(255) NULL ,
  `regon` varchar(255) NULL ,
  `pkd` varchar(255) NULL ,
  `krs` varchar(255) NULL ,
  `kod` varchar(255) NULL ,
  `miasto` varchar(255) NULL ,
  `powiat` varchar(50) NULL ,
  `ulica` varchar(255) NULL ,
  `nr` varchar(255) NULL ,
  `mieszkanie` int(11) NULL ,
  `wojew` varchar(255) NULL ,
  `kraj` varchar(255) NULL ,
  `inny_adres_dostawy` int(11) NULL ,
  `nazwa_firmy_korespondencja` varchar(255) NULL ,
  `imie_korespondencja` varchar(255) NULL ,
  `nazwisko_korespondencja` varchar(255) NULL ,
  `telefon_korespondencja` varchar(30) NULL ,
  `komorka_korespondencja` varchar(255) NULL ,
  `kod_korespondencja` varchar(255) NULL ,
  `miasto_korespondencja` varchar(255) NULL ,
  `powiat_korespondencja` varchar(50) NULL ,
  `ulica_korespondencja` varchar(255) NULL ,
  `nr_korespondencja` varchar(255) NULL ,
  `mieszkanie_korespondencja` int(11) NULL ,
  `wojew_korespondencja` varchar(255) NULL ,
  `kraj_korespondencja` varchar(255) NULL ,
  `haslo` varchar(255) NULL ,
  `faktura` varchar(3) NULL ,
  `weryfikacja` varchar(2) NULL ,
  `kod_weryfikacja` varchar(255) NULL ,
  `weryfikacja_admin` varchar(2) NULL ,
  `rodzaj_klienta` tinyint(1) NULL ,
  `plec` varchar(1) NULL ,
  `data_rej` timestamp NULL ,
  `data` date NULL ,
  `www` varchar(255) NULL ,
  `polecajacy` int(11) NULL ,
  `konto` decimal(12,2) NULL ,
  `id_zamow_info` int(11) NULL ,
  `karta` varchar(255) NULL ,
  `blokada` varchar(2) NULL ,
  `blokada_lojal` varchar(2) NULL ,
  `rabat` decimal(12,2) NULL ,
  `rabatotrz` decimal(12,2) NULL ,
  `rabatwyk` decimal(12,2) NULL ,
  `rabatpolecajacy` decimal(12,2) NULL ,
  `kartaaktywna` varchar(2) NULL ,
  `lubie` varchar(512) NULL ,
  `kupuje` varchar(512) NULL ,
  `opis` text NULL ,
  `facebook_id` varchar(255) NULL ,
  `facebook_rej` varchar(2) NULL ,
  `lang` varchar(2) NULL ,
  `widoczny` varchar(2) NULL ,
  `pliki` varchar(512) NULL ,
  `newsletter` tinyint(1) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: kontrahenci

INSERT INTO `kontrahenci` VALUES('4', 'james890@gmail.com', '', '', '', '', 'Nowak', 'Jakub', '3242352', '', '0', '774-00-01-454', '', '', '', '', '09-411', 'P?ock', '', 'sverdvtrb', '1234', '234', '', 'Polska', '0', '', '', '', '', '', '', '', '', '', '', '0', '', '', '21232f297a57a5a743894a0e4a801fc3', '0', '1', 'e889449d4c81e35716e7bf0e09511314', '1', '0', 'M', '2012-01-01 16:38:00', '0000-00-00', '', '0', '0.00', '4', '00000', '0', '0', '3.00', '0.00', '0.00', '0.00', '', '', '', '', '', '', 'pl', '1', '', '0');
INSERT INTO `kontrahenci` VALUES('5', 'jakub.mielnikiewicz@gmail.com', '', '', 'xxxxxxx xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx xxxxxxx', '', 'xxxxxxx', 'srevre', '345234234', '', '0', '', '', '', '', '', '32-444', 'dfvdfbgr', '', 'erfergvgtr', '3', '0', '?wi?tokrzyskie', 'Polska', '0', '', '', '', '', '', '', '', '', '', '', '0', '', 'Polska', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '1', '0', 'c83dd29c32c166cde90ddee7f43eaaec', '1', '0', '', '2013-05-24 09:00:50', '0000-00-00', '', '0', '0.00', '0', '', '0', '0', '0.00', '0.00', '0.00', '0.00', '', '', '', '', '', '', 'pl', '1', 'a:1:{s:6:\&quot;LAIEUG\&quot;;s:16:\&quot;gazetka frac.jpg\&quot;;}', '0');


-- Dumping structure for table: kontrahencigrupy

CREATE TABLE `kontrahencigrupy` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: kontrahencigrupy



-- Dumping structure for table: kontrahencilogin

CREATE TABLE `kontrahencilogin` (
  `id` int(11) NULL auto_increment ,
  `typ` varchar(20) NULL ,
  `id_kontr` int(11) NULL ,
  `zalogowano` timestamp NULL ,
  `odswiezono` datetime NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: kontrahencilogin

INSERT INTO `kontrahencilogin` VALUES('1', '', '4', '2012-05-11 12:54:08', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('2', '', '4', '2012-05-11 13:39:09', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('3', '', '4', '2012-05-11 14:31:58', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('4', '', '4', '2012-12-13 10:04:07', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('5', 'kontrahenci', '4', '0000-00-00 00:00:00', '2013-02-15 14:34:31');
INSERT INTO `kontrahencilogin` VALUES('6', 'kontrahenci', '4', '2013-02-18 08:54:36', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('7', 'kontrahenci', '4', '0000-00-00 00:00:00', '2013-04-26 12:49:50');
INSERT INTO `kontrahencilogin` VALUES('8', 'login', '2', '2013-02-18 12:30:33', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('9', 'login', '2', '0000-00-00 00:00:00', '2013-02-18 15:05:45');
INSERT INTO `kontrahencilogin` VALUES('10', 'login', '2', '2013-03-06 15:22:05', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('11', 'login', '2', '0000-00-00 00:00:00', '2013-03-06 16:16:29');
INSERT INTO `kontrahencilogin` VALUES('12', 'login', '2', '2013-03-27 08:14:12', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('13', 'login', '2', '0000-00-00 00:00:00', '2013-03-27 12:05:36');
INSERT INTO `kontrahencilogin` VALUES('14', 'login', '2', '2013-04-03 09:34:17', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('15', 'login', '2', '0000-00-00 00:00:00', '2013-04-03 12:35:27');
INSERT INTO `kontrahencilogin` VALUES('16', 'login', '2', '2013-04-24 12:27:21', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('17', 'login', '2', '0000-00-00 00:00:00', '2013-04-24 12:29:16');
INSERT INTO `kontrahencilogin` VALUES('18', 'login', '2', '2013-04-25 10:24:42', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('19', 'login', '2', '0000-00-00 00:00:00', '2013-04-25 14:20:05');
INSERT INTO `kontrahencilogin` VALUES('20', 'login', '2', '2013-04-25 14:20:11', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('21', 'login', '2', '0000-00-00 00:00:00', '2013-04-25 16:41:20');
INSERT INTO `kontrahencilogin` VALUES('22', 'login', '2', '2013-04-26 09:53:01', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('23', 'login', '2', '0000-00-00 00:00:00', '2013-04-26 15:21:59');
INSERT INTO `kontrahencilogin` VALUES('24', 'login', '2', '2013-04-26 15:54:23', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('25', 'login', '2', '0000-00-00 00:00:00', '2013-04-26 15:58:37');
INSERT INTO `kontrahencilogin` VALUES('26', 'login', '2', '2013-04-26 15:58:43', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('27', 'login', '2', '0000-00-00 00:00:00', '2013-04-26 17:53:21');
INSERT INTO `kontrahencilogin` VALUES('28', 'login', '2', '2013-05-16 11:47:52', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('29', 'login', '2', '0000-00-00 00:00:00', '2013-05-16 15:24:51');
INSERT INTO `kontrahencilogin` VALUES('30', 'login', '2', '2013-05-17 09:05:23', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('31', 'login', '2', '0000-00-00 00:00:00', '2013-05-17 09:08:52');
INSERT INTO `kontrahencilogin` VALUES('32', 'login', '2', '2013-05-23 14:01:43', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('33', 'login', '2', '0000-00-00 00:00:00', '2013-05-23 17:39:48');
INSERT INTO `kontrahencilogin` VALUES('34', 'login', '2', '2013-05-24 08:56:05', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('35', 'login', '2', '0000-00-00 00:00:00', '2013-05-24 09:05:13');
INSERT INTO `kontrahencilogin` VALUES('36', 'kontrahenci', '5', '2013-05-24 09:00:54', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('37', 'kontrahenci', '4', '2013-05-24 12:31:26', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('38', 'kontrahenci', '4', '0000-00-00 00:00:00', '2013-05-24 12:34:28');
INSERT INTO `kontrahencilogin` VALUES('39', 'login', '2', '2013-05-24 12:35:18', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('40', 'login', '2', '0000-00-00 00:00:00', '2013-05-24 13:12:10');
INSERT INTO `kontrahencilogin` VALUES('41', 'kontrahenci', '4', '2013-05-24 12:36:37', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('42', 'kontrahenci', '4', '0000-00-00 00:00:00', '2013-05-24 13:11:58');
INSERT INTO `kontrahencilogin` VALUES('43', 'login', '2', '2013-07-04 11:38:12', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('44', 'login', '2', '0000-00-00 00:00:00', '2013-07-04 11:38:48');
INSERT INTO `kontrahencilogin` VALUES('45', 'login', '2', '2013-07-04 11:38:55', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('46', 'login', '2', '0000-00-00 00:00:00', '2013-07-04 15:50:45');
INSERT INTO `kontrahencilogin` VALUES('47', 'login', '2', '2013-07-17 08:32:11', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('48', 'login', '2', '0000-00-00 00:00:00', '2013-07-17 08:32:21');
INSERT INTO `kontrahencilogin` VALUES('49', 'login', '2', '2013-07-19 15:06:53', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('50', 'login', '2', '0000-00-00 00:00:00', '2013-07-19 17:16:43');
INSERT INTO `kontrahencilogin` VALUES('51', 'login', '2', '2013-08-02 08:41:37', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('52', 'login', '2', '0000-00-00 00:00:00', '2013-08-02 08:41:52');
INSERT INTO `kontrahencilogin` VALUES('53', 'login', '2', '2013-08-02 11:33:08', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('54', 'login', '2', '0000-00-00 00:00:00', '2013-08-02 11:34:28');
INSERT INTO `kontrahencilogin` VALUES('55', 'login', '2', '2013-08-29 12:17:27', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('56', 'login', '2', '0000-00-00 00:00:00', '2013-08-29 15:41:22');
INSERT INTO `kontrahencilogin` VALUES('57', 'login', '2', '2013-08-30 11:47:20', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('58', 'login', '2', '0000-00-00 00:00:00', '2013-08-30 13:41:21');
INSERT INTO `kontrahencilogin` VALUES('59', 'login', '2', '2013-09-11 11:58:02', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('60', 'login', '2', '0000-00-00 00:00:00', '2013-09-11 11:59:43');
INSERT INTO `kontrahencilogin` VALUES('61', 'login', '2', '2013-09-27 14:04:17', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('62', 'login', '2', '0000-00-00 00:00:00', '2013-09-27 14:04:46');
INSERT INTO `kontrahencilogin` VALUES('63', 'login', '2', '2013-09-27 14:16:21', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('64', 'login', '2', '0000-00-00 00:00:00', '2013-09-27 15:23:40');
INSERT INTO `kontrahencilogin` VALUES('65', 'login', '2', '2013-11-07 10:53:12', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('66', 'login', '2', '0000-00-00 00:00:00', '2013-11-07 11:13:07');
INSERT INTO `kontrahencilogin` VALUES('67', 'login', '2', '2013-11-08 11:10:38', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('68', 'login', '2', '0000-00-00 00:00:00', '2013-11-08 11:28:03');
INSERT INTO `kontrahencilogin` VALUES('69', 'login', '2', '2013-11-25 08:38:35', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('70', 'login', '2', '0000-00-00 00:00:00', '2013-11-25 08:40:03');
INSERT INTO `kontrahencilogin` VALUES('71', 'login', '1', '0000-00-00 00:00:00', '2013-12-16 09:16:27');
INSERT INTO `kontrahencilogin` VALUES('72', 'login', '2', '2013-12-16 09:16:31', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('73', 'login', '2', '0000-00-00 00:00:00', '2014-01-07 11:14:58');
INSERT INTO `kontrahencilogin` VALUES('74', 'kontrahenci', '13', '0000-00-00 00:00:00', '2013-12-20 10:33:46');
INSERT INTO `kontrahencilogin` VALUES('75', 'kontrahenci', '13', '0000-00-00 00:00:00', '2013-12-20 10:33:49');
INSERT INTO `kontrahencilogin` VALUES('76', 'kontrahenci', '13', '0000-00-00 00:00:00', '2013-12-20 10:43:07');
INSERT INTO `kontrahencilogin` VALUES('77', 'kontrahenci', '13', '0000-00-00 00:00:00', '2013-12-20 10:43:08');
INSERT INTO `kontrahencilogin` VALUES('78', 'kontrahenci', '13', '0000-00-00 00:00:00', '2013-12-20 11:01:16');
INSERT INTO `kontrahencilogin` VALUES('79', 'kontrahenci', '13', '0000-00-00 00:00:00', '2013-12-20 11:01:17');
INSERT INTO `kontrahencilogin` VALUES('80', 'login', '2', '2014-02-03 13:37:10', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('81', 'login', '2', '0000-00-00 00:00:00', '2014-02-03 15:53:01');
INSERT INTO `kontrahencilogin` VALUES('82', 'kontrahenci', '4', '2014-02-03 14:49:30', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('83', 'kontrahenci', '4', '0000-00-00 00:00:00', '2014-02-03 14:57:33');
INSERT INTO `kontrahencilogin` VALUES('84', 'login', '2', '2014-02-04 11:39:41', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('85', 'login', '2', '0000-00-00 00:00:00', '2014-02-04 14:12:54');
INSERT INTO `kontrahencilogin` VALUES('86', 'login', '2', '2014-02-05 12:15:10', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('87', 'login', '2', '0000-00-00 00:00:00', '2014-02-05 14:57:43');
INSERT INTO `kontrahencilogin` VALUES('88', 'login', '2', '2014-02-07 14:03:11', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('89', 'login', '2', '0000-00-00 00:00:00', '2014-02-07 14:03:12');
INSERT INTO `kontrahencilogin` VALUES('90', 'login', '2', '2014-02-14 16:55:43', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('91', 'login', '2', '0000-00-00 00:00:00', '2014-02-14 16:55:56');
INSERT INTO `kontrahencilogin` VALUES('92', 'login', '2', '2014-02-14 16:56:46', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('93', 'login', '2', '0000-00-00 00:00:00', '2014-02-14 18:26:22');
INSERT INTO `kontrahencilogin` VALUES('94', 'login', '2', '2014-02-14 17:18:36', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('95', 'login', '2', '2014-02-24 12:29:20', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('96', 'login', '2', '0000-00-00 00:00:00', '2014-02-24 12:31:16');
INSERT INTO `kontrahencilogin` VALUES('97', 'login', '2', '2014-02-24 15:29:48', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('98', 'login', '2', '0000-00-00 00:00:00', '2014-02-24 16:34:00');
INSERT INTO `kontrahencilogin` VALUES('99', 'login', '2', '2014-02-25 08:18:26', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('100', 'login', '2', '0000-00-00 00:00:00', '2014-02-25 09:55:23');
INSERT INTO `kontrahencilogin` VALUES('101', 'login', '2', '2014-03-28 09:40:39', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('102', 'login', '2', '0000-00-00 00:00:00', '2014-03-28 11:18:35');
INSERT INTO `kontrahencilogin` VALUES('103', 'login', '2', '2014-04-07 08:17:25', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('104', 'login', '2', '0000-00-00 00:00:00', '2014-04-07 13:43:11');
INSERT INTO `kontrahencilogin` VALUES('105', 'login', '2', '2014-04-09 15:39:43', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('106', 'login', '2', '0000-00-00 00:00:00', '2014-04-09 15:48:17');
INSERT INTO `kontrahencilogin` VALUES('107', 'login', '2', '2014-04-15 11:32:20', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('108', 'login', '2', '0000-00-00 00:00:00', '2014-04-15 12:19:11');
INSERT INTO `kontrahencilogin` VALUES('109', 'login', '2', '2014-04-18 09:18:21', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('110', 'login', '2', '0000-00-00 00:00:00', '2014-04-18 09:48:35');
INSERT INTO `kontrahencilogin` VALUES('111', 'login', '2', '2014-05-06 14:56:48', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('112', 'login', '2', '0000-00-00 00:00:00', '2014-05-06 16:10:05');
INSERT INTO `kontrahencilogin` VALUES('113', 'login', '2', '2014-05-07 11:09:56', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('114', 'login', '2', '0000-00-00 00:00:00', '2014-05-07 13:13:07');
INSERT INTO `kontrahencilogin` VALUES('115', 'kontrahenci', '4', '2014-05-07 11:22:56', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('116', 'kontrahenci', '4', '0000-00-00 00:00:00', '2014-05-07 11:32:37');
INSERT INTO `kontrahencilogin` VALUES('117', 'login', '2', '2014-05-13 13:02:21', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('118', 'login', '2', '0000-00-00 00:00:00', '2014-05-13 13:24:41');
INSERT INTO `kontrahencilogin` VALUES('119', 'kontrahenci', '4', '2014-05-13 13:03:47', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('120', 'kontrahenci', '4', '0000-00-00 00:00:00', '2014-05-13 13:16:54');
INSERT INTO `kontrahencilogin` VALUES('121', 'login', '2', '2014-07-08 09:58:49', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('122', 'login', '2', '0000-00-00 00:00:00', '2014-07-08 11:49:44');
INSERT INTO `kontrahencilogin` VALUES('123', 'login', '2', '2014-07-22 09:03:38', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('124', 'login', '2', '0000-00-00 00:00:00', '2014-07-22 09:25:10');
INSERT INTO `kontrahencilogin` VALUES('125', 'login', '2', '2014-07-29 12:02:15', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('126', 'login', '2', '0000-00-00 00:00:00', '2014-07-29 14:12:31');
INSERT INTO `kontrahencilogin` VALUES('127', 'login', '2', '2014-08-25 13:45:46', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('128', 'login', '2', '0000-00-00 00:00:00', '2014-08-25 14:56:33');
INSERT INTO `kontrahencilogin` VALUES('129', 'login', '2', '2014-08-28 15:29:57', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('130', 'login', '2', '0000-00-00 00:00:00', '2014-08-28 16:14:59');
INSERT INTO `kontrahencilogin` VALUES('131', 'login', '2', '2014-08-28 16:49:33', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('132', 'login', '2', '0000-00-00 00:00:00', '2014-08-28 16:58:00');
INSERT INTO `kontrahencilogin` VALUES('133', 'kontrahenci', '4', '2014-08-28 16:50:58', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('134', 'kontrahenci', '4', '0000-00-00 00:00:00', '2014-08-28 16:55:50');
INSERT INTO `kontrahencilogin` VALUES('135', 'login', '2', '2014-08-29 10:31:19', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('136', 'login', '2', '0000-00-00 00:00:00', '2014-08-29 11:33:22');
INSERT INTO `kontrahencilogin` VALUES('137', 'kontrahenci', '4', '2014-08-29 10:31:28', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('138', 'kontrahenci', '4', '0000-00-00 00:00:00', '2014-08-29 11:32:45');
INSERT INTO `kontrahencilogin` VALUES('139', 'login', '2', '2014-08-29 11:42:40', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('140', 'login', '2', '0000-00-00 00:00:00', '2014-08-29 11:56:45');
INSERT INTO `kontrahencilogin` VALUES('141', 'login', '2', '2014-09-02 13:57:03', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('142', 'login', '2', '0000-00-00 00:00:00', '2014-09-02 14:13:43');
INSERT INTO `kontrahencilogin` VALUES('143', 'login', '2', '2014-09-04 14:45:45', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('144', 'login', '2', '0000-00-00 00:00:00', '2014-09-04 14:46:21');
INSERT INTO `kontrahencilogin` VALUES('145', 'login', '2', '2014-09-04 14:46:30', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('146', 'login', '2', '0000-00-00 00:00:00', '2014-09-04 14:46:37');
INSERT INTO `kontrahencilogin` VALUES('147', 'login', '2', '2014-09-04 14:48:04', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('148', 'login', '2', '0000-00-00 00:00:00', '2014-09-04 14:48:38');
INSERT INTO `kontrahencilogin` VALUES('149', 'login', '2', '2014-09-04 14:54:41', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('150', 'login', '2', '0000-00-00 00:00:00', '2014-09-04 14:55:49');
INSERT INTO `kontrahencilogin` VALUES('151', 'login', '2', '2014-09-16 10:38:29', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('152', 'login', '2', '0000-00-00 00:00:00', '2014-09-16 12:20:55');
INSERT INTO `kontrahencilogin` VALUES('153', 'kontrahenci', '4', '2014-09-16 11:27:36', '0000-00-00 00:00:00');
INSERT INTO `kontrahencilogin` VALUES('154', 'kontrahenci', '4', '0000-00-00 00:00:00', '2014-09-16 11:47:43');


-- Dumping structure for table: kontrahencipolecane

CREATE TABLE `kontrahencipolecane` (
  `id` int(11) NULL auto_increment ,
  `id_kontr` int(11) NULL ,
  `id_prod` int(11) NULL ,
  `typ` varchar(20) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: kontrahencipolecane



-- Dumping structure for table: login

CREATE TABLE `login` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(100) NULL ,
  `login` varchar(100) NULL ,
  `haslo` varchar(100) NULL ,
  `imie` varchar(50) NULL ,
  `nazwisko` varchar(50) NULL ,
  `uprawnienia` varchar(512) NULL ,
  `role` varchar(20) NULL ,
  `status` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: login

INSERT INTO `login` VALUES('1', 'Administrator', 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '', 'wszystko', '', '');
INSERT INTO `login` VALUES('2', 'BigCom', 'bigcom', 'af29deede4ea0764334075917d3bc2a4', '', '', 'wszystko', '', '');


-- Dumping structure for table: lojalnoscpolecenia

CREATE TABLE `lojalnoscpolecenia` (
  `id` int(11) NULL auto_increment ,
  `polecajacy` int(11) NULL ,
  `polecany` varchar(255) NULL ,
  `termin` date NULL ,
  `data` timestamp NULL ,
  `wykorzystano` date NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: lojalnoscpolecenia



-- Dumping structure for table: lojalnoscrabaty

CREATE TABLE `lojalnoscrabaty` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `od` float NULL ,
  `do` float NULL ,
  `wartosc` decimal(12,2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: lojalnoscrabaty



-- Dumping structure for table: lojalnoscrabatypole

CREATE TABLE `lojalnoscrabatypole` (
  `id` int(11) NULL auto_increment ,
  `idpol` int(11) NULL ,
  `idkl` int(11) NULL ,
  `wartosc` decimal(12,2) NULL ,
  `rabat` decimal(12,2) NULL ,
  `data` varchar(100) NULL ,
  `upominek` int(11) NULL ,
  `upominkilista` text NULL ,
  `upominekwybrany` int(11) NULL ,
  `dataodebrania` varchar(255) NULL ,
  `idzak` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: lojalnoscrabatypole



-- Dumping structure for table: lojalnoscustawienia

CREATE TABLE `lojalnoscustawienia` (
  `id` int(11) NULL auto_increment ,
  `rabat_standard` decimal(12,2) NULL ,
  `rabat_polecajacy` decimal(12,2) NULL ,
  `rabat_polecony` decimal(12,2) NULL ,
  `waznosc_polecenia` int(11) NULL ,
  `rej_do_zam` int(11) NULL ,
  `tylko_1_zam` varchar(2) NULL ,
  `min_kwota` decimal(12,2) NULL ,
  `min_rabat` decimal(12,2) NULL ,
  `usun_polecajacy` varchar(2) NULL ,
  `usun_polecany` varchar(2) NULL ,
  `rabat_st_on` varchar(1) NULL ,
  `upominek_st_on` varchar(1) NULL ,
  `rabat_pol_on` varchar(1) NULL ,
  `upominek_pol_on` varchar(1) NULL ,
  `start` varchar(255) NULL ,
  `koniec` varchar(255) NULL ,
  `rabat_rodzaj` varchar(1) NULL ,
  `rabat_od` varchar(1) NULL ,
  `upominek_st_kwota` decimal(12,2) NULL ,
  `upominek_pol_kwota` decimal(12,2) NULL ,
  `rabat_pol_dni` int(11) NULL ,
  `rabat_pol_od` int(11) NULL ,
  `rabat_pol_wyp_od` varchar(1) NULL ,
  `zapisano` varchar(1) NULL ,
  `zapisanoczas` varchar(50) NULL ,
  `wyczyszczono` varchar(50) NULL ,
  `status` varchar(1) NULL ,
  `rabat_polec_on` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: lojalnoscustawienia

INSERT INTO `lojalnoscustawienia` VALUES('1', '1.00', '2.00', '1.50', '0', '0', '', '0.00', '0.00', '', '', '1', '1', '1', '1', '2012-01-01', '2013-12-31', '1', '0', '100.00', '100.00', '1', '40', '0', '1', '2012-12-01 12:00:00', '', '0', '1');


-- Dumping structure for table: lojalnoscustawienia0

CREATE TABLE `lojalnoscustawienia0` (
  `id` int(11) NULL auto_increment ,
  `rabat_standard` decimal(12,2) NULL ,
  `rabat_polecajacy` decimal(12,2) NULL ,
  `rabat_polecony` decimal(12,2) NULL ,
  `waznosc_polecenia` int(11) NULL ,
  `rej_do_zam` int(11) NULL ,
  `tylko_1_zam` varchar(2) NULL ,
  `min_kwota` decimal(12,2) NULL ,
  `min_rabat` decimal(12,2) NULL ,
  `usun_polecajacy` varchar(2) NULL ,
  `usun_polecany` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: lojalnoscustawienia0

INSERT INTO `lojalnoscustawienia0` VALUES('1', '0.00', '3.00', '2.50', '30', '0', '', '0.00', '0.00', '', '');


-- Dumping structure for table: marketing

CREATE TABLE `marketing` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `tekst` text NULL ,
  `typ` int(11) NULL ,
  `czas` varchar(1) NULL ,
  `start` varchar(255) NULL ,
  `koniec` varchar(255) NULL ,
  `czastyp` varchar(1) NULL ,
  `czasjednostka` int(11) NULL ,
  `czasjednostkad` int(11) NULL ,
  `klient` text NULL ,
  `suma` int(11) NULL ,
  `wartosc` decimal(12,2) NULL ,
  `data` varchar(255) NULL ,
  `aktywna` varchar(1) NULL ,
  `cronjob` varchar(255) NULL ,
  `datadeaktywacja` varchar(255) NULL ,
  `datarozpoczecia` varchar(255) NULL ,
  `powtorzen` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: marketing

INSERT INTO `marketing` VALUES('1', 'Przyjd? i zobacz nowa ofert?', '&lt;p&gt;tekst mail&lt;/p&gt;', '0', '0', '', '', '0', '0', '0', 'a:1:{i:4;s:40:\&quot;3242352,james890@gmail.com,Jakub,Nowak,4\&quot;;}', '0', '0.00', '2012-10-15', '1', '', '', '2012-10-17', '3');
INSERT INTO `marketing` VALUES('3', 'Nowa akcja jesienna', 'nowa akcja promocyjna', '1', '0', '', '', '0', '0', '0', 'a:1:{i:4;s:40:\&quot;3242352,james890@gmail.com,Jakub,Nowak,4\&quot;;}', '4', '0.00', '2012-10-17', '1', '', '', '2012-10-17', '10');


-- Dumping structure for table: menu

CREATE TABLE `menu` (
  `id` int(11) NULL auto_increment ,
  `rodzic` int(11) NULL ,
  `nazwa` varchar(255) NULL ,
  `skrot` text NULL ,
  `tekst` text NULL ,
  `lang` varchar(2) NULL ,
  `pozycja` int(11) NULL ,
  `link` varchar(255) NULL ,
  `wyswietl` varchar(2) NULL ,
  `data` timestamp NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: menu

INSERT INTO `menu` VALUES('2', '0', 'Menu Oferta', '', '&lt;p&gt;Menu Oferta&lt;/p&gt;', 'pl', '0', 'Menu-oferta', '1', '0000-00-00 00:00:00');


-- Dumping structure for table: odwiedziny

CREATE TABLE `odwiedziny` (
  `id` int(11) NULL auto_increment ,
  `id_prod` int(11) NULL ,
  `data` date NULL ,
  `odwiedziny` int(11) NULL ,
  `typ` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: odwiedziny

INSERT INTO `odwiedziny` VALUES('1', '1', '2012-04-01', '8', '');
INSERT INTO `odwiedziny` VALUES('2', '1', '2012-05-01', '55', '');
INSERT INTO `odwiedziny` VALUES('3', '1', '2012-06-01', '12', '');
INSERT INTO `odwiedziny` VALUES('4', '1', '2012-07-01', '7', '');
INSERT INTO `odwiedziny` VALUES('5', '1', '2012-08-01', '7', '');
INSERT INTO `odwiedziny` VALUES('6', '1', '2012-11-01', '1', '');
INSERT INTO `odwiedziny` VALUES('7', '1', '2012-12-01', '3', 'kategorie');
INSERT INTO `odwiedziny` VALUES('8', '1', '2013-02-01', '1', 'produkty');
INSERT INTO `odwiedziny` VALUES('9', '1', '2013-04-01', '3', 'kategorie');
INSERT INTO `odwiedziny` VALUES('10', '1', '2013-04-01', '2', 'produkty');
INSERT INTO `odwiedziny` VALUES('11', '1', '2013-08-01', '4', 'produkty');
INSERT INTO `odwiedziny` VALUES('12', '1', '2014-02-01', '1', 'kategorie');
INSERT INTO `odwiedziny` VALUES('13', '1', '2014-02-01', '2', 'produkty');
INSERT INTO `odwiedziny` VALUES('14', '1', '2014-04-01', '1', 'kategorie');
INSERT INTO `odwiedziny` VALUES('15', '1', '2014-05-01', '1', 'kategorie');
INSERT INTO `odwiedziny` VALUES('16', '1', '2014-05-01', '1', 'produkty');
INSERT INTO `odwiedziny` VALUES('17', '1', '2014-08-01', '3', 'kategorie');
INSERT INTO `odwiedziny` VALUES('18', '1', '2014-08-01', '4', 'produkty');


-- Dumping structure for table: odwiedzinynow

CREATE TABLE `odwiedzinynow` (
  `id` int(11) NULL auto_increment ,
  `id_prod` int(11) NULL ,
  `data` timestamp NULL ,
  `ip` varchar(255) NULL ,
  `typ` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: odwiedzinynow

INSERT INTO `odwiedzinynow` VALUES('36', '1', '2014-08-29 11:02:35', '127.0.0.1', 'produkty');


-- Dumping structure for table: online

CREATE TABLE `online` (
  `id` int(11) NULL auto_increment ,
  `pos_id` varchar(255) NULL ,
  `pos_auth_key` varchar(255) NULL ,
  `key1` varchar(255) NULL ,
  `key2` varchar(255) NULL ,
  `enable` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: online

INSERT INTO `online` VALUES('1', '', '', '', '', '1');


-- Dumping structure for table: onlineplatnosci

CREATE TABLE `onlineplatnosci` (
  `id` int(11) NULL auto_increment ,
  `platnosc` varchar(255) NULL ,
  `dostawa` int(11) NULL ,
  `wartosc` decimal(12,2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: onlineplatnosci



-- Dumping structure for table: onlinezamowienia

CREATE TABLE `onlinezamowienia` (
  `id` int(11) NULL auto_increment ,
  `idzam` int(11) NULL ,
  `nazwa` text NULL ,
  `status` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: onlinezamowienia



-- Dumping structure for table: opek

CREATE TABLE `opek` (
  `id` int(11) NULL auto_increment ,
  `buyer_id` int(11) NULL ,
  `deal_id` bigint(11) NULL ,
  `list` int(11) NULL ,
  `paczka` int(11) NULL ,
  `config` text NULL ,
  `user` text NULL ,
  `data` timestamp NULL ,
  `wyslano` int(1) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: opek



-- Dumping structure for table: opinie

CREATE TABLE `opinie` (
  `id` int(11) NULL auto_increment ,
  `id_prod` int(11) NULL ,
  `id_kontr` int(11) NULL ,
  `imie` varchar(255) NULL ,
  `czas` int(11) NULL ,
  `kontakt` int(11) NULL ,
  `produkt` int(11) NULL ,
  `opis` text NULL ,
  `data` timestamp NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: opinie



-- Dumping structure for table: otomoto

CREATE TABLE `otomoto` (
  `id` int(11) NULL auto_increment ,
  `login` varchar(255) NULL ,
  `haslo` varchar(255) NULL ,
  `klucz` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: otomoto

INSERT INTO `otomoto` VALUES('1', '', '', '');


-- Dumping structure for table: paczkomaty

CREATE TABLE `paczkomaty` (
  `id` int(11) NULL auto_increment ,
  `id_zam` int(11) NULL ,
  `packcode` varchar(50) NULL ,
  `customerdeliveringcode` varchar(50) NULL ,
  `data` datetime NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: paczkomaty



-- Dumping structure for table: parametry

CREATE TABLE `parametry` (
  `id` int(11) NULL auto_increment ,
  `id_prod` int(11) NULL ,
  `id_param` int(11) NULL ,
  `id_atryb` int(11) NULL ,
  `nazwa` varchar(255) NULL ,
  `opis` text NULL ,
  `img` varchar(255) NULL ,
  `widoczny` varchar(2) NULL ,
  `pozycja` int(11) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: parametry



-- Dumping structure for table: partnerzy

CREATE TABLE `partnerzy` (
  `id` int(11) NULL auto_increment ,
  `typ` varchar(20) NULL ,
  `nazwa` varchar(255) NULL ,
  `opis` text NULL ,
  `link` varchar(255) NULL ,
  `logo` varchar(255) NULL ,
  `lang` varchar(2) NULL ,
  `pozycja` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: partnerzy

INSERT INTO `partnerzy` VALUES('1', 'partnerzy', 'test', '', '', 'logo_1.jpg', 'pl', '0');


-- Dumping structure for table: platnosci

CREATE TABLE `platnosci` (
  `id` int(11) NULL auto_increment ,
  `platnosc` int(11) NULL ,
  `dostawa` int(11) NULL ,
  `koszt` decimal(12,2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: platnosci

INSERT INTO `platnosci` VALUES('1', '1', '1', '20.00');
INSERT INTO `platnosci` VALUES('2', '2', '1', '15.00');
INSERT INTO `platnosci` VALUES('3', '1', '2', '15.00');
INSERT INTO `platnosci` VALUES('4', '2', '2', '10.00');


-- Dumping structure for table: platnosciprodukty

CREATE TABLE `platnosciprodukty` (
  `id` int(11) NULL auto_increment ,
  `id_produkt` int(11) NULL ,
  `id_platnosci` int(11) NULL ,
  `gratis` varchar(1) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: platnosciprodukty



-- Dumping structure for table: pocztapolskadane

CREATE TABLE `pocztapolskadane` (
  `id` int(11) NULL auto_increment ,
  `id_zam` int(11) NULL ,
  `nr_nad` bigint(21) NULL ,
  `guid` varchar(32) NULL ,
  `bufor` int(11) NULL ,
  `data_bufor` timestamp NULL ,
  `data_wysylka` timestamp NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: pocztapolskadane



-- Dumping structure for table: porady

CREATE TABLE `porady` (
  `id` int(11) NULL auto_increment ,
  `typ` int(11) NULL ,
  `temat` varchar(255) NULL ,
  `skrot` text NULL ,
  `tekst` text NULL ,
  `data` datetime NULL ,
  `wyswietl` varchar(11) NULL ,
  `glowna` varchar(11) NULL ,
  `lang` varchar(2) NULL ,
  `status` int(11) NULL ,
  `pozycja` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: porady



-- Dumping structure for table: producent

CREATE TABLE `producent` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `logo` varchar(255) NULL ,
  `link` varchar(255) NULL ,
  `rabat` decimal(12,2) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: producent

INSERT INTO `producent` VALUES('1', 'Test', '', '', '0.00', 'pl');


-- Dumping structure for table: produkty

CREATE TABLE `produkty` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(200) NULL ,
  `oznaczenie` varchar(255) NULL ,
  `id_ext` varchar(255) NULL ,
  `producent` int(11) NULL ,
  `grupa` int(11) NULL ,
  `vat` int(11) NULL ,
  `cena_netto` decimal(12,2) NULL ,
  `cena_brutto` decimal(12,2) NULL ,
  `cena_netto_hurt` decimal(12,2) NULL ,
  `cena_brutto_hurt` decimal(12,2) NULL ,
  `cena_promocji_n_hurt` decimal(12,2) NULL ,
  `cena_promocji_b_hurt` decimal(12,2) NULL ,
  `cena_promocji_n` decimal(12,2) NULL ,
  `cena_promocji_b` decimal(12,2) NULL ,
  `widoczny` varchar(2) NULL ,
  `dostepny` varchar(2) NULL ,
  `dostepne` int(11) NULL ,
  `na_zamow` varchar(2) NULL ,
  `negocjuj_cene` varchar(2) NULL ,
  `promocja` varchar(2) NULL ,
  `promocja_net` varchar(2) NULL ,
  `nowosc` varchar(2) NULL ,
  `glowna` varchar(2) NULL ,
  `polecane` varchar(2) NULL ,
  `wyprzedaz` varchar(2) NULL ,
  `specjalne` varchar(2) NULL ,
  `produkt_dnia` varchar(2) NULL ,
  `skrot` text NULL ,
  `tekst` text NULL ,
  `odwiedziny` bigint(20) NULL ,
  `lang` varchar(2) NULL ,
  `status` int(11) NULL ,
  `link` varchar(255) NULL ,
  `route_id` int(11) NULL ,
  `rozmiarowka` int(11) NULL ,
  `data` timestamp NULL ,
  `zmiana` date NULL ,
  `dostepnosc` int(11) NULL ,
  `rabat` decimal(12,2) NULL ,
  `jedn_miary` varchar(255) NULL ,
  `jedn_miary_typ` varchar(255) NULL ,
  `jedn_miary_ile` int(11) NULL ,
  `jedn_miary_opak` varchar(20) NULL ,
  `jedn_miary_zb` varchar(20) NULL ,
  `jedn_miary_zb_ile` int(11) NULL ,
  `jedn_miary_zb_typ` varchar(20) NULL ,
  `jedn_miary_zb_czy` varchar(2) NULL ,
  `waga` float NULL ,
  `gabaryt` varchar(2) NULL ,
  `pozycja` int(11) NULL ,
  `trybCena` varchar(255) NULL ,
  `cena_atryb` tinyint(1) NULL ,
  `kosztyAllegro` text NULL ,
  `allegroCennik` int(11) NULL ,
  `otoMoto` text NULL ,
  `pkwiu` varchar(255) NULL ,
  `sww` varchar(255) NULL ,
  `ean` varchar(255) NULL ,
  `facebookpostid` varchar(20) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: produkty

INSERT INTO `produkty` VALUES('1', 'Produkt testowy11', '', '', '1', '0', '8', '10.18', '10.99', '10.18', '10.99', '0.00', '0.00', '18.00', '19.44', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '', '5', 'pl', '0', 'Produkt-testowy11-1', '10001', '0', '2012-03-15 15:16:13', '2014-08-28', '1', '0.00', 'szt.', 'int', '0', 'szt.', 'karton', '1', 'int', '0', '0', '', '0', 'produkt', '0', 's:0:\&quot;\&quot;;', '0', 'a:4:{s:6:\&quot;active\&quot;;s:0:\&quot;\&quot;;s:4:\&quot;data\&quot;;s:0:\&quot;\&quot;;s:13:\&quot;data_dod_info\&quot;;s:0:\&quot;\&quot;;s:16:\&quot;data_dod_wyposaz\&quot;;s:0:\&quot;\&quot;;}', '', '', '', '');
INSERT INTO `produkty` VALUES('2', 'test', '', '', '0', '0', '23', '22.22', '24.78', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '1', '0', '0', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '', '', '0', 'pl', '0', 'Test-2', '6289', '0', '2012-12-18 15:57:31', '2014-07-29', '1', '0.00', 'szt.', 'int', '0', 'szt.', 'karton', '1', 'int', '', '0', '', '1', 'produkt', '0', 's:0:\&quot;\&quot;;', '0', 'a:4:{s:6:\&quot;active\&quot;;s:0:\&quot;\&quot;;s:4:\&quot;data\&quot;;s:0:\&quot;\&quot;;s:13:\&quot;data_dod_info\&quot;;s:0:\&quot;\&quot;;s:16:\&quot;data_dod_wyposaz\&quot;;s:0:\&quot;\&quot;;}', '', '', '', '');
INSERT INTO `produkty` VALUES('3', 'tester', '', '', '0', '0', '23', '80.49', '99.00', '71.54', '88.00', '0.00', '0.00', '0.00', '0.00', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '', '0', 'pl', '0', 'Tester-3', '10006', '0', '2014-07-29 13:46:19', '2014-07-29', '1', '0.00', 'szt.', 'int', '0', 'szt.', 'karton', '1', 'int', '0', '0', '', '2', 'produkt', '0', 's:0:\&quot;\&quot;;', '0', 'a:4:{s:6:\&quot;active\&quot;;s:0:\&quot;\&quot;;s:4:\&quot;data\&quot;;s:0:\&quot;\&quot;;s:13:\&quot;data_dod_info\&quot;;s:0:\&quot;\&quot;;s:16:\&quot;data_dod_wyposaz\&quot;;s:0:\&quot;\&quot;;}', '', '', '', '');


-- Dumping structure for table: produktygrupy

CREATE TABLE `produktygrupy` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `pozycja` int(11) NULL ,
  `wyswietl` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: produktygrupy



-- Dumping structure for table: produktypowiazania

CREATE TABLE `produktypowiazania` (
  `id` int(11) NULL auto_increment ,
  `id_prod` int(11) NULL ,
  `powiazany` int(11) NULL ,
  `rodzaj` varchar(10) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: produktypowiazania

INSERT INTO `produktypowiazania` VALUES('1', '3', '1', 'produkt');
INSERT INTO `produktypowiazania` VALUES('2', '1', '2', 'produkt');


-- Dumping structure for table: rabaty

CREATE TABLE `rabaty` (
  `id` int(11) NULL auto_increment ,
  `id_user` int(11) NULL ,
  `id_user_grupa` int(11) NULL ,
  `id_prod` int(11) NULL ,
  `id_prod_grupa` int(11) NULL ,
  `kategoria_rab` int(11) NULL ,
  `producent_id` int(11) NULL ,
  `wartosc_rab` decimal(10,2) NULL ,
  `typ_rab` varchar(5) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: rabaty

INSERT INTO `rabaty` VALUES('1', '0', '0', '0', '0', '1', '0', '0.00', '%');
INSERT INTO `rabaty` VALUES('2', '4', '0', '0', '0', '2', '0', '5.00', '%');
INSERT INTO `rabaty` VALUES('6', '4', '0', '2', '0', '0', '0', '8.00', '%');
INSERT INTO `rabaty` VALUES('7', '4', '0', '3', '0', '0', '0', '8.00', '%');
INSERT INTO `rabaty` VALUES('8', '4', '0', '1', '0', '0', '0', '22.00', '%');
INSERT INTO `rabaty` VALUES('9', '4', '0', '0', '0', '0', '1', '11.00', '%');


-- Dumping structure for table: rabatyprogi

CREATE TABLE `rabatyprogi` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `od` float NULL ,
  `do` float NULL ,
  `koszt` float NULL ,
  `typ` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: rabatyprogi



-- Dumping structure for table: realizacje

CREATE TABLE `realizacje` (
  `id` int(11) NULL auto_increment ,
  `typ` int(11) NULL ,
  `temat` varchar(255) NULL ,
  `skrot` text NULL ,
  `tekst` text NULL ,
  `data` datetime NULL ,
  `wyswietl` varchar(11) NULL ,
  `glowna` varchar(11) NULL ,
  `lang` varchar(2) NULL ,
  `status` int(11) NULL ,
  `pozycja` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: realizacje



-- Dumping structure for table: routers

CREATE TABLE `routers` (
  `id` int(11) NULL auto_increment ,
  `route` varchar(255) NULL ,
  `module` varchar(255) NULL ,
  `controller` varchar(255) NULL ,
  `action` varchar(255) NULL ,
  `params` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: routers

INSERT INTO `routers` VALUES('1', ':controller/:action', 'default', 'index', 'index', '');
INSERT INTO `routers` VALUES('2', 'admin/:controller/:action', 'admin', 'index', 'index', '');
INSERT INTO `routers` VALUES('3', 'ajax/:controller/:action', 'ajax', 'index', 'index', '');
INSERT INTO `routers` VALUES('4', 'sklep/:controller/:action', 'sklep', 'index', 'index', '');
INSERT INTO `routers` VALUES('5', 'platnosci/:controller/:action', 'platnosci', 'index', 'index', '');
INSERT INTO `routers` VALUES('6', 'szablon/:controller/:action', 'szablon', 'index', 'index', '');
INSERT INTO `routers` VALUES('11', 'Produkty', 'default', 'oferta', 'index', '');
INSERT INTO `routers` VALUES('12', 'Nowosci', 'default', 'oferta', 'Nowosci', '');
INSERT INTO `routers` VALUES('13', 'Promocje', 'default', 'oferta', 'Promocje', '');
INSERT INTO `routers` VALUES('14', 'Bestsellery', 'default', 'oferta', 'Bestsellery', '');
INSERT INTO `routers` VALUES('15', 'Wyprzedaz', 'default', 'oferta', 'Wyprzedaz', '');
INSERT INTO `routers` VALUES('16', 'Specjalne', 'default', 'oferta', 'Specjalne', '');
INSERT INTO `routers` VALUES('20', 'Regulamin', 'default', 'podstrony', 'show', 'Regulamin');
INSERT INTO `routers` VALUES('21', 'O-firmie', 'default', 'podstrony', 'show', 'O-firmie');
INSERT INTO `routers` VALUES('22', 'Kontakt', 'default', 'podstrony', 'show', 'Kontakt');
INSERT INTO `routers` VALUES('31', 'Polec-sklep', 'default', 'podstrony', 'polecsklep', 'Polec-sklep');
INSERT INTO `routers` VALUES('32', 'Program-lojalnosciowy', 'default', 'podstrony', 'show', 'Program-lojalnosciowy');
INSERT INTO `routers` VALUES('33', 'Twoje-konto-rabatowe', 'default', 'podstrony', 'show', 'Twoje-konto-rabatowe');
INSERT INTO `routers` VALUES('41', 'Logowanie', 'default', 'kontrahent', 'logowanie', '');
INSERT INTO `routers` VALUES('42', 'Rejestracja', 'default', 'kontrahent', 'index', '');
INSERT INTO `routers` VALUES('43', 'Koszyk', 'default', 'zamowienia', 'index', '');
INSERT INTO `routers` VALUES('44', 'Menu-oferta', 'default', 'podstrony', 'menu', 'Menu-oferta');
INSERT INTO `routers` VALUES('45', 'Wyniki-wyszukiwania', 'default', 'oferta', 'szukaj', '');
INSERT INTO `routers` VALUES('51', 'Aktualnosci', 'default', 'podstrony', 'strona', 'Aktualnosci');
INSERT INTO `routers` VALUES('52', 'Aktualnosc', 'default', 'podstrony', 'podstrona', 'Aktualnosc');
INSERT INTO `routers` VALUES('53', 'Artykuly', 'default', 'podstrony', 'strona', 'Artykuly');
INSERT INTO `routers` VALUES('54', 'Artykul', 'default', 'podstrony', 'podstrona', 'Artykul');
INSERT INTO `routers` VALUES('55', 'Porady', 'default', 'podstrony', 'strona', 'Porady');
INSERT INTO `routers` VALUES('56', 'Porada', 'default', 'podstrony', 'podstrona', 'Porada');
INSERT INTO `routers` VALUES('111', 'Offers', 'default', 'oferta', 'index', '');
INSERT INTO `routers` VALUES('112', 'Newsy', 'default', 'oferta', 'Nowosci', '');
INSERT INTO `routers` VALUES('113', 'Discounts', 'default', 'oferta', 'Promocje', '');
INSERT INTO `routers` VALUES('114', 'Recommended', 'default', 'oferta', 'Bestsellery', '');
INSERT INTO `routers` VALUES('115', 'Sell-offs', 'default', 'oferta', 'Wyprzedaz', '');
INSERT INTO `routers` VALUES('116', 'Special', 'default', 'oferta', 'Specjalne', '');
INSERT INTO `routers` VALUES('120', 'Regulations', 'default', 'podstrony', 'show', 'Regulations');
INSERT INTO `routers` VALUES('121', 'About-company', 'default', 'podstrony', 'show', 'About-company');
INSERT INTO `routers` VALUES('122', 'Contacts', 'default', 'podstrony', 'show', 'Contacts');
INSERT INTO `routers` VALUES('131', 'Recommend-us', 'default', 'podstrony', 'polecsklep', 'Recommend-us');
INSERT INTO `routers` VALUES('132', 'Loyalty-program', 'default', 'podstrony', 'show', 'Loyalty-program');
INSERT INTO `routers` VALUES('133', 'Your-discount-account', 'default', 'podstrony', 'show', 'Your-discount-account');
INSERT INTO `routers` VALUES('141', 'Logon', 'default', 'kontrahent', 'logowanie', '');
INSERT INTO `routers` VALUES('142', 'Registration', 'default', 'kontrahent', 'index', '');
INSERT INTO `routers` VALUES('143', 'Cart', 'default', 'zamowienia', 'index', '');
INSERT INTO `routers` VALUES('145', 'Search-results', 'default', 'oferta', 'szukaj', '');
INSERT INTO `routers` VALUES('151', 'All-news', 'default', 'podstrony', 'strona', 'Aktualnosci');
INSERT INTO `routers` VALUES('152', 'New', 'default', 'podstrony', 'podstrona', 'Aktualnosc');
INSERT INTO `routers` VALUES('153', 'Articles', 'default', 'podstrony', 'strona', 'Artykuly');
INSERT INTO `routers` VALUES('154', 'Article', 'default', 'podstrony', 'podstrona', 'Artykul');
INSERT INTO `routers` VALUES('155', 'Advices', 'default', 'podstrony', 'strona', 'Porady');
INSERT INTO `routers` VALUES('156', 'Advice', 'default', 'podstrony', 'podstrona', 'Porada');
INSERT INTO `routers` VALUES('1001', 'Kategoria-testowa2', 'default', 'oferta', 'kategorie', '1');
INSERT INTO `routers` VALUES('1002', 'Test', 'default', 'oferta', 'kategorie', '2');
INSERT INTO `routers` VALUES('6289', 'Test-2', 'default', 'produkt', 'index', '2');
INSERT INTO `routers` VALUES('10001', 'Produkt-testowy11-1', 'default', 'produkt', 'index', '1');
INSERT INTO `routers` VALUES('10002', 'Polityka-prywatnosci', 'default', 'podstrony', 'show', 'Polityka-prywatnosci');
INSERT INTO `routers` VALUES('10003', 'Polityka-cookies', 'default', 'podstrony', 'show', 'Polityka-cookies');
INSERT INTO `routers` VALUES('10004', 'Privacy-policy', 'default', 'podstrony', 'show', 'Privacy-policy');
INSERT INTO `routers` VALUES('10005', 'Cookies-policy', 'default', 'podstrony', 'show', 'Cookies-policy');
INSERT INTO `routers` VALUES('10006', 'Tester-3', 'default', 'produkt', 'index', '3');


-- Dumping structure for table: rozmiarowka

CREATE TABLE `rozmiarowka` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `jedn_miary` varchar(255) NULL ,
  `jedn_miary_typ` varchar(255) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: rozmiarowka

INSERT INTO `rozmiarowka` VALUES('1', 'Uniwersalna', 'szt.', 'int', 'pl');
INSERT INTO `rozmiarowka` VALUES('2', 'Test', 'szt.', 'int', 'pl');


-- Dumping structure for table: rozmiarproduktu

CREATE TABLE `rozmiarproduktu` (
  `id` int(11) NULL auto_increment ,
  `id_produktu` int(11) NULL ,
  `id_koloru` int(11) NULL ,
  `dostepny` varchar(2) NULL ,
  `dostepnosc` int(11) NULL ,
  `cena_netto` decimal(12,2) NULL ,
  `cena_brutto` decimal(12,2) NULL ,
  `cena_netto_hurt` decimal(12,2) NULL ,
  `cena_brutto_hurt` decimal(12,2) NULL ,
  `cena_promocji_n_hurt` decimal(12,2) NULL ,
  `cena_promocji_b_hurt` decimal(12,2) NULL ,
  `cena_promocji_n` decimal(12,2) NULL ,
  `cena_promocji_b` decimal(12,2) NULL ,
  `opis` text NULL ,
  `skrot` text NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: rozmiarproduktu

INSERT INTO `rozmiarproduktu` VALUES('1', '2', '1', '', '0', '10.00', '11.00', '8.00', '9.00', '4.00', '5.00', '6.00', '7.00', '', '');
INSERT INTO `rozmiarproduktu` VALUES('2', '2', '1', '', '0', '8.00', '7.00', '6.00', '5.00', '4.00', '3.00', '2.00', '1.00', '', '');


-- Dumping structure for table: rozmiary

CREATE TABLE `rozmiary` (
  `id` int(11) NULL auto_increment ,
  `typ` int(11) NULL ,
  `nazwa` varchar(255) NULL ,
  `wkladka` varchar(255) NULL ,
  `dostepnosc` int(11) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: rozmiary

INSERT INTO `rozmiary` VALUES('1', '1', '22', '', '0', 'pl');
INSERT INTO `rozmiary` VALUES('2', '2', '33', '', '0', 'pl');


-- Dumping structure for table: serwer

CREATE TABLE `serwer` (
  `id` int(11) NULL auto_increment ,
  `adres` varchar(255) NULL ,
  `serwer` varchar(255) NULL ,
  `login` varchar(255) NULL ,
  `haslo` varchar(255) NULL ,
  `remote_path` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: serwer

INSERT INTO `serwer` VALUES('1', '', '', '', '', '');


-- Dumping structure for table: siodemka

CREATE TABLE `siodemka` (
  `id` int(11) NULL auto_increment ,
  `nr` bigint(20) NULL ,
  `id_zam` int(11) NULL ,
  `nrpp` bigint(20) NULL ,
  `data` timestamp NULL ,
  `wydanie` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: siodemka



-- Dumping structure for table: siodemkadane

CREATE TABLE `siodemkadane` (
  `id` int(11) NULL auto_increment ,
  `www` varchar(255) NULL ,
  `numer` int(10) NULL ,
  `klucz` varchar(255) NULL ,
  `rodzajPrzesylki` varchar(1) NULL ,
  `placi` varchar(1) NULL ,
  `formaPlatnosci` varchar(1) NULL ,
  `uwagi` varchar(200) NULL ,
  `nazwisko` varchar(100) NULL ,
  `imie` varchar(50) NULL ,
  `telKontakt` varchar(50) NULL ,
  `emailKontakt` varchar(80) NULL ,
  `nrBezpiecznejKoperty` varchar(50) NULL ,
  `kwotaUbezpieczenia` float NULL ,
  `opisZawartosci` varchar(200) NULL ,
  `kwotaPobrania` decimal(6,2) NULL ,
  `formaPobrania` varchar(1) NULL ,
  `nrKonta` varchar(26) NULL ,
  `skladowanie` int(3) NULL ,
  `zastrzDorNaGodz` varchar(5) NULL ,
  `zastrzDorNaDzien` varchar(1) NULL ,
  `typ` varchar(2) NULL ,
  `waga` decimal(4,1) NULL ,
  `gab1` int(5) NULL ,
  `gab2` int(5) NULL ,
  `gab3` int(5) NULL ,
  `numerKuriera` int(6) NULL ,
  `podpisNadawcy` varchar(50) NULL ,
  `zkld` varchar(2) NULL ,
  `zd` varchar(2) NULL ,
  `awizacjaTelefoniczna` varchar(2) NULL ,
  `potwNadEmail` varchar(2) NULL ,
  `potwDostEmail` varchar(2) NULL ,
  `potwDostSMS` varchar(2) NULL ,
  `nadOdbPKP` varchar(2) NULL ,
  `odbNadgodziny` varchar(2) NULL ,
  `odbWlas` varchar(2) NULL ,
  `palNextDay` varchar(2) NULL ,
  `osobaFiz` varchar(2) NULL ,
  `market` varchar(2) NULL ,
  `ksztalt` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: siodemkadane

INSERT INTO `siodemkadane` VALUES('1', 'http://testws.siodemka.com/mm7ws/SiodemkaServiceSoapHttpPort', '1', '2', 'K', '2', 'G', '', '', '', '', '', '', '10', '', '0.00', 'B', '', '0', '00:00', 'B', 'PC', '10.0', '0', '0', '0', '0', '', '', '', '', 'on', 'on', '', '', '', '', '', 'on', '', '');


-- Dumping structure for table: slider

CREATE TABLE `slider` (
  `id` int(11) NULL auto_increment ,
  `img` varchar(255) NULL ,
  `aktywny` int(1) NULL ,
  `opis` varchar(512) NULL ,
  `opis_poz` int(11) NULL ,
  `link` varchar(255) NULL ,
  `czas` int(11) NULL ,
  `kolejnosc` int(11) NULL ,
  `glowny` varchar(2) NULL ,
  `typ` varchar(255) NULL ,
  `rodzaj` int(11) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: slider

INSERT INTO `slider` VALUES('2', 'slider.jpg', '1', '', '0', '', '5', '1', '', 'slider', '0', 'pl');
INSERT INTO `slider` VALUES('3', 'galeria.jpg', '1', '', '0', '', '5', '1', '', 'galeria', '0', 'pl');


-- Dumping structure for table: slidertypy

CREATE TABLE `slidertypy` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `opis` text NULL ,
  `data` timestamp NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: slidertypy



-- Dumping structure for table: sposobdostawy

CREATE TABLE `sposobdostawy` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `wartosc` decimal(12,2) NULL ,
  `opis` text NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: sposobdostawy

INSERT INTO `sposobdostawy` VALUES('1', 'Kurier', '0.00', '', 'pl');
INSERT INTO `sposobdostawy` VALUES('2', 'Poczta Polska', '0.00', '', 'pl');


-- Dumping structure for table: strony

CREATE TABLE `strony` (
  `id` int(11) NULL auto_increment ,
  `rodzic` int(11) NULL ,
  `nazwa` varchar(255) NULL ,
  `tekst` text NULL ,
  `lang` varchar(2) NULL ,
  `pozycja` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: strony



-- Dumping structure for table: subskrypcja

CREATE TABLE `subskrypcja` (
  `id` int(11) NULL auto_increment ,
  `email` varchar(255) NULL ,
  `imie` varchar(255) NULL ,
  `nazwisko` varchar(255) NULL ,
  `telefon` varchar(255) NULL ,
  `allegro_id` int(11) NULL ,
  `allegro_login` varchar(255) NULL ,
  `allegro_aukcje` text NULL ,
  `lang` varchar(2) NULL ,
  `status` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: subskrypcja



-- Dumping structure for table: upominki

CREATE TABLE `upominki` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(150) NULL ,
  `tekst` text NULL ,
  `img` varchar(255) NULL ,
  `polecany` varchar(1) NULL ,
  `polecajacy` varchar(1) NULL ,
  `aktywnosc` varchar(1) NULL ,
  `cena` decimal(10,2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: upominki



-- Dumping structure for table: ups

CREATE TABLE `ups` (
  `id` int(11) NULL auto_increment ,
  `id_zam` int(11) NULL ,
  `nr` int(11) NULL ,
  `data` timestamp NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: ups



-- Dumping structure for table: upusty

CREATE TABLE `upusty` (
  `id` int(11) NULL auto_increment ,
  `idkontrahenta` int(11) NULL ,
  `typ` int(11) NULL ,
  `od` decimal(12,2) NULL ,
  `do` decimal(12,2) NULL ,
  `upust` float NULL ,
  `wartosc` decimal(12,2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: upusty



-- Dumping structure for table: ustawienialang

CREATE TABLE `ustawienialang` (
  `id` int(11) NULL auto_increment ,
  `title` varchar(255) NULL ,
  `keywords` varchar(512) NULL ,
  `description` text NULL ,
  `cookiepolicy` text NULL ,
  `cookielink` varchar(50) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: ustawienialang

INSERT INTO `ustawienialang` VALUES('1', 'Panel', 'panel', 'panel', 'Ta strona korzysta z cookies {PRZYCISK} {LINK}', 'Polityka-cookies', 'pl');
INSERT INTO `ustawienialang` VALUES('2', 'Panel', '', '', 'This site uses cookies. {PRZYCISK} {LINK}', 'Cookies-policy', 'en');
INSERT INTO `ustawienialang` VALUES('3', 'Panel', '', '', 'Diese Seite benutzt Cookies {PRZYCISK} {LINK}', '', 'de');
INSERT INTO `ustawienialang` VALUES('4', 'Panel', '', '', 'Ce site utilise des cookies {PRZYCISK} {LINK}', '', 'fr');
INSERT INTO `ustawienialang` VALUES('5', 'Panel', '', '', 'Este sitio utiliza cookies {PRZYCISK} {LINK}', '', 'es');
INSERT INTO `ustawienialang` VALUES('6', 'Panel', '', '', '??? ???? ???????????? ???? {PRZYCISK} {LINK}', '', 'uk');


-- Dumping structure for table: vat

CREATE TABLE `vat` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: vat

INSERT INTO `vat` VALUES('1', '23');
INSERT INTO `vat` VALUES('2', '8');
INSERT INTO `vat` VALUES('3', '5');
INSERT INTO `vat` VALUES('4', '0');
INSERT INTO `vat` VALUES('5', 'zw.');


-- Dumping structure for table: video

CREATE TABLE `video` (
  `id` int(11) NULL auto_increment ,
  `id_ext` int(11) NULL ,
  `typ` varchar(20) NULL ,
  `nazwa` varchar(255) NULL ,
  `www` varchar(255) NULL ,
  `opis` text NULL ,
  `pozycja` int(11) NULL ,
  `lang` varchar(2) NULL ,
  `data` timestamp NULL ,
  `wyswietl` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: video



-- Dumping structure for table: wagi

CREATE TABLE `wagi` (
  `id` int(11) NULL auto_increment ,
  `nazwa` varchar(255) NULL ,
  `od` float NULL ,
  `do` float NULL ,
  `koszt` float NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: wagi

INSERT INTO `wagi` VALUES('1', 'Do 10 kg', '0', '10', '15');
INSERT INTO `wagi` VALUES('2', 'Pow. 10 kg', '10', '99999', '20');


-- Dumping structure for table: wagidostawy

CREATE TABLE `wagidostawy` (
  `id` int(11) NULL auto_increment ,
  `id_dostawa` int(11) NULL ,
  `id_waga` int(11) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: wagidostawy

INSERT INTO `wagidostawy` VALUES('41', '1', '1');
INSERT INTO `wagidostawy` VALUES('42', '1', '2');
INSERT INTO `wagidostawy` VALUES('43', '2', '1');
INSERT INTO `wagidostawy` VALUES('44', '2', '2');


-- Dumping structure for table: wagikosztdostawy

CREATE TABLE `wagikosztdostawy` (
  `id` int(11) NULL auto_increment ,
  `id_dostawa` int(11) NULL ,
  `id_waga` int(11) NULL ,
  `koszt` float NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: wagikosztdostawy

INSERT INTO `wagikosztdostawy` VALUES('10', '1', '1', '15');
INSERT INTO `wagikosztdostawy` VALUES('11', '1', '2', '20');


-- Dumping structure for table: wizytowka

CREATE TABLE `wizytowka` (
  `id` int(11) NULL auto_increment ,
  `nazwa` text NULL ,
  `temat` varchar(255) NULL ,
  `tekst` text NULL ,
  `short` text NULL ,
  `link` varchar(255) NULL ,
  `wizytowka` text NULL ,
  `domyslny` varchar(1) NULL ,
  `email` varchar(255) NULL ,
  `lang` varchar(2) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: wizytowka



-- Dumping structure for table: youtube

CREATE TABLE `youtube` (
  `id` int(11) NULL auto_increment ,
  `www` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: youtube

INSERT INTO `youtube` VALUES('1', 'http://www.youtube.com/?hl=pl&amp;gl=PL');


-- Dumping structure for table: zamowieniaprodukty

CREATE TABLE `zamowieniaprodukty` (
  `id` int(11) NULL auto_increment ,
  `id_prod` int(11) NULL ,
  `id_zam` int(11) NULL ,
  `nazwa` varchar(255) NULL ,
  `oznaczenie` varchar(255) NULL ,
  `netto` decimal(12,2) NULL ,
  `brutto` decimal(12,2) NULL ,
  `cena_netto` decimal(12,2) NULL ,
  `cena_brutto` decimal(12,2) NULL ,
  `ilosc` decimal(12,2) NULL ,
  `dostepnosc` int(11) NULL ,
  `vat` decimal(12,2) NULL ,
  `wartosc_netto` decimal(12,2) NULL ,
  `wartosc` decimal(12,2) NULL ,
  `rabat` decimal(12,2) NULL ,
  `upust` decimal(12,2) NULL ,
  `rozmiar` varchar(255) NULL ,
  `kolor` varchar(255) NULL ,
  `atrybut` int(11) NULL ,
  `atrybuty` text NULL ,
  `jedn` varchar(20) NULL ,
  `jedn_miary` varchar(20) NULL ,
  `jedn_miary_typ` varchar(5) NULL ,
  `jedn_miary_zb` varchar(20) NULL ,
  `jedn_miary_zb_ile` int(11) NULL ,
  `jedn_miary_zb_typ` varchar(5) NULL ,
  `opis` text NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: zamowieniaprodukty

INSERT INTO `zamowieniaprodukty` VALUES('1', '1', '1', 'Produkt testowy', 'Test', '0.00', '0.00', '1.00', '1.23', '6.00', '0', '0.00', '6.00', '7.38', '0.10', '0.00', '1', '', '0', '', 'szt.', 'szt.', 'int', 'paleta', '1000', 'int', '');
INSERT INTO `zamowieniaprodukty` VALUES('2', '1', '2', 'Produkt testowy', 'Test', '0.00', '0.00', '18.00', '19.44', '1.00', '0', '0.00', '18.00', '19.44', '2.00', '0.00', '0', '', '0', 'N;', 'szt.', 'szt.', 'int', 'paleta', '1', 'int', '');
INSERT INTO `zamowieniaprodukty` VALUES('4', '1', '4', 'Produkt testowy', 'Test', '18.00', '19.44', '17.45', '18.85', '2.00', '0', '0.00', '34.90', '37.70', '3.00', '0.00', '0', '', '0', 'N;', 'szt.', 'szt.', 'int', 'paleta', '1', 'int', '');
INSERT INTO `zamowieniaprodukty` VALUES('5', '1', '5', 'Produkt testowy', '', '18.70', '20.20', '18.70', '20.20', '1.00', '0', '0.00', '18.70', '20.20', '2.00', '0.00', '0', '', '0', 'N;', 'szt.', 'szt.', 'int', 'karton', '1', 'int', '');
INSERT INTO `zamowieniaprodukty` VALUES('6', '2', '5', 'test', '', '22.22', '24.78', '22.22', '24.78', '1.00', '0', '23.00', '22.22', '24.78', '0.00', '0.00', '', '', '0', '', 'szt.', 'szt.', 'int', 'karton', '1', 'int', '');


-- Dumping structure for table: zamowieniauwagi

CREATE TABLE `zamowieniauwagi` (
  `id` int(11) NULL auto_increment ,
  `id_zam` int(11) NULL ,
  `data` timestamp NULL ,
  `tresc` text NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: zamowieniauwagi

INSERT INTO `zamowieniauwagi` VALUES('3', '5', '2014-02-03 15:34:44', 'ewdewf');


-- Dumping structure for table: znajomi

CREATE TABLE `znajomi` (
  `id` int(11) NULL auto_increment ,
  `id_kontr` int(11) NULL ,
  `mail` varchar(255) NULL ,
  PRIMARY KEY  (`id`)
);


-- Dumping data for table: znajomi

