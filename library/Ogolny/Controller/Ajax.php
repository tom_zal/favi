<?php
abstract class Ogolny_Controller_Ajax extends Zend_Controller_Action
{
    private $obConfig, $tlumaczenie, $lang;

    public function init()
	{
        // load configuration
        $this->view->obConfig = $this->obConfig = new Zend_Config_Ini('../application/config.ini', 'general');
		$this->view->ImageDir = $this->ImageDir = new Zend_Config_Ini('../application/config.ini', 'image');
        $this->SmallImageDir = $this->ImageDir->ImageDir . '' . $this->ImageDir->SmallImageDir;
		
        set_include_path('.' . PATH_SEPARATOR . $this->obConfig->path->models . PATH_SEPARATOR . get_include_path());
		
		//echo $_SERVER['HTTP_REFERER'];die();
		$moduleAjax = !isset($_SERVER['HTTP_REFERER']) || strpos($_SERVER['HTTP_REFERER'], "/admin/") !== false ? "admin" : "default";
		$this->view->moduleAjax = $this->moduleAjax = $moduleAjax;//die();

        $lang = new Zend_Session_Namespace($moduleAjax == 'admin' ? 'jezyk_admin' : 'jezyk');
		if(!isset($lang->lang)) $lang->wybrana = $lang->lang = 'pl';
        $this->view->lang = $this->lang = $lang->lang;
		$this->view->jezyk = $this->jezyk = $lang->lang;
		
		$this->view->common = $this->common = new Common(false, $this->moduleAjax);
		$this->view->zl = $this->zl = $this->common->zl;
		$this->view->ustawienia = $this->ustawienia = $this->common->getUstawienia();
		$this->view->langs = $this->langs = $this->common->getJezyki();
		
		$this->view->www = $this->www = $this->obConfig->www;
		$this->view->baseUrl = $this->baseUrl = $this->_request->getBaseUrl();
		$this->view->URI = $this->URI = str_replace($this->baseUrl, '', $_SERVER['REQUEST_URI']);
		$this->view->path = $this->path = str_replace('//','/',$_SERVER['DOCUMENT_ROOT'].$this->baseUrl);
		
		$this->session = $session = new Zend_Session_Namespace();
		$this->view->params = $this->params = $session->params = $this->_request->getParams();
		$this->view->module = $this->_module = $this->module = $this->params['module'];
		$this->view->controller = $this->_controller = $this->controller = $this->params['controller'];
		$this->view->action = $this->_action = $this->action = @$this->params['action'];
		//var_dump($this->params);
		$this->cache = $session->cache;
        $this->caching = false;
		
		$info = new Zend_Session_Namespace('admin');
        if($info->zalogowany == 'ok')
		{
            $this->view->tytulosoby = $this->tytulosoby = $info->nazwa;
            $this->view->typosoby = $this->typosoby = 0;
			$this->view->adminID = $this->adminID = $info->id;
			$this->view->adminNazwa = $this->adminNazwa = $info->login;
			$this->view->loguser = $this->loguser = $this->adminNazwa;
			$this->view->uprawnienia = $this->uprawnienia = $info->prawa;
        }
		else $this->view->adminID = $this->adminID = 0;
		
		if($this->obConfig->producent)
		{
			$producenci = new Producent($this->module);
			$this->view->producenci = $this->producenci = $this->common->producenci = $producenci->getProducenci();
		}
		if($this->obConfig->kolory)
		{
			$kolory = new Kolory($this->module);
			$this->view->kolory = $this->kolory = $this->common->kolory = $kolory->wypiszKolory();
		}
		if($this->obConfig->tryb == 'rozmiary')
		{
			$rozmiarowki = new Rozmiarowka($this->module);
			$this->rozmiarowkiAll = $this->common->rozmiarowkiAll = $rozmiarowki->wypiszAll();
		}
		if($this->obConfig->rabaty)
		{
			$rabatyUser = new Rabaty($this->module);
			$this->rabatyAll = $this->common->rabatyAll = $rabatyUser->getUserSort(0);
		}
        
        $layout = Zend_Layout::startMvc();
    }
	
    protected function produktyGaleria($prod, $userID = 0, $hurt = null, $netto = null, $admin = true)
	{
		if($hurt === null) $hurt = $this->obConfig->tylkoHurt;
		if($netto === null) $hurt = $this->obConfig->cenyNettoDomyslne;
		$common = new Common(true, $admin ? 'admin' : 'default');
		return $common->produktyGaleria($prod, $userID, $hurt, $netto, $admin);
    }
	
	public function lang($nazwa) {
        if (!isset($this->common->tlumaczenia))
            $this->common->tlumaczenia = $this->common->getTlumaczenia();
        return isset($this->common->tlumaczenia[$nazwa]) ?
                $this->common->tlumaczenia[$nazwa] : $nazwa;
        return $this->view->translate($nazwa);
    }

    public function translate($nazwa) {
        if (!isset($this->common->tlumaczenia))
            $this->common->tlumaczenia = $this->common->getTlumaczenia();
        return isset($this->common->tlumaczenia[$nazwa]) ?
                $this->common->tlumaczenia[$nazwa] : $nazwa;
        return $this->view->translate($nazwa);
    }
}
?>