<?php
abstract class Ogolny_Controller_Sklep extends Zend_Controller_Action
{
    protected $obConfig;
    public $sortowanie, $www;

    public function init()
	{
        // load configuration
        $this->obConfig = new Zend_Config_Ini('../application/config.ini', 'general');
		$this->view->obConfig = $this->obConfig;

        set_include_path('.' . PATH_SEPARATOR . $this->obConfig->path->models . PATH_SEPARATOR . get_include_path());
		
		$this->common = new Common();
		$this->view->common = $this->common;
		
		$this->edycjazablokowana = false;
		$this->view->edycjazablokowana = false;
		
		$this->baseUrl = $this->_request->getBaseUrl();
		$this->view->baseUrl = $this->baseUrl;
		
		$this->params = $this->_request->getParams();
		$this->view->params = $this->params;
		//var_dump($this->params);
		$this->_module = $this->params['module'];
		$this->_controller = $this->params['controller'];
		$this->_action = @$this->params['action'];
		
		$URI = str_replace($this->_request->getBaseUrl(), '', $_SERVER['REQUEST_URI']);
		$this->URI = $URI;
        $this->view->URI = $URI;

        $this->www = $this->obConfig->www;
        $this->view->www = $this->obConfig->www;
		
		if($this->obConfig->allegro)
		{
			$this->konto = new Allegrokonta();
			$this->konto->ID = 1;
			$this->allegro = $this->konto->klient();
			$this->view->allegro = $this->allegro;
		}		
		if($this->obConfig->facebook)
		{
			$this->face = new Facebook();
			$this->face->ID = 1;
			$this->facebook = $this->face->klient();
			$this->view->facebook = $this->facebook;
		}

        $sortowanie = new Zend_Session_Namespace('sortowanie');
        if(strlen($this->_request->getParam('sortuj')) > 3)
		{
            $sortowanie->po = $this->_request->getParam('sortuj');
        }
        if(strlen($sortowanie->po) < 3)
		{
            $sortowanie->po = 'nazwa';
        }
        $this->sortowanie = $sortowanie->po;
    
        $info = new Zend_Session_Namespace('jezyk');
        if ($this->_request->getPost('jezyk'))
		{
            $lang_link = $this->_request->getPost('jezyk');
            if (strlen($lang_link) > 0)
			{
                $info->lang = $lang_link;
            }
        }
        if (isset($info->lang))
            $lang = $info->lang;
        else
            $lang = 'pl';
        $this->lang = $lang;
        $this->view->lang = $lang;

        if ($this->_request->getPost('newsletter'))
		{
            $email = $this->_request->getPost('newsletter');
            if (strlen($email) > 2)
			{
				if($this->_request->getPost('newsletterDodaj_x'))
                $this->zapiszDoBazyEmail($email, $this->lang);
				if($this->_request->getPost('newsletterUsun_x'))
                $this->usunZBazyEmail($email, $this->lang);
				$this->view->message = $this->view->newsletter;
            }
        }

        $this->menu();
		$this->menu_glowne();
		$this->menu_dolne();
		$this->menu_oferta();
        $this->elementyWspolne();
        $this->view->body = '';
        $this->view->link = 0;
		
		$producenci = new Producent();
        $this->producenci = $producenci->getProducenci();
        $this->view->producenci = $this->producenci;
		
		$kolor = new Kolory();
		$drukuj_kolor = $kolor->wypiszKolory();
		$this->view->kolory = $drukuj_kolor;

        $user = new Zend_Session_Namespace('kontrahent');
        if ($user->status == 'zalogowany')
		{
            $this->view->zalogowany = $user->nazwa;
			$this->view->zalogowanyID = $user->id;
        }
		else
		{
            $this->view->zalogowany = '';
			$this->view->zalogowanyID = 0;
        }
		
		if(isset($user->typ))
			$this->view->typ_klienta = $user->typ;
		else
			$this->view->typ_klienta = '';
		
		//var_dump($_POST);
		if($this->_request->getPost('koszyk') || $this->_request->getPost('koszyk_x'))
		{
			$koszyk = new Koszyk;
			$message = $koszyk->add();
			
			if($message != null && !empty($message))
			$this->view->message = $message;
			else $this->_redirect('/Koszyk/');
			//else $this->view->message = 'Dodano do koszyka';
		}		
		
        if (isset($_SESSION['wartosc_koszyka']) && isset($_SESSION['ilosc_koszyka']))
		{
            $this->view->wartosc_koszyka = $_SESSION['wartosc_koszyka'];
            $this->view->ilosc_koszyka = $_SESSION['ilosc_koszyka'];
        }
		else
		{
            $this->view->wartosc_koszyka = 0;
            $this->view->ilosc_koszyka = 0;
        }

        Zend_Layout::startMvc();
        $ileonline = new Ileonline();
        $this->view->liczbauser = $ileonline->sprawdz($_SERVER['REMOTE_ADDR'], $_SERVER['PHP_SELF']);
		
		$menu = new Menu();
        $menu->link = $this->_request->getBaseUrl().'/';
		
		$id = $this->_request->getParam('id', 0);
		//if(false)
		if($id > 0)
		{
			$kategoria = $menu->showWybranaKategoria($id);
			if($kategoria['rodzic'] > 0)
			$this->view->podkat = $menu->showWybranaKategoria($kategoria['rodzic']);
		}
		$ids = null;
		if(isset($this->params['action']) && $this->params['action'] == 'menu')
		{
			$ta = $menu->showWybranaKategoriaLink($this->params['id']);
			$path = $menu->get_path(@intval($ta['id']));
			//var_dump($path);die();
			foreach($path as $p) $ids[] = $p['id'];
			
			$menu->display_children_user(0, 0, $ids, null);
			$this->view->menu = $menu->menu;
		}
        $this->view->link = 2;
		$this->view->stopka = $menu->menue;
		
		$this->wojew = $this->common->getWojew();
		$this->view->wojew = $this->wojew;
		
		$this->mce = $this->common->getMce();
		$this->view->mce = $this->mce;
		
		$this->jedn = $this->common->getJedn();
		$this->view->jedn = $this->jedn;
		
		$this->ustawienia = $this->common->getUstawienia();
		$this->view->ustawienia = $this->ustawienia;
		
		$arrow = '<span> | </span>';
		$arrow = '<img src="'.$this->_request->getBaseUrl().'/public/images/strona/arrow_right.png" alt="&gt;" />';
		$this->arrow = $arrow;
		$this->view->arrow = $arrow;
    }

    private function menu()
	{
        $menu = new Kategorie();
        $menu->lang = $this->lang;
        $menu->link = $this->_request->getBaseUrl().'/';
		$params = $this->_request->getParams();
		$ids = null;
		//var_dump($params);
		if(isset($params['action']) && $params['action'] == 'kategorie')
		{
			$path = $menu->get_path($params['id']);
			//var_dump($path);
			foreach($path as $p) $ids[] = $p['id'];
		}
		if($params['controller'] == 'produkt')
		{
			$katprod = new Katprod();
			$nawigacja = $katprod->kategorieProduktu($params['id']);
			//var_dump($nawigacja);
			foreach($nawigacja as $n) $ids[] = $n['id'];
		}
		if($params['module'] == 'szablon')
		{
			$katprod = new Katprod();
			$nawigacja = $katprod->kategorieProduktu($params['id']);
			//var_dump($nawigacja);
			foreach($nawigacja as $n) $ids[] = $n['id'];
		}
		//var_dump($ids);
		$menu->getAllWithDetails();
        $menu->display_children_user(0, 0, $ids, null, $params['module'] == 'szablon');
        $this->view->menu = $menu->menu;
		$this->view->kategDzieci = $menu->dzieci;
    }
	
	private function menu_glowne()
	{
		$menu2 = new Kategorie();
		$menu2->lang = $this->lang;
		$menu = $menu2->renderuj_menu_glowne();
        $this->view->menu_glowne = $menu;
	}
	
	private function menu_dolne()
	{
		$menu3 = new Kategorie();
		$menu3->lang = $this->lang;
		$menu = $menu3->renderuj_menu_dolne();
        $this->view->menu_dolne = $menu;
	}
	
	private function menu_oferta()
	{
		$menu4 = new Menu();
		$menu4->lang = $this->lang;
		$menu = $menu4->start();
        $this->view->menu_oferta = $menu;
	}

    private function elementyWspolne()
	{
		$main = new Podstrony();
		$main->link = 'Projekt-w-trakcie-tworzenia';
        $this->view->projekt = $main->getPodstrona();
        $this->view->projekt->tekst = @stripslashes($this->view->projekt->tekst);
		
		$main = new Podstrony();
		$main->link = 'Banner';
        $this->view->banner = $main->getPodstrona();
        $this->view->banner->tekst = stripslashes($this->view->banner->tekst);
		
		if(true)
		{
			$main = new Podstrony();
			$main->link = 'daneKontakt';
			$this->view->kontakt = $main->getPodstrona();
			$this->view->kontakt->short = @stripslashes($this->view->kontakt->short);
		}
		
		if($this->obConfig->partnerzy)
		{
			$partnerzy = new Partnerzy();
			$this->view->partnerzy = $partnerzy->wypiszAll();
		}		
		
		if($this->obConfig->slajdy)
		{
			$slider = new Slider();
			$this->view->slajdy = $slider->wypiszAktywne();
		}
		
		$this->fraza = 'wpisz szukaną frazę';
		$this->view->fraza = $this->fraza;
		$this->frazaNews = 'Podaj adres e-mail';
		$this->view->frazaNews = $this->frazaNews;
		
		$szukanieStrona = new Zend_Session_Namespace('szukanieStrona');
		$this->view->szukanie = $szukanieStrona;
		if(!isset($szukanieStrona->producent)) $szukanieStrona->producent = -1;
		$this->view->producent = $szukanieStrona->producent;
		
		$sortowanie = new Zend_Session_Namespace('sortowanieStrona');
		$this->view->sortowanie = $sortowanie;
		$this->view->sort = $sortowanie->sort;
		$this->view->order = $sortowanie->order == 'asc' ? 'desc' : 'asc';
		
		$kats = new Kategorie();
		$kats->showAll();
		$this->view->kategorie = $kats->katAll;
    }

    protected function produktyGaleria($prod)
	{
		$common = new Common(true);
		return $common->produktyGaleria($prod, $this->view->zalogowanyID, $this->view->typ_klienta, false);
    }

    protected function zapiszDoBazyEmail($email, $lang) 
	{
        $subskrypcja = new Subskrypcja();
        if ($this->common->email($email)) 
		{
			$exist = $subskrypcja->sprMail($email);
			if(count($exist) > 0)
			{
				$this->view->color = 'yellow';
				$this->view->newsletter = 'Adres już istnieje w bazie danych';
			}
			else
			{
				$subskrypcja->dodaj($email, $lang);
				$this->view->color = 'green';
				$this->view->newsletter = 'Zapisano';
			}
        } 
		else 
		{
            $this->view->color = 'red';
            $this->view->newsletter = 'Błąd adresu e-mail';
        }
    }
	protected function usunZBazyEmail($email, $lang) 
	{
        $subskrypcja = new Subskrypcja();
        if ($this->common->email($email)) 
		{
            $exist = $subskrypcja->sprMail($email);
			if(count($exist) > 0)
			{
				$subskrypcja->usunMail($email, $lang);
				$this->view->color = 'green';
				$this->view->newsletter = 'Usunięto';
			}
			else
			{
				$this->view->color = 'yellow';
				$this->view->newsletter = 'Adres nie istnieje w bazie danych';
			}
        }
		else 
		{
            $this->view->color = 'red';
            $this->view->newsletter = 'Błąd adresu e-mail';
        }
    }
	
	function cmp($a, $b)
	{
		$cmp1 = $a[$this->sorter];
		$cmp2 = $b[$this->sorter];
		if($cmp1 == $cmp2) return 0;
		if($this->sortOrder == 'asc') return ($cmp1 < $cmp2) ? -1 : 1;
		if($this->sortOrder == 'desc') return ($cmp1 > $cmp2) ? -1 : 1;
	}
}
?>