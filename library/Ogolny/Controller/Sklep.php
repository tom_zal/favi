<?php

abstract class Ogolny_Controller_Sklep extends Zend_Controller_Action {

    protected $obConfig;
    public $sortowanie, $www, $zalogowanyID, $typ_klienta, $cenyNettoDomyslne;

    public function init() {
        // load configuration
        $this->view->obConfig = $this->obConfig = new Zend_Config_Ini('../application/config.ini', 'general');
        $this->view->obConfigImage = $this->obConfigImage = new Zend_Config_Ini('../application/config.ini', 'image');

        set_include_path('.' . PATH_SEPARATOR . $this->obConfig->path->models . PATH_SEPARATOR . get_include_path());

        $this->view->mobile = $this->mobile = $_SESSION['mobile'] = $this->isMobile() || @$_GET['mobile'];

        if (isset($_COOKIE["resolution"])) {
            //$resolution = explode('x', $_COOKIE["resolution"]);
            $resolutionX = @intval($_COOKIE["resolutionX"]);
            $resolutionY = @intval($_COOKIE["resolutionY"]);
        }

        $this->view->common = $this->common = new Common(true, 'default');
        $this->view->zl = $this->zl = $this->common->zl;
        $this->view->oznaczenia = $this->oznaczenia = $this->common->oznaczenia;
        $this->view->podstrony = $this->podstrony = $this->common->getPodstrony(true, "pl");
        
        if (count($this->view->podstrony) > 0)
            foreach ($this->podstrony as $strona => $podstrona) {
                $strona = $this->common->makeLink($strona, false);
                //$podstrona['temat'] = $this->lang($podstrona['link']);
                $page = $strona . 'Page';
                $this->view->{$page} = $this->{$page} = $podstrona;
                $link = $strona . 'Link';
                $this->view->{$link} = $this->{$link} = $podstrona['link'];
            }
        //$this->view->ofertaLink = $this->ofertaLink = "Oferta";
        //var_dump($this->view);die();
        if ($this->obConfig->aktualnosci) {
            $this->view->aktualnosciPage = $this->aktualnosciPage = $this->common->getPodstrona('Aktualności', true);
            $this->view->aktualnosciLink = $this->aktualnosciLink = $this->aktualnosciPage['link'];
        }
        $this->view->wynikiLink = $this->wynikiLink = 'Wyniki-wyszukiwania';
        $this->view->ustawienia = $this->ustawienia = $this->common->ustawienia;
        $this->view->edycjazablokowana = $this->edycjazablokowana = false;
        $this->view->dane_firmy = $this->dane_firmy = @unserialize($this->ustawienia['dane_firmy']);

        if ($this->obConfig->menuOfertaMainEdycja)
            $this->view->ustawMenu = @unserialize($this->ustawienia['menu']);
        if ($this->obConfig->slajdyUstawEdycja)
            $this->view->ustawSlider = @unserialize($this->ustawienia['slider']);

        $this->view->www = $this->www = $this->obConfig->www;
        $this->view->baseUrl = $this->baseUrl = $this->_request->getBaseUrl();
        $this->view->URI = $this->URI = str_replace($this->baseUrl, '', $_SERVER['REQUEST_URI']);
        $this->view->path = $this->path = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'] . $this->baseUrl);

        $this->view->session = $this->session = new Zend_Session_Namespace();
        $this->view->cache = $this->cache = $this->session->cache;
        $this->view->params = $this->params = $this->session->params = $this->_request->getParams();
        $this->view->module = $this->_module = $this->module = $this->params['module'];
        $this->view->controller = $this->_controller = $this->controller = $this->params['controller'];
        $this->view->action = $this->_action = $this->action = @strval($this->params['action']);
        //var_dump($this->params);

        $this->view->wojew = $this->wojew = $this->common->getWojew();
        $this->view->wojewKoresp = $this->wojewKoresp = $this->wojew;
        $this->view->mce = $this->mce = $this->common->getMce();
        $this->view->dni = $this->dni = $this->common->getDni();
        //$this->view->jedn = $this->jedn = $this->common->getJedn();
        //$this->view->szt = $this->szt = $this->common->getJedn(true);

        $this->view->page = $this->page = $this->common->makeLink('strona', false);

        if ($this->obConfig->allegro) {
            $this->konto = new Allegrokonta($this->module);
            $this->view->allegro = $this->allegro = $this->konto->klient();
        }
        if ($this->obConfig->facebook) {
            $this->face = new Facebooks($this->module);
            $this->face->ID = $this->obConfig->facebookPostProdukt ? 2 : 1;
            $this->view->facebook = $this->facebook = $this->face->klient();
        }
        if ($this->obConfig->youtube) {
            $this->tube = new Youtube($this->module);
            $this->view->youtube = $this->youtube = $this->tube->klient();
        }

        $sortowanie = new Zend_Session_Namespace('sortowanie');
        if (strlen($this->_request->getParam('sortuj')) > 3) {
            $sortowanie->po = $this->_request->getParam('sortuj');
        }
        if (strlen($sortowanie->po) < 3) {
            $sortowanie->po = 'nazwa';
        }
        $this->sortowanie = $sortowanie->po;

        $info = new Zend_Session_Namespace('jezyk');
        if ($this->_request->getPost('btn_lang')) {
            $info->lang = $this->_request->getPost('val_lang');
            $this->_redirect('/');
        }
        if (!isset($info->lang))
            $info->lang = 'pl';
        $this->view->lang = $this->lang = $lang = $info->lang;
        $this->view->langs = $this->langs = $this->common->getJezyki();
        $this->view->waluta = $this->waluta = @$this->langs[$lang]['waluta'];
        $this->view->zl = $this->zl = @$this->waluta['symbol'];

        if ($this->_request->getPost('newsletter')) {
            $email = $this->_request->getPost('newsletter');
            if (strlen($email) > 2) {
                if ($this->_request->getPost('newsletterDodaj_x'))
                    $this->zapiszDoBazyEmail($email, $this->lang);
                if ($this->_request->getPost('newsletterUsun_x'))
                    $this->usunZBazyEmail($email, $this->lang);
                $this->view->message = $this->view->newsletter;
            }
        }

        $user = new Zend_Session_Namespace('kontrahent');
        if ($user->status == 'zalogowany') {
            $this->view->zalogowany = $this->zalogowany = $user->nazwa;
            $this->view->zalogowanyID = $this->zalogowanyID = $user->id;
            $this->view->zalogowanyDane = $this->zalogowanyDane = $user->dane;
            $logowania = new KontrahenciLogin($this->module);
            $logowania->odswiez($user->id);
        } else {
            $this->view->zalogowany = $this->zalogowany = '';
            $this->view->zalogowanyID = $this->zalogowanyID = 0;
            $this->view->zalogowanyDane = $this->zalogowanyDane = null;
        }

        if ($this->obConfig->producent) {
            $producenci = new Producent($this->module);
            $this->view->producenci = $this->producenci = $this->common->producenci = $producenci->getProducenci();
        }
        if ($this->obConfig->kolory) {
            $kolory = new Kolory($this->module);
            $this->view->kolory = $this->kolory = $this->common->kolory = $kolory->wypiszKolory();
        }
        if ($this->obConfig->tryb == 'rozmiary') {
            $rozmiarowki = new Rozmiarowka($this->module);
            $this->rozmiarowkiAll = $this->common->rozmiarowkiAll = $rozmiarowki->wypiszAll();
        }
        if ($this->obConfig->rabaty) {
            $rabatyUser = new Rabaty($this->module);
            $this->rabatyAll = $this->common->rabatyAll = $this->zalogowanyID > 0 ? $rabatyUser->getUserSort($this->zalogowanyID) : null;
        }
        if ($this->obConfig->dostepnoscPrzedzialy) {
            $dostepnosc = new Dostepnosc($this->module);
            $this->view->dostepnosci = $dostepnosc->wypisz();
        }

        $this->menu();
        $this->menu_glowne();
        $this->menu_dolne();
        //$this->menu_oferta();
        $this->elementyWspolne();
        $this->view->body = '';
        //$this->view->link = 0;

        $this->view->typ_klienta = $this->typ_klienta = @intval($user->typ);

        $cenyNettoDomyslneHurt = ($this->typ_klienta && $this->obConfig->cenyNettoDomyslneHurt);
        $cenyNettoDomyslneDetal = (!$this->typ_klienta && $this->obConfig->cenyNettoDomyslneDetal);
        $this->view->cenyNettoDomyslne = $this->cenyNettoDomyslne = ($cenyNettoDomyslneHurt || $cenyNettoDomyslneDetal);

        $message = new Zend_Session_Namespace('message');
        if (isset($message->msg) && !empty($message->msg)) {
            $this->view->message = $message->msg;
            $this->view->messageDuration = 300000;
            $message->msg = ''; //die();
        }

        if ($this->obConfig->koszykCookie)
            if (@count($_SESSION['koszyk']) == 0)
                if (@$this->ustawienia['koszyk_czas'] > 0) {
                    $koszyk = new Koszyk($this->module);
                    $koszyk->koszykCookie();
                }

        //var_dump($_POST);die();
        if ($this->_request->getPost('koszyk') || $this->_request->getPost('koszyk_x') !== null) {
            $koszyk = new Koszyk($this->module);
            $msg = $koszyk->add();

            if ($msg != null && !empty($msg))
                $message->msg = $msg;
            //$this->view->message = $msg;
            $this->_redirect($this->URI);
            //else $this->view->message = 'Dodano do koszyka';
        }

        if (isset($_SESSION['wartosc_koszyka']) && isset($_SESSION['ilosc_koszyka'])) {
            $this->view->wartosc_koszyka = false && $this->typ_klienta ? $_SESSION['wartosc_koszyka_netto'] : $_SESSION['wartosc_koszyka'];
            $this->view->ilosc_koszyka = $_SESSION['ilosc_koszyka'];
        } else {
            $this->view->wartosc_koszyka = 0;
            $this->view->ilosc_koszyka = 0;
        }

        $layout = Zend_Layout::startMvc();
        $layout->setLayout($this->mobile ? 'mobile' : 'layout');

        $ileonline = new Ileonline($this->module);
        if (false) {
            if (@empty($this->view->message))
                $komunikat = $ileonline->messageNowy($_SERVER['REMOTE_ADDR']);
            if (@!empty($komunikat)) {
                $this->view->message = $komunikat;
                $this->view->messageDuration = 300000;
            }
        }
        $this->view->liczbauser = $ileonline->sprawdz($_SERVER['REMOTE_ADDR'], $_SERVER['PHP_SELF']);
        $this->view->liczbaodwiedzin = $this->ustawienia['odwiedzin'];

        $arrow = '<span> &gt; </span>';
        $arrow = '<img src="' . $this->baseUrl . '/public/images/strona/arrow_path.png" alt="&gt;" />';
        if ($this->mobile)
            $arrow = '<img src="' . $this->baseUrl . '/public/images/strona/mobile/arrow_path.png" alt="&gt;" />';
        $this->view->arrow = $this->arrow = $arrow;

        $listaProdIlosci['lista_short'] = array(20, 50, 100);
        $listaProdIlosci['lista_full'] = array(10, 20, 50);
        $listaProdIlosci['siatka_short'] = array(12, 24, 36);
        $listaProdIlosci['siatka_full'] = array(15, 30, 60);
        $this->listaProdIlosci = $listaProdIlosci;
        $this->view->listaProdIlosci = $listaProdIlosci;
        $this->view->listaProdIlosciAll = true;
        $this->view->listaProdTrybLayout = 'siatka_full';
        if ($this->mobile)
            $this->view->listaProdTrybLayout = 'lista_full';
        $this->view->listaProdTrybGlowna = 'siatka_full';
        $this->view->listaProdTrybProdukt = 'siatka_full';
        
        if (true){
            $main = new Podstrony($this->_module);
            $main->id = 175;
            $this->view->blok1 = $main->getPodstrona();
        }
        if (true){
            $main = new Podstrony($this->_module);
            $main->id = 171;
            $this->view->blok2 = $main->getPodstrona();
        }
        if (true){
            $main = new Podstrony($this->_module);            
            $main->id = 167;
            $this->view->blok3 = $main->getPodstrona();
        }
        if (true){
            $main = new Podstrony($this->_module);
            $main->id = 166;
            $this->view->blok4 = $main->getPodstrona();
        }
        
        if (true){
            $main = new Podstrony($this->_module);
            $this->view->podstronyAll = $main->getPodstronyAll();
            $qqq = $this->view->podstronyAll;
//            dump($qqq);die;
        }
    }

    private function menu() {
        $menu = new Kategorie($this->module);
        $menu->lang = $this->lang;
        $menu->link = $this->baseUrl . '/';
        $params = $this->params;
        $ids = null;
        //var_dump($params);
        if (isset($params['action']) && $params['action'] == 'kategorie') {
            $path = $menu->get_path($params['id']);
            //var_dump($path);
            foreach ($path as $p)
                $ids[] = $p['id'];
        }
        if ($params['controller'] == 'produkt') {
            $katprod = new Katprod($this->module);
            $nawigacja = $katprod->kategorieProduktu($params['id']);
            //var_dump($nawigacja);
            foreach ($nawigacja as $n)
                $ids[] = $n['id'];
        }
        if ($params['module'] == 'szablon') {
            $katprod = new Katprod($this->module);
            $nawigacja = $katprod->kategorieProduktu($params['id']);
            //var_dump($nawigacja);
            foreach ($nawigacja as $n)
                $ids[] = $n['id'];
        }
        //var_dump($ids);
        $menu->ids = $this->view->menuIDs = $ids;
        //$this->view->kategID = $kategID = count($ids) > 0 ? $ids[count($ids)-1] : 0;
        //$this->view->parentID = $parentID = count($ids) > 1 ? $ids[count($ids)-2] : 0;
        $menu->podstrona = $this->_request->getParam('podstrona', 'oferta');
        $menu->menu = "";
        $menu->typ = 'kategorie';
        $menu->active = false;
        $menu->getAllWithDetails();
        $menu->display_children_user(0, 0, $ids, null, $params['module'] == 'szablon');
        $this->view->menu = $menu->menu;
        $this->view->menuActive = $menu->active;
        $this->view->kategDzieci = $menu->dzieci;
        $this->view->myKats = $menu->kats;
//        dumpv($menu->kats);die;
        $arrKats = array();
        foreach ($menu->kats as $k) {
            if ($k['rodzic'] == 82)
                $arrKats[] = $k;
        }
        $this->view->katsBizuteria = $arrKats;
        $arrKats = array();
        foreach ($menu->kats as $k) {
            if ($k['rodzic'] == 84)
                $arrKats[] = $k;
        }
        $this->view->katsZegarki = $arrKats;

        //$this->view->kategListID = @count($menu->dzieci[$kategID]) > 0 ? $kategID : $parentID;
        if ($this->obConfig->kategorieInne) {
            $menu->menu = "";
            $menu->typ = $this->obConfig->kategorieInneNazwa;
            $menu->active = false;
            $menu->getAllWithDetails();
            $menu->display_children_user(0, 0, $ids, null, $params['module'] == 'szablon');
            $this->view->menuInne = $menu->menu;
            $this->view->menuInneActive = $menu->active;
            $this->view->kategDzieciInne = $menu->dzieci;
        }
        if (false && $this->obConfig->produktyGrupy) {
            $produktyGrupy = new Produktygrupy($this->module);
            $this->view->grupy = $grupy = $produktyGrupy->wypisz(true);
            if (true) {
                $szukanie = new Zend_Session_Namespace('szukanieStrona');
                if ($this->_request->getParam('grupa') != null)
                    $szukanie->grupa = intval($this->_request->getParam('grupa'));
                foreach ($grupy as $g => $grupa) {
                    $g = $grupa['id'];
                    $menu->menu = "";
                    $menu->typ = 'kategorie'; //$this->obConfig->kategorieInneNazwa;
                    $menu->grupa = $grupa['id'];
                    $menu->active = false;
                    $menu->getAllWithDetails();
                    $menu->display_children_user(0, 0, $ids, null, $params['module'] == 'szablon');
                    $menuGrupy[$g] = $menu->menu;
                    $menuGrupyActive[$g] = $menu->active && ($szukanie->grupa == $grupa['id']);
                    $menuGrupyDzieci[$g] = $menu->dzieci;
                }
                //var_dump($menuGrupy);die();
                $this->view->menuGrupy = @$menuGrupy;
                $this->view->menuGrupyActive = @$menuGrupyActive;
                $this->view->menuGrupyDzieci = @$menuGrupyDzieci;
            }
        }
    }

    private function menu_menu() {
        $menu = new Menu($this->module);
        $menu->link = $this->baseUrl . '/';

        $id = @intval($this->_request->getParam('id', 0));
        //if(false)
        if ($id > 0) {
            $kategoria = $menu->showWybranaKategoria($id);
            if ($kategoria['rodzic'] > 0)
                $this->view->podkat = $menu->showWybranaKategoria($kategoria['rodzic']);
        }
        $ids = null;
        if ($this->params['controller'] == 'podstrony' && @$this->params['action'] == 'menu') {
            $menu->site = $this->params['id'];
            $ta = $menu->showWybranaKategoriaLink($this->params['id']);
            $path = $menu->get_path(@intval($ta['id']));
            //var_dump($path);die();
            foreach ($path as $p)
                $ids[] = $p['id'];
            $menu->ids = $this->view->ids = $ids;
            $menu->params = $this->params;
            $menu->getAllWithDetails();
            if ($this->obConfig->menuOfertaMain) {
                $menu->display_children_user_menu(0, 0);
                $this->view->menuOferta = $menu->menue;
                $this->view->menu_dolne = @$menu->dzieci[0];
                //$this->view->menu_glowna = @$menu->wypiszMenu(0, 99, true);
            }
            $menu->display_children_user(0, 0, $ids, null);
            $this->view->menu = $menu->menu;
        } else if ($this->obConfig->menuOfertaMain) {
            $site = @$this->params['id'];
            if (intval($site) > 0)
                $site = $this->params['controller'];
            $site = strtolower($site);
            if (isset($this->zamienniki[$site]))
                $site = $this->zamienniki[$site];
            $menu->site = ucfirst($site);
            $menu->params = $this->params;
            $menu->getAllWithDetails();
            $menu->display_children_user_menu(0, 0);
            $this->view->menuOferta = $menu->menue;
            $this->view->menu_dolne = @$menu->dzieci[0];
            //$this->view->menu_glowna = @$menu->wypiszMenu(0, 99, true);
        }
        $this->menu = $menu;
        
    }

    private function menu_glowne() {
        
        $menu2 = new Kategorie($this->module);
        $menu2->lang = $this->lang;
        $menu = $menu2->renderuj_menu_glowne();
        $this->view->menu_glowne = $menu;
        //$this->view->podstrony = $menu2->showWybraniRodzicePodstrony();
    }

    private function menu_dolne() {
        $menu3 = new Kategorie($this->module);
        $menu3->lang = $this->lang;
        $menu = $menu3->renderuj_menu_dolne();
        $this->view->menu_dolne = $menu;
    }

    private function menu_oferta() {
        $menu4 = new Menu($this->module);
        $menu4->lang = $this->lang;
        $menu = $menu4->start();
        $this->view->menu_oferta = $menu;
    }

    private function elementyWspolne() {
        $main = new Podstrony($this->module);
        if (true) {
            $main->link = 'Projekt-w-trakcie-tworzenia';
            $this->view->projekt = $main->getPodstrona();
        }
        if (true) {
            $main->link = 'Stopka-blok';
            $this->view->stopka = $main->getPodstrona();
        }

        if ($this->obConfig->partnerzy) {
            $partnerzy = new Partnerzy($this->module);
            $this->view->partnerzy = $partnerzy->wypiszAll();
        }

        if (false && $this->obConfig->slajdy) {
            $slider = new Slider($this->module);
            $this->view->slajdy = $slider->wypiszAktywne();
            $this->view->slajd = $slider->wypiszGlowne();
        }

        if (false && $this->obConfig->glownaBestseller) {
            $achiwum = new Archiwum($this->module);
            if (!$this->obConfig->wlasneBestseller)
                $bestsellery = $achiwum->bestsellery(10);
            else {
                $kontrahenciPolecane = new Kontrahencipolecane($this->module);
                $bestsellery = $kontrahenciPolecane->wypiszKlientTyp(0, 'bestseller');
                $ids = empty($bestsellery['id_prod']) ? array(0) : @explode(',', $bestsellery['id_prod']);
                $bestsellery = $achiwum->bestsellery(99, $ids);
            }
            if (count($bestsellery) > 0) {
                $this->sorter = $this->obConfig->produktyPozycja ? 'pozycja' : 'nazwa';
                $this->sortOrder = 'asc';
                if ($bestsellery instanceof Zend_Db_Table_Rowset)
                    $bestsellery = $bestsellery->toArray();
                if (true)
                    usort($bestsellery, array($this, 'cmp'));
            }
            //var_dump($bestsellery);die();
            $this->view->bestsellery = $this->produktyGaleria($bestsellery);
            $this->view->bestselleryIlosc = 0;
            //var_dump($this->view->bestsellery);die();
        }

        //$produkty = new Produkty($this->module);
        //$all = $produkty->iloscProduktowAll();
        $this->view->fraza = $this->fraza = 'Szukaj produktu'; //$this->lang('fraza');
        $this->view->frazaMarka = $this->frazaMarka = 'Marka';
        $this->view->frazaModel = $this->frazaModel = 'Model';
        $this->view->frazaSymbol = $this->frazaSymbol = 'Model';
        $this->view->frazaNews = $this->frazaNews = 'Twój adres email';

        if (false && $this->_controller != 'oferta' && $this->_controller != 'produkt') {
            //if($test) {echo 'ssss';var_dump($this->_controller);die();}
            $szukanie = new Zend_Session_Namespace('szukanieStrona');
            $szukanie->nazwa = '';
            $szukanie->oznaczenie = '';
            $szukanie->grupa = 'all';
            $szukanie->producent = -1;
            $szukanie->kolor = -1;
            $szukanie->cena_od = 0;
            $szukanie->cena_do = 0;
            $szukanie->rozmiar = 'all';
            $szukanie->wkladka = 'all';
            $szukanie->atrybuty = null;

            $sortowanie = new Zend_Session_Namespace('sortowanieStrona');
            $sortowanie->sort = 'nazwa';
            $sortowanie->order = 'asc';
        }

        $szukanie = new Zend_Session_Namespace('szukanieStrona');
        $this->view->szukanie = $szukanie;
        if (!isset($szukanie->producent))
            $szukanie->producent = -1;
        $this->view->producent = $szukanie->producent;
        $this->view->cenaMax = @floatval(str_replace(',', '.', $szukanie->cena_do));

        $this->view->nazwa = @empty($szukanie->nazwa) ? $this->fraza : $szukanie->nazwa;
        $this->view->marka = @empty($szukanie->marka) ? $this->frazaMarka : $szukanie->marka;
        $this->view->model = @empty($szukanie->oznaczenie) ? $this->frazaModel : $szukanie->oznaczenie;

        $sortowanie = new Zend_Session_Namespace('sortowanieStrona');
        $this->view->sortowanie = $sortowanie;
        $this->view->sort = $sortowanie->sort;
        $this->view->order = $sortowanie->order == 'asc' ? 'desc' : 'asc';

        if (true) {
            $kats = new Kategorie($this->module);
            $kats->showAll();
            $this->view->kategorie = $kats->katAll;
        }
    }

    public function produktyGaleria($prod) {
        return $this->common->produktyGaleria($prod, $this->zalogowanyID, $this->typ_klienta, $this->cenyNettoDomyslne, false);
    }

    public function lang($nazwa) {
        if (!isset($this->common->tlumaczenia))
            $this->common->tlumaczenia = $this->common->getTlumaczenia();
        return isset($this->common->tlumaczenia[$nazwa]) ? $this->common->tlumaczenia[$nazwa] : $nazwa;
        return $this->view->translate($nazwa);
    }

    public function translate($nazwa) {
        if (!isset($this->common->tlumaczenia))
            $this->common->tlumaczenia = $this->common->getTlumaczenia();
        return isset($this->common->tlumaczenia[$nazwa]) ? $this->common->tlumaczenia[$nazwa] : $nazwa;
        return $this->view->translate($nazwa);
    }

    protected function zapiszDoBazyEmail($email, $lang) {
        $subskrypcja = new Subskrypcja($this->module);
        if ($this->common->email($email)) {
            $exist = $subskrypcja->sprMail($email);
            if (count($exist) > 0) {
                $this->view->color = 'yellow';
                $this->view->newsletter = $this->lang('komunikat_newsletter_error');
            } else {
                $subskrypcja->dodaj($email, $lang);
                $this->view->color = 'green';
                $this->view->newsletter = $this->lang('komunikat_newsletter_ok');
            }
        } else {
            $this->view->color = 'red';
            $this->view->newsletter = $this->lang('komunikat_newsletter_error2');
        }
    }

    protected function usunZBazyEmail($email, $lang) {
        $subskrypcja = new Subskrypcja($this->module);
        if ($this->common->email($email)) {
            $exist = $subskrypcja->sprMail($email);
            if (count($exist) > 0) {
                $subskrypcja->usunMail($email, $lang);
                $this->view->color = 'green';
                $this->view->newsletter = $this->lang('komunikat_newsletter_ok');
            } else {
                $this->view->color = 'yellow';
                $this->view->newsletter = $this->lang('komunikat_newsletter_error2');
            }
        } else {
            $this->view->color = 'red';
            $this->view->newsletter = $this->lang('komunikat_newsletter_error2');
        }
    }

    function cmp($a, $b) {
        $cmp1 = $a[$this->sorter];
        $cmp2 = $b[$this->sorter];
        if ($cmp1 == $cmp2)
            return 0;
        if ($this->sortOrder == 'asc')
            return ($cmp1 < $cmp2) ? -1 : 1;
        if ($this->sortOrder == 'desc')
            return ($cmp1 > $cmp2) ? -1 : 1;
    }

    function cmpPL($a, $b) {
        return strcasecmp($this->common->usunPolskieZnaki($a), $this->common->usunPolskieZnaki($b));
    }

    function isMobile() { //http://detectmobilebrowsers.com/
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        return (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4)));
    }

}

?>