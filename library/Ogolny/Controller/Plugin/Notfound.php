<?php
    class Ogolny_Controller_Plugin_Notfound extends Zend_Controller_Plugin_Abstract {
        public function preDispatch(Zend_Controller_Request_Abstract $request) {
            $dispatcher = Zend_Controller_Front::getInstance()->getDispatcher();
            if (!$dispatcher->isDispatchable($request)) {
                if($request->getModuleName() != 'admin') {
                    $request->setControllerName('index')
                        ->setActionName('index')
                        ->setModuleName('default')
                        ->setDispatched(true);
                } else {
                    $request->setControllerName('index')
                        ->setActionName('panel')
                        ->setModuleName('admin')
                        ->setDispatched(false);
                }
            }
        }
    }
?>