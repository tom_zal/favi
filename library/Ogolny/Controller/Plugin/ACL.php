<?php

    class Ogolny_Controller_Plugin_ACL extends Zend_Controller_Plugin_Abstract {
        
        public function preDispatch(Zend_Controller_Request_Abstract $request) {
            $session = new Zend_Session_Namespace();

            /*
             * Role użytkowników
             */
            $acl = new Zend_Acl();
            $acl->addRole(new Zend_Acl_Role('guest'));
            $acl->addRole(new Zend_Acl_Role('klient'), 'guest');
            $acl->addRole(new Zend_Acl_Role('admin'), 'guest');
            $acl->addRole(new Zend_Acl_Role('marketer'), 'guest');
            $acl->addRole(new Zend_Acl_Role('superuser'), 'admin');

            /*
             * Dodanie zasobów
             */
            $acls = array(
                'default' => array('index','podstrony'),
                'klient' => array('uzytkownik'),
                'ajax' => array(
                    'ajax',
                    array('ajax:default', 'ajax'),
                    array('ajax:admin', 'ajax'),
                    array('ajax:index', 'ajax'),
                    array('ajax:upominki', 'ajax'),
                    array('ajax:gadzety', 'ajax'),
                    array('ajax:podarunkowe', 'ajax'),
                    array('ajax:kodyrabatowe', 'ajax')
                ),
                'admin' => array(
                    'admin',
                    array('admin:index', 'admin'),
                    array('admin:lojalnosc', 'admin'),
                    array('admin:archiwum', 'admin'),
                    array('admin:confmail', 'admin'),
                    array('admin:confsms', 'admin'),
                    array('admin:kontrahent', 'admin'),
                    array('admin:uzytkownicy', 'admin'),
                    array('admin:banery', 'admin'),
					array('admin:ustawienia', 'admin'),
                ),
            );

            foreach($acls as $a)
                foreach($a as $ac)
                    if (is_array($ac))
                        $acl->add(new Zend_Acl_Resource($ac[0]), $ac[1]);
                    else
                        $acl->add(new Zend_Acl_Resource($ac));

            /*
             * Prawa dostępu użytkownika do zasobów
             */
            foreach($acls['default'] as $a)
                $acl->allow('guest', $a);
            
            foreach($acls['klient'] as $a)
                $acl->allow('klient', $a);

            $acl->allow('guest', 'ajax');
            $acl->allow('guest', 'admin:index', array('logowanie','blocked','index'));
            $acl->allow('admin','admin');
            $acl->allow('marketer','admin:index', array('logowanie','index','wyloguj','panel'));
            $acl->allow('marketer','ajax:admin');
            $acl->allow('marketer','admin:lojalnosc', array('dodajzakup','lista','dodajklient'));
            $acl->allow('marketer','admin:kontrahent', array('upominki','upominekwybierz'));
            $acl->allow('marketer','admin:uzytkownicy', array('konto'));
            $acl->allow('admin', 'ajax:admin');
            $acl->allow('superuser','admin');
            $acl->allow('superuser', 'ajax:admin');

            $auth = Zend_Auth::getInstance();
            if ($auth->hasIdentity()) {
                $stor = $auth->getStorage()->read();
                $rola = $stor->role;
            } else {
                $rola = 'guest';
            }

            /*
             * Sprawdzenie modułów
             */
            $resourceName = '';
            if ($request->getModuleName() != 'default')
                $resourceName .= $request->getModuleName().':';
                $resourceName .= $request->getControllerName();

            /*
             * Przeniesienie w przypadku braku autoryzacji
             */
            try {
                if (!$acl->isAllowed($rola, $resourceName, $request->getActionName())) {
                    switch($request->getModuleName()) {
                        case 'admin':
                            $request->setModuleName('admin');
                            $request->setControllerName('index');
                            $request->setActionName('blocked');
                        break;
                        default:
                            $request->setControllerName('index');
                            $request->setActionName('index');
                        break;
                    }
                }
            } catch(Exception $e) {
                $request->setControllerName('index');
                $request->setActionName('index');
            }

        }

    }

?>