<?php
    class Ogolny_Controller_Plugin_Translate extends Zend_Controller_Plugin_Abstract {
        public function preDispatch(Zend_Controller_Request_Abstract $request) {
            $locale = new Zend_Locale();

            $translate = new Zend_Translate('array', '../application/languages/pl.php', 'pl');
			
			$common = new Common(false);
			foreach($common->langs as $lg => $lang) if($lg != 'pl')
			$translate->addTranslation('../application/languages/'.$lg.'.php', $lg);
			
			if(false)
			{
			$translate->addTranslation('../application/languages/en.php', 'en');
            $translate->addTranslation('../application/languages/de.php', 'de');
            $translate->addTranslation('../application/languages/fr.php', 'fr');
			$translate->addTranslation('../application/languages/es.php', 'es');
			$translate->addTranslation('../application/languages/it.php', 'it');
			$translate->addTranslation('../application/languages/uk.php', 'uk');
			$translate->addTranslation('../application/languages/ru.php', 'ru');
			}
                      
            $seslang = new Zend_Session_Namespace('jezyk');
            if(isset($seslang->lang))
                $lang = $seslang->lang;
            else
                $lang = 'pl';
            
            $locale->setLocale($lang);

            $translate->setLocale($locale);
            Zend_Registry::set('locale', $locale->getLanguage());
            Zend_Registry::set('Zend_Translate', $translate);
        }
    }
?>