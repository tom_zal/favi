<?php
abstract class Ogolny_Controller_Page extends Zend_Controller_Action
{
    protected $obConfig, $tlumaczenie, $lang;

    public function init()
	{
        // load configuration
        $this->obConfig = new Zend_Config_Ini('../application/config.ini', 'general');
		$this->view->obConfig = $this->obConfig;
		
		$this->common = new Common();
		$this->view->common = $this->common;
		
		$this->ustawienia = $this->common->getUstawienia();
		$this->view->ustawienia = $this->ustawienia;
		
		$session = new Zend_Session_Namespace();
		
		$this->params = $this->_request->getParams();
		$this->view->params = $this->params;
		$session->params = $this->params;
		
		$this->baseUrl = $this->_request->getBaseUrl();
		$this->view->baseUrl = $this->baseUrl;
		
		$this->path = str_replace('//','/',$_SERVER['DOCUMENT_ROOT'].$this->baseUrl);
		$this->view->path = $this->path;

        set_include_path('.' . PATH_SEPARATOR . $this->obConfig->path->models . PATH_SEPARATOR . get_include_path());

        // wspólne dla wszystkich podstron

        $info = new Zend_Session_Namespace('jezyk');

        if ($this->_request->isPost())
		{
            $lang_link = $this->_request->getPost('jezyk');

            if (strlen($lang_link) > 0)
			{
                $info->lang = $lang_link;
            }
        }

        if (isset($info->lang))
            $lang = $info->lang;
        else
            $lang = 'pl';
        $this->lang = $lang;
        $this->view->lang = $lang;

        //$tlumaczenia = new Zend_Config_Ini('../application/lang.ini', 'lang');
        // $this->view->tlumaczenie = $tlumaczenia->toArray();

        if ($this->_request->isPost())
		{
            $email = $this->_request->getPost('email');
            if (strlen($email) > 2)
			{
                //$this->zapiszDoBazyEmail($email, $this->lang);
            }
        }
		
        //$this->menu();
        //$this->wypiszKategorieGlowne();
		
		$info = new Zend_Session_Namespace('admin');
        if($info->zalogowany == 'ok')
		{
            $this->view->tytulosoby = $this->tytulosoby = $info->nazwa;
			$this->view->adminID = $this->adminID = $info->id;
			$this->view->adminNazwa = $this->adminNazwa = $info->login;
			$this->view->loguser = $this->loguser = $this->adminNazwa;
        }
		
        $layout = Zend_Layout::startMvc();
    }

    /*
	private function menu()
	{
		$menu = new Menu();
		$menu->link = $this->_request->getBaseUrl() . '/';
		$menu->wyswietl_drzewo(0, 0, 'kategoria_glowna', 'kategoria_podkat');
		$this->view->menu = $menu->menu;
    }
	*/

    private function menu()
	{
        $menu = new Kategorie();
        $menu->lang = $this->lang;
        $menu->link = $this->_request->getBaseUrl() . '/';
        $menu->display_children_user(0, 0, 'kategoria_glowna', 'kategoria_podkat', 0);
        $this->view->menu = $menu->menu;
    }

    protected function produktyGaleria($prod)
	{
		$common = new Common(true);
		return $common->produktyGaleria($prod, $this->view->zalogowanyID, $this->view->typ_klienta, false);
    }

    protected function NowosciPromcyjne()
	{
        $specjalne = new Produkty();
        $prod = $specjalne->wypiszSpecjalne('nowosc', $this->lang, 1, $od, 3);
        $all = $this->produktyGaleria($prod);
        $this->view->nowosciPromocyjne = $all;
        $liczba = $specjalne->iloscProduktowSpecjalnych('nowosc', $this->lang, 1);
        if ($liczba > 3)
            $this->view->dalej = 'visible';
        else
            $this->view->dalej = 'hidden';
    }

    protected function wypiszKategorieGlowne()
	{
        $kategorie = new Kategorie();
        $kategorie->lang = $this->lang;
        $kategorie->link = $this->_request->getBaseUrl() . '/';
        $glowne = $kategorie->showWybranyRodzic(0);
        $this->view->kategorieglowne = $glowne;
    }

    protected function zapiszDoBazyEmail($email, $lang)
	{
        $subskrypcja = new Subskrypcja();
        if ($this->common->email($email))
        {
            $subskrypcja->dodaj($email, $lang);
            $this->view->color = 'green';
            $this->view->newsletter = 'Zapisano';
        }
		else
		{
            $this->view->color = 'red';
            $this->view->newsletter = 'Błąd adresu e-mail';
        }
    }
}
?>