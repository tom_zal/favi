<?php
abstract class Ogolny_Controller_Admin extends Zend_Controller_Action
{
    private $oFCKeditor;
    protected $obConfig;
    public $ImageDir, $SmallImageDir, $lang;
	protected $_inituserrole = array('admin','user');

    public function init()
	{
        // load configuration
		$this->view->obConfig = $this->obConfig = new Zend_Config_Ini('../application/config.ini', 'general');
		$this->view->ImageDir = $this->ImageDir = new Zend_Config_Ini('../application/config.ini', 'image');
        $this->SmallImageDir = $this->ImageDir->ImageDir . '' . $this->ImageDir->SmallImageDir;
		
		$this->view->menuBoczne = $this->obConfig->menuBoczne;
		if($this->view->menuBoczne)
		if(isset($_COOKIE["resolution"]))
		{
			//$resolution = explode('x', $_COOKIE["resolution"]);
			$resolutionX = @intval($_COOKIE["resolutionX"]);
			$resolutionY = @intval($_COOKIE["resolutionY"]);
			if($resolutionX > 0 && $resolutionX < 1200) $this->view->menuBoczne = false;
		}
		
		$message = new Zend_Session_Namespace('message');
		if(isset($message->msg) && !empty($message->msg))
		{
			$this->view->message = $message->msg;
			$message->msg = '';
		}
		
		$gc_maxlifetime = ini_get("session.gc_maxlifetime");
                $gc_maxlifetime = 36000;

                ini_set("session.gc_maxlifetime", 60 * 60);
		//ini_set("session.gc_maxlifetime", $gc_maxlifetime);
		$this->view->lifetime_max = $lifetime_max = min($gc_maxlifetime, ini_get("session.gc_maxlifetime"));
		$this->view->lifetime_now = $time = time();
//		                dump($gc_maxlifetime);die;
		$lang = new Zend_Session_Namespace('jezyk_admin');
        $lang_btn = $this->_request->getParam('lang');
        if(!empty($lang_btn))
		{
			$lang->wybrana = $lang->lang = $lang_btn;
			$this->_redirect('admin/index/panel/zakladka/1');
		}
		if(!isset($lang->wybrana)) $lang->wybrana = $lang->lang = 'pl';
        $this->view->lang = $this->lang = $lang->wybrana;
		$this->view->jezyk = $this->jezyk = $lang->wybrana;
		
		$this->view->common = $this->common = new Common(true, 'admin');
		$this->view->zl = $this->zl = $this->common->zl;		
		$this->view->oznaczenia = $this->oznaczenia = $this->common->oznaczenia;
		$this->view->ustawienia = $this->ustawienia = $this->common->getUstawienia();
		$this->view->langs = $this->langs = $this->common->getJezyki();
		$this->view->edycjazablokowana = $this->edycjazablokowana = false;
		
		$this->view->www = $this->www = $this->obConfig->www;
		$this->view->baseUrl = $this->baseUrl = $this->_request->getBaseUrl();
		$this->view->URI = $this->URI = str_replace($this->baseUrl, '', $_SERVER['REQUEST_URI']);
		$this->view->path = $this->path = str_replace('//','/',$_SERVER['DOCUMENT_ROOT'].$this->baseUrl);
		
		$this->session = $session = new Zend_Session_Namespace();
		$this->view->params = $this->params = $session->params = $this->_request->getParams();
		$this->view->module = $this->_module = $this->module = $this->params['module'];
		$this->view->controller = $this->_controller = $this->controller = $this->params['controller'];
		$this->view->action = $this->_action = $this->action = @$this->params['action'];
		//var_dump($this->params);
		$this->cache = $session->cache;
        $this->caching = false;
		
		$this->view->wojew = $this->wojew = $this->common->getWojew();
		$this->view->wojewKoresp = $this->wojewKoresp = $this->wojew;		
		//$kraje = new Allegrokraje();
		//$this->view->kraje = $this->kraje = $kraje->getCountries();
		$this->view->kosztyAllegroAll = $this->kosztyAllegroAll = $this->common->getKosztyAllegro();
		$this->view->mce = $this->mce = $this->common->getMce();
		$this->view->dni = $this->dni = $this->common->getDni();
		$this->view->jedn = $this->jedn = $this->common->getJedn(false);
		$this->view->jednMain = $this->jednMain = $this->common->getJedn(true);
		$this->view->fonts = $this->fonts = $this->common->getFonts();
		$this->view->ceny = $this->ceny = $this->common->getCeny();
		$this->view->typyZamowien = $this->typyZamowien = $this->common->getZamowieniaTypy();
		$this->view->typyZamowienAllegro = $this->typyZamowienAllegro = $this->common->getAllegroZamowieniaTypy();
		$this->view->typyPlatnosci = $this->typyPlatnosci = $this->common->getPlatnosci();
		$this->view->rodzajePlatnosci = $this->rodzajePlatnosci = $this->common->getRodzajePlatnosci();
		$this->view->prawa = $this->prawa = $this->common->getUprawnienia();
		$this->view->prawaMapa = $this->prawaMapa = $this->common->getKontrolerToPrawo();
		
		$ileonline = new Ileonline();
        $this->view->liczbauser = $ileonline->sprawdz($_SERVER['REMOTE_ADDR'], $_SERVER['PHP_SELF']);
		$this->view->liczbaodwiedzin = @$this->ustawienia['odwiedzin'];

        set_include_path('.' . PATH_SEPARATOR . $this->obConfig->path->models . PATH_SEPARATOR . get_include_path());
		
		$this->view->headScript()->appendFile($this->_request->getBaseUrl().'/public/scripts/jquery.tools.min.js', 'text/javascript');
        $this->view->headLink()->appendStylesheet($this->_request->getBaseUrl().'/public/styles/tooltip.css');

        $info = new Zend_Session_Namespace('admin');
        if($info->zalogowany == 'ok')
		{
            $layoutfile = 'zalogowany';
            $this->view->tytulosoby = $this->tytulosoby = $info->nazwa;
            $this->view->typosoby = $this->typosoby = 0;
			$this->view->adminID = $this->adminID = $info->id;
			$this->view->adminNazwa = $this->adminNazwa = $info->login;
			$_SESSION['loguser'] = $this->loguser = $this->adminNazwa;
			$this->view->uprawnienia = $this->uprawnienia = $info->prawa;
			if(@!empty($this->prawaMapa[$this->_controller]))
			if($info->prawa != 'wszystko' && strpos($info->prawa, $this->prawaMapa[$this->_controller]) === false)
			$this->view->edycjazablokowana = $this->edycjazablokowana = true;
			//var_dump($this->edycjazablokowana);die();
            if($info->adres_ip !== $_SERVER['REMOTE_ADDR'])
            {
               $info->zalogowany = 'ko';
               $layoutfile = 'niezalogowany';
               $this->_forward("index");
               $this->view->error_logowania = 'Błąd logowania: Nieprawidłowy Login lub Hasło';
            }
			else
			{
				$logowania = new KontrahenciLogin();
				$logowania->odswiez($info->id, 'login');
				if($this->action != 'wyloguj')
				{
					$akcja = new Login();
					$akcja->id = $info->id;
					$akcja->edytuj(array('last_link' => $this->URI));
				}
			}
        }
		else
		{
            $layoutfile = 'niezalogowany';
            $this->_forward("index");
			if($this->_controller != 'index')
			$this->_redirect('admin');
        }
		
		if($this->obConfig->producent)
		{
			$producenci = new Producent();
			$this->view->producenci = $this->producenci = $this->common->producenci = $producenci->getProducenci();
		}
		if($this->obConfig->kolory)
		{
			$kolory = new Kolory();
			$this->view->kolory = $this->kolory = $this->common->kolory = $kolory->wypiszKolory();
		}
		if($this->obConfig->tryb == 'rozmiary')
		{
			$rozmiarowki = new Rozmiarowka();
			$this->rozmiarowkiAll = $this->common->rozmiarowkiAll = $rozmiarowki->wypiszAll();
		}
		if(false && $this->obConfig->rabaty)
		{
			$rabatyUser = new Rabaty();
			$this->rabatyAll = $this->common->rabatyAll = $rabatyUser->getUserSort(0);
		}
		
		if($this->_controller == 'index' && $this->_action == 'panel')
		{
			unset($_SESSION['lojalnosc']);
		}
		//var_dump($_SESSION['lojalnosc']);
		if($this->obConfig->lojalnosc || @$_SESSION['lojalnosc']) //$this->_controller == 'lojalnosc'
		{
			$layoutfile = 'lojalnosc';
			$_SESSION['lojalnosc'] = true;
			
			$ob = new Query('Lojalnoscustawienia');
			$ob->id = 1;
			$row = $ob->getRow();
			if(isset($row['zapisano']) && $row['zapisano'] == 1)
			{
				$data = $this->timeUpCheck(date('Y-m-d'), $row['koniec']);
				$czas = round($data / $this->jednostkiczasu[$this->wybranajednostka]);
				if($czas <=14)
				{
					$this->view->popupcontent = '<span class="pT">Informacja:</span><p class="pC">Do końca programu lojalnościowego pozostało: <b>'.$czas.'</b> '.$this->wybranajednostka.'<br />Możesz <b>przedłużyć</b> okres trwania programu w zakładce <br /><b>"Rabaty i upominki"</b>.</p>';
					if($czas == 0)
					{
						$ob2 = new Query('Kontrahenci');
						$ob2->column = 'konto';
						$ob2->value = 0;
						$ob2->_updateValueRange(array('konto'=>0.00));						
						$ob->_update(array('wyczyszczono'=> date('Y-m-d H:i:s'),'status'=>1));						
						$this->view->popupcontent .= '<p class="pC">Upłynął ustawiony czas trwania programu.<br/>Koniec: <b>'.$row['koniec'].'</b>.<br /><br />Stan konta klientów został <b>wykasowany</b>.</p>';
					}
				}
			}
		}
        
        $zakladka = (int)$this->_request->getParam('zakladka', 0);
        if($zakladka > 0)
		{
            $page = new Zend_Session_Namespace('zakladka');
            $page->wybrana = $zakladka;
        }
        $page = new Zend_Session_Namespace('zakladka');
        $this->view->zakladka_admin = $page->wybrana;

		$this->menu_podstrony();
        //initialize Zend Layout
        $layout = Zend_Layout::startMvc();
        $layout->setLayout($layoutfile);
    }

    function fcKeditor($nazwa,$tresc,$height=400,$toolbar='posredni',$width=820,$skinpath='office2003',$onlyread=false)
	{
		if(empty($nazwa))
		{
            throw new Exception('Nazwa pola jest wymagana');
        }
		if($this->obConfig->edytor=='fck')return $this->fck($nazwa,$tresc,$height,$toolbar,$width,$skinpath,$onlyread);
		if($this->obConfig->edytor=='cke')return $this->cke($nazwa,$tresc,$height,$toolbar,$width,$skinpath,$onlyread);
		if($this->obConfig->edytor=='mce')return $this->mce($nazwa,$tresc,$height,$toolbar,$width,$skinpath,$onlyread);
	}
	
	function fck($nazwa, $tresc, $height, $toolbar, $width, $skinpath, $onlyread = false)
	{
        // $this->view->editor = array();
		if(!empty($this->oFCKeditor[$nazwa])) 
		{
            throw new Exception('Nazwy pola nie mogą sie dublować');
        } 
		else 
		{
            $this->oFCKeditor[$nazwa] = new FCKeditor($nazwa);
            $this->oFCKeditor[$nazwa]->BasePath = $this->view->baseUrl . '/public/scripts/fckeditor/';
            $this->oFCKeditor[$nazwa]->Config['SkinPath'] = 'skins/' . $skinpath . '/';
            $this->oFCKeditor[$nazwa]->ToolbarSet = $toolbar;//Basic,short,Default,posredni
            $this->oFCKeditor[$nazwa]->Width = $width;
            $this->oFCKeditor[$nazwa]->Height = $height;
            $this->oFCKeditor[$nazwa]->Value = $tresc;
			if(true)
			{
				if($this->obConfig->filebrowser == 'kcfinder')
				{
				$filebrowser = $this->view->baseUrl.'/public/scripts/kcfinder/browse.php?opener=fckeditor&';
				$fileuploader = $this->view->baseUrl.'/public/scripts/kcfinder/upload.php?opener=fckeditor&';
				$this->oFCKeditor[$nazwa]->Config['LinkBrowserURL'] = $filebrowser.'type=files';
				$this->oFCKeditor[$nazwa]->Config['ImageBrowserURL'] = $filebrowser.'type=image';
				$this->oFCKeditor[$nazwa]->Config['FlashBrowserURL'] = $filebrowser.'type=flash';
				$this->oFCKeditor[$nazwa]->Config['LinkUploadURL'] = $fileuploader.'type=files';
				$this->oFCKeditor[$nazwa]->Config['ImageUploadURL'] = $fileuploader.'type=image';
				$this->oFCKeditor[$nazwa]->Config['FlashUploadURL'] = $fileuploader.'type=flash';
				}
				$this->oFCKeditor[$nazwa]->Config['LinkBrowserWindowWidth'] = 700;
				$this->oFCKeditor[$nazwa]->Config['ImageBrowserWindowWidth'] = 700;
				$this->oFCKeditor[$nazwa]->Config['FlashBrowserWindowWidth'] = 700;
				$this->oFCKeditor[$nazwa]->Config['LinkBrowserWindowHeight'] = 500;
				$this->oFCKeditor[$nazwa]->Config['ImageBrowserWindowHeight'] = 500;
				$this->oFCKeditor[$nazwa]->Config['FlashBrowserWindowHeight'] = 500;
				$this->oFCKeditor[$nazwa]->Config['LinkUpload'] = false;
				$this->oFCKeditor[$nazwa]->Config['ImageUpload'] = false;
				$this->oFCKeditor[$nazwa]->Config['FlashUpload'] = false;
				$this->oFCKeditor[$nazwa]->Config['ImageBrowser'] = true;
			}
            return $this->oFCKeditor[$nazwa]->CreateHtml();
        }
    }
	
	function cke($nazwa, $tresc, $height=400, $toolbar='basic', $width=900, $skinpath='office2003', $onlyread=false)
	{
		$img = $this->obConfig->filebrowser == 'kcfinder' ? 'image' : 'Images';
		$fla = $this->obConfig->filebrowser == 'kcfinder' ? 'flash' : 'Flash';
		$this->cke[$nazwa] = new CKEditor();
		$this->cke[$nazwa]->returnOutput = true;
		$basePath = $this->common->local ? '/../../__scripts/ckeditor' : $this->baseUrl.'/public/scripts/ckeditor';
		$this->cke[$nazwa]->basePath = $basePath.'/';
		$filebrowser = $basePath.'/plugins/filemanager/index.html?';
		if($this->obConfig->filebrowser == 'kcfinder')
		$filebrowser = $this->baseUrl.'/public/scripts/kcfinder/browse.php?opener=ckeditor&';
		$this->cke[$nazwa]->config['filebrowserBrowseUrl'] = $filebrowser.'';
		$this->cke[$nazwa]->config['filebrowserImageBrowseUrl'] = $filebrowser.'type='.$img;
		//$this->cke[$nazwa]->config['filebrowserFlashBrowseUrl'] = $filebrowser.'type='.$fla;
		$fileuploader = $filebrowser.'/connectors/php/filemanager.php?';
		if($this->obConfig->filebrowser == 'kcfinder')
		$fileuploader = $this->baseUrl.'/public/scripts/kcfinder/upload.php?opener=ckeditor&';
		$this->cke[$nazwa]->config['filebrowserUploadUrl'] = $fileuploader;
		$this->cke[$nazwa]->config['filebrowserImageUploadUrl'] = $fileuploader.'command=QuickUpload&type='.$img;
		$this->cke[$nazwa]->config['filebrowserFlashUploadUrl'] = $fileuploader.'command=QuickUpload&type='.$fla;
		// print_r($this->cke[$nazwa]->basePath);die;
	
		$config = array();
		$config['readOnly'] = $onlyread;
		$config['enterMode'] = 'CKEDITOR.ENTER_BR';
		$config['allowedContent'] = true;//allow all tags
		//$config['extraAllowedContent'] = 'iframe,hr';
		$config['width'] = $width;// = 900;
		$config['height'] = $height;
		//dump($toolbar);exit();
		if($toolbar == 'basic')
		{
			$config['toolbar'] = array
			(
				array( 'Source' ),
				array( 'Bold', 'Italic', 'Underline', 'Strike' )
			);
		}
		else
		{    			
			$config['toolbar'] = array
			(
				array('Source','DocProps','-','Save','NewPage','Preview','-','Templates'),
				array('Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'),
				array('Image','Flash','Table','Rule','Smiley','SpecialChar','PageBreak'),
				
				array('Bold','Italic','Underline','Strike','-','Subscript','Superscript'),
				array('NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'),
				array('JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'),
				array('Link','Unlink','Anchor'),
				array('FitWindow','ShowBlocks','-','About'),
				
				array('Format','Font','FontSize'),//Styles
				array('TextColor','BGColor')
			);
		}
		$config['SkinPath'] = 'skins/' . $skinpath . '/';
		//dump($config);exit();
		return $this->cke[$nazwa]->editor($nazwa, $tresc, $config);
    }
	
	function mce($nazwa, $tresc, $height, $toolbar, $width, $skinpath, $onlyread = false)
	{
		$ver = 3;
		$new = $ver > 3;
		$toolbar = $new ? 'modern' : 'advanced';//simple
		$skinpath = $new ? 'lightgray' : 'o2k7';
		$skin_variant = 'silver';
		$theme = 'full';//simple
		
		$tinyMCE = 
		'<textarea name="'.$nazwa.'" id="'.$nazwa.'" style="width:'.$width.'px; height:'.$height.'px;">'.$tresc.'</textarea>';
		
		$filemanager = 'fileBrowserCallBack';
		if(true || $this->obConfig->filebrowser == 'kcfinder')
		if(!$new) $filemanager = 'openKCFinder'; else
		$filemanager = 'function(field_name, url, type, win)
		{
			tinyMCE.activeEditor.windowManager.open(
			{
				url: "'.$this->baseUrl.'/public/scripts/kcfinder/browse.php?opener=tinymce&type="+type, 
				width: 700, 
				height: 500,
				resizable: "yes",
				inline: "yes", 
				close_previous: "no",
				popup_css: false
			},
			{ window: win, input: field_name });
			return false;
		}';
		
		$pluginsSimple = 'advimage,advlink,media,contextmenu,table';
		$buttonsSimple = '
		theme_advanced_buttons1_add_before : "newdocument,separator",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect",
		theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle",
		theme_advanced_buttons2_add_before: "cut,copy,separator,",
		theme_advanced_buttons3_add_before : "",
		theme_advanced_buttons3_add : "media,table"';
		
		$pluginsFull = 'autolink,lists,spellchecker,pagebreak,layer,table,save,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,template';
		if($new) $pluginsFull .= ',anchor, charmap, hr, image, link, emoticons, code, textcolor';//compat3x
		else $pluginsFull .= ',style,advhr,advimage,advlink,emotions,iespell,inlinepopups,xhtmlxtras';
		$buttonsFull = '
		theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
		theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
		theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage"';
		
		$plugins = $theme == 'full' ? $pluginsFull : $pluginsSimple;
		$buttons = $theme == 'full' ? $buttonsFull : $buttonsSimple;
		
		$tinyMCE .= '<script type="text/javascript">';
		$tinyMCE .= 'tinyMCE.init(
		{
			language : "pl",
			entity_encoding : "raw",
			mode : "exact",
			elements : "'.$nazwa.'",
			theme : "'.$toolbar.'",
			skin : "'.$skinpath.'",
			skin_variant : "'.$skin_variant.'",
			plugins : "'.$plugins.'",
			width : "'.$width.'",
			height : "'.$height.'",
			'.$buttons.',
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_font_sizes : "8px,9px,10px,11px,12px,14px,16px,18px,20px,22px,24px,28px,32px,36px,48px",
			extended_valid_elements : "hr[class|width|size|noshade]",
			file_browser_callback : '.$filemanager.',
			paste_use_dialog : false,
			theme_advanced_resizing : true,
			theme_advanced_resize_horizontal : false,
			apply_source_formatting : true,
			force_br_newlines : true,
			force_p_newlines : false,
			forced_root_block : "",
			'.($onlyread == true ? 'readonly: true,': '').'
			relative_urls : true,
			document_base_url : "'.(0?$this->obConfig->www:'').'",
			remove_script_host : false,
			accessibility_warnings : false
		});';
		
		$tinyMCE .= '</script>';
		return $tinyMCE;
	}

    protected function produktyGaleria($prod) 
	{
		return $this->common->produktyGaleria($prod, 0, $this->obConfig->tylkoHurt, $this->obConfig->cenyNettoDomyslne, true);
    }
	
	private function menu_podstrony() 
	{
		/***Menu dla podstron***/
		$menu = new Kategorie();
		if($this->obConfig->podstrony)
		{
			$podstrony = $menu->podstronyAdmin(array('p.polozenie desc', 'if(p.lp>0,p.lp,99999) asc', 'p.temat asc'));
			$this->view->podstrony = $podstrony;
		}
		/***Menu dla strony glownej***/
		if($this->obConfig->podstrony)
		{
			$blok = $menu->blokAdmin();
			$this->view->blok = $blok;
		}
		if($this->obConfig->allegro)
		{
			$blokAllegro = $menu->blokAllegroAdmin();
			$this->view->blokAllegro = $blokAllegro;
		}
		if($this->obConfig->lojalnosc)
		{
			$blokLojalnosc = $menu->blokLojalnoscAdmin();
			$this->view->blokLojalnosc = $blokLojalnosc;
		}
	}
	
	function cmp($a, $b)
	{
		$cmp1 = $a[$this->sorter];
		$cmp2 = $b[$this->sorter];
		if($cmp1 == $cmp2) return 0;
		//echo $cmp1.' '.$cmp2.' '.(strcasecmp($cmp1, $cmp2)).'<br>';
		if($this->sortOrder == 'asc') return ($cmp1 < $cmp2) ? -1 : 1;
		if($this->sortOrder == 'desc') return ($cmp1 > $cmp2) ? -1 : 1;
	}
	function cmpPL($a, $b)
	{
		return strcasecmp($this->common->usunPolskieZnaki($a), $this->common->usunPolskieZnaki($b));
	}
	
	public function timeUpCheck($_start, $_end)
	{
		$this->jednostkiczasu = array('minut'=>60, 'godzin'=>3600, 'dni'=>86400, 'sekund'=>1);
		$this->wybranajednostka = 'dni';
		$start = strtotime($_start);
		$end = strtotime($_end);
		return $end - $start;
	}
}
?>